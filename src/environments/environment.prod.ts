export const environment = {
  production: true,
  baseUrl: 'https://svs-internal.prd.api-at.cloud.bpifrance.fr',
  oAuth: 'https://auth.web.bpifrance.fr',
  clientId: 'Z5iCXvkvCCMqp7eHoOV0',
  env: 'Prod',
  fullUrlOAuth: 'https://auth.web.bpifrance.fr/mga/sps/oauth/oauth20/metadata/OIDC'

};
