export const environment = {
  production: true,
  baseUrl: 'https://svs-internal.dev.api-at.cloud.bpifrance.fr',
  oAuth: 'https://authmoa.web.bpifrance.fr',
  clientId: 'EQSM2m6ceHsTCzkMMdtj',
  env: 'Prod',
  fullUrlOAuth: 'https://authmoa.web.bpifrance.fr/mga/sps/oauth/oauth20/metadata/OIDCP'
};
