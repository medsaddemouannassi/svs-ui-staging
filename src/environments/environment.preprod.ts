export const environment = {
  production: true,
  baseUrl: 'https://svs-internal.preprod.api-at.cloud.bpifrance.fr',
  oAuth: 'https://authppd.web.bpifrance.fr',
  clientId: '0Na3A7cdQE5hwALSt1Ip',
  env: 'Prod',
  fullUrlOAuth: 'https://authppd.web.bpifrance.fr/mga/sps/oauth/oauth20/metadata/OIDCP_All-Prof'
};
