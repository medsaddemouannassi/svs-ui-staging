import { Pipe, PipeTransform } from '@angular/core';
import { ParamSettingModel } from '../models/param-setting.model';


@Pipe({

    name: 'docType'

})

export class DocTypePipe implements PipeTransform {



    transform(docTypeName: string, params: ParamSettingModel[]): unknown {


        if (docTypeName != null && docTypeName != '' && docTypeName != 'E3031')

            return params.find(x => x.value == docTypeName).label;

        else {
            return 'Non renseigné'
        }

    }



}
