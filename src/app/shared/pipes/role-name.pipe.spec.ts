import { RoleNamePipe } from './role-name.pipe';
import { Role, RoleNames } from '../models/roles.enum';


describe('RoleNamePipe', () => {
  // This pipe is a pure, stateless function so no need for BeforeEach
  const pipe = new RoleNamePipe();
  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('transforms "role" to "role with no  underscore"', () => {
    expect(pipe.transform(Role.CA)).toBe((RoleNames.find(r => r.RoleName == "Chargé d'Affaires")).RoleName);
    expect(pipe.transform(Role.SVS_EXPERT_SE)).toBe((RoleNames.find(r => r.RoleName == "Expert SE")).RoleName);
    expect(pipe.transform(Role.SVS_CONTENTIEUX)).toBe((RoleNames.find(r => r.RoleName == "Contentieux")).RoleName);
    expect(pipe.transform(Role.ADMIN_SE)).toBe((RoleNames.find(r => r.RoleName == "Filière SE")).RoleName);
  });

  it('transforms an unknown role', () => {
    expect(pipe.transform('')).toBe("Non reconnu");
    expect(pipe.transform(null)).toBe("Non reconnu");
  });

  // ... more tests ...
});