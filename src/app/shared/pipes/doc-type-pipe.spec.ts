import { ParamSettingModel } from '../models/param-setting.model';
import { DocTypePipe } from './doc-type.pipe';


describe('DocTypePipe', () => {
    // This pipe is a pure, stateless function so no need for BeforeEach
    const pipe = new DocTypePipe();
    const paramSettings: ParamSettingModel[] = [
        { "id": "3", "code": "GED_SUBTYPES", "label": "AR du bon de commande", "value": "C224", "effectiveDate": null, "isActiv": true },
        { "id": "2", "code": "GED_SUBTYPES", "label": "Autres", "value": "E303", "effectiveDate": null, "isActiv": true },
        { "id": "4", "code": "GED_SUBTYPES", "label": "Bon de commande", "value": "C218", "effectiveDate": null, "isActiv": true },
        { "id": "5", "code": "GED_SUBTYPES", "label": "Facture proforma", "value": "C217", "effectiveDate": null, "isActiv": true }]
    it('create an instance', () => {
        expect(pipe).toBeTruthy();
    });

    it('transforms "doc type" to "full label"', () => {
        expect(pipe.transform("C224", paramSettings)).toBe("AR du bon de commande");
        expect(pipe.transform("E303", paramSettings)).toBe("Autres");
        expect(pipe.transform("C218", paramSettings)).toBe("Bon de commande");
        expect(pipe.transform("C217", paramSettings)).toBe("Facture proforma");

    });

    it('transforms an empty doc type', () => {
        expect(pipe.transform(null, paramSettings)).toBe("Non renseigné");
    });
    it('transforms a null doc type', () => {
        expect(pipe.transform("", paramSettings)).toBe("Non renseigné");
    });

    it('transforms a doc type of E3031', () => {
        expect(pipe.transform("E3031", paramSettings)).toBe("Non renseigné");
    });

    // ... more tests ...
});