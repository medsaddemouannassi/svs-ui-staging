import { Pipe, PipeTransform } from '@angular/core';
import { RoleNames } from '../models/roles.enum';

@Pipe({
  name: 'roleName'
})
export class RoleNamePipe implements PipeTransform {

  transform(role: string): unknown {
    if (role != null && role != '')

      return RoleNames.find(r => r.RoleCode == role).RoleName;

    else {
      return 'Non reconnu'
    }

  }
}