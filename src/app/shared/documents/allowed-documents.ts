export const AllowedDocumentsConstant = {
    EXTENSION_LIST: ['txt','bmp','gif','jpeg','jpg','png','tif','msg','xls','xlt','xla','csv','xlsx','xltx','xlsm','xltm',
    'ppt','pot','pps','ppa','pptx','potx','ppsx','pptm','potm','ppsm','vsd','doc','dot','docx','dotx','docm','htm','html','pdf','TXT','BMP','GIF','JPEG','JPG','PNG','TIF','MSG','XLS','XLT','XLA','CSV','XLSX','XLTX','XLSM','XLTM',
    'PPT','POT','PPS','PPA','PPTX','POTX','PPSX','PPTM','POTM','PPSM','VSD','DOC','DOT','DOCX','DOTX','DOCM','HTM','HTML','PDF'],
    MAX_SIZE : 41943040
  };
