import { Component, forwardRef, Input, OnDestroy, OnInit } from '@angular/core';
import { ControlValueAccessor, FormBuilder, FormControl, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ConfirmationModalComponent } from '../confirmation-modal/confirmation-modal.component';
import { PopupService } from '../services/popup.service';
import { validateDocument } from './document-validator';
import { NgxFileDropEntry, FileSystemFileEntry } from 'ngx-file-drop';

@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DocumentsComponent),
      multi: true
    }
  ]
})
export class DocumentsComponent implements OnInit, ControlValueAccessor, OnDestroy {

  docForm: FormGroup;
  docErr = false;
  subscriptions: Subscription[] = [];
  documentName = ''
  public editedDocumentList = [];

  @Input() control: FormControl;
  public documentList: File[] = [];

  constructor(
    private fb: FormBuilder,
    private popupService: PopupService
  ) { }

  ngOnInit(): void {
    this.docForm = this.fb.group({
      files: [[]]
    })
    this.subscriptions.push(
      this.docForm.valueChanges.subscribe(value => {
        this.onChange(value);
        this.onTouched();
      }),

    );



  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  setFormTouched() {
    this.docForm.get('files').markAllAsTouched();

  }

  editDocument(index: number) {
    if (this.editedDocumentList[index]) {
      this.documentList[index] = new File([this.documentList[index]], this.documentName, { type: this.documentList[index].type });
      this.docForm.get('files').patchValue(this.documentList);
      this.editedDocumentList[index] = false;
    }
    else {
      this.documentName = this.documentList[index].name;
      this.editedDocumentList[index] = true;
    }

  }

  deleteDocument(index: number): void {
    if (!this.editedDocumentList[index]) {

      this.popupService.popupInfo('Êtes-vous sûr(e) de vouloir supprimer la pièce jointe ?', 'Confirmer', null, 'Annuler', ConfirmationModalComponent);
      const subscription = this.popupService.onConfirm.subscribe((t) => {
        if (t) {
          this.documentList.splice(index, 1);
          this.editedDocumentList.splice(index, 1);

          this.docForm.get('files').patchValue(this.documentList);


        }
        subscription.unsubscribe()

      });
    }
  }


  onBlurDocument(index: number) {
    this.documentList[index] = new File([this.documentList[index]], this.documentName, { type: this.documentList[index].type });
    this.docForm.get('files').patchValue(this.documentList);
    this.editedDocumentList[index] = false;
  }



  get value() {
    return this.docForm.value;
  }

  set value(value) {

    if (value && value.files) {
      this.docForm.setValue(value);
      this.documentList = value.files;

      this.onChange(value);
      this.onTouched();
    } else {

      this.docForm.reset();
      this.documentList = [];
    }
  }
  onChange: any = () => {
  //this is intentional
  };
  onTouched: any = () => {
   //this is intentional
   };

  registerOnChange(fn): void {
    this.onChange = fn;
  }

  writeValue(value) {
    this.value = value
  }
  registerOnTouched(fn) {
    this.onTouched = fn;
  }

  setDisabledState(disabled: boolean) {

    if (disabled) {
      this.docForm.disable();
    } else {
      this.docForm.enable();
    }


  }

  dropped(files: NgxFileDropEntry[]) {

    files.map(f => f.fileEntry)
      .filter(f => f.isFile).forEach(f => {
        const fileEntry = f as FileSystemFileEntry;

        fileEntry.file(file => {

          if (validateDocument(file)) {


            this.documentList.push(file);
          }

        })


      })
      this.docForm.get('files').setValue(this.documentList);
      this.docForm.get('files').markAllAsTouched();


  }

}
