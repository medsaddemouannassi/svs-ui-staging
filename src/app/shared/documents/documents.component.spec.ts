import { FormBuilder, FormControl } from '@angular/forms';
import { FileSystemFileEntry, NgxFileDropEntry } from 'ngx-file-drop';
import { of, Subscription } from 'rxjs';
import { documentsValidator } from './document-validator';
import { DocumentsComponent } from "./documents.component";


describe('DocumentsComponent', () => {
  let fixture: DocumentsComponent;
  let formBuilderMock: FormBuilder;
  let popupServiceMock: any;
  let subscriptionMock: Subscription;




  beforeEach(async () => {
    subscriptionMock = new Subscription();


    formBuilderMock = new FormBuilder();


    popupServiceMock = {
      popupInfo: jest.fn().mockReturnValue(of(true)),
      onConfirm: of(true)
    };

    fixture = new DocumentsComponent(
      formBuilderMock,
      popupServiceMock
    );
    fixture.ngOnInit();


  });

  describe('Test Component', () => {
    it('should Be Truthy', () => {
      expect(fixture).toBeTruthy();
    });
  });


  describe('Test: ngOnInit', () => {
    it('should initialize form', () => {

      const form = {

        files: []
      };

      expect(fixture.docForm.value).toEqual(form);

    });

  });
  describe('Test ngOnDestroy', () => {
    it('should unsubscribe', () => {
      fixture.ngOnDestroy();
      const spyunsubscribe = jest.spyOn(subscriptionMock, 'unsubscribe');
      expect(spyunsubscribe).toBeDefined();
    });
  });


  describe('Test registerOnChange', () => {
    it('should change value', () => {
      let test;
      fixture.registerOnChange(test);
      expect(fixture.onChange).toEqual(test);
    });
  });

  describe('Test writeValue', () => {
    it('should  write value', () => {
      let value = ({

        files: [{} as File]
      });

      fixture.writeValue(value);
      expect(fixture.value).toEqual(value);



    });
    it('should not write value', () => {
      let value = ({

        files: []
      });

      fixture.writeValue(value);
      value = null;
      fixture.writeValue(value);
      expect(fixture.value).not.toEqual(value);


    });
  });


  describe('Test registerOnTouched', () => {
    it('should change onTouched ', () => {
      fixture.registerOnTouched(test);
      expect(fixture.onTouched).not.toEqual(() => {
      //this is intentional
      });
      expect(fixture.onTouched).toEqual(test);

    });
  });

  describe('Test delete document', () => {
    it('should add the document ', () => {
      let files = [{ name: 'file3.png', size: 17567 } as File, { name: 'file2.png', size: 17543 } as File]


      fixture.documentList = files;
      fixture.deleteDocument(1);
      expect(fixture.docForm.value).toEqual({ files: [files[0]] });


    });
  });

  describe('Test form touched', () => {
    it('should add the document ', () => {


      fixture.setFormTouched();
      expect(fixture.docForm.get('files').touched).toEqual(true);


    });
  });


  describe('Test onBlurDocument', () => {
    it('should change name ', () => {
      fixture.documentName = 'newDocumentName'

      fixture.documentList.push({ name: 'file3.png', size: 17567 } as File, { name: 'file2.png', size: 17543 } as File)
      fixture.onBlurDocument(0);
      expect(fixture.documentList[0].name).toEqual('newDocumentName');


    });
  });

  describe('Test editDocument', () => {
    it('should change name of document', () => {
      fixture.documentList.push({ name: 'file3.png', size: 17567 } as File, { name: 'file2.png', size: 17543 } as File)

      fixture.editedDocumentList.push(true)
      fixture.documentName = 'newDocumentName'

      fixture.editDocument(0);
      expect(fixture.documentList[0].name).toEqual('newDocumentName');
      expect(fixture.editedDocumentList[0]).toBeFalsy();


    });
    it('should set name to documentName', () => {
      fixture.documentName = 'oldDocumentName'
      fixture.documentList.push({ name: 'file3.png', size: 17567 } as File, { name: 'file2.png', size: 17543 } as File)

      fixture.editedDocumentList.push(false)
      fixture.editDocument(0);
      expect(fixture.documentName).toEqual('file3.png');
      expect(fixture.editedDocumentList[0]).toBeTruthy();


    });
  });

  describe('Test setDisabledState', () => {
    it('should enable form ', () => {
      fixture.setDisabledState(false);
      expect(fixture.docForm.disabled).toBeFalsy();


    });

    it('should desable form ', () => {
      fixture.setDisabledState(true);
      expect(fixture.docForm.disabled).toBeTruthy();


    });
  });


  describe('Test dropped()', () => {


    it('should open modal when drop file', () => {
      let file:FileSystemFileEntry = {
        isFile: true,
        isDirectory: false,
        name: null,
        file: <T>(callback: (filea: File) => T) => callback({name: 'file3.png', size: 17567} as File),
      }
      fixture.documentList = []
      fixture.dropped([new NgxFileDropEntry('',  file )])
      expect(fixture.documentList.length).toEqual(1)
    })
  });

  
  describe('Test validators', () => {


    it('should validate', () => {
     
      expect(documentsValidator({value :[{name: 'file3.png', size: 17567}]} as FormControl )).toBeNull()
    })
  });

});


