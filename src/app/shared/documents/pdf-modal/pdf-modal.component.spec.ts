import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PdfModalComponent } from './pdf-modal.component';


describe('PdfModalComponent', () => {
  let fixture : PdfModalComponent;
  let ngbActiveModalMock: NgbActiveModal;


  beforeEach(() => {
    ngbActiveModalMock = new NgbActiveModal();
    fixture = new PdfModalComponent (ngbActiveModalMock);
  });

  it('should create', () => {
    expect(fixture).toBeTruthy();
  });
  it('should confirmModal', () => {
    fixture.confirmModal();
    expect(fixture.onConfirm).toHaveBeenCalled;
  });
  
  it('should exitModal', () => {
    fixture.exitModal();
    expect(fixture.onConfirm).toHaveBeenCalled;
    expect(fixture.onExit).toHaveBeenCalled;
  });
  it('should getPDFPath', () => {
    fixture.file = new File(["iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg=="], "filename");
    fixture.getPDFPath();
    expect(fixture.onConfirm).toHaveBeenCalled;
    expect(fixture.onExit).toHaveBeenCalled;
  });

  
});
