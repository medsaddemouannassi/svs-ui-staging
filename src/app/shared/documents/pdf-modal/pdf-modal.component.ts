import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { pdfDefaultOptions } from 'ngx-extended-pdf-viewer';


@Component({
  selector: 'app-pdf-modal',
  templateUrl: './pdf-modal.component.html',
  styleUrls: ['./pdf-modal.component.css']
})
export class PdfModalComponent {
  @Input() file;
  @Input() confirmBtnMessage;
  @Input() cancelBtnMessage;
  @Input() pdfSrc ;
  @Input() isPdf ;
  @Output() onConfirm = new EventEmitter<boolean>();
  @Output() onExit = new EventEmitter<boolean>();
  constructor(public activeModal: NgbActiveModal) {
    pdfDefaultOptions.assetsFolder = 'bleeding-edge';

   }

 


  confirmModal() {
    this.onConfirm.emit(true);

  }
  exitModal() {
    this.onExit.emit(false);
    this.onConfirm.emit(false);
  }
  getPDFPath() {
    if (this.file) {
      var reader = new FileReader();
      reader.readAsDataURL(this.file);
      reader.onload = (event: any) => {
        this.pdfSrc = event.target.result;
      }
    }
  }

}
