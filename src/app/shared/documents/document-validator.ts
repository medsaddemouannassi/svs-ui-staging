import { FormControl } from '@angular/forms';
import { AllowedDocumentsConstant } from './allowed-documents';




export function documentsValidator(control: FormControl) {
  if (control.value) {

    let value: File[] = Array.isArray(control.value) ? control.value : control.value.files;

    let docValid = true;
    if (value) {
      value.forEach(element => {
        docValid = docValid && validateDocument(element);
      });
    }


    
    return docValid ? null : { docError: true };

  } else {

    return null;
  }
}


export function validateDocument(file: File): boolean {
  let isValid = true;
  isValid = isValid && file.size < AllowedDocumentsConstant.MAX_SIZE;
  var regexFileName = /(?:\.([^.]+))?$/;
  const extension = regexFileName.exec(file.name)[1];
  return isValid && AllowedDocumentsConstant.EXTENSION_LIST.findIndex(ext => extension == ext) > -1;
}
