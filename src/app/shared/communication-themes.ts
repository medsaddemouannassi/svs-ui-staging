import { Role } from './models/roles.enum';
import { PagesConstants } from './pages-constant';
import { StatusConstant } from './status-constant';

export interface CommSpecsModal {
    page: string;
    user: Role;
    actor: Role[];
    status: string[];
    lastStatus: string[];
    history: string[];
    action?: string;
}

export interface HistoryMessage {
    from?: Role,
    to?: Role,
    date?: Date,
    message?: string

}

export interface CommunicationModal {
    Theme: string;
    Specs: CommSpecsModal[];
}

export const communicationThemes: CommunicationModal[] =
    [
        {
            Theme: "Demande d'information", Specs: [

                { page: PagesConstants.RECEPTION, user: Role.ADMIN_SE, actor: [Role.SVS_EXPERT_SE], history: [], status: [StatusConstant.WAIT_VALID_FIL],lastStatus : [] , action: StatusConstant.WAIT_INFO_FIL_EXP },
                { page: PagesConstants.RESULT,    user: Role.ADMIN_SE, actor: [Role.SVS_EXPERT_SE], history: [], status: [StatusConstant.WAIT_VALID_FIL],lastStatus : [] , action: StatusConstant.WAIT_INFO_FIL_EXP },
                { page: PagesConstants.RECEPTION_SIMULATION, user: Role.ADMIN_SE, actor: [Role.SVS_EXPERT_SE], history: [], status: [StatusConstant.WAIT_VALID_FIL],lastStatus : [] , action: StatusConstant.WAIT_INFO_FIL_EXP },
                { page: PagesConstants.RESULT_SIMULATION,    user: Role.ADMIN_SE, actor: [Role.SVS_EXPERT_SE], history: [], status: [StatusConstant.WAIT_VALID_FIL],lastStatus : [] , action: StatusConstant.WAIT_INFO_FIL_EXP },

            ]
        },
        {
            Theme: "Demande de complétude" , Specs:  [
                { page: PagesConstants.RECEPTION, user: Role.SVS_EXPERT_SE, actor: [Role.CA], history: [], status: [StatusConstant.WAIT_EXP_VAL],lastStatus:[], action: StatusConstant.WAIT_INFO_EXP_CA },

            ]
        },
        {
            Theme: "Refus de valorisation", Specs: [
                {
                    page: PagesConstants.RECEPTION, user: Role.SVS_EXPERT_SE, actor: [Role.CA], history: [],lastStatus : [] , action: StatusConstant.REJECTED,
                    status: [
                        StatusConstant.WAIT_EXP_VAL,
                        StatusConstant.WAIT_INFO_EXP_CA,
                        StatusConstant.WAIT_INFO_EXP_FIL,
                        StatusConstant.WAIT_VALID_MAN_EXP_QUAL,
                        StatusConstant.VAL_FIL_REJ

                    ]
                },

                {
                    page: PagesConstants.RESULT, user: Role.SVS_EXPERT_SE, actor: [Role.CA], history: [],lastStatus : [] , action: StatusConstant.REJECTED,
                    status: [
                        StatusConstant.WAIT_EXP_VAL,
                        StatusConstant.WAIT_INFO_EXP_CA,
                        StatusConstant.WAIT_INFO_EXP_FIL,
                        StatusConstant.WAIT_VALID_MAN_EXP_QUAL,
                        StatusConstant.VAL_FIL_REJ

                    ]
                },
                

            ]
        },
        {
            Theme: "Rejet de la demande de validation", Specs: [
                { page: PagesConstants.RECEPTION, user: Role.ADMIN_SE, actor: [Role.SVS_EXPERT_SE], history: [], status: [StatusConstant.WAIT_VALID_FIL],lastStatus : [] , action: StatusConstant.VAL_FIL_REJ },
                { page: PagesConstants.RESULT,    user: Role.ADMIN_SE, actor: [Role.SVS_EXPERT_SE], history: [], status: [StatusConstant.WAIT_VALID_FIL],lastStatus : [] , action: StatusConstant.VAL_FIL_REJ },
                { page: PagesConstants.RECEPTION, user: Role.SVS_EXPERT_SE, actor: [Role.SVS_EXPERT_SE], history: [], status: [StatusConstant.WAIT_VALID_MAN_EXP_QUAL],lastStatus : [] , action: StatusConstant.VAL_MAN_EXP_QUALI_REJ },
                { page: PagesConstants.RESULT   , user: Role.SVS_EXPERT_SE, actor: [Role.SVS_EXPERT_SE], history: [], status: [StatusConstant.WAIT_VALID_MAN_EXP_QUAL],lastStatus : [] , action: StatusConstant.VAL_MAN_EXP_QUALI_REJ },
                { page: PagesConstants.RESULT_SIMULATION   , user: Role.SVS_EXPERT_SE, actor: [Role.SVS_EXPERT_SE], history: [], status: [StatusConstant.WAIT_VALID_MAN_EXP_QUAL],lastStatus : [] , action: StatusConstant.VAL_MAN_EXP_QUALI_REJ },
                { page: PagesConstants.RESULT_SIMULATION,    user: Role.ADMIN_SE, actor: [Role.SVS_EXPERT_SE], history: [], status: [StatusConstant.WAIT_VALID_FIL],lastStatus : [] , action: StatusConstant.VAL_FIL_REJ },
                { page: PagesConstants.RESULT_SIMULATION   , user: Role.SVS_EXPERT_SE, actor: [Role.SVS_EXPERT_SE], history: [], status: [StatusConstant.WAIT_VALID_MAN_EXP_QUAL],lastStatus : [] , action: StatusConstant.VAL_MAN_EXP_QUALI_REJ },

            ]
        },

        {
            Theme: "Répondre à la demande d'information", Specs: [


                { page: PagesConstants.RECEPTION, user: Role.SVS_EXPERT_SE, actor: [Role.ADMIN_SE], history: [StatusConstant.WAIT_INFO_FIL_EXP, StatusConstant.RESP_INFO_EXP_FIL], status: [StatusConstant.WAIT_INFO_FIL_EXP],lastStatus : [] , action: StatusConstant.RESP_INFO_EXP_FIL },

                { page: PagesConstants.RECEPTION_SIMULATION, user: Role.SVS_EXPERT_SE, actor: [Role.ADMIN_SE], history: [StatusConstant.WAIT_INFO_FIL_EXP, StatusConstant.RESP_INFO_EXP_FIL], status: [StatusConstant.WAIT_INFO_FIL_EXP],lastStatus : [] , action: StatusConstant.RESP_INFO_EXP_FIL },

                { page: PagesConstants.RECEPTION, user: Role.ADMIN_SE, actor: [Role.SVS_EXPERT_SE], history: [StatusConstant.WAIT_INFO_FIL_EXP, StatusConstant.RESP_INFO_EXP_FIL, StatusConstant.WAIT_INFO_EXP_FIL, StatusConstant.RESP_INFO_FIL_EXP], status: [StatusConstant.WAIT_INFO_EXP_FIL],lastStatus : [] , action: StatusConstant.RESP_INFO_FIL_EXP },
                { page: PagesConstants.RECEPTION_SIMULATION, user: Role.ADMIN_SE, actor: [Role.SVS_EXPERT_SE], history: [StatusConstant.WAIT_INFO_FIL_EXP, StatusConstant.RESP_INFO_EXP_FIL, StatusConstant.WAIT_INFO_EXP_FIL, StatusConstant.RESP_INFO_FIL_EXP], status: [StatusConstant.WAIT_INFO_EXP_FIL],lastStatus : [] , action: StatusConstant.RESP_INFO_FIL_EXP },

                { page: PagesConstants.RESULT, user: Role.SVS_EXPERT_SE, actor: [Role.ADMIN_SE], history: [StatusConstant.WAIT_INFO_FIL_EXP, StatusConstant.RESP_INFO_EXP_FIL], status: [StatusConstant.WAIT_INFO_FIL_EXP],lastStatus : [] , action: StatusConstant.RESP_INFO_EXP_FIL },


                { page: PagesConstants.RESULT, user: Role.ADMIN_SE, actor: [Role.SVS_EXPERT_SE], history: [StatusConstant.WAIT_INFO_FIL_EXP, StatusConstant.RESP_INFO_EXP_FIL, StatusConstant.WAIT_INFO_EXP_FIL, StatusConstant.RESP_INFO_FIL_EXP], status: [StatusConstant.WAIT_INFO_EXP_FIL],lastStatus : [] , action: StatusConstant.RESP_INFO_FIL_EXP },

                { page: PagesConstants.REQUEST, user: Role.CA, actor: [Role.SVS_EXPERT_SE], history: [StatusConstant.WAIT_INFO_EXP_CA, StatusConstant.RESP_INFO_CA_EXP], status: [StatusConstant.WAIT_INFO_EXP_CA],lastStatus : [] , action: StatusConstant.RESP_INFO_CA_EXP },

            ]
        },

        {
            Theme: "Réponse à demande de complétude", Specs: [


                { page: PagesConstants.RECEPTION, user: Role.SVS_EXPERT_SE, actor: [Role.CA], history: [StatusConstant.WAIT_INFO_EXP_CA,StatusConstant.RESP_INFO_CA_EXP], status: [StatusConstant.WAIT_EXP_VAL],lastStatus : [StatusConstant.RESP_INFO_CA_EXP] , action: null },
                { page: PagesConstants.RESULT, user: Role.SVS_EXPERT_SE, actor: [Role.CA], history: [StatusConstant.WAIT_INFO_EXP_CA,StatusConstant.RESP_INFO_CA_EXP], status: [StatusConstant.WAIT_EXP_VAL],lastStatus : [StatusConstant.RESP_INFO_CA_EXP] , action: null },

            ]
        },

        {
            Theme: "Réponse à demande de précision", Specs: [

                { page: PagesConstants.RECEPTION, user: Role.SVS_EXPERT_SE, actor: [Role.ADMIN_SE], history: [StatusConstant.WAIT_INFO_EXP_FIL, StatusConstant.RESP_INFO_FIL_EXP], status: [StatusConstant.WAIT_EXP_VAL],lastStatus : [StatusConstant.RESP_INFO_FIL_EXP] , action: null },
                { page: PagesConstants.RESULT, user: Role.SVS_EXPERT_SE, actor: [Role.ADMIN_SE], history: [StatusConstant.WAIT_INFO_EXP_FIL, StatusConstant.RESP_INFO_FIL_EXP], status: [StatusConstant.WAIT_EXP_VAL],lastStatus : [StatusConstant.RESP_INFO_FIL_EXP] , action: null },
                { page: PagesConstants.RECEPTION_SIMULATION, user: Role.SVS_EXPERT_SE, actor: [Role.ADMIN_SE], history: [StatusConstant.WAIT_INFO_EXP_FIL, StatusConstant.RESP_INFO_FIL_EXP], status: [StatusConstant.WAIT_EXP_VAL],lastStatus : [StatusConstant.RESP_INFO_FIL_EXP] , action: null },

            ]
        },
        {
            Theme: "Réponse à demande d'information", Specs: [


                { page: PagesConstants.RESULT,    user: Role.ADMIN_SE, actor: [Role.SVS_EXPERT_SE], history: [StatusConstant.WAIT_INFO_FIL_EXP, StatusConstant.RESP_INFO_EXP_FIL], status: [StatusConstant.WAIT_VALID_FIL],lastStatus : [StatusConstant.RESP_INFO_EXP_FIL] , action: null },
                { page: PagesConstants.RECEPTION, user: Role.ADMIN_SE, actor: [Role.SVS_EXPERT_SE], history: [StatusConstant.WAIT_INFO_FIL_EXP, StatusConstant.RESP_INFO_EXP_FIL], status: [StatusConstant.WAIT_VALID_FIL],lastStatus : [StatusConstant.RESP_INFO_EXP_FIL] , action: null },

            ]
        },
        {
            Theme: "Refus de valorisation", Specs: [


                { page: PagesConstants.REQUEST,    user: Role.CA, actor: [Role.SVS_EXPERT_SE], history: [StatusConstant.REJECTED], status: [StatusConstant.REJECTED],lastStatus : [] , action: null },

            ]
        },
      
        {
            Theme: "Rejet de la demande de validation", Specs: [


                { page: PagesConstants.RESULT,    user: Role.SVS_EXPERT_SE, actor: [Role.SVS_EXPERT_SE], history: [StatusConstant.VAL_MAN_EXP_QUALI_REJ], status: [StatusConstant.WAIT_EXP_VAL],lastStatus : [StatusConstant.VAL_MAN_EXP_QUALI_REJ] , action: null },
                { page: PagesConstants.RECEPTION, user: Role.SVS_EXPERT_SE, actor: [Role.SVS_EXPERT_SE], history: [StatusConstant.VAL_MAN_EXP_QUALI_REJ], status: [StatusConstant.WAIT_EXP_VAL],lastStatus : [StatusConstant.VAL_MAN_EXP_QUALI_REJ] , action: null },
                { page: PagesConstants.RESULT,    user: Role.SVS_EXPERT_SE, actor: [Role.ADMIN_SE], history: [StatusConstant.VAL_FIL_REJ], status: [StatusConstant.VAL_FIL_REJ],lastStatus : [] , action: null },
                { page: PagesConstants.RESULT_SIMULATION,    user: Role.SVS_EXPERT_SE, actor: [Role.SVS_EXPERT_SE], history: [StatusConstant.VAL_MAN_EXP_QUALI_REJ], status: [StatusConstant.WAIT_EXP_VAL],lastStatus : [StatusConstant.VAL_MAN_EXP_QUALI_REJ] , action: null },
                { page: PagesConstants.RESULT_SIMULATION,    user: Role.SVS_EXPERT_SE, actor: [Role.ADMIN_SE], history: [StatusConstant.VAL_FIL_REJ], status: [StatusConstant.VAL_FIL_REJ],lastStatus : [] , action: null },
                { page: PagesConstants.RECEPTION_SIMULATION, user: Role.SVS_EXPERT_SE, actor: [Role.SVS_EXPERT_SE], history: [StatusConstant.VAL_MAN_EXP_QUALI_REJ], status: [StatusConstant.WAIT_EXP_VAL],lastStatus : [StatusConstant.VAL_MAN_EXP_QUALI_REJ] , action: null },

            ]
        },


        {
            Theme: "Demande de précisions à la filière", Specs: [
                { page: PagesConstants.RECEPTION, user: Role.SVS_EXPERT_SE, history: [StatusConstant.WAIT_INFO_FIL_EXP, StatusConstant.RESP_INFO_EXP_FIL, StatusConstant.WAIT_INFO_EXP_FIL, StatusConstant.RESP_INFO_FIL_EXP], actor: [Role.ADMIN_SE], status: [StatusConstant.WAIT_EXP_VAL, StatusConstant.WAIT_VALID_MAN_EXP_QUAL,StatusConstant.VAL_FIL_REJ],lastStatus : [] , action: StatusConstant.WAIT_INFO_EXP_FIL },
                { page: PagesConstants.RESULT,    user: Role.SVS_EXPERT_SE, history: [StatusConstant.WAIT_INFO_FIL_EXP, StatusConstant.RESP_INFO_EXP_FIL, StatusConstant.WAIT_INFO_EXP_FIL, StatusConstant.RESP_INFO_FIL_EXP], actor: [Role.ADMIN_SE], status: [StatusConstant.WAIT_EXP_VAL, StatusConstant.WAIT_VALID_MAN_EXP_QUAL,StatusConstant.VAL_FIL_REJ],lastStatus : [] , action: StatusConstant.WAIT_INFO_EXP_FIL },
                { page: PagesConstants.RECEPTION_SIMULATION, user: Role.SVS_EXPERT_SE, history: [StatusConstant.WAIT_INFO_FIL_EXP, StatusConstant.RESP_INFO_EXP_FIL, StatusConstant.WAIT_INFO_EXP_FIL, StatusConstant.RESP_INFO_FIL_EXP], actor: [Role.ADMIN_SE], status: [StatusConstant.WAIT_EXP_VAL, StatusConstant.WAIT_VALID_MAN_EXP_QUAL,StatusConstant.VAL_FIL_REJ],lastStatus : [] , action: StatusConstant.WAIT_INFO_EXP_FIL },
                { page: PagesConstants.RESULT_SIMULATION,    user: Role.SVS_EXPERT_SE, history: [StatusConstant.WAIT_INFO_FIL_EXP, StatusConstant.RESP_INFO_EXP_FIL, StatusConstant.WAIT_INFO_EXP_FIL, StatusConstant.RESP_INFO_FIL_EXP], actor: [Role.ADMIN_SE], status: [StatusConstant.WAIT_EXP_VAL, StatusConstant.WAIT_VALID_MAN_EXP_QUAL,StatusConstant.VAL_FIL_REJ],lastStatus : [] , action: StatusConstant.WAIT_INFO_EXP_FIL }

            ]
        },
 
        {
            Theme: "Rejet de la demande d'ajout", Specs: [
                { page: PagesConstants.CATALOG_ADD, user: Role.ADMIN_SE, actor: [Role.SVS_EXPERT_SE], history: [], status: [""],lastStatus : [] , action: StatusConstant.STATUS_RQST_WKFL_REJ },
                { page: PagesConstants.RECEPTION, user: Role.SVS_EXPERT_SE, actor: [Role.ADMIN_SE], history: [StatusConstant.STATUS_RQST_WKFL_REJ], status: [StatusConstant.STATUS_RQST_WKFL_REJ],lastStatus : [] , action: null },

            ]
        },

    ];


export function getThemesByRoleAndStatus(roles: Role[], status: string, lastStatus:string, page: string) {
    // map the specs of theme by user
    // filter if any of the roles is included and not empty

    const myClonedThemes = communicationThemes.map(x => Object.assign({}, x));

    let result= myClonedThemes.filter(theme => {
        theme.Specs = theme.Specs.filter(t => roles.includes(t.user) && t.status.includes(status) && t.page == page && (t.lastStatus.length == 0 || t.lastStatus.includes(lastStatus)));

        return theme.Specs.length > 0
    });

    return result;
   

    
}



export function getThemesByRoleAndStatusAndAction(roles: Role[], status: string,lastStatus : string, page: string , action: string) {
    // map the specs of theme by user
    // filter if any of the roles is included and not empty


    const myClonedThemes = getThemesByRoleAndStatus(roles, status,lastStatus, page).filter(theme => {

        return theme.Specs.filter(s => s.action == action).length > 0
    })



    return myClonedThemes.length > 0 ? myClonedThemes[0] : null;
}


export function getSpecsByPageAndRole(specs: CommSpecsModal[], page: string, roles: Role[]) {
    // created filter for the specific page
    // mapper to get only the actor
    // concat to flatten the arrays into on array of actors
    // set to remove the duplicates ones
    const myClonedSpecs = specs.map(x => Object.assign({}, x));

    return [...new Set([].concat(...(myClonedSpecs.filter(spec => spec.page == page && roles.includes(spec.user))).map(s => s.actor)))];
}


