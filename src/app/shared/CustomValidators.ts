import { AbstractControl, ValidatorFn } from "@angular/forms";
import { formatStringtoDate, formatDateToMatchForamt } from './utils';

export function percentageValidator(control: AbstractControl): { [key: string]: boolean } | null {

  if (control.value !== null && (isNaN(control.value) || control.value < 0 || control.value > 100)) {
    return { 'percentageValidator': true }
  }
  return null;
}


// registration number existance error
export function autocompleteCustomValidator(control: AbstractControl): { [key: string]: boolean } | null {
  let isRegValid = null;
  if (control.value !== null && control.value != '' && (typeof control.value !== 'object')) {
    //check if registration number is an object and not null
    isRegValid = { 'autocompleteCustomValidator': true };
  }
  return isRegValid;
}
// validator Apport
export function apportValidator(price: number): ValidatorFn {

  return (control: AbstractControl): { [key: string]: boolean } | null => {
    if (+control.value >= price || !price) {
      return { 'apportValidator': true }
    }
    return null;
  }
}

// Validator Date Facture Contencieux
export function dateValidator(today): ValidatorFn {
  return (control: AbstractControl): {
    [key: string]: boolean
  } | null => {
    if (control.value != null && control.value != undefined) {
      var myToday = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0);
      if (formatStringtoDate(formatDateToMatchForamt(control.value)) >= myToday) {
        return { 'dateValidator': true }

      }
      return null;
    }
  }
}



