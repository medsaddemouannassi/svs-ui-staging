export function getResidualValuePercent(price: number, rvEuro: number){
    return Math.round(rvEuro*1000/price*100)/1000;
}

export function getResidualValueEuro(price: number, vrPercent: number){
    return twoDigitDecimalRound((vrPercent*price/100).toString());
}

export function residualValuePercentageLimitValidator( vrPercent: number){
  return vrPercent> 10;
}
export function parseToDecimal (rvPercent:number) {
  return Math.round(rvPercent *100 )/100;
}
export function parseToInteger(vrEuro:number) {
  return Math.floor(vrEuro);
}

export function twoDigitDecimalRound(n: string): string {
  if (n)
  {
    if (n.indexOf('.') >= 0 && n.toString().substring(n.indexOf('.') + 1, n.length).length > 2) {
      n = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(Number(n)).replace(/,/g, '.');
      n = n.substr(0, n.length - 1);
    } else if (n.indexOf('.') >= 0 && n.toString().substring(n.indexOf('.') + 1, n.length).length === 1) {
      n = n + "0"; // 2.3 becomes 2.30
    }
    n = n.replace(/\s/g, '');//remove white spaces
    if (Number(n) === parseInt(n)) {
      return parseInt(n).toString(); // 2.00 becomes 2
    } else {
      return n;
    }
  }
}

export function  threeDigitDecimalRound(n: number): number {
  return Math.round(n * 1000) / 1000
}
