export const StatusConstant ={

	/********************************* NOTE: variables start with 'STATUS_' are PARAM CODE, others are PARAM VALUE *********************************/
    
	STATUS_VAL_WKFL: 'STATUS_VAL_WKFL',

  



    STATUS_CREATED: 'STATUS_CREATED',



    /*** STATUS WAITING INFO FIL TO EXP ***/

    WAIT_EXP_VAL: 'WAIT_EXP_VAL',



    /*** STATUS WAITING INFO FIL TO EXP ***/

    WAIT_INFO_EXP_CA: 'WAIT_INFO_EXP_CA',



    /*** STATUS WAITING INFO FIL TO EXP ***/

    RESP_INFO_CA_EXP: 'RESP_INFO_CA_EXP',



    /*** STATUS WAITING INFO FIL TO EXP ***/

    WAIT_INFO_EXP_FIL: 'WAIT_INFO_EXP_FIL',



    /*** STATUS WAITING INFO FIL TO EXP ***/

    RESP_INFO_FIL_EXP: 'RESP_INFO_FIL_EXP',



    /*** STATUS WAITING INFO FIL TO EXP ***/

    WAIT_VALID_MAN_EXP_QUAL: 'WAIT_VALID_MAN_EXP_QUAL',






    /*** STATUS WAITING INFO FIL TO EXP ***/
	// waiting for desc validation(filière)
    WAIT_VALID_FIL: 'WAIT_VALID_FIL',



    /*** STATUS WAITING INFO FIL TO EXP ***/

    WAIT_INFO_FIL_EXP: 'WAIT_INFO_FIL_EXP',



    /*** STATUS WAITING INFO FIL TO EXP ***/

    RESP_INFO_EXP_FIL: 'RESP_INFO_EXP_FIL',



    /*** STATUS WAITING INFO FIL TO EXP ***/

    VAL_FIL_REJ: 'VAL_FIL_REJ',

    VAL_MAN_EXP_QUALI_REJ: 'VAL_MAN_EXP_QUALI_REJ',

    /*** STATUS WAITING INFO FIL TO EXP ***/

    REJECTED: 'REJECTED',



    /*** STATUS WAITING INFO FIL TO EXP ***/

    VALIDATED: 'VALIDATED',
  
	
	//validée par la filière
	FIL_VALIDATED: 'FIL_VALIDATED',

	//validée par le DESC
    EXP_VALIDATED: 'EXP_VALIDATED',
    



      
    /** CATALOGUE ADD REQUEST STATUS */

	STATUS_RQST_WKFL_ATT: 'STATUS_RQST_WKFL_ATT',

	STATUS_RQST_WKFL_REJ: 'STATUS_RQST_WKFL_REJ',

	STATUS_RQST_WKFL_VAL: 'STATUS_RQST_WKFL_VAL',

	STATUS_RQST_WKFL: 'STATUS_RQST_WKFL',
	
    
    VAL_SIMULATION: 'VAL_SIMULATION'
	

}
export const StatusLabelConstant = {
    validatedParLaFilére: 'Validée par la filière',

    validatedParDEsc: 'Validée par le DESC',

    simulation: 'Simulation'
}