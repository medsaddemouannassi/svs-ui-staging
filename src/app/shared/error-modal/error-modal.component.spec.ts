import {ErrorModalComponent} from './error-modal.component';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

describe('ErrorModalComponent', () => {
  let fixture: ErrorModalComponent;
  let ngbActiveModalMock: NgbActiveModal;

  beforeEach(() => {
    ngbActiveModalMock = new NgbActiveModal();
    fixture = new ErrorModalComponent(ngbActiveModalMock);
  });

  it('should create', () => {
    expect(fixture).toBeTruthy();
  });
});
