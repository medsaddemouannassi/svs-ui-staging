import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-error-modal',
  templateUrl: './error-modal.component.html',
  styleUrls: ['./error-modal.component.css']
})
export class ErrorModalComponent implements OnInit {

  @Input() content;
  @Input() confirmBtnMessage;
  @Input() cancelBtnMessage;
  constructor(public activeModal: NgbActiveModal) {}

  ngOnInit(): void {
  // This is intentional
  }

}
