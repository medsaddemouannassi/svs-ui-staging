export function isBlankOrEmpty(str: string){
    return (!str || /^\s*$/.test(str));
}