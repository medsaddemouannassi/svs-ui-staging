import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DocumentConstant } from '../document-constant';
import { DocumentEditorModalComponent } from './document-editor-modal.component';



describe('documentListModal', () => {
  let fixture : DocumentEditorModalComponent;
  let ngbActiveModalMock: NgbActiveModal;



  beforeEach(() => {
    ngbActiveModalMock = new NgbActiveModal();

    fixture = new DocumentEditorModalComponent (ngbActiveModalMock);

    fixture.ngOnInit();
 
  });

  it('should create', () => {
    expect(fixture).toBeTruthy();
  });
  it('should confirmModal', () => {
    fixture.confirmModal();
    expect(fixture.onConfirm).toHaveBeenCalled;
  });
  
  it('should exitModal', () => {
    fixture.exitModal();
    expect(fixture.onConfirm).toHaveBeenCalled;
    expect(fixture.onExit).toHaveBeenCalled;
  });
  it('should update type E', () => {
    fixture.doc.docSubType = 'E303'
    fixture.updateIndex();
    expect(fixture.doc.docFamily).toEqual(DocumentConstant.FAMILY_VALUATION);
  });
  it('should update type C', () => {
    fixture.doc.docSubType = 'C217'
    fixture.updateIndex();
    expect(fixture.doc.docFamily).toEqual(DocumentConstant.FAMILY_CONTRACT);
  });

 

  
});
