import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DocumentConstant } from '../document-constant';
import { ValuationDocumentModel } from '../models/valuationDocument.model';

@Component({
  selector: 'app-document-editor-modal',
  templateUrl: './document-editor-modal.component.html',
  styleUrls: ['./document-editor-modal.component.css']
})
export class DocumentEditorModalComponent implements OnInit{

  @Input() document;
  @Input() index;
  @Input() docSubTypesListParams;
  @Output() onConfirm = new EventEmitter<{document :ValuationDocumentModel,index : number}>();
  @Output() onExit = new EventEmitter<ValuationDocumentModel>();

  doc;

  constructor(public activeModal: NgbActiveModal) { }
  ngOnInit(){
    this.doc = {...this.document}
  }

  confirmModal() {
    this.onConfirm.emit({document :this.doc,index : this.index});
  }
  exitModal() {
    this.onExit.emit(null);
  }

  updateIndex(){
    
    if(this.doc.docSubType == 'E303' || this.doc.docSubType == 'E3031'){
      this.doc.docFamily = DocumentConstant.FAMILY_VALUATION;
      this.doc.docType = DocumentConstant.TYPE_ENVIRONMENTAL_VALUATION;

    } 

    if(this.doc.docSubType == 'C217' || this.doc.docSubType == 'C218' || this.doc.docSubType == 'C224'  ){
      this.doc.docFamily = DocumentConstant.FAMILY_CONTRACT;
      this.doc.docType = DocumentConstant.TYPE_INTERNAL_CONTRACT;

    } 
  }
}
