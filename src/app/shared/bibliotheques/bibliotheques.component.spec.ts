import { BibliothequesComponent } from './bibliotheques.component';


describe('BibliothequesComponent', () => {
    let fixture: BibliothequesComponent;





    beforeEach(async () => {


        fixture = new BibliothequesComponent();
        fixture.ngOnInit();


    });


    describe('Test BibliothequesComponent', () => {
        it('should Be Truthy', () => {
            expect(fixture).toBeTruthy();
        })
    });

    describe('Test changeInputNumber', () => {
        it('shouldchange value', () => {
            fixture.changeInputNumber(5);
            expect(fixture.selectedNumber).toEqual(5);
        })
    });

    describe('Test changeTab()', () => {
        it('should be 3', () => {
            fixture.changeTab(3);
            expect(fixture.selectedTab).toEqual(3);
        })
    });

  });

