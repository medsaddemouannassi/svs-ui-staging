import { FormBuilder } from '@angular/forms';
import { FormParentComponent } from './form-parent.component';


describe('FormParentComponent', () => {
    let fixture: FormParentComponent;
    let formBuilderMock: FormBuilder;




    beforeEach(async () => {


        formBuilderMock = new FormBuilder();




        fixture = new FormParentComponent(
            formBuilderMock
        );


    });


    describe('Test FormParentComponent', () => {
        it('should Be Truthy', () => {
            expect(fixture).toBeTruthy();
        })
    });






  describe('Test get f', () => {
    it('should get controls of form', () => {
    expect(fixture.f).toEqual(fixture.parentForm.controls)

    });
  });


  describe('Test submit', () => {
    it('should set isSubmitted true', () => {
        fixture.submit();
    expect(fixture.isSubmitted).toEqual(true)

    });
  });
});
