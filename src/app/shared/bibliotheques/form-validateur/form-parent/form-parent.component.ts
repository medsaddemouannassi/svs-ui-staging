import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-form-parent',
  templateUrl: './form-parent.component.html',
  styleUrls: ['./form-parent.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormParentComponent {
  parentForm: FormGroup;
  isSubmitted = false;

  constructor(private formBuilder: FormBuilder) {
    this.parentForm = this.formBuilder.group({
      adresse: [],
      test: []

    });
  }


  get f() {
    return this.parentForm.controls;
  }
  submit() {
    this.isSubmitted = true;
  }


}

