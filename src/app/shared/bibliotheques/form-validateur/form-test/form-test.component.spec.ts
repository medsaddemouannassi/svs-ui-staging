import { FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs';
import { FormTestComponent } from './form-test.component';


describe('AddressFormComponent', () => {
    let fixture: FormTestComponent;
    let formBuilderMock: FormBuilder;
    let subscriptionMock: Subscription;




    beforeEach(async () => {
        subscriptionMock = new Subscription();


        formBuilderMock = new FormBuilder();




        fixture = new FormTestComponent(
            formBuilderMock
        );


    });


    describe('Test FormTestComponent', () => {
        it('should Be Truthy', () => {
            expect(fixture).toBeTruthy();
        })
    });

    describe('Test get value', () => {
        it('should return value', () => {
            expect(fixture.value).toEqual(fixture.form.value);
        })
    });

    describe('Test set value', () => {
        it('should set value', () => {
            let test={
                name:''
            }
            fixture.value=test;
            expect(fixture.form.value).toEqual(test);
        })
    });
    describe('Test ngOnDestroy', () => {

        it('should unsubscribe', () => {
            fixture.ngOnDestroy();
            const spyunsubscribe = jest.spyOn(subscriptionMock, 'unsubscribe');
            expect(spyunsubscribe).toBeDefined();
        });
    });

    describe('Test writeValue', () => {
        it('should  write value', () => {
            let value = ({
                name: ''
              });
          fixture.writeValue(value);
          expect(fixture.value).toEqual(value);



        });
        it('should not write value', () => {
          let value = ({
            name: ''
          });

          fixture.writeValue(value);
          value = null;
          fixture.writeValue(value);
          expect(fixture.value).not.toEqual(value);


        });
      });
      describe('Test registerOnChange', () => {
        it('should get register', () => {
          let test;
          fixture.registerOnChange(test);
          expect(fixture.onChange).not.toEqual(() => {
          //this is intentional
          });
          expect(fixture.onChange).toEqual(test);

        });
      });

      describe('Test onTouched', () => {
        it('should get touched', () => {
          let test;
          fixture.registerOnTouched(test);
          expect(fixture.onTouched).not.toEqual(() => {
           //this is intentional
           });
          expect(fixture.onTouched).toEqual(test);

        });
      });


  describe('Test validate', () => {
    it('should retrun null', () => {
      let form: any;
      expect(fixture.validate(form)).toEqual(null);

    });

    it('should not retrun null', () => {
      let form: any;
      fixture.form.controls['name'].setErrors({ 'incorrect': true });
      expect(fixture.validate(form)).toEqual({ "profile": { "valid": false } });

    });
  });

  describe('Test get f', () => {
    it('should get controls of form', () => {
    expect(fixture.f).toEqual(fixture.form.controls)

    });
  });
});
