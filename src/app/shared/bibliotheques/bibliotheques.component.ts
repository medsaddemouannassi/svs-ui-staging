import { Component, OnInit } from '@angular/core';
import { interval } from 'rxjs';
import { take } from 'rxjs/operators';
import { debug, RxJsLoggingLevel } from '../log/debug';


@Component({
  selector: 'app-bibliotheques',
  templateUrl: './bibliotheques.component.html',
  styleUrls: ['./bibliotheques.component.css']
})
export class BibliothequesComponent implements OnInit {
  selectedNumber: number = null;
  options = [
    { id: '1', label: 'LOUAULT' },
    { id: '2', label: 'MAX TRAILER' },
    { id: '3', label: 'METACO BFG' },
    { id: '4-', label: 'ORTHAUS' },
    { id: '6', label: 'PACTON TRAILERS' },
    { id: '82', label: 'ROYEN' },
    { id: '83', label: 'SAMRO' },
    { id: '84', label: 'SCHMITZ' },

  ];
  tabList = [
    { name:'Tab 1' },
    { name:'Tab 2' },
    { name:'Tab 3' }

  ];
  selectedTab=0;
  selectedTest: any;
  changeTab(i){
    this.selectedTab=i;

  }
  ngOnInit(): void {

    const numbers = interval(1000);
    const takeFourNumbers = numbers.pipe(take(4),
      debug(RxJsLoggingLevel.DEBUG, "pipe interval"));

    takeFourNumbers.subscribe();
  }

  changeInputNumber(selectedNumber){
    this.selectedNumber=selectedNumber;
  }
 

}
