export const PagesConstants = {

    /*** Page Setting Constants **/
    REQUEST: 'Request',
    /*** Page Setting Constants **/
    RECEPTION: 'Reception',
    /*** Page Setting Constants **/
    RESULT: 'Result',
    /*** Page Setting Constants **/
    CATALOG_ADD: 'CatalogAdd',
    /*** Page Setting Constants **/
    CATALOG: 'Catalog',
    /*** Page Setting Constants **/
    DESC: 'Desc',
    /*** Page Setting Constants **/
    SEARCH: 'Search',
    /*** Page Setting Constants **/
    RECEPTION_SIMULATION: 'ReceptionSimulation',

    RESULT_SIMULATION: 'ResultSimulation',

}
export const PagesPaths = {

    /*** Page Setting Constants **/
    REQUEST: '/request',
    /*** Page Setting Constants **/
    RECEPTION: '/reception',
    /*** Page Setting Constants **/
    SIMULATION: '/simulation',
    /*** Page Setting Constants **/
    RESULT: '/result',
    /*** Page Setting Constants **/
    CATALOG_ADD_ACTIVITY_SECTOR: '/catalog/',
    /*** Page Setting Constants **/
    CATALOG_ADD_NATURE: '/catalog/add',
    /*** Page Setting Constants **/
    CATALOG_ADD_BRAND: '/catalog/add',
    /*** Page Setting Constants **/
    CATALOG_ADD_TYPE: '/catalog/add',
    /*** Page Setting Constants **/
    CATALOG_ADD: '/catalog/add',
    /*** Page Setting Constants **/
    CATALOG: '/catalog',
    /*** Page Setting Constants **/
    DESC: '/desc',
    /*** Page Setting Constants **/
    SEARCH: '/search',
}