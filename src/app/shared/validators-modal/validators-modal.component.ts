import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SvsDescCollabModel } from '../models/svs-desc-collab.model';
import { SvsDescCollabService } from '../services/svs-desc-collab.service';
import { markFormGroupTouched } from '../utils';
import { StatusConstant } from '../status-constant';
import { ParamSettingModel } from '../models/param-setting.model';
import { ParamSettingService } from '../services/param-setting.service';
import { ValuationRequestWorkflowModel } from '../models/valuation-request-workflow.model';
import { WorkflowInstanceVariable } from '../models/workflow-instance-variable.model';
import { ValuationRequestWorkflowService } from '../services/valuation-request-workflow.service';
import { PopupService } from '../services/popup.service';
import { MdmInformationsModel } from '../models/mdm-informations.model';
import { ValuationModel } from '../models/valuation.model';
import { ConfirmationModalComponent } from '../confirmation-modal/confirmation-modal.component';
import { Router } from '@angular/router';
import { SharedCommunicationDataService } from '../services/shared-communication-data.service';

@Component({
    selector: 'app-validators-list-modal',
    templateUrl: './validators-modal.component.html',
    styleUrls: ['./validators-modal.component.css']
})
export class ValidatorsModalComponent implements OnInit {

    @Output() onConfirm = new EventEmitter<any>();
    @Output() onExit = new EventEmitter<boolean>();
    form: FormGroup;
    networkId: string;
    amount: number;
    valuation:ValuationModel;
    descInfo:MdmInformationsModel;
    valuationRequestWorkflow: ValuationRequestWorkflowModel;

    vldList: SvsDescCollabModel[] = [];
    statusList: ParamSettingModel[];

    constructor(private fb: FormBuilder, private svsDescCollabService: SvsDescCollabService,
         public activeModal: NgbActiveModal, private paramSettingService: ParamSettingService,
         private valuationRequestWorkflowService: ValuationRequestWorkflowService, private popupService: PopupService,
         public router: Router,    private dataSharing: SharedCommunicationDataService


    ) { }

    ngOnInit() {
        this.form = this.fb.group({
            validator: [null, Validators.required],
        });
        this.getAuthorizedCollabsForValoValidation(this.networkId, this.amount);
        this.paramSettingService.getParamByCode(StatusConstant.STATUS_VAL_WKFL).subscribe(res => {
            this.statusList = res;
        });
    }

    getAuthorizedCollabsForValoValidation(networkId: string, amount: number) {
        this.svsDescCollabService.getAuthorizedCollabsForValoValidation(networkId, amount).subscribe(res => {
            this.vldList = res;
        });
    }

    send() {
     

        this.form.updateValueAndValidity();
        markFormGroupTouched(this.form);
        if (this.form.valid) {

            let status = this.statusList.find(expectedStatus => expectedStatus.value == StatusConstant.WAIT_VALID_MAN_EXP_QUAL);
            this.saveValuationRequestWorkflow(status,this.form.value.validator.collaboratorInfo);
            this.confirmModal();
            this.activeModal.close();
        }
    }

    saveValuationRequestWorkflow(status: ParamSettingModel,  selectedDesc : MdmInformationsModel) {

        if (status) {
          let valuationRequestWorkflow = {} as ValuationRequestWorkflowModel;
          valuationRequestWorkflow.valuationRequest = this.valuation.valuationRequest;
          valuationRequestWorkflow.status = status;
          if (status.value === StatusConstant.WAIT_VALID_MAN_EXP_QUAL) {
   
              valuationRequestWorkflow.variable = {} as WorkflowInstanceVariable
    
              valuationRequestWorkflow.variable.expertDesc = this.descInfo;
              valuationRequestWorkflow.variable.administrator = selectedDesc;
    
              valuationRequestWorkflow.variable.urlRequest =location.href;
              valuationRequestWorkflow.variable.urlRefused = location.href;
              valuationRequestWorkflow.variable.urlResponse = location.href;
    
  
          }
          this.valuationRequestWorkflowService.saveValuationRequestWorkflow(valuationRequestWorkflow).subscribe(resp => {
            this.valuationRequestWorkflow = resp;
            this.dataSharing.changeValoStatus(status.label[0].toLowerCase() + status.label.substr(1));
          }, err => {
            console.error('error in ', err)
          }, () => {  
        
            if (this.valuationRequestWorkflow.status.value === StatusConstant.WAIT_VALID_MAN_EXP_QUAL) {
       
                let msg = 'Votre demande n°' + this.valuation.id + " a été adressée à l'expert "+ selectedDesc.fullName+".";
                this.popupService.popupInfo(msg, 'Fermer', null, null, ConfirmationModalComponent);
                const subscription = this.popupService.onConfirm.subscribe((t) => {
                  if (t) {
                    this.router.navigate(['/search'])
    
                  }
                  subscription.unsubscribe();
    
                });
 
    
            }
    
          });
        } else {
          console.error('error : cannot find required status');
        }
    
      }

    onChange: any = () => {
        // This is intentional
    };
    onTouched: any = () => {
        // This is intentional
    };

    registerOnChange(fn): void {
        this.onChange = fn;
    }

    writeValue(value): void {

        if (value === null) {
            this.form.reset();
        }
    }

    registerOnTouched(fn): void {
        this.onTouched = fn;
    }


    confirmModal() {
        this.onConfirm.emit(this.form.getRawValue())
    }
    exitModal() {
        this.onExit.emit(false);
    }
}
