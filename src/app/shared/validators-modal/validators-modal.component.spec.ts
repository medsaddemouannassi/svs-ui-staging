import { FormBuilder } from '@angular/forms';
import { of, throwError } from 'rxjs';
import { MdmInformationsModel } from '../models/mdm-informations.model';
import { ParamSettingModel } from '../models/param-setting.model';
import { ValuationRequestWorkflowModel } from '../models/valuation-request-workflow.model';
import { ValuationRequestModel } from '../models/valuation-request.model';
import { ValuationModel } from '../models/valuation.model';
import { ValidatorsModalComponent } from './validators-modal.component';

describe('DocumentsComponent', () => {
  let fixture: ValidatorsModalComponent;
  let formBuilderMock: FormBuilder;
  let svsDescCollabService: any;
  let activeModal: any;
  let paramSettingServiceMock: any;
  let valuationRequestWorkflowServiceMock: any;
  let popupServiceMock: any;
  let routerMock: any;
  let dataSharingMock: any;

  let paramSettingModel: ParamSettingModel[]=  [{
    'id': '5',
    'code': 'STATUS_VAL_WKFL',
    'label': 'Validée',
    'value': 'STATUS_RQST_WKFL_VAL',
    'effectiveDate': null,
    'isActiv': true,
  } as ParamSettingModel]

  beforeEach(async () => {
    formBuilderMock = new FormBuilder();

    svsDescCollabService = {
      getAuthorizedCollabsForValoValidation: jest.fn().mockReturnValue(of(true))
    };

    activeModal = {
      close : jest.fn().mockReturnValue('')
    }

    paramSettingServiceMock = {
      getParamByCode: jest.fn().mockReturnValue(of(paramSettingModel)),

    }

    let valuationRequestWorkflow = {
      status: {
        value: 'WAIT_VALID_MAN_EXP_QUAL',
        label:'en attente de validation',
      } as ParamSettingModel,
    } as ValuationRequestWorkflowModel;
    valuationRequestWorkflowServiceMock = {
      saveValuationRequestWorkflow: jest.fn().mockReturnValue(of(valuationRequestWorkflow)),
    }

    popupServiceMock = {
      popupInfo: jest.fn().mockReturnValue(of(true)),
      onConfirm: of(true)
    };

    routerMock = {
      navigate: jest.fn().mockReturnValue(new Promise(() => {
          //this is intentional
      }))
    }

    dataSharingMock = {
      changeStatus: jest.fn().mockReturnValue((true)),
      changeValoStatus: jest.fn().mockReturnValue(of(true)),

    };

    fixture = new ValidatorsModalComponent(
      formBuilderMock,
      svsDescCollabService,
      activeModal, paramSettingServiceMock, valuationRequestWorkflowServiceMock, popupServiceMock, routerMock, dataSharingMock
    );
    fixture.ngOnInit();
  });

  describe('Test Component', () => {
    it('should Be Truthy', () => {
      expect(fixture).toBeTruthy();
    });
  });

  describe('Test registerOnChange', () => {
    it('should change value', () => {
      let test;
      fixture.registerOnChange(test);
      expect(fixture.onChange).toEqual(test);
    });
  });

  describe('Test registerOnTouched', () => {
    it('should change value', () => {
      let test;
      fixture.registerOnTouched(test);
      expect(fixture.onTouched).toEqual(test);
    });
  });

  describe('Test writeValue', () => {
    it('should write value', () => {
      fixture.writeValue(null);
      expect(fixture.form.reset).toHaveBeenCalled;
    });
  });

  describe('Test send()', () => {
    it('should call updateValueAndValidity', () => {
      fixture.send();
      expect(fixture.form.updateValueAndValidity).toHaveBeenCalled;
    });

    it('should call confirmModal', () => {
      fixture.form.patchValue({validator:{fullName:'xx'}})
      fixture.send();

      expect(activeModal.close).toHaveBeenCalled();
    });
  });

  describe('Test exitModal', () => {
    it('should write value', () => {
      spyOn(fixture.onExit,"emit")
      fixture.exitModal();
      expect(fixture.onExit.emit).toHaveBeenCalled;
    });
  });

  describe('Test saveValuationRequestWorkflow', () => {
    it('should saveValuationRequestWorkflow have been called', () => {
      fixture.valuation = {
        id:'1',
        valuationRequest: {
          id:'2',
        } as ValuationRequestModel,
      } as ValuationModel;

      let status = {
        label:'en attente de validation',
        value: 'WAIT_VALID_MAN_EXP_QUAL'
      } as ParamSettingModel;

      let selectedDesc = {
        fullName:'Jean Jaque'
      } as MdmInformationsModel;

      fixture.saveValuationRequestWorkflow(status, selectedDesc);
      expect(valuationRequestWorkflowServiceMock.saveValuationRequestWorkflow).toHaveBeenCalled();
      expect(popupServiceMock.popupInfo).toHaveBeenCalled();

    });

  });
});
