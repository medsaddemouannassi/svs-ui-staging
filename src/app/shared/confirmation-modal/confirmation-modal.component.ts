import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-confirmation-modal',
  templateUrl: './confirmation-modal.component.html',
  styleUrls: ['./confirmation-modal.component.css']
})
export class ConfirmationModalComponent  {
  @Input() content;
  @Input() confirmBtnMessage;
  @Input() cancelBtnMessage;
  @Input() description;
  @Output() onConfirm = new EventEmitter<boolean>();
  @Output() onExit = new EventEmitter<boolean>();  
  @Input() showCloseBtn = true;
  @Input() newLineMessage;

  constructor(public activeModal: NgbActiveModal) { }



  confirmModal(){
  this.onConfirm.emit(true);

  }
  exitModal(){
    this.onExit.emit(false);
    this.onConfirm.emit(false);
  }
}
