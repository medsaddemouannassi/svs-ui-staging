import {ConfirmationModalComponent} from './confirmation-modal.component';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

describe('ConfirmationModalComponent', () => {
  let fixture: ConfirmationModalComponent;
  let ngbActiveModalMock: NgbActiveModal;

  beforeEach(() => {
    ngbActiveModalMock = new NgbActiveModal();
    fixture = new ConfirmationModalComponent(ngbActiveModalMock);
  });

  it('should create', () => {
    expect(fixture).toBeTruthy();
  });
  	
  it('should confirmModal', () => {
    fixture.confirmModal();
    expect(fixture.onConfirm).toHaveBeenCalled;
  });
  
  it('should exitModal', () => {
    fixture.exitModal();
    expect(fixture.onConfirm).toHaveBeenCalled;
    expect(fixture.onExit).toHaveBeenCalled;
  });

});
