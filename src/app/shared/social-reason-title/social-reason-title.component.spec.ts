import { of, Subscription } from "rxjs";
import { SocialReasonTitleComponent } from "./social-reason-title.component";

describe('SocialReasonTitleComponent', () => {
    let fixture: SocialReasonTitleComponent;
    let entrepriseInfoServiceMock: any;
    let SubscriptionMock: Subscription;

    beforeEach(() => {
        SubscriptionMock = new Subscription();

        entrepriseInfoServiceMock = {
            socialReasonChanged: of('socialReason')
        };

        fixture = new SocialReasonTitleComponent(
            entrepriseInfoServiceMock

        );

        fixture.ngOnInit();
    });

    describe('Test: ngOnInit', () => {
        it('should get socialReasonChanged', () => {

            expect(fixture.socialReason).toEqual('socialReason');
            expect(fixture.socialReasonFirstTwoLetters).toEqual('so');
        });
    });


    describe('Test: ngOnDestroy', () => {
        it('should unsubscribe', () => {
            expect(fixture.ngOnDestroy());
            const spyunsubscribe = jest.spyOn(SubscriptionMock, 'unsubscribe');
			expect(spyunsubscribe).toBeDefined();
        });
    });
});
