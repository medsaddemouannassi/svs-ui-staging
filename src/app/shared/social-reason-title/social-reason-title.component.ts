import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { EntrepriseInfoService } from '../services/entreprise-info.service';

@Component({
  selector: 'app-social-reason-title',
  templateUrl: './social-reason-title.component.html',
  styleUrls: ['./social-reason-title.component.css']
})
export class SocialReasonTitleComponent implements OnInit, OnDestroy {

  @Input() title: string;
  socialReasonFirstTwoLetters: string;
  socialReason: string;
  subscription: Subscription;
  constructor(private entrepriseInfoService: EntrepriseInfoService) { }


  ngOnInit(): void {
    this.subscription = this.entrepriseInfoService.socialReasonChanged
      .subscribe(socialReason => {
        this.socialReason = socialReason;
        this.socialReasonFirstTwoLetters = socialReason.substring(0,2);
      });
  }

  ngOnDestroy(): void {
    if(this.subscription){
      this.subscription.unsubscribe();

    }
  }

}
