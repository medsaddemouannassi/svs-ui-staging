import { AbstractModel } from './abstract.model';

export interface DiscountAverageResultModel extends AbstractModel{

    discountRateYear: number;
    discountRateValue: string;
    
}