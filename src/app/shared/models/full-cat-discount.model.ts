import { CatDiscountModel } from './cat-discount.model';
import { CatDiscountRateModel } from './cat-discount-rate.model';



export interface FullCatDiscountModel {
	
    catDiscount :CatDiscountModel,
    catDiscountRateList: CatDiscountRateModel[];

}
