import { AbstractModel } from './abstract.model';

export interface CatDiscountAverageProcessElementModel extends AbstractModel {
    id: string;
    discountAverageValue: number;
    startDateLabel: string;
    endDateLabel: string;
    catActivitySectorId: string;
    catNatureId: string;
    catBrandId: string;
    isAnewAssets: boolean;
    minumumValuationToCount: number;

}
