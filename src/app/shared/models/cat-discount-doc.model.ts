import { CatDiscountModel } from './cat-discount.model';

export interface CatDiscountDocModel {
    id:string;
    catDiscount:CatDiscountModel;


	 docFami:string;

	 docType:string;

	 docSubType:string;

	 docPhse:string;

	 gestObject:string;

	 docName:string;

	 docVers:string;
}