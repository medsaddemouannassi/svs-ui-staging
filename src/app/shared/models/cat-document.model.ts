import { AbstractModel } from './abstract.model';
import { CatActivitySectorModel } from './cat-activity-sector.model';
import { CatNatureModel } from './cat-nature.model';
import { CatBrandModel } from './cat-brand.model';
import { CatAssetTypeModel } from './cat-asset-type.model';

export interface CatDocumentModel extends AbstractModel {

    id: string;
    catActivitySector: CatActivitySectorModel;
    catNature: CatNatureModel;
    catBrand: CatBrandModel;
    catAssetType: CatAssetTypeModel;
    discriminator: string;
    docName: string;
    docVersion: number;
    gedPath: string;
}