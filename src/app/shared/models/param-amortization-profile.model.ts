import { ParamProductFamilyModel } from './param-product-family.model';

export interface ParamAmortizationProfileModel {

id : string,
label : string,
paramProductFamily :ParamProductFamilyModel
}

