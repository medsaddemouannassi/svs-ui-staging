export interface ParamSettingModel {

id : string;
code : string;
label : string;
value : string;
effectiveDate : Date;
isActiv : boolean;
}

