import { AbstractModel } from './abstract.model';
import { DiscountAverageResultModel } from './cat-discout-average-result.model';

export interface CatDiscountAverageModel extends AbstractModel {
    valuationsNumber: number;
    discountAvgs : DiscountAverageResultModel[];
}
