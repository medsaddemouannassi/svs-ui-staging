import { AbstractModel } from './abstract.model';

 

export interface CatActivitySectorModel extends AbstractModel{
    id?:string;
    label?:string;
    comment?:string;
    isActive?:boolean;

}
