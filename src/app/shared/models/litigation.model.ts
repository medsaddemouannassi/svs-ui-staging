import { CatActivitySectorModel } from './cat-activity-sector.model';
import { CatAssetTypeModel } from './cat-asset-type.model';
import { CatBrandModel } from './cat-brand.model';
import { CatNatureModel } from './cat-nature.model';
import { LitigationDetail } from './litigation-detail.model';
import { ValuationModel } from './valuation.model';

export interface LitigationModel {

    id: string,

    contractKSIOP: string,

    fixedAsset: string,

    plateNumber: string,

    invoiceDate: Date,

    invoiceDateLabel:string,

    isLostMaterial: boolean,

    materialAge: number,

    materialCurrentValue: number,

    litigationDiscount: number,

    litigationValue: number,

    isCobailing: boolean,

    shareRate: number,

    shareValue: number,

    comment: string,

    catBrand: CatBrandModel,

    catActivitySector: CatActivitySectorModel

    catAssetType: CatAssetTypeModel,

    catNature: CatNatureModel,

    valuation: ValuationModel,

    litigationDetails: LitigationDetail[]
}
