import { CatBrandModel } from './cat-brand.model';

export interface CatBrandDocModel {
    id:string;
    catBrand:CatBrandModel;


	 docFami:string;

	 docType:string;

	 docSubType:string;

	 docPhse:string;

	 gestObject:string;

	 docName:string;

	 docVers:string;
}