export interface MdmInformationsModel  { 
    mat:string;
    fullName: string;
    reseau:string;
    delegation:string;
    idDelegation: string;
    idReseau: string;
    direction:string;
    idDirection:string;
    idFullname: string;
    mail:string;
}
