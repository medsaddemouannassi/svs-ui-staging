export interface AbstractNavigationModel {
    label:string,
    data:string,
    expandedIcon: string,
    collapsedIcon:string,
    nodeType: string,

  } 