import { CatAssetTypeModel } from './cat-asset-type.model';

export interface CatAssetTypeDocModel {
    id:string;
    catAssetType:CatAssetTypeModel;


	 docFami:string;

	 docType:string;

	 docSubType:string;

	 docPhse:string;

	 gestObject:string;

	 docName:string;

	 docVers:number;
}