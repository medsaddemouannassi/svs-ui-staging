import { AbstractModel } from './abstract.model';
import { ParamEnergyModel } from './param-energy.model';
import { ParamUsageUnitModel } from './param-usage-unit.model';

export interface ValuationRequestModel extends AbstractModel{
  id: string,
  externalSystemRef:string,
  underwriterId:string,
  isRetUnderwriter:boolean,
  customerPropoId:string,
  bssResponsible:string,
  requestComment:string,
  financingAmount: number,
  assetCreationDate:Date,
  assetUsVolume:number,
  collAssetEstimYear:number,
  co2Emission:number,
  fiscPower:number,
  isAssetUseAbroad:boolean,
  isANewAsset:boolean,
  isLicencePlateAvaileble:boolean,
  paramEnergy: ParamEnergyModel,
  paramUsageUnit: ParamUsageUnitModel,
  unitaryQuantity: number,
  bssResponsibleFullName: string,
  networkID ? :string;
  workflowInstanceId:string;
  workflowBuisinessKey:string;
  isSimulated: boolean,

}
