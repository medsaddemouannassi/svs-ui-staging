import { AbstractModel } from './abstract.model';

export interface StatusCatalogAddRequest extends AbstractModel{
    id: string,
    label: string
}