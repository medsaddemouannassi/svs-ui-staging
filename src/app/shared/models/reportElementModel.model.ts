import { EntrepriseInfoModel } from './entreprise-info.model';
import { FinancialPlanModel } from './financial-plan.model';
import { ValuationModel } from './valuation.model';
import { ValuationDiscountModel } from './valuationDiscount.model';

export interface ReportElementModel  {
    entrepriseInfo:EntrepriseInfoModel;
	valuation:ValuationModel;
	financialPlans: FinancialPlanModel[];
    discounts: ValuationDiscountModel[];
    valuationDate:Date;
    descName:string;
    statutCotation:string;
}