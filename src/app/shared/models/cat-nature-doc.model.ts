

export interface CatNatureDocModel {
    id: string;

    catNature: CatNatureDocModel;

    docFami: string;

    docType: string;

    docSubType: string;

    docPhse: string;

    gestObject: string;

    docName: string;

    docVers: string;


}
