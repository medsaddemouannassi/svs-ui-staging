 import { ValuationModel } from './valuation.model';
import { CatDiscountModel } from './cat-discount.model';

 export interface ValuationDiscountModel  {
     id : string;
     discountRateYear : number;
     discountRateValue : number;
     valuation : ValuationModel;
     catDiscount:CatDiscountModel;
 }
