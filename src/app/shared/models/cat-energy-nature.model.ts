import { AbstractModel } from './abstract.model';
import { CatBrandModel } from './cat-brand.model';
import { CatEnergyNatureWeightingModel } from './cat-energy-nature-weighting.model';
import { CatNatureModel } from './cat-nature.model';
import { ParamEnergyModel } from './param-energy.model';

export interface CatEnergyNatureModel extends AbstractModel {
  
    id: string;

    paramEnergy: ParamEnergyModel;

    catNature: CatNatureModel;

    catBrand: CatBrandModel;

    activationDate: Date;

    endDate: Date;

    catEnergyNatureWeightingList: CatEnergyNatureWeightingModel[];

    isTouched: boolean;

    isActive: boolean;
}
