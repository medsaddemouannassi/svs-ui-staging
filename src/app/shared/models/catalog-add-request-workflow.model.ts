import { AbstractModel } from './abstract.model';
import { CatRequestModel } from './cat-request.model';
import { ParamSettingModel } from './param-setting.model';
import { WorkflowInstanceVariable } from './workflow-instance-variable.model';

export interface CatRequestWorkFlow extends AbstractModel {


    id: string;

    catRequest: CatRequestModel,

    updateDate: Date,

    status: ParamSettingModel;

    comment: string;

    variable:WorkflowInstanceVariable;

    isSimulated:Boolean;




}