export interface SearchResultModel {
    id: string,
    type:string,
    client:string,
    updateDt:Date,
    status:string,
    requestId:string,
    wallet:string, 
    siren:string,
    dc:string,
    sdc:string,
    ca:string,
    agence:string,
    desc:string,
    network:string,
    activitySetco:string,
    nature:string,
    brand:string,
    assetType:string,
    energy:string,
    isNew:boolean

}