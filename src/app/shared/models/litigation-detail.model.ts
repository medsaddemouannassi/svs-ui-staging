import { LitigationModel } from './litigation.model';

export interface LitigationDetail {

    id: string,

    litigation: LitigationModel,

    discountRateYear: number,

    discountRateValue: number,

    isChecked:boolean

}