import { ValuationSearchModel } from './valuation-search.model';

export interface PageValuationSearch {

    content : ValuationSearchModel[],
    empty : boolean,
    numberOfElements : number,
    number:number,
    totalElements: number,
    }