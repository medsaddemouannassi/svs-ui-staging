export interface EntrepriseInfoModel {
    id: string;
    subsidiary: boolean;
    cp: string;
    description: string;
    siren: string;
    sirenType: string;
    socialReason: string;
    delegation: string;
    regionalDirection: string;
    network: string;
    isFromRet: boolean;
    isPmFromRet: boolean;
    idRet : string;
    mainUnderwId:string;
    mainUnderwName:string;

}
