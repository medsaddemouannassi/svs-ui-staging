export interface ValuationSearchModel {
    valuationSearchId: string;

    valuationRequestId: string;

    valuationRequestLabelSort: number;

    valuationId: string;

    valuationLabelSort: number;

    createdDate: Date;

    updatedDate: Date;

    status: string;

    customerId: string;

    customerName: string;

    walletId: string;

    walletLabel: string;

    numSiren: string;

    elementId: string;

    elementType: string;

    numDc: string;

    numSdc: string;

    caId: string;

    caFullName: string;

    agencyName: string;

    agencyId: string;

    descId: string;

    descFullName: string;

    networkId: string;

    networkName: string;

    sectorId: string;

    sectorLabel: string;

    natureId: string;

    natureLabel: string;

    brandId: string;

    brandLabel: string;


    brandLabelFiltre: string;

    typeId: string;

    typeLabel: string;

    typeLabelFiltre : string

    energyId: string;

    energyLabel: string;

    state: string;
    
    stateLabel: string;

    startDate: string;

    endDate: string;

    updatedDateLabel: string;

    elementLabel: string;

    numSirenLabel: string;

    numDcLabel: string;

    numSdcLabel: string;

    hasDocument : boolean;

    contrKSIOP: string;

    contrKSIOPLabel: string;

    isLitigation: boolean;

    isFromDuplicated : boolean;
}
