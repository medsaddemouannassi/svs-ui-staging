import { FinancialPlanModel } from './financial-plan.model';
import { AbstractModel } from './abstract.model';

export interface PlanEstimatedOutModel extends AbstractModel{
    id:string,
    estimatedOutYear:number,
    estimatedOutPrincRate:number,
    estimatedOutPrincVal: number,
    estimatedCollAssetVal:number,
    potentialLoss:number,
    financialPlan: FinancialPlanModel

}