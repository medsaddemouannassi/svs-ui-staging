import { AbstractModel } from './abstract.model';
import { CatActivitySectorModel } from './cat-activity-sector.model';
import { CatBrandModel } from './cat-brand.model';
import { CatNatureModel } from './cat-nature.model';
import { ParamSettingModel } from './param-setting.model';
import { ValuationRequestModel } from './valuation-request.model';

export interface CatRequestModel extends AbstractModel {

    id:string,
    catActivitySector:CatActivitySectorModel,
    catNature : CatNatureModel,
    catBrand : CatBrandModel,
    requestType:ParamSettingModel,
    workflowCode : string,
    buisinessKey : string,
    activeDate :Date,
    label:string,
    comment:string,
    valuationRequest:ValuationRequestModel
}