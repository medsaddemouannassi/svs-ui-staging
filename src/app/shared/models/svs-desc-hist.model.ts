
export interface SvsDescHistoryModel {
    id: string,
    descCollabId:string,

    collaboratorMatricul: string,
    descId: string,
    maxAmount: number,
    isManager: boolean,
    operationCode: string,
    operationDate:Date

}
