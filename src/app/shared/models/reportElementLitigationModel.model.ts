import { EntrepriseInfoModel } from './entreprise-info.model';
import { FinancialPlanModel } from './financial-plan.model';
import { ValuationModel } from './valuation.model';
import { ValuationDiscountModel } from './valuationDiscount.model';
import { LitigationModel } from './litigation.model';

export interface ReportElementLitigationModel  {
    entrepriseInfo:EntrepriseInfoModel;
	valuation:ValuationModel;
    valuationDate:Date;
    descName:string;
    litigation: LitigationModel;

}