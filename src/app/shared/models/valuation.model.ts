
import { CatNatureModel } from './cat-nature.model';
import { CatBrandModel } from './cat-brand.model';
import { CatAssetTypeModel } from './cat-asset-type.model';
import { ValuationRequestModel } from './valuation-request.model';
import { CatActivitySectorModel } from './cat-activity-sector.model';
import { ParamValidationTypeModel } from './param-validation-type.model';
import { AbstractModel } from './abstract.model';

export interface ValuationModel  extends AbstractModel{
    id:string,
    discountRateComment: string,
    experId: string,
    controllerId: string,
    validatorId: string,
    catNature: CatNatureModel,
    paramValidationType: ParamValidationTypeModel,
    catBrand: CatBrandModel,
    catAssetType: CatAssetTypeModel,
    valuationRequest: ValuationRequestModel,
    catActivitySector: CatActivitySectorModel,
    descNetwork:string,
    descName:string,
    isSubmitted:boolean,
    isDuplicated:boolean
  
}