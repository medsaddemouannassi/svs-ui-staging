

import { AbstractNavigationModel } from './abstract-navigation.model';
import { CatNatureNavigationModel } from './cat-nature-navigation.model';

export interface CatActivitySectorNavigationModel extends AbstractNavigationModel{
    nodeType: string,

    children: CatNatureNavigationModel[]
}




