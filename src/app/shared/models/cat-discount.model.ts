import { AbstractModel } from './abstract.model';
import { CatActivitySectorModel } from './cat-activity-sector.model';
import { CatAssetTypeModel } from './cat-asset-type.model';
import { CatBrandModel } from './cat-brand.model';
import { CatNatureModel } from './cat-nature.model';


export interface CatDiscountModel  extends AbstractModel{

	  id: string;

      catBrand:CatBrandModel;

	  catNature:CatNatureModel;

	  catActivitySector:CatActivitySectorModel;

	  catAssetType:CatAssetTypeModel;

	  activationDate:Date;

	  endDate:Date;

	  enableValuation:boolean;

	  enableRecov:boolean;

	  isPerm:boolean;

	  ponderationRate:number;

	  maxFinPer:number;

	  minFinPer:number;

	  discount:number;

	  creation: string;

	  lastValuationDone:number;

	  isReadjust: boolean;

	  comment: string;

	  isNew:boolean;

	startDateAverLabel: string;

	endDateAverLabel: string;

	nbMinValAver: number;

}
