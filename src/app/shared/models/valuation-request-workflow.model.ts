import { AbstractModel } from './abstract.model';
import { ParamSettingModel } from './param-setting.model';
import { ValuationRequestModel } from './valuation-request.model';
import { WorkflowInstanceVariable } from './workflow-instance-variable.model';

export interface ValuationRequestWorkflowModel extends AbstractModel {
    
    id: string;

    valuationRequest: ValuationRequestModel;

    status: ParamSettingModel;

    comment: string;

    variable:WorkflowInstanceVariable;


}
