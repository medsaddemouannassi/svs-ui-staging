import { ParamAmortizationProfileModel } from './param-amortization-profile.model';

export interface ParamProductFamilyModel {
    id : string;
    label : string;
    paramAmortizationProfiles : ParamAmortizationProfileModel[],
    code: string
}
