
 

export interface RefUnderwriterModel{
    id:string;
    legalName:string;
    nsiren:string;
    siret:string;
    isRetUnderwriter:boolean;
    mainUnderwId:string;
    mainUnderwName:string;

}
