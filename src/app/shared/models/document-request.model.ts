export interface DocumentRequest {

    catActivitySectorId: string;
    catNatureId: string;
    catBrandId: string;
    catAssetTypeId: string;
    discriminator: string;
    pathSource: string;
    docName: string;

}
