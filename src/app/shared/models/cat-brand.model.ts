 
import { AbstractModel } from './abstract.model';
import { CatNatureModel } from './cat-nature.model';
import { CatRequestModel } from './cat-request.model';

export interface CatBrandModel   extends AbstractModel{
    id?:string;
    label?:string;
    catNature?:CatNatureModel;
    comment?:string;
    catRequest:CatRequestModel;
    isActive?:boolean;
}
