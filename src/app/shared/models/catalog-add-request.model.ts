import { AbstractModel } from './abstract.model';

export interface CatRequestWaiting extends AbstractModel {

    id: string;

    catActivitySector: string,

    catNature: string,

    catBrand: string,

    idSector: string;

    idNature: string,
 
    idBrand: string,

    requestType: string,

    workflowCode: string,

    activeDate: Date,

    label: string,

    comment: string,
    
    workflowStatus: string,

    workflowStatusVal: string

}