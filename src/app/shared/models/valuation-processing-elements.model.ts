import { ValuationDiscountModel } from './valuationDiscount.model';
import { FinancialPlanModel } from './financial-plan.model';
import { ValuationRequestModel } from './valuation-request.model';

export interface ValuationProcessingElementsModel {

    discounts:ValuationDiscountModel[];
    plans: FinancialPlanModel[] ;
    valuationRequest:ValuationRequestModel ;



}




