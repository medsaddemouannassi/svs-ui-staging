
import { CatActivitySectorModel } from './cat-activity-sector.model';
import { ParamNapModel } from './param-nap.model';
import { AbstractModel } from './abstract.model';

export interface CatNatureModel extends AbstractModel {
    id:string,
    label:string,
    catActivitySector:CatActivitySectorModel;
    comment: string;
    paramNap:ParamNapModel;
    isActive:boolean;
    isValid: boolean;
    isEnrg: boolean;

}
