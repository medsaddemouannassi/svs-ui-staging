import { MdmInformationsModel } from './mdm-informations.model';

export interface WorkflowInstanceVariable{

    bssResponsible : MdmInformationsModel;
    expertDesc : MdmInformationsModel;
    administrator : MdmInformationsModel;

    companyName: string;
    urlRequest: string;
    urlResponse: string;
    urlRefused: string;


}