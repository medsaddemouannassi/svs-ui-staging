export interface AbstractModel {
    updatedDate?: Date;
    createdDate?: Date;
    createdBy?:string;
    updatedBy?: string;
  } 