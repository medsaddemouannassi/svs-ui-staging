
import { AbstractModel } from './abstract.model';
import { CatDiscountModel } from './cat-discount.model';

export interface CatDiscountRateModel  extends AbstractModel{
    id:string,
    catDiscount:CatDiscountModel,
    discountRateYear:number,
    discountRateValue:number
}
