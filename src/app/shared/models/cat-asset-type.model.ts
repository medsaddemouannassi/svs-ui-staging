 
import { AbstractModel } from './abstract.model';
import { CatBrandModel } from './cat-brand.model';
import { CatRequestModel } from './cat-request.model';

export interface CatAssetTypeModel extends AbstractModel{
    id:string,
    label:string,
    catBrand: CatBrandModel,
    comment:string,
    isActive:boolean,
    catRequest : CatRequestModel

}
