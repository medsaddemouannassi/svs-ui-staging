 

import { AbstractNavigationModel } from './abstract-navigation.model';
import { CatAssetTypeNavigationModel } from './cat-asset-type-navigation.model';

export interface CatBrandNavigationModel extends AbstractNavigationModel{
    
    children: CatAssetTypeNavigationModel[],
    leaf: boolean,
    nodeType: string

}




