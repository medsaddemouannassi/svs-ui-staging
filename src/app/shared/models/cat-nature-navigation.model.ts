

import { AbstractNavigationModel } from './abstract-navigation.model';
import { CatBrandNavigationModel } from './cat-brand-navigation.model';

export interface CatNatureNavigationModel extends AbstractNavigationModel{

    children: CatBrandNavigationModel[],
    leaf: boolean,
    nodeType: string

}




