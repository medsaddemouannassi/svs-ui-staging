import { MdmInformationsModel } from './mdm-informations.model';
import { SvsDescModel } from './svs-desc.model';

export interface SvsDescCollabModel {
    id: string,
    collaboratorMatricul: string,
    collaboratorFullName:string,
    collaboratorInfo:MdmInformationsModel,
    desc: SvsDescModel,
    maxAmount: number,
    isManager: boolean,

}
