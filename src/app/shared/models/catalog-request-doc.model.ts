import { AbstractModel } from './abstract.model';
import { CatRequestWaiting } from './catalog-add-request.model';

export interface CatRequestDoc extends AbstractModel {

    id: string;
    catRequest: CatRequestWaiting;

    discriminator: string;
    docName: string;
    docVersion: number;
    gedPath: string;

}
