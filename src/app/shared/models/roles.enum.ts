export enum Role {
    CA = 'ROLE_SVS_CA',
    ADMIN_SE = 'ROLE_SVS_ADMIN_SE',
    PASS_ADMINISTRATEUR_OSEO = 'ROLE_PASS_ADMINISTRATEUR_OSEO',
    CONSULTATION = 'ROLE_SVS_CONSULTATION',
    SVS_CONTENTIEUX = 'ROLE_SVS_CONTENTIEUX',
    SVS_EXPERT_SE = 'ROLE_SVS_EXPERT_SE'
}
export interface RoleName {
    RoleCode: string;
    RoleName: string;
}

export const RoleNames: RoleName[] = [
    { RoleCode: 'ROLE_SVS_CA', RoleName: "Chargé d'Affaires" },
    { RoleCode: 'ROLE_SVS_ADMIN_SE', RoleName: 'Filière SE' },
    { RoleCode: 'ROLE_SVS_CONTENTIEUX', RoleName: 'Contentieux' },
    { RoleCode: 'ROLE_SVS_EXPERT_SE', RoleName: 'Expert SE' },
    { RoleCode: 'ROLE_PASS_ADMINISTRATEUR_OSEO', RoleName: 'ROLE_PASS_ADMINISTRATEUR_OSEO' },
    { RoleCode: 'ROLE_SVS_CONSULTATION', RoleName: 'ROLE_SVS_CONSULTATION' },
]

