export interface OAuth2Token {
    tokenValue: string;
    roles: string[];
}
