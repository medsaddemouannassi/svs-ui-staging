export interface ValuationSearchCatalogueModel {

	valuationId: string;

	valuationLabelSort: number;

	valuationRequestId :string;

	sectorId: string;

	sectorLabelSearch: string;

	sectorLabel: string;

	natureId: string;

	natureLabelSearch: string;

	natureLabel: string;

	brandId: string;

	brandLabelSearch: string;

	brandLabel: string;

	typeId: string;

	typeLabelSearch: string;

	typeLabel: string;

	updatedDateValuation: Date;

	updatedDateValuationLabel: string;

	updatedDateType: Date;

	updatedDateTypeLabel: string;

	hasDocument: boolean;

	status:string;

	isFromDuplicated:boolean
}
