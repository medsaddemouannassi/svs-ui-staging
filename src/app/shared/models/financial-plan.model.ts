import { AbstractModel } from './abstract.model';
import { ParamAmortizationProfileModel } from './param-amortization-profile.model';
import { ParamProductFamilyModel } from './param-product-family.model';
import { PlanEstimatedOutModel } from './plan-estimated-out.model';
import { ValuationRequestModel } from './valuation-request.model';
import { ValuationModel } from './valuation.model';

export interface FinancialPlanModel extends AbstractModel{
    id:string,
    planDuration: number,
    planContrbAmount: number;
    collateralAssetRating: number,
    status: string,
    valuation: ValuationModel,
    valuationRequest: ValuationRequestModel,
    paramAmortizationProfile: ParamAmortizationProfileModel,
    remainingAmount: number;
    remainingPercent: number;
    planEstimatedOut: PlanEstimatedOutModel[];
    paramProductFamily: ParamProductFamilyModel ;
    isDuplicated :boolean;
}
