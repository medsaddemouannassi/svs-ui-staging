import { ValuationSearchCatalogueModel } from './valuation-search-catalogue.model';
import { SearchCatalogueDataModel, SearchCatalogueResultModel } from './catalogue-search-data.model';

export interface PageValuationSearchCatalogue {

    //content : ValuationSearchCatalogueModel[],
    content : SearchCatalogueResultModel[],
    empty : boolean,
    numberOfElements : number,
    number:number,
    totalElements: number,
    }