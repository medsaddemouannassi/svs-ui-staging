export interface SearchCatalogueDataModel {
	  name :string;
	  natureId :string;
	  brandId :string;
	  disocuntRateYearOne:string;
	  disocuntRateYeartwo :string;
	  disocuntRateYearThree:string;
	  disocuntRateYearFour:string;
	  disocuntRateYearFive:string;
	  commentaire:string;
	  type:string;
	  ponderationRate:string;
	  energyNature;
	 

}
export interface SearchCatalogueResultModel {
	data:SearchCatalogueDataModel;
	children :SearchCatalogueDataModel [] ;
}