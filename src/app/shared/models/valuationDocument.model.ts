import { ValuationRequestModel } from './valuation-request.model';
import { ParamSettingModel } from './param-setting.model';

export interface ValuationDocumentModel  {
    id : string;
    docRequest : string;
    docType : string;
    valuationRequest:ValuationRequestModel;
    docFamily: string;
    docSubType: string;
    docPhase: string;
    gestObject:string;
    docName:string;
    docVersion:string;
    isQualified:boolean;
    paramSetting: ParamSettingModel;
}