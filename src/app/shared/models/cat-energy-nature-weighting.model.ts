import { CatEnergyNatureModel } from './cat-energy-nature.model';

export interface CatEnergyNatureWeightingModel {
    
  id: string;

  catEnergyNature: CatEnergyNatureModel;

  weightingRateYear: number;

  weightingRateValue: number;
}
