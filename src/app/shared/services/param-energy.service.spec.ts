import { of } from 'rxjs';

import { environment } from 'src/environments/environment';
import { ParamEnergyService } from './param-energy.servie';
import { ParamEnergyModel } from '../models/param-energy.model';

describe('Service: ParamEnergyService', () => {
    let service: ParamEnergyService;
    const http = jest.fn();
    let baseUrl = environment.baseUrl;

    describe('Test: getAllParamEnergy', () => {
        it('should return  object', done => {


            const response : ParamEnergyModel[]=[];

            const httpMock = {
                get: jest.fn().mockReturnValue(of(response))
            };
            const serviceMock = new ParamEnergyService(httpMock as any);
            serviceMock.getAllParamEnergy().subscribe((data) => {
                expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/param-energy`);
                expect(httpMock.get).toBeDefined();
                expect(httpMock.get).toHaveBeenCalled();
                expect(data).toEqual(response);
                done();
            });
        });
    });

    describe('Test: getApplicableParamEnergyList', () => {
        it('should return  object', done => {


            const response : ParamEnergyModel[]=[];

            const httpMock = {
                get: jest.fn().mockReturnValue(of(response))
            };
            const serviceMock = new ParamEnergyService(httpMock as any);
            serviceMock.getApplicableParamEnergyList().subscribe((data) => {
                expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/param-energy/applicable`);
                expect(httpMock.get).toBeDefined();
                expect(httpMock.get).toHaveBeenCalled();
                expect(data).toEqual(response);
                done();
            });
        });
    });

    describe('Test: getNatureUnusedParamEnergy', () => {
        it('should return  object', done => {

            const response : ParamEnergyModel[]=[];
            const httpMock = {
                get: jest.fn().mockReturnValue(of(response))
            };
            const serviceMock = new ParamEnergyService(httpMock as any);
            serviceMock.getNatureUnusedParamEnergy('1').subscribe((data) => {
                expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/param-energy/nature-unused/nature/1`);
                expect(httpMock.get).toBeDefined();
                expect(httpMock.get).toHaveBeenCalled();
                expect(data).toEqual(response);
                done();
            });
        });


    });

    describe('Test: getBrandUnusedParamEnergy', () => {
        it('should return  object', done => {

            const response : ParamEnergyModel[]=[];
            const httpMock = {
                get: jest.fn().mockReturnValue(of(response))
            };
            const serviceMock = new ParamEnergyService(httpMock as any);
            serviceMock.getBrandUnusedParamEnergy('1','1').subscribe((data) => {
                expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/param-energy/brand-unused/nature/1/brand/1`);
                expect(httpMock.get).toBeDefined();
                expect(httpMock.get).toHaveBeenCalled();
                expect(data).toEqual(response);
                done();
            });
        });


    });

   
});

