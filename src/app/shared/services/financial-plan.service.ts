import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { FinancialPlanModel } from '../models/financial-plan.model';
import { PlanEstimatedOutModel } from '../models/plan-estimated-out.model';
@Injectable({
  providedIn: 'root'
})
export class FinancialPlanService {

  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }

  loadFinancialPlanByValuationRequestId(valuationRequestId: string) {
    return this.http.get<FinancialPlanModel[]>(
      `${this.baseUrl}/rest/v1/financial-plan/valuation/request/${valuationRequestId}`
    );
  }

  loadFinancialPlanByValuationId(valuationId: string) {
    return this.http.get<FinancialPlanModel[]>(
      `${this.baseUrl}/rest/v1/financial-plan/valuation/${valuationId}`
    );
  }

  saveFinancialPlan(financialPlanList: FinancialPlanModel[]) {
    return this.http.post<FinancialPlanModel[]>(
      `${this.baseUrl}/rest/v1/financial-plan/submit-financial-plan`, financialPlanList
    );

  }
  removeFinancialPlan(removedFinancialPlanList: FinancialPlanModel[]) {
    return this.http.post(
      `${this.baseUrl}/rest/v1/financial-plan/remove-financial-plan`, removedFinancialPlanList
    );
  }

  removePlanEstimatedOut(removedPlanEstimatedOutList: PlanEstimatedOutModel[]) {
    return this.http.post(
      `${this.baseUrl}/rest/v1/financial-plan/remove-plan-estimated-out`, removedPlanEstimatedOutList
    );
  }

  planEstimatedOutRateWithFinancialProcessing(planDuration: number, remainingPercent: number): Observable<PlanEstimatedOutModel[]> {
    return this.http.get<PlanEstimatedOutModel[]>(
      `${this.baseUrl}/rest/v1/financial-plan/plan-estimated-out/rate/financial-processing/${planDuration}/${remainingPercent}`
    );
  }

  planEstimatedOutRatePreCalculated(planDuration: number, amortizationProfileId: string): Observable<PlanEstimatedOutModel[]> {
    return this.http.get<PlanEstimatedOutModel[]>(
      `${this.baseUrl}/rest/v1/financial-plan/plan-estimated-out/rate/pre-calculated/${planDuration}/${amortizationProfileId}`
    );
  }

}
