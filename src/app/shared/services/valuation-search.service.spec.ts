import { of } from 'rxjs';
import { environment } from '../../../environments/environment';
import { PageValuationSearch } from '../models/page-valuation-search.model';
import { ValuationSearchModel } from '../models/valuation-search.model';
import { ValuationSearchService } from './valuation-search.service';


describe('Service: ValuationSearchService', () => {
    
  let baseUrl = environment.baseUrl;
   

    describe('Test: findValuationSearch', () => {
		it('should return list of object', done => {

            let  response :PageValuationSearch= {content :[{} as ValuationSearchModel]} as PageValuationSearch;

            let valuationSearch: ValuationSearchModel = {} as ValuationSearchModel;

            let sort:string='updatedDate:ASC';
            let elementByPage:number=20;
            let indexPage:number=1;
            
            const httpMock = {
                post: jest.fn().mockReturnValue(of(response))
            };
            const serviceMock = new ValuationSearchService(httpMock as any);
            serviceMock.findValuationSearch(valuationSearch,sort,elementByPage,indexPage).subscribe((data) => {
                expect(httpMock.post).toHaveBeenCalled();
                expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/valuation/search?sort=updatedDate:ASC&byPage=20&page=1`, valuationSearch);
                expect(httpMock.post).toBeDefined();
                expect(data).toEqual(response)
                done();
            });
        });
    });


    describe('Test: findValuations', () => {
        it('should return  list of ValuationSearchModel objects', done => {
    
          const response: ValuationSearchModel[] = [];
    
          const httpMock = {
            get: jest.fn().mockReturnValue(of(response))
          };
          const serviceMock = new ValuationSearchService(httpMock as any);
          serviceMock.findValuations('1').subscribe((data) => {
            expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/valuation/search/1`);
            expect(httpMock.get).toBeDefined();
            expect(httpMock.get).toHaveBeenCalled();
            expect(data).toEqual(response);
            done();
          });
        });
    
      });
});