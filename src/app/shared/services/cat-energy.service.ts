import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { CatEnergyNatureModel } from '../models/cat-energy-nature.model';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CatEnergyService {

  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }

  getCatEnergyByNatureId(id: string): Observable<CatEnergyNatureModel[]> {
    return this.http.get<CatEnergyNatureModel[]>(
      `${this.baseUrl}/rest/v1/cat/cat-energy-nature/nature/${id}`);
  }

  getCatEnergyByBrandId(idBrand: string): Observable<CatEnergyNatureModel[]> {
    return this.http.get<CatEnergyNatureModel[]>(
      `${this.baseUrl}/rest/v1/cat/cat-energy-nature/brand/${idBrand}`);
  }
  
  saveCatEnergy(catEnergyList: CatEnergyNatureModel[]) {

    return this.http.post<CatEnergyNatureModel[]>(
      `${this.baseUrl}/rest/v1/cat/cat-energy-nature/`, catEnergyList
    ).pipe(
      catchError(
        err => {
          return throwError(err);
        }
      )
    );
  }

  deleteCatEnergy(catEnergyList: CatEnergyNatureModel[]) {

    return this.http.post<CatEnergyNatureModel[]>(
      `${this.baseUrl}/rest/v1/cat/cat-energy-nature/remove-energy-nature`, catEnergyList
    ).pipe(
      catchError(
        err => {
          return throwError(err);
        }
      )
    );
  }

}
