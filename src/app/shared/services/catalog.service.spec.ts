import { of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CatalogAddRequestService } from "./catalog-add-request.service";
import { SvsDescCollabModel } from '../models/svs-desc-collab.model';
import { CatalogService } from './catalog.service';
import { SvsDescCollabService } from './svs-desc-collab.service';



describe('Service: Catalog', () => {
  let svsDescCollabService;
  beforeEach(() => {
     svsDescCollabService = {
      getSvsDescCollab: jest.fn().mockReturnValue(of({
collaboratorFullName: 'test'
} as SvsDescCollabModel))
}
});
    it('should return Batch', done => {
      let svsDescCollabModel: SvsDescCollabModel = {
    
        collaboratorFullName: 'Batch'
      } as SvsDescCollabModel

    
          const serviceMock = new CatalogService(svsDescCollabService as any);
          //expect(serviceMock.fillInfoCreationUser('gf')).toEqual(of('Batch'));

          serviceMock.fillInfoCreationUser('BATCH').subscribe((data) => {
         
            expect(data).toEqual(svsDescCollabModel);
            done();
        });
      });
     

      it('should return test', done => {
        let svsDescCollabModel: SvsDescCollabModel = {
      
          collaboratorFullName: 'test'
        } as SvsDescCollabModel
  
         
           
            const serviceMock = new CatalogService(svsDescCollabService as any);
            //expect(serviceMock.fillInfoCreationUser('gf')).toEqual(of('Batch'));
  
            serviceMock.fillInfoCreationUser('mat').subscribe((data) => {
           
              expect(data).toEqual(svsDescCollabModel);
              done();
          });
        });
   
  
});