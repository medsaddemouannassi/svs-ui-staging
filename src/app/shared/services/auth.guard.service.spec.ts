import {of} from "rxjs";
import {AuthGuardWithForcedLogin} from './auth-guard.service';

describe('Service: AuthGuardWithForcedLogin', () => {

    describe('Test: canActivate', () => {
        it('should return  object', async () => {

            let authServiceMock: any = {
                isAuthenticated$: of(true),
                isDoneLoading$: of(true)
            }

            let state: any = {
                url: 'search'
            }


            let routerMock: any;
            let authGuardWithForcedLogin = new AuthGuardWithForcedLogin(authServiceMock, routerMock);
            authGuardWithForcedLogin.canActivate(routerMock, state)
        });
    });
});
