import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { ReportElementLitigationModel } from '../models/reportElementLitigationModel.model';
import { ReportElementModel } from '../models/reportElementModel.model';
import { LoadingService } from './loading-service.service';

@Injectable({
  providedIn: 'root'
})
export class ReportServiceService {

  baseUrl = environment.baseUrl;

  
  constructor(private http: HttpClient,private loadingService : LoadingService) { }
  generateReport(reportElement:ReportElementModel): Observable<Blob>{
    const httpOptions = {
      responseType: 'blob' as 'json',
      headers: new HttpHeaders({
        'Accept': 'application/pdf'
      })
    };
    this.loadingService.show()

    
  return this.http.post<Blob>(
      `${this.baseUrl}/rest/v1/report/expert/kslFileExpert`,reportElement,httpOptions
    ).pipe(finalize((()=>this.loadingService.hide())));
  }

  generateLitigationReport(reportLitigationElement:ReportElementLitigationModel): Observable<Blob>{
    const httpOptions = {
      responseType: 'blob' as 'json',
      headers: new HttpHeaders({
        'Accept': 'application/pdf'
      })
    };

    this.loadingService.show()

  return this.http.post<Blob>(
      `${this.baseUrl}/rest/v1/report/litigation/kslFile`,reportLitigationElement,httpOptions
    ).pipe(finalize((()=>this.loadingService.hide())));
  }
}
