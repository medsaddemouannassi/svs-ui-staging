import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { FinancialPlanModel } from '../models/financial-plan.model';
import { ValuationProcessingElementsModel } from '../models/valuation-processing-elements.model';

@Injectable({
  providedIn: 'root'
})
export class ProcessingEquipementValuationService {
  baseUrl = environment.baseUrl;

  financialPlans = new BehaviorSubject<FinancialPlanModel[]>(null);
 
  dateSubmit =  new BehaviorSubject<Date>(null);
  constructor(private http: HttpClient) { }

  getValuationResult(body: ValuationProcessingElementsModel): Observable<FinancialPlanModel[]> {
    return this.http.post<FinancialPlanModel[]>(
      `${this.baseUrl}/rest/v1/valuation-equipement-processing`, body
    ).pipe(
      catchError(
        err => {
          console.error('Cannot get processing result, please contact the application admin...', err);
          return throwError(err);
        }
      )
    );
  }
}
