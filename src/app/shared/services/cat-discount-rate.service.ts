import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { CatDiscountRateModel } from '../models/cat-discount-rate.model';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class CatDiscountRateService {
    baseUrl = environment.baseUrl;

    constructor(private http: HttpClient) { }

    getCatDiscountRateByCatDiscountId(catDiscountId){
        return this.http.get<CatDiscountRateModel[]>(
            `${this.baseUrl}/rest/v1/cat/cat-discount-rate/${catDiscountId}`
        );
    }

    getFirstYearRateByCatParams(sectorId: string, natureId: string, brandId: string, typeId: string, isNew: boolean,paramEnergyId:string){
        return this.http.get<number>(
            `${this.baseUrl}/rest/v1/cat/cat-discount-rate/cat-discount-first-year-rate/${sectorId}/${natureId}/${brandId}/${typeId}/${isNew}/${paramEnergyId}`
        );
    }

    getDiscountRateByTypeIdOrBrandIdOrNatureIdOrSectorId(typeId:string,brandId:string, natureId:string,sectorId:string , isNew:boolean) :Observable<CatDiscountRateModel[]> {
        return this.http.get<CatDiscountRateModel[]>(
            `${this.baseUrl}/rest/v1/cat/cat-discount-rate/search/${typeId}/${brandId}/${natureId}/${sectorId}/${isNew}`);
    }

}

