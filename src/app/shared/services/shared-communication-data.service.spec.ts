import { TestBed } from '@angular/core/testing';

import { SharedCommunicationDataService } from './shared-communication-data.service';

describe('SharedCommunicationDataService', () => {
    let service: any;
    service = {
      changeStatus: jest.fn().mockReturnValue(true),
  };

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(SharedCommunicationDataService);
    });

    describe('Test: service creation', () => {
        it('should be created', () => {
            expect(service).toBeTruthy();
        });
    });

    describe('Test: changeStatus', () => {
        it('should be change staus', () => {
            service.changeStatus('status');
        });
    });

    describe('Test: changeValoStatus', () => {
        it('should change valoStatus', () => {
            service.changeValoStatus('status');
        });
    });

});
