import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ParamAmortizationProfileModel } from '../models/param-amortization-profile.model';
@Injectable({
  providedIn: 'root'
})
export class ParamAmortizationProfileService {

 baseUrl = environment.baseUrl;
 
  constructor(private http: HttpClient) { }

  getAllParamAmortizationProfile() {
    return this.http.get<ParamAmortizationProfileModel[]>(
      `${this.baseUrl}/rest/v1/param-amortization-profile/`
    );
  }

  getParamAmortizationProfileByParamProductFamilyId(idParamProductFamily:string) {
    return this.http.get<ParamAmortizationProfileModel[]>(
      `${this.baseUrl}/rest/v1/param-amortization-profile/findByParamProductFamilyId/${idParamProductFamily}`
    );
  }
}
