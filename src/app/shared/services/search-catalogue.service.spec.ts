import { of } from 'rxjs';
import { environment } from '../../../environments/environment';
import { PageValuationSearchCatalogue } from '../models/page-valuation-search-catalogue.model';
import { SearchCatalogueService } from './search-catalogue.service';
import { SearchCatalogueResultModel } from '../models/catalogue-search-data.model';
import { SearchCatalogueFilterModel } from '../models/catalogue-search-filter.model';


describe('Service: ValuationSearchCatalogueService', () => {


    let baseUrl = environment.baseUrl;

    describe('Test: findValuationSearchCatalogue', () => {
		it('should return list of object', done => {

            let  response :PageValuationSearchCatalogue= {content :[{} as SearchCatalogueResultModel]} as PageValuationSearchCatalogue;

            let searchCatalogueFilterModel: SearchCatalogueFilterModel = {} as SearchCatalogueFilterModel;

            let elementByPage:number=20;
            let indexPage:number=1;

            const httpMock = {
                post: jest.fn().mockReturnValue(of(response))
            };
            const serviceMock = new SearchCatalogueService(httpMock as any);
            serviceMock.findSearchCatalogue(searchCatalogueFilterModel,elementByPage,indexPage).subscribe((data) => {
                expect(httpMock.post).toHaveBeenCalled();
                expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/search/catalogue?byPage=20&page=1`, searchCatalogueFilterModel);
                expect(httpMock.post).toBeDefined();
                expect(data).toEqual(response)
                done();
            });
        });
    });
});
