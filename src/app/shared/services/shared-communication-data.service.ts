import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedCommunicationDataService {

  private lockBtnStatus = new BehaviorSubject('true');
  currentBtnStatus = this.lockBtnStatus.asObservable();

  public triggerCommunication = new Subject();

  
  private valoStatus = new BehaviorSubject('');
  currentValoStatus = this.valoStatus.asObservable();

  public triggerValoStatus = new Subject();




  changeStatus(status: string) {
    this.lockBtnStatus.next(status)
  }

  changeValoStatus(status: string) {
    this.valoStatus.next(status)
  }

}
