import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ValuationDiscountModel } from '../models/valuationDiscount.model';
import { CatDiscountAverageProcessElementModel } from '../models/cat-discount-average-process-element.model';
import { CatDiscountAverageModel } from '../models/cat-discount-average.model';
import { ValuationModel } from '../models/valuation.model';

@Injectable({
  providedIn: 'root'
})
export class DecoteService {

  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }

  getDiscount(activitySectorId: string, natureId: string, brandId: string, typeId: string, isNew: boolean): Observable<object> {
    return this.http.get(
      `${this.baseUrl}/rest/v1/discount/${activitySectorId}/${natureId}/${brandId}/${typeId}/${isNew}`
    ).pipe(
      catchError(
        err => {
          console.error('Cannot get discount, please contact the application admin...', err);
          return throwError(err);
        }
      ));
  }

  getDiscountByValuationId(idValuation:string): Observable<ValuationDiscountModel[]>{

    return this.http.get<ValuationDiscountModel[]> (`${this.baseUrl}/rest/v1/discount/valuation/${idValuation}`).pipe(
      catchError(
        err => {
          console.error('cannot get valuationDiscount, please contact the application admin ...' , err);
          return throwError (err);
        }
      )
    );
  }

  saveValuationDiscounts(valuationDiscount: ValuationDiscountModel[]): Observable<ValuationDiscountModel[]> {
    return this.http.post<ValuationDiscountModel[]>(
      `${this.baseUrl}/rest/v1/discount`, valuationDiscount
    ).pipe(
      catchError(
        err => {
          return throwError(err);
        }
      )
    );
  }
  getDiscountByValuationIdOrFromCat(idValuation :string,typeId:string,brandId:string, natureId:string,sectorId:string,isNew :boolean,paramEnergyId:string): Observable<ValuationDiscountModel[]>{
    return this.http.get<ValuationDiscountModel[]>(`${this.baseUrl}/rest/v1/discount/valuation-or-cat-inherit/${idValuation}/${typeId}/${brandId}/${natureId}/${sectorId}/${isNew}/${paramEnergyId}`)

    
  }
  deleteValuationDiscounts(valuation: ValuationModel): Observable<string> {
    return this.http.post<string>(
      `${this.baseUrl}/rest/v1/discount/remove-valuation-discounts`, valuation
    );
  }
  getDiscountsAverage(elements :CatDiscountAverageProcessElementModel):Observable<CatDiscountAverageModel>{
    return this.http.post<CatDiscountAverageModel>(
      `${this.baseUrl}/rest/v1/discount/discounts-valuation-average`, elements
    ).pipe(
      catchError(
        err => {
          return throwError(err);
        }
      )
    );
  }
  checkDiscountIsChanged(valuationDiscount: ValuationDiscountModel[]): Observable<string> {
    return this.http.post(
      `${this.baseUrl}/rest/v1/discount/draft/discounts-valuation-changed`, valuationDiscount, {responseType: 'text'});
  }


}
