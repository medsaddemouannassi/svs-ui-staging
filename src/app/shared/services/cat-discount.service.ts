import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable, throwError,  } from 'rxjs';
import { CatDiscountModel } from '../models/cat-discount.model';
import { catchError } from 'rxjs/operators';
import { FullCatDiscountModel } from '../models/full-cat-discount.model';

@Injectable({
    providedIn: 'root'
})
export class CatDiscountService {

    baseUrl = environment.baseUrl;

    constructor(private http: HttpClient) { }



    getDiscountDetailsByIdAndDesc(id :string,desc: string,isNew:boolean): Observable<CatDiscountModel> {
        if(desc == 'brand'){
            return this.http.get<CatDiscountModel>(
                `${this.baseUrl}/rest/v1/cat/discount/brand/${id}/${isNew}`);
        }else{
            return this.http.get<CatDiscountModel>(
                `${this.baseUrl}/rest/v1/cat/discount/nature/${id}/${isNew}`);
        }

    }

    getDiscountDetailsByTypeIdOrBrandIdOrNatureIdOrSectorId(typeId:string,brandId:string, natureId:string,sectorId:string , isNew:boolean) :Observable<CatDiscountModel> {
        return this.http.get<CatDiscountModel>(
            `${this.baseUrl}/rest/v1/cat/discount/search/${typeId}/${brandId}/${natureId}/${sectorId}/${isNew}`);
    }

    saveCatDiscountAndRate(fullCatDiscount: FullCatDiscountModel, discriminant: string, isNew: boolean) : Observable<FullCatDiscountModel>{

        return this.http.post<FullCatDiscountModel>(
            `${this.baseUrl}/rest/v1/cat/discount/old/${discriminant}/${isNew}`, fullCatDiscount
          ).pipe(
            catchError(
              err => {
                return throwError(err);
              }
            )
          );
    }

}
