import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ParamProductFamilyModel } from '../models/param-product-family.model';
@Injectable({
  providedIn: 'root'
})
export class ParamProductFamilyService {

 baseUrl = environment.baseUrl;
 
  constructor(private http: HttpClient) { }

  getAllParamProductFamily() {
    return this.http.get<ParamProductFamilyModel[]>(
      `${this.baseUrl}/rest/v1/param-product-family`
    );
  }
}
