import { of, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CatRequestWorkFlow } from '../models/catalog-add-request-workflow.model';
import { CatalogAddRequestWorkflowService } from './catalog-add-request-workflow.service';


describe('Service: CatalogAddRequestService', () => {
   
    let baseUrl = environment.baseUrl;

    describe('Test: loadCatAddRequests', () => {
        it('should return list of CatRequestWaiting object', done => {   
      
         const response = {} as CatRequestWorkFlow;

         const httpMock = {
          post: jest.fn().mockReturnValue(of(response))
         };
         
         const serviceMock = new CatalogAddRequestWorkflowService(httpMock as any);
         serviceMock.saveCatRequestWorkflow({} as CatRequestWorkFlow).subscribe((data) => {
          expect(httpMock.post).toHaveBeenCalledWith( `${baseUrl}/rest/v1/cat/workflow`,{} as CatRequestWorkFlow);
          expect(httpMock.post).toBeDefined();
          expect(httpMock.post).toHaveBeenCalled();
          expect(data).toEqual(response);

      
          done();
         });
        });
    });

    it('should throw error', (done => {
        const httpMock = {
            post: jest.fn().mockImplementation(() => {
                return throwError(new Error('my error message'));
            })
        };

        
        const serviceMock = new CatalogAddRequestWorkflowService(httpMock as any);
        serviceMock.saveCatRequestWorkflow({} as CatRequestWorkFlow).subscribe((data) => {},()=>{
         expect(httpMock.post).toHaveBeenCalledWith( `${baseUrl}/rest/v1/cat/workflow`,{} as CatRequestWorkFlow);
         expect(httpMock.post).toBeDefined();
         expect(httpMock.post).toHaveBeenCalled();

     
         done();
        });
    }));
});