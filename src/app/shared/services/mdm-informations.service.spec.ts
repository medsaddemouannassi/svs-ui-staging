import { MdmInformationService } from "./mdm-informations.service";
import { environment } from 'src/environments/environment';
import { MdmInformationsModel } from '../models/mdm-informations.model';
import { of, throwError } from 'rxjs';

describe('Service: MdmInformationService', () => {

    let baseUrl = environment.baseUrl;

    describe('Test: getMdmCaInformation', () => {
        it('should return  object', done => {
            let mdmInformationsModel: MdmInformationsModel = {} as MdmInformationsModel;

            const httpMock = {
                get: jest.fn().mockReturnValue(of(mdmInformationsModel))
            };
            const serviceMock = new MdmInformationService(httpMock as any);
            serviceMock.getMdmCaInformation('M74591').subscribe((data) => {
                expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/mdm/rh-informations/M74591`);
                expect(httpMock.get).toBeDefined();
                expect(httpMock.get).toHaveBeenCalled();
                expect(data).toEqual(mdmInformationsModel);
                done();
            });
        });

        it('should throw error', (done => {
			const httpMock = {
				get: jest.fn().mockImplementation(() => {
                    return throwError(new Error('my error message'));
                })
			};
			const serviceMock = new MdmInformationService(httpMock as any);
			serviceMock.getMdmCaInformation('M74591').subscribe((data) => {
			//this is intentional
			},()=>{
			    expect(httpMock.get).toHaveBeenCalledWith( `${baseUrl}/rest/v1/mdm/rh-informations/M74591`);
				expect(httpMock.get).toBeDefined();
				expect(httpMock.get).toHaveBeenCalled();

                done();

            });
          }));

    });
    describe('Test: getAllNetworks', () => {
        it('should return  object', done => {
            let mdmInformationsModel: MdmInformationsModel [] = [];

            const httpMock = {
                get: jest.fn().mockReturnValue(of(mdmInformationsModel))
            };
            const serviceMock = new MdmInformationService(httpMock as any);
            serviceMock.getAllNetworks().subscribe((data) => {
                expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/mdm/networks`);
                expect(httpMock.get).toBeDefined();
                expect(httpMock.get).toHaveBeenCalled();
                expect(data).toEqual(mdmInformationsModel);
                done();
            });
        });
        it('should throw error', (done => {
            const httpMock = {
                get: jest.fn().mockImplementation(() => {
                    return throwError(new Error('my error message'));
                })
            };
            const serviceMock = new MdmInformationService(httpMock as any);
            serviceMock.getAllNetworks().subscribe((data) => {
            // This is intentional
            }, () => {
                expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/mdm/networks`);
                expect(httpMock.get).toBeDefined();
                expect(httpMock.get).toHaveBeenCalled();

                done();

            });
        })
    );

    });

describe('Test: getAllAgencies', () => {
    it('should return  list of object', done => {
        let mdmInformationsModel: MdmInformationsModel[] =[];

        const httpMock = {
            get: jest.fn().mockReturnValue(of(mdmInformationsModel))
        };
        const serviceMock = new MdmInformationService(httpMock as any);
        serviceMock.getAllAgencies().subscribe((data) => {
            expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/mdm/agencies`);
            expect(httpMock.get).toBeDefined();
            expect(httpMock.get).toHaveBeenCalled();
            expect(data).toEqual(mdmInformationsModel);
            done();
        });
    });
    it('should throw error', (done => {
        const httpMock = {
            get: jest.fn().mockImplementation(() => {
                return throwError(new Error('my error message'));
            })
        };
        const serviceMock = new MdmInformationService(httpMock as any);
        serviceMock.getAllAgencies().subscribe((data) => {
        // This is intentional
        }, () => {
            expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/mdm/agencies`);
            expect(httpMock.get).toBeDefined();
            expect(httpMock.get).toHaveBeenCalled();

            done();

        });
    })
);

});

describe('Test: getAllCollabsByQuery', () => {
    it('should return  collbsNAme', done => {
        let mdmInformationsModel: MdmInformationsModel[] =[];

        const httpMock = {
            get: jest.fn().mockReturnValue(of(mdmInformationsModel))
        };
        const serviceMock = new MdmInformationService(httpMock as any);
        serviceMock.getAllCollabsByQuery('test').subscribe((data) => {
            expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/mdm/collabs/test`);
            expect(httpMock.get).toBeDefined();
            expect(httpMock.get).toHaveBeenCalled();
            expect(data).toEqual(mdmInformationsModel);
            done();
        });
    });
    it('should throw error', (done => {
        const httpMock = {
            get: jest.fn().mockImplementation(() => {
                return throwError(new Error('my error message'));
            })
        };
        const serviceMock = new MdmInformationService(httpMock as any);
        serviceMock.getAllCollabsByQuery('test').subscribe((data) => {
        // This is intentional
        }, () => {
            expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/mdm/collabs/test`);
            expect(httpMock.get).toBeDefined();
            expect(httpMock.get).toHaveBeenCalled();

            done();

        });
    }));

});
});
