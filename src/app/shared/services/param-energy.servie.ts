import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ParamEnergyModel } from '../models/param-energy.model';
import {Observable} from 'rxjs';
import { retry } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ParamEnergyService {

    baseUrl = environment.baseUrl;

    constructor(private http: HttpClient) { }

    getAllParamEnergy(): Observable<ParamEnergyModel[]> {
        return this.http.get<ParamEnergyModel[]>(
            `${this.baseUrl}/rest/v1/cat/param-energy`
        );
    }


    getApplicableParamEnergyList(): Observable<ParamEnergyModel[]> {
        return this.http.get<ParamEnergyModel[]>(
            `${this.baseUrl}/rest/v1/cat/param-energy/applicable`
        );
    }

    getNatureUnusedParamEnergy(idNature : string): Observable<ParamEnergyModel[]> {
        return this.http.get<ParamEnergyModel[]>(
            `${this.baseUrl}/rest/v1/cat/param-energy/nature-unused/nature/${idNature}`
        );
    }

    getBrandUnusedParamEnergy(idNature : string, idBrand : string): Observable<ParamEnergyModel[]> {
        return this.http.get<ParamEnergyModel[]>(
            `${this.baseUrl}/rest/v1/cat/param-energy/brand-unused/nature/${idNature}/brand/${idBrand}`
        );
    }
}
