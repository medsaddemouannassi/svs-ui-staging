import { of } from 'rxjs';
import { environment } from '../../../environments/environment';
import { MdmInformationsModel } from '../models/mdm-informations.model';
import { SvsDescModel } from '../models/svs-desc.model';
import { SvsDescService } from './svs-desc.service';


describe('SvsDescService', () => {

    let baseUrl = environment.baseUrl;

    describe('Test: getSvsDescList', () => {
      it('should return  object', done => {
          let svsDescList: SvsDescModel = {} as SvsDescModel;

          const httpMock = {
              get: jest.fn().mockReturnValue(of(svsDescList))
          };
          const serviceMock = new SvsDescService(httpMock as any);
          serviceMock.getSvsDescList().subscribe((data) => {
              expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/svs-desc`);
              expect(httpMock.get).toBeDefined();
              expect(httpMock.get).toHaveBeenCalled();
              expect(data).toBeTruthy();
              done();
          });
      });

  });
  describe('Test: getAvailableSvsDesc', () => {
    it('should return  object', done => {
      let svsDescList: SvsDescModel = {} as SvsDescModel;

        const httpMock = {
            get: jest.fn().mockReturnValue(of(svsDescList))
        };
        const serviceMock = new SvsDescService(httpMock as any);
        serviceMock.getAvailableSvsDesc().subscribe((data) => {
            expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/svs-desc/available-svs-desc`);
            expect(httpMock.get).toBeDefined();
            expect(httpMock.get).toHaveBeenCalled();
            expect(data).toBeTruthy();
            done();
        });
    });
  });


  describe('Test: getDescManagerByQuery', () => {
    it('should return  object', done => {
      let svsDescList: MdmInformationsModel = {} as MdmInformationsModel;

        const httpMock = {
            get: jest.fn().mockReturnValue(of(svsDescList))
        };
        const serviceMock = new SvsDescService(httpMock as any);
        serviceMock.getDescManagerByQuery('car').subscribe((data) => {
            expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/svs-desc/manager-list/car`);
            expect(httpMock.get).toBeDefined();
            expect(httpMock.get).toHaveBeenCalled();
            expect(data).toBeTruthy();
            done();
        });
    });
  });
  describe('Test: submitSvsDesc', () => {
    it('should return  object', done => {

        let  response :SvsDescModel = {} as SvsDescModel;

        let svsCollab: SvsDescModel = {} as SvsDescModel;

        const httpMock = {
            post: jest.fn().mockReturnValue(of(response))
        };
        const serviceMock = new SvsDescService(httpMock as any);
        serviceMock.submitSvsDesc(svsCollab).subscribe((data) => {
            expect(httpMock.post).toHaveBeenCalled();
            expect(httpMock.post).toBeDefined();
            expect(data).toBeTruthy();
            done();
        });
    });

  });
  describe('Test: removeSvsDesc', () => {
    it('should return  object', done => {

        let  response :SvsDescModel = {} as SvsDescModel;

        let svsCollab: SvsDescModel = {} as SvsDescModel;

        const httpMock = {
            post: jest.fn().mockReturnValue(of(response))
        };
        const serviceMock = new SvsDescService(httpMock as any);
        serviceMock.removeSvsDesc(svsCollab).subscribe((data) => {
            expect(httpMock.post).toHaveBeenCalled();
            expect(httpMock.post).toBeDefined();
            expect(data).toBeTruthy();
            done();
        });
    });

  });
  describe('Test: getSvsNetworksListByManagerId', () => {
    it('should return  list ', done => {
      let svsDescList: SvsDescModel  [] =  [];

        const httpMock = {
            get: jest.fn().mockReturnValue(of(svsDescList))
        };
        const serviceMock = new SvsDescService(httpMock as any);
        const managerId ='13085'
        serviceMock.getSvsNetworksListByManagerId(managerId).subscribe((data) => {
            expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/svs-desc/manager/${managerId}`);
            expect(httpMock.get).toBeDefined();
            expect(httpMock.get).toHaveBeenCalled();
            expect(data).toEqual(svsDescList);
            done();
        });
    });
  });
  });
