import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ValuationRequestModel } from '../models/valuation-request.model';


@Injectable({
  providedIn: 'root'
})
export class ValuationRequestService {

  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }

  addValuationRequest(valuationRequest: any): Observable<ValuationRequestModel> {
    return this.http.post<ValuationRequestModel>(
      `${this.baseUrl}/rest/v1/valuation-request`,valuationRequest
    );
  }

  
  getValuationRequestById(id: string): Observable<ValuationRequestModel> {
    return this.http.get<ValuationRequestModel>(
      `${this.baseUrl}/rest/v1/valuation-request/${id}`,
      
    );
  }

  getValuationRequestIdByValuationId(id: string): Observable<string> {
    return this.http.get<string>(
      `${this.baseUrl}/rest/v1/valuation-request/id/${id}`,
      
    );
  }

}