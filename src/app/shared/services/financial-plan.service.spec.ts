import { of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { FinancialPlanModel } from '../models/financial-plan.model';
import { PlanEstimatedOutModel } from '../models/plan-estimated-out.model';
import { FinancialPlanService } from './financial-plan.service';


describe('Service: FinancialPlanService', () => {
    let service: FinancialPlanService;
    const http = jest.fn();
    let baseUrl = environment.baseUrl;

    beforeEach(() => {
        service = new FinancialPlanService(http as any);
    });



    describe('Test: loadFinancialPlanByValuationRequestId', () => {
        it('should return  object', done => {
            let financialPlanModelArray: FinancialPlanModel[] = [];
            let financialPlanModel: FinancialPlanModel = {} as FinancialPlanModel;
            financialPlanModel.id = '1';
            financialPlanModelArray.push(financialPlanModel);
            financialPlanModel = Object.assign({}, financialPlanModel);
            financialPlanModel.id = '2';
            financialPlanModelArray.push(financialPlanModel);
            const valuationRequestId = '1';
            const httpMock = {
                get: jest.fn().mockReturnValue(of(financialPlanModelArray))
            };
            const serviceMock = new FinancialPlanService(httpMock as any);
            serviceMock.loadFinancialPlanByValuationRequestId(valuationRequestId).subscribe((data) => {
                expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/financial-plan/valuation/request/${valuationRequestId}`);
                expect(httpMock.get).toBeDefined();
                expect(httpMock.get).toHaveBeenCalled();
                expect(data).toEqual(financialPlanModelArray);
                done();
            });
        });

    });


    describe('Test: loadFinancialPlanByValuationId', () => {
        it('should return  object', done => {
            let financialPlanModelArray: FinancialPlanModel[] = [];
            let financialPlanModel: FinancialPlanModel = {} as FinancialPlanModel;
            financialPlanModel.id = '1';
            financialPlanModelArray.push(financialPlanModel);
            financialPlanModel = Object.assign({}, financialPlanModel);
            financialPlanModel.id = '2';
            financialPlanModelArray.push(financialPlanModel);
            const valuationId = '1';
            const httpMock = {
                get: jest.fn().mockReturnValue(of(financialPlanModelArray))
            };
            const serviceMock = new FinancialPlanService(httpMock as any);
            serviceMock.loadFinancialPlanByValuationId(valuationId).subscribe((data) => {
                expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/financial-plan/valuation/${valuationId}`);
                expect(httpMock.get).toBeDefined();
                expect(httpMock.get).toHaveBeenCalled();
                expect(data).toEqual(financialPlanModelArray);
                done();
            });
        });

        describe('Test: saveFinancialPlan', () => {
            it('should return  object', done => {
                let financialPlanModelArray: FinancialPlanModel[] = [];
                let financialPlanModel: FinancialPlanModel = {} as FinancialPlanModel;
                financialPlanModelArray.push(financialPlanModel);
                financialPlanModelArray.push(financialPlanModel);
                let financialPlanModelArrayResponse: FinancialPlanModel[] = [];
                financialPlanModel = Object.assign({}, financialPlanModel);
                financialPlanModel.id = '1';
                financialPlanModelArrayResponse.push(financialPlanModel);
                financialPlanModel = Object.assign({}, financialPlanModel);
                financialPlanModel.id = '2';
                financialPlanModelArrayResponse.push(financialPlanModel);

                const httpMock = {
                    post: jest.fn().mockReturnValue(of(financialPlanModelArrayResponse))
                };
                const serviceMock = new FinancialPlanService(httpMock as any);
                serviceMock.saveFinancialPlan(financialPlanModelArray).subscribe((data) => {
                    expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/financial-plan/submit-financial-plan`, financialPlanModelArray);
                    expect(httpMock.post).toBeDefined();
                    expect(httpMock.post).toHaveBeenCalled();
                    expect(data).toEqual(financialPlanModelArrayResponse);
                    done();
                });
            });
        });

        describe('Test: removeFinancialPlan', () => {
            it('should return  object', done => {
                let financialPlanModelArray: FinancialPlanModel[] = [];
                let financialPlanModel: FinancialPlanModel = {} as FinancialPlanModel;
                financialPlanModelArray.push(financialPlanModel);
                financialPlanModelArray.push(financialPlanModel);
                let financialPlanModelArrayResponse: FinancialPlanModel[] = [];
                financialPlanModel = Object.assign({}, financialPlanModel);
                financialPlanModel.id = '1';
                financialPlanModelArrayResponse.push(financialPlanModel);
                financialPlanModel = Object.assign({}, financialPlanModel);
                financialPlanModel.id = '2';
                financialPlanModelArrayResponse.push(financialPlanModel);

                const httpMock = {
                    post: jest.fn().mockReturnValue(of(financialPlanModelArrayResponse))
                };
                const serviceMock = new FinancialPlanService(httpMock as any);
                serviceMock.removeFinancialPlan(financialPlanModelArray).subscribe((data) => {
                    expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/financial-plan/remove-financial-plan`, financialPlanModelArray);
                    expect(httpMock.post).toBeDefined();
                    expect(httpMock.post).toHaveBeenCalled();
                    expect(data).toEqual(financialPlanModelArrayResponse);
                    done();
                });
            });
        });

        describe('Test: removePlanEstimatedOut', () => {
            it('should return  object', done => {
                let planEstimatedOutArray: PlanEstimatedOutModel[] = [];
                let planEstimatedOutModel: PlanEstimatedOutModel = {} as PlanEstimatedOutModel;
                planEstimatedOutArray.push(planEstimatedOutModel);
                planEstimatedOutArray.push(planEstimatedOutModel);
                let planEstimatedOutArrayResponse: PlanEstimatedOutModel[] = [];
                planEstimatedOutModel = Object.assign({}, planEstimatedOutModel);
                planEstimatedOutModel.id = '1';
                planEstimatedOutArrayResponse.push(planEstimatedOutModel);
                planEstimatedOutModel = Object.assign({}, planEstimatedOutModel);
                planEstimatedOutModel.id = '2';
                planEstimatedOutArrayResponse.push(planEstimatedOutModel);

                const httpMock = {
                    post: jest.fn().mockReturnValue(of(planEstimatedOutArrayResponse))
                };
                const serviceMock = new FinancialPlanService(httpMock as any);
                serviceMock.removePlanEstimatedOut(planEstimatedOutArray).subscribe((data) => {
                    expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/financial-plan/remove-plan-estimated-out`, planEstimatedOutArray);
                    expect(httpMock.post).toBeDefined();
                    expect(httpMock.post).toHaveBeenCalled();
                    expect(data).toEqual(planEstimatedOutArrayResponse);
                    done();
                });
            });


        });

        describe('Test: plan estimated outs rate financial process', () => {
            it('should return object', done => {


                const response = [{ estimatedOutYear: 1, estimatedOutPrincRate: 80 } as PlanEstimatedOutModel];


                const httpMock = {
                    get: jest.fn().mockReturnValue(of(response))
                };
                const serviceMock = new FinancialPlanService(httpMock as any);
                serviceMock.planEstimatedOutRateWithFinancialProcessing(55, 2).subscribe((data) => {
                    expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/financial-plan/plan-estimated-out/rate/financial-processing/55/2`);
                    expect(httpMock.get).toBeDefined();
                    expect(httpMock.get).toHaveBeenCalled();
                    expect(data).toEqual(response);

                    done();
                });
            });
        });

        describe('Test: plan estimated outs rate precalculated', () => {
            it('should return object', done => {


                const response = [{ estimatedOutYear: 1, estimatedOutPrincRate: 80 } as PlanEstimatedOutModel];


                const httpMock = {
                    get: jest.fn().mockReturnValue(of(response))
                };
                const serviceMock = new FinancialPlanService(httpMock as any);
                serviceMock.planEstimatedOutRatePreCalculated(55, '7').subscribe((data) => {
                    expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/financial-plan/plan-estimated-out/rate/pre-calculated/55/7`);
                    expect(httpMock.get).toBeDefined();
                    expect(httpMock.get).toHaveBeenCalled();
                    expect(data).toEqual(response);

                    done();
                });
            });
        });
    });
});
