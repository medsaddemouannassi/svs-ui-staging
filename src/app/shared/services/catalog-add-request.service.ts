import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CatRequestModel } from '../models/cat-request.model';
import { CatRequestWaiting } from '../models/catalog-add-request.model';
import { CatRequestDoc } from '../models/catalog-request-doc.model';
import { DocumentInfoToUploadModel } from '../models/document-info-to-upload';
import { DocumentTempService } from './document-temp.service';
import { tap, mergeMap } from 'rxjs/operators';
import { generateFileName } from '../fileUtils';

@Injectable({
    providedIn: 'root'
})
export class CatalogAddRequestService {

    baseUrl = environment.baseUrl;

    constructor(private http: HttpClient, private documentTempService: DocumentTempService) { }


    loadCatAddRequests(): Observable<CatRequestWaiting[]> {
        return this.http.get<CatRequestWaiting[]>(
            `${this.baseUrl}/rest/v1/cat/requests`
        );
    }

    loadCatRequestDocs(idCatReq): Observable<CatRequestDoc[]> {
        return this.http.get<CatRequestDoc[]>(
            `${this.baseUrl}/rest/v1/cat/requests/docs/${idCatReq}`
        );
    }

    saveCatRequest(catRequestModel: CatRequestModel): Observable<CatRequestModel> {
        return this.http.post<CatRequestModel>(
            `${this.baseUrl}/rest/v1/cat/requests/save`, catRequestModel
        );
    }



    saveCatRequestDocument(file: File, catRequest: CatRequestModel) {
        let fileName = generateFileName(file.name);
        const source = from(this.documentTempService.saveTempDocument(file, fileName));

        return source.pipe(mergeMap(data => {
            let params = new HttpParams();
            params = params.append('catActivitySectorId', catRequest.catActivitySector.id);
            params = params.append('catNatureId', catRequest.catNature.id);
            params = params.append('catBrandId', catRequest.catBrand ? catRequest.catBrand.id : null);
            params = params.append('idCatRequest', catRequest.id);
            let documentInfoToUpload = {
                "fileName": file.name,
                "fileUrl": 'temp/' + fileName
            } as DocumentInfoToUploadModel;
            return this.http.post<CatRequestDoc>(`${this.baseUrl}/rest/v1/cat/requests/save-doc`, documentInfoToUpload, { params: params });
        }));



    }
}
