import { of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ParamProductFamilyModel } from '../models/param-product-family.model';
import { ParamProductFamilyService } from './param-product-family.service';
import { ParamNapService } from './param-nap.service';
import { ParamNapModel } from '../models/param-nap.model';
import { HttpParams } from '@angular/common/http';


describe('Service: ParamNapService', () => {
    let service: ParamNapService;
    const http = jest.fn();
    let baseUrl = environment.baseUrl;

    beforeEach(() => {
        service = new ParamNapService(http as any);
    });



    describe('Test: getAllParamNAPListByQuery', () => {
        it('should return  list', done => {
            let paramNapList: ParamNapModel[] = [];
            let paramNapModel: ParamNapModel = {} as ParamNapModel;
            paramNapModel.id = '1';
            paramNapList.push(paramNapModel);
            paramNapModel = Object.assign({}, paramNapModel);
            paramNapModel.id = '2';
            paramNapList.push(paramNapModel);
            const httpMock = {
                get: jest.fn().mockReturnValue(of(paramNapList))
            };
            let params = new HttpParams();
            params = params.append('query', null);
            const serviceMock = new ParamNapService(httpMock as any);
            serviceMock.getAllParamNAPListByQuery(null).subscribe((data) => {
                expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/param-nap`,{ params: params });
                expect(httpMock.get).toBeDefined();
                expect(httpMock.get).toHaveBeenCalled();
                expect(data).toEqual(paramNapList);
                done();
            });
        });

    });








});

