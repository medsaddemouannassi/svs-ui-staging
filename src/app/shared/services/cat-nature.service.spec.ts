import { HttpParams } from '@angular/common/http';
import { of, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CatNatureModel } from '../models/cat-nature.model';
import { ParamNapModel } from '../models/param-nap.model';
import { CatNatureService } from './cat-nature.service';


describe('Service: CatNatureService', () => {

 let baseUrl = environment.baseUrl;


 describe('Test: getNatureList', () => {
    it('should return  object', done => {


     const response : CatNatureModel[]=[];

     const httpMock = {
      get: jest.fn().mockReturnValue(of(response))
     };
     const serviceMock = new CatNatureService(httpMock as any);
     serviceMock.getNatureList().subscribe((data) => {
         expect(httpMock.get).toHaveBeenCalledWith( `${baseUrl}/rest/v1/cat/natures`);
      expect(httpMock.get).toBeDefined();
      expect(httpMock.get).toHaveBeenCalled();
      expect(data).toEqual(response);
      done();
     });
    });

   });

   describe('Test: getCatNatureById', () => {
    it('should return  object', done => {

     const paramNapModel={} as ParamNapModel;
        paramNapModel.id='14';
        paramNapModel.label='test'
     const response : CatNatureModel= {
        id:'1',
        label:'test',
        catActivitySector:null,
        comment: 'commnt',
        paramNap:paramNapModel,
        isActive: true,
        isValid: false,
        isEnrg: false
     };

     const httpMock = {
      get: jest.fn().mockReturnValue(of(response))
     };
     const serviceMock = new CatNatureService(httpMock as any);
     serviceMock.getCatNatureById('1').subscribe((data) => {
         expect(httpMock.get).toHaveBeenCalledWith( `${baseUrl}/rest/v1/cat/natures/1`);
      expect(httpMock.get).toBeDefined();
      expect(httpMock.get).toHaveBeenCalled();
      expect(data).toEqual(response);
      done();
     });
    });

   });

   describe('Test: save Nature', () => {
	it('should return  object', done => {
		   let catNaturereception :CatNatureModel = {} as CatNatureModel;

			let response = Object;

			const httpMock = {
				post: jest.fn().mockReturnValue(of(response))
			};
			const serviceMock = new CatNatureService(httpMock as any);
			serviceMock.saveCatNature(catNaturereception).subscribe((data) => {
			    expect(httpMock.post).toHaveBeenCalledWith( `${baseUrl}/rest/v1/cat/natures`,catNaturereception);
				expect(httpMock.post).toBeDefined();
				expect(httpMock.post).toHaveBeenCalled();
				expect(data).toEqual(response);

				done();
			});
        });

       it('should save Nature throws error', (done => {
           const httpMock = {
               post: jest.fn().mockImplementation(() => {
                   return throwError(new Error('my error message'));
               })
           };
           let catNaturereception: CatNatureModel = {} as CatNatureModel;
           const serviceMock = new CatNatureService(httpMock as any);
           serviceMock.saveCatNature(catNaturereception).subscribe((data) => {
           //this is intentional
           }, () => {
               expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/natures`, catNaturereception);
               expect(httpMock.post).toBeDefined();
               expect(httpMock.post).toHaveBeenCalled();

               done();

           });
       }));

    });

    describe('Test: getCatNatureListByCatSectorId', () => {
        it('should return  List', done => {


         const response : CatNatureModel[] =[ {
            id:'1',
            label:'test',
            catActivitySector: {id:"1",  label :"sector",comment:"",  isActive:true},
            comment: 'commnt',
            paramNap:null,
            isActive:true,
            isValid: false,
            isEnrg: false
         }];

         const httpMock = {
          get: jest.fn().mockReturnValue(of(response))
         };
         const serviceMock = new CatNatureService(httpMock as any);
         serviceMock.getNatureListByCatActivitySectorID('1').subscribe((data) => {
             expect(httpMock.get).toHaveBeenCalledWith( `${baseUrl}/rest/v1/cat/natures/sector/1`);
          expect(httpMock.get).toBeDefined();
          expect(httpMock.get).toHaveBeenCalled();
          expect(data).toEqual(response);
          done();
         });
        });

       });

    describe('Test: getNatureNapBySectorIdList', () => {
        it('should return ist of string', done => {
            const response: string[] = ["code1", "code2", "code3"];
            const httpMock = {
                get: jest.fn().mockReturnValue(of(response))
            };
            const serviceMock = new CatNatureService(httpMock as any);
            serviceMock.getNatureNapBySectorIdList('1').subscribe((data) => {
                expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/natures/nap-codes/1`);
                expect(httpMock.get).toBeDefined();
                expect(httpMock.get).toHaveBeenCalled();
                expect(data).toEqual(response);
                done();
            });
        });
    });

       describe('Test: loadNatureListByQuery', () => {
        it('should return  object', done => {
          const response =["natureLabel1"]  ;
         const httpMock = {
          get: jest.fn().mockReturnValue(of(response))
           };
         const serviceMock = new CatNatureService(httpMock as any);
         let params = new HttpParams();
         params = params.append('sector', 'sector');
         params = params.append('nature', 'nature');
         params = params.append('brand', 'brand');
         params = params.append('type', 'type');
         params = params.append('isActive', 'true');

         serviceMock.loadNatureListByQuery('sector','nature','brand','type', 'true').subscribe((data) => {
         expect(httpMock.get).toHaveBeenCalledWith( `${baseUrl}/rest/v1/cat/natures/search/label`,{params:params});
          expect(httpMock.get).toBeDefined();
          expect(httpMock.get).toHaveBeenCalled();
          expect(data).toEqual(response);
          done();
         });
        });
    });

    describe('Test: getCatNatureValid', () => {
        it('should return  list of CatNatureModel object', done => {
            const response = [] as CatNatureModel[];
            const httpMock = {
                get: jest.fn().mockReturnValue(of(response))
            };
            const serviceMock = new CatNatureService(httpMock as any);
            serviceMock.getCatNatureValid().subscribe((data) => {
                expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/natures/valid/`);
                expect(httpMock.get).toBeDefined();
                expect(httpMock.get).toHaveBeenCalled();
                expect(data).toEqual(response);
                done();
            });
        });
    });

    
});
