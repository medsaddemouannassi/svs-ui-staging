import { Injectable, SecurityContext } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';
import { CommunicationModalComponent } from '../communication-modal/communication-modal.component';
import { communicationThemes } from '../communication-themes';
import { DocumentEditorModalComponent } from '../document-editor-modal/document-editor-modal.component';
import { DocumentListModalComponent } from '../document-list-modal/document-list-modal.component';
import { PdfModalComponent } from '../documents/pdf-modal/pdf-modal.component';
import { CatRequestWaiting } from '../models/catalog-add-request.model';
import { ValuationDocumentModel } from '../models/valuationDocument.model';
import { ValidatorsModalComponent } from '../validators-modal/validators-modal.component';
import { DomSanitizer } from '@angular/platform-browser';
import { ValuationModel } from '../models/valuation.model';
import { MdmInformationsModel } from '../models/mdm-informations.model';


@Injectable({
  providedIn: 'root'
})
export class PopupService {

  constructor(private modalService: NgbModal,
    private sanitizer: DomSanitizer

  ) { }
  onConfirm: Subject<boolean> = new Subject<boolean>();
  onConfirmDoc: Subject<{ document: ValuationDocumentModel, index: number }> = new Subject<{ document: ValuationDocumentModel, index: number }>();
  onConfirmDocs: Subject<{ docIndex: ValuationDocumentModel, doc: File }[]> = new Subject<{ docIndex: ValuationDocumentModel, doc: File }[]>();
  onConfirmCommunication: Subject<any> = new Subject<any>();
  onConfirmDescValidator: Subject<any> = new Subject<any>();



  popupInfo(message: string, btnConfirm: string, description: string, btnCancel: string, type: any, hideCloseBtn?,newLineMessage?): void {
    const modalRef = this.modalService.open(type, {
      centered: true, backdrop: 'static',
      keyboard: false
    });
    modalRef.componentInstance.content = message;
    modalRef.componentInstance.confirmBtnMessage = btnConfirm;
    modalRef.componentInstance.description = description;
    modalRef.componentInstance.cancelBtnMessage = btnCancel;
    if (hideCloseBtn == true) {
      modalRef.componentInstance.showCloseBtn = false;

    }
    else {
      modalRef.componentInstance.showCloseBtn = true;

    }
    if (newLineMessage != null) {
      modalRef.componentInstance.newLineMessage = newLineMessage;
    }
    if (modalRef.componentInstance.onConfirm) {
      modalRef.componentInstance.onConfirm.subscribe((t) => this.onConfirm.next(t));
    }
  }
  popupPDF(btnConfirm: string, btnCancel: string, file: File, pdfSrc: string, isPdf: boolean): void {
    const modalRef = this.modalService.open(PdfModalComponent, {
      centered: false, windowClass: "pdfModalClass", backdrop: 'static',
      keyboard: false
    });
    let safeUrl = this.sanitizer.sanitize(SecurityContext.RESOURCE_URL, this.sanitizer.bypassSecurityTrustResourceUrl(pdfSrc));
    modalRef.componentInstance.confirmBtnMessage = btnConfirm;
    modalRef.componentInstance.file = file;
    modalRef.componentInstance.pdfSrc = safeUrl;
    modalRef.componentInstance.isPdf = isPdf;
    modalRef.componentInstance.cancelBtnMessage = btnCancel;
    modalRef.componentInstance.onConfirm.subscribe((t) => this.onConfirm.next(t));
  }
  popupDocument(document: ValuationDocumentModel, index: number, paramList: any): void {
    const modalRef = this.modalService.open(DocumentEditorModalComponent, {
      centered: true, backdrop: 'static',
      keyboard: false
    });
    modalRef.componentInstance.document = document;
    modalRef.componentInstance.index = index;
    modalRef.componentInstance.docSubTypesListParams = paramList;
    if (modalRef.componentInstance.onConfirm) {
      modalRef.componentInstance.onConfirm.subscribe((data) => this.onConfirmDoc.next(data));
    }
  }


  popupDocumentList(documentsList: any[], paramList: any): void {
    const modalRef = this.modalService.open(DocumentListModalComponent, {
      centered: true, backdrop: 'static',
      keyboard: false
    });
    modalRef.componentInstance.documentsList = documentsList;
    modalRef.componentInstance.docSubTypesListParams = paramList;
    if (modalRef.componentInstance.onConfirm) {
      modalRef.componentInstance.onConfirm.subscribe((data) => this.onConfirmDocs.next(data));
    }
  }

  popupCommunication(roles: any[], page: string, status: string, lastStatus: string, valRequestHistory: any[], theme, catRequest?: CatRequestWaiting): void {
    const modalRef = this.modalService.open(CommunicationModalComponent, {
      centered: true, backdrop: 'static',
      keyboard: false
    });
    modalRef.componentInstance.themes = communicationThemes;
    modalRef.componentInstance.page = page;
    modalRef.componentInstance.roles = roles;
    modalRef.componentInstance.status = status;
    modalRef.componentInstance.valRequestHistory = valRequestHistory;
    modalRef.componentInstance.catRequest = catRequest;
    modalRef.componentInstance.initialTheme = theme;
    modalRef.componentInstance.lastStatus = lastStatus;

    if (modalRef.componentInstance.onConfirm) {
      modalRef.componentInstance.onConfirm.subscribe((data) => this.onConfirmCommunication.next(data));
    }
    if (modalRef.componentInstance.onExit) {
      modalRef.componentInstance.onExit.subscribe(() => this.onConfirmCommunication.next(null));
    }
  }

  popupAskForValidation(networkId: string, valuation:ValuationModel,descInfo:MdmInformationsModel): void {
    const modalRef = this.modalService.open(ValidatorsModalComponent,
      {
        scrollable: true,
        windowClass: "summaryModalClass",
        centered: true,
        backdrop: 'static',
        keyboard: false
      }
    );
    modalRef.componentInstance.networkId = networkId;
    modalRef.componentInstance.amount = valuation.valuationRequest.financingAmount;
    modalRef.componentInstance.valuation = valuation;
    modalRef.componentInstance.descInfo = descInfo;

    
    if (modalRef.componentInstance.onConfirm) {
      modalRef.componentInstance.onConfirm.subscribe((data) => this.onConfirmDescValidator.next(data));
    }
    if (modalRef.componentInstance.onExit) {
      modalRef.componentInstance.onExit.subscribe(() => this.onConfirmDescValidator.next(null));
    }
  }

  dismissAll() {
    this.modalService.dismissAll();
  }

}
