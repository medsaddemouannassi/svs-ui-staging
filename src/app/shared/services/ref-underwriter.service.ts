import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { RefUnderwriterModel } from '../models/ref-underwriter.model';


@Injectable({
providedIn: 'root'
})
export class RefUnderwriterService {

baseUrl = environment.baseUrl;

constructor(private http: HttpClient) { }

loadLegalNamesCustomersByQuery(query: string): Observable<RefUnderwriterModel[]> {
return this.http.get<RefUnderwriterModel[]>(`${this.baseUrl}/rest/v1/ref-underwriters/search/lgl-name/${query}`);
}
loadNsirenByQuery(query: string): Observable<RefUnderwriterModel[]> {
    return this.http.get<RefUnderwriterModel[]>(`${this.baseUrl}/rest/v1/ref-underwriters/search/nsiren/${query}`);
}

}
