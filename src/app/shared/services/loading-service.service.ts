import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root',
  })
  export class LoadingService {
    private _loading = new BehaviorSubject<boolean>(false);

    public count = 0;
    public readonly loading$ = this._loading.asObservable();
  
    constructor() {}
  
    show() {
      this.count = this.count +1;
      this._loading.next(true);
    }
  
    hide() {
      this.count = this.count-1
      if(this.count == 0){
      this._loading.next(false);
      }
    }
  }