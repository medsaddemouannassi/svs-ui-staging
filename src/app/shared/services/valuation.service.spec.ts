import { of, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ValuationModel } from '../models/valuation.model';
import { ValuationDiscountModel } from '../models/valuationDiscount.model';
import { ValuationService } from './valuation.service';


describe('Service: ValuationService', () => {

    let baseUrl = environment.baseUrl;


    describe('Test: getValuationByIdRequest', () => {
		it('should return  object', done => {

			const response = {
                id: 'EVE15004'

            } as ValuationModel

			const httpMock = {
				get: jest.fn().mockReturnValue(of(response))
			};
			const serviceMock = new ValuationService(httpMock as any);
			serviceMock.getValuationByIdRequest('1').subscribe((data) => {
			    expect(httpMock.get).toHaveBeenCalledWith( `${baseUrl}/rest/v1/valuation/request/1`);
				expect(httpMock.get).toBeDefined();
				expect(httpMock.get).toHaveBeenCalled();
				expect(data).toEqual(response);

				done();
			});
		});

        it('should throw error', (done => {
			const httpMock = {
				get: jest.fn().mockImplementation(() => {
                    return throwError(new Error('my error message'));
                })
			};
			const serviceMock = new ValuationService(httpMock as any);
			serviceMock.getValuationByIdRequest('1').subscribe((data) => {
			//this is intentional
			},()=>{
			    expect(httpMock.get).toHaveBeenCalledWith( `${baseUrl}/rest/v1/valuation/request/1`);
				expect(httpMock.get).toBeDefined();
				expect(httpMock.get).toHaveBeenCalled();

                done();

            });
          }));

	});


	describe('Test: getValuation', () => {
		it('should return  object', done => {

			const response = {
                entrepriseInfo: null

            }

			const httpMock = {
				get: jest.fn().mockReturnValue(of(response))
			};
			const serviceMock = new ValuationService(httpMock as any);
			serviceMock.getValuationById('1').subscribe((data) => {
			    expect(httpMock.get).toHaveBeenCalledWith( `${baseUrl}/rest/v1/valuation/1`);
				expect(httpMock.get).toBeDefined();
				expect(httpMock.get).toHaveBeenCalled();
				expect(data).toEqual(response);

				done();
			});
		});

        it('should throw error', (done => {
			const httpMock = {
				get: jest.fn().mockImplementation(() => {
                    return throwError(new Error('my error message'));
                })
			};
			const serviceMock = new ValuationService(httpMock as any);
			serviceMock.getValuationById('1').subscribe((data) => {
			//this is intentional
			},()=>{
			    expect(httpMock.get).toHaveBeenCalledWith( `${baseUrl}/rest/v1/valuation/1`);
				expect(httpMock.get).toBeDefined();
				expect(httpMock.get).toHaveBeenCalled();

                done();

            });
          }));

	});


	describe('Test: saveValuation', () => {
	it('should return  object', done => {
		   let valuationReception:ValuationModel = {} as ValuationModel;

			let response = Object;

			const httpMock = {
				post: jest.fn().mockReturnValue(of(response))
			};
			const serviceMock = new ValuationService(httpMock as any);
			serviceMock.saveValuation(valuationReception).subscribe((data) => {
			    expect(httpMock.post).toHaveBeenCalledWith( `${baseUrl}/rest/v1/valuation/`,valuationReception);
				expect(httpMock.post).toBeDefined();
				expect(httpMock.post).toHaveBeenCalled();
				expect(data).toEqual(response);

				done();
			});
		});

		it('should throw error', (done => {
			let valuationReception:ValuationModel = {} as ValuationModel;

			const httpMock = {
				post: jest.fn().mockImplementation(() => {
                    return throwError(new Error('my error message'));
                })
			};
			const serviceMock = new ValuationService(httpMock as any);
			serviceMock.saveValuation(valuationReception).subscribe((data) => {
			//this is intentional
			},()=>{
			    expect(httpMock.post).toHaveBeenCalledWith( `${baseUrl}/rest/v1/valuation/`, valuationReception);
				expect(httpMock.post).toBeDefined();
				expect(httpMock.post).toHaveBeenCalled();
                done();

            });
          }));
	});


	describe('Test: saveValuationDiscounts', () => {
		it('should return  object', done => {
			let discountModel:ValuationDiscountModel  = {} as ValuationDiscountModel;

			 const response = Object;

			 const httpMock = {
				 post: jest.fn().mockReturnValue(of(response))
			 };
			 const serviceMock = new ValuationService(httpMock as any);
			 serviceMock.saveValuationDiscounts(discountModel).subscribe((data) => {
				 expect(httpMock.post).toHaveBeenCalledWith( `${baseUrl}/rest/v1/discount/`,discountModel);
				 expect(httpMock.post).toBeDefined();
				 expect(httpMock.post).toHaveBeenCalled();
				 expect(data).toEqual(response);
				 done();
			 });
		 });

		 it('should throw error', (done => {
			let discountModel:ValuationDiscountModel  = {} as ValuationDiscountModel;

			const httpMock = {
				post: jest.fn().mockImplementation(() => {
                    return throwError(new Error('my error message'));
                })
			};
			const serviceMock = new ValuationService(httpMock as any);
			serviceMock.saveValuationDiscounts(discountModel).subscribe((data) => {
			//this is intentional
			},()=>{
			    expect(httpMock.post).toHaveBeenCalledWith( `${baseUrl}/rest/v1/discount/`, discountModel);
				expect(httpMock.post).toBeDefined();
				expect(httpMock.post).toHaveBeenCalled();
                done();

            });
          }));


	});


	describe('Test: getLast10ValuationsByBrandId', () => {
		it('should return  list of Valuations objects', done => {


			const response: ValuationModel[] = [];

			const httpMock = {
				get: jest.fn().mockReturnValue(of(response))
			};
			const serviceMock = new ValuationService(httpMock as any);
			serviceMock.getLast10ValuationsByBrandId('1').subscribe((data) => {
				expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/valuation/cat-brand/1`);
				expect(httpMock.get).toBeDefined();
				expect(httpMock.get).toHaveBeenCalled();
				expect(data).toEqual(response);
				done();
			});
		});

	   });

	describe('Test: getLast10ValuationByNatureId', () => {
		it('should return  object', done => {

			const response: ValuationModel []= []

			const httpMock = {
				get: jest.fn().mockReturnValue(of(response))
			};
			const serviceMock = new ValuationService(httpMock as any);
			serviceMock.getLast10ValuationByNatureId('1').subscribe((data) => {
			    expect(httpMock.get).toHaveBeenCalledWith( `${baseUrl}/rest/v1/valuation/cat-nature/1`);
				expect(httpMock.get).toBeDefined();
				expect(httpMock.get).toHaveBeenCalled();
				expect(data).toEqual(response);

				done();
			});
		});




	});
});
