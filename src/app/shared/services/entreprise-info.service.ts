import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject, throwError } from 'rxjs';
import { catchError, tap, finalize } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { EntrepriseInfoModel } from '../models/entreprise-info.model';
import { EntrepriseInfoSeachModel } from '../models/entreprise-info-search.model';
import { LoadingService } from './loading-service.service';

@Injectable({
  providedIn: 'root'
})
export class EntrepriseInfoService {
  baseUrl = environment.baseUrl;
  socialReasonChanged = new BehaviorSubject('');
  affiliateChanged = new Subject<boolean>();
  contrepartieChanged =new BehaviorSubject('');
  currentMessage = this.contrepartieChanged.asObservable();


  public entrepriseInfo: EntrepriseInfoModel = null;
  noResultFound = false;

  constructor(private http: HttpClient, private loadingService:LoadingService) { }

  loadEntrepriseInfo(idRet: string, idBcp: string): Observable<EntrepriseInfoModel> {
    let entrepriseInfoSeachModel: EntrepriseInfoSeachModel={} as EntrepriseInfoSeachModel;
      entrepriseInfoSeachModel.idRet = idRet;
      entrepriseInfoSeachModel.idBcp = idBcp;
    this.loadingService.show();

    return this.http.post<EntrepriseInfoModel>(
      `${this.baseUrl}/rest/v1/entreprise-info`, entrepriseInfoSeachModel
    ).pipe(
      tap(data => {
        this.socialReasonChanged.next(data.socialReason);
        this.contrepartieChanged.next(data.mainUnderwName);
        

      }),
      catchError(
        err => {
          console.error('entreprise infos are not found, please contact the application admin...', err);
          return throwError(err);
        }
      ),finalize(() => {
        this.loadingService.hide()
      }
    ));
  }

  uploadDocument(file: File): Observable<any> {
    const formData = new FormData();
    formData.append('file', file, file.name);
    return this.http.post(`${this.baseUrl}/rest/v1/valuation/uploadDocument`, formData, { reportProgress: true });
  }

  uploadOtherDocument(file: File): Observable<any> {
    const formData = new FormData();
    formData.append('file', file, file.name);
    return this.http.post(`${this.baseUrl}/rest/v1/valuation/uploadOtherDocument`, formData, { reportProgress: true });
  }
  
  loadEntrepriseInfoBySiren(id: number, siren: string): Observable<EntrepriseInfoModel[]> {
    return this.http.get<EntrepriseInfoModel[]>(
      `${this.baseUrl}/rest/v1/entreprise-info/${id}/${siren}`);       
  }
}
