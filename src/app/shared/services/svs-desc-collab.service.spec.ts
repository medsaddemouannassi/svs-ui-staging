import { of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { MdmInformationsModel } from '../models/mdm-informations.model';
import { SvsDescCollabModel } from '../models/svs-desc-collab.model';
import { SvsDescHistoryModel } from '../models/svs-desc-hist.model';
import { SvsDescCollabService } from './svs-desc-collab.service';


describe('SvsDescCollabService', () => {
  let service: SvsDescCollabService;
  const http = jest.fn();
  let baseUrl = environment.baseUrl;

  beforeEach(() => {
    service = new SvsDescCollabService(http as any);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('Test: getSvsDescCollabList', () => {
    it('should return  object', done => {
        let svsCollabList: SvsDescCollabModel = {} as SvsDescCollabModel;

        const httpMock = {
            get: jest.fn().mockReturnValue(of(svsCollabList))
        };
        const serviceMock = new SvsDescCollabService(httpMock as any);
        serviceMock.getSvsDescCollabList('1').subscribe((data) => {
            expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/svs-desc-collab/1`);
            expect(httpMock.get).toBeDefined();
            expect(httpMock.get).toHaveBeenCalled();
            expect(data).toBeTruthy();
            done();
        });
    });

});

describe('Test: getDescCollabByQuery', () => {
  it('should return  object', done => {
      let svsCollabList: MdmInformationsModel = {} as MdmInformationsModel;

      const httpMock = {
          get: jest.fn().mockReturnValue(of(svsCollabList))
      };
      const serviceMock = new SvsDescCollabService(httpMock as any);
      serviceMock.getDescCollabByQuery('1','car').subscribe((data) => {
          expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/svs-desc-collab/collab-list/1/car`);
          expect(httpMock.get).toBeDefined();
          expect(httpMock.get).toHaveBeenCalled();
          expect(data).toBeTruthy();
          done();
      });
  });
});
describe('Test: removeSvsDescCollab', () => {
  it('should return  object', done => {

      let  response :SvsDescCollabModel = {} as SvsDescCollabModel;

      let svsCollab: SvsDescCollabModel = {} as SvsDescCollabModel;

      const httpMock = {
          post: jest.fn().mockReturnValue(of(response))
      };
      const serviceMock = new SvsDescCollabService(httpMock as any);
      serviceMock.removeSvsDescCollab(svsCollab).subscribe((data) => {
          expect(httpMock.post).toHaveBeenCalled();
          expect(httpMock.post).toBeDefined();
          expect(data).toBeTruthy();
          done();
      });
  });

});
describe('Test: submitSvsDescCollab', () => {
  it('should return  object', done => {

      let  response :SvsDescCollabModel = {} as SvsDescCollabModel;

      let svsCollab: SvsDescCollabModel = {} as SvsDescCollabModel;

      const httpMock = {
          post: jest.fn().mockReturnValue(of(response))
      };
      const serviceMock = new SvsDescCollabService(httpMock as any);
      serviceMock.submitSvsDescCollab(svsCollab).subscribe((data) => {
          expect(httpMock.post).toHaveBeenCalled();
          expect(httpMock.post).toBeDefined();
          expect(data).toBeTruthy();
          done();
      });
  });

});
describe('Test: isDescManager', () => {
    it('should return  boolean', done => {

        const httpMock = {
            get: jest.fn().mockReturnValue(of(true))
        };
        const mat ="M0001"
        const serviceMock = new SvsDescCollabService(httpMock as any);
        serviceMock.isDescManager(mat).subscribe((data) => {
            expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/svs-desc-collab/is-manager/${mat}`);
            expect(httpMock.get).toBeDefined();
            expect(httpMock.get).toHaveBeenCalled();
            expect(data).toEqual(true);
            done();
        });
    });
  });

  describe('Test: getSvsDescCollab', () => {
    it('should return  boolean', done => {

        let svsCollabList: SvsDescCollabModel = {} as SvsDescCollabModel;

        const httpMock = {
            get: jest.fn().mockReturnValue(of(svsCollabList))
        };
        const mat ="M0001"
        const serviceMock = new SvsDescCollabService(httpMock as any);
        serviceMock.getSvsDescCollab(mat).subscribe((data) => {
            expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/svs-desc-collab/collab/${mat}`);
            expect(httpMock.get).toBeDefined();
            expect(httpMock.get).toHaveBeenCalled();
            expect(data).toEqual(svsCollabList);
            done();
        });
    });
  });

  describe('Test: getSvsDescHistory', () => {
    it('should return SvsDescHistoryModel object', done => {

        let history: SvsDescHistoryModel = {} as SvsDescHistoryModel;

        const httpMock = {
            get: jest.fn().mockReturnValue(of(history))
        };
        const histId ="M0001"
        const serviceMock = new SvsDescCollabService(httpMock as any);
        serviceMock.getSvsDescHistory(histId).subscribe((data) => {
            expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/svs-desc-collab/history/${histId}`);
            expect(httpMock.get).toBeDefined();
            expect(httpMock.get).toHaveBeenCalled();
            expect(data).toEqual(history);
            done();
        });
    });
  });

  describe('Test: getAuthorizedCollabsForValoValidation', () => {
    it('should return list of SvsDescCollabModel object', done => {

        let validatorsList: SvsDescCollabModel[] = [];

        const httpMock = {
            get: jest.fn().mockReturnValue(of(validatorsList))
        };
        const networkId ="106";
        const amount = 100000;
        const serviceMock = new SvsDescCollabService(httpMock as any);
        serviceMock.getAuthorizedCollabsForValoValidation(networkId, amount).subscribe((data) => {
            expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/svs-desc-collab/validators/${networkId}/${amount}`);
            expect(httpMock.get).toBeDefined();
            expect(httpMock.get).toHaveBeenCalled();
            expect(data).toEqual(validatorsList);
            done();
        });
    });
  });

});
