import { of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CatAssetTypeNavigationModel } from '../models/cat-asset-type-navigation.model';
import { CatBrandNavigationModel } from '../models/cat-brand-navigation.model';
import { CatDataNavigationModel } from '../models/cat-data-navigation.model';
import { CatNatureNavigationModel } from '../models/cat-nature-navigation.model';
import { NodeService } from './node.service';


describe('Service: NodeService', () => {

    let baseUrl = environment.baseUrl;

 describe('Test: getBrandList', () => {
    it('should return  object', done => {


     const response : CatBrandNavigationModel[]=[];

     const httpMock = {
      get: jest.fn().mockReturnValue(of(response))
     };
     const serviceMock = new NodeService(httpMock as any);
     serviceMock.getBrand('1').subscribe((data) => {
         expect(httpMock.get).toHaveBeenCalledWith( `${baseUrl}/rest/v1/cat/brands/navigation/1`);
      expect(httpMock.get).toBeDefined();
      expect(httpMock.get).toHaveBeenCalled();
      expect(data).toEqual(response);
      done();
     });
    });

   });

   describe('Test: getAssetType', () => {
    it('should return  object', done => {


     const response : CatAssetTypeNavigationModel[]=[];

     const httpMock = {
      get: jest.fn().mockReturnValue(of(response))
     };
     const serviceMock = new NodeService(httpMock as any);
     serviceMock.getAssetType('1').subscribe((data) => {
         expect(httpMock.get).toHaveBeenCalledWith( `${baseUrl}/rest/v1/cat/asset-types/navigation/1`);
      expect(httpMock.get).toBeDefined();
      expect(httpMock.get).toHaveBeenCalled();
      expect(data).toEqual(response);
      done();
     });
    });

   });

   describe('Test: getActivitySectorsWithNature', () => {
    it('should return  object', done => {


     const response = {} as CatDataNavigationModel;

     const httpMock = {
      get: jest.fn().mockReturnValue(of(response))
     };
     const serviceMock = new NodeService(httpMock as any);
     serviceMock.getActivitySectorsWithNature().subscribe((data) => {
         expect(httpMock.get).toHaveBeenCalledWith( `${baseUrl}/rest/v1/cat/sectors/navigation`);
      expect(httpMock.get).toBeDefined();
      expect(httpMock.get).toHaveBeenCalled();
      expect(data).toEqual(response);
      done();
     });
    });

   });

   describe('Test: getNature', () => {
    it('should return CatNatureNavigationModel list', done => {


     const response : CatNatureNavigationModel[]=[];

     const httpMock = {
      get: jest.fn().mockReturnValue(of(response))
     };
     const serviceMock = new NodeService(httpMock as any);
     serviceMock.getNature('1').subscribe((data) => {
         expect(httpMock.get).toHaveBeenCalledWith( `${baseUrl}/rest/v1/cat/natures/navigation/1`);
      expect(httpMock.get).toBeDefined();
      expect(httpMock.get).toHaveBeenCalled();
      expect(data).toEqual(response);
      done();
     });
    });

   });

});
