import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { EntrepriseInfoModel } from '../models/entreprise-info.model';
import { ReportElementLitigationModel } from '../models/reportElementLitigationModel.model';
import { ReportElementModel } from '../models/reportElementModel.model';
import { LoadingService } from './loading-service.service';
import { ReportServiceService } from './report-service.service';


describe('ReportServiceService', () => {
  let service: ReportServiceService;
  const http = jest.fn();
  let baseUrl = environment.baseUrl;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = new ReportServiceService(http as any,new LoadingService());
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });


  describe('Test: generateReport', () => {
    it('should return  object', done => {

        let  response :Blob = {} as Blob;

        let  entrepriseInfo :EntrepriseInfoModel = {} as EntrepriseInfoModel;
        let  reportElement :ReportElementModel = {} as ReportElementModel;

        const httpMock = {
            post: jest.fn().mockReturnValue(of(response))
        };
        const serviceMock = new ReportServiceService(httpMock as any,new LoadingService());
        serviceMock.generateReport(reportElement).subscribe((data) => {
            expect(httpMock.post).toHaveBeenCalled();
            expect(httpMock.post).toBeDefined();
            expect(data).toBeTruthy();
            done();
        });
    });
    it('should return  object litigation', done => {

      let  response :Blob = {} as Blob;

      let  entrepriseInfo :EntrepriseInfoModel = {} as EntrepriseInfoModel;
      let  reportElement :ReportElementLitigationModel = {} as ReportElementLitigationModel;

      const httpMock = {
          post: jest.fn().mockReturnValue(of(response))
      };
      const serviceMock = new ReportServiceService(httpMock as any,new LoadingService());
      serviceMock.generateLitigationReport(reportElement).subscribe((data) => {
          expect(httpMock.post).toHaveBeenCalled();
          expect(httpMock.post).toBeDefined();
          expect(data).toBeTruthy();
          done();
      });
  });
  });
});
