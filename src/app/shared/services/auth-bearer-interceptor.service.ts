import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable, Optional } from '@angular/core';
import { OAuthModuleConfig, OAuthResourceServerErrorHandler, OAuthStorage } from 'angular-oauth2-oidc';
import { delay } from 'q';
import { Observable } from 'rxjs';
import { catchError, retryWhen, scan } from 'rxjs/operators';

@Injectable()
export class AuthBearerInterceptor implements HttpInterceptor {

  constructor(
    private authStorage: OAuthStorage,
    private errorHandler: OAuthResourceServerErrorHandler,
    @Optional() private moduleConfig: OAuthModuleConfig
  ) {
  }

  private checkUrl(url: string): boolean {
    const found = this.moduleConfig.resourceServer.allowedUrls.find(u => url.startsWith(u));
    return !!found;
  }

  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const url = req.url.toLowerCase();

    if (!this.moduleConfig) { return next.handle(req); }
    if (!this.moduleConfig.resourceServer) { return next.handle(req); }
    if (!this.moduleConfig.resourceServer.allowedUrls) { return next.handle(req); }
    if (!this.checkUrl(url)) { return next.handle(req); }

    const sendAccessToken = this.moduleConfig.resourceServer.sendAccessToken;

    if (sendAccessToken) {
      const tokenType = 'access_token';
      const header = this.bearerHeader(tokenType);

      const headers = req.headers.set('Authorization', header);
      req = req.clone({ headers });
    }

    return next.handle(req).pipe(retryWhen(e => e.pipe(scan((errorCount, error) => {
      console.error('error',error)

      if (errorCount >= 1 || (error.status != 403 && !url.includes('oauth20/token'))) {
          throw error;
      }

      delay(1000)

      return errorCount + 1;
  }, 0),
  )),
        catchError(err => this.errorHandler.handleError(err)),
    );
  }


  private bearerHeader(tokenType: string): string {
    const token = this.authStorage.getItem(tokenType);
    return 'Bearer ' + token;
  }
}
