import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { filter, map } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { SvsDescCollabService } from './svs-desc-collab.service';

@Injectable({ providedIn: 'root' })
export class RolesGuard implements CanActivate {
    constructor(
        private router: Router,
        private authenticationService: AuthService,
        private svsDescCollabService: SvsDescCollabService,
        private authService: AuthService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUserRoles = this.authenticationService.currentUserRolesList;
        let hasAuthorization = false;
        if (currentUserRoles != null && currentUserRoles.length > 0) {
            hasAuthorization = route.data.role && route.data.role.some(role => currentUserRoles.includes(role));
            if (!hasAuthorization && route.routeConfig.path == 'desc') {
                this.svsDescCollabService.isDescManager(this.authService.identityClaims['mat']).subscribe(res => {
                    if (!res) {
                        return this.router.navigateByUrl('/401');
                    }
               
                });
                return true;
            }

          else  if (hasAuthorization) {
                return this.authenticationService.isDoneLoading$
                    .pipe(
                        filter(isDone => isDone),
                        map(_ => route.data.role && route.data.role.some(role => currentUserRoles.includes(role)))
                    );
            }
            else{
                return this.router.navigateByUrl('/401');

            }

        }
        else {
            return null
        }

    }
}