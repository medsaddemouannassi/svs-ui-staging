import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { LitigationModel } from '../models/litigation.model';
import { Observable } from 'rxjs';
@Injectable({
    providedIn: 'root'
})
export class LitigationService {

    baseUrl = environment.baseUrl;

    constructor(private http: HttpClient) { }

    saveLitigation(litigation: LitigationModel): Observable<LitigationModel> {
        return this.http.post<LitigationModel>(
            `${this.baseUrl}/rest/v1/litigation`, litigation
        );
    }

    findLitigationByValuationId(valuationId: string): Observable<LitigationModel> {
        return this.http.get<LitigationModel>(
            `${this.baseUrl}/rest/v1/litigation/valuation/${valuationId}`
        );
    }
    findLitigationById(litigationId: string): Observable<LitigationModel> {
        return this.http.get<LitigationModel>(
            `${this.baseUrl}/rest/v1/litigation/${litigationId}`
        );
    }



}
