import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { generateFileName } from '../fileUtils';
import { tap } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
@Injectable({
  providedIn: 'root'
})
export class DocumentTempService {


  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }

  saveTempDocument(file, fileName) {

    let params = new HttpParams();
    params = params.append('filename', fileName);
    params = params.append('key', 'temp');

    return new Promise((resolve) => {
      this.http.get(`${this.baseUrl}/rest/v1/s3psurl`, { responseType: 'text', params: params }).subscribe(
        ((data: String) => {
          if (data) {
            this.http.put(data.toString(), file).subscribe(dataPut => {
              resolve('complete')
            });

          }
        }))
    });

  }

}
