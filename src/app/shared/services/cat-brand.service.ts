import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { CatBrandModel } from '../models/cat-brand.model';

@Injectable({
  providedIn: 'root'
})
export class CatBrandService {

  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }

  getBrandList(): Observable<CatBrandModel[]> {
    return this.http.get<CatBrandModel[]>(
      `${this.baseUrl}/rest/v1/cat/brands`
    );
  }

  getCatBrandById(id: string): Observable<CatBrandModel> {
    return this.http.get<CatBrandModel>(
      `${this.baseUrl}/rest/v1/cat/brands/${id}`);
  }

  getCatBrandByCatReqstId(id: string): Observable<CatBrandModel> {
    return this.http.get<CatBrandModel>(
      `${this.baseUrl}/rest/v1/cat/brands/cat-request/${id}`);
  }

  saveCatBrand(catBrand: CatBrandModel): Observable<CatBrandModel> {
    return this.http.post<CatBrandModel>(
      `${this.baseUrl}/rest/v1/cat/brands/`, catBrand
    ).pipe(
      catchError(
        err => {
          return throwError(err);
        }
      )
    )
  }

  getCatBrandListByCatNatureId(catNatureId: string): Observable<CatBrandModel[]> {
    return this.http.get<CatBrandModel[]>(
      `${this.baseUrl}/rest/v1/cat/brands/natures/${catNatureId}`);
  }

  deleteCatBrandById(brandId: string): Observable<any> {

    return this.http.post(`${this.baseUrl}/rest/v1/cat/brands/delete`, brandId);

  }
  loadCatBrandListByQuery(sector: string, nature: string, brand: string, type: string): Observable<string[]> {
    let params = new HttpParams();
    params = params.append('sector', sector);
    params = params.append('nature', nature);
    params = params.append('brand', brand);
    params = params.append('type', type);

    return this.http.get<string[]>(`${this.baseUrl}/rest/v1/cat/brands/search/label`,{params:params});
  }


  loadCatBrandListByLabel(brand: string): Observable<string[]> {
    let params = new HttpParams();
    params = params.append('brand', brand);
       return this.http.get<string[]>(`${this.baseUrl}/rest/v1/cat/brands/search/catbrand/label`,{params:params});
  }



}
