import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError, of, from } from 'rxjs';
import { catchError, mergeMap, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { CatDocumentModel } from '../models/cat-document.model';
import { DocumentInfoToUploadModel } from '../models/document-info-to-upload';
import { DocumentRequest } from '../models/document-request.model';
import { DocumentTempService } from './document-temp.service';
import { generateFileName } from '../fileUtils';

@Injectable({
    providedIn: 'root'
})
export class CatDocumentService {

    baseUrl = environment.baseUrl;

    constructor(private http: HttpClient, private documentTempService: DocumentTempService) { }


    uploadDocument(file: File, sectorId: string, natureId: string, brandId: string, assetTypeId: string, disc: string) {
        let fileName = generateFileName(file.name);
        const source = from(this.documentTempService.saveTempDocument(file, fileName));

        return source.pipe(mergeMap(data => {

            let params = new HttpParams();

            params = params.append('catActivitySectorId', sectorId);
            params = params.append('catNatureId', natureId);
            params = params.append('catBrandId', brandId);
            params = params.append('catAssetTypeId', assetTypeId);
            params = params.append('discriminator', disc);
            let documentInfoToUpload = {
                "fileName": file.name,
                "fileUrl": 'temp/' + fileName
            } as DocumentInfoToUploadModel;

            return this.http.post<CatDocumentModel>(`${this.baseUrl}/rest/v1/cat/cat-doc/save`, documentInfoToUpload, { params: params })
        }));
    }



    saveDocumentRequest(sectorId: string, natureId: string, brandId: string, assetTypeId: string, disc: string, pathSource: string, docName: string): Observable<CatDocumentModel> {

        let documentRequest = {} as DocumentRequest

        documentRequest.catActivitySectorId = sectorId;
        documentRequest.catNatureId = natureId
        documentRequest.catBrandId = brandId
        documentRequest.catAssetTypeId = assetTypeId
        documentRequest.discriminator = disc
        documentRequest.pathSource = pathSource
        documentRequest.docName = docName
        return this.http.post<CatDocumentModel>(`${this.baseUrl}/rest/v1/cat/cat-doc/validate-request`, documentRequest);

    }

    searchDocument(sectorId: string, natureId: string, brandId: string, assetTypeId: string, discriminator: string): Observable<CatDocumentModel[]> {
        let params = new HttpParams();
        params = params.append('sectorId', sectorId);
        params = params.append('natureId', natureId);
        params = params.append('brandId', brandId);
        params = params.append('assetTypeId', assetTypeId);
        params = params.append('discriminator', discriminator);
        return this.http.get<CatDocumentModel[]>(`${this.baseUrl}/rest/v1/cat/cat-doc/search`, { params: params });
    }

    downloadDocument(gedPath: string, docName: string): Observable<Blob> {
        const httpOptions = {
            responseType: 'blob' as 'json',
            headers: new HttpHeaders({
                'Accept': 'application/*'
            })
        };

        return this.http.post<Blob>(`${this.baseUrl}/rest/v1/cat/cat-doc/download?gedPath=${gedPath}&docName=${docName}`, null, httpOptions);



    }

    updateDocument(document: CatDocumentModel): Observable<CatDocumentModel> {
        return this.http.post<CatDocumentModel>(
            `${this.baseUrl}/rest/v1/cat/cat-doc/update`, document
        ).pipe(
            catchError(
                err => {
                    return throwError(err);
                }
            )
        );
    }
    deleteCatDocument(document: CatDocumentModel): Observable<boolean> {
        return this.http.post<boolean>(
            `${this.baseUrl}/rest/v1/cat/cat-doc/delete`, document
        ).pipe(
            catchError(
                err => {
                    return throwError(err);
                }
            )
        );
    }

    searchInheratedDocuments(sectorId: string, natureId: string, brandId: string, disc: string): Observable<CatDocumentModel[]> {
        let params = new HttpParams();
        params = params.append('sectorId', sectorId);
        params = params.append('natureId', natureId);
        params = params.append('brandId', brandId);
        params = params.append('discriminator', disc);
        return this.http.get<CatDocumentModel[]>(`${this.baseUrl}/rest/v1/cat/cat-doc/search-inherited`, { params: params })
    }


}
