import { ProcessingEquipementValuationService } from './processing-equipement-valuation.service';
import { environment } from 'src/environments/environment';
import { of, throwError } from 'rxjs';
import { ValuationProcessingElementsModel } from '../models/valuation-processing-elements.model';
import { FinancialPlanModel } from '../models/financial-plan.model';
import { ValuationDiscountModel } from '../models/valuationDiscount.model';
import { ValuationRequestModel } from '../models/valuation-request.model';

describe('Service: ProcessingEquipementValuationService', () => {
 let service: ProcessingEquipementValuationService;
 const http = jest.fn();
 const baseUrl = environment.baseUrl;

 beforeEach(() => {
  service = new ProcessingEquipementValuationService(http as any);
 });



 describe('Test: getValuationResult', () => {
  it('should return  object', done => {


   const response: FinancialPlanModel[] = [{} as FinancialPlanModel];
   const body: ValuationProcessingElementsModel = {
    discounts: [{} as ValuationDiscountModel],
    plans: [],
    valuationRequest: {} as ValuationRequestModel
   };
  const httpMock = {
   post: jest.fn().mockReturnValue(of(response))
   };
  const serviceMock = new ProcessingEquipementValuationService(httpMock as any);
  serviceMock.getValuationResult(body).subscribe((data) => {
   expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/valuation-equipement-processing`, body);
   expect(data).toEqual(response);
   done();
  });
 });

 it('should throw error', (done => {
    const response: FinancialPlanModel[] = [{} as FinancialPlanModel];
    const body: ValuationProcessingElementsModel = {
     discounts: [{} as ValuationDiscountModel],
     plans: [],
     valuationRequest: {} as ValuationRequestModel
    };
  const httpMock = {
   post: jest.fn().mockImplementation(() => {
   return throwError(new Error('my error message'));
  })
 };
  const serviceMock = new ProcessingEquipementValuationService(httpMock as any);
   serviceMock.getValuationResult(body).subscribe((data) => {
   //this is intentional
   }, ()=>{
    expect(httpMock.post).toHaveBeenCalledWith( `${baseUrl}/rest/v1/valuation-equipement-processing`, body);
    expect(httpMock.post).toBeDefined();
    expect(httpMock.post).toHaveBeenCalled();
    done();
   });
  }));



 });
});
