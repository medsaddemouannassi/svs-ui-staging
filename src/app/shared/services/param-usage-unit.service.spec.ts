import { of } from 'rxjs';

import { environment } from 'src/environments/environment';
import { ParamUsageUnitService } from './param-usage-unit';

describe('Service: ParamEnergyService', () => {
    let service: ParamUsageUnitService;
    const http = jest.fn();
    let baseUrl = environment.baseUrl;

    beforeEach(() => {
        service = new ParamUsageUnitService(http as any);
    });


    describe('Test: getAllParamUsageUnit', () => {
        it('should return  object', done => {


            const response = [];

            const httpMock = {
                get: jest.fn().mockReturnValue(of(response))
            };
            const serviceMock = new ParamUsageUnitService(httpMock as any);
            serviceMock.getAllParamUsageUnit().subscribe((data) => {
                expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/param-usage-unit`);
                expect(httpMock.get).toBeDefined();
                expect(httpMock.get).toHaveBeenCalled();
                expect(data).toEqual(response);
                done();
            });
        });


    });
});

