import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { ValuationRequestWorkflowModel } from '../models/valuation-request-workflow.model';
import { LoadingService } from './loading-service.service';

@Injectable({
  providedIn: 'root'
})
export class ValuationRequestWorkflowService {

  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient,private loadingService:LoadingService) { }

  saveValuationRequestWorkflow(valuationRequestWorkflow: ValuationRequestWorkflowModel): Observable<ValuationRequestWorkflowModel> {
    this.loadingService.show();

    return this.http.post<ValuationRequestWorkflowModel>(`${this.baseUrl}/rest/v1/valuation-request/workflow`, valuationRequestWorkflow).pipe(
      catchError(
        err => {
          return throwError(err);
        }
      ),finalize((()=>this.loadingService.hide()))
    );
  }

  getValuationRequestStatus(valuationRequestId: string): Observable<ValuationRequestWorkflowModel> {
    return this.http.get<ValuationRequestWorkflowModel>(`${this.baseUrl}/rest/v1/valuation-request/workflow/${valuationRequestId}`).pipe(
      catchError(
        err => {
          return throwError(err);
        }
      )
    );
  }

  getValuationRequestHistory(valuationRequestId: string): Observable<ValuationRequestWorkflowModel[]> {
    return this.http.get<ValuationRequestWorkflowModel[]>(`${this.baseUrl}/rest/v1/valuation-request/workflow/history/${valuationRequestId}`).pipe(
      catchError(
        err => {
          return throwError(err);
        }
      )
    );
  }


  assigneValuationRequest(instanceId: string, buisenessKey: string,isSimulated:boolean ): Observable<boolean> {
    if(!isSimulated) {isSimulated = false}
    return this.http.get<boolean>(`${this.baseUrl}/rest/v1/valuation-request/workflow/assigne/${instanceId}/${buisenessKey}/${isSimulated}`).pipe(
      catchError(
        err => {
          return throwError(err);
        }
      )
    );
  }

}
