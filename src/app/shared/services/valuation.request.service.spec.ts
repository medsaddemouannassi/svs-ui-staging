import { of } from 'rxjs';
import { environment } from '../../../environments/environment';
import { ValuationRequestModel } from '../models/valuation-request.model';
import { ValuationRequestService } from "./valuation.request.service";

describe('Service: ValuationRequest', () => {

    let baseUrl = environment.baseUrl;

    describe('Test: getValuationRequestById', () => {
		it('should return  object', done => {

			const response :ValuationRequestModel={} as ValuationRequestModel;

			const httpMock = {
				get: jest.fn().mockReturnValue(of(response))
			};
			const serviceMock = new ValuationRequestService(httpMock as any);
			serviceMock.getValuationRequestById('1').subscribe((data) => {
			    expect(httpMock.get).toHaveBeenCalledWith( `${baseUrl}/rest/v1/valuation-request/1`);
				expect(httpMock.get).toBeDefined();
				expect(httpMock.get).toHaveBeenCalled();
				expect(data).toEqual(response);
				done();
			});
		});
    });

    describe('Test: addValuationRequest', () => {
		it('should add and retun  object', done => {

			const response :ValuationRequestModel={} as ValuationRequestModel;

            const httpMock = {
				post: jest.fn().mockReturnValue(of(response))
            };
            let valuationRequest :ValuationRequestModel={} as ValuationRequestModel;
			const serviceMock = new ValuationRequestService(httpMock as any);
			serviceMock.addValuationRequest(valuationRequest).subscribe((data) => {
			    expect(httpMock.post).toHaveBeenCalledWith( `${baseUrl}/rest/v1/valuation-request`,valuationRequest);
				expect(httpMock.post).toBeDefined();
				expect(httpMock.post).toHaveBeenCalled();
				expect(data).toEqual(response);
				done();
			});
		});
	});
	
	
    describe('Test: getValuationRequestIdByValuationId', () => {
		it('should return  id', done => {

			const response= '5'

			const httpMock = {
				get: jest.fn().mockReturnValue(of(response))
			};
			const serviceMock = new ValuationRequestService(httpMock as any);
			serviceMock.getValuationRequestIdByValuationId('1').subscribe((data) => {
			    expect(httpMock.get).toHaveBeenCalledWith( `${baseUrl}/rest/v1/valuation-request/id/1`);
				expect(httpMock.get).toBeDefined();
				expect(httpMock.get).toHaveBeenCalled();
				expect(data).toEqual(response);
				done();
			});
		});
    });
});   