import { of, throwError } from 'rxjs';

import { environment } from 'src/environments/environment';
import { CatDiscountService } from './cat-discount.service';
import { CatDiscountModel } from '../models/cat-discount.model';
import { FullCatDiscountModel } from '../models/full-cat-discount.model';

describe('Service: CatBrandService', () => {

    let baseUrl = environment.baseUrl;


    describe('Test: getDiscountDetailsByIdAndDesc', () => {
        const response = {} as CatDiscountModel;

        const httpMock = {
            get: jest.fn().mockReturnValue(of(response))
        };
        const serviceMock = new CatDiscountService(httpMock as any);

        it('should return  desc brand ', done => {
            serviceMock.getDiscountDetailsByIdAndDesc("1", "brand", true).subscribe((data) => {
                expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/discount/brand/1/true`);
                expect(httpMock.get).toBeDefined();
                expect(httpMock.get).toHaveBeenCalled();
                expect(data).toEqual(response);
                done();
            });
        });
        it('should return  Discoutn nature', done => {

            serviceMock.getDiscountDetailsByIdAndDesc("1", "nature", true).subscribe((data) => {
                expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/discount/nature/1/true`);
                expect(httpMock.get).toBeDefined();
                expect(httpMock.get).toHaveBeenCalled();
                expect(data).toEqual(response);
                done();
            });
        });

    });

    describe('Test: saveCatDiscountAndRate', () => {
        it('should return  cat discounts and rate', done => {
            let fullCatDiscountModel: FullCatDiscountModel;

            const response = {} as FullCatDiscountModel;

            const httpMock = {
                post: jest.fn().mockReturnValue(of(response))
            };
            const serviceMock = new CatDiscountService(httpMock as any);
            serviceMock.saveCatDiscountAndRate(fullCatDiscountModel, 'nature', true).subscribe((data) => {
                expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/discount/old/nature/true`, fullCatDiscountModel);
                expect(httpMock.post).toBeDefined();
                expect(httpMock.post).toHaveBeenCalled();
                expect(data).toEqual(response);
                done();
            });
        });
            it('should throw error', (done => {
                const httpMock = {
                    post: jest.fn().mockImplementation(() => {
                        return throwError(new Error('my error message'));
                    })
                };
                const serviceMock = new CatDiscountService(httpMock as any);
                let fullCatDiscountModel: FullCatDiscountModel;

                serviceMock.saveCatDiscountAndRate(fullCatDiscountModel, 'nature', true).subscribe((data) => {
                   // This is intentional

                }, () => {
                    expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/discount/old/nature/true`, fullCatDiscountModel);
                    expect(httpMock.post).toBeDefined();
                    expect(httpMock.post).toHaveBeenCalled();

                    done();

                });
            }));


        describe('Test: getDiscountDetailsByTypeIdOrBrandIdOrNatureIdOrSectorId', () => {
            it('should return  cat discount ', done => {
                const response = {} as CatDiscountModel;

                const httpMock = {
                    get: jest.fn().mockReturnValue(of(response))
                };
                const serviceMock = new CatDiscountService(httpMock as any);
                serviceMock.getDiscountDetailsByTypeIdOrBrandIdOrNatureIdOrSectorId("1", "1", "1", "1", true).subscribe((data) => {
                    expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/discount/search/1/1/1/1/true`);
                    expect(httpMock.get).toBeDefined();
                    expect(httpMock.get).toHaveBeenCalled();
                    expect(data).toEqual(response);
                    done();
                });
            })

        });
    });

});
