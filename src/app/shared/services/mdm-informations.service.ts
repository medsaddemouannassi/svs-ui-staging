import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { MdmInformationsModel } from '../models/mdm-informations.model';

@Injectable({
  providedIn: 'root'
})
export class MdmInformationService {
  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }

  getAllNetworks(): Observable<any> {
    return this.http.get(`${this.baseUrl}/rest/v1/mdm/networks`)
      .pipe(catchError(
        err => {
          return throwError(err);
        }
      ))
  }

  getMdmCaInformation(mat: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/rest/v1/mdm/rh-informations/${mat}`)
      .pipe(catchError(
        err => {
          return throwError(err);
        }
      ))
  }


 
  getAllAgencies(): Observable<MdmInformationsModel[]> {

    return this.http.get<MdmInformationsModel[]>(`${this.baseUrl}/rest/v1/mdm/agencies`)
    
    .pipe(catchError(
    
    err => {
    
    return throwError(err);
  }
))
}​​​



  
getAllCollabsByQuery(query: string): Observable<any> {
  return this.http.get(`${this.baseUrl}/rest/v1/mdm/collabs/${query}`)
    .pipe(catchError(
      err => {
        return throwError(err);
      }
    ))
}
 

}
