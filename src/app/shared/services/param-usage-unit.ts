import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ParamUsageUnitModel } from '../models/param-usage-unit.model';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ParamUsageUnitService {

    baseUrl = environment.baseUrl;

    constructor(private http: HttpClient) { }

    getAllParamUsageUnit(): Observable<ParamUsageUnitModel[]> {
        return this.http.get<ParamUsageUnitModel[]>(
            `${this.baseUrl}/rest/v1/cat/param-usage-unit`
        );
    }
}