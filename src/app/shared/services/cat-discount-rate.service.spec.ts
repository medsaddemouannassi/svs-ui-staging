import { of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CatDiscountRateService } from './cat-discount-rate.service';
import { CatDiscountRateModel } from '../models/cat-discount-rate.model';


describe('Service: CatDiscountRateService', () => {

    let baseUrl = environment.baseUrl;


describe('Test: getCatDiscountRateByCatDiscountId', () => {
    it('should return  list of CatDiscountRateModel', done => {

        let list: CatDiscountRateModel[] = [];
        let catDiscountRate: CatDiscountRateModel = {} as CatDiscountRateModel;
        catDiscountRate.id = '1';
        list.push(catDiscountRate);
        const catDiscountId = '1';
        const httpMock = {
            get: jest.fn().mockReturnValue(of(list))
        };
        const serviceMock = new CatDiscountRateService(httpMock as any);
        serviceMock.getCatDiscountRateByCatDiscountId(catDiscountId).subscribe((data) => {
            expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/cat-discount-rate/${catDiscountId}`);
            expect(httpMock.get).toBeDefined();
            expect(httpMock.get).toHaveBeenCalled();
            expect(data).toEqual(list);
            done();
        });


    });

});

    describe('Test: getFirstYearRateByCatParams', () => {
        it('should return number', done => {

            let firstYearRate = 25;
            const httpMock = {
                get: jest.fn().mockReturnValue(of(firstYearRate))
            };
            let sectorId = '1';
            let natureId = '1';
            let brandId = '1';
            let typeId = '1';
            let isNew = false;
            let paramEnergyId='1'
            const serviceMock = new CatDiscountRateService(httpMock as any);
            serviceMock.getFirstYearRateByCatParams(sectorId, natureId, brandId, typeId, isNew,paramEnergyId).subscribe((data) => {
                expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/cat-discount-rate/cat-discount-first-year-rate/${sectorId}/${natureId}/${brandId}/${typeId}/${isNew}/${paramEnergyId}`);
                expect(httpMock.get).toBeDefined();
                expect(httpMock.get).toHaveBeenCalled();
                expect(data).toEqual(firstYearRate);
                done();
            });


        });

    });

    describe('Test: getDiscountRateByTypeIdOrBrandIdOrNatureIdOrSectorId', () => {
        it('should return  cat discount ', done => {
            const response : CatDiscountRateModel[] = [];

            const httpMock = {
                get: jest.fn().mockReturnValue(of(response))
            };
            const serviceMock = new CatDiscountRateService(httpMock as any);
            serviceMock.getDiscountRateByTypeIdOrBrandIdOrNatureIdOrSectorId("1", "1", "1", "1", true).subscribe((data) => {
                expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/cat-discount-rate/search/1/1/1/1/true`);
                expect(httpMock.get).toBeDefined();
                expect(httpMock.get).toHaveBeenCalled();
                expect(data).toEqual(response);
                done();
            });
        })

    });
});
