import { of, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ParamEnergyModel } from '../models/param-energy.model';
import { ParamSettingModel } from '../models/param-setting.model';
import { SettingConstant } from '../setting-constant';
import { ParamSettingService } from './param-setting.service';



describe('Service: ParamSettingService', () => {
    let service: ParamSettingService;
    const http = jest.fn();
    let baseUrl = environment.baseUrl;

    beforeEach(() => {
        service = new ParamSettingService(http as any);
    });


    describe('Test: getParamRate', () => {
        it('should return  object', done => {

            let valuationDate = new Date();
            const response : ParamEnergyModel[]=[];

            const httpMock = {
                get: jest.fn().mockReturnValue(of(response)),
                post: jest.fn().mockReturnValue(of(response))

            };
            const serviceMock = new ParamSettingService(httpMock as any);
            serviceMock.getParamRate(valuationDate).subscribe((data) => {
                expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/param/setting/date`,{filtreDate:valuationDate,filtreCode:SettingConstant.VALUE_RATE});
                expect(httpMock.post).toBeDefined();
                expect(httpMock.post).toHaveBeenCalled();
                expect(data).toEqual(response);
                done();
            });
        });

        it('should throw error', (done => {
            const valuationDate = new Date();

            const httpMock = {
                post: jest.fn().mockImplementation(() => {
                    return throwError(new Error('my error message'));
                })
            };
            const serviceMock = new ParamSettingService(httpMock as any);
            serviceMock.getParamRate(valuationDate).subscribe((data) => {
            //this is intentional
            },()=>{
                expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/param/setting/date`,{filtreDate:valuationDate,filtreCode:SettingConstant.VALUE_RATE});
                expect(httpMock.post).toBeDefined();
                expect(httpMock.post).toHaveBeenCalled();

                done();

            });
          }));


    });
    describe('Test: getParamByCode', () => {
        it('should return   list of object', done => {

            const response : ParamSettingModel[]=[];
            const docCode='code';
            const httpMock = {
                get: jest.fn().mockReturnValue(of(response)),

            };
            const serviceMock = new ParamSettingService(httpMock as any);
            serviceMock.getParamByCode(docCode).subscribe((data) => {
                expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/param/setting/code/${docCode}`);
                expect(httpMock.get).toBeDefined();
                expect(httpMock.get).toHaveBeenCalled();
                expect(data).toEqual(response);
                done();
            });
        });

        it('should throw error', (done => {
            const docCode='code';


                const httpMock = {
                    get: jest.fn().mockImplementation(() => {
                        return throwError(new Error('my error message'));
                    })
                };
                const serviceMock = new ParamSettingService(httpMock as any);
                serviceMock.getParamByCode(docCode).subscribe((data) => {
                //this is intentional
                },()=>{
                    expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/param/setting/code/${docCode}`);
                    expect(httpMock.get).toBeDefined();
                    expect(httpMock.get).toHaveBeenCalled();

                    done();

                });
          }));


    });
});
