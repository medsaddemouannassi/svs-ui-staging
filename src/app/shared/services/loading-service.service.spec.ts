import { TestBed } from '@angular/core/testing';
import { LoadingService } from './loading-service.service';



describe('LoadingService', () => {
    let service: any;
    service = {
      show: jest.fn().mockReturnValue(true),
      hide:jest.fn().mockReturnValue(true)
  };

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(LoadingService);
    });

    describe('Test: service creation', () => {
        it('should be created', () => {
            expect(service).toBeTruthy();
        });
    });

    describe('Test: show spinner', () => {
        it('should be change staus', () => {
            service.show();
            expect(service.count).toEqual(1);
        });
    });

   describe('Test: hide', () => {
        it('should hide spinner ', () => {
          service.count= 1 
            service.hide();
           expect(service.count).toEqual(0);

        });
        it('should not hide spinner ', () => {
          service.count= 2 
            service.hide();
           expect(service.count).toEqual(1);

        });
    });

});
