import { of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ParamProductFamilyModel } from '../models/param-product-family.model';
import { ParamProductFamilyService } from './param-product-family.service';


describe('Service: ParamProductFamilyService', () => {
    let service: ParamProductFamilyService;
    const http = jest.fn();
    let baseUrl = environment.baseUrl;

    beforeEach(() => {
        service = new ParamProductFamilyService(http as any);
    });



    describe('Test: getAllParamProductFamily', () => {
        it('should return  object', done => {
            let paramProductFamilyModelArray: ParamProductFamilyModel[] = [];
            let paramProductFamilyModel: ParamProductFamilyModel = {} as ParamProductFamilyModel;
            paramProductFamilyModel.id = '1';
            paramProductFamilyModelArray.push(paramProductFamilyModel);
            paramProductFamilyModel = Object.assign({}, paramProductFamilyModel);
            paramProductFamilyModel.id = '2';
            paramProductFamilyModelArray.push(paramProductFamilyModel);
            const httpMock = {
                get: jest.fn().mockReturnValue(of(paramProductFamilyModelArray))
            };
            const serviceMock = new ParamProductFamilyService(httpMock as any);
            serviceMock.getAllParamProductFamily().subscribe((data) => {
                expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/param-product-family`);
                expect(httpMock.get).toBeDefined();
                expect(httpMock.get).toHaveBeenCalled();
                expect(data).toEqual(paramProductFamilyModelArray);
                done();
            });
        });

    });








});

