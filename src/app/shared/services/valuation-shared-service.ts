import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ValuationRequestModel } from '../models/valuation-request.model';
import { ValuationModel } from '../models/valuation.model';

@Injectable({
    providedIn: 'root'
})
export class ValuationSharedService {
    private _isFromDuplicatedReception: boolean = false;
    private _isFromUndoDuplicatedReception: boolean = false;
    private _discountValueChanged: boolean = false;
    private _valuation = {} as ValuationModel;
    private _selectedValuatonId: string;
    private _financialPlans: any[] = [];
    private _discounts: any[] = [];
    private _docsToAdd: any[] = [];
    private _valuationRequest = {} as ValuationRequestModel;
    private _isFromRequest: boolean = false;
    private _idRefTiers: string = null;
    private _idBcp: string = null;
    private _catActivitySector;
    private _catBrand;
    private _catNature;
    private _catAssetType;


    public undoRequestDuplicationSubject: Subject<boolean> = new Subject()

    getIsFromDuplicatedReception(): boolean {
        return this._isFromDuplicatedReception;
    }
    setIsFromDuplicatedReception(_isFromDuplicatedReception) {
        this._isFromDuplicatedReception = _isFromDuplicatedReception;
    }

    getIsFromUndoDuplicatedReception(): boolean {
        return this._isFromUndoDuplicatedReception;
    }
    setIsFromUndoDuplicatedReception(_isFromUndoDuplicatedReception) {
        this._isFromUndoDuplicatedReception = _isFromUndoDuplicatedReception;
    }

    getValuation() {
        return this._valuation;
    }
    setValuation(valuation) {
        this._valuation = valuation;
    }

    getSelectedValuationId() {
        return this._selectedValuatonId;
    }
    setSelectedValuationId(selectedValuatonId) {
        this._selectedValuatonId = selectedValuatonId;
    }
    getFinancialPlans() {
        return this._financialPlans;

    }
    setFinancialPlans(_financialPlans) {
        this._financialPlans = _financialPlans;

    }



    // SVS-468 Dupliquer une demande déjà validée pour le CA

    getIsFromRequest(): boolean {
        return this._isFromRequest;
    }
    setIsFromRequest(isFromRequest) {
        this._isFromRequest = isFromRequest;
    }

    getValuationRequest() {
        return this._valuationRequest;
    }
    setValuationRequest(valuationRequest: ValuationRequestModel) {
        this._valuationRequest = valuationRequest;
    }
    getIdRefTiers() {
        return this._idRefTiers;
    }


    setIdRefTiers(idRefTiers) {
        this._idRefTiers = idRefTiers;
    }

    getIdBcp() {
        return this._idBcp;
    }
    setIdBcp(idBcp) {
        this._idBcp = idBcp;
    }



    getDiscounts() {
        return this._discounts;

    }
    setDiscounts(_discounts) {
        this._discounts = _discounts;
    }
    getDocsToAdd() {
        return this._docsToAdd;
    }
    setDocsToAdd(_docsToAdd) {
        this._docsToAdd = _docsToAdd;
    }

    getDiscountValueChanged() {
        return this._discountValueChanged;
    }
    setDiscountValueChanged(_discountValueChanged) {
        this._discountValueChanged = _discountValueChanged;
    }


    getCatActivitySector() {
        return this._catActivitySector;
    }
    setCatActivitySector(_catActivitySector) {
        this._catActivitySector = _catActivitySector;
    }
    getCatBrand() {
        return this._catBrand;
    }
    setCatBrand(_catBrand) {
        this._catBrand = _catBrand;
    }
    getCatNature() {
        return this._catNature;
    }
    setCatNature(_catNature) {
        this._catNature = _catNature;
    }
    getCatAssetType() {
        return this._catAssetType;
    }
    setCatAssetType(_catAssetType) {
        this._catAssetType = _catAssetType;
    }

    resetAllValue() {
        this._isFromDuplicatedReception = false;
        this._isFromUndoDuplicatedReception = false;
        this._discountValueChanged = false;
        this._isFromRequest = false;
        this._valuation = {} as ValuationModel;
        this._selectedValuatonId = null;
        this._financialPlans = [];
        this._discounts = [];
        this._docsToAdd = [];
        this._catActivitySector = null;
        this._catBrand = null;
        this._catNature = null;
        this._catAssetType = null;
        this._valuationRequest = {} as ValuationRequestModel;
        this._idRefTiers = null;
        this._idBcp = null;
    }

    //    simulation cat request
    private _catRequest;
    private _workFlowRequest;

    public addCatRequestSubject: Subject<boolean> = new Subject()

    getCatRequest() {
        return this._catRequest;
    }
    setCatRequest(_catRequest) {
        this._catRequest = _catRequest;
    }

    getWorkFlowRequest() {
        return this._workFlowRequest;
    }
    setWorkFlowRequest(_workFlowRequest) {
        this._workFlowRequest = _workFlowRequest;
    }

}
