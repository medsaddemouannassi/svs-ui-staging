import { of, throwError } from 'rxjs';
import { CatDiscountAverageProcessElementModel } from '../models/cat-discount-average-process-element.model';
import { CatDiscountAverageModel } from '../models/cat-discount-average.model';
import { ValuationModel } from '../models/valuation.model';
import { ValuationDiscountModel } from '../models/valuationDiscount.model';
import { DecoteService } from './decote.service';


describe('Service: DecoteService', () => {
 let service: DecoteService;
 const http = jest.fn();
    let baseUrl ;

 beforeEach(() => {
  service = new DecoteService(http as any);
  baseUrl= service.baseUrl;
 });


 describe('Test: getDiscount', () => {
  it('should return object', done => {


   const response = Object;


   const httpMock = {
    get: jest.fn().mockReturnValue(of(response))
   };
   const serviceMock = new DecoteService(httpMock as any);
   serviceMock.getDiscount('1', '1', '1', '1',true).subscribe((data) => {
       expect(httpMock.get).toHaveBeenCalledWith( `${baseUrl}/rest/v1/discount/1/1/1/1/true`);
    expect(httpMock.get).toBeDefined();
    expect(httpMock.get).toHaveBeenCalled();
    expect(data).toEqual(response);

    done();
   });
  });

        it('should throw error', (done => {
   const httpMock = {
    get: jest.fn().mockImplementation(() => {
                    return throwError(new Error('my error message'));
                })
   };
   const serviceMock = new DecoteService(httpMock as any);
   serviceMock.getDiscount('1', '1', '1', '1',true).subscribe((data) => {
   //this is intentional
   }, ()=>{
       expect(httpMock.get).toHaveBeenCalledWith( `${baseUrl}/rest/v1/discount/1/1/1/1/true`);
    expect(httpMock.get).toBeDefined();
    expect(httpMock.get).toHaveBeenCalled();

                done();

            });
          }));
	});

	describe('Test: getDiscountByValuationIdOrFromCat', () => {
		it('should return Discount', done => {

			let valuationDiscountModel:ValuationDiscountModel[] ;

			const response : ValuationDiscountModel[]= [];


			const httpMock = {
			 get: jest.fn().mockReturnValue(of(response))
			};
			const serviceMock = new DecoteService(httpMock as any);
			serviceMock.getDiscountByValuationIdOrFromCat('1','1','1','1','1',true,'1').subscribe((data) => {
				expect(httpMock.get).toHaveBeenCalledWith( `${baseUrl}/rest/v1/discount/valuation-or-cat-inherit/1/1/1/1/1/true/1`);
			 expect(httpMock.get).toBeDefined();
			 expect(httpMock.get).toHaveBeenCalled();
			 expect(data).toEqual(response);

			 done();
			});
		});
		it('should throw error', (done => {
			const httpMock = {
				get: jest.fn().mockImplementation(() => {
                    return throwError(new Error('my error message'));
                })
			};
			const serviceMock = new DecoteService(httpMock as any);
			serviceMock.getDiscountByValuationIdOrFromCat('1','1','1','1','1',true,'1').subscribe((data) => {
			//this is intentional
			},()=>{
			    expect(httpMock.get).toHaveBeenCalledWith( `${baseUrl}/rest/v1/discount/valuation-or-cat-inherit/1/1/1/1/1/true/1`);
				expect(httpMock.get).toBeDefined();
				expect(httpMock.get).toHaveBeenCalled();

                done();

            });
		  }));



});
describe('Test: saveValuationDiscounts', () => {
	it('should return  Valuation discounts', done => {
		let valuationDiscountModel:ValuationDiscountModel[] ;

		 const response : ValuationDiscountModel[]= [];

		 const httpMock = {
			 post: jest.fn().mockReturnValue(of(response))
		 };
		 const serviceMock = new DecoteService(httpMock as any);
		 serviceMock.saveValuationDiscounts(valuationDiscountModel).subscribe((data) => {
			 expect(httpMock.post).toHaveBeenCalledWith( `${baseUrl}/rest/v1/discount`,valuationDiscountModel);
			 expect(httpMock.post).toBeDefined();
			 expect(httpMock.post).toHaveBeenCalled();
			 expect(data).toEqual(response);
			 done();
		 });
	 });

	 it('should throw error', (done => {
		const httpMock = {
			post: jest.fn().mockImplementation(() => {
				return throwError(new Error('my error message'));
			})
		};
		let valuationDiscountModel:ValuationDiscountModel[] ;
		const serviceMock = new DecoteService(httpMock as any);
		serviceMock.saveValuationDiscounts(valuationDiscountModel).subscribe((respnse) => {
		//this is intentional
		},()=>{
			expect(httpMock.post).toHaveBeenCalledWith( `${baseUrl}/rest/v1/discount`,valuationDiscountModel);
			expect(httpMock.post).toBeDefined();
			expect(httpMock.post).toHaveBeenCalled();

			done();

		});
	  }));



});

describe('Test: getDiscountByValuation ', () => {
	it('should return Discount', done => {


		const response : ValuationDiscountModel[]= [];


		const httpMock = {
		 get: jest.fn().mockReturnValue(of(response))
		};
		const serviceMock = new DecoteService(httpMock as any);
		serviceMock.getDiscountByValuationId('1').subscribe((data) => {
			expect(httpMock.get).toHaveBeenCalledWith( `${baseUrl}/rest/v1/discount/valuation/1`);
		 expect(httpMock.get).toBeDefined();
		 expect(httpMock.get).toHaveBeenCalled();
		 expect(data).toEqual(response);

		 done();
		});
	});
	it('should throw error', (done => {
		const httpMock = {
			get: jest.fn().mockImplementation(() => {
				return throwError(new Error('my error message'));
			})
		};
		const serviceMock = new DecoteService(httpMock as any);
		serviceMock.getDiscountByValuationId('1').subscribe((data) => {
		//this is intentional
		},()=>{
			expect(httpMock.get).toHaveBeenCalledWith( `${baseUrl}/rest/v1/discount/valuation/1`);
			expect(httpMock.get).toBeDefined();
			expect(httpMock.get).toHaveBeenCalled();

			done();

		});
	  }));



});

	describe('Test: deleteValuationDiscounts', () => {
		it('should delete valuation discounts', done => {
			let valuation = {} as ValuationModel;
			valuation.id = '1';

			const response = "";

			const httpMock = {
				post: jest.fn().mockReturnValue(of(response))
			};
			const serviceMock = new DecoteService(httpMock as any);
			serviceMock.deleteValuationDiscounts(valuation).subscribe((data) => {
				expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/discount/remove-valuation-discounts`, valuation);
				expect(httpMock.post).toBeDefined();
				expect(httpMock.post).toHaveBeenCalled();
				expect(data).toEqual(response);
				done();
			});
		});

		it('should throw error', (done => {
			let valuation = {} as ValuationModel;
			valuation.id = '1';

			const httpMock = {
				post: jest.fn().mockImplementation(() => {
					return throwError(new Error('my error message'));
				})
			};
			const serviceMock = new DecoteService(httpMock as any);
			serviceMock.deleteValuationDiscounts(valuation).subscribe((respnse) => {
			//this is intentional
			}, () => {
				expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/discount/remove-valuation-discounts`, valuation);
				expect(httpMock.post).toBeDefined();
				expect(httpMock.post).toHaveBeenCalled();

				done();

			});
		}));

	});


	describe('Test: getDiscountsAverage', () => {
		it('should return CatDiscountAverageModel object', done => {

			let elements :CatDiscountAverageProcessElementModel;
			const response : CatDiscountAverageModel ={
				valuationsNumber: 33,
				discountAvgs:[]
			};

			const httpMock = {
				post: jest.fn().mockReturnValue(of(response))
			};
			const serviceMock = new DecoteService(httpMock as any);
			serviceMock.getDiscountsAverage(elements).subscribe((data) => {
				expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/discount/discounts-valuation-average`, elements);
				expect(httpMock.post).toBeDefined();
				expect(httpMock.post).toHaveBeenCalled();
				expect(data).toEqual(response);
				done();
			});
		});

		it('should getDiscountsAverage throw error', (done => {

			let elements :CatDiscountAverageProcessElementModel;
			const httpMock = {
				post: jest.fn().mockImplementation(() => {
					return throwError(new Error('my error message'));
				})
			};
			const serviceMock = new DecoteService(httpMock as any);
			serviceMock.getDiscountsAverage(elements).subscribe((respnse) => {
			//this is intentional
			}, () => {
				expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/discount/discounts-valuation-average`, elements);
				expect(httpMock.post).toBeDefined();
				expect(httpMock.post).toHaveBeenCalled();

				done();

			});
		}));

	});
	describe('Test: checkDiscountIsChanged', () => {
		it('should return checkDiscountIsChanged text', done => {
			let ValuationModel = {
				"id": "1",
				"discountRateComment": "comment",
				"experId": "1",
				"controllerId": "1",
				"validatorId": "1",
			
			  } as ValuationModel
			let discounts = [
				{
				  "id": "1",
				  "discountRateYear": 4,
				  "discountRateValue": 2,
				  "collAssetEstimVal": 1,
				  "valuation": ValuationModel,
				  "catDiscountRate": null,
				  "catDiscount": null
				} as ValuationDiscountModel,

			  ];
			const response : String = 'NO_CHANGES';

			const httpMock = {
				post: jest.fn().mockReturnValue(of('NO_CHANGES'))
			};
			const serviceMock = new DecoteService(httpMock as any);
			serviceMock.checkDiscountIsChanged(discounts).subscribe((data) => {
				expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/discount/draft/discounts-valuation-changed`, discounts,{responseType: 'text'});
				expect(httpMock.post).toBeDefined();
				expect(httpMock.post).toHaveBeenCalled();
				expect(data).toEqual(response);
				done();
			});
		});

		it('should checkDiscountIsChanged throw error', (done => {
			let ValuationModel = {
				"id": "1",
				"discountRateComment": "comment",
				"experId": "1",
				"controllerId": "1",
				"validatorId": "1",
			
			  } as ValuationModel
			let discounts = [
				{
				  "id": "1",
				  "discountRateYear": 4,
				  "discountRateValue": 2,
				  "collAssetEstimVal": 1,
				  "valuation": ValuationModel,
				  "catDiscountRate": null,
				  "catDiscount": null
				} as ValuationDiscountModel,

			  ];
			const httpMock = {
				post: jest.fn().mockImplementation(() => {
					return throwError(new Error('my error message'));
				})
			};
			const serviceMock = new DecoteService(httpMock as any);
			serviceMock.checkDiscountIsChanged(discounts).subscribe((respnse) => {
			//this is intentional
			}, () => {
				expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/discount/draft/discounts-valuation-changed`, discounts,{responseType: 'text'});
				expect(httpMock.post).toBeDefined();
				expect(httpMock.post).toHaveBeenCalled();
				done();

			});
		}));

	});

});
