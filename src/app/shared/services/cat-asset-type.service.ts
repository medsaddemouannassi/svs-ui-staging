import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { CatAssetTypeModel } from '../models/cat-asset-type.model';

@Injectable({
    providedIn: 'root'
})
export class CatAssetTypeService {

    baseUrl = environment.baseUrl;

    constructor(private http: HttpClient) { }


      saveAssetType(assetType:CatAssetTypeModel):Observable<any>{
        return this.http.post(`${this.baseUrl}/rest/v1/cat/asset-types`, assetType).pipe(
          catchError(
            err => {
              return throwError(err);
            }
          )
        )
    }

      getAssetTypeById(id): Observable<CatAssetTypeModel> {
        return this.http.get<CatAssetTypeModel>(
          `${this.baseUrl}/rest/v1/cat/asset-types/${id}`
        );
      }

      getAssetTypeByCatReqstId(id): Observable<CatAssetTypeModel> {
        return this.http.get<CatAssetTypeModel>(
          `${this.baseUrl}/rest/v1/cat/asset-types/cat-request/${id}`
        );
      }
      deleteAssetType(assetTypeId:string):Observable<any>{
        return this.http.post(`${this.baseUrl}/rest/v1/cat/asset-types/delete`, assetTypeId).pipe(
          catchError(
            err => {
              return throwError(err);
            }
          )
        );
    }
      getAssetTypeListByCatBrandId(catBrandId): Observable<CatAssetTypeModel[]> {
        return this.http.get<CatAssetTypeModel[]>(
          `${this.baseUrl}/rest/v1/cat/asset-types/brands/${catBrandId}`
        );
      }

      loadAssetTypeListByQuery(sector: string, nature: string, brand: string, type: string): Observable<string[]> {
        let params = new HttpParams();
        params = params.append('sector', sector);
        params = params.append('nature', nature);
        params = params.append('brand', brand);
        params = params.append('type', type);
        return this.http.get<string[]>(`${this.baseUrl}/rest/v1/cat/asset-types/search/label`,{params:params});
        }

        loadAssetTypeListByLabel(type: string): Observable<string[]> {
          let params = new HttpParams();

          params = params.append('type', type);
          return this.http.get<string[]>(`${this.baseUrl}/rest/v1/cat/asset-types/search/type/label`,{params:params});
          }
}

