import { HttpParams } from '@angular/common/http';
import { of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CatActivitySectorModel } from '../models/cat-activity-sector.model';
import { CatActivitySectorService } from './cat-activity-sector.service';


describe('Service: CatActivitySectorService', () => {

   let baseUrl = environment.baseUrl;

 describe('Test: getActivitySectorList', () => {
    it('should return  object', done => {


     const response: CatActivitySectorModel[]=[];


     const httpMock = {
      get: jest.fn().mockReturnValue(of(response)),
      post: jest.fn().mockReturnValue(of(response))

     };
     const serviceMock = new CatActivitySectorService(httpMock as any);
     serviceMock.getActivitySectorList().subscribe((data) => {
         expect(httpMock.get).toHaveBeenCalledWith( `${baseUrl}/rest/v1/cat/sectors`);
      expect(httpMock.get).toBeDefined();
      expect(httpMock.get).toHaveBeenCalled();
      expect(data).toEqual(response);
      done();
     });
    });


   });


   describe('Test: submitActivitySector', () => {
    it('should return  object', done => {


     const response: CatActivitySectorModel ={} as CatActivitySectorModel;


     const httpMock = {
      post: jest.fn().mockReturnValue(of(response))

     };
     const serviceMock = new CatActivitySectorService(httpMock as any);
     serviceMock.submitActivitySector({} as CatActivitySectorModel).subscribe((data) => {
         expect(httpMock.post).toHaveBeenCalledWith( `${baseUrl}/rest/v1/cat/sectors`,{});
      expect(httpMock.post).toBeDefined();
      expect(httpMock.post).toHaveBeenCalled();
      expect(data).toEqual(response);
      done();
     });
    });


   });

   describe('Test: getActivitysectorById', () => {
    it('should return  object', done => {


     const response: CatActivitySectorModel ={id:'1'} as CatActivitySectorModel;


     const httpMock = {
      get: jest.fn().mockReturnValue(of(response))

     };
     const serviceMock = new CatActivitySectorService(httpMock as any);
     serviceMock.getActivitySectorById('1').subscribe((data) => {
         expect(httpMock.get).toHaveBeenCalledWith( `${baseUrl}/rest/v1/cat/sectors/1`);
      expect(httpMock.get).toBeDefined();
      expect(httpMock.get).toHaveBeenCalled();
      expect(data).toEqual(response);
      done();
     });
    });
   });

describe('Test: loadActivitySectorListByQuery', () => {
      it('should return  object', done => {
        const response: string[] =['label']  ;
       const httpMock = {
        get: jest.fn().mockReturnValue(of(response))
         };
       const serviceMock = new CatActivitySectorService(httpMock as any);
       let params = new HttpParams();
       params = params.append('sector', 'sector');
       params = params.append('nature', 'nature');
       params = params.append('brand', 'brand');
       params = params.append('type', 'type');
       serviceMock.loadActivitySectorListByQuery('sector','nature','brand','type').subscribe((data) => {
           expect(httpMock.get).toHaveBeenCalledWith( `${baseUrl}/rest/v1/cat/sectors/search/label`,{params:params});
        expect(httpMock.get).toBeDefined();
        expect(httpMock.get).toHaveBeenCalled();
        expect(data).toEqual(response);
        done();
       });
      });
  });
});
