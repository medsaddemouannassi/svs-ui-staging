import { of } from 'rxjs';

import { environment } from 'src/environments/environment';
import { ParamAmortizationProfileService } from './param-amortization-profile.service';
import { ParamAmortizationProfileModel } from '../models/param-amortization-profile.model';

describe('Service: ParamAmortizationProfileService', () => {
    let service: ParamAmortizationProfileService;
    const http = jest.fn();
    let baseUrl = environment.baseUrl;

    beforeEach(() => {
        service = new ParamAmortizationProfileService(http as any);
    });



    describe('Test: getAllParamAmortizationProfile', () => {
        it('should return  object', done => {
            let paramAmortizationProfileArray: ParamAmortizationProfileModel[] = [];
            let paramAmortizationProfileModel: ParamAmortizationProfileModel = {} as ParamAmortizationProfileModel;
            paramAmortizationProfileModel.id = '1';
            paramAmortizationProfileArray.push(paramAmortizationProfileModel);
            paramAmortizationProfileModel = Object.assign({}, paramAmortizationProfileModel);
            paramAmortizationProfileModel.id = '2';
            paramAmortizationProfileArray.push(paramAmortizationProfileModel);
            const httpMock = {
                get: jest.fn().mockReturnValue(of(paramAmortizationProfileArray))
            };
            const serviceMock = new ParamAmortizationProfileService(httpMock as any);
            serviceMock.getAllParamAmortizationProfile().subscribe((data) => {
                expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/param-amortization-profile/`);
                expect(httpMock.get).toBeDefined();
                expect(httpMock.get).toHaveBeenCalled();
                expect(data).toEqual(paramAmortizationProfileArray);
                done();
            });
        });

    });




    describe('Test: getAllParamAmortizationProfile', () => {
        it('should return  object', done => {
            let paramAmortizationProfileArray: ParamAmortizationProfileModel[] = [];
            let paramAmortizationProfileModel: ParamAmortizationProfileModel = {} as ParamAmortizationProfileModel;
            paramAmortizationProfileModel.id = '1';
            paramAmortizationProfileArray.push(paramAmortizationProfileModel);
            paramAmortizationProfileModel = Object.assign({}, paramAmortizationProfileModel);
            paramAmortizationProfileModel.id = '2';
            paramAmortizationProfileArray.push(paramAmortizationProfileModel);
            const httpMock = {
                get: jest.fn().mockReturnValue(of(paramAmortizationProfileArray))
            };
            const serviceMock = new ParamAmortizationProfileService(httpMock as any);
            let idParamProductFamily='1';
            serviceMock.getParamAmortizationProfileByParamProductFamilyId(idParamProductFamily).subscribe((data) => {
                expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/param-amortization-profile/findByParamProductFamilyId/${idParamProductFamily}`);
                expect(httpMock.get).toBeDefined();
                expect(httpMock.get).toHaveBeenCalled();
                expect(data).toEqual(paramAmortizationProfileArray);
                done();
            });
        });

    });




});

