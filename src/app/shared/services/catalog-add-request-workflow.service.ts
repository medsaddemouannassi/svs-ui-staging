import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { CatRequestWorkFlow } from '../models/catalog-add-request-workflow.model';

@Injectable({
  providedIn: 'root'
})
export class CatalogAddRequestWorkflowService {

  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }



  saveCatRequestWorkflow(catRequestWorkFlow: CatRequestWorkFlow): Observable<CatRequestWorkFlow> {
  
    return this.http.post<CatRequestWorkFlow>(`${this.baseUrl}/rest/v1/cat/workflow`, catRequestWorkFlow).pipe(
      catchError(
        err => {
          return throwError(err);
        }
      )
    );
  }

  getCatRequestStatus(workflowCode: string): Observable<CatRequestWorkFlow> {
   

    return this.http.get<CatRequestWorkFlow>(`${this.baseUrl}/rest/v1/cat/workflow/${workflowCode}`).pipe(
      catchError(
        err => {
          return throwError(err);
        }
      )
    );
  }



}