import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { LoginOptions, OAuthErrorEvent, OAuthService } from 'angular-oauth2-oidc';
import { BehaviorSubject, Observable, ReplaySubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { OAuth2Token } from '../models/oauth2-token.model';

@Injectable({ providedIn: 'root' })
export class AuthService {

  private isAuthenticatedSubject$ = new BehaviorSubject<boolean>(false);
  public isAuthenticated$ = this.isAuthenticatedSubject$.asObservable();

  private isDoneLoadingSubject$ = new ReplaySubject<boolean>();
  public isDoneLoading$ = this.isDoneLoadingSubject$.asObservable();

  private currentUserRolesSubject$ = new BehaviorSubject<string[]>([]);
  public currentUserRoles$ = this.currentUserRolesSubject$.asObservable();

  constructor(
    private oauthService: OAuthService,
    private router: Router,
    private http: HttpClient
  ) {
    // Useful for debugging:
    this.oauthService.events.subscribe(event => {
      if (event instanceof OAuthErrorEvent) {
        console.error(event);
      } 
    });
    this.oauthService.setupAutomaticSilentRefresh();
  }

  private loadDiscoveryDocumentAndLogin(): Promise<boolean> {
    return this.oauthService.loadDiscoveryDocument(environment.fullUrlOAuth).then((doc) => {

      return this.oauthService.tryLogin();

    }).then((_) => {

      let options: LoginOptions & { state?: string } = null

      if (!this.oauthService.hasValidIdToken() || !this.oauthService.hasValidAccessToken()) {

        const state = typeof options.state === 'string' ? options.state : '';

        this.oauthService.initLoginFlow(state);

        return false;

      } else {

        return true;

      }

    })
  }
  public runInitialLoginSequence(): Promise<void> {
    return this.loadDiscoveryDocumentAndLogin()
      .then(() => {
        if (this.oauthService.getIdentityClaims()) {
          // This is intentional

        }
      })
      .then(async () => {
        if (this.oauthService.hasValidAccessToken()) {
          await this.getCurrentUserRoles().toPromise().then(roles => {
            this.currentUserRolesSubject$.next(roles);
          });
          return Promise.resolve();
        }
      })
      .then(() => {
        this.isDoneLoadingSubject$.next(true);
        this.isAuthenticatedSubject$.next(true);
        if (this.oauthService.state && this.oauthService.state !== 'undefined' && this.oauthService.state !== 'null') {
          this.router.navigateByUrl(decodeURIComponent(this.oauthService.state));
        }
      })
      .catch(() => {
        this.isDoneLoadingSubject$.next(true);
        this.isAuthenticatedSubject$.next(false);
      });

  }

  public login(targetUrl?: string) {
    this.oauthService.initImplicitFlow(targetUrl || this.router.url);
  }

  public getCurrentUserRoles(): Observable<string[]> {
    return this.http.get<OAuth2Token>(`${environment.baseUrl}/rest/v1/auth-token`)
      .pipe(map(data => data.roles));
  }

  public logout() { this.oauthService.logOut(); }
  public refresh() { this.oauthService.silentRefresh(); }
  public hasValidToken() { return this.oauthService.hasValidAccessToken(); }
  public get accessToken() { return this.oauthService.getAccessToken(); }
  public get identityClaims() { return this.oauthService.getIdentityClaims(); }
  public get idToken() { return this.oauthService.getIdToken(); }
  public get logoutUrl() { return this.oauthService.logoutUrl; }

  public get currentUserRolesList(): string[] {
    return this.currentUserRolesSubject$.value;
  }
}
