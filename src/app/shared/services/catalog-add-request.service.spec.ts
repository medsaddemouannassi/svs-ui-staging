import { of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CatalogAddRequestService } from "./catalog-add-request.service";
import { CatRequestDoc } from '../models/catalog-request-doc.model';
import { CatRequestModel } from '../models/cat-request.model';
import { HttpParams } from '@angular/common/http';
import { DocumentInfoToUploadModel } from '../models/document-info-to-upload';


describe('Service: DecoteService', () => {

    let baseUrl = environment.baseUrl;
    let documentTempService={
        saveTempDocument: jest.fn().mockReturnValue(of('url'))
    }
    describe('Test: loadCatAddRequests', () => {
        it('should return list of CatRequestWaiting object', done => {

            const response = [];

            const httpMock = {
                get: jest.fn().mockReturnValue(of(response))
            };
          
            const serviceMock = new CatalogAddRequestService(httpMock as any,documentTempService as any);
            serviceMock.loadCatAddRequests().subscribe((data) => {
                expect(httpMock.get).toBeDefined();
                expect(httpMock.get).toHaveBeenCalled();
                expect(data).toEqual(response);


                done();
            });
        });
    });

    describe('Test: loadCatRequestDocs', () => {
        it('should return list of CatRequestDoc object', done => {

            const response = [];

            const httpMock = {
                get: jest.fn().mockReturnValue(of(response))
            };
            const serviceMock = new CatalogAddRequestService(httpMock as any,documentTempService as any);
            serviceMock.loadCatRequestDocs('15').subscribe((data) => {
                expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/requests/docs/15`);
                expect(httpMock.get).toBeDefined();
                expect(httpMock.get).toHaveBeenCalled();
                expect(data).toEqual(response);


                done();
            });
        });
    });


    describe('Test: saveCatRequestDocument', () => {
        it('should return CatRequestDoc object', done => {

            const response: CatRequestDoc = {} as CatRequestDoc;

            const httpMock = {
                post: jest.fn().mockReturnValue(of(response))
            };
            const serviceMock = new CatalogAddRequestService(httpMock as any,documentTempService as any);
            let params = new HttpParams();
                      let catRequest: CatRequestModel = {
                id: '1',
                catActivitySector: {
                    id: '1'
                },
                catNature: {
                    id: '1'
                },
                catBrand: {
                    id: '1'
                }
            } as CatRequestModel;
            const myBlob = new Blob([`/9j/4AAQSkZJRgABAQEBLAEsAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/
            2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/`], { type: 'image/jpeg' });  
            let fileBase64="data:application/octet-stream;base64,LzlqLzRBQVFTa1pKUmdBQkFRRUJMQUVzQUFELzJ3QkRBQVlFQlFZRkJBWUdCUVlIQndZSUNoQUtDZ2tKQ2hRT0R3d1FGeFFZR0JjVUZoWWFIU1VmR2hzakhCWVdJQ3dnSXlZbktTb3BHUjh0TUMwb01DVW9LU2ovCiAgICAgICAgICAgIDJ3QkRBUWNIQndvSUNoTUtDaE1vR2hZYUtDZ29LQ2dvS0Nnb0tDZ29LQ2dvS0Nnb0tDZ29LQ2dvS0Nnb0tDZ29LQ2dvS0Nnb0tDZ29LQ2dvS0Nnb0tDZ29LQ2ov"
           
            params=params.append('catActivitySectorId', catRequest.catActivitySector.id);
            params=params.append('catNatureId', catRequest.catNature.id);
            params=params.append('catBrandId', catRequest.catBrand ? catRequest.catBrand.id : null);
            params=params.append('idCatRequest', catRequest.id);
            let documentInfoToUpload = {
                "fileName": "name",
                "fileUrl": 'url'
              } as DocumentInfoToUploadModel;           

            serviceMock.saveCatRequestDocument(new File([myBlob], "name"), catRequest).subscribe((data) => {
                expect(httpMock.post).toBeDefined();
                expect(httpMock.post).toHaveBeenCalled();
                expect(data).toEqual(response);



                done();
            });
        });
    });

    describe('Test: saveCatRequest', () => {
        it('should return CatRequestModel objet', done => {
            const request: CatRequestModel = {} as CatRequestModel;

            const response: CatRequestModel = {} as CatRequestModel;

            const httpMock = {
                post: jest.fn().mockReturnValue(of(response))
            };
            const serviceMock = new CatalogAddRequestService(httpMock as any,documentTempService as any);
            serviceMock.saveCatRequest(request).subscribe((data) => {
                expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/requests/save`,request);
                expect(httpMock.post).toBeDefined();
                expect(httpMock.post).toHaveBeenCalled();
                expect(data).toEqual(response);


                done();
            });
        });
    });
});