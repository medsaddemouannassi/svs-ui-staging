import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { from, Observable, throwError } from 'rxjs';
import { catchError, finalize, mergeMap, tap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { DocumentConstant } from '../document-constant';
import { DocumentInfoToUploadModel } from '../models/document-info-to-upload';
import { EntrepriseInfoModel } from '../models/entreprise-info.model';
import { ValuationRequestModel } from '../models/valuation-request.model';
import { LoadingService } from './loading-service.service';
import { DocumentTempService } from './document-temp.service';
import { ValuationDocumentModel } from '../models/valuationDocument.model';
import { generateFileName } from '../fileUtils';

@Injectable({
  providedIn: 'root'
})
export class ValuationRequestDocsService {

  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient, private loadingService: LoadingService, private documentTempService: DocumentTempService) { }




  uploadRequestDocument(file: File, valuationRequest: ValuationRequestModel, clientInfo: EntrepriseInfoModel, updateSimulStatus?: boolean): Observable<any> {
    let fileName = generateFileName(file.name);
    const source = from(this.documentTempService.saveTempDocument(file, fileName));

    return source.pipe(mergeMap(data => {
      this.showLoading();
      let params = new HttpParams();
      params = params.append('valuationRequestId', valuationRequest.id);
      params = params.append('cpNumber', clientInfo.cp);
      params = params.append('phase', DocumentConstant.PHASE_MEP);
      params = params.append('family', DocumentConstant.FAMILY_VALUATION);
      params = params.append('type', DocumentConstant.TYPE_ENVIRONMENTAL_VALUATION);
      params = params.append('subType', DocumentConstant.SUB_TYPE_OTHER_VALUATION);
      params = params.append('isQualified', 'false');
      params = params.append('updateSimulStatus', updateSimulStatus ? updateSimulStatus.toString() : 'false');
      let documentInfoToUpload = {
        "fileName": file.name,
        "fileUrl": 'temp/' + fileName
      } as DocumentInfoToUploadModel;
      return this.http.post<any>(`${this.baseUrl}/rest/v1/valuation-request-docs`, documentInfoToUpload, { params: params })
        .pipe(finalize(() => this.loadingService.hide()))

    }));



  }


  private showLoading() {
    this.loadingService.show();

  }


  private hideLoading() {
    this.loadingService.hide();

  }

  uploadLitigationtDocument(file: File, valuationRequest: ValuationRequestModel, clientInfo: EntrepriseInfoModel, updateSimulStatus?: boolean): Observable<any> {
    let fileName = generateFileName(file.name);
    const source = from(this.documentTempService.saveTempDocument(file, fileName));

    return source.pipe(mergeMap(data => {
      this.showLoading();


      let params = new HttpParams();
      params = params.append('valuationRequestId', valuationRequest.id);
      params = params.append('cpNumber', clientInfo.cp);
      params = params.append('phase', DocumentConstant.PHASE_LITIGATION);
      params = params.append('family', DocumentConstant.FAMILY_VALUATION);
      params = params.append('type', DocumentConstant.TYPE_VALUATION);
      params = params.append('subType', DocumentConstant.SUB_TYPE_CONTENTIEUX);
      params = params.append('isQualified', 'true');
      params = params.append('updateSimulStatus', updateSimulStatus ? updateSimulStatus.toString() : 'false');

      let documentInfoToUpload = {
        "fileName": file.name,
        "fileUrl": 'temp/' + fileName
      } as DocumentInfoToUploadModel;

      return this.http.post<any>(`${this.baseUrl}/rest/v1/valuation-request-docs`, documentInfoToUpload, { params: params }).pipe(finalize(() => this.hideLoading()))

    }));



  }



  uploadRequestDocumentFromReception(file: File, valuationRequest: ValuationRequestModel, clientInfo: EntrepriseInfoModel,
    family: string, type: string, subType: string, updateSimulStatus?: boolean): Observable<any> {
    let fileName = generateFileName(file.name);
    const source = from(this.documentTempService.saveTempDocument(file, fileName));

    return source.pipe(mergeMap(data => {

      let params = new HttpParams();
      params = params.append('valuationRequestId', valuationRequest.id);
      params = params.append('cpNumber', clientInfo.cp);
      params = params.append('phase', DocumentConstant.PHASE_MEP);
      params = params.append('family', family);
      params = params.append('type', type);
      params = params.append('updateSimulStatus', updateSimulStatus ? updateSimulStatus.toString() : 'false');
      if (subType == 'E3031') {
        params = params.append('subType', 'E303');
        params = params.append('isQualified', 'false');
      } else {
        params = params.append('subType', subType);
        params = params.append('isQualified', 'true');
      }
      let documentInfoToUpload = {
        "fileName": file.name,
        "fileUrl": 'temp/' + fileName
      } as DocumentInfoToUploadModel;

      this.loadingService.show();

      return this.http.post<any>(`${this.baseUrl}/rest/v1/valuation-request-docs`, documentInfoToUpload, { params: params }).pipe(finalize(() => this.loadingService.hide()));
    }));
  }
  getBssRespDocsByValReqId(valuationRequestId: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/rest/v1/valuation-request-docs/${valuationRequestId}`)
      .pipe(catchError(
        err => {
          console.error('Cannot get valuation document, please contact the application admin...', err);
          return throwError(err);
        }
      )
      );
  }



  getLitigiationDocByValReqId(valuationRequestId: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/rest/v1/valuation-request-docs/contencieux/${valuationRequestId}`)
      .pipe(catchError(
        err => {
          console.error('Cannot get valuation document, please contact the application admin...', err);
          return throwError(err);
        }
      )
      );
  }

  updateBssRespDocuments(valuationRequestDosList: ValuationDocumentModel[]): Observable<any> {
    return this.http.post(`${this.baseUrl}/rest/v1/valuation-request-docs/bss-resp-docs-titles`, valuationRequestDosList)
      .pipe(catchError(
        err => {
          console.error('Cannot get valuation document, please contact the application admin...', err);
          return throwError(err);
        }
      )
      );
  }

  deleteDocuments(valuationRequestDosList: ValuationDocumentModel[]): Observable<any> {
    return this.http.post(`${this.baseUrl}/rest/v1/valuation-request-docs/delete-documents`, valuationRequestDosList)
      .pipe(catchError(
        err => {
          console.error('Cannot delete document, please contact the application admin...', err);
          return throwError(err);
        }
      )
      );
  }


  displayDocument(valuationRequestDosList: ValuationDocumentModel): Observable<any> {
    this.loadingService.show();
    return this.http.post(`${this.baseUrl}/rest/v1/valuation-request-docs/display`, valuationRequestDosList)
      .pipe(catchError(
        err => {
          console.error('Cannot get valuation document, please contact the application admin...', err);
          return throwError(err);
        }
      ), finalize(() => this.loadingService.hide())
      );
  }

  displayDocumentOfValuationByValuationRequestId(valuationRequestId): Observable<any> {
    this.loadingService.show();

    return this.http.get(`${this.baseUrl}/rest/v1/valuation-request-docs/valuation/display/${valuationRequestId}`)
      .pipe(catchError(
        err => {
          console.error('Cannot get valuation document, please contact the application admin...', err);
          return throwError(err);
        }
      ), finalize(() => this.loadingService.hide())
      );
  }
  uploadValuationResult(file: File, valuationRequest: ValuationRequestModel, clientInfo: EntrepriseInfoModel,
    phase: string, updateSimulStatus?: boolean): Observable<any> {
    this.loadingService.show();
    let fileName = generateFileName(file.name);

    const source = from(this.documentTempService.saveTempDocument(file, fileName));

    return source.pipe(mergeMap(data => {

      let params = new HttpParams();
      params = params.append('valuationRequestId', valuationRequest.id);
      params = params.append('cpNumber', clientInfo.cp);
      params = params.append('phase', phase);
      params = params.append('family', DocumentConstant.FAMILY_VALUATION);
      params = params.append('type', DocumentConstant.TYPE_VALUATION);
      params = params.append('subType', DocumentConstant.SUB_TYPE_VALUATION_RESULT);
      params = params.append('isQualified', 'true');
      params = params.append('updateSimulStatus', updateSimulStatus ? updateSimulStatus.toString() : 'false');

      let documentInfoToUpload = {
        "fileName": file.name,
        "fileUrl": 'temp/' + fileName
      } as DocumentInfoToUploadModel;
      return this.http.post<any>(`${this.baseUrl}/rest/v1/valuation-request-docs`, documentInfoToUpload, { params: params }).pipe(finalize(() => this.loadingService.hide()));
    }));
  }

  uploadValuationSimulation(file: File, valuationRequest: ValuationRequestModel, clientInfo: EntrepriseInfoModel, updateSimulStatus?: boolean): Observable<any> {
    this.loadingService.show();
    let fileName = generateFileName(file.name);

    const source = from(this.documentTempService.saveTempDocument(file, fileName));

    return source.pipe(mergeMap(data => {
      let params = new HttpParams();
      params = params.append('valuationRequestId', valuationRequest.id);
      params = params.append('cpNumber', clientInfo.cp);
      params = params.append('phase', DocumentConstant.PHASE_MEP);
      params = params.append('family', DocumentConstant.FAMILY_CONTRACT);
      params = params.append('type', DocumentConstant.TYPE_INTERNAL_CONTRACT);
      params = params.append('subType', DocumentConstant.SUB_TYPE_VALUATION_SIMULATION);
      params = params.append('isQualified', 'true');
      params = params.append('updateSimulStatus', updateSimulStatus ? updateSimulStatus.toString() : 'false');
      let documentInfoToUpload = {
        "fileName": file.name,
        "fileUrl": 'temp/' + fileName
      } as DocumentInfoToUploadModel;
      return this.http.post<any>(`${this.baseUrl}/rest/v1/valuation-request-docs`, documentInfoToUpload, { params: params }).pipe(finalize(() => this.loadingService.hide()));
    }));

  }


  uploadValuationLitigation(file: File, valuationRequest: ValuationRequestModel, clientInfo: EntrepriseInfoModel, updateSimulStatus?: boolean): Observable<any> {
    this.loadingService.show();
    let fileName = generateFileName(file.name);
    const source = from(this.documentTempService.saveTempDocument(file, fileName));

    return source.pipe(mergeMap(data => {

      let params = new HttpParams();
      params = params.append('valuationRequestId', valuationRequest.id);
      params = params.append('cpNumber', clientInfo.cp);
      params = params.append('phase', DocumentConstant.PHASE_LITIGATION);
      params = params.append('family', DocumentConstant.FAMILY_VALUATION);
      params = params.append('type', DocumentConstant.TYPE_VALUATION);
      params = params.append('subType', DocumentConstant.SUB_TYPE_VALUATION_RESULT);
      params = params.append('isQualified', 'true');
      params = params.append('updateSimulStatus', updateSimulStatus ? updateSimulStatus.toString() : 'false');

      let documentInfoToUpload = {
        "fileName": file.name,
        "fileUrl": 'temp/' + fileName
      } as DocumentInfoToUploadModel;
      return this.http.post<any>(`${this.baseUrl}/rest/v1/valuation-request-docs`, documentInfoToUpload, { params: params }).pipe(finalize(() => this.loadingService.hide()));
    }))
  }
}
