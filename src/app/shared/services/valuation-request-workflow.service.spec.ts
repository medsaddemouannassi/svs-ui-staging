import { of, throwError } from 'rxjs';
import { environment } from "../../../environments/environment";
import { ValuationRequestWorkflowModel } from '../models/valuation-request-workflow.model';
import { LoadingService } from './loading-service.service';
import { ValuationRequestWorkflowService } from './valuation-request-workflow.service';

describe('Service: ValuationRequestWorkflowService', () => {

    let baseUrl = environment.baseUrl;

    describe('Test: saveValuationRequestWorkflow', () => {
        it('should return ValuationRequestWorkflowModel object', done => {

            const response = {} as ValuationRequestWorkflowModel;
            const httpMock = {
                post: jest.fn().mockReturnValue(of(response))
            };
           

            const serviceMock = new ValuationRequestWorkflowService(httpMock as any,new LoadingService());
            serviceMock.saveValuationRequestWorkflow({} as ValuationRequestWorkflowModel).subscribe((data) => {
                expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/valuation-request/workflow`, {} as ValuationRequestWorkflowModel);
                expect(data).toEqual(response);
                done();
            });
        });
        it('should return ValuationRequestWorkflowModel object', done => {

            const response = {} as ValuationRequestWorkflowModel;
            const httpMock = {
                post: jest.fn().mockReturnValue(of(response))
            };
            

            const serviceMock = new ValuationRequestWorkflowService(httpMock as any,new LoadingService());
            serviceMock.saveValuationRequestWorkflow({} as ValuationRequestWorkflowModel).subscribe((data) => {
                expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/valuation-request/workflow`, {} as ValuationRequestWorkflowModel);
                expect(data).toEqual(response);
                done();
            });
        });

        it('should saveValuationRequestWorkflow throw error', (done => {
            const httpMock = {
                post: jest.fn().mockImplementation(() => {
                    return throwError(new Error('my error message'));
                })
            };
           

            const serviceMock = new ValuationRequestWorkflowService(httpMock as any,new LoadingService());
            serviceMock.saveValuationRequestWorkflow({} as ValuationRequestWorkflowModel).subscribe((data) => { }, () => {
                expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/valuation-request/workflow`, {} as ValuationRequestWorkflowModel);
                expect(httpMock.post).toBeDefined();
                expect(httpMock.post).toHaveBeenCalled();
                done();
            });
        }));
        it('should throw error getValuationRequestStatus', (done => {
            const httpMock = {
                get: jest.fn().mockImplementation(() => {
                    return throwError(new Error('my error message'));
                })
            };
        

            const serviceMock = new ValuationRequestWorkflowService(httpMock as any,new LoadingService());
             serviceMock.getValuationRequestStatus("1").subscribe((data) => { }, () => {
                expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/valuation-request/workflow/1`);
                expect(httpMock.get).toBeDefined();
                expect(httpMock.get).toHaveBeenCalled();
                done();
            });
        }));
        it('should throw error getValuationRequestHistory', (done => {
            const httpMock = {
                get: jest.fn().mockImplementation(() => {
                    return throwError(new Error('my error message'));
                })
            };
           

            const serviceMock = new ValuationRequestWorkflowService(httpMock as any,new LoadingService()); 
            serviceMock.getValuationRequestHistory("1").subscribe((data) => { }, () => {
                expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/valuation-request/workflow/history/1`);
                expect(httpMock.get).toBeDefined();
                expect(httpMock.get).toHaveBeenCalled();
                done();
            });
        }));
    });


    describe('Test: getValuationRequestStatus', () => {
        it('should return ValuationRequestWorkflowModel object', done => {
            let svsDescList: ValuationRequestWorkflowModel = {} as ValuationRequestWorkflowModel;

            const httpMock = {
                get: jest.fn().mockReturnValue(of(svsDescList))
            };
          

            const serviceMock = new ValuationRequestWorkflowService(httpMock as any,new LoadingService());
            serviceMock.getValuationRequestStatus('1').subscribe((data) => {
                expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/valuation-request/workflow/1`);
                expect(httpMock.get).toBeDefined();
                expect(httpMock.get).toHaveBeenCalled();
                expect(data).toBeTruthy();
                done();
            });
        });

        it('should getValuationRequestStatus throw error', (done => {
            const httpMock = {
                get: jest.fn().mockImplementation(() => {
                    return throwError(new Error('my error message'));
                })
            };
           

            const serviceMock = new ValuationRequestWorkflowService(httpMock as any,new LoadingService());
            serviceMock.getValuationRequestStatus('1').subscribe((data) => { }, () => {
                expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/valuation-request/workflow/1`);
                expect(httpMock.get).toBeDefined();
                expect(httpMock.get).toHaveBeenCalled();
                done();
            });
        }));
    });
      describe('Test: getValuationRequestStatus', () => {
        it('should return ValuationRequestWorkflowModel object', done => {
            let svsDescList: ValuationRequestWorkflowModel = {} as ValuationRequestWorkflowModel;

            const httpMock = {
                get: jest.fn().mockReturnValue(of(svsDescList))
            };
            

            const serviceMock = new ValuationRequestWorkflowService(httpMock as any,new LoadingService());
            serviceMock.assigneValuationRequest('1','2',true).subscribe((data) => {
                expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/valuation-request/workflow/assigne/1/2/true`);
                expect(httpMock.get).toBeDefined();
                expect(httpMock.get).toHaveBeenCalled();
                expect(data).toBeTruthy();
                done();
            });
        });

        it('should getValuationRequestStatus throw error', (done => {
            const httpMock = {
                get: jest.fn().mockImplementation(() => {
                    return throwError(new Error('my error message'));
                })
            };
           

            const serviceMock = new ValuationRequestWorkflowService(httpMock as any,new LoadingService());
            serviceMock.assigneValuationRequest('1','2',true).subscribe((data) => { }, () => {
                expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/valuation-request/workflow/assigne/1/2/true`);
                expect(httpMock.get).toBeDefined();
                expect(httpMock.get).toHaveBeenCalled();
                done();
            });
        }));
    });

});
