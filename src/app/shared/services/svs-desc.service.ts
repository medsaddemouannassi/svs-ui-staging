import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { MdmInformationsModel } from '../models/mdm-informations.model';
import { SvsDescModel } from '../models/svs-desc.model';

@Injectable({
  providedIn: 'root'
})
export class SvsDescService {

  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }

  getSvsDescList(): Observable<SvsDescModel[]> {
    return this.http.get<SvsDescModel[]>(`${this.baseUrl}/rest/v1/svs-desc`)
    
  }
  submitSvsDesc(svsDesc: SvsDescModel): Observable<SvsDescModel> {
    return this.http.post<SvsDescModel>(`${this.baseUrl}/rest/v1/svs-desc`, svsDesc)
  
  }
  removeSvsDesc(svsDesc: SvsDescModel): Observable<SvsDescModel> {
    return this.http.post<SvsDescModel>(`${this.baseUrl}/rest/v1/svs-desc/remove`, svsDesc);
  }

  getAvailableSvsDesc(): Observable<SvsDescModel[]> {
    return this.http.get<SvsDescModel[]>(`${this.baseUrl}/rest/v1/svs-desc/available-svs-desc`);
  }
  getDescManagerByQuery(query: string): Observable<MdmInformationsModel[]> {
    return this.http.get<MdmInformationsModel[]>(`${this.baseUrl}/rest/v1/svs-desc/manager-list/${query}`);
  }

  getSvsNetworksListByManagerId(id: string): Observable<SvsDescModel[]> {
    return this.http.get<SvsDescModel[]>(`${this.baseUrl}/rest/v1/svs-desc/manager/${id}`);
  }

}
