import { HttpParams } from '@angular/common/http';
import { of, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CatAssetTypeModel } from '../models/cat-asset-type.model';
import { CatAssetTypeService } from './cat-asset-type.service';


describe('Service: CatActivitySectorService', () => {

    let baseUrl = environment.baseUrl;

    describe('Test: getAssetTypeById', () => {
        it('should return  object', done => {


            const response = { id: '1' } as CatAssetTypeModel;

            const httpMock = {
                get: jest.fn().mockReturnValue(of(response))
            };
            const serviceMock = new CatAssetTypeService(httpMock as any);
            serviceMock.getAssetTypeById('1').subscribe((data) => {
                expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/asset-types/1`);
                expect(httpMock.get).toBeDefined();
                expect(httpMock.get).toHaveBeenCalled();
                expect(data).toEqual(response);
                done();
            });
        });

    });

    describe('Test: getAssetTypeByCatReqstId', () => {
        it('should return  object', done => {


            const response = { id: '1' } as CatAssetTypeModel;

            const httpMock = {
                get: jest.fn().mockReturnValue(of(response))
            };
            const serviceMock = new CatAssetTypeService(httpMock as any);
            serviceMock.getAssetTypeByCatReqstId('1').subscribe((data) => {
                expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/asset-types/cat-request/1`);
                expect(httpMock.get).toBeDefined();
                expect(httpMock.get).toHaveBeenCalled();
                expect(data).toEqual(response);
                done();
            });
        });

    });
    describe('Test: saveAssetType', () => {
        it('should return  object', done => {


            const response: CatAssetTypeModel = {} as CatAssetTypeModel;


            const httpMock = {
                post: jest.fn().mockReturnValue(of(response))

            };
            const serviceMock = new CatAssetTypeService(httpMock as any);
            serviceMock.saveAssetType({} as CatAssetTypeModel).subscribe((data) => {
                expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/asset-types`, {});
                expect(httpMock.post).toBeDefined();
                expect(httpMock.post).toHaveBeenCalled();
                expect(data).toEqual(response);
                done();
            });
        });

        it('should saveAssetType throws error', (done => {

            const httpMock = {
                post: jest.fn().mockImplementation(() => {
                    return throwError(new Error('my error message'));
                })
            };
            const serviceMock = new CatAssetTypeService(httpMock as any);
            serviceMock.saveAssetType({} as CatAssetTypeModel).subscribe((respnse) => {
            //this is intentional
            }, () => {
                expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/asset-types`, {});
                expect(httpMock.post).toBeDefined();
                expect(httpMock.post).toHaveBeenCalled();

                done();

            });
        }));

    });

    describe('Test: deleteAssetType', () => {
        it('should delete  object', done => {

            const assetTypeID = '1';


            const httpMock = {
                post: jest.fn().mockReturnValue(of(true))

            };
            const serviceMock = new CatAssetTypeService(httpMock as any);
            serviceMock.deleteAssetType(assetTypeID).subscribe((data) => {
                expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/asset-types/delete`, assetTypeID);
                expect(httpMock.post).toBeDefined();
                expect(httpMock.post).toHaveBeenCalled();
                done();
            });
        });

        it('should deleteAssetType throws error', (done => {

            const httpMock = {
                post: jest.fn().mockImplementation(() => {
                    return throwError(new Error('my error message'));
                })
            };
            const serviceMock = new CatAssetTypeService(httpMock as any);
            serviceMock.deleteAssetType('1').subscribe((respnse) => {
            //this is intentional
            }, () => {
                expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/asset-types/delete`, '1');
                expect(httpMock.post).toBeDefined();
                expect(httpMock.post).toHaveBeenCalled();

                done();

            });
        }));
    });

    describe('Test: getAssetTypeListByCatBrandId', () => {
        it('should return  Object', done => {


            const response: CatAssetTypeModel[] = [];

            const httpMock = {
                get: jest.fn().mockReturnValue(of(response))
            };
            const serviceMock = new CatAssetTypeService(httpMock as any);
            serviceMock.getAssetTypeListByCatBrandId("1").subscribe((data) => {
                expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/asset-types/brands/1`);
                expect(httpMock.get).toBeDefined();
                expect(httpMock.get).toHaveBeenCalled();
                expect(data).toEqual(response);
                done();
            });
        });

    });


    describe('Test: loadAssetTypeListByQuery', () => {
        it('should return  object', done => {
            const response =["typeLabel1", "TypeLabel2", "N.S"];
            const httpMock = {
                get: jest.fn().mockReturnValue(of(response))
            };
            const serviceMock = new CatAssetTypeService(httpMock as any);
            let params = new HttpParams();
            params = params.append('sector', 'sector');
            params = params.append('nature', 'nature');
            params = params.append('brand', 'brand');
            params = params.append('type', 'type');
            serviceMock.loadAssetTypeListByQuery('sector', 'nature', 'brand', 'type').subscribe((data) => {
                expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/asset-types/search/label`,{params:params});
                expect(httpMock.get).toBeDefined();
                expect(httpMock.get).toHaveBeenCalled();
                expect(data).toEqual(response);
                done();
            });
        });
    });


    describe('Test: loadAssetTypeListByLabel', () => {
        it('should return  object', done => {
            const response =["typeLabel1", "TypeLabel2", "N.S"];
            const httpMock = {
                get: jest.fn().mockReturnValue(of(response))
            };
            const serviceMock = new CatAssetTypeService(httpMock as any);
            let params = new HttpParams();

            params = params.append('type', 'type');
            serviceMock.loadAssetTypeListByLabel('type').subscribe((data) => {
                expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/asset-types/search/type/label`,{params:params});
                expect(httpMock.get).toBeDefined();
                expect(httpMock.get).toHaveBeenCalled();
                expect(data).toEqual(response);
                done();
            });
        });
    });

});
