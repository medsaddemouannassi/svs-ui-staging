import { of, throwError } from 'rxjs';
import { CatEnergyNatureModel } from '../models/cat-energy-nature.model';
import { environment } from 'src/environments/environment';
import { CatEnergyService } from './cat-energy.service';

describe('CatEnergyService', () => {
  let baseUrl = environment.baseUrl;

  describe('Test: getCatEnergyByNatureId', () => {
    it('should return list of CatEnergyNatureModel', done => {
      const response: CatEnergyNatureModel[] = [];
      const httpMock = {
        get: jest.fn().mockReturnValue(of(response))
      };
      const serviceMock = new CatEnergyService(httpMock as any);
      serviceMock.getCatEnergyByNatureId('1').subscribe(data => {
        expect(httpMock.get).toHaveBeenCalledWith(
          `${baseUrl}/rest/v1/cat/cat-energy-nature/nature/1`
        );
        expect(httpMock.get).toBeDefined();
        expect(httpMock.get).toHaveBeenCalled();
        expect(data).toEqual(response);
        done();
      });
    });
  });

  describe('Test: getCatEnergyByBrandId', () => {
    it('should return list of CatEnergyNatureModel', done => {
      const response: CatEnergyNatureModel[] = [];
      const httpMock = {
        get: jest.fn().mockReturnValue(of(response))
      };
      const serviceMock = new CatEnergyService(httpMock as any);
      serviceMock.getCatEnergyByBrandId('1').subscribe(data => {
        expect(httpMock.get).toHaveBeenCalledWith(
          `${baseUrl}/rest/v1/cat/cat-energy-nature/brand/1`
        );
        expect(httpMock.get).toBeDefined();
        expect(httpMock.get).toHaveBeenCalled();
        expect(data).toEqual(response);
        done();
      });
    });
  });


  describe('Test: saveCatEnergy', () => {
    it('should return new list of CatEnergyNatureModel ', done => {
      const response: CatEnergyNatureModel[] = [];
      const listToSave: CatEnergyNatureModel[] = [];
      const httpMock = {
        post: jest.fn().mockReturnValue(of(response))
      };
      const serviceMock = new CatEnergyService(httpMock as any);
      serviceMock.saveCatEnergy(listToSave).subscribe(data => {
        expect(httpMock.post).toHaveBeenCalledWith(
          `${baseUrl}/rest/v1/cat/cat-energy-nature/`,
          listToSave
        );
        expect(httpMock.post).toBeDefined();
        expect(httpMock.post).toHaveBeenCalled();
        expect(data).toEqual(response);
        done();
      });
    });

    it('should saveCatEnergy throw error', (done => {
      const httpMock = {
          post: jest.fn().mockImplementation(() => {
              return throwError(new Error('my error message'));
          })
      };
      const serviceMock = new CatEnergyService(httpMock as any);
      const listToSave: CatEnergyNatureModel[] = [];

      serviceMock.saveCatEnergy(listToSave).subscribe((data) => {
         // This is intentional

      }, () => {
          expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/cat-energy-nature/`,
          listToSave);
          expect(httpMock.post).toBeDefined();
          expect(httpMock.post).toHaveBeenCalled();

          done();

      });
  }));

  });

  describe('Test: deleteValuationDiscounts', () => {
		it('should delete valuation discounts', done => {
			let catEnergies = [{} as CatEnergyNatureModel, {} as CatEnergyNatureModel];


			const response = "";

			const httpMock = {
				post: jest.fn().mockReturnValue(of(response))
			};
			const serviceMock = new CatEnergyService(httpMock as any);
			serviceMock.deleteCatEnergy(catEnergies).subscribe((data) => {
				expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/cat-energy-nature/remove-energy-nature`, catEnergies);
				expect(httpMock.post).toBeDefined();
				expect(httpMock.post).toHaveBeenCalled();
				done();
			});
    });

		it('should deleteCatEnergy throw error', (done => {
			let catEnergies = [{} as CatEnergyNatureModel, {} as CatEnergyNatureModel];

			const httpMock = {
				post: jest.fn().mockImplementation(() => {
					return throwError(new Error('my error message'));
				})
			};
			const serviceMock = new CatEnergyService(httpMock as any);
			serviceMock.deleteCatEnergy(catEnergies).subscribe((data) => {
			//this is intentional
			}, () => {
				expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/cat-energy-nature/remove-energy-nature`, catEnergies);
				expect(httpMock.post).toBeDefined();
				expect(httpMock.post).toHaveBeenCalled();

				done();

			});
		}));

  });

});
