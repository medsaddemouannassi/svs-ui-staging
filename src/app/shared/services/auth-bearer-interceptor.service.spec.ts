import {of, throwError} from 'rxjs';
import {HttpRequest, HttpResponse} from '@angular/common/http';
import {AuthBearerInterceptor} from './auth-bearer-interceptor.service';


describe('Service: DocumentTempService', () => {
  let service: AuthBearerInterceptor;
  let authStorage: {} = jest.fn();
  const errorHandler = jest.fn();
  let moduleConfig: {} = jest.fn();

  beforeEach(() => {
    authStorage = {
      getItem: jest.fn()
    };
    moduleConfig = {
      resourceServer: {
        allowedUrls: jest.fn(),
        sendAccessToken: jest.fn()
      }
    };
    service = new AuthBearerInterceptor(authStorage as any, errorHandler as any, moduleConfig as any);
  });


  describe('Test: checkUrl', () => {
    it('should intercept request', done => {
      const mockHandler = {
        handle: jest.fn(() =>  of(new HttpResponse({status: 200, body: {data: 'thisIsWhatImTesting'}})))
      };

      const requestMock = new HttpRequest('GET', '/test');

      // @ts-ignore
      const myPrivateFunc = jest.spyOn(service, 'checkUrl'). mockReturnValue(false);
      service.intercept(requestMock, mockHandler);
      expect(myPrivateFunc).toHaveBeenCalled();
      expect(mockHandler.handle).toHaveBeenCalled();
      done();
    });

    it('should intercept request', done => {
      const mockHandler = {
        handle: jest.fn(() =>  of(new HttpResponse({status: 200, body: {data: 'thisIsWhatImTesting'}})))
      };

      const requestMock = new HttpRequest('GET', '/test');
      // @ts-ignore
      const myPrivateFunc = jest.spyOn(service, 'checkUrl').mockReturnValue(true);
      service.intercept(requestMock, mockHandler);
      expect(myPrivateFunc).toHaveBeenCalled();
      expect(mockHandler.handle).toHaveBeenCalled();
      done();
    });
    });

});
