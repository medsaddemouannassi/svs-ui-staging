import { HttpParams } from '@angular/common/http';
import { of, throwError } from 'rxjs';
import { ValuationDocumentModel } from 'src/app/shared/models/valuationDocument.model';
import { environment } from '../../../environments/environment';
import { DocumentConstant } from '../document-constant';
import { DocumentInfoToUploadModel } from '../models/document-info-to-upload';
import { EntrepriseInfoModel } from '../models/entreprise-info.model';
import { ValuationRequestModel } from '../models/valuation-request.model';
import { LoadingService } from './loading-service.service';
import { ValuationRequestDocsService } from './valuation-request-docs.service';



describe('Service: ValuationRequestDocsService', () => {

  let baseUrl = environment.baseUrl;
  let documentTempService = {
    saveTempDocument: jest.fn().mockReturnValue(of('url'))
  }
  describe('Test: uploadRequestDocument', () => {
    it('should upload document from request', done => {

      const response = "url" ;

      const httpMock = {
        post: jest.fn().mockReturnValue(of(response))
      };
      const serviceMock = new ValuationRequestDocsService(httpMock as any, new LoadingService(), documentTempService as any);
      let params = new HttpParams();
      params = params.append('valuationRequestId', '2');
      params = params.append('cpNumber', '50');
      params = params.append('phase', DocumentConstant.PHASE_MEP);
      params = params.append('family', DocumentConstant.FAMILY_VALUATION);
      params = params.append('type', DocumentConstant.TYPE_ENVIRONMENTAL_VALUATION);
      params = params.append('subType', DocumentConstant.SUB_TYPE_OTHER_VALUATION);
      params = params.append('isQualified', 'false');
      params = params.append('updateSimulStatus', 'false');

      const myBlob = new Blob([`/9j/4AAQSkZJRgABAQEBLAEsAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/
      2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/`], { type: 'image/jpeg' });
      let fileBase64 = "data:application/octet-stream;base64,LzlqLzRBQVFTa1pKUmdBQkFRRUJMQUVzQUFELzJ3QkRBQVlFQlFZRkJBWUdCUVlIQndZSUNoQUtDZ2tKQ2hRT0R3d1FGeFFZR0JjVUZoWWFIU1VmR2hzakhCWVdJQ3dnSXlZbktTb3BHUjh0TUMwb01DVW9LU2ovCiAgICAgIDJ3QkRBUWNIQndvSUNoTUtDaE1vR2hZYUtDZ29LQ2dvS0Nnb0tDZ29LQ2dvS0Nnb0tDZ29LQ2dvS0Nnb0tDZ29LQ2dvS0Nnb0tDZ29LQ2dvS0Nnb0tDZ29LQ2ov"

      let documentInfoToUpload = {
        "fileName": "name",
        "fileUrl": 'url'
      } as DocumentInfoToUploadModel;


      serviceMock.uploadRequestDocument(new File([myBlob], "name"), { id: '2' } as ValuationRequestModel, { cp: '50' } as EntrepriseInfoModel).subscribe((data) => {
        expect(httpMock.post).toBeDefined();

        expect(data).toEqual(response);
        done();
      });
    });
  });

  describe('Test: uploadLitigationtDocument', () => {
    it('should upload contencieux document from request', done => {

      const response = "url" ;

      const httpMock = {
        post: jest.fn().mockReturnValue(of(response))
      };
      const serviceMock = new ValuationRequestDocsService(httpMock as any, new LoadingService(), documentTempService as any);
      let params = new HttpParams();
      params = params.append('valuationRequestId', '2');
      params = params.append('cpNumber', '50');
      params = params.append('phase', DocumentConstant.PHASE_LITIGATION);
      params = params.append('family', DocumentConstant.FAMILY_VALUATION);
      params = params.append('type', DocumentConstant.TYPE_VALUATION);
      params = params.append('subType', DocumentConstant.SUB_TYPE_CONTENTIEUX);
      params = params.append('isQualified', 'true');
      params = params.append('updateSimulStatus', 'false');

      const myBlob = new Blob([`/9j/4AAQSkZJRgABAQEBLAEsAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/
      2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/`], { type: 'image/jpeg' });
      let fileBase64 = "data:application/octet-stream;base64,LzlqLzRBQVFTa1pKUmdBQkFRRUJMQUVzQUFELzJ3QkRBQVlFQlFZRkJBWUdCUVlIQndZSUNoQUtDZ2tKQ2hRT0R3d1FGeFFZR0JjVUZoWWFIU1VmR2hzakhCWVdJQ3dnSXlZbktTb3BHUjh0TUMwb01DVW9LU2ovCiAgICAgIDJ3QkRBUWNIQndvSUNoTUtDaE1vR2hZYUtDZ29LQ2dvS0Nnb0tDZ29LQ2dvS0Nnb0tDZ29LQ2dvS0Nnb0tDZ29LQ2dvS0Nnb0tDZ29LQ2dvS0Nnb0tDZ29LQ2ov"

      let documentInfoToUpload = {
        "fileName": "name",
        "fileUrl": 'url'
      } as DocumentInfoToUploadModel;


      serviceMock.uploadLitigationtDocument(new File([myBlob], "name"), { id: '2' } as ValuationRequestModel, { cp: '50' } as EntrepriseInfoModel).subscribe((data) => {
        expect(httpMock.post).toBeDefined();

        expect(data).toEqual(response);
        done();
      });
    });
  });
  describe('Test: uploadRequestDocumentFromReception', () => {
    it('should upload document from reception', done => {

      const response = "url" ;

      const httpMock = {
        post: jest.fn().mockReturnValue(of(response))
      };
      const serviceMock = new ValuationRequestDocsService(httpMock as any, new LoadingService(), documentTempService as any);
      let params = new HttpParams();
      params = params.append('valuationRequestId', '2');
      params = params.append('cpNumber', '50');
      params = params.append('phase', DocumentConstant.PHASE_MEP);
      params = params.append('family', DocumentConstant.FAMILY_VALUATION);
      params = params.append('type', DocumentConstant.TYPE_ENVIRONMENTAL_VALUATION);
      params = params.append('updateSimulStatus', 'false');
      params = params.append('subType', DocumentConstant.SUB_TYPE_OTHER_VALUATION);
      params = params.append('isQualified', 'true');

      const myBlob = new Blob([`/9j/4AAQSkZJRgABAQEBLAEsAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/
      2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/`], { type: 'image/jpeg' });
      let fileBase64 = "data:application/octet-stream;base64,LzlqLzRBQVFTa1pKUmdBQkFRRUJMQUVzQUFELzJ3QkRBQVlFQlFZRkJBWUdCUVlIQndZSUNoQUtDZ2tKQ2hRT0R3d1FGeFFZR0JjVUZoWWFIU1VmR2hzakhCWVdJQ3dnSXlZbktTb3BHUjh0TUMwb01DVW9LU2ovCiAgICAgIDJ3QkRBUWNIQndvSUNoTUtDaE1vR2hZYUtDZ29LQ2dvS0Nnb0tDZ29LQ2dvS0Nnb0tDZ29LQ2dvS0Nnb0tDZ29LQ2dvS0Nnb0tDZ29LQ2dvS0Nnb0tDZ29LQ2ov"
      let documentInfoToUpload = {
        "fileName": "name",
        "fileUrl": 'url'
      } as DocumentInfoToUploadModel;
      serviceMock.uploadRequestDocumentFromReception(new File([myBlob], "name"), { id: '2' } as ValuationRequestModel, { cp: '50' } as EntrepriseInfoModel, DocumentConstant.FAMILY_VALUATION, DocumentConstant.TYPE_ENVIRONMENTAL_VALUATION, DocumentConstant.SUB_TYPE_OTHER_VALUATION, false).subscribe((data) => {
        expect(httpMock.post).toBeDefined();

        expect(data).toEqual(response);
        done();
      });
    });
  });

  describe('Test: uploadValuationResult', () => {
    it('should upload result PDF', done => {

      const response = "url" ;

      const httpMock = {
        post: jest.fn().mockReturnValue(of(response))
      };
      const serviceMock = new ValuationRequestDocsService(httpMock as any, new LoadingService(),documentTempService as any);
      let params = new HttpParams();
      params = params.append('valuationRequestId', '2');
      params = params.append('cpNumber', '50');
      params = params.append('phase', DocumentConstant.PHASE_MEP);
      params = params.append('family', DocumentConstant.FAMILY_VALUATION);
      params = params.append('type', DocumentConstant.TYPE_VALUATION);
      params = params.append('subType', DocumentConstant.SUB_TYPE_VALUATION_RESULT);
      params = params.append('isQualified', 'true');
      params = params.append('updateSimulStatus', 'false');

      const myBlob = new Blob([`/9j/4AAQSkZJRgABAQEBLAEsAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/
      2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/`], { type: 'image/jpeg' });
      let fileBase64 = "data:application/octet-stream;base64,LzlqLzRBQVFTa1pKUmdBQkFRRUJMQUVzQUFELzJ3QkRBQVlFQlFZRkJBWUdCUVlIQndZSUNoQUtDZ2tKQ2hRT0R3d1FGeFFZR0JjVUZoWWFIU1VmR2hzakhCWVdJQ3dnSXlZbktTb3BHUjh0TUMwb01DVW9LU2ovCiAgICAgIDJ3QkRBUWNIQndvSUNoTUtDaE1vR2hZYUtDZ29LQ2dvS0Nnb0tDZ29LQ2dvS0Nnb0tDZ29LQ2dvS0Nnb0tDZ29LQ2dvS0Nnb0tDZ29LQ2dvS0Nnb0tDZ29LQ2ov"

      let documentInfoToUpload = {
        "fileName": "name",
        "fileUrl": 'url'
      } as DocumentInfoToUploadModel;
      serviceMock.uploadValuationResult(new File([myBlob], "name"), { id: '2' } as ValuationRequestModel, { cp: '50' } as EntrepriseInfoModel,'phase',true).subscribe((data) => {
        expect(httpMock.post).toBeDefined();

        expect(data).toEqual(response);
        done();
      });
    });
  });
  describe('Test: uploadValuationSimulation', () => {
    it('should upload result PDF', done => {

      const response = "url" ;

      const httpMock = {
        post: jest.fn().mockReturnValue(of(response))
      };
      const serviceMock = new ValuationRequestDocsService(httpMock as any, new LoadingService(),documentTempService as any);
      let params = new HttpParams();
      params = params.append('valuationRequestId', '2');
      params = params.append('cpNumber', '50');
      params = params.append('phase', DocumentConstant.PHASE_MEP);
      params = params.append('family', DocumentConstant.FAMILY_CONTRACT);
      params = params.append('type', DocumentConstant.TYPE_INTERNAL_CONTRACT);
      params = params.append('subType', DocumentConstant.SUB_TYPE_VALUATION_SIMULATION);
      params = params.append('isQualified', 'true');
      params = params.append('updateSimulStatus', 'false');

      const myBlob = new Blob([`/9j/4AAQSkZJRgABAQEBLAEsAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/
      2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/`], { type: 'image/jpeg' });
      let fileBase64 = "data:application/octet-stream;base64,LzlqLzRBQVFTa1pKUmdBQkFRRUJMQUVzQUFELzJ3QkRBQVlFQlFZRkJBWUdCUVlIQndZSUNoQUtDZ2tKQ2hRT0R3d1FGeFFZR0JjVUZoWWFIU1VmR2hzakhCWVdJQ3dnSXlZbktTb3BHUjh0TUMwb01DVW9LU2ovCiAgICAgIDJ3QkRBUWNIQndvSUNoTUtDaE1vR2hZYUtDZ29LQ2dvS0Nnb0tDZ29LQ2dvS0Nnb0tDZ29LQ2dvS0Nnb0tDZ29LQ2dvS0Nnb0tDZ29LQ2dvS0Nnb0tDZ29LQ2ov"

      let documentInfoToUpload = {
        "fileName": "name",
        "fileUrl": 'url'
      } as DocumentInfoToUploadModel;
      serviceMock.uploadValuationSimulation(new File([myBlob], "name"), { id: '2' } as ValuationRequestModel, { cp: '50' } as EntrepriseInfoModel).subscribe((data) => {
        expect(httpMock.post).toBeDefined();

        expect(data).toEqual(response);
        done();
      });
    });
  });
  describe('Test: getBssRespDocsByValReqId', () => {
    it('should return  Valuation documents', done => {
      let valuationDocumentList: ValuationDocumentModel[];

      const response: ValuationDocumentModel[] = [];


      const httpMock = {
        get: jest.fn().mockReturnValue(of(response))
      };
      const serviceMock = new ValuationRequestDocsService(httpMock as any, new LoadingService(),documentTempService as any);
      serviceMock.getBssRespDocsByValReqId('1').subscribe((data) => {
        expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/valuation-request-docs/1`);
        expect(httpMock.get).toBeDefined();
        expect(httpMock.get).toHaveBeenCalled();
        expect(data).toEqual(response);

        done();
      });
    });
    it('should throw error', (done => {
      const httpMock = {
        get: jest.fn().mockImplementation(() => {
          return throwError(new Error('my error message'));
        })
      };
      const serviceMock = new ValuationRequestDocsService(httpMock as any, new LoadingService(),documentTempService as any);
      serviceMock.getBssRespDocsByValReqId('1').subscribe((data) => {
        //this is intentional
      }, () => {
        expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/valuation-request-docs/1`);
        expect(httpMock.get).toBeDefined();
        expect(httpMock.get).toHaveBeenCalled();

        done();

      });
    }));

  });
  describe('Test: updateBssRespDocumentsTitles', () => {
    it('should return  Valuation discounts', done => {
      let valuationDocumentModel: ValuationDocumentModel[];

      const response: ValuationDocumentModel[] = [];

      const httpMock = {
        post: jest.fn().mockReturnValue(of(response))
      };
      const serviceMock = new ValuationRequestDocsService(httpMock as any, new LoadingService(),documentTempService as any);
      serviceMock.updateBssRespDocuments(valuationDocumentModel).subscribe((data) => {
        expect(httpMock.post).toBeDefined();
        expect(httpMock.post).toHaveBeenCalled();
        expect(data).toEqual(response);
        done();
      });
    });
    it('should throw error', (done => {
      let valuationDocumentModel: ValuationDocumentModel[];
      const httpMock = {
        post: jest.fn().mockImplementation(() => {
          return throwError(new Error('my error message'));
        })
      };
      const serviceMock = new ValuationRequestDocsService(httpMock as any, new LoadingService(),documentTempService as any);
      serviceMock.updateBssRespDocuments(valuationDocumentModel).subscribe((data) => {
        //this is intentional
      }, () => {
        expect(httpMock.post).toBeDefined();
        expect(httpMock.post).toHaveBeenCalled();

        done();

      });
    }))

  })
  describe('Test: getLitigiationDocByValReqId', () => {
    it('should return  Valuation documents', done => {
      let valuationDocumentList: ValuationDocumentModel[];

      const response: ValuationDocumentModel[] = [];


      const httpMock = {
        get: jest.fn().mockReturnValue(of(response))
      };
      const serviceMock = new ValuationRequestDocsService(httpMock as any, new LoadingService(),documentTempService as any);
      serviceMock.getLitigiationDocByValReqId('1').subscribe((data) => {
        expect(httpMock.get).toBeDefined();
        expect(httpMock.get).toHaveBeenCalled();
        expect(data).toEqual(response);

        done();
      });
    });
    it('should throw error', (done => {
      const httpMock = {
        get: jest.fn().mockImplementation(() => {
          return throwError(new Error('my error message'));
        })
      };
      const serviceMock = new ValuationRequestDocsService(httpMock as any, new LoadingService(),documentTempService as any);
      serviceMock.getLitigiationDocByValReqId('1').subscribe((data) => {
        //this is intentional
      }, () => {
        expect(httpMock.get).toBeDefined();
        expect(httpMock.get).toHaveBeenCalled();

        done();

      });
    }));

  });

  describe('Test: displayDocument', () => {
    it('should return  url visionneuse', done => {
      let valuationDocumentModel: ValuationDocumentModel;

      const response = { url: 'url' };

      const httpMock = {
        post: jest.fn().mockReturnValue(of(response))
      };
      const serviceMock = new ValuationRequestDocsService(httpMock as any, new LoadingService(),documentTempService as any);
      serviceMock.displayDocument(valuationDocumentModel).subscribe((data) => {
        expect(httpMock.post).toBeDefined();
        expect(httpMock.post).toHaveBeenCalled();
        expect(data).toEqual(response);
        done();
      });
    });
    it('should throw error', (done => {
      let valuationDocumentModel: ValuationDocumentModel;
      const httpMock = {
        post: jest.fn().mockImplementation(() => {
          return throwError(new Error('my error message'));
        })
      };
      const serviceMock = new ValuationRequestDocsService(httpMock as any, new LoadingService(),documentTempService as any);
      serviceMock.displayDocument(valuationDocumentModel).subscribe((data) => {
        //this is intentional
      }, () => {
        expect(httpMock.post).toBeDefined();
        expect(httpMock.post).toHaveBeenCalled();

        done();

      });
    }))

  })


  describe('Test: displayDocumentOfValuationByValuationRequestId', () => {
    it('should return  url visionneuse', done => {

      const response = { url: 'url' };

      const httpMock = {
        get: jest.fn().mockReturnValue(of(response))
      };
      const serviceMock = new ValuationRequestDocsService(httpMock as any, new LoadingService(),documentTempService as any);
      serviceMock.displayDocumentOfValuationByValuationRequestId('15').subscribe((data) => {
        expect(httpMock.get).toBeDefined();
        expect(httpMock.get).toHaveBeenCalled();
        expect(data).toEqual(response);
        done();
      });
    });
    it('should throw error', (done => {
      const httpMock = {
        get: jest.fn().mockImplementation(() => {
          return throwError(new Error('my error message'));
        })
      };
      const serviceMock = new ValuationRequestDocsService(httpMock as any, new LoadingService(),documentTempService as any);
      serviceMock.displayDocumentOfValuationByValuationRequestId('15').subscribe((data) => {
        //this is intentional
      }, () => {
        expect(httpMock.get).toBeDefined();
        expect(httpMock.get).toHaveBeenCalled();

        done();

      });
    }))

  })

  describe('Test: deleteDocuments', () => {
    it('should return  trues', done => {
      let valuationDocumentModel: ValuationDocumentModel[];

      const response: ValuationDocumentModel[] = [];

      const httpMock = {
        post: jest.fn().mockReturnValue(of(response))
      };
      const serviceMock = new ValuationRequestDocsService(httpMock as any, new LoadingService(),documentTempService as any);
      serviceMock.deleteDocuments(valuationDocumentModel).subscribe((data) => {
        expect(httpMock.post).toBeDefined();
        expect(httpMock.post).toHaveBeenCalled();
        expect(data).toEqual(response);
        done();
      });
    });
    it('should throw error', (done => {
      let valuationDocumentModel: ValuationDocumentModel[];
      const httpMock = {
        post: jest.fn().mockImplementation(() => {
          return throwError(new Error('my error message'));
        })
      };
      const serviceMock = new ValuationRequestDocsService(httpMock as any, new LoadingService(),documentTempService as any);
      serviceMock.deleteDocuments(valuationDocumentModel).subscribe((data) => {
        //this is intentional
      }, () => {
        expect(httpMock.post).toBeDefined();
        expect(httpMock.post).toHaveBeenCalled();

        done();

      });
    }))

  })

  describe('Test: uploadValuationLitigation', () => {
    it('should upload litigation', done => {

      const response = "url" ;

      const httpMock = {
        post: jest.fn().mockReturnValue(of(response))
      };
      const serviceMock = new ValuationRequestDocsService(httpMock as any, new LoadingService(),documentTempService as any);
      let params = new HttpParams();
      params = params.append('valuationRequestId', '2');
      params = params.append('cpNumber', '50');
      params = params.append('phase', DocumentConstant.PHASE_LITIGATION);
      params = params.append('family', DocumentConstant.FAMILY_VALUATION);
      params = params.append('type', DocumentConstant.TYPE_VALUATION);
      params = params.append('subType', DocumentConstant.SUB_TYPE_VALUATION_RESULT);
      params = params.append('isQualified', 'true');
      params = params.append('updateSimulStatus', 'false');

      const myBlob = new Blob([`/9j/4AAQSkZJRgABAQEBLAEsAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/
      2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/`], { type: 'image/jpeg' });
      let fileBase64 = "data:application/octet-stream;base64,LzlqLzRBQVFTa1pKUmdBQkFRRUJMQUVzQUFELzJ3QkRBQVlFQlFZRkJBWUdCUVlIQndZSUNoQUtDZ2tKQ2hRT0R3d1FGeFFZR0JjVUZoWWFIU1VmR2hzakhCWVdJQ3dnSXlZbktTb3BHUjh0TUMwb01DVW9LU2ovCiAgICAgIDJ3QkRBUWNIQndvSUNoTUtDaE1vR2hZYUtDZ29LQ2dvS0Nnb0tDZ29LQ2dvS0Nnb0tDZ29LQ2dvS0Nnb0tDZ29LQ2dvS0Nnb0tDZ29LQ2dvS0Nnb0tDZ29LQ2ov"

      let documentInfoToUpload = {
        "fileName": "name",
        "fileUrl": 'url'
      } as DocumentInfoToUploadModel;
      serviceMock.uploadValuationLitigation(new File([myBlob], "name"), { id: '2' } as ValuationRequestModel, { cp: '50' } as EntrepriseInfoModel).subscribe((data) => {
        expect(httpMock.post).toBeDefined();

        expect(data).toEqual(response);
        done();
      });
    });
  });

});
