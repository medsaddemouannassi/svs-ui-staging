import { AuthConfig, OAuthModuleConfig } from 'angular-oauth2-oidc';
import { environment } from 'src/environments/environment';

export const authConfig: AuthConfig = {
  issuer: environment.oAuth,
  clientId: environment.clientId,
  redirectUri: window.location.origin,
  silentRefreshRedirectUri: window.location.origin + '/silent-refresh.html',
  scope: 'openid profile email api',
  responseType: 'code',
  clearHashAfterLogin: false
};
authConfig.logoutUrl = `${authConfig.issuer}v2/logout?client_id=${authConfig.clientId}&returnTo=${encodeURIComponent(authConfig.redirectUri)}`;

export const authModuleConfig: OAuthModuleConfig = {
  // Inject "Authorization: Bearer ..." header for these APIs:
  resourceServer: {
    allowedUrls: [environment.baseUrl],
    sendAccessToken: true,
  },
};