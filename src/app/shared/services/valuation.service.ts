import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ValuationModel } from '../models/valuation.model';

@Injectable({
  providedIn: 'root'
})
export class ValuationService {

  baseUrl = environment.baseUrl;
  valuation : ValuationModel = {} as ValuationModel;

  constructor(private http: HttpClient) { }


  getValuationByIdRequest(id: string): Observable<ValuationModel> {
    return this.http.get<ValuationModel>(
      `${this.baseUrl}/rest/v1/valuation/request/${id}`
    ).pipe(
      catchError(
        err => {
          console.error('valuation is not found, please contact the application admin...', err);
          return throwError(err);
        }
      )
    );
  }


  getValuationById(id: string): Observable<any> {
    return this.http.get<any>(
      `${this.baseUrl}/rest/v1/valuation/${id}`
    ).pipe(
      catchError(
        err => {
          console.error('valuation is not found, please contact the application admin...', err);
          return throwError(err);
        }
      )
    );
  }

saveValuationDiscounts(body: any): Observable<Object> {
  return this.http.post<Object>(
    `${this.baseUrl}/rest/v1/discount/`, body
  ).pipe(
    catchError(
      err => {
        return throwError(err);
      }
    )
  );
}


saveValuation(valuationReception: ValuationModel): Observable<object> {
    return this.http.post<ValuationModel>(
      `${this.baseUrl}/rest/v1/valuation/`, valuationReception
    ).pipe(
      catchError(
        err => {
          return throwError(err);
        }
      )
    );
  }
  getLast10ValuationByNatureId(id:string):Observable<ValuationModel[]>{
    return this.http.get<ValuationModel[]>(
      `${this.baseUrl}/rest/v1/valuation/cat-nature/${id}`);
  }

  getLast10ValuationsByBrandId(id: string): Observable<ValuationModel[]> {
    return this.http.get<ValuationModel[]>(
        `${this.baseUrl}/rest/v1/valuation/cat-brand/${id}`
    );
}
}
