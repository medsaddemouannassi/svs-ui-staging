import { HttpParams } from '@angular/common/http';
import { of, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CatBrandModel } from '../models/cat-brand.model';
import { CatNatureModel } from '../models/cat-nature.model';
import { CatBrandService } from './cat-brand.service';

describe('Service: CatBrandService', () => {
    const http = jest.fn();
    let baseUrl = environment.baseUrl;

 describe('Test: getBrandList', () => {
    it('should return  list of CatBrandModel objects', done => {


     const response : CatBrandModel[]=[];

     const httpMock = {
      get: jest.fn().mockReturnValue(of(response))
     };
     const serviceMock = new CatBrandService(httpMock as any);
     serviceMock.getBrandList().subscribe((data) => {
         expect(httpMock.get).toHaveBeenCalledWith( `${baseUrl}/rest/v1/cat/brands`);
      expect(httpMock.get).toBeDefined();
      expect(httpMock.get).toHaveBeenCalled();
      expect(data).toEqual(response);
      done();
     });
    });

   });

   describe('Test: getCatBrandById', () => {
    it('should return  CatBrandModel object', done => {

        const response: CatBrandModel= {
            id: "1",
            label: "tesst",
            catNature: {
                id: "1",
                label: "Semi-remorque plateau",
                catActivitySector: {

                    id: "1",
                    label: "Agricole",
                    comment: "comment",
                    isActive: true
                },
                comment: "comment",
                externalId: null,
                paramNap:null,
                 isActive: true,
                isValid: false,
                isEnrg: false
               } as CatNatureModel,
               comment: null,
               isActive: true,
               catRequest:null
           } as CatBrandModel

           const httpMock = {
               get: jest.fn().mockReturnValue(of(response))
           };
           const serviceMock = new CatBrandService(httpMock as any);
           serviceMock.getCatBrandById('1').subscribe((data) => {
               expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/brands/1`);
               expect(httpMock.get).toBeDefined();
               expect(httpMock.get).toHaveBeenCalled();
               expect(data).toEqual(response);

               done();
           });
       });
   });

   describe('Test: getCatBrandByCatReqstId', () => {
    it('should return  CatBrandModel object', done => {

        const response: CatBrandModel= {
            "id": "1",
            "label": "tesst",
            catNature: {
                id: "1",
                label: "Semi-remorque plateau",
                catActivitySector: {

                    "id": "1",
                    "label": "Agricole",
                    "comment": "comment",
                    "isActive": true
                },
                comment: "comment",
                externalId: null,
                 paramNap:null,
                 isActive: true,
                 isValid: false,
                 isEnrg: false
            } as CatNatureModel,
               "comment": null,
               "isActive": true,
               "catRequest":null
           } as CatBrandModel

           const httpMock = {
               get: jest.fn().mockReturnValue(of(response))
           };
           const serviceMock = new CatBrandService(httpMock as any);
           serviceMock.getCatBrandByCatReqstId('15').subscribe((data) => {
               expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/brands/cat-request/15`);
               expect(httpMock.get).toBeDefined();
               expect(httpMock.get).toHaveBeenCalled();
               expect(data).toEqual(response);

               done();
           });
       });
   });

    describe('Test: saveCatBrand', () => {
        it('should return  CatBrandModel object', done => {
            let catBrandModel: CatBrandModel;

            const response = {
                "id": "1",
                "label": "tesst",
                catNature: {
                    id: "1",
                    label: "Semi-remorque plateau",
                    catActivitySector: {

                        "id": "1",
                        "label": "Agricole",
                        "comment": "comment",
                        "isActive": true
                    },
                    comment: "comment",
                    externalId: null,
                     paramNap:null,
                     isActive: true,
                     isValid: false,
                     isEnrg: false
                } as CatNatureModel,
                   "comment": null,
                   "isActive": true,
                   "catRequest":null

               }

            const httpMock = {
                post: jest.fn().mockReturnValue(of(response))
            };
            const serviceMock = new CatBrandService(httpMock as any);
            serviceMock.saveCatBrand(catBrandModel).subscribe((data) => {
                expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/brands/`, catBrandModel);
                expect(httpMock.post).toBeDefined();
                expect(httpMock.post).toHaveBeenCalled();
                expect(data).toEqual(response);
                done();
            });
        });


        it('should throw error', (done => {
            let catBrandModel: CatBrandModel;
			const httpMock = {
				post: jest.fn().mockImplementation(() => {
                    return throwError(new Error('my error message'));
                })
			};
			const serviceMock = new CatBrandService(httpMock as any);
			serviceMock.saveCatBrand(catBrandModel).subscribe((data) => {
			//this is intentional
			},()=>{
			    expect(httpMock.post).toHaveBeenCalledWith( `${baseUrl}/rest/v1/cat/brands/`, catBrandModel);
				expect(httpMock.post).toBeDefined();
				expect(httpMock.post).toHaveBeenCalled();
                done();

            });
          }));

    });

    describe('Test: deleteCatBrandById', () => {
        it('should delete  CatBrandModel object', done => {
            let response: any;
            let catBrandId: any;
            const httpMock = {
                post: jest.fn().mockReturnValue(of(response))
            };
            const serviceMock = new CatBrandService(httpMock as any);
            serviceMock.deleteCatBrandById(catBrandId).subscribe((data) => {
                expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/brands/delete`, catBrandId);
                expect(httpMock.post).toBeDefined();
                expect(httpMock.post).toHaveBeenCalled();
                expect(data).toEqual(response);
                done();
            });
        });
    });
    describe('Test: getCatBrandListByCatNatureId', () => {
        it('should getCatBrandListByCatNatureId ', done => {
            let response:  CatBrandModel[]=[];
            const httpMock = {
                get: jest.fn().mockReturnValue(of(response))
            };
            const serviceMock = new CatBrandService(httpMock as any);
            serviceMock.getCatBrandListByCatNatureId("1").subscribe((data) => {
                expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/brands/natures/1`);
                expect(httpMock.get).toBeDefined();
                expect(httpMock.get).toHaveBeenCalled();
                expect(data).toEqual(response);
                done();
            });
        });
    });

    describe('Test: loadCatBrandListByQuery', () => {
        it('should return  object', done => {
          const response = ["brandLabel1"] ;
         const httpMock = {
          get: jest.fn().mockReturnValue(of(response))
           };
         const serviceMock = new CatBrandService(httpMock as any);
         let params = new HttpParams();
         params = params.append('sector', 'sector');
         params = params.append('nature', 'nature');
         params = params.append('brand', 'brand');
         params = params.append('type', 'type');
         serviceMock.loadCatBrandListByQuery('sector','nature','brand','type').subscribe((data) => {
             expect(httpMock.get).toHaveBeenCalledWith( `${baseUrl}/rest/v1/cat/brands/search/label`,{params:params});
          expect(httpMock.get).toBeDefined();
          expect(httpMock.get).toHaveBeenCalled();
          expect(data).toEqual(response);
          done();
         });
        });
    });



    describe('Test: loadCatBrandListByLabel', () => {
        it('should return  object', done => {
          const response = ["brandLabel1"] ;
         const httpMock = {
          get: jest.fn().mockReturnValue(of(response))
           };
         const serviceMock = new CatBrandService(httpMock as any);
         let params = new HttpParams();

         params = params.append('brand', 'brand');

         serviceMock.loadCatBrandListByLabel('brand').subscribe((data) => {
             expect(httpMock.get).toHaveBeenCalledWith( `${baseUrl}/rest/v1/cat/brands/search/catbrand/label`,{params:params});
          expect(httpMock.get).toBeDefined();
          expect(httpMock.get).toHaveBeenCalled();
          expect(data).toEqual(response);
          done();
         });
        });
    });



  describe('Test: loadCatBrandListByLabel', () => {
        it('should return  object', done => {
          const response = ["brandLabel1"] ;
         const httpMock = {
          get: jest.fn().mockReturnValue(of(response))
           };
         const serviceMock = new CatBrandService(httpMock as any);
         let params = new HttpParams();

         params = params.append('brand', 'brand');

         serviceMock.loadCatBrandListByLabel('brand').subscribe((data) => {
             expect(httpMock.get).toHaveBeenCalledWith( `${baseUrl}/rest/v1/cat/brands/search/catbrand/label`,{params:params});
          expect(httpMock.get).toBeDefined();
          expect(httpMock.get).toHaveBeenCalled();
          expect(data).toEqual(response);
          done();
         });
        });
    });

});
