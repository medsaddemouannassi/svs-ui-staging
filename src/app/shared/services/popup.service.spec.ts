import { TestBed } from '@angular/core/testing';
import { of, Subject } from 'rxjs';
import { ValuationDocumentModel } from '../models/valuationDocument.model';
import { PopupService } from './popup.service';
import { DomSanitizer } from '@angular/platform-browser';
import { MdmInformationsModel } from '../models/mdm-informations.model';
import { ValuationModel } from '../models/valuation.model';
import { ValuationRequestModel } from '../models/valuation-request.model';


describe('PopupService', () => {
  let service: PopupService;
  let modalServiceMock: any;
  const modalRef = {
    componentInstance: {
      document: {} as ValuationDocumentModel,
      index: '1',
      docSubTypesListParams: null
    }
  }
  let sanitizer: any;
  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PopupService);
    modalServiceMock = {
      open: jest.fn().mockReturnValue(of(modalRef)),
      onConfirm: new Subject<boolean>(),
      dismissAll: jest.fn().mockReturnValue(of(true)),
    };
    sanitizer = TestBed.inject(DomSanitizer);

    service = new PopupService(modalServiceMock, sanitizer);

  });

  describe('Test: popupInfo', () => {
    it('should popupInfo', () => {
      const mock = {
        componentInstance: {
          confirmBtnMessage: '',
          file: null,
          pdfSrc: null,
          isPdf: false,
          cancelBtnMessage: 'cancelButton',
          docSubTypesListParams: null,
          onConfirm: new Subject<boolean>(),
        },
        result: true,
      };
      spyOn(modalServiceMock, 'open').and.returnValue(mock as any);
      spyOn(mock.componentInstance, 'onConfirm').and.returnValue(of(true));

      service.popupInfo('Confimer', null, null, 'Annuler', false);
      expect(mock.componentInstance.cancelBtnMessage).toEqual('Annuler');
    });
  });


  describe('Test: popupPDF', () => {
    it('should popupPDF', () => {
      const mock = {
        componentInstance: {
          confirmBtnMessage: '',
          file: null,
          pdfSrc: null,
          isPdf: false,
          cancelBtnMessage: 'cancelButton',
          docSubTypesListParams: null,
          onConfirm: new Subject<boolean>(),
        },
        result: true,
      };
      spyOn(modalServiceMock, 'open').and.returnValue(mock as any);
      spyOn(mock.componentInstance, 'onConfirm').and.returnValue(of(true));

      service.popupPDF('Confimer', 'Annuler', null, null, true);
      expect(mock.componentInstance.confirmBtnMessage).toEqual('Confimer');
    });
  });

  describe('Test: service', () => {
    it('should be created', () => {
      expect(service).toBeTruthy();
    });
  });
  describe('Test: popupDocument', () => {
    it('should popupDocument', () => {
      const mock = {
        componentInstance: {
          onConfirm: new Subject<boolean>(),
          document: null,
          index: null,
        },
        result: true,
      };
      spyOn(modalServiceMock, 'open').and.returnValue(mock as any);
      spyOn(mock.componentInstance, 'onConfirm').and.returnValue(of(true));

      let document = {
        id: '1',
        docFamily: '',
        docName: ''
      } as ValuationDocumentModel;
      service.popupDocument(document, 1, null);
      expect(mock.componentInstance.index).toEqual(1);
    });
  });

  describe('Test: popupDocumentList', () => {
    it('should popupDocumentList', () => {
      const mock = {
        componentInstance: {
          documentsList: [],
          docSubTypesListParams: null,
          onConfirm: new Subject<boolean>(),
        },
        result: true,
      };
      spyOn(modalServiceMock, 'open').and.returnValue(mock as any);
      spyOn(mock.componentInstance, 'onConfirm').and.returnValue(of(true));
      let documentList = [{
        id: '1',
        docFamily: '',
        docName: ''
      } as ValuationDocumentModel];
      service.popupDocumentList(documentList, 1);
      expect(mock.componentInstance.documentsList).toEqual(documentList);
    });
  });

  describe('Test: popupCommunication', () => {
    it('should popupCommunication', () => {
      const mock = {
        componentInstance: {
          onConfirm: new Subject<boolean>(),
          theme: null,
          page: null,
          roles: null,
          catRequest: null
        }
      };
      spyOn(modalServiceMock, 'open').and.returnValue(mock as any);
      spyOn(mock.componentInstance, 'onConfirm').and.returnValue(of({ data: '' }));


      service.popupCommunication([], 'page x', 'stat', null, null, null);
      expect(mock.componentInstance.page).toEqual('page x');
    });
  });


  describe('Test: popupAskForValidation', () => {
    it('should set value to networkId and amount', () => {
      const mock = {
        componentInstance: {
          onConfirm: new Subject<boolean>(),
          theme: null,
          page: null,
          roles: null,
          networkId: null,
          amount: null
        }
      };
      spyOn(modalServiceMock, 'open').and.returnValue(mock as any);
      spyOn(mock.componentInstance, 'onConfirm').and.returnValue(of({ data: '' }));

      let valuation = {
        valuationRequest : {
          financingAmount: 230000
        } as ValuationRequestModel
      } as ValuationModel;
      let descInfo = {} as MdmInformationsModel;
      service.popupAskForValidation('106', valuation, descInfo);
      expect(mock.componentInstance.amount).toEqual(230000);
      expect(mock.componentInstance.networkId).toEqual('106');

    });
  });


  describe('Test: dismissAll', () => {
    it('should call dismissAll of modalServiceMock', () => {
      service.dismissAll();
      expect(modalServiceMock.dismissAll).toHaveBeenCalled();
    });
  });

});
