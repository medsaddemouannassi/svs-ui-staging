import { of } from 'rxjs';
import { DocumentTempService } from './document-temp.service';
import { generateFileName } from '../fileUtils';
import { HttpParams } from '@angular/common/http';



describe('Service: DocumentTempService', () => {
  let service: DocumentTempService;
  const http = jest.fn();
  let baseUrl;

  beforeEach(() => {
    service = new DocumentTempService(http as any);
    baseUrl = service.baseUrl;
  });


  describe('Test: getDiscount', () => {
    it('should return object', done => {


      const response = 'complete';


      const httpMock = {
        get: jest.fn().mockReturnValue(of(response)),
        put:jest.fn().mockReturnValue(of(response))
      };
      const serviceMock = new DocumentTempService(httpMock as any);
      let file={name:'test.pdf'}
      let params = new HttpParams();
      let fileName=generateFileName(file.name);
      params = params.append('filename', fileName);
      params = params.append('key', 'temp');
      serviceMock.saveTempDocument(file,fileName).then((data) => {
        expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/s3psurl`,{responseType: 'text', params: params});
        expect(httpMock.get).toBeDefined();
        expect(httpMock.get).toHaveBeenCalled();
        expect(data).toEqual(response);

        done();
      });
    });
  });
});