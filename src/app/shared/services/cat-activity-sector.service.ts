import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CatActivitySectorModel } from '../models/cat-activity-sector.model';


@Injectable({
  providedIn: 'root'
})
export class CatActivitySectorService {

  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }


  submitActivitySector(catActivitiSector: CatActivitySectorModel): Observable<CatActivitySectorModel> {
    return this.http.post<CatActivitySectorModel>(
      `${this.baseUrl}/rest/v1/cat/sectors`, catActivitiSector
    
    );
  }

  getActivitySectorList(): Observable<CatActivitySectorModel[]> {
    return this.http.get<CatActivitySectorModel[]>(
      `${this.baseUrl}/rest/v1/cat/sectors`
    );
  }

  getActivitySectorById(id: string): Observable<CatActivitySectorModel> {
    return this.http.get<CatActivitySectorModel>(
      `${this.baseUrl}/rest/v1/cat/sectors/${id}`
    );
  }

  loadActivitySectorListByQuery(sector: string,nature: string,brand: string,type: string): Observable<string[]> {
    let params = new HttpParams();
    params = params.append('sector', sector);
    params = params.append('nature', nature);
    params = params.append('brand', brand);
    params = params.append('type', type);

  
    return this.http.get<string[]>(`${this.baseUrl}/rest/v1/cat/sectors/search/label`,{ params: params });
    }

}
