import { environment } from 'src/environments/environment';
import { of } from 'rxjs';
import { RefUnderwriterService } from './ref-underwriter.service';
import { RefUnderwriterModel } from '../models/ref-underwriter.model';

describe('Service: RefUnderwriterService', () => {
 let service: RefUnderwriterService;
 const http = jest.fn();
 const baseUrl = environment.baseUrl;

 beforeEach(() => {
  service = new RefUnderwriterService(http as any);
 });




  describe('Test: loadLegalNamesCustomersByQuery', () => {
    it('should return  list of RefUnderwriterModel objects', done => {

      const response: RefUnderwriterModel[] = [];

      const httpMock = {
        get: jest.fn().mockReturnValue(of(response))
      };
      const serviceMock = new RefUnderwriterService(httpMock as any);
      serviceMock.loadLegalNamesCustomersByQuery('customName').subscribe((data) => {
        expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/ref-underwriters/search/lgl-name/customName`);
        expect(httpMock.get).toBeDefined();
        expect(httpMock.get).toHaveBeenCalled();
        expect(data).toEqual(response);
        done();
      });
    });

  });


  describe('Test: loadNsirenByQuery', () => {
    it('should return  list of RefUnderwriterModel objects', done => {

      const response: RefUnderwriterModel[] = [];

      const httpMock = {
        get: jest.fn().mockReturnValue(of(response))
      };
      const serviceMock = new RefUnderwriterService(httpMock as any);
      serviceMock.loadNsirenByQuery('nsirenQuery').subscribe((data) => {
        expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/ref-underwriters/search/nsiren/nsirenQuery`);
        expect(httpMock.get).toBeDefined();
        expect(httpMock.get).toHaveBeenCalled();
        expect(data).toEqual(response);
        done();
      });
    });

  });
});

