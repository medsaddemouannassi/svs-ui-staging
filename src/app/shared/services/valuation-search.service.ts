import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { PageValuationSearch } from '../models/page-valuation-search.model';
import { ValuationSearchModel } from '../models/valuation-search.model';

@Injectable({
  providedIn: 'root'
})
export class ValuationSearchService {

  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }

  findValuationSearch(valuationSearch:ValuationSearchModel,sort:string,elementByPage:number,indexPage:number): Observable<PageValuationSearch> {
    return this.http.post<PageValuationSearch>(`${this.baseUrl}/rest/v1/valuation/search?sort=${sort}&byPage=${elementByPage}&page=${indexPage}`,valuationSearch);
  }
  
  
  findValuations(id: string): Observable<ValuationSearchModel[]> {
    return this.http.get<ValuationSearchModel[]>(
      `${this.baseUrl}/rest/v1/valuation/search/${id}`,
      
    );
  }
}
