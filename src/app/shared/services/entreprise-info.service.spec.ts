import { of, throwError } from 'rxjs';
import { environment } from '../../../environments/environment';
import { EntrepriseInfoService } from './entreprise-info.service';
import { LoadingService } from './loading-service.service';


describe('Service: EntrepriseInfoService', () => {
    let service: EntrepriseInfoService;
    const http = jest.fn();
    let baseUrl = environment.baseUrl;
    beforeEach(() => {
        service = new EntrepriseInfoService(http as any,new LoadingService());
    });



    describe('Test: loadEntrepriseInfo', () => {
        it('should return  object', done => {


            const response = {
                id: null,
                subsidiary: null,
                cp: null,
                description: null,
                siren: null,
                sirenType: null,
                socialReason: 'test reason social',
                delegation: null,
                regionalDirection: null,
                network: null,
                isFromRet: null,
                isPmFromRet: null,
                idRet: null,
                mainUnderwId: null,
                mainUnderwName: null



            };


            const httpMock = {
                post: jest.fn().mockReturnValue(of(response))
            };
            const serviceMock = new EntrepriseInfoService(httpMock as any,new LoadingService());
            serviceMock.loadEntrepriseInfo('1','1').subscribe((data) => {
                expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/entreprise-info`,{"idBcp": "1", "idRet": "1"});
                expect(httpMock.post).toBeDefined();
                expect(httpMock.post).toHaveBeenCalled();
                expect(serviceMock.socialReasonChanged.getValue()).toEqual('test reason social')
                expect(data).toEqual(response);

                done();
            });
        });

        it('should throw error', (done => {
            const httpMock = {
                post: jest.fn().mockImplementation(() => {
                    return throwError(new Error('my error message'));
                })
            };
            const serviceMock = new EntrepriseInfoService(httpMock as any,new LoadingService());
            serviceMock.loadEntrepriseInfo('1','1').subscribe((data) => {
            // This is intentional
            }, () => {
                expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/entreprise-info`,{"idBcp": "1", "idRet": "1"});
                expect(httpMock.post).toBeDefined();
                expect(httpMock.post).toHaveBeenCalled();
                expect(serviceMock.socialReasonChanged.getValue()).toEqual('')

                done();

            });
        }));


    });


    describe('Test: uploadDocument', () => {
        it('should upload document', done => {

            let fileMock: File = new File(["file content"], "file Name", {
                type: "text/plain",
            });
            const formData = new FormData();

            formData.append('file', fileMock, fileMock.name);


            const response = {
                success: true,
                message: 'Expense saved successfully'
            };





            const httpMock = {
                post: jest.fn().mockReturnValue(of(response))
            };
            const serviceMock = new EntrepriseInfoService(httpMock as any,new LoadingService());
            serviceMock.uploadDocument(fileMock).subscribe((data) => {
                expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/valuation/uploadDocument`, formData, { reportProgress: true });
                expect(httpMock.post).toBeDefined();
                expect(httpMock.post).toHaveBeenCalled();
                expect(data).toEqual(response);

                done();
            });
        });


    });

    describe('Test: upload Other Document', () => {
        it('should upload other document', done => {

            let fileMock: File = new File(["file content"], "file Name", {
                type: "text/plain",
            });
            const formData = new FormData();

            formData.append('file', fileMock, fileMock.name);


            const response = {
                success: true,
                message: 'Expense saved successfully'
            };





            const httpMock = {
                post: jest.fn().mockReturnValue(of(response))
            };
            const serviceMock = new EntrepriseInfoService(httpMock as any,new LoadingService());
            serviceMock.uploadOtherDocument(fileMock).subscribe((data) => {
                expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/valuation/uploadOtherDocument`, formData, { reportProgress: true });
                expect(httpMock.post).toBeDefined();
                expect(httpMock.post).toHaveBeenCalled();
                expect(data).toEqual(response);

                done();
            });
        });


    });
});
