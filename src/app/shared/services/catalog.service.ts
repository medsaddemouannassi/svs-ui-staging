
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { SvsDescCollabService } from './svs-desc-collab.service';
import { SvsDescCollabModel } from '../models/svs-desc-collab.model';
@Injectable({
  providedIn: 'root'
})
export class CatalogService {

  constructor(private svsDescCollabService: SvsDescCollabService) { }

 
  fillInfoCreationUser(mat: string): Observable<SvsDescCollabModel> {
    
    if ("BATCH" === mat) {
      let svsDescCollabModel= {collaboratorFullName: 'Batch'}  as SvsDescCollabModel
      return of(svsDescCollabModel);
    } else {
      return this.svsDescCollabService.getSvsDescCollab(mat);
     
    }
  }

  
  



}
