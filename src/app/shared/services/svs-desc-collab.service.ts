import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { MdmInformationsModel } from '../models/mdm-informations.model';
import { SvsDescCollabModel } from '../models/svs-desc-collab.model';
import { SvsDescHistoryModel } from '../models/svs-desc-hist.model';

@Injectable({
  providedIn: 'root'
})
export class SvsDescCollabService {
  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }
  getSvsDescCollabList( descId:string): Observable<SvsDescCollabModel[]> {
    return this.http.get<SvsDescCollabModel[]>(`${this.baseUrl}/rest/v1/svs-desc-collab/${descId}`)
    
  }
  submitSvsDescCollab(svsDescCollab:SvsDescCollabModel): Observable<SvsDescCollabModel>{
    return this.http.post<SvsDescCollabModel>(`${this.baseUrl}/rest/v1/svs-desc-collab`,svsDescCollab)
   
  }
  removeSvsDescCollab(svsDescCollab:SvsDescCollabModel): Observable<SvsDescCollabModel>{
    return this.http.post<SvsDescCollabModel>(`${this.baseUrl}/rest/v1/svs-desc-collab/remove`,svsDescCollab);
  }

  
  getDescCollabByQuery(descId:string,query:string): Observable<MdmInformationsModel[]> {
    return this.http.get<MdmInformationsModel[]>(`${this.baseUrl}/rest/v1/svs-desc-collab/collab-list/${descId}/${query}`);
  }

  isDescManager(mat:string): Observable<boolean> {
    return this.http.get<boolean>(`${this.baseUrl}/rest/v1/svs-desc-collab/is-manager/${mat}`);
  }

  getSvsDescCollab( descId:string): Observable<SvsDescCollabModel> {
    return this.http.get<SvsDescCollabModel>(`${this.baseUrl}/rest/v1/svs-desc-collab/collab/${descId}`);
  }

  getSvsDescHistory( histId:string): Observable<SvsDescHistoryModel> {
    return this.http.get<SvsDescHistoryModel>(`${this.baseUrl}/rest/v1/svs-desc-collab/history/${histId}`);
  }

  getAuthorizedCollabsForValoValidation(networkId:string, amount: number): Observable<SvsDescCollabModel[]> {
    return this.http.get<SvsDescCollabModel[]>(`${this.baseUrl}/rest/v1/svs-desc-collab/validators/${networkId}/${amount}`);
  }

}
