import { HttpParams } from '@angular/common/http';
import { of, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CatDocumentModel } from '../models/cat-document.model';
import { CatDocumentService } from './cat-document.service';
import { DocumentInfoToUploadModel } from '../models/document-info-to-upload';
import { DocumentRequest } from '../models/document-request.model';




describe('Service: CatDocumentService', () => {

    let baseUrl = environment.baseUrl;
    let documentTempService={
        saveTempDocument: jest.fn().mockReturnValue(of('url'))
    }
    describe('Test: uploadDocument', () => {
        it('should return list of object', done => {

            const response = { id: 'test' } as CatDocumentModel;

            let params = new HttpParams();

            params=params.append('catActivitySectorId', '1');
            params=params.append('catNatureId', '2');
            params=params.append('catBrandId', '3');
            params=params.append('catAssetTypeId', '4');
            params=params.append('discriminator', 'G');
            const myBlob = new Blob([`/9j/4AAQSkZJRgABAQEBLAEsAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/
            2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/`], { type: 'image/jpeg' });  

            let fileBase64="data:application/octet-stream;base64,LzlqLzRBQVFTa1pKUmdBQkFRRUJMQUVzQUFELzJ3QkRBQVlFQlFZRkJBWUdCUVlIQndZSUNoQUtDZ2tKQ2hRT0R3d1FGeFFZR0JjVUZoWWFIU1VmR2hzakhCWVdJQ3dnSXlZbktTb3BHUjh0TUMwb01DVW9LU2ovCiAgICAgICAgICAgIDJ3QkRBUWNIQndvSUNoTUtDaE1vR2hZYUtDZ29LQ2dvS0Nnb0tDZ29LQ2dvS0Nnb0tDZ29LQ2dvS0Nnb0tDZ29LQ2dvS0Nnb0tDZ29LQ2dvS0Nnb0tDZ29LQ2ov"
            let documentInfoToUpload = {
                "fileName": "name",
                "fileUrl": 'url'
              } as DocumentInfoToUploadModel;
            const httpMock = {
                post: jest.fn().mockReturnValue(of(response))
            };
            const serviceMock = new CatDocumentService(httpMock as any,documentTempService as any);
            serviceMock.uploadDocument(new File([myBlob], "name"), '1', '2', '3', '4', 'G').subscribe((data) => {
                expect(httpMock.post).toHaveBeenCalled();
                expect(httpMock.post).toBeDefined();
                expect(data).toEqual(response)
                done();
            });
        });
        
    });

   


    describe('Test: searchDocument', () => {
        it('should return list of object', done => {

            const response = {} as CatDocumentModel[];
            const httpMock = {
                get: jest.fn().mockReturnValue(of(response))
            };
            let params = new HttpParams();
            params = params.append('sectorId', "1");
            params = params.append('natureId', "2");
            params = params.append('brandId', "3");
            params = params.append('assetTypeId', "4");
            params = params.append('discriminator', "G");

            const serviceMock = new CatDocumentService(httpMock as any, documentTempService as any);
            serviceMock.searchDocument('1', '2', '3', '4', 'G').subscribe((data) => {
                expect(httpMock.get).toHaveBeenCalled();
                expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/cat-doc/search`, { params: params });
                expect(httpMock.get).toBeDefined();
                expect(data).toEqual(response)
                done();
            });
        });
    });



    describe('Test: downloadDocument', () => {
        it('should return list of object', done => {
            let gedPath = 'c://test.txt';
            let docName = 'test.txt';
            const response = {} as Blob;



            const httpMock = {
                post: jest.fn().mockReturnValue(of(response))
            };
            const serviceMock = new CatDocumentService(httpMock as any,documentTempService as any);
            serviceMock.downloadDocument(gedPath, docName).subscribe((data) => {
                expect(httpMock.post).toHaveBeenCalled();
                expect(httpMock.post).toBeDefined();
                expect(data).toEqual(response)
                done();
            });





        });
    });

    describe('Test: updateDocument', () => {
        it('should return updated document', done => {

            const response = { id: 'test' } as CatDocumentModel;



            const httpMock = {
                post: jest.fn().mockReturnValue(of(response))
            };
            const serviceMock = new CatDocumentService(httpMock as any,documentTempService as any);
            serviceMock.updateDocument({} as CatDocumentModel).subscribe((data) => {
                expect(httpMock.post).toHaveBeenCalled();
                expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/cat-doc/update`, {} as CatDocumentModel);
                expect(httpMock.post).toBeDefined();
                expect(data).toEqual(response)
                done();
            });
        });
        it('should return errorrr', done => {

            const response = { id: 'test' } as CatDocumentModel;



            const httpMock = {
                post: jest.fn().mockImplementation(() => {
                    return throwError(new Error('my error message'));
                })
            };
            const serviceMock = new CatDocumentService(httpMock as any,documentTempService as any);
            serviceMock.updateDocument({} as CatDocumentModel).subscribe((data) => {

            }, () => {
                expect(httpMock.post).toHaveBeenCalled();
                expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/cat-doc/update`, {} as CatDocumentModel);
                expect(httpMock.post).toBeDefined();
                done();
            });
        });
    });
    describe('Test: deleteCatDocument', () => {
        it('should return boolean of deleted doc', done => {



            const httpMock = {
                post: jest.fn().mockReturnValue(of(true))
            };
            const serviceMock = new CatDocumentService(httpMock as any,documentTempService as any);
            serviceMock.deleteCatDocument({} as CatDocumentModel).subscribe((data) => {
                expect(httpMock.post).toHaveBeenCalled();
                expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/cat-doc/delete`, {} as CatDocumentModel);
                expect(httpMock.post).toBeDefined();
                expect(data).toEqual(true)
                done();
            });
        });
    });
    describe('Test: deleteCatDocument', () => {
        it('should return delete error', done => {



            const httpMock = {
                post: jest.fn().mockImplementation(() => {
                    return throwError(new Error('my error message'));
                })
            };
            const serviceMock = new CatDocumentService(httpMock as any,documentTempService as any);
            serviceMock.deleteCatDocument({} as CatDocumentModel).subscribe((data) => {

            }, (err) => {
                expect(httpMock.post).toHaveBeenCalled();
                expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/cat-doc/delete`, {} as CatDocumentModel);
                expect(httpMock.post).toBeDefined();
                done();
            });
        });
    });
    describe('Test: searchInheratedDocuments', () => {
        it('should return  List of object', done => {


            const response = {} as CatDocumentModel[];
            const httpMock = {
                get: jest.fn().mockReturnValue(of(response))
            };
            let params = new HttpParams();
            params = params.append('sectorId', "1");
            params = params.append('natureId', "2");
            params = params.append('brandId', "3");
            params = params.append('discriminator', "G");
            const serviceMock = new CatDocumentService(httpMock as any,documentTempService as any);
            serviceMock.searchInheratedDocuments('1', '2', '3', 'G').subscribe((data) => {
                expect(httpMock.get).toHaveBeenCalled();
                expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/cat-doc/search-inherited`, { params: params });
                expect(httpMock.get).toBeDefined();
                expect(data).toEqual(response)
                done();
            });


        });

    });

    describe('Test: searchInheratedDocuments', () => {
    it('should return list of object1', done => {

        const response = { id: 'test' } as CatDocumentModel;


        let documentRequest = {} as DocumentRequest

        documentRequest.catActivitySectorId = '1';
        documentRequest.catNatureId =  '2'
        documentRequest.catBrandId =  '3'
        documentRequest.catAssetTypeId =  '4'
        documentRequest.discriminator = 'G'
        documentRequest.pathSource = 'path'
        documentRequest.docName = 'test.txt'
     
       
        const httpMock = {
            post: jest.fn().mockReturnValue(of(response))
        };
        const serviceMock = new CatDocumentService(httpMock as any,documentTempService as any);
        serviceMock.saveDocumentRequest( '1', '2', '3', '4', 'G','path','test.txt').subscribe((data) => {
            expect(httpMock.post).toHaveBeenCalled();
            expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/cat/cat-doc/validate-request`, documentRequest);
            expect(httpMock.post).toBeDefined();
            expect(data).toEqual(response)
            done();
        });
    });
});


});
