import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ParamSettingModel } from '../models/param-setting.model';
import { SettingConstant } from '../setting-constant';


@Injectable({
  providedIn: 'root'
})
export class ParamSettingService {

  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }

  getParamRate(valuationDate : Date) : Observable<object>  {
    
    return this.http.post(
        `${this.baseUrl}/rest/v1/param/setting/date`,{filtreDate:valuationDate,filtreCode:SettingConstant.VALUE_RATE}
      ).pipe(
        catchError(
          err => {
            console.error('Cannot get discount, please contact the application admin...', err);
            return throwError(err);
          }
        )
      );

  }
  getParamByCode(code : string) : Observable<ParamSettingModel[]>  {
    return this.http.get<ParamSettingModel[]>(
        `${this.baseUrl}/rest/v1/param/setting/code/${code}`
      ).pipe(
        catchError(
          err => {
            console.error('Cannot get discount, please contact the application admin...', err);
            return throwError(err);
          }
        )
      );

  }



}