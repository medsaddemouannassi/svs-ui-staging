import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { CatNatureModel } from '../models/cat-nature.model';

@Injectable({
  providedIn: 'root'
})
export class CatNatureService {

  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }

  getNatureList(): Observable<CatNatureModel[]> {
    return this.http.get<CatNatureModel[]>(
      `${this.baseUrl}/rest/v1/cat/natures`
    );
  }

  getCatNatureById(id: string): Observable<CatNatureModel> {
    return this.http.get<CatNatureModel>(
      `${this.baseUrl}/rest/v1/cat/natures/${id}`);
  }

  saveCatNature(nature: CatNatureModel): Observable<any> {
    return this.http.post(`${this.baseUrl}/rest/v1/cat/natures`, nature).pipe(
      catchError(
        err => {
          return throwError(err);
        } 
      )
    )
  }


  getNatureNapBySectorIdList(idSector): Observable<string[]> {
    return this.http.get<string[]>(
      `${this.baseUrl}/rest/v1/cat/natures/nap-codes/${idSector}`
    );
  }

  getNatureListByCatActivitySectorID(catActivtySectorId: string): Observable<CatNatureModel[]> {
    return this.http.get<CatNatureModel[]>(
      `${this.baseUrl}/rest/v1/cat/natures/sector/${catActivtySectorId}`
    );
  }

  loadNatureListByQuery(sector: string, nature: string, brand: string, type: string,isActive?:string): Observable<string[]> {
    let params = new HttpParams();
    params = params.append('sector', sector);
    params = params.append('nature', nature);
    params = params.append('brand', brand);
    params = params.append('type', type);
    params = params.append('isActive', isActive? isActive.toString(): 'false');

    return this.http.get<string[]>(`${this.baseUrl}/rest/v1/cat/natures/search/label`,{params:params});
  }

  getCatNatureValid(): Observable<CatNatureModel[]>{
    return this.http.get<CatNatureModel[]>(`${this.baseUrl}/rest/v1/cat/natures/valid/`);
  }
}
