import { environment } from 'src/environments/environment';
import { of } from 'rxjs';
import { LitigationModel } from '../models/litigation.model';
import { LitigationService } from './litigation-service';

describe('Service: LitigationService', () => {

    let baseUrl = environment.baseUrl;

    describe('Test: saveLitigation', () => {
        it('should return  litigation ', done => {
            let litigation: LitigationModel;

            const response: LitigationModel = { id: '1' } as LitigationModel;

            const httpMock = {
                post: jest.fn().mockReturnValue(of(response))
            };
            const serviceMock = new LitigationService(httpMock as any);
            serviceMock.saveLitigation(litigation).subscribe((data) => {
                expect(httpMock.post).toHaveBeenCalledWith(`${baseUrl}/rest/v1/litigation`, litigation);
                expect(httpMock.post).toBeDefined();
                expect(httpMock.post).toHaveBeenCalled();
                expect(data).toEqual(response);
                done();
            });
        });



    });
    describe('Test: findLitigationByValuationId', () => {
        it('should return  object', done => {
            let litigation: LitigationModel;

            const httpMock = {
                get: jest.fn().mockReturnValue(of(litigation))
            };
            const serviceMock = new LitigationService(httpMock as any);
            serviceMock.findLitigationByValuationId('5').subscribe((data) => {
                expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/litigation/valuation/5`);
                expect(httpMock.get).toBeDefined();
                expect(httpMock.get).toHaveBeenCalled();
                expect(data).toEqual(litigation);
                done();
            });
        });


    });


    describe('Test: findLitigationById', () => {
        it('should return  object', done => {
            let litigation: LitigationModel;

            const httpMock = {
                get: jest.fn().mockReturnValue(of(litigation))
            };
            const serviceMock = new LitigationService(httpMock as any);
            serviceMock.findLitigationById('5').subscribe((data) => {
                expect(httpMock.get).toHaveBeenCalledWith(`${baseUrl}/rest/v1/litigation/5`);
                expect(httpMock.get).toBeDefined();
                expect(httpMock.get).toHaveBeenCalled();
                expect(data).toEqual(litigation);
                done();
            });
        });


    });

});
