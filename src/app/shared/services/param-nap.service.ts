import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ParamNapModel } from '../models/param-nap.model';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ParamNapService {

 baseUrl = environment.baseUrl;
 
  constructor(private http: HttpClient) { }

  getAllParamNAPListByQuery(query) : Observable<ParamNapModel[]>{
    let params = new HttpParams();
    params = params.append('query', query);
    return this.http.get<ParamNapModel[]>(
      `${this.baseUrl}/rest/v1/param-nap` ,{ params: params }
    );
  }

}
