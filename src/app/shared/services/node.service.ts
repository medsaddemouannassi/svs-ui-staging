import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TreeNode } from 'primeng/api/treenode';
import { Subject } from 'rxjs';
import { environment } from '../../../environments/environment';
import { CatAssetTypeNavigationModel } from '../models/cat-asset-type-navigation.model';
import { CatBrandNavigationModel } from '../models/cat-brand-navigation.model';
import { CatDataNavigationModel } from '../models/cat-data-navigation.model';
import { CatNatureNavigationModel } from '../models/cat-nature-navigation.model';


@Injectable({
  providedIn: 'root'
})
export class NodeService {
  baseUrl = environment.baseUrl;

  nodeSubject: Subject<TreeNode> = new Subject<TreeNode>();

  nodeDeleteSubject: Subject<TreeNode> = new Subject<TreeNode>();

  nodeAddSubject: Subject<TreeNode> = new Subject<TreeNode>();
  constructor(private http: HttpClient) { }

  getActivitySectorsWithNature() {
    return this.http.get<CatDataNavigationModel>(`${this.baseUrl}/rest/v1/cat/sectors/navigation`)
  }

  getBrand(natureId: string) {
    return this.http.get<CatBrandNavigationModel[]>(`${this.baseUrl}/rest/v1/cat/brands/navigation/${natureId}`);

  }

  getAssetType(brandId: string) {
    return this.http.get<CatAssetTypeNavigationModel[]>(`${this.baseUrl}/rest/v1/cat/asset-types/navigation/${brandId}`);

  }

  getNature(sectorId: string) {
    return this.http.get<CatNatureNavigationModel[]>(`${this.baseUrl}/rest/v1/cat/natures/navigation/${sectorId}`);
  }


}
