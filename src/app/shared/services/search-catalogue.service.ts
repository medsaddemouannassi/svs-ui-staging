import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { PageValuationSearchCatalogue } from '../models/page-valuation-search-catalogue.model';
import { SearchCatalogueFilterModel } from '../models/catalogue-search-filter.model';

@Injectable({
  providedIn: 'root'
})
export class SearchCatalogueService {

  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }


 
  findSearchCatalogue(valuationSearchCatalogu:SearchCatalogueFilterModel,elementByPage:number,indexPage:number): Observable<PageValuationSearchCatalogue> {
    return this.http.post<PageValuationSearchCatalogue>(`${this.baseUrl}/rest/v1/search/catalogue?byPage=${elementByPage}&page=${indexPage}`,valuationSearchCatalogu);
  }

 
}
