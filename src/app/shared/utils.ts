import { FormGroup } from "@angular/forms";

export function markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach(control => {
      control.markAsTouched();
      if (control.controls) {
        markFormGroupTouched(control);
      }
    });
}

// format string to date
export function formatStringtoDate(dateStr) {
  var parts = dateStr.split("/");
  return new Date(parts[2], parts[1] - 1, parts[0],0,0,0,0);
}
export function formatDateToMatchForamt(dateStr: string) {
  dateStr = dateStr.split('/').join('');
  var d = Number(dateStr.length);
  return dateStr.substring(0, 2) + "/" + dateStr.substring(2, 4) + "/" + dateStr.substring(4, d)
}