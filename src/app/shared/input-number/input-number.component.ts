import { Component, EventEmitter, forwardRef, Input, Output } from '@angular/core';
import { ControlValueAccessor, FormControl, NG_VALIDATORS, NG_VALUE_ACCESSOR, Validator } from '@angular/forms';

@Component({
  selector: 'app-input-number',
  templateUrl: './input-number.component.html',
  styleUrls: ['./input-number.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputNumberComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => InputNumberComponent),
      multi: true
    }
  ]
})
export class InputNumberComponent implements   ControlValueAccessor,  Validator{
  @Input() selectedNumber: any;
  @Input() disabled: boolean;
  @Input() min;
  @Input() max;
  @Input() withToolTip: boolean  = false;
  @Output() inputModelChange = new EventEmitter<number>();
  @Output() exceedsRate = new EventEmitter<boolean>();
  @Input() invalidControl= false;

  isValid = true;
  @Input() symbol;
  @Input() allowNeg : boolean = false;

  get value() {

    return this.selectedNumber;
  }

  set value(value) {
    this.selectedNumber = value;
    this.onChange(value);
    this.onTouched();
  }


  more(): void {
    if (this.max && this.selectedNumber >= this.max) {
      this.selectedNumber = this.max;
      this.exceedsRate.emit(true);
      this.isValid = false
    } else if (Number.isNaN(this.selectedNumber)) {
      this.selectedNumber = null;
      this.exceedsRate.emit(true);
    } else if (this.selectedNumber == null) {
      this.selectedNumber = (this.min && this.min >= 1) ? this.min : 1;
      this.exceedsRate.emit(false);
      this.isValid = true
    } else if(this.selectedNumber != 100){
      this.selectedNumber++;
      this.exceedsRate.emit(false);
      this.isValid = true
    }
    this.inputModelChange.emit(this.selectedNumber);
    this.onChange(this.selectedNumber);
    this.onTouched();
  }

  less(): void {
    if (this.min && this.selectedNumber <= this.min) {
      this.selectedNumber = this.min;
      this.exceedsRate.emit(true);
    } else if (this.selectedNumber == null) {
      this.selectedNumber = (this.max && this.max <= -1) ? this.max : -1;
      this.exceedsRate.emit(false);
    } else if (Number.isNaN(this.selectedNumber)) {
      this.selectedNumber = null;
      this.exceedsRate.emit(true);
    } else if(this.selectedNumber != 0 || this.allowNeg){
      this.selectedNumber--;
      this.exceedsRate.emit(false);
    }
    this.inputModelChange.emit(this.selectedNumber);
    this.onChange(this.selectedNumber);
    this.onTouched();
  }

  changeValue(value: number): void {
    if (this.max && value > this.max) {
      this.selectedNumber = this.max;
      this.exceedsRate.emit(true);
    } else if (this.min && value < this.min) {
      this.selectedNumber = this.min;
      this.exceedsRate.emit(true);
    } else if (value && (Number.isNaN(value) || (value.toString()) === '-'.toString())) {
      this.selectedNumber = null;
      this.exceedsRate.emit(true);
    } else {
      this.selectedNumber = value;
      this.exceedsRate.emit(false);
    }
    this.inputModelChange.emit(value);
    this.onChange(this.selectedNumber);
    this.onTouched();
  }
  setDisabledState(disabled: boolean) {

   this.disabled = disabled;

  }
  writeValue(value) {

    this.selectedNumber = value
  }
  onChange: any = () => { // This is intentional
  };
  onTouched: any = () => { // This is intentional
  };

  registerOnChange(fn) {
    this.onChange = fn;
  }
  registerOnTouched(fn) {
    this.onTouched = fn;
  }

  validate(_: FormControl) {

    return this.isValid ? null : { exceedRate: { valid: false, }, };
  }


}
