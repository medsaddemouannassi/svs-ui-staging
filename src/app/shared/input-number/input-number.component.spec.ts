import { InputNumberComponent } from "./input-number.component";
import { FormControl } from '@angular/forms';

describe('Input number component', () => {
    let fixture: InputNumberComponent;





    beforeEach(async () => {


        fixture = new InputNumberComponent();


    });


    describe('test more',()=>{


        it('should add 1',()=>{

            fixture.min = 5;
            fixture.max = 10;
            fixture.selectedNumber = 9;

            fixture.more();

            expect(fixture.selectedNumber ).toEqual(10)
        })

        it('should not add 1',()=>{

            fixture.min = 5;
            fixture.max = 10;
            fixture.selectedNumber = 10;

            fixture.more();

            expect(fixture.selectedNumber ).toEqual(10)
        })

        it('should set min',()=>{

            fixture.min = 5;
            fixture.max = 10;
            fixture.selectedNumber = null;

            fixture.more();

            expect(fixture.selectedNumber ).toEqual(5)
        })

        it('should set 1',()=>{

            fixture.min = null;
            fixture.max = 10;
            fixture.selectedNumber = null;

            fixture.more();

            expect(fixture.selectedNumber ).toEqual(1)
        })

        it('should set 1',()=>{

            fixture.min = null;
            fixture.max = 10;
            fixture.selectedNumber = Number('-');

            fixture.more();

            expect(fixture.selectedNumber).toEqual(null)
        })


        it('should set maw',()=>{

            fixture.min = null;
            fixture.max = 10;
            fixture.selectedNumber = 11;

            fixture.more();

            expect(fixture.selectedNumber ).toEqual(10)
        })

    })

    describe('test less',()=>{


        it('should mines 1',()=>{

            fixture.min = 5;
            fixture.max = 10;
            fixture.selectedNumber = 7;

            fixture.less();

            expect(fixture.selectedNumber ).toEqual(6)
        })

        it('should be null',()=>{

            fixture.min = 5;
            fixture.max = 10;
            fixture.selectedNumber = Number.NaN;

            fixture.less();

            expect(fixture.selectedNumber ).toEqual(null)
        })

        it('should not mines 1',()=>{

            fixture.min = 5;
            fixture.max = 10;
            fixture.selectedNumber = 5;

            fixture.less();

            expect(fixture.selectedNumber ).toEqual(5)
        })

        it('should set min',()=>{

            fixture.min = 5;
            fixture.max = 10;
            fixture.selectedNumber = 4;

            fixture.less();

            expect(fixture.selectedNumber ).toEqual(5)
        })

        it('should set max',()=>{

            fixture.min = null;
            fixture.max = -5;
            fixture.selectedNumber = null;

            fixture.less();

            expect(fixture.selectedNumber ).toEqual(-5)

            fixture.max = null;
            fixture.selectedNumber = null;

            fixture.less();

            expect(fixture.selectedNumber ).toEqual(-1)
        })

      

    })


    describe('test valuechange',()=>{


        it('should selectedNumber be 10',()=>{

            fixture.min = 5;
            fixture.max = 10;

            fixture.changeValue(12);

            expect(fixture.selectedNumber ).toEqual(10)
        })

        it('should selectedNumber be 5',()=>{

            fixture.min = 5;
            fixture.max = 10;

            fixture.changeValue(3);

            expect(fixture.selectedNumber ).toEqual(5)
        })

       

        it('should selectedNumber be 7',()=>{

            fixture.min = 5;
            fixture.max = 10;

            fixture.changeValue(7);

            expect(fixture.selectedNumber ).toEqual(7)
        })


      

    })

    describe('Test setDisabledState()', ()=>{
        it('should disabled  be true', ()=>{
            fixture.setDisabledState(true);
            expect(fixture.disabled).toEqual(true);
        });
    });

    describe('Test writeValue', () => {
        it('should  write value', () => {
            let value = 30;
            fixture.writeValue(value);
            expect(fixture.value).toEqual(value);
        });
    });

    describe('Test registerOnChange', () => {
        it('should get register', () => {
            let test;
            fixture.registerOnChange(test);
            expect(fixture.onChange).not.toEqual(() => {      
                 //this is intentional
            });
            expect(fixture.onChange).toEqual(test);

        });
    });

    describe('Test registerOnTouched', () => {
        it('should get register', () => {
            fixture.registerOnTouched(test);
            expect(fixture.onTouched).not.toEqual(() => {
                //this is intentional
            });
            expect(fixture.onTouched).toEqual(test);
        });
    });

    describe('Test validate', () => {
        it('should retrun null', () => {
            let formControl: FormControl = new FormControl();
            expect(fixture.validate(formControl)).toEqual(null);
        });

    });

    describe('Test set value', () => {
        it('should set value', () => {
            let num= 3;
            fixture.value=num;
            expect(fixture.selectedNumber).toEqual(num);
        })
    });
})