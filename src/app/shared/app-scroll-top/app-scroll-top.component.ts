import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';

@Component({
  selector: 'app-app-scroll-top',
  templateUrl: './app-scroll-top.component.html',
  styleUrls: ['./app-scroll-top.component.css']
})
export class AppScrollTopComponent implements OnInit {

  constructor(@Inject(DOCUMENT) private document: Document) { }
 
  scrollToTop() {
    (function smoothscroll() {
      var currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
      if (currentScroll > 0) {
        window.requestAnimationFrame(smoothscroll);
        window.scrollTo(0, currentScroll - (currentScroll / 8));
      }
    })();
  }
  
  ngOnInit(): void {
    //ngOnInit
  }

}
