export function convertFile(file) {
    return new Promise((resolve) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
            resolve(reader.result)

        }
    });
}

export function generateFileName(fileName){
    return encodeURI(Date.now()+'-'+fileName);

}