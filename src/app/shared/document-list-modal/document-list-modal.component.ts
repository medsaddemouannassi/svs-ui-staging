import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DocumentConstant } from '../document-constant';
import { validateDocument } from '../documents/document-validator';
import { ValuationDocumentModel } from '../models/valuationDocument.model';
import { DocInsertionService } from './doc-insertion.service';

@Component({
  selector: 'app-document-list-modal',
  templateUrl: './document-list-modal.component.html',
  styleUrls: ['./document-list-modal.component.css']
})
export class DocumentListModalComponent implements OnInit {

  @Input() documentsList: { docIndex: ValuationDocumentModel, doc: File }[] = []
  @Input() docSubTypesListParams;
  @Output() onConfirm = new EventEmitter<{ docIndex: ValuationDocumentModel, doc: File }[]>();
  @Output() onExit = new EventEmitter<ValuationDocumentModel>();

  constructor(public activeModal: NgbActiveModal,private docInsertionService : DocInsertionService) { }
  ngOnInit() {
    this.docInsertionService.documentInsert.subscribe(doc=>
    this.documentsList.push(doc));
  }

  confirmModal() {
    this.onConfirm.emit(this.documentsList );
  }
  exitModal() {
    this.onExit.emit(null);
  }

  updateIndex(i: number) {

    if (this.documentsList[i].docIndex.docSubType == 'E303') {
      this.documentsList[i].docIndex.docFamily = DocumentConstant.FAMILY_VALUATION;
      this.documentsList[i].docIndex.docType = DocumentConstant.TYPE_ENVIRONMENTAL_VALUATION;

    }

    if (this.documentsList[i].docIndex.docSubType == 'C217' || this.documentsList[i].docIndex.docSubType == 'C218' || this.documentsList[i].docIndex.docSubType == 'C224') {
      this.documentsList[i].docIndex.docFamily = DocumentConstant.FAMILY_CONTRACT;
      this.documentsList[i].docIndex.docType = DocumentConstant.TYPE_INTERNAL_CONTRACT;

    }
  }
  uploadDocument(event) {
    const target = event.target as HTMLInputElement;
    let filesToAdd: File[] = [];
    let validDoc = true;
    for (let i = 0; i < target.files.length; i++) {
      validDoc = validDoc && validateDocument(target.files[i]);
      filesToAdd.push(target.files[i]);
    }
    if (validDoc) {
      filesToAdd.forEach(el => {
        let index = {} as ValuationDocumentModel;

        index.docFamily = DocumentConstant.FAMILY_VALUATION;
        index.docType = DocumentConstant.TYPE_ENVIRONMENTAL_VALUATION;
        index.docSubType = DocumentConstant.SUB_TYPE_OTHER_VALUATION;
        index.isQualified = false;
        index.docName = el.name;
        this.documentsList.push({ docIndex: index, doc: el })
      });
    }
  }
  onBlurDocument(index: number) {
    this.documentsList[index].doc = new File([this.documentsList[index].doc], this.documentsList[index].docIndex.docName,
       { type: this.documentsList[index].doc.type });
  }

  delete(i){
    this.documentsList.splice(i,1);
  }
}
