import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DocumentConstant } from '../document-constant';
import { ValuationDocumentModel } from '../models/valuationDocument.model';
import { DocInsertionService } from './doc-insertion.service';
import { DocumentListModalComponent } from './document-list-modal.component';


describe('documentListModal', () => {
  let fixture : DocumentListModalComponent;
  let ngbActiveModalMock: NgbActiveModal;
  let docInsertionService: DocInsertionService;



  beforeEach(() => {
    ngbActiveModalMock = new NgbActiveModal();
    docInsertionService = new DocInsertionService();

    fixture = new DocumentListModalComponent (ngbActiveModalMock,docInsertionService);

    fixture.ngOnInit();
    docInsertionService.documentInsert.next( {
      doc : {name: 'file3.png', size: 17567} as File,
      docIndex:{
        docName:'file3.png',
        docFamily : 'E',
        docType:'E1',
        docSubType :'E3031',
        isQualified: false

      } as ValuationDocumentModel
    })
  });

  it('should create', () => {
    expect(fixture).toBeTruthy();
  });
  it('should confirmModal', () => {
    fixture.confirmModal();
    expect(fixture.onConfirm).toHaveBeenCalled;
  });
  
  it('should exitModal', () => {
    fixture.exitModal();
    expect(fixture.onConfirm).toHaveBeenCalled;
    expect(fixture.onExit).toHaveBeenCalled;
  });
  it('should update type E', () => {
    fixture.documentsList[0].docIndex.docSubType = 'E303'
    fixture.updateIndex(0);
    expect(fixture.documentsList[0].docIndex.docFamily).toEqual(DocumentConstant.FAMILY_VALUATION);
  });
  it('should update type C', () => {
    fixture.documentsList[0].docIndex.docSubType = 'C217'
    fixture.updateIndex(0);
    expect(fixture.documentsList[0].docIndex.docFamily).toEqual(DocumentConstant.FAMILY_CONTRACT);
  });

  it('should upload', () => {
    let file = { name: 'file3.png', size: 17567 } as File
    let input = {
     
      target: { files: [file] }

    } 

    fixture.uploadDocument(input);
    expect(fixture.documentsList.length).toEqual(2);
  });

  it('should update doc name', () => {
    fixture.documentsList[0].docIndex.docName = 'xxx'
    fixture.onBlurDocument(0);
    expect(fixture.documentsList[0].doc.name).toEqual('xxx');
  });

  it('should remove', () => {
    fixture.delete(0);
    expect(fixture.documentsList.length).toEqual(0);
  });

  
});
