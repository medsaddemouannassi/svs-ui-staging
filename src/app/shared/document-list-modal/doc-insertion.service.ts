import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ValuationDocumentModel } from '../models/valuationDocument.model';

@Injectable({
  providedIn: 'root'
})
export class DocInsertionService {

  documentInsert:Subject<{ docIndex: ValuationDocumentModel, doc: File }> = new Subject<{ docIndex: ValuationDocumentModel, doc: File }>();

}
