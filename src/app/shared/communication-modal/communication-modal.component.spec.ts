
import { FormBuilder } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { communicationThemes } from '../communication-themes';
import { ParamSettingModel } from '../models/param-setting.model';
import { Role } from '../models/roles.enum';
import { ValuationRequestWorkflowModel } from '../models/valuation-request-workflow.model';
import { PagesConstants } from '../pages-constant';
import { StatusConstant } from '../status-constant';
import { CommunicationModalComponent } from './communication-modal.component';

describe('CommunicationModalComponent', () => {
  let fixture: CommunicationModalComponent;
  let activeModalMock: NgbActiveModal;
  let routerMock: any;
  let fbMock: FormBuilder;

  routerMock = {
    findValuationSearch: jest.fn().mockReturnValue((true))
  };
  fbMock = new FormBuilder();

  let communicationForm = fbMock.group({
    theme: [null,],
    comment: [null,],
    history: [null,],
    actor: [null,]
  });

  beforeEach(() => {
    activeModalMock = new NgbActiveModal();
    fixture = new CommunicationModalComponent(activeModalMock, routerMock, fbMock);

  });
  describe(' test ngoninit', () => {
    it('should create', () => {
      fixture.communicationForm = communicationForm;
      fixture.roles =
        [Role.ADMIN_SE, Role.CA, Role.CONSULTATION, Role.PASS_ADMINISTRATEUR_OSEO, Role.SVS_CONTENTIEUX, Role.SVS_EXPERT_SE]
      spyOn(fixture, 'getThemesByRoleAndStatus').and.returnValue(communicationThemes);
      fixture.ngOnInit();
      expect(fixture.themes).toEqual(communicationThemes);
      expect(fixture).toBeTruthy();
    });

  })

  describe('confirmModal', () => {
    it('should confirmModal', () => {
      fixture.roles =
        [Role.ADMIN_SE, Role.CA, Role.CONSULTATION, Role.PASS_ADMINISTRATEUR_OSEO, Role.SVS_CONTENTIEUX, Role.SVS_EXPERT_SE];

      jest.spyOn(fixture.onConfirm, 'emit').mockImplementation();

      fixture.ngOnInit();
      fixture.confirmModal();

      expect(fixture.onConfirm.emit).toHaveBeenCalled();
    });
  })

  describe('exitModal', () => {

    it('should exitModal', () => {
      jest.spyOn(fixture.onExit, 'emit').mockImplementation();

      fixture.exitModal();
      expect(fixture.onExit.emit).toHaveBeenCalled();
    });
  });
  describe('submit', () => {

    it('should submit', () => {
      jest.spyOn(fixture.onExit, 'emit').mockImplementation();
      fixture.communicationForm = communicationForm;
      fixture.communicationForm.controls['theme'].setValue(communicationThemes[0]);
      fixture.page = "CatalogAdd";
      fixture.roles =
        [Role.ADMIN_SE, Role.CA, Role.CONSULTATION, Role.PASS_ADMINISTRATEUR_OSEO, Role.SVS_CONTENTIEUX, Role.SVS_EXPERT_SE]

      fixture.submit();
    });
    it('should submit form invalid', () => {
      jest.spyOn(fixture.onExit, 'emit').mockImplementation();
      fixture.communicationForm = communicationForm;
      fixture.communicationForm.controls['theme'].setValue(communicationThemes[0]);
      fixture.page = "CatalogAdd";
      fixture.roles =
        [Role.ADMIN_SE, Role.CA, Role.CONSULTATION, Role.PASS_ADMINISTRATEUR_OSEO, Role.SVS_CONTENTIEUX, Role.SVS_EXPERT_SE]
      fixture.communicationForm.controls['comment'].setErrors({ 'incorrect': true });
      fixture.submit();
    });
  })

describe(' test ngAfterViewInit', () => {
    it('should update form value with initial theme', () => {
      fixture.status = StatusConstant.WAIT_EXP_VAL
      fixture.page = PagesConstants.RECEPTION;
      fixture.roles = [Role.SVS_EXPERT_SE];
      fixture.valRequestHistory = [
        { comment: 'a comment', status: { value: StatusConstant.WAIT_INFO_EXP_CA } as ParamSettingModel } as ValuationRequestWorkflowModel,
        { comment: 'a comment', status: { value: StatusConstant.RESP_INFO_CA_EXP } as ParamSettingModel } as ValuationRequestWorkflowModel,

      ]
      fixture.ngOnInit();
      fixture.themes = [
        {
          Theme: "Réponse à la demande", Specs: [

            { page: PagesConstants.RECEPTION, user: Role.SVS_EXPERT_SE, actor: [Role.CA], history: [StatusConstant.WAIT_INFO_EXP_CA, StatusConstant.RESP_INFO_CA_EXP], status: [StatusConstant.WAIT_EXP_VAL],lastStatus: [] , action: null },

          ]
        }]

      fixture.initialTheme = fixture.themes[0]


      fixture.ngAfterViewInit();
      expect(fixture.communicationForm.controls['theme'].value).toEqual(fixture.themes[0]);
    });


    it('should update form value without initial theme', () => {
      fixture.status = StatusConstant.WAIT_EXP_VAL
      fixture.page = PagesConstants.RECEPTION;
      fixture.roles = [Role.SVS_EXPERT_SE];
      fixture.valRequestHistory = [
        { comment: 'a comment', status: { value: StatusConstant.WAIT_INFO_FIL_EXP } as ParamSettingModel } as ValuationRequestWorkflowModel,
        { comment: 'a comment', status: { value: StatusConstant.RESP_INFO_EXP_FIL } as ParamSettingModel } as ValuationRequestWorkflowModel,
      ]
      fixture.ngOnInit();
      fixture.themes = [
        {
          Theme: "Réponse à demande de précision", Specs: [

            { page: PagesConstants.RECEPTION, user: Role.SVS_EXPERT_SE, actor: [Role.ADMIN_SE], history: [StatusConstant.WAIT_INFO_FIL_EXP, StatusConstant.RESP_INFO_EXP_FIL], status: [StatusConstant.WAIT_EXP_VAL],lastStatus: [] , action: null },

          ]
        }]



      fixture.ngAfterViewInit();
      expect(fixture.communicationForm.controls['theme'].value).toEqual(fixture.themes[0]);
    });

    it('should not update ', () => {
      fixture.status = StatusConstant.WAIT_EXP_VAL
      fixture.page = PagesConstants.RECEPTION;
      fixture.roles = [Role.SVS_EXPERT_SE];
      fixture.valRequestHistory = [
        { comment: 'a comment', status: { value: StatusConstant.WAIT_INFO_FIL_EXP } as ParamSettingModel } as ValuationRequestWorkflowModel,
        { comment: 'a comment', status: { value: StatusConstant.RESP_INFO_EXP_FIL } as ParamSettingModel } as ValuationRequestWorkflowModel,
      ]
      fixture.ngOnInit();
      fixture.themes = [
        {
          Theme: "Réponse à demande de précision", Specs: [

            { page: PagesConstants.RECEPTION, user: Role.SVS_EXPERT_SE, actor: [Role.ADMIN_SE], history: [StatusConstant.WAIT_INFO_FIL_EXP, StatusConstant.RESP_INFO_EXP_FIL], status: [StatusConstant.WAIT_EXP_VAL],lastStatus: [] , action: null },

          ]
        },
        {
          Theme: "Demande de précisions à la filière", Specs: [
            { page: PagesConstants.RECEPTION, user: Role.SVS_EXPERT_SE, history: [StatusConstant.WAIT_INFO_FIL_EXP, StatusConstant.RESP_INFO_EXP_FIL, StatusConstant.WAIT_INFO_EXP_FIL, StatusConstant.RESP_INFO_FIL_EXP], actor: [Role.ADMIN_SE], status: [StatusConstant.WAIT_EXP_VAL, StatusConstant.WAIT_VALID_MAN_EXP_QUAL, StatusConstant.VAL_FIL_REJ],lastStatus: [] , action: StatusConstant.WAIT_INFO_EXP_FIL },

          ]
        }
      ]
      fixture.ngAfterViewInit();
      expect(fixture.communicationForm.controls['theme'].value).toBeNull();
    });
  })

  describe('loadUserByTheme', () => {



  it('should update activeHistory and set message from expert when status is RESP_INFO_EXP_FIL ', () => {
    fixture.communicationForm = communicationForm;
    fixture.valRequestHistory = [ {
      comment: "RESP_INFO_EXP_FIL",
      status: { value: StatusConstant.RESP_INFO_EXP_FIL } as ParamSettingModel
    } as ValuationRequestWorkflowModel ]
    spyOn(fixture, 'getSpecsByPageAndRole').and.returnValues([Role.ADMIN_SE])

    let theme = {
      Theme: "Demande d'information", Specs: [

        { history: [StatusConstant.RESP_INFO_EXP_FIL,StatusConstant.WAIT_EXP_VAL] },

      ]
    }
    fixture.communicationForm.controls['theme'].setValue(theme);

    fixture.loadUserByTheme();
    expect(fixture.activeHistory[0].from).toEqual(Role.SVS_EXPERT_SE)
  });

  it('should update activeHistory and set message from expert when status is WAIT_INFO_EXP_FIL', () => {
    fixture.communicationForm = communicationForm;
    fixture.valRequestHistory = [{
      comment: "WAIT_INFO_EXP_FIL ",
      status: { value: StatusConstant.WAIT_INFO_EXP_FIL  } as ParamSettingModel
    } as ValuationRequestWorkflowModel]
    spyOn(fixture, 'getSpecsByPageAndRole').and.returnValues([Role.SVS_EXPERT_SE])

    let theme = {
      Theme: "Demande d'information", Specs: [

        { history: [StatusConstant.WAIT_INFO_EXP_FIL ] },

      ]
    }
    fixture.communicationForm.controls['theme'].setValue(theme);

    fixture.loadUserByTheme();
    expect(fixture.activeHistory[0].from).toEqual(Role.SVS_EXPERT_SE)
  });

  it('should update activeHistory and set message from expert when status is WAIT_INFO_EXP_CA ', () => {
    fixture.communicationForm = communicationForm;
    fixture.valRequestHistory = [{
      comment: "WAIT_INFO_EXP_CA  ",
      status: { value: StatusConstant.WAIT_INFO_EXP_CA   } as ParamSettingModel
    } as ValuationRequestWorkflowModel]
    spyOn(fixture, 'getSpecsByPageAndRole').and.returnValues([Role.SVS_EXPERT_SE])

    let theme = {
      Theme: "Demande d'information", Specs: [

        { history: [StatusConstant.WAIT_INFO_EXP_CA  ] },

      ]
    }
    fixture.communicationForm.controls['theme'].setValue(theme);

    fixture.loadUserByTheme();
    expect(fixture.activeHistory[0].from).toEqual(Role.SVS_EXPERT_SE)
  });

  it('should update activeHistory and set message from CA when status is RESP_INFO_CA_EXP ', () => {
    fixture.communicationForm = communicationForm;
    fixture.valRequestHistory = [{
      comment: "RESP_INFO_CA_EXP  ",
      status: { value: StatusConstant.RESP_INFO_CA_EXP   } as ParamSettingModel
    } as ValuationRequestWorkflowModel]
    spyOn(fixture, 'getSpecsByPageAndRole').and.returnValues([Role.CA])

    let theme = {
      Theme: "Demande d'information", Specs: [

        { history: [StatusConstant.RESP_INFO_CA_EXP  ] },

      ]
    }
    fixture.communicationForm.controls['theme'].setValue(theme);

    fixture.loadUserByTheme();
    expect(fixture.activeHistory[0].from).toEqual(Role.CA)
  });
  it('should update activeHistory and set message from Admin when status is WAIT_INFO_FIL_EXP ', () => {
    fixture.communicationForm = communicationForm;
    fixture.valRequestHistory = [{
      comment: "WAIT_INFO_FIL_EXP",
      status: { value: StatusConstant.WAIT_INFO_FIL_EXP   } as ParamSettingModel
    } as ValuationRequestWorkflowModel]
    spyOn(fixture, 'getSpecsByPageAndRole').and.returnValues([Role.ADMIN_SE])

    let theme = {
      Theme: "Demande d'information", Specs: [

        { history: [StatusConstant.WAIT_INFO_FIL_EXP  ] },

      ]
    }
    fixture.communicationForm.controls['theme'].setValue(theme);

    fixture.loadUserByTheme();
    expect(fixture.activeHistory[0].from).toEqual(Role.ADMIN_SE)
  });

  it('should update activeHistory and set message from Admin when status is RESP_INFO_FIL_EXP  ', () => {
    fixture.communicationForm = communicationForm;
    fixture.valRequestHistory = [{
      comment: "RESP_INFO_FIL_EXP ",
      status: { value: StatusConstant.RESP_INFO_FIL_EXP    } as ParamSettingModel
    } as ValuationRequestWorkflowModel]
    spyOn(fixture, 'getSpecsByPageAndRole').and.returnValues([Role.ADMIN_SE])

    let theme = {
      Theme: "Demande d'information", Specs: [

        { history: [StatusConstant.RESP_INFO_FIL_EXP   ] },

      ]
    }
    fixture.communicationForm.controls['theme'].setValue(theme);

    fixture.loadUserByTheme();
    expect(fixture.activeHistory[0].from).toEqual(Role.ADMIN_SE)
  });

  it('should load user by theme invalid', () => {
    fixture.communicationForm = communicationForm ;
    fixture.communicationForm.controls['theme'].setValue(null) ;
    fixture.page = "CatalogAdd";
    fixture.roles =
      [Role.ADMIN_SE, Role.CA, Role.CONSULTATION, Role.PASS_ADMINISTRATEUR_OSEO, Role.SVS_CONTENTIEUX, Role.SVS_EXPERT_SE]
    fixture.loadUserByTheme();
  });


});
});
