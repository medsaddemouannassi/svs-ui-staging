import { AfterViewInit, Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CommunicationModal, getSpecsByPageAndRole, getThemesByRoleAndStatus, HistoryMessage } from '../communication-themes';
import { CatRequestWaiting } from '../models/catalog-add-request.model';
import { Role } from '../models/roles.enum';
import { ValuationRequestWorkflowModel } from '../models/valuation-request-workflow.model';
import { StatusConstant } from '../status-constant';
import { markFormGroupTouched } from '../utils';

@Component({
  selector: 'app-communication-modal',
  templateUrl: './communication-modal.component.html',
  styleUrls: ['./communication-modal.component.css']
})
export class CommunicationModalComponent implements OnInit, AfterViewInit {


  @Output() onConfirm = new EventEmitter<any>();
  @Output() onExit = new EventEmitter<boolean>();
  themes: CommunicationModal[] = [];
  initialTheme = null;
  page: string;
  roles: Role[];
  status: string;
  lastStatus:string;
  actors: string[] = [];
  communicationForm: FormGroup;
  actorOptions: any[] = [];
  catRequest: CatRequestWaiting;
  showForm = false;

  valRequestHistory: ValuationRequestWorkflowModel[] = [];

  activeHistory: HistoryMessage[] = []

  constructor(public activeModal: NgbActiveModal, public router: Router,
    private fb: FormBuilder

  ) {
  }

  ngOnInit(): void {

    this.themes = this.getThemesByRoleAndStatus(this.roles, this.status,this.lastStatus, this.page);

    this.communicationForm = this.fb.group({
      theme: [null, Validators.required],
      actor: [null, Validators.required],
      comment: [null, Validators.required],
    });

    this.showForm = true;

  }

  ngAfterViewInit() {

    if (this.initialTheme) {

      this.themes[this.themes.findIndex(t => t.Theme == this.initialTheme.Theme)] = this.initialTheme
      
      this.communicationForm.patchValue({ theme: this.initialTheme })
      
      this.loadUserByTheme();

    } else {
      if (this.themes.length == 1) {
        this.communicationForm.patchValue({ theme: this.themes[0] })
        this.loadUserByTheme();
      }
    }

  }




  loadUserByTheme() {
    this.activeHistory = []
    this.actorOptions = [];
    this.communicationForm.patchValue({ actor: null });
    if (this.communicationForm.controls['theme'].value) {
      this.actorOptions = this.getSpecsByPageAndRole((this.communicationForm.controls['theme'].value).Specs, this.page, this.roles);
      this.communicationForm.patchValue({
        actor: this.actorOptions[0]
      });


      this.activeHistory = this.valRequestHistory
        .filter(valHist => ((this.communicationForm.controls['theme'].value).Specs[0].history as string[]).includes(valHist.status.value))
        .map(valHist => {

          let message = {} as HistoryMessage;
          message.message = valHist.comment;
          message.date = valHist.createdDate;
          switch (valHist.status.value) {
            case StatusConstant.WAIT_INFO_EXP_CA:
            case StatusConstant.WAIT_INFO_EXP_FIL:
            case StatusConstant.RESP_INFO_EXP_FIL: {


              message.from = Role.SVS_EXPERT_SE;
              break;
            }
            case StatusConstant.RESP_INFO_CA_EXP: {
              message.from = Role.CA;
              break;
            }

            case StatusConstant.WAIT_INFO_FIL_EXP:
            case StatusConstant.RESP_INFO_FIL_EXP: {


              message.from = Role.ADMIN_SE;
              break;
            }

            default:
          }

          return message;
        })

      //  this.activeHistory.reverse()


      if((this.communicationForm.controls['theme'].value).Specs[0].action){
        this.activeHistory = this.activeHistory.length > 0 ? [this.activeHistory[0]] : [];
        this.communicationForm.get('comment').enable();

      } else{
        this.activeHistory =this.activeHistory.slice(0,2);

        this.activeHistory.reverse();


        this.communicationForm.get('comment').disable();


      }



    }
  }
  submit() {
    this.communicationForm.updateValueAndValidity();

    if (this.communicationForm.invalid) {
      markFormGroupTouched(this.communicationForm);
    }
    else {
      this.confirmModal();
      this.activeModal.dismiss('Submited');
      this.exitModal();
    }
  }
  confirmModal() {
    this.onConfirm.emit(this.communicationForm.getRawValue())
  }
  exitModal() {
    this.onExit.emit(false);
  }


  getThemesByRoleAndStatus(roles, status,lastStatus, page): any[] {
    return getThemesByRoleAndStatus(roles, status,lastStatus, page);
  }

  getSpecsByPageAndRole(specs: any[], page:string, roles : any []){

  return getSpecsByPageAndRole(specs,page,roles)
  }

}
