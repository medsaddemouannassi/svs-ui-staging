import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem } from 'primeng/api/menuitem';
import { Subscription } from 'rxjs';
import { Role } from './shared/models/roles.enum';
import { AuthService } from './shared/services/auth.service';
import { LoadingService } from './shared/services/loading-service.service';
import { SharedCommunicationDataService } from './shared/services/shared-communication-data.service';
import { ValuationSharedService } from './shared/services/valuation-shared-service';
import { SvsDescCollabService } from './shared/services/svs-desc-collab.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  items: MenuItem[] = [];

  title = 'bpi-ui';
  showCatalogueBtn = false;
  showAccessRightsBtn = false;
  lockBtnCommunication:string;
  sharedCommunicationSubscription: Subscription;
  loading$ = this.loader.loading$;

  constructor(private authService: AuthService,
    private svsDescCollabService: SvsDescCollabService,
    private valuationSharedService:ValuationSharedService,
    private router: Router,
    private data: SharedCommunicationDataService,public loader: LoadingService) {



    if ((window.location.href.includes('localhost') || window.location.href.includes('bpifrance.fr')) && window.location.href.includes('?state') && (!window.location.href.includes('/#/?state'))) {
      var splitted = window.location.href.split('?state');
      document.location.replace(splitted[0] + '#/?state' + splitted[1]);

    }
    else {
      authService.runInitialLoginSequence().then(resp => {
        this.checkBtnRoles();
      });

    }
    this.sharedCommunicationSubscription = this.data.currentBtnStatus.subscribe(status => this.lockBtnCommunication = status)
    if (!window.location.href.includes('reception' || 'result')){
     // this.showAccessRightsBtn = false;
    }
  }
  checkBtnRoles(){
    if (this.authService.currentUserRolesList.includes(Role.ADMIN_SE)) {
      this.showCatalogueBtn = true;
      this.showAccessRightsBtn = true;
    }
    else if (this.authService.currentUserRolesList.includes(Role.SVS_EXPERT_SE)) {
      this.svsDescCollabService.isDescManager(this.authService.identityClaims['mat']).subscribe(res => {
        if (res) {
          this.showAccessRightsBtn = true;
        }
      });
    }
  }

  triggerCommunication(){
    this.data.triggerCommunication.next()
  }
  redirectToInfo(){
    window.open(
      'https://com15.sharepoint.com/sites/EVE',
      '_blank' // <- This is what makes it open in a new window.
    );
    
  }
  ngOnDestroy() {
    this.sharedCommunicationSubscription.unsubscribe();
  }
  resetAllSharedValuationValues(){
    this.valuationSharedService.resetAllValue();
  }
}

