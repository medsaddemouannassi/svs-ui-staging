import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CatalogAddRequestsComponent } from './equipment/catalog/catalog-add-requests/catalog-add-requests.component';
import { CatalogComponent } from './equipment/catalog/catalog.component';
import { MenuCatActivitySectorComponent } from './equipment/catalog/menu-cat-activity-sector/menu-cat-activity-sector.component';
import { MenuCatBrandComponent } from './equipment/catalog/menu-cat-brand/menu-cat-brand.component';
import { MenuCatNatureComponent } from './equipment/catalog/menu-cat-nature/menu-cat-nature.component';
import { MenuCatTypeComponent } from './equipment/catalog/menu-cat-type/menu-cat-type.component';
import { DescManagementComponent } from './equipment/desc-management/desc-management.component';
import { RequestComponent } from './equipment/request/request.component';
import { ResultExpertComponent } from './equipment/result/expert/result-expert.component';
import { SearchComponent } from './equipment/search/search.component';
import { ReceptionContentieuxComponent } from './equipment/valorisation/contentieux/reception-contentieux/reception-contentieux.component';
import { ReceptionComponent } from './equipment/valorisation/expert/reception.component';
import { BibliothequesComponent } from './shared/bibliotheques/bibliotheques.component';
import { Role } from './shared/models/roles.enum';
import { Page401Component } from './shared/page-401/page-401.component';
import { AuthGuardWithForcedLogin } from './shared/services/auth-guard.service';
import { RolesGuard } from './shared/services/roles-guard.service';
import { UnderConstructionComponent } from './shared/under-construction/under-construction.component';

const routes: Routes = [

  {
    path: 'search',
    component: SearchComponent,
    canActivate: [AuthGuardWithForcedLogin, RolesGuard],
    data: { role: [Role.SVS_EXPERT_SE, Role.CA, Role.ADMIN_SE, Role.CONSULTATION, Role.SVS_CONTENTIEUX] }
  },
  {
    path: 'reception/expert/:id',
    component: ReceptionComponent,
    canActivate: [AuthGuardWithForcedLogin, RolesGuard],
    data: { role: [Role.SVS_EXPERT_SE, Role.ADMIN_SE] }
  },
  {
    path: 'simulation/:idRet/:isRetUnderwriter',
    component: ReceptionComponent,
    canActivate: [AuthGuardWithForcedLogin, RolesGuard],
    data: { role: [Role.SVS_EXPERT_SE, Role.ADMIN_SE] }
  },
  {
    path: 'request/:idRefTiers/:idBcp',
    component: RequestComponent,
    canActivate: [AuthGuardWithForcedLogin, RolesGuard],
    data: { role: [Role.CA] }
  },
  {
    path: 'request/:idRequest',
    component: RequestComponent,
    canActivate: [AuthGuardWithForcedLogin, RolesGuard],
    data: { role: [Role.CA, Role.ADMIN_SE,Role.CONSULTATION, Role.SVS_EXPERT_SE] }
  },
  { path: 'biblio', component: BibliothequesComponent },
  {
    path: 'result/expert/:idValuation', component: ResultExpertComponent, canActivate: [AuthGuardWithForcedLogin, RolesGuard], data: { role: [Role.SVS_EXPERT_SE, Role.ADMIN_SE] }
  },

  {
    path: 'result/contentieux/:idLitigation', component: UnderConstructionComponent, canActivate: [AuthGuardWithForcedLogin, RolesGuard], data: { role: [Role.SVS_CONTENTIEUX] }
  },
  { path: '401', component: Page401Component },
  { path: 'equipement/cotations/:idcotationId', component: UnderConstructionComponent },

  {
    path: 'desc', component: DescManagementComponent,
    canActivate: [AuthGuardWithForcedLogin, RolesGuard],
    data: { role: [Role.ADMIN_SE, Role.SVS_EXPERT_SE] }

  },
  {
    path: 'desc/:idHistory', component: DescManagementComponent,
    canActivate: [AuthGuardWithForcedLogin, RolesGuard],
    data: { role: [Role.ADMIN_SE] }

  },
  {
    path: 'catalog',
    component: CatalogComponent,
    children: [
      { path: 'sector/:id', component: MenuCatActivitySectorComponent },
      { path: 'nature/:id', component: MenuCatNatureComponent },
      { path: 'brand/:id', component: MenuCatBrandComponent },
      { path: 'type/:id', component: MenuCatTypeComponent },
      { path: 'brand/add/:id', component: MenuCatBrandComponent },
      { path: 'type/add/:id', component: MenuCatTypeComponent },
      { path: '', component: CatalogAddRequestsComponent },
      { path: 'request/:id', component: CatalogAddRequestsComponent },
      { path: 'type/add/:id', component: MenuCatTypeComponent },
      { path: 'nature/add/:id', component: MenuCatNatureComponent },
    ],
    canActivate: [AuthGuardWithForcedLogin, RolesGuard],
    data: { role: [Role.ADMIN_SE] }
  },
  {
    path: 'contentieux/:id',
    component: UnderConstructionComponent,
    canActivate: [AuthGuardWithForcedLogin, RolesGuard],
    data: { role: [Role.SVS_CONTENTIEUX] }
  },



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
