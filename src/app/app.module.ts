import { CommonModule, HashLocationStrategy, LocationStrategy } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { AuthConfig, OAuthModule, OAuthModuleConfig, OAuthStorage, ValidationHandler } from 'angular-oauth2-oidc';
import { JwksValidationHandler } from 'angular-oauth2-oidc-jwks';
import { TabModule } from 'angular-tabs-component';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { NgxFileDropModule } from 'ngx-file-drop';
import { IConfig, NgxMaskModule } from 'ngx-mask';
import { ButtonModule } from 'primeng/button';
import { ContextMenuModule } from 'primeng/contextmenu';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { MenubarModule } from 'primeng/menubar';
import { MultiSelectModule } from 'primeng/multiselect';
import { ProgressBarModule } from 'primeng/progressbar';
import { SliderModule } from 'primeng/slider';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { TooltipModule } from 'primeng/tooltip';
import { TreeModule } from 'primeng/tree';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CatDiscountsAverageComponent } from './equipment/catalog/cat-discount-average/cat-discount-average.component';
import { CatalogAddRequestsComponent } from './equipment/catalog/catalog-add-requests/catalog-add-requests.component';
import { CatalogNewDiscountsComponent } from './equipment/catalog/catalog-new-discounts/catalog-new-discounts.component';
import { CatalogOldDiscountsComponent } from './equipment/catalog/catalog-old-discounts/catalog-old-discounts.component';
import { CatalogComponent } from './equipment/catalog/catalog.component';
import { MenuCatActivitySectorComponent } from './equipment/catalog/menu-cat-activity-sector/menu-cat-activity-sector.component';
import { MenuCatBrandComponent } from './equipment/catalog/menu-cat-brand/menu-cat-brand.component';
import { MenuCatNatureComponent } from './equipment/catalog/menu-cat-nature/menu-cat-nature.component';
import { MenuCatTypeComponent } from './equipment/catalog/menu-cat-type/menu-cat-type.component';
import { ClientInfoComponent } from './equipment/client-info-component/client-info-component.component';
import { DescManagementComponent } from './equipment/desc-management/desc-management.component';
import { PlanListComponent } from './equipment/plan-list/plan-list.component';
import { RequestComponent } from './equipment/request/request.component';
import { ValuationRequestComponent } from './equipment/request/valuation-request/valuation-request.component';
import { ResultHistoryComponent } from './equipment/result/shared/result-history/result-history.component';
import { ResultPlanListNoteComponent } from './equipment/result/expert/result-plan-list/result-plan-list-note/result-plan-list-note.component';
import { ResultPlanListProfileComponent } from './equipment/result/expert/result-plan-list/result-plan-list-profile/result-plan-list-profile.component';
import { ResultPlanListComponent } from './equipment/result/expert/result-plan-list/result-plan-list.component';
import { ResultRequestInformationDescriptiveComponent } from './equipment/result/shared/result-request-information/result-request-information-descriptive/result-request-information-descriptive.component';
import { ResultRequestInformationGeneralitiesComponent } from "./equipment/result/shared/result-request-information/result-request-information-generalities/result-request-information-generalities.component";
import { ResultRequestInformationQuotationComponent } from './equipment/result/shared/result-request-information/result-request-information-quotation/result-request-information-quotation.component';
import { ResultRequestInformationComponent } from "./equipment/result/shared/result-request-information/result-request-information.component";
import { ResultExpertComponent } from './equipment/result/expert/result-expert.component';
import { CriteriaComponent } from './equipment/search/criteria/criteria.component';
import { ResultTableCatalogueComponent } from './equipment/search/result-table-catalogue/result-table-catalogue.component';
import { ResultTableMaterialComponent } from './equipment/search/result-table-material/result-table-material.component';
import { SearchComponent } from './equipment/search/search.component';
import { SummaryModalComponent } from './equipment/summary-modal/summary-modal.component';
import { AppScrollTopComponent } from './shared/app-scroll-top/app-scroll-top.component';
import { BibliothequesComponent } from './shared/bibliotheques/bibliotheques.component';
import { AddressFormComponent } from './shared/bibliotheques/form-validateur/address-form/address-form.component';
import { FormParentComponent } from './shared/bibliotheques/form-validateur/form-parent/form-parent.component';
import { FormTestComponent } from './shared/bibliotheques/form-validateur/form-test/form-test.component';
import { CommunicationModalComponent } from './shared/communication-modal/communication-modal.component';
import { ConfirmationModalComponent } from './shared/confirmation-modal/confirmation-modal.component';
import { DocumentEditorModalComponent } from './shared/document-editor-modal/document-editor-modal.component';
import { DocumentListModalComponent } from './shared/document-list-modal/document-list-modal.component';
import { DocumentsComponent } from './shared/documents/documents.component';
import { PdfModalComponent } from './shared/documents/pdf-modal/pdf-modal.component';
import { SafePipe } from './shared/documents/pdf-modal/safe-pipe';
import { InputNumberComponent } from './shared/input-number/input-number.component';
import { Page401Component } from './shared/page-401/page-401.component';
import { DocTypePipe } from './shared/pipes/doc-type.pipe';
import { RoleNamePipe } from './shared/pipes/role-name.pipe';
import { AuthBearerInterceptor } from './shared/services/auth-bearer-interceptor.service';
import { authConfig, authModuleConfig } from './shared/services/auth-config';
import { SocialReasonTitleComponent } from './shared/social-reason-title/social-reason-title.component';
import { MaterialQualificationComponent } from './equipment/valorisation/expert/material-qualification-component/material-qualification.component';
import { ReceptionComponent } from './equipment/valorisation/expert/reception.component';
import { ValuationReceptionPlansComponent } from './equipment/valorisation/expert/valuation-reception-plans/valuation-reception-plans.component';
import { ReceptionDecoteComponent } from './equipment/valorisation/expert/reception-decote/reception-decote.component';
import { ReceptionSubmitComponent } from './equipment/valorisation/expert/reception-submit/reception-submit.component';
import { FinancingPlanProfileComponent } from './equipment/valorisation/expert/valuation-reception-plans/financing-plan-profile/financing-plan-profile.component';
import { CreateCatRequestModalComponent } from './equipment/valorisation/expert/material-qualification-component/create-cat-request-modal/create-cat-request-modal.component';
import { ReceptionContentieuxComponent } from './equipment/valorisation/contentieux/reception-contentieux/reception-contentieux.component';
import { ValuationRequestReceptionComponent } from './equipment/valorisation/shared/valuation-request-reception/valuation-request-reception.component';
import { InformationContentieuxComponent } from './equipment/valorisation/contentieux/reception-contentieux/information-contentieux/information-contentieux.component';
import { MaterialQualificationContentieuxComponent } from './equipment/valorisation/contentieux/reception-contentieux/material-qualification-contentieux/material-qualification-contentieux.component';
import { DecoteContentieuxComponent } from './equipment/valorisation/contentieux/reception-contentieux/decote-contentieux/decote-contentieux.component';
import { ResultContentieuxComponent } from './equipment/result/contentieux/result-contentieux.component';
import { ResultDecoteContentieuxComponent } from './equipment/result/contentieux/result-decote-contentieux/result-decote-contentieux.component';
import { ValuationCreationComponent } from './equipment/search/valuation-creation/valuation-creation.component';
import { ValidatorsModalComponent } from './shared/validators-modal/validators-modal.component';
import { CatalogEnergyComponent } from './equipment/catalog/catalog-energy/catalog-energy.component';
import {ListboxModule} from 'primeng/listbox';
import {TreeTableModule} from 'primeng/treetable';
import { UnderConstructionComponent } from './shared/under-construction/under-construction.component';
import { ProgressSpinnerModule } from 'primeng/progressspinner';

export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;






@NgModule({
  declarations: [
    AppComponent,
    BibliothequesComponent,
    InputNumberComponent,
    FormParentComponent,
    FormTestComponent,
    AddressFormComponent,
    ClientInfoComponent,
    ValuationRequestReceptionComponent,
    MaterialQualificationComponent,
    AppScrollTopComponent,
    ReceptionComponent,
    RequestComponent,
    ValuationRequestComponent,
    ConfirmationModalComponent,
    PlanListComponent,
    SummaryModalComponent,
    SocialReasonTitleComponent,
    ReceptionDecoteComponent,
    ValuationReceptionPlansComponent,
    ReceptionSubmitComponent,
    FinancingPlanProfileComponent,
    ResultExpertComponent,
    ResultRequestInformationDescriptiveComponent,
    ResultRequestInformationQuotationComponent,
    ResultHistoryComponent,
    ResultPlanListComponent,
    ResultPlanListNoteComponent,
    ResultPlanListProfileComponent,
    ResultRequestInformationGeneralitiesComponent,
    ResultRequestInformationComponent,
    DocumentsComponent,
    Page401Component,
    PdfModalComponent,
    DescManagementComponent,
    CatalogComponent,
    MenuCatNatureComponent,
    MenuCatActivitySectorComponent,
    MenuCatBrandComponent,
    MenuCatTypeComponent,
    CatalogNewDiscountsComponent,
    CatalogOldDiscountsComponent,
    SafePipe,
    DocTypePipe,
    DocumentEditorModalComponent,
    DocumentListModalComponent,
    SearchComponent,
    CriteriaComponent,
    ResultTableMaterialComponent,
    CatDiscountsAverageComponent,
    ResultTableCatalogueComponent,
    CatalogAddRequestsComponent,
    CreateCatRequestModalComponent,
    CommunicationModalComponent,
    RoleNamePipe,
    ReceptionContentieuxComponent,
    InformationContentieuxComponent,
    MaterialQualificationContentieuxComponent,
    DecoteContentieuxComponent,
    ResultContentieuxComponent,
    ResultDecoteContentieuxComponent,
    ValuationCreationComponent,
    ValidatorsModalComponent,
    CatalogEnergyComponent,
    UnderConstructionComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    NgSelectModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TabModule,
    HttpClientModule,
    NgbModule,
    OAuthModule.forRoot(authModuleConfig),
    NgxExtendedPdfViewerModule,
    AutocompleteLibModule,
    TreeModule,
    ButtonModule,
    MenubarModule,
    TooltipModule,
    NgxFileDropModule,
    TableModule,
		SliderModule,
		DialogModule,
		MultiSelectModule,
		ContextMenuModule,
		DropdownModule,
		ButtonModule,
		ToastModule,
    InputTextModule,
    ProgressBarModule,
    NgxMaskModule.forRoot(),
    ListboxModule,
    TreeTableModule,
    ProgressSpinnerModule,

  ],
  providers: [
    { provide: OAuthModuleConfig, useValue: authModuleConfig },
    { provide: ValidationHandler, useClass: JwksValidationHandler },
    { provide: OAuthStorage, useValue: localStorage },
    { provide: AuthConfig, useValue: authConfig },
    { provide: HTTP_INTERCEPTORS, useClass: AuthBearerInterceptor, multi: true },
    {provide: LocationStrategy, useClass: HashLocationStrategy}



  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
