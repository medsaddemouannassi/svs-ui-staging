import { TestBed } from '@angular/core/testing';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { FileSystemFileEntry, NgxFileDropEntry } from 'ngx-file-drop';
import { Subscription, throwError } from 'rxjs';
import { of } from 'rxjs/internal/observable/of';
import { DocInsertionService } from '../../../../shared/document-list-modal/doc-insertion.service';
import { MdmInformationsModel } from '../../../../shared/models/mdm-informations.model';
import { ValuationRequestModel } from '../../../../shared/models/valuation-request.model';
import { ValuationModel } from '../../../../shared/models/valuation.model';
import { ValuationDocumentModel } from '../../../../shared/models/valuationDocument.model';
import { ValuationSharedService } from '../../../../shared/services/valuation-shared-service';
import { ValuationRequestReceptionComponent } from "./valuation-request-reception.component";
import { DomSanitizer } from '@angular/platform-browser';


describe('ValuationRequestReceptionComponent', () => {
  let fixture: ValuationRequestReceptionComponent;
  let formBuilderMock: any;
  let valuationServiceMock: any;
  let routerMock: any;
  let financialPlanServiceMock: any;
  let paramAmortizationProfileServiceMock: any;
  let paramProductFamilyServiceMock: any;
  let paramEnergyServiceMock: any;
  let paramUsageUnitServiceMock: any;
  let valuationRequestDocsServiceMock: any;
  let mdmInformationServiceMock: any;
  let popupServiceMock: any;
  let paramSettingServiceMock: any;
  let subscriptionMock: Subscription;
  let entrepriseInfoServiceMock: any;
  let authServiceMock: any;
  let mainUnderw;
  let valuationSharedServiceMock: any

  let mdminfos: MdmInformationsModel = {
    mat: 'M102277',
    fullName: 'john jane',
    reseau: 'reseau1',
    delegation: 'deleg1',
    idDelegation: '13',
    idReseau: '25',
    idDirection: '15',
    direction: 'direction1',
    idFullname: 'M102277-john jane',
    mainUnderw : "contrepartieTest",
    mail:"111"
  } as MdmInformationsModel;
  let documentsList: ValuationDocumentModel[] = [{
    "id": "1",
    "docRequest": "2",
    "docType": "E",
    "valuationRequest": null,
    "docFamily": "E",
    "docSubType": "E303",
    "docPhase": "s",
    "gestObject": "non",
    "docName": "document",
    "docVersion": "1",
    "isQualified": true,
    "paramSetting": null
  }, {
    "id": "2",
    "docRequest": "2",
    "docType": "E",
    "valuationRequest": null,
    "docFamily": "E",
    "docSubType": "E303",
    "docPhase": "s",
    "gestObject": "non",
    "docName": "document",
    "docVersion": "1",
    "isQualified": true,
    "paramSetting": null



  }];

  let sanitizer: any;

  beforeEach(() => {
    valuationSharedServiceMock = TestBed.inject(ValuationSharedService);

    subscriptionMock = new Subscription();

    let valuation = {
      valuationRequest: {
        id: '1',
        bssResponsible: 'M40255'
      } as ValuationRequestModel
    } as ValuationModel;

    valuationServiceMock = {
      getValuationByIdRequest: jest.fn().mockReturnValue(of(valuation))
    };
    financialPlanServiceMock = {
      loadFinancialPlanByValuationRequestId: jest.fn().mockReturnValue(of(true)),
      loadFinancialPlanByValuationId: jest.fn().mockReturnValue(of(true)),
    }
    authServiceMock = {
      currentUserRolesList: ['ROLE_SVS_ADMIN_SE'],
      runInitialLoginSequence: jest.fn().mockReturnValue(Promise.resolve({ data: true }))

    }
    paramAmortizationProfileServiceMock = {
      getAllParamAmortizationProfile: jest.fn().mockReturnValue(of(true))
    }

    paramProductFamilyServiceMock = {
      getAllParamProductFamily: jest.fn().mockReturnValue(of(true))
    }

    paramEnergyServiceMock = {
      getAllParamEnergy: jest.fn().mockReturnValue(of(true))
    }

    paramUsageUnitServiceMock = {
      getAllParamUsageUnit: jest.fn().mockReturnValue(of(true))
    }
    valuationRequestDocsServiceMock = {
      getBssRespDocsByValReqId: jest.fn().mockReturnValue(of(documentsList)),
      displayDocument: jest.fn().mockReturnValue(of({ url: 'url' }))
    }

    mdmInformationServiceMock = {
      getMdmCaInformation: jest.fn().mockReturnValue(of(mdminfos))
    }

    entrepriseInfoServiceMock = {
      contrepartieChanged: of('contrepartieTest')
    }

    paramSettingServiceMock = {
      getParamByCode: jest.fn().mockReturnValue(of([{
        id: '1',
        code: 'GED_SUBTYPES',
        label: 'autres',
        value: 'E303'
      }]))

    }
    formBuilderMock = new FormBuilder();

    routerMock = {
      snapshot: {
        paramMap: {
          get: (key: string) => {
            switch (key) {
              case 'id': return '1';
              //case 'idBcp': return '1';
            }
          },
        },
      }

    };
    popupServiceMock = {
      popupInfo: jest.fn().mockReturnValue(of(true)),
      popupPDF: jest.fn().mockReturnValue(of(true)),
      popupDocumentList: jest.fn().mockReturnValue(of(true)),
      popupDocument: jest.fn().mockReturnValue(of({ document: { docName: 'name' }, index: 0 })),
      onConfirm: of(true),
      onConfirmDocs: of([]),
      onConfirmDoc: of({})

    };
    sanitizer = TestBed.inject(DomSanitizer);

    fixture = new ValuationRequestReceptionComponent(
      formBuilderMock, routerMock, financialPlanServiceMock, valuationServiceMock, paramAmortizationProfileServiceMock, paramProductFamilyServiceMock,
      paramEnergyServiceMock, paramUsageUnitServiceMock, valuationRequestDocsServiceMock, mdmInformationServiceMock, popupServiceMock, paramSettingServiceMock, new DocInsertionService()
      , entrepriseInfoServiceMock, authServiceMock, valuationSharedServiceMock,sanitizer
    );

    fixture.ngOnInit();

  });



  describe('Test Component', () => {
    it('should Be Truthy', () => {
      expect(fixture).toBeTruthy();
    });

    it('should valuationServiceMock return error', () => {
      valuationServiceMock = {

        getValuationByIdRequest: jest.fn().mockImplementation(() => {
          return throwError(new Error('my error message'));
        }),
      };
      fixture = new ValuationRequestReceptionComponent(
        formBuilderMock, routerMock, financialPlanServiceMock, valuationServiceMock, paramAmortizationProfileServiceMock, paramProductFamilyServiceMock,
        paramEnergyServiceMock, paramUsageUnitServiceMock, valuationRequestDocsServiceMock, mdmInformationServiceMock, popupServiceMock, paramSettingServiceMock, new DocInsertionService()
        , entrepriseInfoServiceMock, authServiceMock, valuationSharedServiceMock,sanitizer
      );

      fixture.ngOnInit();
    });

    it('should mdmInformationServiceMock throws error ', () => {
      spyOn(mdmInformationServiceMock, 'getMdmCaInformation').and.returnValue(throwError({ status: 404 }));
      fixture.ngOnInit();
    });

    it('test call service ng on init', () => {

      fixture = new ValuationRequestReceptionComponent(
        formBuilderMock, routerMock, financialPlanServiceMock, valuationServiceMock, paramAmortizationProfileServiceMock, paramProductFamilyServiceMock,
        paramEnergyServiceMock, paramUsageUnitServiceMock, valuationRequestDocsServiceMock, mdmInformationServiceMock, popupServiceMock, paramSettingServiceMock, new DocInsertionService()
        , entrepriseInfoServiceMock, authServiceMock, valuationSharedServiceMock,sanitizer
      );

      fixture.ngOnInit();
      expect(valuationServiceMock.getValuationByIdRequest).toHaveBeenCalled();
      valuationServiceMock.getValuationByIdRequest({}).subscribe(res => {
        expect(mdmInformationServiceMock.getMdmCaInformation).toHaveBeenCalled();
        mdmInformationServiceMock.getMdmCaInformation().subscribe(res => {
          expect(fixture.bssResponsibleInformation).toEqual(mdminfos)
        })

        expect(valuationRequestDocsServiceMock.getBssRespDocsByValReqId).toHaveBeenCalled();

        //this is intentional
      }, (er) => {
        //this is intentional
      }, () => {

        //  mdminfos

      })
    });

    it('should valuationRequestDocsService getBssRespDocsByValReqId throws error ', () => {
      spyOn(valuationRequestDocsServiceMock, 'getBssRespDocsByValReqId').and.returnValue(throwError({ status: 404 }));
      fixture.ngOnInit();
    });

  });


  describe('Test:  onBlurRemainingAmount', () => {
    it('should return 10 when financingAmount=100000 remainingAmount=10000', () => {

      fixture.form.get('financingAmount').setValue("100000");

      fixture.plansControls.push(new FormGroup({
        remainingPercent: new FormControl(null),
      }));

      let planControl: FormControl = new FormControl('10000');
      fixture.onBlurRemainingAmount(planControl, 0);

      expect(fixture.isPriceIncorrect).toEqual(false);
      expect(fixture.plansControls[0].get('remainingPercent').value).toEqual(10);

    });

    it('should return null when financingAmount=0 remainingAmount=10000', () => {

      fixture.form.get('financingAmount').setValue(0);

      fixture.plansControls.push(new FormGroup({
        remainingPercent: new FormControl(null),
      }));

      let planControl: FormControl = new FormControl('10000');
      fixture.onBlurRemainingAmount(planControl, 0);

      expect(fixture.isPriceIncorrect).toEqual(true);
      expect(fixture.plansControls[0].get('remainingPercent').value).toEqual(null)

    });

    it('should return null when financingAmount=undefined remainingAmount=10000', () => {

      fixture.form.get('financingAmount').setValue(undefined);

      fixture.plansControls.push(new FormGroup({
        remainingPercent: new FormControl(null),
      }));

      let planControl: FormControl = new FormControl('10000');
      fixture.onBlurRemainingAmount(planControl, 0);

      expect(fixture.isPriceIncorrect).toEqual(true);
      expect(fixture.plansControls[0].get('remainingPercent').value).toEqual(null)

    });

  });

  describe('Test:  onBlurRemainingPercent', () => {
    it('should return null when financingAmount=100000 remainingPercent=10', () => {
      fixture.form.get('financingAmount').setValue("100000");

      fixture.plansControls.push(new FormGroup({
        remainingAmount: new FormControl(null),
      }));

      let planControl: FormControl = new FormControl('10');
      fixture.onBlurRemainingPercent(planControl, 0);

      expect(fixture.isPriceIncorrect).toEqual(false);
      expect(fixture.plansControls[0].get('remainingAmount').value).toEqual("10000");

    });

    it('should return null when financingAmount=0 remainingPercent=10', () => {
      fixture.form.get('financingAmount').setValue(0);

      fixture.plansControls.push(new FormGroup({
        remainingAmount: new FormControl(null),
      }));

      let planControl: FormControl = new FormControl('10');
      fixture.onBlurRemainingPercent(planControl, 0);

      expect(fixture.isPriceIncorrect).toEqual(true);
      expect(fixture.plansControls[0].get('remainingAmount').value).toEqual(null);

    });

    it('should return null when financingAmount=undefined remainingPercent=10', () => {
      fixture.form.get('financingAmount').setValue(undefined);

      fixture.plansControls.push(new FormGroup({
        remainingAmount: new FormControl(null),
      }));

      let planControl: FormControl = new FormControl('10');
      fixture.onBlurRemainingPercent(planControl, 0);

      expect(fixture.isPriceIncorrect).toEqual(true);
      expect(fixture.plansControls[0].get('remainingAmount').value).toEqual(null);

    });

  });

  describe('Test:  onBlurPrice', () => {
    it('should return false when financingAmount=undefined', () => {
      fixture.form.get('financingAmount').setValue(undefined);
      fixture.onBlurPrice();
      expect(fixture.isPriceIncorrect).toEqual(true);
    });

    it('should update all plansControls remainingAmount when financingAmount=100000 and percent=10', () => {
      fixture.form.get('financingAmount').setValue("100000");

      fixture.plansControls.push(new FormGroup({
        remainingAmount: new FormControl(null),
        remainingPercent: new FormControl(10)
      }));
      let pcToAdd1 = new FormGroup({
        remainingAmount: new FormControl(null),
        remainingPercent: new FormControl(20)
      });
      let pcToAdd2 = new FormGroup({
        remainingAmount: new FormControl(null),
        remainingPercent: new FormControl(30)
      });
      fixture.plansControls.push(pcToAdd1);
      fixture.plansControls.push(pcToAdd2);

      fixture.onBlurPrice();
      expect(fixture.isPriceIncorrect).toEqual(false);
      expect(fixture.plansControls[0].get('remainingAmount').value).toEqual("10000");
      expect(fixture.plansControls[1].get('remainingAmount').value).toEqual("20000");
      expect(fixture.plansControls[2].get('remainingAmount').value).toEqual("30000");
    });

  });

  describe('Test loadFinancialPlanByValuationRequestId', () => {
    it('should return error', () => {
      spyOn(financialPlanServiceMock, 'loadFinancialPlanByValuationRequestId').and.returnValue(throwError({ status: 404 }));
      fixture.loadFinancialPlanByValuationRequestId(1);
    });
    it('should implements plans by valuation req Id', () => {
      fixture.loadFinancialPlanByValuationRequestId(1);
      expect(fixture.plansList).toBeDefined();
    });

    it('should implements plans by valuation Id', () => {
      fixture.isFromContentieux = true;
      fixture.loadFinancialPlanByValuationRequestId(1);
      expect(fixture.plansList).toBeDefined();
    });
  });



  describe('Test get f()', () => {
    it('should return form controls', () => {
      expect(fixture.f).toEqual(fixture.form.controls);
    })
  });

  describe('Test get value()', () => {
    
    it('should return form value', () => {
      expect(fixture.value).toEqual(fixture.form.getRawValue());
    })
  });

  describe('Test changeTab()', () => {
    it('should be 3', () => {
      fixture.changeTab(3);
      expect(fixture.selectedPlan).toEqual(3);
    })
  });

  describe('Test get isAOldAsset()', () => {
    it('should return isAOldAsset value', () => {
      expect(fixture.isAOldAsset).toEqual(!fixture.form.get('isAOldAsset').value);
    })
  });

  describe('Test get collAssetEstimYear()', () => {
    it('should return collAssetEstimYear value', () => {
      expect(fixture.collAssetEstimYear).toEqual(fixture.form.get('collAssetEstimYear').value);
    })
  });

  describe('Test get paramUsageUnitId()', () => {
    it('should return paramUsageUnitId value', () => {
      expect(fixture.paramUsageUnitId).toEqual(fixture.form.get('paramUsageUnitId').value);
    })
  });

  describe('Test get assetUsVolume()', () => {
    it('should return assetUsVolume value', () => {
      expect(fixture.assetUsVolume).toEqual(fixture.form.get('assetUsVolume').value);
    })
  });

  describe('Test dropped()', () => {
    it('should open modal when drop file', () => {
      let file: FileSystemFileEntry = {
        isFile: true,
        isDirectory: false,
        name: null,
        file: <T>(callback: (filea: File) => T) => callback({ name: 'file3.png', size: 17567 } as File),
      }
      fixture.dropped([new NgxFileDropEntry('', file)])
      fixture.form.patchValue({ docsToAdd: [] })
      expect(popupServiceMock.popupDocumentList)
    })
  });
  describe('Test adjustNonqualifiedDocuments()', () => {
    it('should adjust doc index', () => {

      let docs: any[] = [
        {
          docFamily: 'E',
          docType: 'E1',
          docSubType: 'E303',
          isQualified: false
        }, {
          docFamily: 'E',
          docType: 'E1',
          docSubType: 'E303',
          isQualified: true

        }
      ]
      fixture.adjustNonqualifiedDocuments(docs)
      expect(docs[0]).toEqual({
        isModified: false,
        docFamily: 'E',
        docType: 'E1',
        docSubType: 'E3031',
        isQualified: false

      })
    })
  });

  describe('Test deleteDoc()', () => {
    it('should adjust doc index', () => {

      let docs: any[] = [
        {
          docFamily: 'E',
          docType: 'E1',
          docSubType: 'E303',
          isQualified: false
        }, {
          docFamily: 'E',
          docType: 'E1',
          docSubType: 'E303',
          isQualified: true

        }
      ]
      fixture.form.patchValue({ docsToAdd: docs })
      fixture.deleteDocument(1)
      expect(fixture.form.value.docsToAdd.length).toEqual(1)
    })
  });
  describe('Test displayDoc()', () => {
    it('should display doc ', () => {

      fixture.documents = [{} as ValuationDocumentModel]

      fixture.displayDocument(1)
      expect(valuationRequestDocsServiceMock.displayDocument).toHaveBeenCalled();
      valuationRequestDocsServiceMock.displayDocument(1).subscribe(res => {
        expect(popupServiceMock.popupPDF).toHaveBeenCalled()
      })
    })
  });
  describe('Test editdoc()', () => {
    it('should edit doc doc ', () => {

      let docs: any[] = [
        {
          isModified: false,
          docFamily: 'E',
          docType: 'E1',
          docSubType: 'E303',
          isQualified: false
        }, {
          isModified: false,
          docFamily: 'E',
          docType: 'E1',
          docSubType: 'E303',
          isQualified: true

        }
      ]

      let docsToAdd = [
        {
          doc: { name: 'file3.png', size: 17567 } as File,
          docIndex: {
            docName: 'name',
            docFamily: 'E',
            docType: 'E1',
            docSubType: 'E303',
            isQualified: false
          } as ValuationDocumentModel
        }, {
          doc: { name: 'file3.png', size: 17567 } as File,
          docIndex: {
            docFamily: 'E',
            docType: 'E1',
            docSubType: 'E303',
            isQualified: true
          } as ValuationDocumentModel

        }
      ]
      fixture.form.patchValue({ docsToAdd: docsToAdd, documentsList: docs })
      fixture.editDocumentName(0, true)
      expect(popupServiceMock.popupDocument).toHaveBeenCalled();
      fixture.editDocumentName(0, false)
      expect(popupServiceMock.popupDocument).toHaveBeenCalled();

    })
  });
  describe('Test init apport label', () => {

    it('should set apport label', () => {
      fixture.initApportLabel('2', 1);
      expect(fixture.apportLabellist[1]).toEqual('Premier loyer majoré');
      fixture.initApportLabel('1', 2);
      expect(fixture.apportLabellist[2]).toEqual('Apport ');
    });
  });
  describe('Test ngOnDestroy', () => {

    it('should unsubscribe', () => {
      fixture.ngOnDestroy();
      const spyunsubscribe = jest.spyOn(subscriptionMock, 'unsubscribe');
      expect(spyunsubscribe).toBeDefined();
    });
  });

  describe('Test registerOnChange', () => {
    it('should get register', () => {
      let test = (test) => { return test };
      let size = fixture.subscriptions.length;
      fixture.registerOnChange(test);
      expect(fixture.onChange).not.toEqual(() => {
        //this is intentional
      });
      expect(fixture.subscriptions.length).toEqual(size + 1);


    });
  });

  describe('Test get registerOnChange()', () => {
    it('should fixture.onTouched  = fn', () => {
      let fn: any;
      fixture.registerOnTouched(fn);
      expect(fixture.onTouched).toEqual(fn);
    })
  });

  describe('Test writeValue', () => {
    it('should  write value', () => {
      let value = ({
        "assetUsVolume": "",
        "bssResponsibleFullName": "john jane",
        "collAssetEstimYear": "",
        "delegation": "deleg1",
        "docsToAdd": [],
        "documentsList": [],
        "financingAmount": "",
        "isAOldAsset": "",
        "isAssetUseAbroad": "",
        "isLicencePlateAvaileble": "",
        "isRetUnderwriter": "",
        "mainUnderw": "contrepartieTest",
        "paramEnergy": "",
        "paramUsageUnitId": "",
        "plans": [],
        "requestComment": "",
        "underwriterId": "",
        "unitaryQuantity": "",
      });

      fixture.writeValue(value);
      expect(fixture.value).toEqual(value);

    });

    it('should  write value', () => {
      let value = null;
      fixture.writeValue(value);
      expect(fixture.form.reset).toBeTruthy();
    });

    it('should  write valid', () => {
      fixture.valide = { isValid: false }
      expect(fixture.disabled).toEqual(true);

      fixture.valide = null
      expect(fixture.disabled).toEqual(false);
    });

    it('should  test validate', () => {
      expect(fixture.validate(null)).toEqual({ valuationRequest: { valid: false } });

      fixture.form.patchValue({ financingAmount: 5000, paramEnergy: '1' })

      expect(fixture.validate(null)).toBeNull();


      // fixture.valide = null
      // expect(fixture.disabled).toEqual(false);
    });
  });

  describe('Test setDisabledState()', () => {
    it('should disabled = true', () => {
      fixture.setDisabledState(true);
      expect(fixture.disabled).toEqual(true);
    });
    it('should disabled = false', () => {
      fixture.setDisabledState(false);
      expect(fixture.disabled).toEqual(false);
    })
  });

  describe('Test setPlansForm', () => {
    it('should not init plans', () => {
      fixture.plansList = null;
      fixture.initApportLabel = null;
      fixture.setPlansForm();
      expect(fixture.initApportLabel).toEqual(null);
    });
  
  });

  describe('Test ngAfterViewInit', () => {
    it('should init duplication', () => {
      valuationSharedServiceMock.setIsFromDuplicatedReception(true)
      fixture.ngAfterViewInit();
      expect(fixture.profilOptions).toEqual(true)
    });
  
  });

});

