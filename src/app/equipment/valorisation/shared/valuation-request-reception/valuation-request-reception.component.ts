import { AfterViewInit, Component, EventEmitter, forwardRef, Input, OnDestroy, OnInit, Output,SecurityContext } from '@angular/core';
import { AbstractControl, ControlValueAccessor, FormArray, FormBuilder, FormControl, FormGroup, NG_VALIDATORS, NG_VALUE_ACCESSOR, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { FileSystemFileEntry, NgxFileDropEntry } from 'ngx-file-drop';
import { forkJoin, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { MdmInformationsModel } from 'src/app/shared/models/mdm-informations.model';
import { ValuationDocumentModel } from 'src/app/shared/models/valuationDocument.model';
import { ValuationRequestDocsService } from 'src/app/shared/services/valuation-request-docs.service';
import { ValuationService } from 'src/app/shared/services/valuation.service';
import { DocumentConstant } from '../../../../shared/document-constant';
import { DocInsertionService } from '../../../../shared/document-list-modal/doc-insertion.service';
import { validateDocument } from '../../../../shared/documents/document-validator';
import { FinancialPlanModel } from '../../../../shared/models/financial-plan.model';
import { ParamAmortizationProfileModel } from '../../../../shared/models/param-amortization-profile.model';
import { ValuationRequestModel } from '../../../../shared/models/valuation-request.model';
import { ValuationModel } from '../../../../shared/models/valuation.model';
import { AuthService } from '../../../../shared/services/auth.service';
import { EntrepriseInfoService } from '../../../../shared/services/entreprise-info.service';
import { FinancialPlanService } from '../../../../shared/services/financial-plan.service';
import { MdmInformationService } from '../../../../shared/services/mdm-informations.service';
import { ParamAmortizationProfileService } from '../../../../shared/services/param-amortization-profile.service';
import { ParamEnergyService } from '../../../../shared/services/param-energy.servie';
import { ParamProductFamilyService } from '../../../../shared/services/param-product-family.service';
import { ParamSettingService } from '../../../../shared/services/param-setting.service';
import { ParamUsageUnitService } from '../../../../shared/services/param-usage-unit';
import { PopupService } from '../../../../shared/services/popup.service';
import { ValuationSharedService } from '../../../../shared/services/valuation-shared-service';
import { getResidualValueEuro, getResidualValuePercent, twoDigitDecimalRound } from '../../../../shared/ValuationUtils';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'app-valuation-request-reception',
  templateUrl: './valuation-request-reception.component.html',
  styleUrls: ['./valuation-request-reception.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ValuationRequestReceptionComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => ValuationRequestReceptionComponent),
      multi: true
    }
  ]
})
export class ValuationRequestReceptionComponent implements OnInit, ControlValueAccessor, OnDestroy, AfterViewInit {

  public valuationReception: ValuationModel = {} as ValuationModel;
  @Output() valuationToSend = new EventEmitter<ValuationModel>();
  valuationRequest: ValuationRequestModel = {} as ValuationRequestModel;
  isPriceIncorrect: boolean;
  docSubTypeListParams: any;
  @Input() valuationRequestId;
  @Input() submitePassed = false;
  @Input() submited = false;
  disabled = false;
  test = false;

  @Input() set valide(value) {
    if (value && this.form) {
      this.form.disable();
      this.disabled = true;
    }
    else {
      if (this.form != null) {
        this.form.enable();
        this.disabled = false;
      }

    }
  }


  get value(): any {
    return this.form.getRawValue();
  }

  set value(value) {
    if (value) {
      this.form.patchValue(value);
      this.onChange(value);
      this.onTouched();
    }
  }

  get plansControls(): AbstractControl[] {
    return (this.form.get('plans') as FormArray).controls;
  }

  get f(): any {
    return this.form.controls;
  }

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private financialPlanService: FinancialPlanService,
    private valuationService: ValuationService, private paramAmortizationProfileService: ParamAmortizationProfileService,
    private paramProductFamilyService: ParamProductFamilyService, private paramEnergyService: ParamEnergyService,
    private paramUsageUnitService: ParamUsageUnitService, private valuationRequestDocsService: ValuationRequestDocsService,
    private mdmInformationService: MdmInformationService, private popupService: PopupService,
    private paramSettingService: ParamSettingService, private docInsertionService: DocInsertionService,
    private entrepriseInfoService: EntrepriseInfoService, private authService: AuthService,
    private ValuationSharedService: ValuationSharedService, private sanitizer: DomSanitizer
  ) { }

  form: FormGroup;
  subscriptions: Subscription[] = [];
  subscription: Subscription;
  energyOptions: any;
  productOptions: any;
  profilOptions: any;
  paramUsageUnitOptions: any;
  plansList: any = {} as any;
  documents: ValuationDocumentModel[];
  iDRental: string = '2';
  idFurnitureLease: string = '3';
  idLoan: string = '1';
  pramAmortizationProfileOptionsByProductId: ParamAmortizationProfileModel[] = [];
  apportLabellist = ["Apport ou premier loyer majoré", "", "", "", "", "", ""];
  selectedPlan = 0;
  documentName: string;
  bssResponsibleInformation: MdmInformationsModel;
  isFromContentieux = false;
  ngOnInit(): void {
    //test doc params
    this.paramSettingService.getParamByCode("GED_SUBTYPES").subscribe((data) => {
      this.docSubTypeListParams = data;
      this.docSubTypeListParams.push({ code: 'GED_SUBTYPES', label: 'Non renseigné', value: 'E3031' });
    });
    if (this.valuationRequestId) {
      this.isFromContentieux = true
    }
    const id: string = (this.isFromContentieux) ? this.valuationRequestId : this.route.snapshot.paramMap.get('id');
    this.form = this.fb.group({

      bssResponsibleFullName: [{ value: '', disabled: true }],
      delegation: [{ value: '', disabled: true }],
      financingAmount: ['', Validators.required],
      mainUnderw: { value: '', disabled: true },
      plans: this.fb.array([...this.initialisePlans([])]),
      paramEnergy: ['', Validators.required],
      unitaryQuantity: [''],
      isLicencePlateAvaileble: [''],
      isAOldAsset: [''],
      isAssetUseAbroad: [''],
      collAssetEstimYear: [''],
      paramUsageUnitId: [''],
      assetUsVolume: [''],
      requestComment: [{ value: '', disabled: true }],
      underwriterId: [''],
      isRetUnderwriter: [''],
      documentsList: [[]],
      docsToAdd: [[]]


    });
    this.subscriptions.push(
      this.form.valueChanges.subscribe(value => {
        this.onChange(this.form.getRawValue());
        this.onTouched();
      })
    );
    //Load all lists

    if (!this.ValuationSharedService.getIsFromDuplicatedReception() && !this.ValuationSharedService.getIsFromUndoDuplicatedReception()) {



      forkJoin(this.getAllParamLists()).subscribe(data => {
        this.profilOptions = data[0];
        this.productOptions = data[1];
        this.energyOptions = data[2];
        this.paramUsageUnitOptions = data[3];

      }, () => {

      }, () => {
          if(id){

        this.valuationService.getValuationByIdRequest(id).subscribe(data => {


          this.valuationService.valuation = data;
          this.valuationReception = data;
          this.valuationRequest = data.valuationRequest;
          this.valuationToSend.emit(this.valuationReception);




        }, err => { console.error(err); }, () => {

          this.afterLoadValuation();



        });
        }

        else{
            // simulation case
            this.form.controls['requestComment'].enable();
            this.form.updateValueAndValidity();
            this.valuationRequest.bssResponsible = this.authService.identityClaims['mat'];
            this.loadBssInfo(this.valuationRequest.bssResponsible,true);
          }

      });
    }
    // load Contrepartie Data
    this.subscription = this.entrepriseInfoService.contrepartieChanged
      .subscribe(mainUnderwName => {
        this.form.patchValue({
          mainUnderw: mainUnderwName
        })
      });

  }

  afterLoadValuation() {
  if (this.valuationRequest.bssResponsible) {
         this.loadBssInfo(this.valuationRequest.bssResponsible,false)
    // load Contrepartie Data
      this.subscription = this.entrepriseInfoService.contrepartieChanged
        .subscribe(mainUnderwName => {
          this.form.patchValue({
            mainUnderw: mainUnderwName
          })
        });
       } else {
         console.error('cannot find valuation request reception bssResponsible')
       }
    this.form.patchValue({
      financingAmount: this.valuationRequest.financingAmount? twoDigitDecimalRound(this.valuationRequest.financingAmount.toString()) : null,
      mainUnderw: '',
      paramEnergy: this.valuationRequest.paramEnergy,
      unitaryQuantity: this.valuationRequest.unitaryQuantity,
      isLicencePlateAvaileble: this.valuationRequest.isLicencePlateAvaileble,
      isAOldAsset: !this.valuationRequest.isANewAsset,
      isAssetUseAbroad: this.valuationRequest.isAssetUseAbroad,
      collAssetEstimYear: this.valuationRequest.collAssetEstimYear,
      paramUsageUnitId: (this.valuationRequest.paramUsageUnit != null) ? this.valuationRequest.paramUsageUnit.id : null,
      assetUsVolume: this.valuationRequest.assetUsVolume,
      requestComment: this.valuationRequest.requestComment,
      underwriterId: this.valuationRequest.underwriterId,
      isRetUnderwriter: this.valuationRequest.isRetUnderwriter
    });

    this.loadFinancialPlanByValuationRequestId(this.route.snapshot.paramMap.get('id'));

    this.initDocumentList();
  }


loadBssInfo(bssResp:string,isSimulation:boolean){
    this.mdmInformationService.getMdmCaInformation(bssResp).subscribe((data) => {
    this.bssResponsibleInformation = data;
    if(isSimulation || this.valuationRequest.isSimulated){
      this.valuationRequest.bssResponsibleFullName = "Desc";
      this.bssResponsibleInformation.fullName = "Desc";
    }else{
      this.valuationRequest.bssResponsibleFullName = this.bssResponsibleInformation.fullName;
    }

    this.valuationRequest.networkID = this.bssResponsibleInformation.idReseau;
  }, (error) => { console.error(error) }, () => {

    this.form.patchValue({
      delegation: this.bssResponsibleInformation.delegation,
      bssResponsibleFullName: this.bssResponsibleInformation.fullName
    });
  });
}
  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  onChange: any = () => {       //this is intentional
  };
  onTouched: any = () => {       //this is intentional
  };
  registerOnChange(fn: any) {
    if(this.form)
    {
      this.subscriptions.push(this.form.valueChanges.pipe(map(_ => this.form.getRawValue())).subscribe(fn));

    }


  }

  ngAfterViewInit() {
    if (this.ValuationSharedService.getIsFromDuplicatedReception() || this.ValuationSharedService.getIsFromUndoDuplicatedReception()) {
      forkJoin(this.getAllParamLists()).subscribe(data => {
        this.profilOptions = data[0];
        this.productOptions = data[1];
        this.energyOptions = data[2];
        this.paramUsageUnitOptions = data[3];
      }, () => {

      }, () => {
        this.valuationService.valuation = this.ValuationSharedService.getValuation();
        this.valuationReception = this.ValuationSharedService.getValuation();
        this.valuationRequest = this.ValuationSharedService.getValuation().valuationRequest;
        this.valuationToSend.emit(this.valuationReception);
        this.afterLoadValuation();
        if(this.ValuationSharedService.getDocsToAdd() &&this.ValuationSharedService.getDocsToAdd().length>0 )
        {
          this.form.patchValue({ docsToAdd: this.ValuationSharedService.getDocsToAdd()})

        }

      })

    }

  }

  writeValue(value): void {
    if (value) {
      this.value = value;
    }

    if (value === null) {
      this.form.reset();
    }
  }

  registerOnTouched(fn): void {
    this.onTouched = fn;
  }

  validate(_: FormControl): any {
    return this.form.valid ? null : { valuationRequest: { valid: false } };
  }

  changeTab(i): void {
    this.selectedPlan = i;
  }

  private newPlan(plan: FinancialPlanModel): FormGroup {

    return this.fb.group({
      planDuration: [{ value: plan.planDuration, disabled: true }, Validators.required],
      planContrbAmount: [{ value: (plan && plan.planContrbAmount)?twoDigitDecimalRound(plan.planContrbAmount.toString()):null, disabled: true }],
      amortizationProfileId: [plan.paramAmortizationProfile.id, Validators.required],
      paramProductFamilyId: [plan.paramProductFamily.id, Validators.required],
      remainingAmount: { value: plan.remainingAmount ? twoDigitDecimalRound(plan.remainingAmount.toString()) : null, disabled: true },
      remainingPercent: { value: plan.remainingPercent, disabled: true },


      planEstimatedOut: [{ value: plan.planEstimatedOut, disabled: true }, Validators.required],
      id: [(plan != null && plan.id != null) ? plan.id : null],
      collateralAssetRating: [(plan != null && plan.collateralAssetRating != null) ? plan.collateralAssetRating : null],
      status: [(plan != null && plan.status != null) ? plan.status : null],
      valuation: this.valuationReception,
      valuationRequest: this.valuationRequest,


      updatedDate: [(plan != null && plan.updatedDate != null) ? plan.updatedDate : null],
      createdDate: [(plan != null && plan.createdDate != null) ? plan.createdDate : null],
      createdBy: [(plan != null && plan.createdBy != null) ? plan.createdBy : null],
      updatedBy: [(plan != null && plan.updatedBy != null) ? plan.updatedBy : null]

    });

  }

  private initialisePlans(plans: FinancialPlanModel[]): FormGroup[] {
    const arr = [];
    if (plans) {
      plans.forEach((plan: FinancialPlanModel) => {
        arr.push(this.newPlan(plan));
      });
    }
    return arr;
  }

  onBlurRemainingAmount(planControl: FormControl, index: number): void {
    this.isPriceIncorrect = false;

    if (!this.form.get('financingAmount').value) {

      this.isPriceIncorrect = true;

    } else {
      this.plansControls[index].patchValue({
        remainingPercent: getResidualValuePercent(this.form.get('financingAmount').value, planControl.value)
      });
    }

  }

  onBlurRemainingPercent(planControl: FormControl, index: number): void {
    this.isPriceIncorrect = false;

    if (!this.form.get('financingAmount').value) {

      this.isPriceIncorrect = true;

    } else {
      // calculate RV euro from Price and RV %
      this.plansControls[index].patchValue({
        remainingAmount: getResidualValueEuro(this.form.get('financingAmount').value, planControl.value)
      });
    }
  }

  onBlurPrice(): void {
    this.isPriceIncorrect = false;
    if (this.form.get('financingAmount').value) {
      this.form.get('financingAmount').setValue(twoDigitDecimalRound(this.form.get('financingAmount').value))

      this.plansControls.forEach((plan: any) => {

        if (plan.get('remainingPercent').value) {
          plan.patchValue({
            remainingAmount: getResidualValueEuro(this.form.get('financingAmount').value, plan.get('remainingPercent').value)
          });
        }

      });
    } else {
      this.isPriceIncorrect = true;
    }
  }

  get isAOldAsset(): any {
    return !this.form.get('isAOldAsset').value;
  }

  get collAssetEstimYear(): any {
    return this.form.get('collAssetEstimYear').value;
  }

  get paramUsageUnitId(): any {
    return this.form.get('paramUsageUnitId').value;
  }

  get assetUsVolume(): any {
    return this.form.get('assetUsVolume').value;
  }

  getAllParamLists() {
    return [this.paramAmortizationProfileService.getAllParamAmortizationProfile(), this.paramProductFamilyService.getAllParamProductFamily(),
    this.paramEnergyService.getAllParamEnergy(), this.paramUsageUnitService.getAllParamUsageUnit()];

  }

  loadFinancialPlanByValuationRequestId(id: any) {
    if (this.isFromContentieux) {
      this.financialPlanService.loadFinancialPlanByValuationId(id).subscribe(
        data => {
          this.plansList = data;

        }, err => {
          console.error(err);
        }, () => {

          this.setPlansForm();
        });
    }
    else {
      this.financialPlanService.loadFinancialPlanByValuationRequestId(id).subscribe(
        data => {
          this.plansList = data;

        }, err => {
          console.error(err);
        }, () => {

          this.setPlansForm();
        });
    }

  }
  setPlansForm() {

    this.form.setControl('plans', this.fb.array([...this.initialisePlans(this.plansList)]));
    if(this.plansList){
    this.plansList.forEach((element, index) => {
      this.initApportLabel(element.paramProductFamily.id, index);
    });
  }
  }

  editDocumentName(index: number, isNew) {
    if (isNew) {
      this.popupService.popupDocument(this.form.getRawValue().docsToAdd[index].docIndex, index, this.docSubTypeListParams);
      let subscription = this.popupService.onConfirmDoc.subscribe(data => {

        let docList = this.form.getRawValue().docsToAdd;
        docList[data.index].docIndex = data.document;
        docList[data.index].doc = new File([docList[data.index].doc], docList[data.index].docName,
          { type: docList[data.index].doc.type });
        this.form.patchValue({ docsToAdd: docList })

        subscription.unsubscribe();
      })

    } else {
      this.popupService.popupDocument(this.form.getRawValue().documentsList[index], index, this.docSubTypeListParams);
      let subscription = this.popupService.onConfirmDoc.subscribe(data => {
        let docList = this.form.getRawValue().documentsList;

        docList[data.index] = data.document;
        docList[data.index].isModified = true

        this.form.patchValue({ documentsList: docList })

        subscription.unsubscribe();
      })
    }


  }

  // SVS -115
  displayDocument(index) {
    this.valuationRequestDocsService.displayDocument(this.documents[index]).subscribe(res => {
      let safeUrl = this.sanitizer.sanitize(SecurityContext.RESOURCE_URL, this.sanitizer.bypassSecurityTrustResourceUrl(res.url));

      window.open(safeUrl, "_blank");    })

  }

  initApportLabel(id: string, index) {
    if ((id == this.idFurnitureLease) || id === this.iDRental) {
      this.apportLabellist[index] = 'Premier loyer majoré'
    }
    else if (id === this.idLoan) {
      this.apportLabellist[index] = 'Apport '
    }
  }


  deleteDocument(i: number) {
    let docList: any[] = this.form.getRawValue().docsToAdd;
    docList.splice(i, 1);
    this.form.patchValue({ docsToAdd: docList })
  }
  adjustNonqualifiedDocuments(documents: any[]) {
    documents.forEach((element, index) => {
      element.isModified = false;
      if (!element.isQualified) {
        element.docSubType = "E3031";
      }
    });
  }

  public openFileSelector() {
    if (!this.disabled) {
      this.popupService.popupDocumentList([], this.docSubTypeListParams);
      this.onConfirmDocsAddition()
    }


  }

  private onConfirmDocsAddition() {
    let sub = this.popupService.onConfirmDocs.subscribe(docs => {
      sub.unsubscribe();
      let oldDoc: any[] = this.form.get('docsToAdd').value;

      if (oldDoc) {
        oldDoc.push(...docs)
      } else {
        oldDoc = docs;
      }

      this.form.patchValue({ docsToAdd: oldDoc })


    })
  }


  dropped(files: NgxFileDropEntry[]) {
    this.openFileSelector();


    files.map(f => f.fileEntry)
      .filter(f => f.isFile).forEach(f => {
        const fileEntry = f as FileSystemFileEntry;

        fileEntry.file(file => {

          if (validateDocument(file)) {


            let index = {} as ValuationDocumentModel;

            index.docFamily = DocumentConstant.FAMILY_VALUATION;
            index.docType = DocumentConstant.TYPE_ENVIRONMENTAL_VALUATION;
            index.docSubType = DocumentConstant.SUB_TYPE_OTHER_VALUATION;
            index.isQualified = false;
            index.docName = file.name;


            this.docInsertionService.documentInsert.next({ docIndex: index, doc: file })
          }

        })


      })

  }

  setDisabledState(disabled: boolean) {
    if (disabled) {
      this.form.disable()
      this.disabled = true
    } else {
      this.form.enable()
      this.disabled = false

    }
  }

  initDocumentList() {
    this.valuationRequestDocsService.getBssRespDocsByValReqId(this.valuationRequest.id).
      subscribe((data) => {

        this.documents = data.map(a => { return { ...a } });
        let docs = data.map(a => { return { ...a } });

        this.adjustNonqualifiedDocuments(docs);

        this.form.patchValue({ documentsList: docs });
      }, (err) => { console.error(err) }, () => {
        if (this.isFromContentieux) {
          // this.valuation
        }
      })
  }

}


