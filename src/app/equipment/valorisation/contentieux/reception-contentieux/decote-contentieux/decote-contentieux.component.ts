import { Component, OnInit, Input, forwardRef, OnDestroy } from '@angular/core';
import { ValuationDiscountModel } from '../../../../../shared/models/valuationDiscount.model';
import { FormGroup, FormBuilder, Validators, FormArray, NG_VALUE_ACCESSOR, NG_VALIDATORS, ControlValueAccessor, FormControl } from '@angular/forms';
import { ValuationModel } from '../../../../../shared/models/valuation.model';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { CatDiscountRateService } from '../../../../../shared/services/cat-discount-rate.service';
import { LitigationModel } from '../../../../../shared/models/litigation.model';
import { twoDigitDecimalRound, threeDigitDecimalRound } from '../../../../../shared/ValuationUtils';

@Component({
  selector: 'app-decote-contentieux',
  templateUrl: './decote-contentieux.component.html',
  styleUrls: ['./decote-contentieux.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DecoteContentieuxComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => DecoteContentieuxComponent),
      multi: true
    }
  ]
})
export class DecoteContentieuxComponent implements OnInit, ControlValueAccessor, OnDestroy {
  
  percentSymbol = "%";
  discountRateList = [];
  discountRateListFromCatalog = [];
  discountRateListFromLitigation = [];

  decoteContentieuxForm: FormGroup;
  subscriptions: Subscription[] = [];
  selectedDecote = -1;

  constructor(private catDiscountRateService: CatDiscountRateService, private fb: FormBuilder) {
  }






  onChange: any = () => { // This is intentional
  };
  onTouched: any = () => {// This is intentional
  };

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  writeValue(value) {
    if (value) {
      this.value = value;
    }

    if (value === null) {
      this.decoteContentieuxForm.reset();
    }
  }



  get f() {
    return this.decoteContentieuxForm.controls;
  }

  get decoteRatesControls() {
    return this.getDecoteRatesFormArray().controls;
  }

  getDecoteRatesFormArray(): FormArray {
    return (this.decoteContentieuxForm.get('decoteRates') as FormArray);
  }



  get value(): any {
    return this.decoteContentieuxForm.getRawValue();
  }

  set value(value) {
    if (value) {
      this.decoteContentieuxForm.patchValue(value);
      this.onChange(value);
      this.onTouched();
    }
  }


  registerOnChange(fn: any) {
    this.subscriptions.push(this.decoteContentieuxForm.valueChanges.pipe(map(_ => this.decoteContentieuxForm.getRawValue())).subscribe(fn));

  }

  registerOnTouched(fn): void {
    this.onTouched = fn;
  }

  validate(_: FormControl) {
    return this.decoteContentieuxForm.valid ? null : { profile: { valid: false, }, };
  }
  @Input() valuationExpert: ValuationModel;
  @Input() litigation: LitigationModel;
  @Input() materialQualification;

  ageFromDate = null;
  ageFromDateChanged = false;

  @Input() set changeAgeFromDate(age) {

    this.ageFromDate = age;
    if (this.decoteContentieuxForm) {
      this.initMaterialAge();
      this.decoteContentieuxForm.patchValue({ isLostMaterial: false });
      this.isLostMaterialChange(false)
      this.resetCobaillage();

    }

  }


  @Input() set isDateFromAgeChanged(isChanged) {
    this.ageFromDateChanged = isChanged;

  }
  @Input() set isSubmitted(value) {
    if (value && this.decoteContentieuxForm) {
      this.decoteContentieuxForm.markAllAsTouched();
    }

  }

  ngOnInit(): void {

    this.decoteContentieuxForm = this.fb.group({
      decoteRates: [],
      materialAge: ['', Validators.required],
      materialCurrentValue: [{ value: '', disabled: true }, Validators.required],
      litigationDiscount: ['', Validators.required],
      litigationValue: ['', Validators.required],
      isCobailing: [{ value: false, disabled: false }],
      comment: [''],
      isLostMaterial: [{ value: false, disabled: false }],
      shareRate: ['', [Validators.min(0), Validators.max(100)]],
      shareValue: [{ value: '', disabled: true }],

    });

    this.subscriptions.push(
      this.decoteContentieuxForm.valueChanges.subscribe(value => {
        this.onChange(value);
        this.onTouched();
      })
    );
    this.createDecoteForm();
  }

  createDecoteForm() {


    this.catDiscountRateService.getDiscountRateByTypeIdOrBrandIdOrNatureIdOrSectorId(this.materialQualification.catAssetType.id, this.materialQualification.catBrand.id, this.materialQualification.catNature.id, this.materialQualification.catActivitySector.id, this.valuationExpert.valuationRequest.isANewAsset).subscribe((discountRateList: ValuationDiscountModel[]) => {
      this.discountRateListFromCatalog = discountRateList;
      this.discountRateListFromLitigation = this.litigation.litigationDetails;

      if (this.litigation.id && this.litigation.catAssetType.id == this.materialQualification.catAssetType.id && this.litigation.catBrand.id == this.materialQualification.catBrand.id && this.litigation.catNature.id == this.materialQualification.catNature.id && this.litigation.catActivitySector.id == this.materialQualification.catActivitySector.id) {
        this.discountRateList = this.discountRateListFromLitigation;

      } else {

        this.discountRateList = this.discountRateListFromCatalog;
        if (this.discountRateListFromLitigation) {
          this.discountRateList.forEach((element, i) => {
            element.id = this.discountRateListFromLitigation[i].id;
          });
        }
        else {
          this.discountRateList = this.discountRateList.map(a => { { a.id = null } return { ...a } });

        }

      }

    }, err => {
      console.error('error on getDiscountByValuationIdOrFromCat');
    }, () => {
      this.initDecoteForm();
    });
  }



  initDecoteForm() {
    this.decoteContentieuxForm.setControl('decoteRates', this.fb.array([...this.initialiseDecoteRate()]));
    this.initializeDecoteContentieux();
    this.initializeFormContentieuxValue();


  }

  initializeFormContentieuxValue() {


    if (this.litigation && this.litigation.id && this.litigation.catAssetType.id == this.materialQualification.catAssetType.id && this.litigation.catBrand.id == this.materialQualification.catBrand.id && this.litigation.catNature.id == this.materialQualification.catNature.id && this.litigation.catActivitySector.id == this.materialQualification.catActivitySector.id) {

      this.decoteContentieuxForm.patchValue({ materialAge: this.litigation.materialAge });
      this.decoteContentieuxForm.patchValue({ litigationDiscount: this.litigation.litigationDiscount });
      this.decoteContentieuxForm.patchValue({ isCobailing: this.litigation.isCobailing });
      this.decoteContentieuxForm.patchValue({ comment: this.litigation.comment });
      this.decoteContentieuxForm.patchValue({ isLostMaterial: this.litigation.isLostMaterial });
      this.decoteContentieuxForm.patchValue({ shareRate: this.litigation.shareRate });
      this.decoteContentieuxForm.patchValue({ shareValue: twoDigitDecimalRound(this.litigation.shareValue.toString()) });
      this.decoteContentieuxForm.patchValue({ materialCurrentValue: twoDigitDecimalRound(this.litigation.materialCurrentValue.toString()) });
      this.decoteContentieuxForm.patchValue({ litigationValue: twoDigitDecimalRound(this.litigation.litigationValue.toString()) });

      if (this.litigation.isLostMaterial) {
        this.isLostMaterialChange(true);
      }
    }
    else {
      if (this.ageFromDate) {
        this.initMaterialAge();

      }
    }
  }
  initMaterialAge() {
    if (this.ageFromDate && this.ageFromDate > 0 && this.ageFromDate <= 15) {
      if (this.selectedDecote >= 0) {
        this.decoteRatesControls[this.selectedDecote].patchValue({
          isChecked: false
        })
      }
      this.decoteRatesControls[this.ageFromDate - 1].patchValue({
        isChecked: true
      })
      this.selectedDecote = this.ageFromDate - 1;
      this.calculMaterialCurrentValue();

    }
    else {
      if (this.ageFromDate > 15) {
        if (this.selectedDecote >= 0) {
          this.decoteRatesControls[this.selectedDecote].patchValue({
            isChecked: false
          })
        }
        this.decoteRatesControls[14].patchValue({
          isChecked: true
        })
        this.selectedDecote = 14;
        this.calculMaterialCurrentValue();

      }
      else {
        this.resetComponant();
      }

    }
    this.decoteContentieuxForm.patchValue({
      materialAge: this.ageFromDate
    });

    this.decoteContentieuxForm.patchValue({
      litigationValue: null
    });


  }

  initialiseDecoteRate(): FormGroup[] {
    let arr: FormGroup[] = [];


    this.discountRateList.forEach((discount, i) => {

      arr.push(this.newDiscount(discount));
      if (discount.isChecked) {
        this.selectedDecote = i;
      }

    });
    return arr;
  }
  newDiscount(discount) {

    return this.fb.group({
      id: [discount.id ? discount.id : null],
      discountRateValue: [{ value: discount ? discount.discountRateValue : null, disabled: true }, Validators.required],
      discountRateYear: [{ value: discount ? discount.discountRateYear : null, disabled: true }, Validators.required],
      isChecked: [{ value: (discount && discount.isChecked) ? true : false, disabled: false }, Validators.required],
    });
  }

  eventCheck(index) {

    if (this.selectedDecote >= 0 && index != this.selectedDecote) {
      this.decoteRatesControls[this.selectedDecote].patchValue({
        isChecked: false
      })
    }
    if (index == this.selectedDecote && !this.decoteContentieuxForm.getRawValue().decoteRates[this.selectedDecote].isChecked) {
      this.decoteRatesControls[this.selectedDecote].patchValue({
        isChecked: false
      })
      this.decoteContentieuxForm.patchValue({
        materialCurrentValue: null,
        litigationValue: null
      });
    }
    else {
      this.selectedDecote = index;
      this.calculMaterialCurrentValue();
      this.changeMaterialAge();
      this.decoteContentieuxForm.patchValue({
        litigationValue: null
      });
    }


  }
  onBlurLitigationDiscount() {
    if (this.decoteContentieuxForm.getRawValue().litigationDiscount) {
      this.decoteContentieuxForm.patchValue({
        litigationDiscount: threeDigitDecimalRound(this.decoteContentieuxForm.getRawValue().litigationDiscount)
      });
    }
    this.decoteContentieuxForm.patchValue({ litigationValue: null });
    this.decoteContentieuxForm.patchValue({ shareValue: null });

  }

  onBlurShareRate() {
    if (this.decoteContentieuxForm.getRawValue().shareRate) {
      this.decoteContentieuxForm.patchValue({
        shareRate: threeDigitDecimalRound(this.decoteContentieuxForm.getRawValue().shareRate)
      });
    }
  }
  calculMaterialCurrentValue() {
    let selectedDecoteValue = this.decoteContentieuxForm.getRawValue().decoteRates[this.selectedDecote].discountRateValue;
    let materialCurrentValue = (this.valuationExpert.valuationRequest.financingAmount) * (100 - selectedDecoteValue) / 100;
    this.decoteContentieuxForm.patchValue({
      materialCurrentValue: twoDigitDecimalRound(materialCurrentValue.toString())
    });
  }
  lockDiscountValues() {
    this.decoteRatesControls.forEach((control, index) => {
      this.decoteRatesControls[index].get('discountRateValue').disable();
    });
  }

  calculateShareValue() {
    if (this.decoteContentieuxForm.get('shareRate').value && this.decoteContentieuxForm.get('litigationValue').value) {
      let value = (this.decoteContentieuxForm.get('shareRate').value * this.decoteContentieuxForm.get('litigationValue').value) / 100;
      this.decoteContentieuxForm.patchValue({ shareValue: twoDigitDecimalRound(value.toString()) });

    }
    else {
      this.decoteContentieuxForm.patchValue({ shareValue: 0 });
    }
  }

  isLostMaterialChange(isLostMaterial: boolean) {
    if (isLostMaterial) {
      if (!this.decoteContentieuxForm.getRawValue().materialAge || (this.selectedDecote >= 0 && !this.decoteContentieuxForm.getRawValue().decoteRates[this.selectedDecote].isChecked)) {
        this.initMaterialAge();
      }
      this.decoteContentieuxForm.patchValue({ litigationValue: 0 });
      this.decoteContentieuxForm.patchValue({ shareValue: 0 });
      this.decoteContentieuxForm.disable();
      this.decoteContentieuxForm.controls.isLostMaterial.enable();
      this.decoteContentieuxForm.controls.comment.enable();
      this.decoteContentieuxForm.controls.isCobailing.enable();
      this.decoteContentieuxForm.controls.shareRate.enable();
    }
    else {
      this.decoteContentieuxForm.enable();
      this.lockDiscountValues();
      this.decoteContentieuxForm.patchValue({ litigationValue: null });
    }
  }
  contxValueChanged() {
    if (!(this.decoteContentieuxForm.get('litigationValue').value)
      || this.decoteContentieuxForm.get('litigationValue').value == ''
      || this.decoteContentieuxForm.get('litigationValue').value == 0) {

    }
    else {
      this.decoteContentieuxForm.controls.isCobailing.enable();
      this.calculateShareValue();
    }
  }

  initializeDecoteContentieux() {
    this.decoteContentieuxForm.patchValue({
      litigationDiscount: '20'
    });
  }
  changeLitigationDiscount(newDecote) {

    if (!newDecote || newDecote < 0 || newDecote > 100) {
      this.initializeDecoteContentieux();
    }

  }

  calculLitigationValue() {

    let selectedDecoteValue = this.decoteContentieuxForm.getRawValue().litigationDiscount;
    let materialCurrentValue = this.decoteContentieuxForm.getRawValue().materialCurrentValue;
    let litigationValue = (materialCurrentValue) * (100 - selectedDecoteValue) / 100;
    this.decoteContentieuxForm.patchValue({
      litigationValue: twoDigitDecimalRound(litigationValue.toString())
    });
    this.contxValueChanged();
  }

  changeMaterialAge() {
    if (!this.decoteContentieuxForm.getRawValue().materialAge) {
      this.decoteContentieuxForm.patchValue({
        materialAge: this.selectedDecote + 1
      });
    }
  }

  disabledButtonCalcul() {

    let isDisabled = ((!this.ageFromDate && this.ageFromDateChanged) || !this.decoteContentieuxForm.getRawValue().materialAge || !this.decoteContentieuxForm.getRawValue().materialCurrentValue || this.decoteContentieuxForm.disabled || this.decoteContentieuxForm.getRawValue().isLostMaterial);
    if (isDisabled) {
      this.decoteContentieuxForm.patchValue({
        litigationValue: this.decoteContentieuxForm.get('isLostMaterial').value ? 0 : null
      });
    }
    return isDisabled;
  }


  resetCobaillage() {
    this.decoteContentieuxForm.controls.shareRate.reset();
    this.decoteContentieuxForm.controls.shareValue.reset();
    this.decoteContentieuxForm.controls.comment.reset();
    this.decoteContentieuxForm.patchValue({ isCobailing: false });
  }

  resetComponant() {
    if (this.selectedDecote >= 0) {
      this.decoteRatesControls[this.selectedDecote].patchValue({
        isChecked: false
      })
    }

    this.decoteContentieuxForm.controls.materialAge.reset();
    this.decoteContentieuxForm.controls.materialCurrentValue.reset();
    this.initializeDecoteContentieux();
    this.decoteContentieuxForm.controls.litigationValue.reset();
    this.resetCobaillage();
  }


}
