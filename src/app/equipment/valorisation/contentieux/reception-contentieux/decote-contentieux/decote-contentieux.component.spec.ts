
import { FormBuilder, FormArray } from '@angular/forms';

import { DecoteContentieuxComponent } from './decote-contentieux.component';
import { of, Subscription, throwError } from 'rxjs';
import { ValuationModel } from '../../../../../shared/models/valuation.model';
import { CatAssetTypeModel } from '../../../../../shared/models/cat-asset-type.model';
import { CatBrandModel } from '../../../../../shared/models/cat-brand.model';
import { CatNatureModel } from '../../../../../shared/models/cat-nature.model';
import { CatActivitySectorModel } from '../../../../../shared/models/cat-activity-sector.model';
import { ValuationRequestModel } from '../../../../../shared/models/valuation-request.model';
import { CatDiscountRateModel } from '../../../../../shared/models/cat-discount-rate.model';
import { LitigationModel } from '../../../../../shared/models/litigation.model';
import { LitigationDetail } from '../../../../../shared/models/litigation-detail.model';

describe('DecoteContentieuxComponent', () => {
  let fixture: DecoteContentieuxComponent;
  let formBuilderMock: FormBuilder;
  let catDiscountRateServiceMock: any;
  let materialQualification: any;
  let litigation: LitigationModel

  let listDiscountRatValues;
  let fileMock;

  let valuation: ValuationModel;
  let subscriptionMock: Subscription;




  beforeEach(() => {
    subscriptionMock = new Subscription();

    valuation = {
      "id": "1",
      "discountRateComment": "comment",
      "experId": "1",
      "controllerId": "1",
      "validatorId": "1",
      "isSubmitted" : null,
      "isDuplicated" : null,
      "paramValidationType": null,
      "valuationRequest": {
        "id": "3",
        "isANewAsset": true,

      } as ValuationRequestModel,
      "createdDate": null,
      "descNetwork": null,
      "descName": null,
      "catAssetType": {
        "id": "1"
      } as CatAssetTypeModel,
      "catBrand": {
        "id": "1"
      } as CatBrandModel,
      "catNature": {
        "id": "1"
      } as CatNatureModel,

      "catActivitySector": {
        "id": "1"
      } as CatActivitySectorModel
    }

    materialQualification = {
      "createdDate": null,
      "descNetwork": null,
      "descName": null,
      "catAssetType": {
        "id": "1"
      } as CatAssetTypeModel,
      "catBrand": {
        "id": "1"
      } as CatBrandModel,
      "catNature": {
        "id": "1"
      } as CatNatureModel,

      "catActivitySector": {
        "id": "1"
      } as CatActivitySectorModel
    }

    litigation = {
      "isLostMaterial": false,
      "id": "1",
      "catAssetType": {
        "id": "1"
      } as CatAssetTypeModel,
      "catBrand": {
        "id": "1"
      } as CatBrandModel,
      "catNature": {
        "id": "1"
      } as CatNatureModel,

      "catActivitySector": {
        "id": "1"
      } as CatActivitySectorModel,
      "litigationDetails": [
        {
          "id": "1",
          "discountRateYear": 1,
          "discountRateValue": 1,
          "isChecked": true
        } as LitigationDetail,
        {
          "id": "2",
          "discountRateYear": 2,
          "discountRateValue": 2,
          "isChecked": false
        } as LitigationDetail,
        {
          "id": "3",
          "discountRateYear": 3,
          "discountRateValue": 3,
          "isChecked": false
        } as LitigationDetail,
        {
          "id": "4",
          "discountRateYear": 4,
          "discountRateValue": 4,
          "isChecked": false
        } as LitigationDetail,
        {
          "id": "5",
          "discountRateYear": 5,
          "discountRateValue": 5,
          "isChecked": false
        } as LitigationDetail,
        {
          "id": "5",
          "discountRateYear": 5,
          "discountRateValue": 5,
          "isChecked": false
        } as LitigationDetail,
        {
          "id": "6",
          "discountRateYear": 6,
          "discountRateValue": 6,
          "isChecked": false
        } as LitigationDetail,
        {
          "id": "7",
          "discountRateYear": 7,
          "discountRateValue": 7,
          "isChecked": false
        } as LitigationDetail,
        {
          "id": "8",
          "discountRateYear": 8,
          "discountRateValue": 8,
          "isChecked": false
        } as LitigationDetail,
        {
          "id": "9",
          "discountRateYear": 9,
          "discountRateValue": 9,
          "isChecked": false
        } as LitigationDetail,
        {
          "id": "10",
          "discountRateYear": 10,
          "discountRateValue": 10,
          "isChecked": false
        } as LitigationDetail,
        {
          "id": "11",
          "discountRateYear": 11,
          "discountRateValue": 11,
          "isChecked": false
        } as LitigationDetail,
        {
          "id": "12",
          "discountRateYear": 12,
          "discountRateValue": 11,
          "isChecked": false
        } as LitigationDetail,
        {
          "id": "13",
          "discountRateYear": 13,
          "discountRateValue": 11,
          "isChecked": false
        } as LitigationDetail,
        {
          "id": "14",
          "discountRateYear": 11,
          "discountRateValue": 11,
          "isChecked": false
        } as LitigationDetail,
        {
          "id": "15",
          "discountRateYear": 15,
          "discountRateValue": 11,
          "isChecked": false
        } as LitigationDetail
      ]

    } as LitigationModel


    listDiscountRatValues = [
      {
        "id": "1",
        "discountRateYear": 4,
        "discountRateValue": 2,
        "collAssetEstimVal": 1,
        "valuation": valuation,
        "catDiscount": null
      } as CatDiscountRateModel,
      {
        "id": "1",
        "discountRateYear": 4,
        "discountRateValue": 2,
        "collAssetEstimVal": 1,
        "valuation": valuation,
        "catDiscount": null
      } as CatDiscountRateModel,
      {
        "id": "1",
        "discountRateYear": 4,
        "discountRateValue": 2,
        "collAssetEstimVal": 1,
        "valuation": valuation,
        "catDiscount": null
      } as CatDiscountRateModel,
      {
        "id": "1",
        "discountRateYear": 4,
        "discountRateValue": 2,
        "collAssetEstimVal": 1,
        "valuation": valuation,
        "catDiscount": null
      } as CatDiscountRateModel,
      {
        "id": "1",
        "discountRateYear": 4,
        "discountRateValue": 2,
        "collAssetEstimVal": 1,
        "valuation": valuation,
        "catDiscount": null
      } as CatDiscountRateModel,
      {
        "id": "1",
        "discountRateYear": 4,
        "discountRateValue": 2,
        "collAssetEstimVal": 1,
        "valuation": valuation,
        "catDiscount": null
      } as CatDiscountRateModel,
      {
        "id": "1",
        "discountRateYear": 4,
        "discountRateValue": 2,
        "collAssetEstimVal": 1,
        "valuation": valuation,
        "catDiscount": null
      } as CatDiscountRateModel,
      {
        "id": "1",
        "discountRateYear": 4,
        "discountRateValue": 2,
        "collAssetEstimVal": 1,
        "valuation": valuation,
        "catDiscount": null
      } as CatDiscountRateModel,
      {
        "id": "1",
        "discountRateYear": 4,
        "discountRateValue": 2,
        "collAssetEstimVal": 1,
        "valuation": valuation,
        "catDiscount": null
      } as CatDiscountRateModel,
      {
        "id": "1",
        "discountRateYear": 4,
        "discountRateValue": 2,
        "collAssetEstimVal": 1,
        "valuation": valuation,
        "catDiscount": null
      } as CatDiscountRateModel,
      {
        "id": "1",
        "discountRateYear": 4,
        "discountRateValue": 2,
        "collAssetEstimVal": 1,
        "valuation": valuation,
        "catDiscount": null
      } as CatDiscountRateModel,
      {
        "id": "1",
        "discountRateYear": 4,
        "discountRateValue": 2,
        "collAssetEstimVal": 1,
        "valuation": valuation,
        "catDiscount": null
      } as CatDiscountRateModel,
      {
        "id": "1",
        "discountRateYear": 4,
        "discountRateValue": 2,
        "collAssetEstimVal": 1,
        "valuation": valuation,
        "catDiscount": null
      } as CatDiscountRateModel,
      {
        "id": "1",
        "discountRateYear": 4,
        "discountRateValue": 2,
        "collAssetEstimVal": 1,
        "valuation": valuation,
        "catDiscount": null
      } as CatDiscountRateModel,
      {
        "id": "1",
        "discountRateYear": 4,
        "discountRateValue": 2,
        "collAssetEstimVal": 1,
        "valuation": valuation,
        "catDiscount": null
      } as CatDiscountRateModel,
      {
        "id": "2",
        "discountRateYear": 4,
        "discountRateValue": 2,
        "collAssetEstimVal": 1,
        "valuation": valuation,
        "catDiscount": null
      } as CatDiscountRateModel,
      {
        "id": "3",
        "discountRateYear": 4,
        "discountRateValue": 2,
        "collAssetEstimVal": 1,
        "valuation": valuation,
        "catDiscount": null
      } as CatDiscountRateModel,
    ];

    fileMock = new File(["Fenouil"], "Fenouil.txt", {
      type: "text/plain",
    });

    catDiscountRateServiceMock = {

      getDiscountRateByTypeIdOrBrandIdOrNatureIdOrSectorId: jest.fn().mockReturnValue(of((listDiscountRatValues))),

    };

    formBuilderMock = new FormBuilder();


    fixture = new DecoteContentieuxComponent(catDiscountRateServiceMock, formBuilderMock);
    fixture.valuationExpert = valuation;
    fixture.materialQualification = materialQualification;
    fixture.litigation = litigation;
    fixture.ngOnInit();

  });

  describe('Test Component', () => {
    it('should create', () => {
      expect(fixture).toBeTruthy();
    });
  });

  describe('Test onBlurLitigationDiscount', () => {
    it('should convertNumber', () => {
      fixture.decoteContentieuxForm.patchValue({ litigationDiscount: 10.555555 });
      fixture.onBlurLitigationDiscount()
      expect(fixture.decoteContentieuxForm.getRawValue().litigationDiscount).toEqual(10.556);

    });
  });


  describe('Test onBlurShareRate', () => {
    it('should convertNumber', () => {
      fixture.decoteContentieuxForm.patchValue({ shareRate: 10.555555 });
      fixture.onBlurShareRate()
      expect(fixture.decoteContentieuxForm.getRawValue().shareRate).toEqual(10.556);

    });
  });


  describe('Test get value()', () => {
    it('should return form value', () => {
      expect(fixture.value).toEqual(fixture.decoteContentieuxForm.getRawValue());
    })
  });

  describe('Test registerOnChange', () => {
    it('should get register', () => {
      let test = (test) => { return test };
      let size = fixture.subscriptions.length;
      fixture.registerOnChange(test);
      expect(fixture.onChange).not.toEqual(() => {
        //this is intentional
      });
      expect(fixture.subscriptions.length).toEqual(size + 1);


    });
  });

  describe('Test registerOnTouched', () => {
    it('should change value', () => {
      let test;
      fixture.registerOnTouched(test);
      expect(fixture.onTouched).toEqual(test);
    });
  });

  describe('Test changeAgeFromDate', () => {
    it('should changeAgeFromDate', () => {
      fixture.decoteContentieuxForm.patchValue({ ageFromDate: '10' });
    });
  });

  describe('Test writeValue', () => {
    it('should  write value', () => {
      let value = '1'
      fixture.writeValue(value);
      expect(fixture.value).toBeDefined();

    });
    it('should  write value', () => {
      let value = null
      fixture.writeValue(value);
      expect(fixture.decoteContentieuxForm.reset).toHaveBeenCalled

    });

  });
  describe('Test validate', () => {
    it('should retrun null', () => {
      let form: any;

      fixture.decoteContentieuxForm.patchValue({ materialAge: 'test' });
      fixture.decoteContentieuxForm.patchValue({ valueDate: 'test' });
      fixture.decoteContentieuxForm.patchValue({ contentieuxDecote: 'test' });
      fixture.decoteContentieuxForm.patchValue({ contentieuxValue: 'test' });
      fixture.decoteContentieuxForm.patchValue({ isBaillage: 'test' });
      fixture.decoteContentieuxForm.patchValue({ stolenMaterial: 'test' });
      expect(fixture.validate(form).profile.valid).toEqual(false);

    });

    it('should not retrun null', () => {
      let form: any;
      fixture.decoteContentieuxForm.controls['materialAge'].setErrors({ 'incorrect': true });
      expect(fixture.validate(form)).toEqual({ "profile": { "valid": false } });

    });
  });

  describe('Test ngOnDestroy', () => {

    it('should unsubscribe', () => {
      fixture.ngOnDestroy();
      const spyunsubscribe = jest.spyOn(subscriptionMock, 'unsubscribe');
      expect(spyunsubscribe).toBeDefined();
    });
  });

  describe('Test get f', () => {

    it('should return form controls', () => {

      expect(fixture.f).toEqual(fixture.decoteContentieuxForm.controls);
    });
  });

  describe('Test get decoteRatesControls', () => {

    it('should return getDecoteRatesFormArray controls', () => {

      expect(fixture.decoteRatesControls).toEqual(fixture.getDecoteRatesFormArray().controls);
    });
  });

  describe('Test get getDecoteRatesFormArray', () => {

    it('should return getDecoteRatesFormArray as array', () => {

      expect(fixture.getDecoteRatesFormArray()).toEqual((fixture.decoteContentieuxForm.get('decoteRates') as FormArray));
    });
  });

  describe('Test eventCheck()', () => {

    it('should set isModified false', () => {
      fixture.selectedDecote = 1;
      fixture.decoteRatesControls[2].patchValue({
        isModified: true
      })
      fixture.eventCheck(2);
      expect(fixture.decoteRatesControls[2].value.isChecked).toEqual(false);
      expect(fixture.selectedDecote).toEqual(2);

    });
    it('should set same index and selectected', () => {
      fixture.selectedDecote = 1;
      fixture.decoteRatesControls[1].patchValue({
        isModified: true
      })
      fixture.eventCheck(1);
      expect(fixture.decoteRatesControls[1].value.isChecked).toEqual(false);
      expect(fixture.selectedDecote).toEqual(1);

    });

  });
  describe('Test calculateShareValue()', () => {

    it('should calculate share Value', () => {
      fixture.decoteContentieuxForm.patchValue({ shareRate: 15 })
      fixture.decoteContentieuxForm.patchValue({ litigationValue: 500 })
      fixture.calculateShareValue();
      expect(fixture.decoteContentieuxForm.get('shareValue').value).toEqual("75");
    });
    it('should set share value to 0 ', () => {
      fixture.decoteContentieuxForm.patchValue({ shareRate: null })
      fixture.decoteContentieuxForm.patchValue({ litigationValue: null })
      fixture.calculateShareValue();
      expect(fixture.decoteContentieuxForm.get('shareValue').value).toEqual(0);
    });

  });
  describe('Test lockDiscountValues()', () => {

    it('should lockDiscountValues', () => {

      fixture.lockDiscountValues();
      //expect(fixture.decoteContentieuxForm.get('decoteRates').get('discountRateValue').disabled).toEqual(true);
    });
  });

  describe('Test isLostMaterialChange()', () => {

    it('should isLostMaterialChange disable', () => {

      fixture.isLostMaterialChange(true);
      fixture.decoteContentieuxForm.controls.isLostMaterial.enable();

      expect(fixture.decoteContentieuxForm.get('litigationValue').value).toEqual(0);
      expect(fixture.decoteContentieuxForm.get('comment').disabled).toEqual(false);
      expect(fixture.decoteContentieuxForm.get('materialAge').disabled).toEqual(true);
      expect(fixture.decoteContentieuxForm.get('materialCurrentValue').disabled).toEqual(true);
      expect(fixture.decoteContentieuxForm.get('litigationDiscount').disabled).toEqual(true);
      expect(fixture.decoteContentieuxForm.get('litigationValue').disabled).toEqual(true);
      expect(fixture.decoteContentieuxForm.get('isLostMaterial').disabled).toEqual(false);
    });
    it('should isLostMaterialChange enable', () => {

      fixture.isLostMaterialChange(false);
      expect(fixture.decoteContentieuxForm.get('litigationValue').value).toEqual(null);
      expect(fixture.decoteContentieuxForm.get('comment').disabled).toEqual(false);
      expect(fixture.decoteContentieuxForm.get('materialAge').disabled).toEqual(false);
      expect(fixture.decoteContentieuxForm.get('materialCurrentValue').disabled).toEqual(false);
      expect(fixture.decoteContentieuxForm.get('litigationDiscount').disabled).toEqual(false);
      expect(fixture.decoteContentieuxForm.get('litigationValue').disabled).toEqual(false);
      expect(fixture.decoteContentieuxForm.get('isLostMaterial').disabled).toEqual(false);
    });
  });

  describe('Test contxValueChanged()', () => {

    it('should do nothing', () => {
      fixture.decoteContentieuxForm.patchValue({ shareRate: null })
      fixture.contxValueChanged();
    });
    it('should calculate valueshare', () => {

      fixture.decoteContentieuxForm.patchValue({ shareRate: 15 })
      fixture.decoteContentieuxForm.patchValue({ litigationValue: 500 })
      fixture.contxValueChanged();
      expect(fixture.decoteContentieuxForm.get('shareValue').value).toEqual("75");
    });
  });
  describe('Test lockDiscountValues()', () => {

    it('should lockDiscountValues', () => {

      fixture.lockDiscountValues();
    });
  });
  describe('Test initializeDecoteContentieux()', () => {

    it('should set DecoteContentieux to zero', () => {

      fixture.initializeDecoteContentieux();
      expect(fixture.decoteContentieuxForm.get('litigationDiscount').value).toEqual('20');
    });
    it('should set isLostMaterialChange true', () => {
      fixture.litigation.isLostMaterial = true;
      fixture.initializeDecoteContentieux();
      expect(fixture.decoteContentieuxForm.get('litigationDiscount').value).toEqual('20');
    });
  });

  describe('Test changeLitigationDiscount', () => {

    it('should reset LitigationDiscount', () => {
      fixture.decoteContentieuxForm.patchValue({ litigationDiscount: '25' })

      fixture.changeLitigationDiscount(150);
      expect(fixture.decoteContentieuxForm.get('litigationDiscount').value).toEqual('20');
    });

    it('should KEEP LitigationDiscount', () => {
      fixture.decoteContentieuxForm.patchValue({ litigationDiscount: '25' })
      fixture.changeLitigationDiscount(25);
      expect(fixture.decoteContentieuxForm.get('litigationDiscount').value).toEqual('25');
    });

  });

  describe('Test calculLitigationValue()', () => {

    it('should return value of ligigationValue', () => {

      fixture.decoteContentieuxForm.patchValue({ litigationDiscount: 35 })
      fixture.decoteContentieuxForm.patchValue({ materialCurrentValue: 5800 })

      fixture.calculLitigationValue();
      expect(fixture.decoteContentieuxForm.get('litigationValue').value).toEqual("3770");
    });
  });

  describe('Test changeMaterialAge()', () => {

    it('should do nithing', () => {

      fixture.decoteContentieuxForm.patchValue({ materialAge: 25 })

      fixture.changeMaterialAge();
    });

    it('should change the matrial age', () => {
      fixture.selectedDecote = 5
      fixture.decoteContentieuxForm.patchValue({ materialAge: null })
      fixture.changeMaterialAge();
      expect(fixture.decoteContentieuxForm.get('materialAge').value).toEqual(6);
    });
  });

  describe('Test disabledButtonCalcul()', () => {

    it('do nothing to the btn calcul', () => {
      fixture.decoteContentieuxForm.patchValue({ litigationValue: 25 })

      fixture.ageFromDate = 5
      fixture.decoteContentieuxForm.patchValue({ materialAge: 11 })
      fixture.decoteContentieuxForm.patchValue({ materialCurrentValue: 25 })
      fixture.decoteContentieuxForm.enable();
      fixture.decoteContentieuxForm.patchValue({ isLostMaterial: false })

      fixture.disabledButtonCalcul();
      expect(fixture.decoteContentieuxForm.get('litigationValue').value).toEqual(25);
    });

    it('should return the state of calcul btn', () => {
      fixture.decoteContentieuxForm.patchValue({ litigationValue: 25 })

      fixture.ageFromDate = 5
      fixture.decoteContentieuxForm.patchValue({ materialAge: null })
      fixture.decoteContentieuxForm.patchValue({ materialCurrentValue: null })
      fixture.decoteContentieuxForm.enable();
      fixture.decoteContentieuxForm.patchValue({ isLostMaterial: false })

      fixture.disabledButtonCalcul();
      expect(fixture.decoteContentieuxForm.get('litigationValue').value).toEqual(null);
    });
    it('should return the state of calcul btn and ligigation is zero', () => {
      fixture.decoteContentieuxForm.patchValue({ litigationValue: 25 })

      fixture.ageFromDate = 5
      fixture.decoteContentieuxForm.patchValue({ materialAge: null })
      fixture.decoteContentieuxForm.patchValue({ materialCurrentValue: null })
      fixture.decoteContentieuxForm.enable();
      fixture.decoteContentieuxForm.patchValue({ isLostMaterial: true })

      fixture.disabledButtonCalcul();
      expect(fixture.decoteContentieuxForm.get('litigationValue').value).toEqual(0);
    });

  });

  describe('Test resetCobaillage()', () => {

    it('should reset and disable all cobaillage items', () => {

      fixture.resetCobaillage();
      expect(fixture.decoteContentieuxForm.get('isCobailing').value).toBeFalsy;
      expect(fixture.decoteContentieuxForm.get('shareValue').value).toEqual(null);
      expect(fixture.decoteContentieuxForm.get('comment').value).toEqual(null);
      expect(fixture.decoteContentieuxForm.get('shareRate').value).toEqual(null);
    });
  });

  describe('Test changeAgeFromDate()', () => {

    it('should change age from date', () => {

      fixture.changeAgeFromDate = 5;
      expect(fixture.ageFromDate).toEqual(5);

    });
  });

  describe('Test isDateFromAgeChanged()', () => {

    it('should change ageFromDateChanged ', () => {

      fixture.isDateFromAgeChanged = true;
      expect(fixture.ageFromDateChanged).toBeTruthy();

    });
  });

  describe('Test isSubmitted()', () => {

    it('should call markAllAsTouched of decoteContentieuxForm ', () => {

      fixture.isSubmitted = true;
      expect(fixture.decoteContentieuxForm.touched).toBeTruthy();

    });
  });

  describe('Test createDecoteForm()', () => {

    it('should createDecoteForm litigation id not equal catasset', () => {
      fixture.litigation.id = "15";
      fixture.litigation.catAssetType.id = "14";
      fixture.createDecoteForm();
      expect(fixture.discountRateList).toEqual(fixture.discountRateListFromCatalog);
    });

    it('should createDecoteForm no discount rate list', () => {
      fixture.litigation.id = "15";
      fixture.litigation.catAssetType.id = "14";
      fixture.litigation.litigationDetails = null;
      fixture.discountRateListFromLitigation = null;
      fixture.createDecoteForm();
      let discountList = fixture.discountRateList.map(a => { { a.id = null } return { ...a } })
      expect(fixture.discountRateList).toEqual(discountList);
    });

    it('should createDecoteForm error', () => {
      spyOn(catDiscountRateServiceMock, 'getDiscountRateByTypeIdOrBrandIdOrNatureIdOrSectorId').and.returnValue(throwError({ status: 404 }));
      fixture.createDecoteForm();
    });
  });


  describe('Test initMaterialAge()', () => {

    it('should set the decote', () => {
      fixture.selectedDecote = 2;
      fixture.ageFromDate = 2;
      fixture.initMaterialAge();
      expect(fixture.decoteRatesControls[fixture.selectedDecote].value.isChecked).toEqual(true);
    });

    it('should set last decote', () => {
      fixture.ageFromDate = 16;
      fixture.initMaterialAge();
      expect(fixture.selectedDecote).toEqual(14);
    });

    it('should reset all decotes', () => {
      fixture.selectedDecote = 0;
      fixture.initMaterialAge();
      expect(fixture.selectedDecote).toEqual(0);
    });

  });

});
