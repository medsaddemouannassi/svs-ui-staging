import { Component, EventEmitter, forwardRef, Input, OnInit, Output } from '@angular/core';
import { ControlValueAccessor, FormBuilder, FormGroup, NG_VALIDATORS, NG_VALUE_ACCESSOR, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { forkJoin, of, Subscription } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { CatActivitySectorModel } from '../../../../../shared/models/cat-activity-sector.model';
import { CatAssetTypeModel } from '../../../../../shared/models/cat-asset-type.model';
import { CatBrandModel } from '../../../../../shared/models/cat-brand.model';
import { CatNatureModel } from '../../../../../shared/models/cat-nature.model';
import { CatActivitySectorService } from '../../../../../shared/services/cat-activity-sector.service';
import { CatAssetTypeService } from '../../../../../shared/services/cat-asset-type.service';
import { CatBrandService } from '../../../../../shared/services/cat-brand.service';
import { CatNatureService } from '../../../../../shared/services/cat-nature.service';
import { CreateCatRequestModalComponent } from '../../../expert/material-qualification-component/create-cat-request-modal/create-cat-request-modal.component';


@Component({
  selector: 'app-material-qualification-contentieux',
  templateUrl: './material-qualification-contentieux.component.html',
  styleUrls: ['./material-qualification-contentieux.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MaterialQualificationContentieuxComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => MaterialQualificationContentieuxComponent),
      multi: true
    }
  ]
})
export class MaterialQualificationContentieuxComponent implements OnInit, ControlValueAccessor {

  mtrQualifForm: FormGroup;
  subscriptions: Subscription[] = [];
  isFormDisabled = true;
  @Output() isFormQualifDisabled = new EventEmitter<boolean>(true);
  @Input() valuationExpert;
  @Input() litigation;

  @Input() set isSubmitted(value) {
    if (value && this.mtrQualifForm) {
      this.mtrQualifForm.markAllAsTouched();
    }

  }

  get value(): any {
    return this.mtrQualifForm.value;
  }

  set value(value) {
    if (value) {
      this.mtrQualifForm.patchValue(value);
      this.onChange(value);
      this.onTouched();
    }
  }

  registerOnChange(fn): void {
    this.onChange = fn;
  }

  registerOnTouched(fn): void {
    this.onTouched = fn;
  }

  writeValue(value): void {
    if (value) {
      this.value = value;
    }

    if (value === null) {
      this.mtrQualifForm.reset();
    }
  }

  activitySectorOptions: CatActivitySectorModel[];
  naturesOptions: CatNatureModel[];
  brandsOptions: CatBrandModel[];
  typeOptions: CatAssetTypeModel[];

  activitySectorOptionsInit: CatActivitySectorModel[];
  naturesOptionsInit: CatNatureModel[];
  brandsOptionsInit: CatBrandModel[];
  typeOptionsInit: CatAssetTypeModel[];

  constructor(private fb: FormBuilder, private catActivitySectorService: CatActivitySectorService, private catNatureService: CatNatureService,
    private catBrandService: CatBrandService, private catAssetTypeService: CatAssetTypeService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.mtrQualifForm = this.fb.group({
      catActivitySector: ['', Validators.required],
      catNature: ['', Validators.required],
      catBrand: ['', Validators.required],
      catAssetType: ['', Validators.required]
    });
    this.subscriptions.push(
      this.mtrQualifForm.valueChanges.subscribe(value => {
        this.onChange(value);
        this.onTouched();
      }));
    
    if(this.litigation && this.litigation.id){
      this.getAllMaterialQualifLists(this.litigation.catActivitySector.id, this.litigation.catNature.id, this.litigation.catBrand.id);
    }else{
      this.getAllMaterialQualifLists(this.valuationExpert.catActivitySector.id, this.valuationExpert.catNature.id, this.valuationExpert.catBrand.id);
    }


  }

  onChange: any = () => {
    // This is intentional
  };
  onTouched: any = () => {
    // This is intentional
  };


  getAllMaterialQualifLists(sectorId: string, natureId: string, brandId : string) {

    forkJoin([this.catActivitySectorService.getActivitySectorList().pipe(catchError(error => of([]))),
      this.catNatureService.getNatureListByCatActivitySectorID(sectorId).pipe(catchError(error => of([]))),
      this.catBrandService.getCatBrandListByCatNatureId(natureId).pipe(catchError(error => of([]))),
      this.catAssetTypeService.getAssetTypeListByCatBrandId(brandId).pipe(catchError(error => of([])))]
    ).subscribe(data => {
      this.activitySectorOptions = data[0];
      this.naturesOptions = data[1];
      this.brandsOptions = data[2];
      this.typeOptions = data[3];

    }, (err) => {
      console.error(err);
    }, () => {
      this.updateFormValues();
      this.stashCatLists();
    });

  }

  updateFormValues() {
    if(this.litigation && this.litigation.id){
      this.mtrQualifForm.patchValue({
        catActivitySector: this.litigation.catActivitySector,
        catNature: this.litigation.catNature,
        catBrand: this.litigation.catBrand,
        catAssetType: this.litigation.catAssetType
      });
    }else{
      this.mtrQualifForm.patchValue({
        catActivitySector: this.valuationExpert.catActivitySector,
        catNature: this.valuationExpert.catNature,
        catBrand: this.valuationExpert.catBrand,
        catAssetType: this.valuationExpert.catAssetType
      });
    }
    
  }

  setDisabledState(isDisabled: boolean) {
    if (isDisabled) {
      this.mtrQualifForm.disable();
    } else {
      this.mtrQualifForm.enable();
    }
  }

  enableQualif(){
    //enable qualif form
    this.mtrQualifForm.enable();
    //to show 'valider' and 'annuler' btns
    this.isFormDisabled = false;
    //to be send to parent
    this.isFormQualifDisabled.emit(false);
  }

  disableQualif(){
    this.mtrQualifForm.disable();
    // to show 'modifier' btn
    this.isFormDisabled = true;
    //reset form values
    this.updateFormValues();
    //to send to parent
    this.isFormQualifDisabled.emit(true);

  }
  resetQualif(){
    this.resetCatList();
    this.disableQualif();
  }

  validerQualif(){
    this.mtrQualifForm.markAllAsTouched();
    if(this.mtrQualifForm.valid){
      this.mtrQualifForm.disable();
      this.isFormDisabled = true;
      this.isFormQualifDisabled.emit(true);
    }
 
  }

  loadCatNatureListBySectorId() {
    this.mtrQualifForm.patchValue({
      catNature: null,
      catBrand: null,
      catAssetType: null
    });
    this.naturesOptions = null;
    this.brandsOptions = null;
    this.typeOptions = null;

    if (this.mtrQualifForm.controls['catActivitySector'].value) {
      this.catNatureService.getNatureListByCatActivitySectorID(this.mtrQualifForm.controls['catActivitySector'].value.id).subscribe(data => {
        this.naturesOptions = data;
      });
    }
  }

  loadCatBrandListByCatNatureId() {
    this.mtrQualifForm.patchValue({
      catBrand: null,
      catAssetType: null
    });
    this.typeOptions = null;
    this.brandsOptions = null;

    if (this.mtrQualifForm.controls['catNature'].value) {
      this.catBrandService.getCatBrandListByCatNatureId(this.mtrQualifForm.controls['catNature'].value.id).subscribe(data => {
        this.brandsOptions = data;

      });
    }
  }

  loadCatAssetTypeListByCatBrandId() {
    this.typeOptions = null;
    this.mtrQualifForm.patchValue({ catAssetType: null });

    if (this.mtrQualifForm.controls['catBrand'].value) {
      this.catAssetTypeService.getAssetTypeListByCatBrandId(this.mtrQualifForm.controls['catBrand'].value.id).subscribe(data => {
        this.typeOptions = data;
      });
    }
  }

  stashCatLists(){
    this.activitySectorOptionsInit = this.activitySectorOptions;
    this.naturesOptionsInit = this.naturesOptions;
    this.brandsOptionsInit = this.brandsOptions;
    this.typeOptionsInit = this.typeOptions;
  }

  resetCatList(){
    this.activitySectorOptions = this.activitySectorOptionsInit;
    this.naturesOptions = this.naturesOptionsInit;
    this.brandsOptions = this.brandsOptionsInit;
    this.typeOptions = this.typeOptionsInit;
  }


  addCatRequest(desc) {
    const modalRef = this.modalService.open(CreateCatRequestModalComponent,
      {
        scrollable: true,
        windowClass: "createBrandModalClass",
        centered: true
      }
    );
    modalRef.componentInstance.catNature = this.mtrQualifForm.getRawValue().catNature;
    modalRef.componentInstance.catActivitySector = this.mtrQualifForm.getRawValue().catActivitySector;
    if (desc=='type') {
      modalRef.componentInstance.catBrand = this.mtrQualifForm.getRawValue().catBrand;

    }
    else {
      modalRef.componentInstance.catBrand = null;

    }

  }
}
