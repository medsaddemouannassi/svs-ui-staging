
import { MaterialQualificationContentieuxComponent } from './material-qualification-contentieux.component';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';
import { CatActivitySectorModel } from '../../../../../shared/models/cat-activity-sector.model';
import { CatNatureModel } from '../../../../../shared/models/cat-nature.model';
import { CatBrandModel } from '../../../../../shared/models/cat-brand.model';
import { CatAssetTypeModel } from '../../../../../shared/models/cat-asset-type.model';
import { ValuationModel } from '../../../../../shared/models/valuation.model';
import { EventEmitter } from '@angular/core';
import { LitigationModel } from '../../../../../shared/models/litigation.model';

describe('MaterialQualificationContentieuxComponent', () => {
  let fixture: MaterialQualificationContentieuxComponent;
  let formBuilderMock: FormBuilder;
  let catActivitySectorServiceMock: any;
  let catNatureServiceMock: any;
  let modalServiceMock: any;
  let catBrandServicMock: any;
  let catAssetTypeServiceMock: any;

  let sectorActivityList = [{

  } as CatActivitySectorModel] as CatActivitySectorModel[];

  let natureList = [{

  } as CatNatureModel] as CatNatureModel[];

  let brandList = [{

  } as CatBrandModel] as CatBrandModel[];

  let typeList = [{

  } as CatAssetTypeModel] as CatAssetTypeModel[];


  beforeEach(() => {



    formBuilderMock = new FormBuilder();

    catActivitySectorServiceMock = {
      getActivitySectorList: jest.fn().mockReturnValue(of(sectorActivityList)),
    }

    catNatureServiceMock = {
      getNatureListByCatActivitySectorID: jest.fn().mockReturnValue(of(natureList)),
    }

    catBrandServicMock = {
      getCatBrandListByCatNatureId: jest.fn().mockReturnValue(of(brandList)),
    }

    catAssetTypeServiceMock = {
      getAssetTypeListByCatBrandId: jest.fn().mockReturnValue(of(typeList)),
    }

    modalServiceMock = {
      open: jest.fn().mockReturnValue({
        componentInstance: {
          content: ''
        }
      }),
    }

    fixture = new MaterialQualificationContentieuxComponent(formBuilderMock, catActivitySectorServiceMock, catNatureServiceMock, catBrandServicMock, catAssetTypeServiceMock, modalServiceMock);
    fixture.valuationExpert = {
      id: '1',
      catActivitySector: {
        id: '1',
        label: 'Transport'
      },
      catNature: {
        id: '11',
        label: 'Abateuse'
      } as CatNatureModel,
      catBrand: {
        id: '13',
        label: 'bosh'
      } as CatBrandModel,
      catAssetType: {
        id: '114',
        label: 'typeName'
      } as CatAssetTypeModel
    } as ValuationModel;
    fixture.ngOnInit();
  });

  describe('Test Component', () => {
    it('should create', () => {
      expect(fixture).toBeTruthy();
    });
  });

  describe('Test setDisabledState', () => {
    it('should  disable form', () => {

      fixture.setDisabledState(true);
      expect(fixture.mtrQualifForm.disabled).toEqual(true);

    });
    it('should  enable form', () => {

      fixture.setDisabledState(false);
      expect(fixture.mtrQualifForm.disabled).toEqual(false);

    });

  });


  describe('Test updateFormValues()', () => {
    it('should update form controls', () => {

      fixture.updateFormValues();
      expect(fixture.mtrQualifForm.controls['catActivitySector'].value).toEqual(fixture.valuationExpert.catActivitySector);
      expect(fixture.mtrQualifForm.controls['catNature'].value).toEqual(fixture.valuationExpert.catNature);
      expect(fixture.mtrQualifForm.controls['catBrand'].value).toEqual(fixture.valuationExpert.catBrand);
      expect(fixture.mtrQualifForm.controls['catAssetType'].value).toEqual(fixture.valuationExpert.catAssetType);

    });
  });

  describe('Test get value()', () => {
    it('should return form value', () => {
      expect(fixture.value).toEqual(fixture.mtrQualifForm.value);
    })
  });

  describe('Test registerOnChange', () => {
    it('should change value', () => {
      let test;
      fixture.registerOnChange(test);
      expect(fixture.onChange).toEqual(test);
    });
  });

  describe('Test registerOnTouched', () => {
    it('should change value', () => {
      let test;
      fixture.registerOnTouched(test);
      expect(fixture.onTouched).toEqual(test);
    });
  });

  describe('Test writeValue', () => {
    it('should  write value', () => {
      let value = '1'
      fixture.writeValue(value);
      expect(fixture.value).toBeDefined();

    });
    it('should  write value', () => {
      let value = null
      fixture.writeValue(value);
      expect(fixture.mtrQualifForm.reset).toHaveBeenCalled

    });

  });

  describe('Test enableQualif()', () => {
    it('should enable form', () => {

      fixture.enableQualif();
      expect(fixture.mtrQualifForm.enabled).toEqual(true);
      expect(fixture.isFormDisabled).toEqual(false);

      let expectedEvent = new EventEmitter<boolean>(true);
      expectedEvent.emit(false);
      expect(fixture.isFormQualifDisabled).toEqual(expectedEvent);

    })
  });

  describe('Test disableQualif()', () => {
    it('should disable form', () => {

      fixture.disableQualif();
      expect(fixture.mtrQualifForm.disabled).toEqual(true);
      expect(fixture.isFormDisabled).toEqual(true);
      expect(fixture.updateFormValues()).toHaveBeenCalled

      let expectedEvent = new EventEmitter<boolean>(true);
      expectedEvent.emit(true);
      expect(fixture.isFormQualifDisabled).toEqual(expectedEvent);

    })
  });

  describe('Test resetQualif()', () => {
    it('should disable form', () => {

      fixture.resetQualif();
      expect(fixture.mtrQualifForm.disabled).toEqual(true);
      expect(fixture.resetCatList()).toHaveBeenCalled

    })
  });

  describe('Test validerQualif()', () => {
    it('should validate qualif', () => {

      fixture.validerQualif();
      expect(fixture.mtrQualifForm.disabled).toEqual(true);
      expect(fixture.isFormDisabled).toEqual(true);
      let expectedEvent = new EventEmitter<boolean>(true);
      expectedEvent.emit(true);
      expect(fixture.isFormQualifDisabled).toEqual(expectedEvent);

    })

   });

   describe('Test loadCatNatureListBySectorId()', () => {
    it('should reload naturesOptions and set brandsOptions and typeOptions to null', () => {

      fixture.loadCatNatureListBySectorId();
      expect(fixture.naturesOptions).toEqual(natureList);
      expect(fixture.brandsOptions).toEqual(null);
      expect(fixture.typeOptions).toEqual(null);

    })
  });

  describe('Test loadCatBrandListByCatNatureId()', () => {
    it('should reload brandsOptions and set typeOptions to null', () => {

      fixture.loadCatBrandListByCatNatureId();
      expect(fixture.brandsOptions).toEqual(brandList);
      expect(fixture.typeOptions).toEqual(null);

    })
  });

  describe('Test loadCatAssetTypeListByCatBrandId()', () => {
    it('should reload typeOptions', () => {

      fixture.loadCatAssetTypeListByCatBrandId();
      expect(fixture.typeOptions).toEqual(typeList);

    })
  });

  describe("test addCatRequest", () => {
    it('should open modalService with type request', () => {
      fixture.addCatRequest('type');
      expect(modalServiceMock.open).toHaveBeenCalled
    });

    it('should open modalService with brand request', () => {
      fixture.addCatRequest('brand');
      expect(modalServiceMock.open).toHaveBeenCalled
    });
  });
  describe('Test isSubmited', () => {


    it('should mark as touched', () => {
      fixture.isSubmitted = true;

      expect(fixture.mtrQualifForm.touched).toEqual(true);
    })

    it('should not mark as touched', () => {
      fixture.isSubmitted = false;

      expect(fixture.mtrQualifForm.touched).toEqual(false);
    })
  });
});
