import { FormBuilder, FormControl } from '@angular/forms';
import { of, Subject } from 'rxjs';
import { ValuationRequestModel } from '../../../../../shared/models/valuation-request.model';
import { ValuationDocumentModel } from '../../../../../shared/models/valuationDocument.model';
import { InformationContentieuxComponent } from './information-contentieux.component';
import { TestBed } from '@angular/core/testing';
import { DomSanitizer } from '@angular/platform-browser';


describe('InformationContentieuxComponent', () => {
  let fixture: InformationContentieuxComponent;
  let formBuilderMock: FormBuilder;
  let valuationRequestDocsServiceMock: any;
  let popupServiceMock: any;
  let documentsList: any;
  let sanitizer: any;



  beforeEach(() => {
    formBuilderMock = new FormBuilder();
    documentsList = [{

      "docName": "document1",
    } as ValuationDocumentModel, {

      "docName": "document2"
    } as ValuationDocumentModel]
    valuationRequestDocsServiceMock = {
      getLitigiationDocByValReqId: jest.fn().mockReturnValue(of(documentsList)),
      displayDocument: jest.fn().mockReturnValue(of({ url: 'url' }))
    }

    popupServiceMock = {
      popupPDF: jest.fn().mockReturnValue(of(true)),
      popupInfo: jest.fn().mockReturnValue(of(true)),
      onConfirm: new Subject<boolean>()

    }
    sanitizer = TestBed.inject(DomSanitizer);

    fixture = new InformationContentieuxComponent(formBuilderMock, valuationRequestDocsServiceMock, popupServiceMock,sanitizer);
    fixture.valuationRequest = { id: '1', isLicencePlateAvaileble: false } as ValuationRequestModel;

    fixture.ngOnInit();

  })
  describe("test ngOnInit()", () => {
    it('ngOnInit', () => {

      fixture.valuationRequest = { id: '1', isLicencePlateAvaileble: true } as ValuationRequestModel; fixture.ngOnInit()

      expect(fixture.form).toBeDefined()
    })


  })
  describe('Test registerOnChange', () => {
    it('should get register', () => {
      let test = (test) => { return test };
      let size = fixture.subscriptions.length;
      fixture.registerOnChange(test);
      expect(fixture.onChange).not.toEqual(() => {
        //this is intentional
      });
      expect(fixture.subscriptions.length).toEqual(size + 1);


    });
  });


  describe("test setDisabledState()", () => {
    it('should enable form', () => {
      fixture.setDisabledState(false);
      expect(fixture.form.enable).toHaveBeenCalled
    });

    it('should disable form', () => {
      fixture.setDisabledState(true);
      expect(fixture.form.disable).toHaveBeenCalled
    });


  });

  describe('Test writeValue', () => {
    it('should get value', () => {
      const value = {
        contractKSIOP: 'ddd',
        fixedAsset: 'ddd',
        plateNumber: 'ddd',
        invoiceDate: '01/01/2001',
        files: null,
        updatedFiles: [],
        deletedDocs: []



      };

      fixture.writeValue(value);
      expect(fixture.value).toEqual(value)

      fixture.value = value;
      expect(fixture.value).toEqual(value)
    });

    it('should not set  value', () => {
      const value = {
        contractKSIOP: 'ddd',
        fixedAsset: 'ddd',
        plateNumber: 'ddd',
        invoiceDate: '01/01/2001',
        files: null,


      };

      fixture.writeValue(null);
      expect(fixture.value).not.toEqual(value)

      fixture.value = null;
      expect(fixture.value).not.toEqual(value)


    });
  });

  describe('Test registerOnTouched', () => {
    it('should get register', () => {
      fixture.registerOnTouched(test);
      expect(fixture.onTouched).not.toEqual(() => {
        //this is intentional
      });
      expect(fixture.onTouched).toEqual(test);
    });
  });

  describe('Test validate', () => {
    it('should retrun null', () => {

      const value = {
        contractKSIOP: 'ddd',
        fixedAsset: 'ddd',
        plateNumber: 'ddd',
        invoiceDate: '01/01/2001',
        files: null,


      };

      fixture.value = value;
      let formControl: FormControl = new FormControl();

      expect(fixture.validate(formControl)).toEqual(null);

    });

    it('should not retrun null', () => {



      fixture.writeValue(null);
      let formControl: FormControl = new FormControl();

      expect(fixture.validate(formControl)).toEqual({ informationContentieux: { valid: false } });



    });
  });

  describe('Test onblurinvoiceDate', () => {


    it('should formate date', () => {
      fixture.form.patchValue({ invoiceDate: '21022022' });

      spyOn(fixture.oninvoiceDateChange, "emit").and.returnValue('')
      fixture.onblurinvoiceDate();


      expect(fixture.form.getRawValue().invoiceDate).toEqual('21/02/2022');

      expect(fixture.oninvoiceDateChange.emit).toHaveBeenCalledWith('21/02/2022');


    })

    it('should not formate date', () => {
      fixture.form.patchValue({ invoiceDate: null });

      spyOn(fixture.oninvoiceDateChange, "emit").and.returnValue('')
      fixture.onblurinvoiceDate();


      expect(fixture.form.getRawValue().invoiceDate).toEqual(null);

      expect(fixture.oninvoiceDateChange.emit).toHaveBeenCalledWith(null);


    })


  });

  describe('Test displayDocument', () => {
    it('should display doc ', () => {
      fixture.litigationDocs = documentsList;


      fixture.displayDocument(1)
      expect(valuationRequestDocsServiceMock.displayDocument).toHaveBeenCalled();
      valuationRequestDocsServiceMock.displayDocument(1).subscribe(res => {
        expect(popupServiceMock.popupPDF).toHaveBeenCalled()
      })
    })
  });

  describe('Test:  editDocumentName', () => {
    it('should set doc edited value to true ', () => {
      fixture.litigationDocs = documentsList;
      fixture.editedDocumentList = [false, false];
      fixture.documentName = "test";
      fixture.editDocumentName(1);
      expect(fixture.editedDocumentList[1]).toEqual(true);

    });

  });

  describe('Test:  delete document', () => {
    it('should delete document when the doc not updated ', () => {
      fixture.litigationDocs = documentsList;

      fixture.deleteDocument(1);
      popupServiceMock.onConfirm.next(true);
      popupServiceMock.onConfirm.subscribe((res)=>{
        expect(fixture.form.getRawValue().deletedDocs).toEqual([documentsList[1]]);

      })

    });

    it('should delete document when the doc not updated ', () => {
      fixture.litigationDocs = documentsList;
      fixture.litigationDocs = documentsList;
      fixture.editedDocumentList = [false, true];
      fixture.documentName = "test";
      fixture.onBlurDocument(1);

      fixture.deleteDocument(1);
      popupServiceMock.onConfirm.next(true);
      popupServiceMock.onConfirm.subscribe((res)=>{
        expect(fixture.form.getRawValue().deletedDocs).toEqual([documentsList[1]]);
        expect(fixture.form.getRawValue().updatedFiles).toEqual([]);


      })

    });

    it('should not delete documents', () => {
      fixture.litigationDocs = documentsList;
      fixture.litigationDocs = documentsList;
      fixture.editedDocumentList = [false, true];
      fixture.documentName = "test";
      fixture.onBlurDocument(1);

      fixture.deleteDocument(1);
      popupServiceMock.onConfirm.next(false);
      popupServiceMock.onConfirm.subscribe((res)=>{
        expect(fixture.form.getRawValue().deletedDocs).toEqual([]);


      })

    });

  });

  describe('Test:  onBlurDocument', () => {
    it('should updateDocs ', () => {
      fixture.litigationDocs = documentsList;
      fixture.editedDocumentList = [false, true];
      fixture.documentName = "test";
      fixture.onBlurDocument(1);
      expect(fixture.form.getRawValue().updatedFiles[0].docName).toEqual("test");

    });

    it('should set update docs', () => {
      fixture.litigationDocs = documentsList;
      fixture.editedDocumentList = [false, true];
      fixture.documentName = "test";
      fixture.form.patchValue({ updatedFiles: [documentsList[1]] })
      fixture.onBlurDocument(1);
      expect(fixture.form.getRawValue().updatedFiles[0].docName).toEqual("test");

    });

    it('should init update docs', () => {
      fixture.litigationDocs = documentsList;
      fixture.editedDocumentList = [false, true];
      fixture.documentName = "test";
      fixture.form.patchValue({ updatedFiles: null })
      fixture.onBlurDocument(1);
      expect(fixture.form.getRawValue().updatedFiles[0].docName).toEqual("test");

    });

  });


  describe('Test isSubmited', () => {


    it('should mark as touched', () => {
      fixture.isSubmitted = true;

      expect(fixture.form.touched).toEqual(true);
    })

    it('should not mark as touched', () => {
      fixture.isSubmitted = false;

      expect(fixture.form.touched).toEqual(false);
    })
  });

  describe('Test ngOnDestroy', () => {
    it('should unsubscribe', () => {
      fixture.ngOnDestroy();

      expect(fixture.subscriptions[0].closed).toEqual(true);
    });

    it('should not unsubscribe', () => {
      fixture.subscriptions[0].unsubscribe();
      fixture.ngOnDestroy();

      expect(fixture.subscriptions[0].closed).toEqual(true);
    })
  });
})
