import { Component, EventEmitter, forwardRef, Input, OnInit, Output, SecurityContext } from '@angular/core';
import { ControlValueAccessor, FormBuilder, FormControl, FormGroup, NG_VALIDATORS, NG_VALUE_ACCESSOR, Validator, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { ConfirmationModalComponent } from '../../../../../shared/confirmation-modal/confirmation-modal.component';
import { dateValidator } from '../../../../../shared/CustomValidators';
import { ValuationRequestModel } from '../../../../../shared/models/valuation-request.model';
import { ValuationDocumentModel } from '../../../../../shared/models/valuationDocument.model';
import { PopupService } from '../../../../../shared/services/popup.service';
import { ValuationRequestDocsService } from '../../../../../shared/services/valuation-request-docs.service';
import { formatDateToMatchForamt } from '../../../../../shared/utils';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-information-contentieux',
  templateUrl: './information-contentieux.component.html',
  styleUrls: ['./information-contentieux.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InformationContentieuxComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => InformationContentieuxComponent),
      multi: true
    }
  ]
})
export class InformationContentieuxComponent implements OnInit, ControlValueAccessor, Validator {

  constructor(private fb: FormBuilder, private valuationRequestDocsService: ValuationRequestDocsService, private popupService: PopupService,private sanitizer: DomSanitizer) { }

  form: FormGroup;

  subscriptions: Subscription[] = [];



  @Input() valuationRequest: ValuationRequestModel;


  @Input() set isSubmitted(value) {
    if (value && this.form) {
      this.form.markAllAsTouched();
    }

  }
  @Output() oninvoiceDateChange = new EventEmitter<string>();

  litigationDocs: ValuationDocumentModel[] = [];
  editedDocumentList: boolean[] = [];
  documentName: string;



  get value(): any {
    return this.form.getRawValue();
  }

  set value(value) {
    if (value) {
      this.form.patchValue(value);
      this.onChange(value);
      this.onTouched();
    }
  }
  ngOnInit(): void {


    this.form = this.fb.group({

      contractKSIOP: ['', Validators.required],
      fixedAsset: [],
      plateNumber: [{ value: null, disabled: !this.valuationRequest.isLicencePlateAvaileble }, this.valuationRequest.isLicencePlateAvaileble ? Validators.required : []],
      invoiceDate: [{ value: null, disabled: false },
      [Validators.pattern(/(^(((0[1-9]|1[0-9]|2[0-8])([\/]|())(0[1-9]|1[012]))|((29|30|31)([\/]|())(0[13578]|1[02]))|((29|30)([\/]|())(0[4,6,9]|11)))([\/]|())(19|[2-9][0-9])\d\d$)|(^29([\/]|())02([\/]|())(19|[2-9][0-9])(00|04|08|12|16|20|24|28|32|36|40|44|48|52|56|60|64|68|72|76|80|84|88|92|96)$)/), Validators.required, dateValidator(new Date())]],
      files: [],
      updatedFiles: [[]],
      deletedDocs: [[]]
    });


    this.subscriptions.push(
      this.form.valueChanges.subscribe(value => {
        this.onChange(value);
        this.onTouched();
      })
    );

    this.valuationRequestDocsService.getLitigiationDocByValReqId(this.valuationRequest.id).subscribe(docs => this.litigationDocs = docs);





  }
  onblurinvoiceDate() {
    if (this.form.get('invoiceDate').value) {
      this.form.patchValue({
        invoiceDate: formatDateToMatchForamt(this.form.get('invoiceDate').value)
      })
    }

    if (this.form.get('invoiceDate').valid) {

      this.oninvoiceDateChange.emit(this.form.get('invoiceDate').value);

    } else {
      this.oninvoiceDateChange.emit(null);
    }
  }




  onChange: any = () => {       //this is intentional
  };
  onTouched: any = () => {       //this is intentional
  };

  registerOnChange(fn: any) {
    this.subscriptions.push(this.form.valueChanges.pipe(map(_ => this.form.getRawValue())).subscribe(fn));
  }

  writeValue(value): void {
    if (value) {
      this.form.setValue(value);

    }

    if (value === null) {
      this.form.reset();
    }
  }

  registerOnTouched(fn): void {
    this.onTouched = fn;
  }

  setDisabledState(disabled: boolean) {

    if (disabled) {
      this.form.disable();


    }
    else {
      this.form.enable();
      if (!this.valuationRequest.isLicencePlateAvaileble) {
        this.form.get('plateNumber').disable();
      }
    }


  }

  validate(_: FormControl): any {
    return this.form.valid ? null : { informationContentieux: { valid: false } };
  }

  displayDocument(index) {
    this.valuationRequestDocsService.displayDocument(this.litigationDocs[index]).subscribe(res => {
      let safeUrl = this.sanitizer.sanitize(SecurityContext.RESOURCE_URL, this.sanitizer.bypassSecurityTrustResourceUrl(res.url));

      window.open(safeUrl, "_blank");
    })

  }

  editDocumentName(index: number) {
    this.editedDocumentList[index] = true;
    this.documentName = this.litigationDocs[index].docName;

  }
  onBlurDocument(index: number) {

    this.litigationDocs[index].docName = this.documentName;
    let updatedDocs = this.form.getRawValue().updatedFiles;
    let docIndex = -1
    if (updatedDocs == null || updatedDocs == undefined) {
      updatedDocs = []
    } else {
      docIndex = this.form.getRawValue().updatedFiles.findIndex(val => val.id == this.litigationDocs[index].id);
    }
    if (docIndex == -1) {
      updatedDocs.push(this.litigationDocs[index]);
    } else {
      updatedDocs[docIndex] = this.litigationDocs[index];
    }

    this.form.patchValue({ updatedFiles: updatedDocs })


    this.editedDocumentList[index] = false;

  }
  deleteDocument(index: number) {
    this.popupService.popupInfo('Êtes-vous sûr(e) de vouloir supprimer la pièce jointe ?', 'Confirmer', null, 'Annuler', ConfirmationModalComponent);
    const subscription = this.popupService.onConfirm.subscribe((t) => {

      if (t) {
        let deletedDocs: any[] = this.form.getRawValue().deletedDocs;
        if (deletedDocs == null || deletedDocs == undefined) {
          deletedDocs = []
        }

        let updatedDocs = this.form.getRawValue().updatedFiles;
        if(updatedDocs){
          let docIndex = this.form.getRawValue().updatedFiles.findIndex(val => val.id == this.litigationDocs[index].id);

          if(docIndex > -1){
            updatedDocs.splice(docIndex,1);
          }

        }


        deletedDocs.push(this.litigationDocs[index]);
        this.litigationDocs.splice(index, 1);
        this.editedDocumentList.splice(index, 1);

        this.form.patchValue({ updatedFiles: updatedDocs ,deletedDocs:deletedDocs})


      }
      subscription.unsubscribe();
    })
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => {
      if (!s.closed) {
        s.unsubscribe();
      }
    });

  }

}
