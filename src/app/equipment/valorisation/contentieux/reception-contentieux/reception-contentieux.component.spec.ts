
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';
import { CatActivitySectorModel } from '../../../../shared/models/cat-activity-sector.model';
import { CatAssetTypeModel } from '../../../../shared/models/cat-asset-type.model';
import { CatBrandModel } from '../../../../shared/models/cat-brand.model';
import { CatNatureModel } from '../../../../shared/models/cat-nature.model';
import { EntrepriseInfoModel } from '../../../../shared/models/entreprise-info.model';
import { LitigationDetail } from '../../../../shared/models/litigation-detail.model';
import { LitigationModel } from '../../../../shared/models/litigation.model';
import { ValuationRequestModel } from '../../../../shared/models/valuation-request.model';
import { ValuationModel } from '../../../../shared/models/valuation.model';
import { ReceptionContentieuxComponent } from './reception-contentieux.component';



describe('ReceptionContentieuxComponent', () => {
  let fixture: ReceptionContentieuxComponent;
  let formBuilderMock: FormBuilder;
  let valuationRequestServiceMock: any;
  let popupServiceMock: any;
  let litigationServiceMock: any;
  let routeMock: any;
  let routerMock: any;

  let materialQualification: any;
  let valuation: ValuationModel;
  let valuationRequestDocSeviceMock: any;
  let entrepriseInfoServiceMock: any;
  let latigation: any;
  let dataSharingMock: any;

  beforeEach(() => {
    latigation = {
      "valuation": {} as ValuationModel,
      "id": "1",
      "catAssetType": {
        "id": "1"
      } as CatAssetTypeModel,
      "catBrand": {
        "id": "1"
      } as CatBrandModel,
      "catNature": {
        "id": "1"
      } as CatNatureModel,

      "catActivitySector": {
        "id": "1"
      } as CatActivitySectorModel,
      "litigationDetails": [
        {
          "id": "1",
          "discountRateYear": 1,
          "discountRateValue": 1,
          "isChecked": true
        } as LitigationDetail,
        {
          "id": "2",
          "discountRateYear": 2,
          "discountRateValue": 2,
          "isChecked": false
        } as LitigationDetail,
        {
          "id": "3",
          "discountRateYear": 3,
          "discountRateValue": 3,
          "isChecked": false
        } as LitigationDetail,
        {
          "id": "4",
          "discountRateYear": 4,
          "discountRateValue": 4,
          "isChecked": false
        } as LitigationDetail,
        {
          "id": "5",
          "discountRateYear": 5,
          "discountRateValue": 5,
          "isChecked": false
        } as LitigationDetail,
        {
          "id": "5",
          "discountRateYear": 5,
          "discountRateValue": 5,
          "isChecked": false
        } as LitigationDetail,
        {
          "id": "6",
          "discountRateYear": 6,
          "discountRateValue": 6,
          "isChecked": false
        } as LitigationDetail,
        {
          "id": "7",
          "discountRateYear": 7,
          "discountRateValue": 7,
          "isChecked": false
        } as LitigationDetail,
        {
          "id": "8",
          "discountRateYear": 8,
          "discountRateValue": 8,
          "isChecked": false
        } as LitigationDetail,
        {
          "id": "9",
          "discountRateYear": 9,
          "discountRateValue": 9,
          "isChecked": false
        } as LitigationDetail,
        {
          "id": "10",
          "discountRateYear": 10,
          "discountRateValue": 10,
          "isChecked": false
        } as LitigationDetail,
        {
          "id": "11",
          "discountRateYear": 11,
          "discountRateValue": 11,
          "isChecked": false
        } as LitigationDetail
      ]

    } as LitigationModel
    routeMock = {
      snapshot: {
        paramMap: {
          get: (key: string) => {
            switch (key) {
              case 'id': return '1';
            }
          },
        },
      }

    };
    routerMock = {
      navigate: jest.fn().mockReturnValue(new Promise(() => {
        //this is intentional
      })),
    }

    valuationRequestServiceMock = {

      getValuationRequestIdByValuationId: jest.fn().mockReturnValue(of(('5'))),

    };
    popupServiceMock = {

      popupInfo: jest.fn().mockReturnValue(of(true)),
      onConfirm: of(true)
    };
    litigationServiceMock = {


      findLitigationByValuationId: jest.fn().mockReturnValue(of(latigation)),
      saveLitigation: jest.fn().mockReturnValue(of(latigation))

    };
    dataSharingMock = {
      changeStatus: jest.fn().mockReturnValue(of(true)),
    }


    valuation = {
      "id": "1",
      "discountRateComment": "comment",
      "experId": "1",
      "controllerId": "1",
      "validatorId": "1",
      "paramValidationType": null,
      "valuationRequest": {
        "id": "3",
        "isANewAsset": true,
        

      } as ValuationRequestModel,
      "isDuplicated": null,
      "isSubmitted" :null,
      "createdDate": null,
      "descNetwork": null,
      "descName": null,
      "catAssetType": {
        "id": "1"
      } as CatAssetTypeModel,
      "catBrand": {
        "id": "1"
      } as CatBrandModel,
      "catNature": {
        "id": "1"
      } as CatNatureModel,

      "catActivitySector": {
        "id": "1"
      } as CatActivitySectorModel
    } as ValuationModel

    materialQualification = {
      "createdDate": null,
      "descNetwork": null,
      "descName": null,
      "catAssetType": {
        "id": "1"
      } as CatAssetTypeModel,
      "catBrand": {
        "id": "1"
      } as CatBrandModel,
      "catNature": {
        "id": "1"
      } as CatNatureModel,

      "catActivitySector": {
        "id": "1"
      } as CatActivitySectorModel
    }





    entrepriseInfoServiceMock = {
      entrepriseInfo: {cp:'11'} as EntrepriseInfoModel
    }

    valuationRequestDocSeviceMock = {
      uploadRequestDocument: jest.fn().mockReturnValue(of(true)),
      updateBssRespDocuments: jest.fn().mockReturnValue(of(true)),
      deleteDocuments: jest.fn().mockReturnValue(of(true)),
      uploadLitigationtDocument : jest.fn().mockReturnValue(of(true)),

    };


    formBuilderMock = new FormBuilder();


    fixture = new ReceptionContentieuxComponent(
      routeMock,
      formBuilderMock,
      valuationRequestServiceMock,
      entrepriseInfoServiceMock,
      valuationRequestDocSeviceMock,
      popupServiceMock,
      litigationServiceMock,
      routerMock,
      dataSharingMock);
    spyOn(window, "scrollTo").and.returnValue(true);
    fixture.ngOnInit();
  });

  describe('Test Component', () => {
    it('should create', () => {
      expect(fixture).toBeTruthy();
    });
  });

  describe('Test ngOninit', () => {
    it('should get valuationRequestId ', () => {
      expect(fixture.valuationRequestId).toEqual('5');
    });
    it('should set  valuationRequestIdLoaded true ', () => {
      expect(fixture.valuationRequestIdLoaded).toEqual(true);
    });
  });

  describe('Test get f', () => {
    it('should return contentieuxForm controls', () => {
      expect(fixture.f).toEqual(fixture.contentieuxForm.controls);
    });
  });

  describe('Test getValuationReception', () => {
    it('should get valuationExpert', () => {
      let valation = {
        "id": "1",
        "discountRateComment": null,
        "experId": null,
        "controllerId": null,
        "validatorId": null,
        "catNature": null,
        "paramValidationType": null,
        "catBrand": null,
        "catAssetType": null,
        "valuationRequest": {
          "id": "5"
        } as ValuationRequestModel,
        "catActivitySector": null,

      } as ValuationModel
      fixture.getValuationReception(valation)
      expect(fixture.valuationExpert).toEqual(valation);
      expect(fixture.valuationRequest).toEqual(valation.valuationRequest);
      expect(fixture.valuatuationExpertLoaded).toEqual(true);

    });
  });

  describe('Test changeClientInfoLoaded  ', () => {
    it('should set clientInfoLoaded  true', () => {
      fixture.clientInfoLoaded = false;
      fixture.changeClientInfoLoaded();
      expect(fixture.clientInfoLoaded).toEqual(true);
    });
  });

  describe('Test materialAgeCalucul  ', () => {
    it('should be null', () => {

      fixture.materialAgeCalucul(null);
      expect(fixture.materialAge).toBeNull();
    });

    it('should get current year', () => {


      expect(fixture.getCurrentYear()).toEqual((new Date()).getFullYear());
    });

    it('should get current month', () => {


      expect(fixture.getCurrentMonth()).toEqual((new Date()).getMonth());
    });

    it('should get current day', () => {


      expect(fixture.getCurrentDay()).toEqual(+String((new Date()).getDate()).padStart(2, '0'));
    });

    it('should be 8', () => {

      fixture.valuationRequest = { isANewAsset: true } as ValuationRequestModel;

      spyOn(fixture, 'getCurrentYear').and.returnValue(2022)
      spyOn(fixture, 'getCurrentMonth').and.returnValue(6)
      spyOn(fixture, 'getCurrentDay').and.returnValue(17)

      fixture.materialAgeCalucul('01/05/2015');
      expect(fixture.materialAge).toEqual(8);
    });

    it('should be 7', () => {

      fixture.valuationRequest = { isANewAsset: true } as ValuationRequestModel;

      spyOn(fixture, 'getCurrentYear').and.returnValue(2022)
      spyOn(fixture, 'getCurrentMonth').and.returnValue(6)
      spyOn(fixture, 'getCurrentDay').and.returnValue(17)

      fixture.materialAgeCalucul('01/08/2015');
      expect(fixture.materialAge).toEqual(7);
    });

    it('should be 8 same month', () => {

      fixture.valuationRequest = { isANewAsset: true } as ValuationRequestModel;

      spyOn(fixture, 'getCurrentYear').and.returnValue(2022)
      spyOn(fixture, 'getCurrentMonth').and.returnValue(5)
      spyOn(fixture, 'getCurrentDay').and.returnValue(17)

      fixture.materialAgeCalucul('12/06/2015');
      expect(fixture.materialAge).toEqual(8);
    });

    it('should be 7 same month', () => {

      fixture.valuationRequest = { isANewAsset: true } as ValuationRequestModel;

      spyOn(fixture, 'getCurrentYear').and.returnValue(2022)
      spyOn(fixture, 'getCurrentMonth').and.returnValue(5)
      spyOn(fixture, 'getCurrentDay').and.returnValue(17)

      fixture.materialAgeCalucul('18/06/2015');
      expect(fixture.materialAge).toEqual(7);
    });


    it('should be 5', () => {

      fixture.valuationRequest = { isANewAsset: false, collAssetEstimYear: 2017 } as ValuationRequestModel;

      spyOn(fixture, 'getCurrentYear').and.returnValue(2022)
      spyOn(fixture, 'getCurrentMonth').and.returnValue(6)
      spyOn(fixture, 'getCurrentDay').and.returnValue(17)

      fixture.materialAgeCalucul('01/05/2015');

      expect(fixture.materialAge).toEqual(5);
    });
  });

  describe('Test isFormQualif  ', () => {
    it('should set isFormQualif  true', () => {
      fixture.contentieuxForm.patchValue({
        contentieuxInformation: { contractKSIOP: '', fixedAsset: '', plateNumber: '', invoiceDate: '01/05/2020' },
        materialQualification: { catActivitySector: { id: '1' }, catNature: { id: '1' }, catBrand: { id: '1' }, catAssetType: { id: '1' } },
        decoteContentieux: { materialAge: 3, materialCurrentValue: 5000, litigationDiscount: 20, litigationValue: 4000 },

      })
      fixture.valuationRequest = { isANewAsset: true } as ValuationRequestModel
      fixture.isFormQualif(true);
      expect(fixture.isFormQualifDisabled).toEqual(true);
    });

    it('should set isFormQualif  false', () => {
      fixture.isFormQualifDisabled = true;
      fixture.isFormQualif(false);
      expect(fixture.isFormQualifDisabled).toEqual(false);
    });
  });

  describe('Test getDecoteContentieuxValue  ', () => {

    it('should map decote contentieux', () => {
      let liti =
      {
        "id": "1",
        "catAssetType": {
          "id": "1"
        } as CatAssetTypeModel,
        "catBrand": {
          "id": "1"
        } as CatBrandModel,
        "catNature": {
          "id": "1"
        } as CatNatureModel,

        "catActivitySector": {
          "id": "1"
        } as CatActivitySectorModel,
        "litigationDetails": [
          {
            "id": "1",
            "discountRateYear": 1,
            "discountRateValue": 1,
            "isChecked": true
          } as LitigationDetail,],
        "valuation": valuation,
        "contractKSIOP": "string",

        "fixedAsset": "string",

        "plateNumber": "string",

        "invoiceDate": null,

        "isLostMaterial": true,

        "materialAge": 2,

        "materialCurrentValue": 2,

        "litigationDiscount": 2,

        "litigationValue": 2,

        "isCobailing": true,

        "shareRate": 2,

        "shareValue": 2,

        "comment": "string",

      } as LitigationModel;
      fixture.litigation = liti;
      const value = {
        materialAge: '15',
        materialCurrentValue: 'ddd',
        litigationDiscount: 'ddd',
        litigationValue: 'ddd',
        isCobailing: 'ddd',
        comment: 'ddd',
        isLostMaterial: 'ddd',
        shareRate: 'ddd',
        shareValue: '01/01/2001',
        decoteRates: null
      };
      fixture.getDecoteContentieuxValue(value);
      expect(fixture.litigation.comment).toEqual('ddd');
    });
  });

  describe('Test getMaterialQualificationValue  ', () => {
    it('should set getMaterialQualificationValue  ', () => {
      fixture.litigation = {} as LitigationModel
      fixture.contentieuxForm.patchValue({
        materialQualification: { catActivitySector: { id: '1' }, catNature: { id: '1' }, catBrand: { id: '1' }, catAssetType: { id: '1' } },

      })
      fixture.getMaterialQualificationValue(materialQualification);
      expect(fixture.litigation.catActivitySector.id).toEqual('1');
      expect(fixture.litigation.catBrand.id).toEqual('1');
      expect(fixture.litigation.catAssetType.id).toEqual('1');
      expect(fixture.litigation.catNature.id).toEqual('1');
    });


  });

  describe('Test getFormValue  ', () => {
    it('should get getFormValue', () => {
      fixture.litigation = {} as LitigationModel
      fixture.contentieuxForm.patchValue({
        contentieuxInformation: { contractKSIOP: '', fixedAsset: '', plateNumber: '', invoiceDate: '01/05/2020' },
        materialQualification: { catActivitySector: { id: '1' }, catNature: { id: '1' }, catBrand: { id: '1' }, catAssetType: { id: '1' } },
        decoteContentieux: { materialAge: 3, materialCurrentValue: 5000, litigationDiscount: 20, litigationValue: 4000 },

      })

      fixture.getFormValue();
      expect(fixture.isFormQualifDisabled).toEqual(true);
    });
  });

  describe('Test getInformationContentieuxValue  ', () => {
    it('should get getInformationContentieuxValue', () => {
      const value = {
        contractKSIOP: 'ddd',
        fixedAsset: 'ddd',
        plateNumber: 'ddd',
        invoiceDate: '01/01/2001',
        files: null
      };
      fixture.litigation = {} as LitigationModel
      fixture.getInformationContentieuxValue(value);
      expect(fixture.litigation.contractKSIOP).toEqual('ddd');
    });
  });



  describe('Test materialAgeChange  ', () => {
    it('should update materialAge', () => {
      fixture.valuationRequest = { isANewAsset: true } as ValuationRequestModel;

      fixture.materialAgeChange('01/05/2015');
      expect(fixture.isDateFromAgeChanged).toEqual(true);

    });
  });

  describe('Test saveLitigation  ', () => {
    it('should save and switch page', () => {
      fixture.contentieuxForm.get('contentieuxInformation').patchValue({ files: '[]' });
      fixture.contentieuxForm.get('contentieuxInformation').patchValue({ updatedFiles: '[]' });
      fixture.contentieuxForm.get('contentieuxInformation').patchValue({ deletedDocs: '[]' });



      spyOn(fixture, 'getFormValue').and.returnValue(null);

      fixture.saveLitigation();
      expect(routerMock.navigate).toHaveBeenCalled();
    });
  });

  describe('Test onSubmit  ', () => {
    it('should call popup when form is not valid', () => {
      fixture.contentieuxForm.controls['decoteContentieux'].setErrors({'incorrect': true});

      fixture.onSubmit();

      expect(popupServiceMock.popupInfo).toHaveBeenCalled();

    });
    it('should call popup when form is valid and comment not exist', () => {
      fixture.contentieuxForm.get('decoteContentieux').patchValue({ comment: null });

      fixture.onSubmit();

      expect(popupServiceMock.popupInfo).toHaveBeenCalled();

    });

    it('should not call popup when form is valid and comment  exist', () => {
      fixture.contentieuxForm.get('decoteContentieux').patchValue({ comment: 'commentaire' });
      spyOn(fixture, 'getFormValue').and.returnValue(null);

      fixture.onSubmit();

      expect(popupServiceMock.popupInfo).toHaveBeenCalledTimes(0);

    });
  });


  describe('Test upload and update docs  ', () => {
    it('should upload docs', () => {


      fixture.uploadDocuments([{ name :'test.png',size:115} as File])
      expect(valuationRequestDocSeviceMock.uploadLitigationtDocument).toHaveBeenCalled();
    });

    it('should update docs', () => {


      fixture.updateDocuments([{id:'15',docName:'changed.pdf'}])
      expect(valuationRequestDocSeviceMock.updateBssRespDocuments).toHaveBeenCalled();
    });
  });

  describe('Test delete docs  ', () => {
    it('should upload docs', () => {


      fixture.deleteDocuments([{id:'15',docName:'changed.pdf'}])
      expect(valuationRequestDocSeviceMock.deleteDocuments).toHaveBeenCalled();
    });

  
  });

  



});
