import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { forkJoin, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ConfirmationModalComponent } from '../../../../shared/confirmation-modal/confirmation-modal.component';
import { LitigationModel } from '../../../../shared/models/litigation.model';
import { ValuationRequestModel } from '../../../../shared/models/valuation-request.model';
import { ValuationModel } from '../../../../shared/models/valuation.model';
import { EntrepriseInfoService } from '../../../../shared/services/entreprise-info.service';
import { LitigationService } from '../../../../shared/services/litigation-service';
import { PopupService } from '../../../../shared/services/popup.service';
import { ValuationRequestDocsService } from '../../../../shared/services/valuation-request-docs.service';
import { ValuationRequestService } from '../../../../shared/services/valuation.request.service';
import { formatStringtoDate } from '../../../../shared/utils';
import { SharedCommunicationDataService } from '../../../../shared/services/shared-communication-data.service';

@Component({
  selector: 'app-reception-contentieux',
  templateUrl: './reception-contentieux.component.html',
  styleUrls: ['./reception-contentieux.component.css']
})
export class ReceptionContentieuxComponent implements OnInit {


  constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private valuationRequestService: ValuationRequestService,
    private entrepriseInfoService: EntrepriseInfoService,
    private valuationRequestDocSevice:ValuationRequestDocsService,
    private popupService: PopupService, private litigationService: LitigationService,
    private router: Router,
    private dataSharing: SharedCommunicationDataService,

  ) { }
  valuationExpertId;
  valuationRequestId;
  valuationRequestIdLoaded = false;
  valuatuationExpertLoaded = false;
  isFormQualifDisabled = true;
  valuationExpert: ValuationModel;
  valuationRequest: ValuationRequestModel;
  clientInfoLoaded: boolean = false;
  contentieuxForm: FormGroup;
  materialAge: number = null;
  litigation: LitigationModel;
  isSubmitted = false;
  isDateFromAgeChanged = false;
  get f() {
    return this.contentieuxForm.controls;
  }
  ngOnInit(): void {
    window.scrollTo(0, 0);
    this.dataSharing.changeStatus("true");

    this.clientInfoLoaded = true;

    this.valuationExpertId = this.route.snapshot.paramMap.get('id');

    this.valuationRequestService.getValuationRequestIdByValuationId(this.valuationExpertId).subscribe(data => {
      this.valuationRequestId = data;
    }, (err) => {
      console.error(err);
    }, () => {
      this.contentieuxForm = this.fb.group({
        clientInfo: [],
        valuation: [],
        materialQualification: [],
        decoteContentieux: [],
        contentieuxInformation: []


      });
      this.contentieuxForm.controls['valuation'].disable();
      this.contentieuxForm.controls['materialQualification'].disable();
      this.valuationRequestIdLoaded = true
    })


  }


  getValuationReception(valuation: any) {
    this.valuationExpert = valuation;
    this.valuationRequest = this.valuationExpert.valuationRequest;
    this.litigationService.findLitigationByValuationId(this.valuationExpert.id).subscribe(data => {
      this.litigation = data;
    }, () => {
    }, () => {
      this.valuatuationExpertLoaded = true;

      if (this.litigation) {


        this.contentieuxForm.get('contentieuxInformation').patchValue({

          contractKSIOP: this.litigation ? this.litigation.contractKSIOP : null,
          fixedAsset: this.litigation ? this.litigation.fixedAsset : null,
          plateNumber: this.valuationRequest.isLicencePlateAvaileble && this.litigation ? this.litigation.plateNumber : null,
          invoiceDate: this.litigation ? this.getStringDate(this.litigation.invoiceDate) : null,
          files: [],
          updatedFiles:[],
          deletedDocs:[]

        })
      }

    })
  }

  getStringDate(date: Date) {

    const datePipe = new DatePipe("en-US");


    return datePipe.transform(date, "dd/MM/YYYY");



  }

  getCurrentYear() {
    return (new Date()).getFullYear();
  }
  getCurrentMonth() {

    return (new Date()).getMonth();

  }

  getCurrentDay() {

    return +String((new Date()).getDate()).padStart(2, '0');
    ;

  }

  materialAgeChange(billDate: string) {
    this.materialAgeCalucul(billDate);
    this.isDateFromAgeChanged = true;
  }

  materialAgeCalucul(billDate: string) {

    if (billDate) {

      let date = formatStringtoDate(billDate);

      if (this.valuationRequest.isANewAsset) {
        this.materialAge = this.getCurrentYear() - date.getFullYear();



        if (date.getMonth() < this.getCurrentMonth()) {
          this.materialAge += 1;

        }

        if ((date.getMonth() == this.getCurrentMonth()) && (+String(date.getDate()).padStart(2, '0') < this.getCurrentDay())) {


          this.materialAge += 1;
        }




      } else {
        this.materialAge = this.getCurrentYear() - this.valuationRequest.collAssetEstimYear;
      }

    } else {
      this.materialAge = null;
    }


  }
  changeClientInfoLoaded() {
    this.clientInfoLoaded = true;
  }

  isFormQualif(valueEmited) {
    if (valueEmited == false && this.isFormQualifDisabled) {
      this.contentieuxForm.controls.decoteContentieux.reset();
    }
    else {
      if (this.contentieuxForm.getRawValue().contentieuxInformation.invoiceDate) {
        this.materialAgeCalucul(this.contentieuxForm.getRawValue().contentieuxInformation.invoiceDate)

      }
    }
    this.isFormQualifDisabled = valueEmited;
  }


  onSubmit() {
    if (this.contentieuxForm.valid) {

    if (!this.contentieuxForm.getRawValue().decoteContentieux.comment) {
      this.popupService.popupInfo("Vous vous apprêtez à soumettre une valorisation en contentieux sans commentaire, voulez-vous continuer ?",
        'OUI', null, 'NON', ConfirmationModalComponent, true);
      const subscription = this.popupService.onConfirm.subscribe((t) => {
        if (t) {
          this.saveLitigation();
        }
        subscription.unsubscribe();
      });
    }
    else {
      this.saveLitigation();
    }
  }
  else {
    this.popupService.popupInfo(
      "Merci de compléter les informations demandées.", null, null, null, ConfirmationModalComponent);
    const subscriptionPopup = this.popupService.onConfirm.subscribe((t) => {

      subscriptionPopup.unsubscribe();

    });
  }
  }

  saveLitigation() {
    this.isSubmitted = true; 
      this.getFormValue();
      this.litigationService.saveLitigation(this.litigation).subscribe(data => {
        this.litigation = data;
        if(this.contentieuxForm.getRawValue().contentieuxInformation.files && this.contentieuxForm.getRawValue().contentieuxInformation.files.files && this.contentieuxForm.getRawValue().contentieuxInformation.files.files.length>0){
          this.uploadDocuments(this.contentieuxForm.getRawValue().contentieuxInformation.files.files);

        }
        if(this.contentieuxForm.getRawValue().contentieuxInformation.updatedFiles && this.contentieuxForm.getRawValue().contentieuxInformation.updatedFiles.length>0){
          this.updateDocuments(this.contentieuxForm.getRawValue().contentieuxInformation.updatedFiles)

        }
        if(this.contentieuxForm.getRawValue().contentieuxInformation.deletedDocs && this.contentieuxForm.getRawValue().contentieuxInformation.deletedDocs.length>0){

          this.deleteDocuments(this.contentieuxForm.getRawValue().contentieuxInformation.deletedDocs)

        }

      }, () => {

      }, () => {
      this.router.navigate(['result/contentieux', this.litigation.id])

      })

    }

  

  getFormValue() {
    this.litigation.valuation = this.valuationExpert;
    this.getInformationContentieuxValue(this.contentieuxForm.getRawValue().contentieuxInformation);
    this.getMaterialQualificationValue(this.contentieuxForm.getRawValue().materialQualification);
    this.getDecoteContentieuxValue(this.contentieuxForm.getRawValue().decoteContentieux)
  }
  getInformationContentieuxValue(litigationInfoValue: any) {

    this.litigation.contractKSIOP = litigationInfoValue.contractKSIOP;
    this.litigation.fixedAsset = litigationInfoValue.fixedAsset;
    this.litigation.plateNumber = litigationInfoValue.plateNumber;
    this.litigation.invoiceDateLabel = litigationInfoValue.invoiceDate;
  }

  getMaterialQualificationValue(materialQualificationValue: any) {
    this.litigation.catActivitySector = materialQualificationValue.catActivitySector;
    this.litigation.catNature = materialQualificationValue.catNature;
    this.litigation.catBrand = materialQualificationValue.catBrand;
    this.litigation.catAssetType = materialQualificationValue.catAssetType;

  }

  getDecoteContentieuxValue(decoteContentieux: any) {
    this.litigation.materialAge = decoteContentieux.materialAge
    this.litigation.materialCurrentValue = decoteContentieux.materialCurrentValue
    this.litigation.litigationDiscount = decoteContentieux.litigationDiscount;
    this.litigation.litigationValue = decoteContentieux.litigationValue;
    this.litigation.isCobailing = (decoteContentieux.isCobailing) ? true : false;
    this.litigation.comment = decoteContentieux.comment;
    this.litigation.isLostMaterial = (decoteContentieux.isLostMaterial) ? true : false;
    this.litigation.shareRate = decoteContentieux.shareRate;
    this.litigation.shareValue = decoteContentieux.shareValue;
    this.litigation.litigationDetails = decoteContentieux.decoteRates;
  }


  uploadDocuments(files :File[]){
    let entrepriseInfo = this.entrepriseInfoService.entrepriseInfo;

    let requests =[];

    files.forEach(file=>{

      requests.push(this.valuationRequestDocSevice.uploadLitigationtDocument(file,this.valuationRequest,entrepriseInfo).pipe(catchError(error => of(null))))

    })

    forkJoin(requests).subscribe()

  }

  updateDocuments(files :any[]){
    
    this.valuationRequestDocSevice.updateBssRespDocuments(files).subscribe()

  }

  deleteDocuments(files:any[]){
    this.valuationRequestDocSevice.deleteDocuments(files).subscribe();
  }
}
