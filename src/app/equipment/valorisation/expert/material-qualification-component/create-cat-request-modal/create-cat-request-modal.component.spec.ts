import {FormBuilder} from '@angular/forms';
import {UrlTree} from '@angular/router';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {of} from 'rxjs';
import {CatActivitySectorModel} from '../../../../../shared/models/cat-activity-sector.model';
import {CatBrandModel} from '../../../../../shared/models/cat-brand.model';
import {CatNatureModel} from '../../../../../shared/models/cat-nature.model';
import {CatRequestModel} from '../../../../../shared/models/cat-request.model';
import {CatRequestDoc} from '../../../../../shared/models/catalog-request-doc.model';
import {ParamSettingModel} from '../../../../../shared/models/param-setting.model';
import {CreateCatRequestModalComponent} from './create-cat-request-modal.component';
import {TestBed} from '@angular/core/testing';
import {ValuationSharedService} from '../../../../../shared/services/valuation-shared-service';


describe('SummaryModalComponent', () => {
  let fixture: CreateCatRequestModalComponent;
  let catActivitySectorServiceMock;
  let catNatureServiceMock;
  let catBrandServiceMock;
  let formBuilderMock;
  let catalogAddRequestServiceMock;
  let paramSettingServiceMock;
  let catalogAddRequestWorkflowServiceMock;
  let activeModalMock;
  let popupServiceMock;
  let catSectorList: CatActivitySectorModel[];
  let catBrandList: CatBrandModel[];
  let paramSettingList: ParamSettingModel[];
  let catNatureList: CatNatureModel[];

  let mdmInformationServiceMock;
  let authServiceMock;
  let routerMock;
  let valuationSharedServiceMock: any;

  beforeEach(() => {
    valuationSharedServiceMock = TestBed.inject(ValuationSharedService);


    paramSettingList = [{
      'id': '5',
      'code': 'STATUS_VAL_WKFL',
      'label': 'Validée',
      'value': 'STATUS_RQST_WKFL_VAL',
      'effectiveDate': null,
      'isActiv': true,
    } as ParamSettingModel]
    formBuilderMock = new FormBuilder();

    catBrandList = [{

      id: '5',
      label: 'QSD',
      catNature: {
        id: '5',
        label: 'nature label',
        catActivitySector: {
          id: '5',
          label: 'Transport',
          comment: 'comment act sector',
          isActive: true

        },
        comment: 'comment nature',
        paramNap: null,
        isActive: true

      },
      comment: 'commentaire brand',
      isActive: true
    } as CatBrandModel];
    catSectorList = [{id: '1', label: 'setc label'}]

    catActivitySectorServiceMock = {
      getActivitySectorList: jest.fn().mockReturnValue(of(catSectorList)),
    }
    catBrandServiceMock = {
      getCatBrandListByCatNatureId: jest.fn().mockReturnValue(of(catBrandList)),
    }

    catalogAddRequestServiceMock = {
      saveCatRequest: jest.fn().mockReturnValue(of({
        id: '1'
      } as CatRequestModel)),

      saveCatRequestDocument: jest.fn().mockReturnValue(of({
        id: '1'
      } as CatRequestDoc)),
    }
    paramSettingServiceMock = {
      getParamByCode: jest.fn().mockReturnValue(of(paramSettingList)),

    }
    catalogAddRequestWorkflowServiceMock = {
      saveCatRequestWorkflow: jest.fn().mockReturnValue(of(true)),
    }
    activeModalMock = new NgbActiveModal();
    popupServiceMock = {
      popupInfo: jest.fn().mockReturnValue(of(true)),
      onConfirm: of(true)
    };
    catNatureServiceMock = {
      getNatureListByCatActivitySectorID: jest.fn().mockReturnValue(of(catNatureList))

    }
    catNatureList = [{
      id: '133',
      label: 'nature label',
      catActivitySector: {
        id: '133',
        label: 'nature label',

      },
      comment: 'comment nature',
      paramNap: null,
      isActive: true,
      isValid: false,
      isEnrg: false
    }
    ]
    authServiceMock = {
      identityClaims: {
        'mat': 'M102'
      },

      currentUserRolesList: ['ROLE_SVS_ADMIN_SE']

    }
    mdmInformationServiceMock = {
      getMdmCaInformation: jest.fn().mockReturnValue(of({
        desc: {
          id: '52'
        }
      })),
    }

    routerMock = {
      navigate: jest.fn(),
      url: '/search',
      serializeUrl: jest.fn().mockReturnValue('url/url'),
      createUrlTree: jest.fn().mockReturnValue(new UrlTree())
    };
    fixture = new CreateCatRequestModalComponent(catActivitySectorServiceMock, catNatureServiceMock, catBrandServiceMock, formBuilderMock,
      catalogAddRequestServiceMock, paramSettingServiceMock,
      catalogAddRequestWorkflowServiceMock, activeModalMock, popupServiceMock, authServiceMock, mdmInformationServiceMock, routerMock, valuationSharedServiceMock)
    fixture.catActivitySector = catSectorList[0]
    fixture.catNature = catNatureList[0];
    fixture.catBrand = catBrandList[0];
    fixture.ngOnInit();


  });

  describe('Test: ngOnInit', () => {
    it('should initialize form', () => {

      const form = {
        label: null,
        catActivitySector: catSectorList[0],
        catNature: catNatureList[0],
        catBrand: catBrandList[0],
        comment: null,
        files: null

      };
      fixture.ngOnInit();

      expect(fixture.catRequestForm.getRawValue()).toEqual(form);

    });

    it('should fill list', () => {

      expect(fixture.sectorList).toEqual(
        catSectorList);
      expect(fixture.natureList).toEqual(
        catNatureList);
      expect(fixture.brandList).toEqual(
        catBrandList);


    });

  });


  describe('Test: submit', () => {
    it('should not submit when form not valid', () => {


      fixture.submit();

      expect(fixture.isSubmitted).toEqual(true);
      expect(catalogAddRequestServiceMock.saveCatRequest).toHaveBeenCalledTimes(0)

    });
    it('should submit when form is valid', () => {


      fixture.catRequestForm.patchValue({
        label: "label",
        comment: "commentaire",
        files: {files: [{name: 'file3.png', size: 17567} as File]}
      });
      fixture.submit();
      expect(fixture.catRequest.id).toEqual("1");
      expect(catalogAddRequestWorkflowServiceMock.saveCatRequestWorkflow).not.toHaveBeenCalled();
      expect(fixture.isSubmitted).toEqual(false);
      expect(popupServiceMock.popupInfo).toHaveBeenCalled();

    });
    it('should submit when form is valid', () => {


      fixture.catRequestForm.patchValue({
        label: "label",
        comment: "commentaire",
        files: {files: [{name: 'file3.png', size: 17567} as File]}
      });
      fixture.buisinessKey = 'ca';
      fixture.workflowCode = 'ca';
      fixture.submit();
      expect(fixture.catRequest.id).toEqual("1");
      expect(fixture.buisinessKey).not.toBeNull();
      expect(catalogAddRequestWorkflowServiceMock.saveCatRequestWorkflow).toHaveBeenCalled();

      expect(fixture.isSubmitted).toEqual(false);
      expect(popupServiceMock.popupInfo).toHaveBeenCalled();
    });

    it('should   submit when form is valid', () => {
      popupServiceMock = {
        popupInfo: jest.fn().mockReturnValue(of(true)),
        onConfirm : of(false)
      };
      // tslint:disable-next-line:max-line-length
      fixture = new CreateCatRequestModalComponent(catActivitySectorServiceMock, catNatureServiceMock, catBrandServiceMock, formBuilderMock, catalogAddRequestServiceMock, paramSettingServiceMock, catalogAddRequestWorkflowServiceMock, activeModalMock, popupServiceMock, authServiceMock, mdmInformationServiceMock, routerMock, valuationSharedServiceMock);
      fixture.catActivitySector = catSectorList[0]
      fixture.catNature = catNatureList[0];
      fixture.catBrand = catBrandList[0];
      fixture.ngOnInit();
      fixture.catRequestForm.patchValue({
        label: 'label',
        comment: 'commentaire',
        files: {files: [{name: 'file3.png', size: 17567} as File]}
      });
      const spyInstance = jest.spyOn(fixture.activeModal, 'dismiss');
      fixture.submit();
      expect(spyInstance).toHaveBeenCalled();
    });
  });

});

