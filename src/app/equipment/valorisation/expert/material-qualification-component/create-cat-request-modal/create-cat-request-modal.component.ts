import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {forkJoin} from 'rxjs';
import {ConfirmationModalComponent} from '../../../../../shared/confirmation-modal/confirmation-modal.component';
import {CatActivitySectorModel} from '../../../../../shared/models/cat-activity-sector.model';
import {CatBrandModel} from '../../../../../shared/models/cat-brand.model';
import {CatNatureModel} from '../../../../../shared/models/cat-nature.model';
import {CatRequestModel} from '../../../../../shared/models/cat-request.model';
import {CatRequestWorkFlow} from '../../../../../shared/models/catalog-add-request-workflow.model';
import {MdmInformationsModel} from '../../../../../shared/models/mdm-informations.model';
import {ParamSettingModel} from '../../../../../shared/models/param-setting.model';
import {WorkflowInstanceVariable} from '../../../../../shared/models/workflow-instance-variable.model';
import {AuthService} from '../../../../../shared/services/auth.service';
import {CatActivitySectorService} from '../../../../../shared/services/cat-activity-sector.service';
import {CatBrandService} from '../../../../../shared/services/cat-brand.service';
import {CatNatureService} from '../../../../../shared/services/cat-nature.service';
import {CatalogAddRequestWorkflowService} from '../../../../../shared/services/catalog-add-request-workflow.service';
import {CatalogAddRequestService} from '../../../../../shared/services/catalog-add-request.service';
import {MdmInformationService} from '../../../../../shared/services/mdm-informations.service';
import {ParamSettingService} from '../../../../../shared/services/param-setting.service';
import {PopupService} from '../../../../../shared/services/popup.service';
import {StatusConstant} from '../../../../../shared/status-constant';
import {ValuationSharedService} from '../../../../../shared/services/valuation-shared-service';


@Component({
  selector: 'app-create-cat-request-modal',
  templateUrl: './create-cat-request-modal.component.html',
  styleUrls: ['./create-cat-request-modal.component.css']
})
export class CreateCatRequestModalComponent implements OnInit {

  constructor(
    private catActivitySectorService: CatActivitySectorService, private catNatureService: CatNatureService, private catBrandService: CatBrandService, private fb: FormBuilder, private catalogAddRequestService: CatalogAddRequestService
    , private paramSettingService: ParamSettingService, private catalogAddRequestWorkflowService: CatalogAddRequestWorkflowService, public activeModal: NgbActiveModal, private popupService: PopupService,
    private authService: AuthService, private mdmInformationService: MdmInformationService, public router: Router, private valuationSharedService: ValuationSharedService
  ) {
  }

  sectorList: CatActivitySectorModel[] = [];
  natureList: CatNatureModel[] = [];
  brandList: CatBrandModel[] = []
  catRequestForm: FormGroup;
  catRequest: CatRequestModel;
  catNature: CatNatureModel = null;
  catBrand: CatBrandModel = null;
  catActivitySector: CatActivitySectorModel = null;
  workflowCode: string;
  buisinessKey: string;
  isSubmitted = false;
  valuationRequest: any;
  descInfo: MdmInformationsModel;

  ngOnInit(): void {
    this.catRequestForm = this.fb.group({
      label: [null, Validators.required],
      catActivitySector: [{value: null, disabled: true}, Validators.required],
      catNature: [{value: null, disabled: true}, Validators.required],
      catBrand: [{value: null, disabled: true}],
      comment: [null, Validators.required],
      files: [null]
    });

    forkJoin([this.catActivitySectorService.getActivitySectorList(), this.catNatureService.getNatureListByCatActivitySectorID(this.catActivitySector.id), this.catBrandService.getCatBrandListByCatNatureId(this.catNature.id)]).subscribe(res => {
      this.sectorList = res[0];
      this.natureList = res[1];
      this.brandList = res[2];
    }, (err) => {
      console.error(err)
    }, () => {

      this.catRequestForm.patchValue({
        catActivitySector: this.catActivitySector,
        catNature: this.catNature,
        catBrand: this.catBrand
      })
    });

    this.mdmInformationService.getMdmCaInformation(this.authService.identityClaims['mat']).subscribe((data) => {

      this.descInfo = data


    });


  }

  submit() {
    this.isSubmitted = true;
    this.catRequestForm.markAllAsTouched();
    if (this.catRequestForm.valid) {
      let paramSetting: ParamSettingModel = {} as ParamSettingModel;
      paramSetting.id = '2';

      this.catRequest = this.catRequestForm.getRawValue();
      let catNature = {label: String(this.catRequest.catNature)} as CatNatureModel;
      let catActivitySector = {label: this.catRequest.catActivitySector} as CatActivitySectorModel;
      if (this.catRequest.catBrand) {
        let catBrand = {label: String(this.catRequest.catBrand)} as CatBrandModel;
        this.catRequest.catBrand = catBrand;
      }
      this.catRequest.catActivitySector = catActivitySector;
      this.catRequest.catNature = catNature;
      this.catRequest.requestType = paramSetting;
      this.catRequest.workflowCode = this.workflowCode;
      this.catRequest.buisinessKey = this.buisinessKey;
      if (this.valuationRequest && this.valuationRequest.id) {
        this.catRequest.valuationRequest = this.valuationRequest;

      }
      this.catalogAddRequestService.saveCatRequest(this.catRequest).subscribe(data => {
          this.catRequest = data;


        }, (err) => {
          console.error(err);
        }, () => {

          if (this.catRequestForm.value.files && this.catRequestForm.value.files.files && this.catRequestForm.value.files.files.length > 0) {

            this.catRequestForm.value.files.files.forEach(file => {
              this.catalogAddRequestService.saveCatRequestDocument(file, this.catRequest).subscribe();

            });
          }

          this.paramSettingService.getParamByCode(StatusConstant.STATUS_RQST_WKFL).subscribe(data => {
            let status = data.find(stat => stat.value == StatusConstant.STATUS_RQST_WKFL_ATT);
            var date = new Date();
            date.setDate(date.getDate() + 1);
            let workFlowRequest = {
              catRequest: {id: this.catRequest.id, activeDate: date},
              status: status
            } as CatRequestWorkFlow;
            workFlowRequest.variable = {} as WorkflowInstanceVariable;
            workFlowRequest.variable.expertDesc = this.descInfo;

            workFlowRequest.variable.urlRequest = location.origin + '/#' + this.router.serializeUrl(this.router.createUrlTree(['catalog', 'request', this.catRequest.id]));
            workFlowRequest.variable.urlResponse = location.href;
            workFlowRequest.variable.urlRefused = location.href;

            if (this.buisinessKey && this.workflowCode) {
              workFlowRequest.isSimulated = false;
              this.catalogAddRequestWorkflowService.saveCatRequestWorkflow(workFlowRequest).subscribe();

            } else {
              this.valuationSharedService.setCatRequest(this.catRequest);
              this.valuationSharedService.setWorkFlowRequest(workFlowRequest);
              this.valuationSharedService.addCatRequestSubject.next(true);
            }
            this.isSubmitted = false;

            this.popupService.popupInfo("Votre demande d'ajout au catalogue n° " + this.catRequest.id + " a été adressée à la filière."
              , null, null, null, ConfirmationModalComponent);
            this.popupService.onConfirm.subscribe((t) => {
              if (!t) {
                this.activeModal.dismiss('done');

              }
              //if confirmed
              if (t) {
                this.activeModal.dismiss('done');
              }
            });

          })
        }
      )


    }
  }


}
