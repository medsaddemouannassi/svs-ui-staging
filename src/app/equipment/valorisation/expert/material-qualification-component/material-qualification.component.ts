import { Component, forwardRef, Input, OnChanges, OnDestroy, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { ControlValueAccessor, FormBuilder, FormControl, FormGroup, NG_VALIDATORS, NG_VALUE_ACCESSOR, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { CatAssetTypeModel } from 'src/app/shared/models/cat-asset-type.model';
import { CatBrandModel } from 'src/app/shared/models/cat-brand.model';
import { CatNatureModel } from 'src/app/shared/models/cat-nature.model';
import { ValuationRequestModel } from 'src/app/shared/models/valuation-request.model';
import { CatActivitySectorService } from 'src/app/shared/services/cat-activity-sector.service';
import { CatAssetTypeService } from 'src/app/shared/services/cat-asset-type.service';
import { CatBrandService } from 'src/app/shared/services/cat-brand.service';
import { CatNatureService } from 'src/app/shared/services/cat-nature.service';
import { CreateCatRequestModalComponent } from './create-cat-request-modal/create-cat-request-modal.component';


@Component({
  selector: 'app-material-qualification',
  templateUrl: './material-qualification.component.html',
  styleUrls: ['./material-qualification.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MaterialQualificationComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => MaterialQualificationComponent),
      multi: true
    }
  ]
})
export class MaterialQualificationComponent implements OnInit, ControlValueAccessor, OnDestroy, OnChanges {

  catTypeIsLoading = false;
  catBrndIsLoading = false;

  get value(): any {
    return this.form.value;
  }

  set value(value) {
    this.form.setValue(value);
    this.onChange(value);
    this.onTouched();
  }

  get f(): any {
    return this.form.controls;
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.valide && !changes.valide.firstChange) {
      this.disableForm()
    }
  }

  constructor(private fb: FormBuilder, private catNatureService: CatNatureService, private catBrandService: CatBrandService,
    private catAssetTypeService: CatAssetTypeService, private modalService: NgbModal,
    private catActivitySectorService: CatActivitySectorService

  ) { }

  /*@ViewChild('auto') auto;

  focus(e): void {
    e.stopPropagation();
    this.auto.focus();
  }*/

  customFilter = function (catBrandOptions: any[], query: string): any[] {

    return catBrandOptions;

  };
  form: FormGroup;
  subscriptions: Subscription[] = [];

  @Input() submited = false;
  /*** to remove  */
  @Input() activitySectorOptions;
  @Input() brandOptions: CatBrandModel[];
  @Input() natureOptions: CatNatureModel[];
  @Input() assetTypeOptions: CatAssetTypeModel[];
  /**** */
  catActivitySectorOptions: string[] = [];
  catNatureOptions: string[] = [];
  catAssetTypeOptions: string[] = [];
  catBrandOptions: string[] = [];

  isSelectedCatType = false;
  Typekeyword = 'label';
  isSelectedCatBrand = false;
  brandkeyword = 'label'
  @Input() valide;

  @Input() valuationRequest: ValuationRequestModel;

  ngOnInit(): void {

    this.form = this.fb.group({
      catActivitySector: ['', Validators.required],
      catNature: ['', Validators.required],
      catBrand: ['', Validators.required],
      catAssetType: ['', Validators.required],
      isSubmitted: ['']
    });
    this.subscriptions.push(
      this.form.valueChanges.subscribe(value => {
        this.onChange(value);
        this.onTouched();


      }));

    this.loadALlCatSectorLabel();
    this.loadAllCatNatureLabel();


  }

  loadALlCatSectorLabel() {
    this.catActivitySectorService.getActivitySectorList().subscribe(res => {
      res=res.filter(sector => sector.isActive);


      res.forEach((element, index) =>
      this.catActivitySectorOptions[index] = element.label);
    })
  }
  loadAllCatNatureLabel() {
    this.catNatureService.getNatureList().subscribe(res => {
      res=res.filter(nature => nature.isActive);
      res.forEach((element, index) => this.catNatureOptions[index] = element.label);
    })
  }
  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  onChange: any = () => {
    // This is intentional
  };
  onTouched: any = () => {
    // This is intentional
  };

  registerOnChange(fn): void {
    this.onChange = fn;
  }

  writeValue(value): void {
    if (value) {
      this.value = value;
    }

    if (value === null) {
      this.form.reset();
    }
  }

  registerOnTouched(fn): void {
    this.onTouched = fn;
  }

  validate(_: FormControl): any {

    return this.form.valid ? null : { materialQualification: { valid: false, }, };
  }


  addCatRequest(desc) {
    const modalRef = this.modalService.open(CreateCatRequestModalComponent,
      {
        scrollable: true,
        windowClass: "createBrandModalClass",
        centered: true,
        backdrop: 'static'
      }
    );
    modalRef.componentInstance.catNature = this.form.getRawValue().catNature;
    modalRef.componentInstance.catActivitySector = this.form.getRawValue().catActivitySector;
    modalRef.componentInstance.workflowCode = this.valuationRequest.workflowInstanceId;
    modalRef.componentInstance.buisinessKey = this.valuationRequest.workflowBuisinessKey;
    modalRef.componentInstance.valuationRequest = this.valuationRequest;


    if (desc == 'type') {
      modalRef.componentInstance.catBrand = this.form.getRawValue().catBrand;

    }
    else {
      modalRef.componentInstance.catBrand = null;

    }

  }
  disableForm() {
    if (this.valide) {

      this.form.disable()
    }
    else {

      this.form.enable();
    }
  }
  setDisabledState(disabled: boolean) {
    if (disabled) {
      this.form.disable()
    } else {
      this.form.enable()
    }
  }

  searchStarstWith(term: string, item: any) {
    // check if the name startsWith the input, everything in lowercase so "a" will match "A"
    return item.toLowerCase().startsWith(term.toLowerCase())
  }
  loadCatType(query) {

    let nature = (this.form.value.catNature && this.form.value.catNature.id) ? this.form.value.catNature.label : this.form.value.catNature;
    let brand = ( this.form.value.catBrand && this.form.value.catBrand.id )? this.form.value.catBrand.label : this.form.value.catBrand;
    let sector =( this.form.value.catActivitySector && this.form.value.catActivitySector.id) ? this.form.value.catActivitySector.label : this.form.value.catActivitySector;
    this.catAssetTypeOptions = [];
    this.catAssetTypeService.loadAssetTypeListByQuery(sector, nature, brand, (query != null && query != undefined && query != '') ? query : null).subscribe(
      res => {
        this.catAssetTypeOptions = res;
        this.isSelectedCatType = false;
      },()=>{
              //this is intentional
},()=>{
        this.catTypeIsLoading = false;

      }
    )
  }
  selectEventCatType(event) {
    if (event) {
      this.isSelectedCatType = true;

      this.form.patchValue({ catAssetType: event })

         this.loadCatBrand( this.form.value.catBrand)
         this.loadCatSector();
        this.loadCatNature();

    }
  }
  resetCatType() {
    this.form.get('catAssetType').reset();
    this.catAssetTypeOptions = [];
    this.loadCatBrand(this.form.value.catBrand);
    this.loadCatNature();
    this.loadCatSector();
    this.loadCatType(this.form.value.catAssetType);
  }
  resetCatBrand() {
    this.form.get('catBrand').reset();
    this.catBrandOptions = []
    this.loadCatBrand(this.form.value.catBrand);
    this.loadCatNature();
    this.loadCatSector();
    this.loadCatType(this.form.value.catAssetType);
  }

  resetCatNature() {

    this.form.get('catNature').reset();
    this.catNatureOptions = [];
    this.loadCatNature();
    this.loadCatSector();
    this.loadCatBrand(this.form.value.catBrand);
    this.loadCatType(this.form.value.catAssetType);
  }
  resetCatSector() {
    this.form.get('catActivitySector').reset();
    this.catActivitySectorOptions = [];
    this.loadCatBrand(this.form.value.catBrand);
    this.loadCatNature();
    this.loadCatSector();
    this.loadCatType(this.form.value.catAssetType);
  }
  loadCatBrand(query) {
    let nature = (this.form.value.catNature && this.form.value.catNature.id) ? this.form.value.catNature.label : this.form.value.catNature;
    let assetType =( this.form.value.catAssetType && this.form.value.catAssetType.id) ? this.form.value.catAssetType.label : this.form.value.catAssetType;
    let sector =( this.form.value.catActivitySector && this.form.value.catActivitySector.id) ? this.form.value.catActivitySector.label : this.form.value.catActivitySector;
    // if (this.catBrandOptions.length == 0 || (query != null && query != undefined && query != '')) 33{
    this.catBrandOptions = [];
    this.catBrandService.loadCatBrandListByQuery(sector, nature, (query != null && query != undefined && query != '') ? query : null, assetType).subscribe(
      res => {
        this.catBrandOptions = res;
        this.isSelectedCatBrand = false;
      }, () => {
                 //this is intentional
}, () => {
        this.catBrndIsLoading = false;

      });


  }
  selectEventCatBrand(event) {

    if (event) {
      this.isSelectedCatBrand = true;
      this.form.patchValue({ catBrand: event })
         this.loadCatNature()

      if(!this.form.value.catAssetType){this.loadCatType( this.form.value.catAssetType)}
        this.loadCatSector()



    }
  }

  loadCatSector() {
    if (this.form.value.catActivitySector || this.form.value.catNature || this.form.value.catBrand || this.form.value.catAssetType) {
      let nature = (this.form.value.catNature && this.form.value.catNature.id) ? this.form.value.catNature.label : this.form.value.catNature;
      let brand = ( this.form.value.catBrand && this.form.value.catBrand.id )? this.form.value.catBrand.label : this.form.value.catBrand;
      let assetType =( this.form.value.catAssetType && this.form.value.catAssetType.id) ? this.form.value.catAssetType.label : this.form.value.catAssetType;
      let sector =( this.form.value.catActivitySector && this.form.value.catActivitySector.id) ? this.form.value.catActivitySector.label : this.form.value.catActivitySector;

      this.catActivitySectorService.loadActivitySectorListByQuery(sector ? sector : null,
         nature, brand, assetType).subscribe(
        res => {
          this.catActivitySectorOptions = res;
        }, (err) => {
          console.error(err);

        });
    }
    else {
      this.loadALlCatSectorLabel();
    }
  }
  loadCatNature() {

    if (this.form.value.catActivitySector || this.form.value.catNature || this.form.value.catBrand || this.form.value.catAssetType) {

      let brand = ( this.form.value.catBrand && this.form.value.catBrand.id )? this.form.value.catBrand.label : this.form.value.catBrand;
      let assetType =( this.form.value.catAssetType && this.form.value.catAssetType.id) ? this.form.value.catAssetType.label : this.form.value.catAssetType;
      let sector =( this.form.value.catActivitySector && this.form.value.catActivitySector.id) ? this.form.value.catActivitySector.label : this.form.value.catActivitySector;
      let nature =( this.form.value.catNature && this.form.value.catNature.id) ? this.form.value.catNature.label : this.form.value.catNature;

      this.catNatureService.loadNatureListByQuery(sector, nature ?nature : null,
        brand, assetType,'true').subscribe(
          res => {
            this.catNatureOptions = res;

          }
          , (err) => {
            console.error(err);
          }
          );
        }
    else {
      this.loadAllCatNatureLabel();
    }
  }
  loadAllQualifMaterielByNature(isNatureChanged) {
    this.loadCatSector();
    if(isNatureChanged){
      this.loadCatBrand(null);

    }else{
      this.loadCatBrand(this.form.value.catBrand);
    }



    this.loadCatType(this.form.value.catAssetType)

  }
  loadQualificationListBySector() {

    this.loadCatNature();


    this.loadCatBrand(this.form.value.catBrand)
    this.loadCatType(this.form.value.catAssetType)
  }

  updateVlueBrand(event) {
    if (event)
   { if (this.catBrandOptions.length == 1) {
      this.form.patchValue({ catBrand: this.catBrandOptions[0] })
      this.loadCatType(this.form.value.catAssetType)
      this.loadCatSector();
     this.loadCatNature();
    }
  }
}
  updateValueType(event) {
    if (event)
{
    if (this.catAssetTypeOptions.length == 1) {
      this.form.patchValue({ catAssetType: this.catAssetTypeOptions[0] })
      this.loadCatBrand( this.form.value.catBrand)
      this.loadCatSector();
     this.loadCatNature();
    }
  }
  }

  brndInputChanged(query){
    this.catBrndIsLoading = true;
    this.loadCatBrand(query);
  }

  typeInputChanged(query){
    this.catTypeIsLoading = true;
    this.loadCatType(query);
  }

  loadFromDraft(sector , nature ,brand , type){
    this.form.patchValue({

      catActivitySector: sector , 
      catNature: nature,
      catBrand: brand, 
      catAssetType:type ,
    })
    if (sector){
      this.loadQualificationListBySector();
    }
    if (nature){
      this.loadAllQualifMaterielByNature(true);
    }
    if (brand){
      this.selectEventCatBrand(brand)
    }
    if (type){
      this.selectEventCatType(type);
    }
  }
}
