import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { of, Subscription } from 'rxjs';
import { MaterialQualificationComponent } from './material-qualification.component';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { CommonModule } from '@angular/common';
import { CatNatureService } from 'src/app/shared/services/cat-nature.service';
import { CatAssetTypeService } from 'src/app/shared/services/cat-asset-type.service';
import { CatActivitySectorService } from 'src/app/shared/services/cat-activity-sector.service';
import { ValuationRequestModel } from 'src/app/shared/models/valuation-request.model';
import { CatBrandService } from 'src/app/shared/services/cat-brand.service';



describe('MaterialQualificationComponent', () => {
  let fixture: ComponentFixture<MaterialQualificationComponent>;
  let component: MaterialQualificationComponent;
  let formBuilderMock: FormBuilder;
  let subscriptionMock: Subscription;
  let catActivitySectorServiceMock: any;
  let catNatureServiceMock: any;
  let catBrandServiceMock: any;
  let catAssetTypeServiceMock: any;
  let modalServiceMock: any;

  beforeEach(() => {
    subscriptionMock = new Subscription();

    formBuilderMock = new FormBuilder();

    catActivitySectorServiceMock = {
      getActivitySectorList: jest.fn().mockReturnValue(of([
        { id: '0', label: 'Tracteur', catActivitySector: { id: '2', label: 'Electronique' } },
        { id: '1', label: 'Semi-remorque porte caisse', catActivitySector: { id: '2', label: 'Electronique' } },
        { id: '2', label: 'Semi-remorque porte engins', catActivitySector: { id: '2', label: 'Electronique' } }
      ])),
      loadActivitySectorListByQuery: jest.fn().mockReturnValue(of([
        { id: '0', label: 'Tracteur', catActivitySector: { id: '2', label: 'Electronique' } },
        { id: '1', label: 'Semi-remorque porte caisse', catActivitySector: { id: '2', label: 'Electronique' } },
        { id: '2', label: 'Semi-remorque porte engins', catActivitySector: { id: '2', label: 'Electronique' } }
      ]))
    }
    catNatureServiceMock = {
      getNatureList: jest.fn().mockReturnValue(of([
        { id: '0', label: 'Tracteur', catActivitySector: { id: '2', label: 'Electronique' } },
        { id: '1', label: 'Semi-remorque porte caisse', catActivitySector: { id: '2', label: 'Electronique' } },
        { id: '2', label: 'Semi-remorque porte engins', catActivitySector: { id: '2', label: 'Electronique' } }
      ])),
      loadNatureListByQuery: jest.fn().mockReturnValue(of([
        { id: '0', label: 'Tracteur', catActivitySector: { id: '2', label: 'Electronique' } },
        { id: '1', label: 'Semi-remorque porte caisse', catActivitySector: { id: '2', label: 'Electronique' } },
        { id: '2', label: 'Semi-remorque porte engins', catActivitySector: { id: '2', label: 'Electronique' } }
      ]))

    }

    catBrandServiceMock = {
      loadCatBrandListByQuery: jest.fn().mockReturnValue(of([
        { id: '0', label: 'LOUAULT', catNature: { id: '0', label: 'Tracteur', catActivitySector: null } },
        { id: '1', label: 'MAX TRAILER', catNature: { id: '0', label: 'Tracteur', catActivitySector: null } },
        { id: '2', label: 'METACO BFG', catNature: { id: '0', label: 'Tracteur', catActivitySector: null } }
      ]))
    }
    catAssetTypeServiceMock = {
      loadAssetTypeListByQuery: jest.fn().mockReturnValue(of([
        { id: '0', label: '3 essieux', catBrand: { id: '1', label: 'MAX TRAILER', catNature: null } },
        { id: '1', label: 'NS', catBrand: { id: '1', label: 'MAX TRAILER', catNature: null } }

      ]))
    }

    modalServiceMock = {
      open: jest.fn().mockReturnValue({
        componentInstance: {
          content: '',
          catNature: null,
          catActivitySector: null,
          workflowCode: null,
          buisinessKey: null,
          catBrand: null
        }
      }),
    };

    TestBed.configureTestingModule({
      declarations: [MaterialQualificationComponent],
      imports: [
        NgSelectModule,
        ReactiveFormsModule,
        AutocompleteLibModule,
        CommonModule
      ]
    }).overrideComponent(MaterialQualificationComponent, {
      set: {
        providers: [
          { provide: FormBuilder, useValue: formBuilderMock },
          { provide: CatNatureService, useValue: catNatureServiceMock },
          { provide: CatBrandService, useValue: catBrandServiceMock },
          { provide: CatAssetTypeService, useValue: catAssetTypeServiceMock },
          { provide: NgbModal, useValue: modalServiceMock },
          { provide: CatActivitySectorService, useValue: catActivitySectorServiceMock },

        ]
      }
    }
    ).compileComponents();

    fixture = TestBed.createComponent(MaterialQualificationComponent);
    component = fixture.debugElement.componentInstance;

    component.ngOnInit();


  });

  describe('Test: ngOnInit', () => {
    it('should initialize form', () => {

      const form = {
        catActivitySector: '',
        catNature: '',
        catBrand: '',
        catAssetType: '',
        isSubmitted: ''
      };

      expect(component.form.value).toEqual(form);

    });

    it('should change form', () => {

      const form = {
        catActivitySector: 'hello',
        catNature: '',
        catBrand: '',
        catAssetType: '',
        isSubmitted: ''
      };
      component.form.controls['catActivitySector'].setValue('hello');

      expect(component.form.value).toEqual(form);

    });
  });


  describe('Test HTML', () => {
    it('should not show error message activitySector', () => {
      fixture.detectChanges();
      const div = fixture.debugElement.nativeElement.querySelector('#activitySectorErrorMessage');
      expect(div).toBeFalsy();


    });

    it('should  show error message activitySector', () => {
      component.submited = true;
      fixture.detectChanges();

      const div = fixture.debugElement.nativeElement.querySelector('#activitySectorErrorMessage');

      expect(div).toBeTruthy();
      expect(div.textContent).toContain('Champs à compléter')

    });

    it('should not show error message natureErrorMessage', () => {
      fixture.detectChanges();
      const div = fixture.debugElement.nativeElement.querySelector('#natureErrorMessage');
      expect(div).toBeFalsy();


    });

    it('should  show error message natureErrorMessage', () => {
      component.submited = true;
      fixture.detectChanges();

      const div = fixture.debugElement.nativeElement.querySelector('#natureErrorMessage');

      expect(div).toBeTruthy();
      expect(div.textContent).toContain('Champs à compléter')

    });

    it('should not show error message brandErrorMessage', () => {
      fixture.detectChanges();
      const div = fixture.debugElement.nativeElement.querySelector('#brandErrorMessage');
      expect(div).toBeFalsy();


    });

    it('should  show error message brandErrorMessage', () => {
      component.submited = true;
      fixture.detectChanges();

      const div = fixture.debugElement.nativeElement.querySelector('#brandErrorMessage');

      expect(div).toBeTruthy();
      expect(div.textContent).toContain('Champs à compléter')

    });

    it('should not show error message assetTypeErrorMessage', () => {
      fixture.detectChanges();
      const div = fixture.debugElement.nativeElement.querySelector('#assetTypeErrorMessage');
      expect(div).toBeFalsy();


    });

    it('should  show error message assetTypeErrorMessage', () => {
      component.submited = true;
      fixture.detectChanges();

      const div = fixture.debugElement.nativeElement.querySelector('#assetTypeErrorMessage');

      expect(div).toBeTruthy();
      expect(div.textContent).toContain('Champs à compléter')

    });
  });


  describe('Test ngOnDestroy', () => {
    it('should unsubscribe', () => {
      component.ngOnDestroy();
      const spyunsubscribe = jest.spyOn(subscriptionMock, 'unsubscribe');
      expect(spyunsubscribe).toBeDefined();
    });
  });


  describe('Test registerOnChange', () => {
    it('should get register', () => {
      let test;
      component.registerOnChange(test);
      expect(component.onChange).not.toEqual(() => {
        //this is intentional
      });
      expect(component.onChange).toEqual(test);
    });
  });

  describe('Test registerOnTouched', () => {
    it('should get register', () => {
      let test;
      component.registerOnTouched(test);
      expect(component.onTouched).not.toEqual(() => {
        //this is intentional
      });
      expect(component.onTouched).toEqual(test);
    });
  });

  describe('Test validate', () => {

    it('should not retrun null', () => {
      let form: any;
      component.form.controls['catActivitySector'].setErrors({ 'incorrect': true });
      expect(component.validate(form)).toEqual({ "materialQualification": { "valid": false } });

    });
  });

  describe('Test writeValue', () => {
    it('should get value', () => {
      let value = ({
        catActivitySector: 'test',
        catNature: 'test',
        catBrand: 'test',
        catAssetType: 'test',
        isSubmitted: '',
      });

      component.writeValue(value);
      expect(component.value).toEqual(value)
    });
    it('should not write value', () => {
      let value = ({
        catActivitySector: 'test',
        catNature: 'test',
        catBrand: 'test',
        catAssetType: 'test',
        isSubmitted: false
      });
      let valueNull = ({
        catActivitySector: null,
        catNature: null,
        catBrand: null,
        catAssetType: null,
        isSubmitted: null
      });
      component.writeValue(value);
      component.writeValue(null);

      expect(component.value).toEqual(valueNull)


    });
  });

  /*
  describe("test lodCatNatureListBySectorID ", () => {
    it('should update ListeCat Nature', () => {
      component.form.controls['catActivitySector'].patchValue({ id: '2', label: 'Electronique' })

      component.loadAllCatNatureLabel();
      expect(component.natureOptions).toEqual([{ id: '0', label: 'Tracteur', catActivitySector: { id: '2', label: 'Electronique' } } as CatNatureModel,
      { id: '1', label: 'Semi-remorque porte caisse', catActivitySector: { id: '2', label: 'Electronique' } } as CatNatureModel,
      { id: '2', label: 'Semi-remorque porte engins', catActivitySector: { id: '2', label: 'Electronique' } } as CatNatureModel]);
    })


    it('should not update ListeCat Nature', () => {
      component.form.controls['catActivitySector'].patchValue(null)

      component.loadAllCatNatureLabel();
      expect(component.natureOptions).toBeNull()
    })
  });


  describe("test lodCatBrandListByCatNatureID ", () => {
    it('should update ListeCatBrand ', () => {

      component.form.controls['catNature'].patchValue({ id: '0', label: 'Tracteur' })
      component.loadCatBrand("Tracteur");
      expect(component.brandOptions).toEqual([
        { id: '0', label: 'LOUAULT', catNature: { id: '0', label: 'Tracteur', catActivitySector: null } } as CatBrandModel,
        { id: '1', label: 'MAX TRAILER', catNature: { id: '0', label: 'Tracteur', catActivitySector: null } } as CatBrandModel,
        { id: '2', label: 'METACO BFG', catNature: { id: '0', label: 'Tracteur', catActivitySector: null } } as CatBrandModel
      ]);
    })

    it('should not update ListeCatBrand ', () => {

      component.form.controls['catNature'].patchValue(null)
      component.loadAllCatNatureLabel();
      expect(component.brandOptions).toBeDefined();
    })
  });

  describe("test lodCatAssetTypeListByCatBrandID ", () => {
    it('should update ListeCatAssetType ', () => {

      component.form.controls['catBrand'].patchValue({ id: '1', label: 'MAX TRAILER' })
      component.loadCatType("3 essieux");
      expect(component.assetTypeOptions).toEqual([{ id: '0', label: '3 essieux', catBrand: { id: '1', label: 'MAX TRAILER', catNature: null } } as CatAssetTypeModel,
      { id: '1', label: 'NS', catBrand: { id: '1', label: 'MAX TRAILER', catNature: null } } as CatAssetTypeModel

      ]);
    })

    it('should not update ListeCatAssetType ', () => {

      component.form.controls['catBrand'].patchValue(null)
      component.loadCatType("3 essieux");
      expect(component.assetTypeOptions).toBeNull();
    })
  });

*/
  describe("test disableForm ", () => {
    it('should disable ', () => {
      component.valide = true;
      component.disableForm()
      expect(component.form.disabled).toEqual(true);
    })


    it('should enable ', () => {
      component.valide = false;
      component.disableForm()
      expect(component.form.disabled).toEqual(false);
    })
  });

  describe("test addCatRequest", () => {
    it('should open modalService with type request', () => {
      component.valuationRequest = { workflowBuisinessKey: '1', workflowInstanceId: '2' } as ValuationRequestModel
      component.addCatRequest('type');
      expect(modalServiceMock.open).toHaveBeenCalled
    });

    it('should open modalService with brand request', () => {
      component.valuationRequest = { workflowBuisinessKey: '1', workflowInstanceId: '2' } as ValuationRequestModel

      component.addCatRequest('brand');
      expect(modalServiceMock.open).toHaveBeenCalled
    });
  });

  describe("test setDisabledState()", () => {
    it('should enable form', () => {
      component.setDisabledState(false);
      expect(component.form.enable).toHaveBeenCalled
    });

    it('should disable form', () => {
      component.setDisabledState(true);
      expect(component.form.disable).toHaveBeenCalled
    });
  });



  describe('Test get f()', () => {
    it('should return form controls', () => {
      expect(component.f).toEqual(component.form.controls);
    })
  });

  /*  describe('Test searchStarstWith()', () => {
      it('should return true when item starts with t', () => {
        let item ={
          label: 'TRANSPORT'
        }
        expect(component.searchStarstWith('t', item)).toEqual(true);
      });

      it('should return false when item doesnt start with t', () => {
        let item ={
          label: 'AGRICOLE'
        }
        expect(component.searchStarstWith('t', item)).toEqual(false);
      })
    });*/

    describe('Test selectEventCatType()', () => {
    it('should set is Selected to true and update form ', () => {
      component.selectEventCatType("type")
      expect(component.isSelectedCatType ).toEqual(true);
    })
  });
  describe('Test selectEventCatBrand()', () => {
    it('should set is Selected to true and update form ', () => {
      component.selectEventCatBrand("brand")
      expect(component.isSelectedCatBrand ).toEqual(true);
    })
  });
  describe('Test resetCatType()', () => {
    it('should set is Selected to true and update form ', () => {
      component.resetCatType();
      expect(component.loadCatBrand ).toHaveBeenCalled;
    })
  });
  describe('Test resetCatBrand()', () => {
    it('should set is Selected to true and update form ', () => {
      component.resetCatBrand();
      expect(component.loadCatBrand ).toHaveBeenCalled;
    })
  });
  describe('Test resetCatNature()', () => {
    it('should resetCatNature', () => {
      component.resetCatNature();
      expect(component.loadCatBrand ).toHaveBeenCalled;
    })
  });
  describe('Test resetCatSector()', () => {
    it('should resetCatSector ', () => {
      component.resetCatSector();
      expect(component.loadCatBrand ).toHaveBeenCalled;
    })
  });
  describe('Test loadAllQualifMaterielByNature()', () => {
    it('should loadAllQualifMaterielByNature if isNatureCHanged ', () => {
      component.loadAllQualifMaterielByNature(true);
      expect(component.loadCatBrand ).toHaveBeenCalled;
    })
    it('should loadAllQualifMaterielByNature if isNatureCHanged false ', () => {
      component.loadAllQualifMaterielByNature(false);
      expect(component.loadCatBrand ).toHaveBeenCalled;
    })
  });
  describe('Test loadQualificationListBySector()', () => {
    it('should loadQualificationListBySector ', () => {
      component.loadQualificationListBySector();
      expect(component.loadCatNature ).toHaveBeenCalled;
    })

  });
  describe('Test updateVlueBrand()', () => {
    it('should updateVlueBrand  ', () => {

      component.catBrandOptions = [  'LOUAULT']
      component.updateVlueBrand(true);
      expect(component.loadCatNature ).toHaveBeenCalled;
    })

  });
  describe('Test updateValueType()', () => {
    it('should updateValueType ', () => {
      component.catAssetTypeOptions = [  'LOUAULT']

      component.updateValueType(true);
      expect(component.loadCatNature ).toHaveBeenCalled;
    })

  });
  describe('Test loadFromDraft()', () => {

    it('should loadFromDraft when Sector is missing', () => {
      component.loadFromDraft( null, 'ELECTRODIALISEUR','EURODIA','ED 5-6');
      expect(component.form.get('catNature').value).toEqual('ELECTRODIALISEUR');
    });
  });
});
