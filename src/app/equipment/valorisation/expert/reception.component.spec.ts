import { TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { UrlTree } from '@angular/router';
import { forkJoin, of, Subject, Subscription, throwError } from 'rxjs';
import { CatActivitySectorModel } from '../../../shared/models/cat-activity-sector.model';
import { CatAssetTypeModel } from '../../../shared/models/cat-asset-type.model';
import { CatBrandModel } from '../../../shared/models/cat-brand.model';
import { CatNatureModel } from '../../../shared/models/cat-nature.model';
import { CatRequestWorkFlow } from '../../../shared/models/catalog-add-request-workflow.model';
import { EntrepriseInfoModel } from '../../../shared/models/entreprise-info.model';
import { ParamSettingModel } from '../../../shared/models/param-setting.model';
import { Role } from '../../../shared/models/roles.enum';
import { SvsDescCollabModel } from '../../../shared/models/svs-desc-collab.model';
import { SvsDescModel } from '../../../shared/models/svs-desc.model';
import { ValuationRequestWorkflowModel } from '../../../shared/models/valuation-request-workflow.model';
import { ValuationRequestModel } from '../../../shared/models/valuation-request.model';
import { ValuationModel } from '../../../shared/models/valuation.model';
import { PagesConstants } from '../../../shared/pages-constant';
import { ValuationSharedService } from '../../../shared/services/valuation-shared-service';
import { StatusConstant } from '../../../shared/status-constant';
import { ReceptionComponent } from './reception.component';
import { CatRequestModel } from 'src/app/shared/models/cat-request.model';
import { CatRequestDoc } from 'src/app/shared/models/catalog-request-doc.model';

describe('ReceptionComponent', () => {
  let fixture: ReceptionComponent;
  let formBuilderMock: FormBuilder;
  let routeMock: any;
  let valuationServiceMock: any;
  let entrepriseInfoServiceMock: any;
  let valuationRequestDocsServiceMock: any;
  let catActivitySectorServiceMock: any;
  let catNatureServiceMock: any;
  let catBrandServiceMock: any;
  let catAssetTypeServiceMock: any;
  let popupServiceMock: any;
  let svsDescCollabServiceMock: any;
  let authServiceMock: any;
  let paramSettingServiceMock;
  let valuationRequestWorkflowServiceMock;
  let dataSharingMock: any;
  let mdmInformationServiceMock;
  let catRequestWorkflowServiceMock;
  let catalogAddRequestServiceMock;
  let catalogAddRequestWorkflowServiceMock;
  let valuationRequest: ValuationRequestModel = {
    id: '1',
    financingAmount: 23
  } as ValuationRequestModel;
  let valuationReception: ValuationModel = {
    valuationRequest: valuationRequest
  } as ValuationModel;
  let routerMock: any;
  let valuationSharedServiceMock: any;
  let decoteServiceMock: any;
  let receptionSubmitComponent: any;
  let loadingServiceMock: any;
  beforeEach(() => {

    valuationSharedServiceMock = TestBed.inject(ValuationSharedService);
    catalogAddRequestWorkflowServiceMock = {
      saveCatRequestWorkflow: jest.fn().mockReturnValue(of(true)),
    }
    loadingServiceMock ={
      show: jest.fn().mockReturnValue(of(true)),
      hide: jest.fn().mockReturnValue(of(true)),
    }

    catalogAddRequestServiceMock = {
      saveCatRequest: jest.fn().mockReturnValue(of({
        id: '1'
      } as CatRequestModel)),

      saveCatRequestDocument: jest.fn().mockReturnValue(of({
        id: '1'
      } as CatRequestDoc)),
    }
    catActivitySectorServiceMock = {
      getActivitySectorList: jest.fn().mockReturnValue(of([
        { id: '0', label: 'Agricole', isActive: true },
        { id: '1', label: 'Agro-alimentaire', isActive: true },
        { id: '2', label: 'Electronique', isActive: true }])),

      getActivitySectorById: jest.fn().mockReturnValue(of({ id: '0', label: 'Agricole' }))


    }
    receptionSubmitComponent = {
      submitDraft: jest.fn().mockReturnValue(of(true))
    }


    formBuilderMock = new FormBuilder();
    routeMock = {
      snapshot: {
        paramMap: {
          get: (key: string) => {
            switch (key) {
              case 'id': return '1';
              default: return null;
            }
          },
        },
      }
    };

    let resp = {
      valuationRequest: {
        financingAmount: 1235500,
        bssResponsible: '1255'
      } as ValuationRequestModel
    } as ValuationModel

    valuationServiceMock = {
      saveValuation: jest.fn().mockReturnValue(of({
        id : '125',
        valuationRequest: {
          id:'777'
        } as ValuationRequestModel
      } as ValuationModel)),
      getValuationByIdRequest: jest.fn().mockReturnValue(of(resp)),
      getValuationById: jest.fn().mockReturnValue(of({
        "id": "1",
        "discountRateComment": "",
        "experId": "",
        "controllerId": "",
        "validatorId": "",
        "catNature": "",
        "paramValidationType": "",
        "catBrand": "",
        "catAssetType": "",
        "valuationRequest": {
          "id": "5"
        },
        "catActivitySector": "",

      })),
      valuation: new Subject()
    };

    valuationRequestDocsServiceMock = {
      uploadRequestDocumentFromReception: jest.fn().mockReturnValue(of(true)),
      updateBssRespDocuments: jest.fn().mockReturnValue(of(true))
    }

    popupServiceMock = {
      popupInfo: jest.fn().mockReturnValue(of(true)),
      popupCommunication: jest.fn().mockReturnValue(of(true)),
      onConfirmCommunication: new Subject(),
      onConfirm: of(true)
    }

    svsDescCollabServiceMock = {
      getSvsDescCollab: jest.fn().mockReturnValue(of({
        maxAmount: 132500,
        desc: {
          id: '1'
        } as SvsDescModel
      } as SvsDescCollabModel))
    }
    dataSharingMock = {
      currentBtnStatus: {
        subscribe: jest.fn().mockReturnValue(of(true)),
      },
      triggerCommunication: new Subject(),

      changeStatus: jest.fn().mockReturnValue(of(true)),
    }

    authServiceMock = {
      identityClaims: {
        'mat': 'M102'
      },
      currentUserRolesList: ['ROLE_SVS_ADMIN_SE']
    }


    decoteServiceMock = {
      deleteValuationDiscounts: jest.fn().mockReturnValue(of(true)),
    }



    catNatureServiceMock = {
      getNatureListByCatActivitySectorID: jest.fn().mockReturnValue(of([
        { id: '0', label: 'nature1' },
        { id: '1', label: 'nature2' },
        { id: '2', label: 'nature3' }]))
    }

    catBrandServiceMock = {
      getCatBrandListByCatNatureId: jest.fn().mockReturnValue(of([
        { id: '0', label: 'brand1' },
        { id: '1', label: 'brand2' },
        { id: '2', label: 'brand3' }]))
    }
    catAssetTypeServiceMock = {
      getAssetTypeListByCatBrandId: jest.fn().mockReturnValue(of([
        { id: '0', label: 'assetType1' },
        { id: '1', label: 'assetType2' },
        { id: '2', label: 'assetType3' }]))
    }
    routerMock = {
      navigate: jest.fn(),
      url: '/search',

      serializeUrl: jest.fn().mockReturnValue('url/url'),
      createUrlTree: jest.fn().mockReturnValue(new UrlTree()),
      navigateByUrl: jest.fn().mockImplementation(() => Promise.resolve(true)),

    };

    paramSettingServiceMock = {

      getParamByCode: jest.fn().mockReturnValue(of([

        { value: StatusConstant.STATUS_CREATED },

        { value: StatusConstant.WAIT_EXP_VAL },

        { value: StatusConstant.WAIT_INFO_EXP_CA },

        { value: StatusConstant.RESP_INFO_CA_EXP },

        { value: StatusConstant.WAIT_INFO_EXP_FIL },

        { value: StatusConstant.RESP_INFO_FIL_EXP },

        { value: StatusConstant.WAIT_VALID_MAN_EXP_QUAL },

        { value: StatusConstant.WAIT_VALID_FIL },

        { value: StatusConstant.WAIT_INFO_FIL_EXP },

        { value: StatusConstant.RESP_INFO_EXP_FIL },

        { value: StatusConstant.VAL_FIL_REJ },

        { value: StatusConstant.FIL_VALIDATED },

        { value: StatusConstant.EXP_VALIDATED },


      ]))

    }

    entrepriseInfoServiceMock = {
      entrepriseInfo: {} as EntrepriseInfoModel
    }

    valuationRequestWorkflowServiceMock = {
      getValuationRequestStatus: jest.fn().mockReturnValue(of({ status: { value: StatusConstant.WAIT_EXP_VAL } } as ValuationRequestWorkflowModel)),
      getValuationRequestHistory: jest.fn().mockReturnValue(of([{ status: { value: StatusConstant.WAIT_EXP_VAL } } as ValuationRequestWorkflowModel
        , { status: { value: StatusConstant.STATUS_CREATED } } as ValuationRequestWorkflowModel])),
      saveValuationRequestWorkflow: jest.fn().mockReturnValue(of({} as ValuationRequestWorkflowModel)),
      assigneValuationRequest: jest.fn().mockReturnValue(of(true))
    }

    mdmInformationServiceMock = {
      getMdmCaInformation: jest.fn().mockReturnValue(of({
        desc: {
          id: '52'
        }
      })),
    }

    catRequestWorkflowServiceMock = {
      getCatRequestStatus: jest.fn().mockReturnValue(of({
        status: { value: StatusConstant.STATUS_RQST_WKFL_REJ }
      } as CatRequestWorkFlow)),
    }

    valuationServiceMock.valuation = { id: 'xx' }

    fixture = new ReceptionComponent(
      formBuilderMock,
      routeMock,
      valuationServiceMock,
      valuationRequestDocsServiceMock,
      entrepriseInfoServiceMock,
      catActivitySectorServiceMock,
      popupServiceMock,
      svsDescCollabServiceMock,
      authServiceMock,
      dataSharingMock,
      catNatureServiceMock,
      catBrandServiceMock,
      catAssetTypeServiceMock,
      mdmInformationServiceMock,
      paramSettingServiceMock,
      valuationRequestWorkflowServiceMock,
      routerMock,
      valuationSharedServiceMock,
      decoteServiceMock,
      catRequestWorkflowServiceMock,
      catalogAddRequestServiceMock,
      catalogAddRequestWorkflowServiceMock,
      loadingServiceMock
    );
    fixture.valuationReception = valuationReception;
    fixture.communicationSubscription = new Subscription();
    spyOn(window, "scrollTo").and.returnValue(true);


    fixture.ngOnInit();


  });

  describe('Test: ngOnInit', () => {
    it('should initialize form', () => {

      const form = {
        clientInfo: null,
        valuationRequest: null,
        materialQualification: null,
        valuationReceptionPlans: null
      };
      expect(fixture.equipmentForm.value).toEqual(form);

    });

    it('should deploy communication modal resp info CA', () => {


      valuationRequestWorkflowServiceMock = {
        getValuationRequestStatus: jest.fn().mockReturnValue(of({ status: { value: StatusConstant.WAIT_EXP_VAL } } as ValuationRequestWorkflowModel)),
        getValuationRequestHistory: jest.fn().mockReturnValue(of([
          { status: { value: StatusConstant.WAIT_EXP_VAL } } as ValuationRequestWorkflowModel,
          { status: { value: StatusConstant.RESP_INFO_CA_EXP } } as ValuationRequestWorkflowModel
        ])),
        saveValuationRequestWorkflow: jest.fn().mockReturnValue(of({} as ValuationRequestWorkflowModel)),
      }
      authServiceMock = {
        identityClaims: {
          'mat': 'M102'
        },

        currentUserRolesList: [Role.SVS_EXPERT_SE]

      }



      fixture = new ReceptionComponent(
        formBuilderMock,
        routeMock,
        valuationServiceMock,
        valuationRequestDocsServiceMock,
        entrepriseInfoServiceMock,
        catActivitySectorServiceMock,
        popupServiceMock,
        svsDescCollabServiceMock,
        authServiceMock,
        dataSharingMock,
        catNatureServiceMock,
        catBrandServiceMock,
        catAssetTypeServiceMock,
        mdmInformationServiceMock,
        paramSettingServiceMock,
        valuationRequestWorkflowServiceMock,
        routerMock,
        valuationSharedServiceMock,
        decoteServiceMock,
        catRequestWorkflowServiceMock,
        catalogAddRequestServiceMock,
        catalogAddRequestWorkflowServiceMock,
        loadingServiceMock
      );

      fixture.valuationReception = valuationReception;
      fixture.communicationSubscription = new Subscription();
      spyOn(fixture, 'deployCommunicatioPopup');

      fixture.ngOnInit();



      forkJoin([valuationRequestWorkflowServiceMock.getValuationRequestStatus, valuationRequestWorkflowServiceMock.getValuationRequestHistory]).subscribe(res => {
        expect(fixture.deployCommunicatioPopup).toHaveBeenCalledWith(
          {
            Theme: "Réponse à demande de complétude", Specs: [


              { page: PagesConstants.RECEPTION, user: Role.SVS_EXPERT_SE, actor: [Role.CA], history: [StatusConstant.WAIT_INFO_EXP_CA, StatusConstant.RESP_INFO_CA_EXP], status: [StatusConstant.WAIT_EXP_VAL], lastStatus: [StatusConstant.RESP_INFO_CA_EXP], action: null },

            ]
          }
        )


      });

    })
    it('should deploy communication modal resp info fil', () => {


      valuationRequestWorkflowServiceMock = {
        getValuationRequestStatus: jest.fn().mockReturnValue(of({ status: { value: StatusConstant.WAIT_VALID_FIL } } as ValuationRequestWorkflowModel)),
        getValuationRequestHistory: jest.fn().mockReturnValue(of([
          { status: { value: StatusConstant.WAIT_VALID_FIL } } as ValuationRequestWorkflowModel,
          { status: { value: StatusConstant.RESP_INFO_EXP_FIL } } as ValuationRequestWorkflowModel
        ])),
        saveValuationRequestWorkflow: jest.fn().mockReturnValue(of({} as ValuationRequestWorkflowModel)),
      }
      authServiceMock = {
        identityClaims: {
          'mat': 'M102'
        },

        currentUserRolesList: [Role.ADMIN_SE]

      }



      fixture = new ReceptionComponent(
        formBuilderMock,
        routeMock,
        valuationServiceMock,
        valuationRequestDocsServiceMock,
        entrepriseInfoServiceMock,
        catActivitySectorServiceMock,
        popupServiceMock,
        svsDescCollabServiceMock,
        authServiceMock,
        dataSharingMock,
        catNatureServiceMock,
        catBrandServiceMock,
        catAssetTypeServiceMock,
        mdmInformationServiceMock,
        paramSettingServiceMock,
        valuationRequestWorkflowServiceMock,
        routerMock,
        valuationSharedServiceMock,
        decoteServiceMock,
        catRequestWorkflowServiceMock,
        catalogAddRequestServiceMock,
        catalogAddRequestWorkflowServiceMock,
        loadingServiceMock
      );

      fixture.valuationReception = valuationReception;
      fixture.communicationSubscription = new Subscription();
      spyOn(fixture, 'deployCommunicatioPopup');

      fixture.ngOnInit();

      forkJoin([valuationRequestWorkflowServiceMock.getValuationRequestStatus, valuationRequestWorkflowServiceMock.getValuationRequestHistory]).subscribe(res => {
        expect(fixture.deployCommunicatioPopup).toHaveBeenCalledWith(
          {
            Theme: "Réponse à demande de précision", Specs: [

              { page: PagesConstants.RECEPTION, user: Role.SVS_EXPERT_SE, actor: [Role.ADMIN_SE], history: [StatusConstant.WAIT_INFO_EXP_FIL, StatusConstant.RESP_INFO_FIL_EXP], status: [StatusConstant.WAIT_EXP_VAL], lastStatus: [StatusConstant.RESP_INFO_FIL_EXP], action: null },

            ]
          },
        )
      })
    });

    it('should deploy communication modal resp info fil', () => {


      valuationRequestWorkflowServiceMock = {
        getValuationRequestStatus: jest.fn().mockReturnValue(of({ status: { value: StatusConstant.WAIT_VALID_FIL } } as ValuationRequestWorkflowModel)),
        getValuationRequestHistory: jest.fn().mockReturnValue(of([
          { status: { value: StatusConstant.WAIT_VALID_FIL } } as ValuationRequestWorkflowModel,
          { status: { value: StatusConstant.RESP_INFO_EXP_FIL } } as ValuationRequestWorkflowModel
        ])),
        saveValuationRequestWorkflow: jest.fn().mockReturnValue(of({} as ValuationRequestWorkflowModel)),
      }
      authServiceMock = {
        identityClaims: {
          'mat': 'M102'
        },

        currentUserRolesList: [Role.ADMIN_SE]

      }



      fixture = new ReceptionComponent(
        formBuilderMock,
        routeMock,
        valuationServiceMock,
        valuationRequestDocsServiceMock,
        entrepriseInfoServiceMock,
        catActivitySectorServiceMock,
        popupServiceMock,
        svsDescCollabServiceMock,
        authServiceMock,
        dataSharingMock,
        catNatureServiceMock,
        catBrandServiceMock,
        catAssetTypeServiceMock,
        mdmInformationServiceMock,
        paramSettingServiceMock,
        valuationRequestWorkflowServiceMock,
        routerMock,
        valuationSharedServiceMock,
        decoteServiceMock,
        catRequestWorkflowServiceMock,
        catalogAddRequestServiceMock,
        catalogAddRequestWorkflowServiceMock,
        loadingServiceMock
      );

      fixture.valuationReception = valuationReception;
      fixture.communicationSubscription = new Subscription();
      spyOn(fixture, 'deployCommunicatioPopup');

      fixture.ngOnInit();

      forkJoin([valuationRequestWorkflowServiceMock.getValuationRequestStatus, valuationRequestWorkflowServiceMock.getValuationRequestHistory]).subscribe(res => {
        expect(fixture.deployCommunicatioPopup).toHaveBeenCalledWith(
          {
            Theme: "Réponse à demande de précision", Specs: [

              { page: PagesConstants.RECEPTION, user: Role.SVS_EXPERT_SE, actor: [Role.ADMIN_SE], history: [StatusConstant.WAIT_INFO_EXP_FIL, StatusConstant.RESP_INFO_FIL_EXP], status: [StatusConstant.WAIT_EXP_VAL], lastStatus: [StatusConstant.RESP_INFO_FIL_EXP], action: null },

            ]
          },
        )
      })
    });

    it('should deploy communication modal to admin resp expert', () => {


      valuationRequestWorkflowServiceMock = {
        getValuationRequestStatus: jest.fn().mockReturnValue(of({ status: { value: StatusConstant.WAIT_INFO_EXP_FIL } } as ValuationRequestWorkflowModel)),
        getValuationRequestHistory: jest.fn().mockReturnValue(of([
          { status: { value: StatusConstant.WAIT_INFO_EXP_FIL } } as ValuationRequestWorkflowModel,
          { status: { value: StatusConstant.WAIT_EXP_VAL } } as ValuationRequestWorkflowModel
        ])),
        saveValuationRequestWorkflow: jest.fn().mockReturnValue(of({} as ValuationRequestWorkflowModel)),
      }
      authServiceMock = {
        identityClaims: {
          'mat': 'M102'
        },

        currentUserRolesList: [Role.ADMIN_SE]

      }



      fixture = new ReceptionComponent(
        formBuilderMock,
        routeMock,
        valuationServiceMock,
        valuationRequestDocsServiceMock,
        entrepriseInfoServiceMock,
        catActivitySectorServiceMock,
        popupServiceMock,
        svsDescCollabServiceMock,
        authServiceMock,
        dataSharingMock,
        catNatureServiceMock,
        catBrandServiceMock,
        catAssetTypeServiceMock,
        mdmInformationServiceMock,
        paramSettingServiceMock,
        valuationRequestWorkflowServiceMock,
        routerMock,
        valuationSharedServiceMock,
        decoteServiceMock,
        catRequestWorkflowServiceMock,
        catalogAddRequestServiceMock,
        catalogAddRequestWorkflowServiceMock,
        loadingServiceMock

      );

      fixture.valuationReception = valuationReception;
      fixture.communicationSubscription = new Subscription();
      spyOn(fixture, 'deployCommunicatioPopup');

      fixture.ngOnInit();

      forkJoin([valuationRequestWorkflowServiceMock.getValuationRequestStatus, valuationRequestWorkflowServiceMock.getValuationRequestHistory]).subscribe(res => {
        expect(fixture.deployCommunicatioPopup).toHaveBeenCalled()
      })
    });

    it('should deploy communication modal to expert resp admin', () => {


      valuationRequestWorkflowServiceMock = {
        getValuationRequestStatus: jest.fn().mockReturnValue(of({ status: { value: StatusConstant.WAIT_INFO_FIL_EXP } } as ValuationRequestWorkflowModel)),
        getValuationRequestHistory: jest.fn().mockReturnValue(of([
          { status: { value: StatusConstant.WAIT_INFO_FIL_EXP } } as ValuationRequestWorkflowModel,
          { status: { value: StatusConstant.WAIT_VALID_FIL } } as ValuationRequestWorkflowModel
        ])),
        saveValuationRequestWorkflow: jest.fn().mockReturnValue(of({} as ValuationRequestWorkflowModel)),
      }
      authServiceMock = {
        identityClaims: {
          'mat': 'M102'
        },

        currentUserRolesList: [Role.SVS_EXPERT_SE]

      }



      fixture = new ReceptionComponent(
        formBuilderMock,
        routeMock,
        valuationServiceMock,
        valuationRequestDocsServiceMock,
        entrepriseInfoServiceMock,
        catActivitySectorServiceMock,
        popupServiceMock,
        svsDescCollabServiceMock,
        authServiceMock,
        dataSharingMock,
        catNatureServiceMock,
        catBrandServiceMock,
        catAssetTypeServiceMock,
        mdmInformationServiceMock,
        paramSettingServiceMock,
        valuationRequestWorkflowServiceMock,
        routerMock,
        valuationSharedServiceMock,
        decoteServiceMock,
        catRequestWorkflowServiceMock,
        catalogAddRequestServiceMock,
        catalogAddRequestWorkflowServiceMock,
        loadingServiceMock

      );

      fixture.valuationReception = valuationReception;
      fixture.communicationSubscription = new Subscription();
      spyOn(fixture, 'deployCommunicatioPopup');

      fixture.ngOnInit();

      forkJoin([valuationRequestWorkflowServiceMock.getValuationRequestStatus, valuationRequestWorkflowServiceMock.getValuationRequestHistory]).subscribe(res => {
        expect(fixture.deployCommunicatioPopup).toHaveBeenCalled()
      })
    });

    it('should deploy communication modal reject catalog', () => {


      valuationRequestWorkflowServiceMock = {
        getValuationRequestStatus: jest.fn().mockReturnValue(of({ valuationRequest: { workflowInstanceId: '', workflowBuisinessKey: '' }, status: { value: StatusConstant.WAIT_EXP_VAL } } as ValuationRequestWorkflowModel)),
        getValuationRequestHistory: jest.fn().mockReturnValue(of([
          { valuationRequest: { workflowInstanceId: '', workflowBuisinessKey: '' }, status: { value: StatusConstant.WAIT_EXP_VAL } } as ValuationRequestWorkflowModel,
          { status: { value: StatusConstant.STATUS_CREATED } } as ValuationRequestWorkflowModel
        ])),
        saveValuationRequestWorkflow: jest.fn().mockReturnValue(of({} as ValuationRequestWorkflowModel)),
        assigneValuationRequest: jest.fn().mockReturnValue(of(true))
      }
      authServiceMock = {
        identityClaims: {
          'mat': 'M102'
        },

        currentUserRolesList: [Role.SVS_EXPERT_SE]

      }





      fixture = new ReceptionComponent(
        formBuilderMock,
        routeMock,
        valuationServiceMock,
        valuationRequestDocsServiceMock,
        entrepriseInfoServiceMock,
        catActivitySectorServiceMock,
        popupServiceMock,
        svsDescCollabServiceMock,
        authServiceMock,
        dataSharingMock,
        catNatureServiceMock,
        catBrandServiceMock,
        catAssetTypeServiceMock,
        mdmInformationServiceMock,
        paramSettingServiceMock,
        valuationRequestWorkflowServiceMock,
        routerMock,
        valuationSharedServiceMock,
        decoteServiceMock,
        catRequestWorkflowServiceMock,
        catalogAddRequestServiceMock,
        catalogAddRequestWorkflowServiceMock,
        loadingServiceMock
      );

      fixture.valuationReception = valuationReception;
      fixture.communicationSubscription = new Subscription();

      fixture.ngOnInit();

      forkJoin([valuationRequestWorkflowServiceMock.getValuationRequestStatus, valuationRequestWorkflowServiceMock.getValuationRequestHistory]).subscribe(res => {
        catRequestWorkflowServiceMock.getCatRequestStatus().subscribe(r => {
          expect(popupServiceMock.popupCommunication).toHaveBeenCalled()
        })
      })
    });

    it('should not deploy communication because admin', () => {


      valuationRequestWorkflowServiceMock = {
        getValuationRequestStatus: jest.fn().mockReturnValue(of({ valuationRequest: { workflowInstanceId: '', workflowBuisinessKey: '' }, status: { value: StatusConstant.WAIT_EXP_VAL } } as ValuationRequestWorkflowModel)),
        getValuationRequestHistory: jest.fn().mockReturnValue(of([
          { valuationRequest: { workflowInstanceId: '', workflowBuisinessKey: '' }, status: { value: StatusConstant.WAIT_EXP_VAL } } as ValuationRequestWorkflowModel,
          { status: { value: StatusConstant.STATUS_CREATED } } as ValuationRequestWorkflowModel
        ])),
        saveValuationRequestWorkflow: jest.fn().mockReturnValue(of({} as ValuationRequestWorkflowModel)),
        assigneValuationRequest: jest.fn().mockReturnValue(of(true))
      }
      authServiceMock = {
        identityClaims: {
          'mat': 'M102'
        },

        currentUserRolesList: [Role.ADMIN_SE]

      }





      fixture = new ReceptionComponent(
        formBuilderMock,
        routeMock,
        valuationServiceMock,
        valuationRequestDocsServiceMock,
        entrepriseInfoServiceMock,
        catActivitySectorServiceMock,
        popupServiceMock,
        svsDescCollabServiceMock,
        authServiceMock,
        dataSharingMock,
        catNatureServiceMock,
        catBrandServiceMock,
        catAssetTypeServiceMock,
        mdmInformationServiceMock,
        paramSettingServiceMock,
        valuationRequestWorkflowServiceMock,
        routerMock,
        valuationSharedServiceMock,
        decoteServiceMock,
        catRequestWorkflowServiceMock,
        catalogAddRequestServiceMock,
        catalogAddRequestWorkflowServiceMock,
        loadingServiceMock
      );

      fixture.valuationReception = valuationReception;
      fixture.communicationSubscription = new Subscription();

      fixture.ngOnInit();

      forkJoin([valuationRequestWorkflowServiceMock.getValuationRequestStatus, valuationRequestWorkflowServiceMock.getValuationRequestHistory]).subscribe(res => {
        catRequestWorkflowServiceMock.getCatRequestStatus().subscribe(r => {
          expect(popupServiceMock.popupCommunication).toHaveBeenCalled()
        })
      })
    });

    it('should not deploy communication because no catalog demand rejected', () => {


      valuationRequestWorkflowServiceMock = {
        getValuationRequestStatus: jest.fn().mockReturnValue(of({ valuationRequest: { workflowInstanceId: '', workflowBuisinessKey: '' }, status: { value: StatusConstant.WAIT_EXP_VAL } } as ValuationRequestWorkflowModel)),
        getValuationRequestHistory: jest.fn().mockReturnValue(of([
          { valuationRequest: { workflowInstanceId: '', workflowBuisinessKey: '' }, status: { value: StatusConstant.WAIT_EXP_VAL } } as ValuationRequestWorkflowModel,
          { status: { value: StatusConstant.STATUS_CREATED } } as ValuationRequestWorkflowModel
        ])),
        saveValuationRequestWorkflow: jest.fn().mockReturnValue(of({} as ValuationRequestWorkflowModel)),
        assigneValuationRequest: jest.fn().mockReturnValue(of(true))
      }
      authServiceMock = {
        identityClaims: {
          'mat': 'M102'
        },

        currentUserRolesList: [Role.ADMIN_SE]

      }

      catRequestWorkflowServiceMock = {
        getCatRequestStatus: jest.fn().mockReturnValue(of(null)),
      }





      fixture = new ReceptionComponent(
        formBuilderMock,
        routeMock,
        valuationServiceMock,
        valuationRequestDocsServiceMock,
        entrepriseInfoServiceMock,
        catActivitySectorServiceMock,
        popupServiceMock,
        svsDescCollabServiceMock,
        authServiceMock,
        dataSharingMock,
        catNatureServiceMock,
        catBrandServiceMock,
        catAssetTypeServiceMock,
        mdmInformationServiceMock,
        paramSettingServiceMock,
        valuationRequestWorkflowServiceMock,
        routerMock,
        valuationSharedServiceMock,
        decoteServiceMock,
        catRequestWorkflowServiceMock,
        catalogAddRequestServiceMock,
        catalogAddRequestWorkflowServiceMock,
        loadingServiceMock
      );

      fixture.valuationReception = valuationReception;
      fixture.communicationSubscription = new Subscription();

      fixture.ngOnInit();

      forkJoin([valuationRequestWorkflowServiceMock.getValuationRequestStatus, valuationRequestWorkflowServiceMock.getValuationRequestHistory]).subscribe(res => {
        catRequestWorkflowServiceMock.getCatRequestStatus().subscribe(r => {
          expect(popupServiceMock.popupCommunication).toHaveBeenCalled()
        })
      })
    });

    it('should fill list', () => {
      popupServiceMock.onConfirmCommunication.next({ theme: { theme: "ss", Specs: [{ action: StatusConstant.WAIT_INFO_FIL_EXP }] } })
      expect(fixture.activitySectorOptions[0]).toEqual(
        { id: '0', label: 'Agricole', isActive: true } as CatActivitySectorModel);



    })

    it('should call communication pop up', () => {


      valuationRequestWorkflowServiceMock = {
        getValuationRequestStatus: jest.fn().mockReturnValue(of({ valuationRequest: { workflowInstanceId: '', workflowBuisinessKey: '' }, status: { value: StatusConstant.WAIT_EXP_VAL } } as ValuationRequestWorkflowModel)),
        getValuationRequestHistory: jest.fn().mockReturnValue(of([
          { valuationRequest: { workflowInstanceId: '', workflowBuisinessKey: '' }, status: { value: StatusConstant.WAIT_EXP_VAL } } as ValuationRequestWorkflowModel,
          { status: { value: StatusConstant.STATUS_CREATED } } as ValuationRequestWorkflowModel
        ])),
        saveValuationRequestWorkflow: jest.fn().mockReturnValue(of({} as ValuationRequestWorkflowModel)),
        assigneValuationRequest: jest.fn().mockReturnValue(of(true))
      }
      authServiceMock = {
        identityClaims: {
          'mat': 'M102'
        },

        currentUserRolesList: [Role.ADMIN_SE]

      }

      catRequestWorkflowServiceMock = {
        getCatRequestStatus: jest.fn().mockReturnValue(of(null)),
      }
      dataSharingMock.triggerCommunication = new Subject();





      fixture = new ReceptionComponent(
        formBuilderMock,
        routeMock,
        valuationServiceMock,
        valuationRequestDocsServiceMock,
        entrepriseInfoServiceMock,
        catActivitySectorServiceMock,
        popupServiceMock,
        svsDescCollabServiceMock,
        authServiceMock,
        dataSharingMock,
        catNatureServiceMock,
        catBrandServiceMock,
        catAssetTypeServiceMock,
        mdmInformationServiceMock,
        paramSettingServiceMock,
        valuationRequestWorkflowServiceMock,
        routerMock,
        valuationSharedServiceMock,
        decoteServiceMock,
        catRequestWorkflowServiceMock,
        catalogAddRequestServiceMock,
        catalogAddRequestWorkflowServiceMock,
        loadingServiceMock
      );

      fixture.valuationReception = valuationReception;
      fixture.communicationSubscription = new Subscription();

      fixture.ngOnInit();

      forkJoin([valuationRequestWorkflowServiceMock.getValuationRequestStatus, valuationRequestWorkflowServiceMock.getValuationRequestHistory]).subscribe(res => {
        catRequestWorkflowServiceMock.getCatRequestStatus().subscribe(r => {
          dataSharingMock.triggerCommunication.next();
          dataSharingMock.triggerCommunication.subscribe(() => {

          })
        })
      })




    });
  });


  describe('Test:  submit', () => {
    it('should return false', () => {
      fixture.equipmentForm.value.materialQualification = null;
      fixture.submit();
      expect(fixture.valide).toEqual(false);

    });

    it('should return false', () => {

      spyOn(fixture, 'isUsedEquipment').and.returnValue(false);

      fixture.equipmentForm.value.materialQualification = { "catActivitySector ": "2", "catNature": "1", "catBrand": "1", "catAssetType": "1" };
      fixture.equipmentForm.value.materialQualification.catActivitySector = { "id": "2" } as CatActivitySectorModel
      fixture.equipmentForm.value.materialQualification.catBrand = { "id": "1" } as CatBrandModel
      fixture.equipmentForm.value.materialQualification.catNature = { "id": "1" } as CatNatureModel
      fixture.equipmentForm.value.materialQualification.catAssetType = { "id": "1" } as CatAssetTypeModel
      fixture.equipmentForm.value.valuationRequest = { "financingAmount": 100000 };
      fixture.equipmentForm.value.valuationRequest.paramEnergy = { "id": "4", "label": "Biocarburant" };
      fixture.equipmentForm.value.valuationRequest.paramUsageUnitId = "1";
      fixture.equipmentForm.value.valuationRequest.paramUsageUnit = { "id": "1", "unitLabel": "Kilomètres" };

      fixture.submit();
      expect(fixture.valide).toEqual(false);
    });

    it('should return true', () => {


      fixture.equipmentForm.setValue({
        clientInfo: [{ affiliater: true }],
        materialQualification: {
          catActivitySector: { id: '0', label: 'Agricole' },
          catBrand: { id: '0', label: 'Agricole' },
          catNature: { id: '0', label: 'Agricole' },
          catAssetType: { id: '0', label: 'Agricole' }

        },
        valuationRequest: {
          financingAmount: 100000,
          paramEnergy: { id: '1', label: 'Diesel' },
          isANewAsset: false,
          documentsList: [{
            isModified: true,
            docFamily: 'E',
            docType: 'E1',
            docSubType: 'E303',
            isQualified: false
          }, {
            isModified: true,
            docFamily: 'E',
            docType: 'E1',
            docSubType: 'E3031',
            isQualified: false

          },],
          docsToAdd: [
            {
              doc: { name: 'file3.png', size: 17567 } as File,
              docIndex: {
                docName: 'file3.png',
                docFamily: 'E',
                docType: 'E1',
                docSubType: 'E3031',
                isQualified: false

              }
            }
          ]

        },
        valuationReceptionPlans: null
      })

      fixture.valuationSharedService.setIsFromDuplicatedReception(true);




      fixture.submit();

      expect(valuationServiceMock.saveValuation).toHaveBeenCalled();


    });
    it('should return true', () => {


      fixture.equipmentForm.setValue({
        clientInfo: [{ affiliater: true }],
        materialQualification: {
          catActivitySector: { id: '0', label: 'Agricole' },
          catBrand: { id: '0', label: 'Agricole' },
          catNature: { id: '0', label: 'Agricole' },
          catAssetType: { id: '0', label: 'Agricole' }

        },
        valuationRequest: {
          financingAmount: 100000,
          paramEnergy: { id: '1', label: 'Diesel' },
          isANewAsset: false,
          documentsList: [{
            isModified: true,
            docFamily: 'E',
            docType: 'E1',
            docSubType: 'E303',
            isQualified: false
          }, {
            isModified: true,
            docFamily: 'E',
            docType: 'E1',
            docSubType: 'E3031',
            isQualified: false

          },],
          docsToAdd: [
            {
              doc: { name: 'file3.png', size: 17567 } as File,
              docIndex: {
                docName: 'file3.png',
                docFamily: 'E',
                docType: 'E1',
                docSubType: 'E3031',
                isQualified: false

              }
            }
          ]

        },
        valuationReceptionPlans: null
      })
      fixture.isAddMode = true;

      fixture.valuationSharedService.setIsFromDuplicatedReception(false);




      fixture.submit();

      expect(valuationServiceMock.saveValuation).toHaveBeenCalled();


    });

    it('should throw error', () => {
      spyOn(fixture, 'isUsedEquipment').and.returnValue(false);

      fixture.equipmentForm.value.materialQualification = { "catActivitySector ": "2", "catNature": "1", "catBrand": "1", "catAssetType": "1" };
      fixture.equipmentForm.value.materialQualification.catActivitySector = { "id": "2" } as CatActivitySectorModel
      fixture.equipmentForm.value.materialQualification.catBrand = { "id": "1" } as CatBrandModel
      fixture.equipmentForm.value.materialQualification.catNature = { "id": "1" } as CatNatureModel
      fixture.equipmentForm.value.materialQualification.catAssetType = { "id": "1" } as CatAssetTypeModel
      fixture.equipmentForm.value.valuationRequest = { "financingAmount": 100000 };
      fixture.equipmentForm.value.valuationRequest.paramEnergy = { "id": "4", "label": "Biocarburant" };
      fixture.equipmentForm.value.valuationRequest.paramUsageUnitId = "1";
      fixture.equipmentForm.value.valuationRequest.paramUsageUnit = { "id": "1", "unitLabel": "Kilomètres" };

      spyOn(valuationServiceMock, 'saveValuation').and.returnValue(throwError({ status: 404 }));
      fixture.submit();
      expect(fixture.valide).toEqual(false);

    });

  });



  describe('Test:  getValuationReception', () => {

    it('should return false when valuationReception.id is defined and is  from duplicate', () => {
      let valation = {
        "id": "1",
        "discountRateComment": "",
        "experId": "",
        "controllerId": "",
        "validatorId": "",
        "catNature": "",
        "paramValidationType": "",
        "catBrand": "",
        "catAssetType": "",
        "valuationRequest": {
          "id": "5"
        },
        "catActivitySector": "",

      }

      fixture.valuationSharedService.setIsFromDuplicatedReception(true);

      fixture.getValuationReception(valation);
      expect(fixture.valide).toEqual(false);

    });

    it('should return false when valuationReception.id is defined and is  from undo duplicate', () => {
      let valation = {
        "id": "1",
        "discountRateComment": "",
        "experId": "",
        "controllerId": "",
        "validatorId": "",
        "catNature": "",
        "paramValidationType": "",
        "catBrand": "",
        "catAssetType": "",
        "valuationRequest": {
          "id": "5"
        },
        "catActivitySector": "",

      }

      fixture.valuationSharedService.setIsFromDuplicatedReception(false)
      fixture.valuationSharedService.setIsFromUndoDuplicatedReception(true)
      fixture.valuationSharedService.setCatNature({ id: '0', label: 'nature1' })
      fixture.valuationSharedService.setCatBrand({ id: '0', label: 'brand1' })
      fixture.valuationSharedService.setCatAssetType({ id: '0', label: 'assetType1' })
      fixture.valuationSharedService.setCatActivitySector({ id: '0', label: 'sector' })


      fixture.getValuationReception(valation);
      expect(fixture.valide).toEqual(false);

    });
    it('should return false when valuationReception.id is defined and is  from duplicate', () => {
      let valation = {
        "id": "1",
        "discountRateComment": "",
        "experId": "",
        "controllerId": "",
        "validatorId": "",
        "catNature": "",
        "paramValidationType": "",
        "catBrand": "",
        "catAssetType": "",
        "valuationRequest": {
          "id": "5"
        },
        "catActivitySector": "",

      }

      fixture.valuationSharedService.setIsFromDuplicatedReception(true)

      fixture.getValuationReception(valation);
      expect(fixture.valide).toEqual(false);

    });

  });

  describe('Test:  duplicateValuation', () => {
    it('go to search when duplicate', () => {


      fixture.equipmentForm.value.materialQualification = { "catActivitySector ": "2", "catNature": "1", "catBrand": "1", "catAssetType": "1" };
      fixture.equipmentForm.value.materialQualification.catActivitySector = { "id": "2" } as CatActivitySectorModel
      fixture.equipmentForm.value.materialQualification.catBrand = { "id": "1" } as CatBrandModel
      fixture.equipmentForm.value.materialQualification.catNature = { "id": "1" } as CatNatureModel
      fixture.equipmentForm.value.materialQualification.catAssetType = { "id": "1" } as CatAssetTypeModel
      fixture.equipmentForm.value.valuationRequest = { "financingAmount": 100000, "paramEnergy": { "id": "4", "label": "Biocarburant" } };
      fixture.equipmentForm.value.valuationRequest.paramEnergy = { "id": "4", "label": "Biocarburant" };
      fixture.equipmentForm.value.valuationRequest.paramUsageUnitId = "1";
      fixture.equipmentForm.value.valuationRequest.paramUsageUnit = { "id": "1", "unitLabel": "Kilomètres" };
      fixture.valide = true;
      fixture.isReset = false;
      fixture.valuationSharedService.setIsFromDuplicatedReception(false)
      fixture.valuationSharedService.setIsFromUndoDuplicatedReception(false)
      fixture.valuationSharedService.setCatNature({ id: '0', label: 'nature1' })
      fixture.valuationSharedService.setCatBrand({ id: '0', label: 'brand1' })
      fixture.valuationSharedService.setCatAssetType({ id: '0', label: 'assetType1' })
      fixture.valuationSharedService.setCatActivitySector({ id: '0', label: 'sector' })
      fixture.valuationSharedService.setValuation({
        "id": "1",
        "discountRateComment": "",
        "experId": "",
        "controllerId": "",
        "validatorId": "",
        "catNature": "",
        "paramValidationType": "",
        "catBrand": "",
        "catAssetType": "",
        "valuationRequest": {
          "id": "5"
        },
        "catActivitySector": "",

      })

      fixture.duplicateValuation()
      expect(routerMock.navigate).toHaveBeenCalled();

    });

    it('cancel duplicate', () => {


      fixture.equipmentForm.value.materialQualification = { "catActivitySector ": "2", "catNature": "1", "catBrand": "1", "catAssetType": "1" };
      fixture.equipmentForm.value.materialQualification.catActivitySector = { "id": "2" } as CatActivitySectorModel
      fixture.equipmentForm.value.materialQualification.catBrand = { "id": "1" } as CatBrandModel
      fixture.equipmentForm.value.materialQualification.catNature = { "id": "1" } as CatNatureModel
      fixture.equipmentForm.value.materialQualification.catAssetType = { "id": "1" } as CatAssetTypeModel
      fixture.equipmentForm.value.valuationRequest = { "financingAmount": 100000, "paramEnergy": { "id": "4", "label": "Biocarburant" } };
      fixture.equipmentForm.value.valuationRequest.paramEnergy = { "id": "4", "label": "Biocarburant" };
      fixture.equipmentForm.value.valuationRequest.paramUsageUnitId = "1";
      fixture.equipmentForm.value.valuationRequest.paramUsageUnit = { "id": "1", "unitLabel": "Kilomètres" };
      fixture.valide = true;
      fixture.isReset = false;
      fixture.valuationSharedService.setIsFromDuplicatedReception(true)
      fixture.valuationSharedService.setIsFromUndoDuplicatedReception(false)
      fixture.valuationSharedService.setCatNature({ id: '0', label: 'nature1' })
      fixture.valuationSharedService.setCatBrand({ id: '0', label: 'brand1' })
      fixture.valuationSharedService.setCatAssetType({ id: '0', label: 'assetType1' })
      fixture.valuationSharedService.setCatActivitySector({ id: '0', label: 'sector' })
      fixture.valuationSharedService.setValuation({
        "id": "1",
        "discountRateComment": "",
        "experId": "",
        "controllerId": "",
        "validatorId": "",
        "catNature": "",
        "paramValidationType": "",
        "catBrand": "",
        "catAssetType": "",
        "valuationRequest": {
          "id": "5"
        },
        "catActivitySector": "",

      })

      fixture.duplicateValuation()
      expect(fixture.valuationSharedService.getIsFromUndoDuplicatedReception()).toBeTruthy();

    });

  });

  describe('Test:  deployCommunicatioPopup', () => {
    it('deployCommunicatioPopup', () => {

      fixture.statusList = [{ value: StatusConstant.WAIT_INFO_FIL_EXP } as ParamSettingModel]
      fixture.valuationRequestWorkflow = { status: { value: StatusConstant.WAIT_VALID_FIL } } as ValuationRequestWorkflowModel
      fixture.valuationRequestHistory = [{ status: { value: StatusConstant.WAIT_VALID_FIL } } as ValuationRequestWorkflowModel, { status: { value: StatusConstant.WAIT_EXP_VAL } } as ValuationRequestWorkflowModel]


      fixture.deployCommunicatioPopup(null);
      popupServiceMock.onConfirmCommunication.next({ theme: { theme: "ss", Specs: [{ action: StatusConstant.WAIT_INFO_FIL_EXP }] } })
      spyOn(valuationRequestWorkflowServiceMock, 'saveValuationRequestWorkflow').and.returnValue(of(null));

      expect(popupServiceMock.popupCommunication).toHaveBeenCalled();

    });

  });

  describe('Test:  isUsedEquipment', () => {
    it('should return false', () => {
      fixture.equipmentForm.value.valuationRequest = { "isAOldAsset": "false" };
      let result = fixture.isUsedEquipment();
      expect(result).toBeFalsy();
    });
    it('should return false', () => {
      fixture.equipmentForm.value.valuationRequest = { "isAOldAsset": "true" ,
        paramUsageUnitId:null,
        collAssetEstimYear: 2021,
        assetUsVolume: 1};
      let result = fixture.isUsedEquipment();
      expect(result).toBeFalsy();
    });
    it('should return false', () => {
      fixture.equipmentForm.value.valuationRequest = { "isAOldAsset": "true" ,
        paramUsageUnitId:'1',
        collAssetEstimYear: null,
        assetUsVolume: 1};
      let result = fixture.isUsedEquipment();
      expect(result).toBeFalsy();
    });
    it('should return false', () => {
      fixture.equipmentForm.value.valuationRequest = { "isAOldAsset": "true" ,
        paramUsageUnitId:'1',
        collAssetEstimYear: 2021,
        assetUsVolume: null};
      let result = fixture.isUsedEquipment();
      expect(result).toBeFalsy();
    });

    it('should return true', () => {
      fixture.equipmentForm.value.valuationRequest = { "isAOldAsset": "true" };
      fixture.equipmentForm.value.valuationRequest = { "collAssetEstimYear": 2021, "paramUsageUnitId": "4", "assetUsVolume": "50" };
      let result = fixture.isUsedEquipment();
      expect(result).toBeTruthy();
    });

    it('should return true', () => {
      fixture.equipmentForm.value.valuationRequest = { "isAOldAsset": "true" ,
        paramUsageUnitId:'1',
        collAssetEstimYear: 2021,
        assetUsVolume: 1};
      let result = fixture.isUsedEquipment();
      expect(result).toBeTruthy();
    });

  });

  describe('Test:  resetForm', () => {
    it('should equipmentForm reset be called', () => {
      spyOn(fixture.equipmentForm, 'reset').and.returnValue(true);
      fixture.resetForm();
      expect(fixture.equipmentForm.reset()).toBeTruthy();
    });

  });

  describe('Test:  changeClientInfoLoaded', () => {
    it('should set  clientInfoLoaded true', () => {
      fixture.changeClientInfoLoaded();
      expect(fixture.clientInfoLoaded).toEqual(true);
    });

  });

  describe('Test:  checkButtonValiderLabel', () => {
    it('should set  checkButtonValiderLabel to Modifier ', () => {
      fixture.valide = true;
      fixture.isReset = false;
      fixture.checkButtonValiderLabel();
      expect(fixture.buttonvaliderLabel).toEqual("Modifier");
    });
    it('should set  checkButtonValiderLabel to valider ', () => {

      fixture.checkButtonValiderLabel();
      expect(fixture.buttonvaliderLabel).toEqual("Valider");
    })
  });

  describe('Test:  updateValuation', () => {
    it('should set  updateValuation true  ', () => {
      fixture.updateValuation();
      expect(fixture.updated).toEqual(true);
    });
  });
  describe('Test:  submitOrupdate', () => {

    it('should set  call submit methode ', () => {

      fixture.buttonvaliderLabel = "Valider";
      fixture.submitOrupdate();
      expect(fixture.isReset).toEqual(false);
    })

    it('should set isReset true and updated true when btn buttonvaliderLabel = "Modifier" ', () => {

      fixture.buttonvaliderLabel = "Modifier";
      fixture.submitOrupdate();
      expect(fixture.isReset).toEqual(true);
      expect(fixture.updated).toEqual(true);
    })
  });

  describe('Test:  resetValuation', () => {
    it('should   resetValuation   ', () => {
      fixture.resetValuation();
      expect(fixture.updated).toEqual(false);
    });
  });

  describe('Test:  checkIfCollabIsAllowed', () => {
    it('should blockValidation  be false when user is admin', () => {
      fixture.idValuationRequest = '1';
      fixture.blockValidation = true;
      fixture.checkIfCollabIsAllowed();
      expect(fixture.blockValidation).toEqual(false);
    });

    it('should blockValidation be true when financingAmount <= 2_000_000 and user is not admin', () => {
      //if expert does not have the sufficent amount || is not of the same desc
      authServiceMock.currentUserRolesList = [];
      fixture.idValuationRequest = '1';
      fixture.blockValidation = false;
      fixture.checkIfCollabIsAllowed();
      expect(fixture.blockValidation).toEqual(true);
    });
    it('should blockValidation be true', () => {
      //if the valuation is not over the desired amount
      spyOn(svsDescCollabServiceMock, 'getSvsDescCollab').and.returnValue(of(null));
      authServiceMock.currentUserRolesList = [];
      fixture.idValuationRequest = '1';
      fixture.blockValidation = true;
      fixture.checkIfCollabIsAllowed();
      expect(fixture.blockValidation).toEqual(true);
      expect(dataSharingMock.changeStatus).toHaveBeenCalled();
    })
  });

  describe('Test saveValuationRequestWorkflow ()', () => {
    it('should valuationRequestWorkflow status be WAIT_INFO_EXP_FIL', () => {
      spyOn(valuationRequestWorkflowServiceMock, 'saveValuationRequestWorkflow').and.returnValue(of({ status: { value: StatusConstant.WAIT_INFO_EXP_FIL } as ParamSettingModel }));
      fixture.saveValuationRequestWorkflow({ value: StatusConstant.STATUS_VAL_WKFL } as ParamSettingModel);
      expect(valuationRequestWorkflowServiceMock.saveValuationRequestWorkflow).toHaveBeenCalled();
      expect(fixture.valuationRequestWorkflow.status.value).toEqual(StatusConstant.WAIT_INFO_EXP_FIL);
    });

    it('should valuationRequestWorkflow status be WAIT_INFO_FIL_EXP', () => {
      spyOn(valuationRequestWorkflowServiceMock, 'saveValuationRequestWorkflow').and.returnValue(of({ status: { value: StatusConstant.WAIT_INFO_FIL_EXP } as ParamSettingModel }));
      fixture.saveValuationRequestWorkflow({ value: StatusConstant.STATUS_VAL_WKFL } as ParamSettingModel);
      expect(valuationRequestWorkflowServiceMock.saveValuationRequestWorkflow).toHaveBeenCalled();
      expect(fixture.valuationRequestWorkflow.status.value).toEqual(StatusConstant.WAIT_INFO_FIL_EXP);
    });

    it('should valuationRequestWorkflow status be WAIT_INFO_FIL_EXP', () => {
      spyOn(valuationRequestWorkflowServiceMock, 'saveValuationRequestWorkflow').and.returnValue(of({ status: { value: StatusConstant.RESP_INFO_FIL_EXP } as ParamSettingModel }));
      fixture.saveValuationRequestWorkflow({ value: StatusConstant.RESP_INFO_FIL_EXP } as ParamSettingModel);
      expect(valuationRequestWorkflowServiceMock.saveValuationRequestWorkflow).toHaveBeenCalled();
      expect(fixture.valuationRequestWorkflow.status.value).toEqual(StatusConstant.RESP_INFO_FIL_EXP);
    });

    it('should valuationRequestWorkflow status be RESP_INFO_EXP_FIL', () => {
      spyOn(valuationRequestWorkflowServiceMock, 'saveValuationRequestWorkflow').and.returnValue(of({ status: { value: StatusConstant.RESP_INFO_EXP_FIL } as ParamSettingModel }));
      fixture.saveValuationRequestWorkflow({ value: StatusConstant.RESP_INFO_EXP_FIL } as ParamSettingModel);
      expect(valuationRequestWorkflowServiceMock.saveValuationRequestWorkflow).toHaveBeenCalled();
      expect(fixture.valuationRequestWorkflow.status.value).toEqual(StatusConstant.RESP_INFO_EXP_FIL);
    });

    it('should valuationRequestWorkflow status be RESP_INFO_CA_EXP', () => {
      spyOn(valuationRequestWorkflowServiceMock, 'saveValuationRequestWorkflow').and.returnValue(of({ status: { value: StatusConstant.RESP_INFO_CA_EXP } as ParamSettingModel }));
      fixture.saveValuationRequestWorkflow({ value: StatusConstant.RESP_INFO_CA_EXP } as ParamSettingModel);
      expect(valuationRequestWorkflowServiceMock.saveValuationRequestWorkflow).toHaveBeenCalled();
      expect(fixture.valuationRequestWorkflow.status.value).toEqual(StatusConstant.RESP_INFO_CA_EXP);
    });

    it('should valuationRequestWorkflow status be WAIT_INFO_EXP_CA', () => {
      spyOn(valuationRequestWorkflowServiceMock, 'saveValuationRequestWorkflow').and.returnValue(of({ status: { value: StatusConstant.WAIT_INFO_EXP_CA } as ParamSettingModel }));
      fixture.saveValuationRequestWorkflow({ value: StatusConstant.WAIT_INFO_EXP_CA } as ParamSettingModel);
      expect(valuationRequestWorkflowServiceMock.saveValuationRequestWorkflow).toHaveBeenCalled();
      expect(fixture.valuationRequestWorkflow.status.value).toEqual(StatusConstant.WAIT_INFO_EXP_CA);
    });

    it('should valuationRequestWorkflow status be WAIT_INFO_EXP_FIL', () => {
      spyOn(valuationRequestWorkflowServiceMock, 'saveValuationRequestWorkflow').and.returnValue(of({ status: { value: StatusConstant.WAIT_INFO_EXP_FIL } as ParamSettingModel }));
      fixture.saveValuationRequestWorkflow({ value: StatusConstant.WAIT_INFO_EXP_FIL } as ParamSettingModel);
      expect(valuationRequestWorkflowServiceMock.saveValuationRequestWorkflow).toHaveBeenCalled();
      expect(fixture.valuationRequestWorkflow.status.value).toEqual(StatusConstant.WAIT_INFO_EXP_FIL);
    });

    it('should valuationRequestWorkflow status be REJECTED', () => {
      spyOn(valuationRequestWorkflowServiceMock, 'saveValuationRequestWorkflow').and.returnValue(of({ status: { value: StatusConstant.REJECTED } as ParamSettingModel }));
      fixture.saveValuationRequestWorkflow({ value: StatusConstant.REJECTED } as ParamSettingModel);
      expect(valuationRequestWorkflowServiceMock.saveValuationRequestWorkflow).toHaveBeenCalled();
      expect(fixture.valuationRequestWorkflow.status.value).toEqual(StatusConstant.REJECTED);
    });

  });

  describe('Test saveValuation ()', () => {
    it('should call sercvice', () => {
      fixture.isAddMode = true;
      fixture.equipmentForm.setValue({
        clientInfo: [{ affiliater: true }],
        materialQualification: {
          catActivitySector: { id: '0', label: 'Agricole' },
          catBrand: { id: '0', label: 'Agricole' },
          catNature: { id: '0', label: 'Agricole' },
          catAssetType: { id: '0', label: 'Agricole' }

        },
        valuationRequest: {
          financingAmount: 100000,
          paramEnergy: { id: '1', label: 'Diesel' },
          isANewAsset: false,
          documentsList: [{
            isModified: true,
            docFamily: 'E',
            docType: 'E1',
            docSubType: 'E303',
            isQualified: false
          }, {
            isModified: true,
            docFamily: 'E',
            docType: 'E1',
            docSubType: 'E3031',
            isQualified: false

          },],
          docsToAdd: [
            {
              doc: { name: 'file3.png', size: 17567 } as File,
              docIndex: {
                docName: 'file3.png',
                docFamily: 'E',
                docType: 'E1',
                docSubType: 'E3031',
                isQualified: false

              }
            }
          ]

        },
        valuationReceptionPlans: null
      })

      fixture.saveValuation(fixture.equipmentForm);
      expect(valuationServiceMock.saveValuation).toHaveBeenCalled();
      valuationServiceMock.saveValuation().subscribe(() => {
        expect(valuationRequestDocsServiceMock.updateBssRespDocuments).toHaveBeenCalled();

      })


      expect(fixture.valide).toEqual(true)
    });

  });

  describe('Test isExpert', () => {

    it('should return false', () => {
      expect(fixture.isExpert()).toBeFalsy();
    });
  });

  describe('Test deleteOldDiscountsAndResetComment', () => {

    it('should call deleteValuationDiscounts false', () => {
      fixture.deleteOldDiscountsAndResetComment();
      expect(decoteServiceMock.deleteValuationDiscounts).toHaveBeenCalled()
    });
  });

  describe('Test ngOnDestroy', () => {

    it('should unsubscribe', () => {
      fixture.ngOnDestroy();
      const spyunsubscribe = jest.spyOn(fixture.communicationSubscription, 'unsubscribe');
      expect(spyunsubscribe).toBeDefined();
    });
  });

  describe('Test getDuplicateButtonName', () => {

    it('should return Dupliquer', () => {
      fixture.valuationSharedService.setIsFromDuplicatedReception(false)
      expect(fixture.getDuplicateButtonName()).toEqual('Dupliquer');
    });

    it('should return Annuler la duplication', () => {
      fixture.valuationSharedService.setIsFromDuplicatedReception(true)

      expect(fixture.getDuplicateButtonName()).toEqual('Annuler la duplication');
    });
  });


  describe('Test isSimulationValo()', () => {

    it('should isSimulation be true when idRet', () => {
      spyOn(routeMock.snapshot.paramMap, 'get').and.returnValue("22121");
      fixture.isSimulationValo();
      expect(fixture.isSimulation).toEqual(true);
    });
  });


  describe('Test initWorkflowProcess()', () => {

    it('should isSimulation be true when idRet', () => {
      fixture.isWorflowInitialized = false;
      fixture.valuationRequestHistory = [{ status: { value: StatusConstant.WAIT_VALID_FIL } } as ValuationRequestWorkflowModel, { status: { value: StatusConstant.REJECTED } } as ValuationRequestWorkflowModel]

      let aa = [Role.SVS_EXPERT_SE]
      spyOn(authServiceMock, 'currentUserRolesList').and.returnValue(of(aa));
      spyOn(fixture, 'isExpert').and.returnValue(true);


      fixture.valuationReception = {
        valuationRequest: {
          isSimulated: true
        } as ValuationRequestModel
      } as ValuationModel;

      let resp = [
        {
          status: { value: StatusConstant.STATUS_CREATED },
          valuationRequest: {
            workflowInstanceId: '111',
            workflowBuisinessKey: '222',
            isSimulated: true
          } as ValuationRequestModel
        } as ValuationRequestWorkflowModel,
        {
          status: { value: StatusConstant.RESP_INFO_CA_EXP },
          valuationRequest: {
            workflowInstanceId: '111',
            workflowBuisinessKey: '222',
            isSimulated: true
          } as ValuationRequestModel
        } as ValuationRequestWorkflowModel]

      spyOn(valuationRequestWorkflowServiceMock, 'getValuationRequestHistory').and.returnValue(of(resp));

      expect(fixture.isExpert()).toEqual(true);
    });

    it('should isSimulation be true when idRet 2', () => {
      authServiceMock = {
        identityClaims: {
          'mat': 'M102'
        },
        currentUserRolesList: [Role.SVS_EXPERT_SE]
      }
      catRequestWorkflowServiceMock = {
        getCatRequestStatus: jest.fn(() => of(
          {
            status: {
              value: StatusConstant.STATUS_RQST_WKFL_REJ
            }
          }
        ))
      }
      fixture = new ReceptionComponent(
        formBuilderMock,
        routeMock,
        valuationServiceMock,
        valuationRequestDocsServiceMock,
        entrepriseInfoServiceMock,
        catActivitySectorServiceMock,
        popupServiceMock,
        svsDescCollabServiceMock,
        authServiceMock,
        dataSharingMock,
        catNatureServiceMock,
        catBrandServiceMock,
        catAssetTypeServiceMock,
        mdmInformationServiceMock,
        paramSettingServiceMock,
        valuationRequestWorkflowServiceMock,
        routerMock,
        valuationSharedServiceMock,
        decoteServiceMock,
        catRequestWorkflowServiceMock,
        catalogAddRequestServiceMock,
        catalogAddRequestWorkflowServiceMock,
        loadingServiceMock
      );
      fixture.isWorflowInitialized = false;
      fixture.valuationRequestHistory = [{ status: { value: StatusConstant.WAIT_VALID_FIL } } as ValuationRequestWorkflowModel, { status: { value: StatusConstant.RESP_INFO_CA_EXP } } as ValuationRequestWorkflowModel];
      fixture.ngOnInit()

      // let aa = [Role.SVS_EXPERT_SE]
      // spyOn(authServiceMock, 'currentUserRolesList').and.returnValue(of(aa));
      // spyOn(fixture, 'isExpert').and.returnValue(true);


      fixture.valuationReception = {
        valuationRequest: {
          isSimulated: true
        } as ValuationRequestModel
      } as ValuationModel;

      let resp = [
        {
          status: { value: StatusConstant.STATUS_CREATED },
          valuationRequest: {
            workflowInstanceId: '111',
            workflowBuisinessKey: '222',
            isSimulated: true
          } as ValuationRequestModel
        } as ValuationRequestWorkflowModel,
        {
          status: { value: StatusConstant.RESP_INFO_CA_EXP },
          valuationRequest: {
            workflowInstanceId: '111',
            workflowBuisinessKey: '222',
            isSimulated: true
          } as ValuationRequestModel
        } as ValuationRequestWorkflowModel]

      spyOn(valuationRequestWorkflowServiceMock, 'getValuationRequestHistory').and.returnValue(of(resp));
      fixture.initWorkflowProcess();
      expect(fixture.isExpert()).toEqual(true);
    });

  });

  describe('Test ngAfterViewChecked()', () => {

    it('should afterViewCheckedRuning be true', () => {
      fixture.afterViewCheckedRuning = false;
      fixture.isWorflowInitialized = false;

      fixture.valuationReception = {
        valuationRequest: {
          id: '1',
          isSimulated: true,
          workflowBuisinessKey: null,
          workflowInstanceId: null
        } as ValuationRequestModel
      } as ValuationModel;

      let resp = {
        valuationRequest: {
          workflowBuisinessKey: '787-966',
          workflowInstanceId: '555-787'
        } as ValuationRequestModel
      } as ValuationModel

      spyOn(valuationServiceMock, 'getValuationByIdRequest').and.returnValue(of(resp));
      spyOn(valuationSharedServiceMock, 'getCatRequest').and.returnValue(of(true));

      fixture.ngAfterViewChecked();
      expect(fixture.afterViewCheckedRuning).toEqual(true);
      expect(fixture.initWorkflowProcess).toHaveBeenCalled;

    });
  });
  it('should afterViewCheckedRuning be true', () => {
    fixture.afterViewCheckedRuning = false;
    fixture.isWorflowInitialized = false;

    fixture.valuationReception = {
      valuationRequest: {
        id: '1',
        isSimulated: true,
        workflowBuisinessKey: null,
        workflowInstanceId: null
      } as ValuationRequestModel
    } as ValuationModel;

    let resp = {
      valuationRequest: {
        workflowBuisinessKey: null,
        workflowInstanceId: null
      } as ValuationRequestModel
    } as ValuationModel

    spyOn(valuationServiceMock, 'getValuationByIdRequest').and.returnValue(of(resp));

    fixture.ngAfterViewChecked();
    expect(fixture.afterViewCheckedRuning).toEqual(false);

  });

  describe('Test saveDraft()', () => {

    it('should prepareValaution to have been called', () => {
      fixture.buttonvaliderLabel = 'Valider';
      fixture.equipmentForm.value.valuationRequest = {
        financingAmount: 300000
      } as ValuationRequestModel
      fixture.saveDraft();
      expect(fixture.prepareValaution).toHaveBeenCalled;

    });
    /*
    it('should popupdraft to have been called', () => {
      //let receptionDecoteComponentmock = TestBed.inject(ReceptionDecoteComponent);

      let processEquipementValuationMock;
      let paramAmortizationProfileServiceMock;
      let paramProductFamilyServiceMock;
      let financialPlanServiceMock;
      fixture.receptionSubmitComponent = new ReceptionSubmitComponent(formBuilderMock,
        processEquipementValuationMock,
        routerMock,
        financialPlanServiceMock,
        paramProductFamilyServiceMock,
        paramAmortizationProfileServiceMock,
        valuationServiceMock,
        valuationSharedServiceMock,
        popupServiceMock)
      fixture.buttonvaliderLabel = 'Modifier';
      fixture.equipmentForm.value.valuationRequest = {
        financingAmount: 300000,
        isANewAsset: true,
      } as ValuationRequestModel
      fixture.showPopupDraft = false;
      let decoteServiceMock;

      fixture.receptionSubmitComponent.receptionDecoteComponent = new ReceptionDecoteComponent(
        decoteServiceMock,  authServiceMock,  popupServiceMock,
     valuationSharedServiceMock
      );
      spyOn(receptionSubmitComponent, 'submitDraft').arguments(false).and.returnValue(of(true));
      //spyOn(receptionSubmitComponent, 'saveValuationDiscounts').and.returnValue(of(true));
      //spyOn(ReceptionDecoteComponent, 'saveValuationDiscounts').and.returnValue(of(true));

      fixture.saveDraft();
      expect(popupServiceMock.popupInfo).toHaveBeenCalled;
    });

    it('should make popup draft true', () => {
      let processEquipementValuationMock;
      let paramAmortizationProfileServiceMock;
      let paramProductFamilyServiceMock;
      let financialPlanServiceMock;
      fixture.receptionSubmitComponent = new ReceptionSubmitComponent(formBuilderMock,
        processEquipementValuationMock,
        routerMock,
        financialPlanServiceMock,
        paramProductFamilyServiceMock,
        paramAmortizationProfileServiceMock,
        valuationServiceMock,
        valuationSharedServiceMock,
        popupServiceMock)
      fixture.receptionSubmitComponent.receptionDecoteComponent
      fixture.buttonvaliderLabel = 'Modifier';
      fixture.equipmentForm.value.valuationRequest = {
        financingAmount: 300000,
        isANewAsset: true,
      } as ValuationRequestModel
      fixture.showPopupDraft = true;
      spyOn(receptionSubmitComponent, 'submitDraft').and.returnValue(of(true));
      spyOn(receptionSubmitComponent, 'receptionDecoteComponent').and.returnValue(of(true));
      fixture.saveDraft();
      expect(receptionSubmitComponent.submitDraft).toHaveBeenCalled;
    });
*/
  });

  describe('Test getValuationEquipmentQualification()', () => {

    it('should prepareValaution to have been called', () => {


      fixture.valuationReception = {
        catActivitySector: {
          id: '1',
          label: 'agricole'
        } as CatActivitySectorModel,
        catNature: {
          id: '1'
        } as CatNatureModel,
        catBrand: {
          id: '1'
        } as CatBrandModel,
        catAssetType: {
          id: '1'
        } as CatAssetTypeModel
      } as ValuationModel

      spyOn(fixture, 'checkMaterialQualificationValididty').and.returnValue(of(true));
      spyOn(fixture, 'validRequestForm').and.returnValue(of(true));
      spyOn(fixture, 'checkIsOldAssetValidity').and.returnValue(of(true));

      fixture.getValuationEquipmentQualification();
      expect(fixture.natureOptions).toEqual([
        { id: '0', label: 'nature1' },
        { id: '1', label: 'nature2' },
        { id: '2', label: 'nature3' }]);
      expect(fixture.valide).toEqual(true);
    });

  });

  describe('Test checkMaterialQualificationValididty()', () => {

    it('should checkMaterialQualificationValididty be true', () => {

     fixture.equipmentForm.controls['materialQualification'].patchValue({
        catActivitySector: {
          id : '1'
        } as CatActivitySectorModel ,
        catNature:{
          id : '1'
        } as CatNatureModel,
        catBrand:{
          id : '1'
        } as CatBrandModel,
        catAssetType:{
          id : '1'
        } as CatAssetTypeModel
      })
      expect(fixture.checkMaterialQualificationValididty()).toEqual(true);

    });

  });

  describe('Test checkIsOldAssetValidity()', () => {

    it('should checkIsOldAssetValidity be true', () => {

     fixture.equipmentForm.controls['valuationRequest'].patchValue({
        isAOldAsset : true,
        assetUsVolume : 1,
        collAssetEstimYear: 2020,
        paramUsageUnitId: 1
      })
      expect(fixture.checkIsOldAssetValidity()).toEqual(true);

    });

    it('should checkIsOldAssetValidity be true when isAOldAsset is null', () => {

      fixture.equipmentForm.controls['valuationRequest'].patchValue({
         isAOldAsset : null,
         assetUsVolume : 1,
         collAssetEstimYear: 2020,
         paramUsageUnitId: 1
       })
       expect(fixture.checkIsOldAssetValidity()).toEqual(true);

     });

  });
});

