import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FinancialPlanModel } from '../../../../shared/models/financial-plan.model';
import { ParamAmortizationProfileModel } from '../../../../shared/models/param-amortization-profile.model';
import { ParamProductFamilyModel } from '../../../../shared/models/param-product-family.model';
import { ValuationProcessingElementsModel } from '../../../../shared/models/valuation-processing-elements.model';
import { ValuationModel } from '../../../../shared/models/valuation.model';
import { ValuationDiscountModel } from '../../../../shared/models/valuationDiscount.model';
import { FinancialPlanService } from '../../../../shared/services/financial-plan.service';
import { ParamAmortizationProfileService } from '../../../../shared/services/param-amortization-profile.service';
import { ParamProductFamilyService } from '../../../../shared/services/param-product-family.service';
import { ProcessingEquipementValuationService } from '../../../../shared/services/processing-equipement-valuation.service';
import { ReceptionDecoteComponent } from '../reception-decote/reception-decote.component';
import { ValuationReceptionPlansComponent } from '../valuation-reception-plans/valuation-reception-plans.component';
import { ValuationService } from '../../../../shared/services/valuation.service';
import { ValuationSharedService } from '../../../../shared/services/valuation-shared-service';
import { ConfirmationModalComponent } from '../../../../shared/confirmation-modal/confirmation-modal.component';
import { PopupService } from '../../../../shared/services/popup.service';

@Component({
  selector: 'app-reception-submit',
  templateUrl: './reception-submit.component.html',
  styleUrls: ['./reception-submit.component.css']
})
export class ReceptionSubmitComponent implements OnInit {


  discount: ValuationDiscountModel[];
  @ViewChild(ValuationReceptionPlansComponent) valuationReceptionPlansComponent: ValuationReceptionPlansComponent;
  @ViewChild(ReceptionDecoteComponent) receptionDecoteComponent: ReceptionDecoteComponent;

  constructor(
    private fb: FormBuilder,
    private processEquipementValuation: ProcessingEquipementValuationService,
    private router: Router,
    private financialPlanService: FinancialPlanService,
    private paramProductFamilyService: ParamProductFamilyService,
    private paramAmortizationProfileService: ParamAmortizationProfileService,
    private valuationService: ValuationService,
    private valuationSharedService: ValuationSharedService,
    private popupService: PopupService

  ) { }
  submitReceptionForm: FormGroup
  isSubmited = false;
  planEstimatedOutsError = false;
  @Input() blockValidation: any;
  @Input() valuation: ValuationModel;
  showFinancePlans: boolean;
  valuationId: string;
  financialPlan: FinancialPlanModel[] = [];
  removedfinancialPlans: any[] = [];
  isNew: boolean;
  disabledForm: boolean;

  productOptions: ParamProductFamilyModel[] = [];
  allProfilOptions: ParamAmortizationProfileModel[] = [];

  @Input() set disabled(disabled) {

    if (disabled != null && disabled != undefined) {
      this.disabledForm = disabled;
      if (this.disabledForm && this.submitReceptionForm) {
        this.submitReceptionForm.disable();
      }
    }
  }
  ngOnInit(): void {


    this.valuationId = this.valuation.id;


    this.submitReceptionForm = this.fb.group({

      valuationReceptionPlans: [null, Validators.required]

    });
    if (this.disabledForm && this.submitReceptionForm) {
      this.submitReceptionForm.disable();
      this.submitReceptionForm.controls.valuationReceptionPlans.disable()
    }

    this.paramProductFamilyService.getAllParamProductFamily().subscribe(data => {
      this.productOptions = data;

    })
    this.paramAmortizationProfileService.getAllParamAmortizationProfile().subscribe(data => {
      this.allProfilOptions = data;
    })
  }


  resetSubmit(submitted: boolean) {

    this.isSubmited = submitted;
    this.planEstimatedOutsError = false;
  }

  submit() {

    this.isSubmited = true;
    this.planEstimatedOutsError = this.checkForPlanEstimatedOuts();

    if (this.submitReceptionForm.valid && !this.planEstimatedOutsError && this.valuationReceptionPlansComponent.value.financialPlans.length < 7) {
      this.valuation.isSubmitted = true
      if (this.valuationSharedService.getIsFromDuplicatedReception()) {
        this.valuation.isDuplicated = true

      }
      this.valuationService.getValuationByIdRequest(this.valuation.valuationRequest.id).subscribe(dataValuation => {

        this.valuation.valuationRequest=dataValuation.valuationRequest;
      }, () => {

      }, () => {
        this.valuationService.saveValuation(this.valuation).subscribe(data => {
        }, () => {
            //this is intentional
        }, () => {
          let financialPlans = this.prepareFinancialPlans(true)

          this.financialPlanService.saveFinancialPlan(financialPlans)
            .subscribe(data => {
              this.valuationSharedService.resetAllValue();
              this.financialPlan = data;
              let processElement: ValuationProcessingElementsModel = {} as ValuationProcessingElementsModel;
              processElement.plans = data;
              processElement.discounts = this.discount;
              processElement.valuationRequest = this.valuation.valuationRequest;
              this.processEquipementValuation.getValuationResult(processElement).subscribe(result => {
                this.processEquipementValuation.financialPlans.next(result);
                this.processEquipementValuation.dateSubmit.next(new Date());
                this.router.navigate(['result/expert', this.valuationId])
              })

            })

          if (this.removedfinancialPlans != null && this.removedfinancialPlans.length > 0) {
            this.removedfinancialPlans.forEach(removedPlan => {

              if (removedPlan && removedPlan.planEstimatedOut && removedPlan.planEstimatedOut.planEstimatedOuts != undefined && removedPlan.planEstimatedOut.planEstimatedOuts != null) {
                removedPlan.planEstimatedOut = removedPlan.planEstimatedOut.planEstimatedOuts;
              }
            })

            this.financialPlanService.removeFinancialPlan(this.removedfinancialPlans).subscribe(data => {
              // This is intentional
            })


          }
        })
      })






    }
    else {
      if (this.valuationReceptionPlansComponent.value.financialPlans.length > 6) {
        this.popupService.popupInfo(
          "Vous ne pouvez avoir que six plans de financement maximum.", null, null, null, ConfirmationModalComponent);
        const subscriptionPopup = this.popupService.onConfirm.subscribe((t) => {

          subscriptionPopup.unsubscribe();

        })
      }

    }

  }





  prepareFinancialPlans(withPlanEstimatedOut) {
    let financialPlans = this.valuationReceptionPlansComponent.value.financialPlans;

    financialPlans.forEach(plan => {
      let paramAmortizationProfile: ParamAmortizationProfileModel = {} as ParamAmortizationProfileModel;
      paramAmortizationProfile = this.allProfilOptions.find(prof => prof.id == plan.amortizationProfileId);

      let paramProductFamilyModel: ParamProductFamilyModel = {} as ParamProductFamilyModel;
      paramProductFamilyModel = this.productOptions.find(prod => prod.id == plan.paramProductFamilyId);


      plan.paramAmortizationProfile = paramAmortizationProfile;
      plan.paramProductFamily = paramProductFamilyModel;
      if (withPlanEstimatedOut) {
        if (plan && plan.planEstimatedOut)
          plan.planEstimatedOut = plan.planEstimatedOut.planEstimatedOuts;


      }

    })
    return financialPlans;
  }




  setDiscount(discount: ValuationDiscountModel[]) {
    this.discount = discount;
    if (discount && discount[0] && discount[0].valuation) {
      this.valuation.discountRateComment = discount[0].valuation.discountRateComment;

    }
  }
  resetplans(resp: boolean) {
    this.submitReceptionForm.reset();
  }

  checkForPlanEstimatedOuts() {
    let planEstimatedOutsError = false;

    this.valuationReceptionPlansComponent.value.financialPlans.forEach(plan => {

      if (plan.planEstimatedOut != null && plan.planEstimatedOut.planEstimatedOuts != undefined && plan.planEstimatedOut.planEstimatedOuts != null) {
        plan.planEstimatedOut = plan.planEstimatedOut.planEstimatedOuts;
      }

      if (plan.planEstimatedOut == null) {
        planEstimatedOutsError = true
      }
      else {
        plan.planEstimatedOut.forEach(out => {
          if (out.estimatedOutPrincRate == null) {
            planEstimatedOutsError = true;

          }
        });
      }
    }
    );
    return planEstimatedOutsError;
  }

  verifyIfDiscountValid(resp: boolean) {
    this.showFinancePlans = resp;
  }
  submitDraft(validDraft?: boolean) {

    if (validDraft && !this.showFinancePlans) {
      this.receptionDecoteComponent.saveValuationDiscounts(true);
    }
    else {
      let financialPlans = this.prepareFinancialPlans(true);
      this.financialPlanService.saveFinancialPlan(financialPlans)
        .subscribe(data => {
          this.valuationSharedService.resetAllValue();
          this.financialPlan = data;
          let processElement: ValuationProcessingElementsModel = {} as ValuationProcessingElementsModel;
          processElement.plans = data;
          processElement.discounts = this.discount;
          processElement.valuationRequest = this.valuation.valuationRequest;
        })

    }
  }

}
