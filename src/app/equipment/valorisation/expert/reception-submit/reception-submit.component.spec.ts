import { FormBuilder } from '@angular/forms';
import { BehaviorSubject, of } from 'rxjs';
import { FinancialPlanModel } from '../../../../shared/models/financial-plan.model';
import { ParamAmortizationProfileModel } from '../../../../shared/models/param-amortization-profile.model';
import { ParamProductFamilyModel } from '../../../../shared/models/param-product-family.model';
import { PlanEstimatedOutModel } from '../../../../shared/models/plan-estimated-out.model';
import { ValuationRequestModel } from '../../../../shared/models/valuation-request.model';
import { ValuationModel } from '../../../../shared/models/valuation.model';
import { ValuationDiscountModel } from '../../../../shared/models/valuationDiscount.model';
import { ValuationReceptionPlansComponent } from '../valuation-reception-plans/valuation-reception-plans.component';
import { ReceptionSubmitComponent } from "./reception-submit.component";
import { TestBed } from '@angular/core/testing';
import { ValuationSharedService } from '../../../../shared/services/valuation-shared-service';


describe('ReceptionSubmitComponent', () => {
  let fixture: ReceptionSubmitComponent;
  let formBuilderMock: any;
  let processEquipementValuationServiceMock: any
  let routerMock: any;
  let financialPlanServiceMock: any;

  let paramProductFamilyServiceMock:any;
  let paramAmortizationProfileServiceMock : any;

  let valuationServiceMock:any;
  let valuationSharedServiceMock : any;

  let popupServiceMock 
  let mockSavedPlans: 

  
  FinancialPlanModel = {
    collateralAssetRating: null,
    createdBy: "fenouil",
    createdDate: null,
    isDuplicated : null,
    id: "34",
    paramAmortizationProfile: {
      id: "2",

    } as ParamAmortizationProfileModel,
    paramProductFamily: {
      id: "2"
    } as ParamProductFamilyModel,
    planContrbAmount: 500,
    planDuration: 80,
    planEstimatedOut: [
      {
        createdBy: "amine",
        createdDate: null,
        estimatedCollAssetVal: null,
        estimatedOutPrincRate: 80,
        estimatedOutPrincVal: null,
        estimatedOutYear: 1,
        financialPlan: null,
        id: "85",
        potentialLoss: null,
        updatedBy: "amine",
        updatedDate: null
      } as PlanEstimatedOutModel,
      {
        createdBy: "amine",
        createdDate: null,
        estimatedCollAssetVal: null,
        estimatedOutPrincRate: 85,
        estimatedOutPrincVal: null,
        estimatedOutYear: 2,
        financialPlan: null,
        id: "86",
        potentialLoss: null,
        updatedBy: "amine",
        updatedDate: null
      } as PlanEstimatedOutModel,

      {
        createdBy: "amine",
        createdDate: null,
        estimatedCollAssetVal: null,
        estimatedOutPrincRate: 70,
        estimatedOutPrincVal: null,
        estimatedOutYear: 3,
        financialPlan: null,
        id: "87",
        potentialLoss: null,
        updatedBy: "amine",
        updatedDate: null
      } as PlanEstimatedOutModel,

      {
        createdBy: "amine",
        createdDate: null,
        estimatedCollAssetVal: null,
        estimatedOutPrincRate: 65,
        estimatedOutPrincVal: null,
        estimatedOutYear: 4,
        financialPlan: null,
        id: "88",
        potentialLoss: null,
        updatedBy: "amine",
        updatedDate: null
      } as PlanEstimatedOutModel,

      {
        createdBy: "amine",
        createdDate: null,
        estimatedCollAssetVal: null,
        estimatedOutPrincRate: 40,
        estimatedOutPrincVal: null,
        estimatedOutYear: 5,
        financialPlan: null,
        id: "89",
        potentialLoss: null,
        updatedBy: "amine",
        updatedDate: null
      } as PlanEstimatedOutModel,

      {
        createdBy: "amine",
        createdDate: null,
        estimatedCollAssetVal: null,
        estimatedOutPrincRate: 30,
        estimatedOutPrincVal: null,
        estimatedOutYear: 6,
        financialPlan: null,
        id: "90",
        potentialLoss: null,
        updatedBy: "amine",
        updatedDate: null
      } as PlanEstimatedOutModel,

      {
        createdBy: "amine",
        createdDate: null,
        estimatedCollAssetVal: null,
        estimatedOutPrincRate: 10,
        estimatedOutPrincVal: null,
        estimatedOutYear: 7,
        financialPlan: null,
        id: "91",
        potentialLoss: null,
        updatedBy: "amine",
        updatedDate: null
      } as PlanEstimatedOutModel
    ]
    ,
    remainingAmount: null,
    remainingPercent: 7,
    status: null,
    updatedBy: "amine",
    updatedDate: null,
    valuation: {
      id: "1",
      valuationRequest: {
        id: "2"
      } as ValuationRequestModel
    } as ValuationModel,
    valuationRequest: {
      id: "2"
    } as ValuationRequestModel
  }

  let mockPlansResult = {
    amortizationProfileId: "2",
    collateralAssetRating: null,
    createdBy: "amine",
    createdDate: "2021-06-24T11:23:42.324401",
    id: "34",
    paramAmortizationProfile: {
      id: "2",
      paramProductFamily: {
        id: "2"
      }
    },
    paramProductFamilyId: "2",
    planContrbAmount: 500,
    planDuration: 80,
    planEstimatedOut: {
      planEstimatedOuts: [
        {
          createdBy: "amine",
          createdDate: null,
          estimatedCollAssetVal: null,
          estimatedOutPrincRate: 80,
          estimatedOutPrincVal: null,
          estimatedOutYear: 1,
          financialPlan: null,
          id: "85",
          potentialLoss: null,
          updatedBy: "amine",
          updatedDate: null
        },
        {
          createdBy: "amine",
          createdDate: null,
          estimatedCollAssetVal: null,
          estimatedOutPrincRate: 85,
          estimatedOutPrincVal: null,
          estimatedOutYear: 2,
          financialPlan: null,
          id: "86",
          potentialLoss: null,
          updatedBy: "amine",
          updatedDate: null
        },

        {
          createdBy: "amine",
          createdDate: null,
          estimatedCollAssetVal: null,
          estimatedOutPrincRate: 70,
          estimatedOutPrincVal: null,
          estimatedOutYear: 3,
          financialPlan: null,
          id: "87",
          potentialLoss: null,
          updatedBy: "amine",
          updatedDate: null
        },

        {
          createdBy: "amine",
          createdDate: null,
          estimatedCollAssetVal: null,
          estimatedOutPrincRate: 65,
          estimatedOutPrincVal: null,
          estimatedOutYear: 4,
          financialPlan: null,
          id: "88",
          potentialLoss: null,
          updatedBy: "amine",
          updatedDate: null
        },

        {
          createdBy: "amine",
          createdDate: null,
          estimatedCollAssetVal: null,
          estimatedOutPrincRate: 40,
          estimatedOutPrincVal: null,
          estimatedOutYear: 5,
          financialPlan: null,
          id: "89",
          potentialLoss: null,
          updatedBy: "amine",
          updatedDate: null
        },

        {
          createdBy: "amine",
          createdDate: null,
          estimatedCollAssetVal: null,
          estimatedOutPrincRate: 30,
          estimatedOutPrincVal: null,
          estimatedOutYear: 6,
          financialPlan: null,
          id: "90",
          potentialLoss: null,
          updatedBy: "amine",
          updatedDate: null
        },

        {
          createdBy: "amine",
          createdDate: null,
          estimatedCollAssetVal: null,
          estimatedOutPrincRate: 10,
          estimatedOutPrincVal: null,
          estimatedOutYear: 7,
          financialPlan: null,
          id: "91",
          potentialLoss: null,
          updatedBy: "amine",
          updatedDate: null
        }
      ]
    },
    remainingAmount: null,
    remainingPercent: 7,
    status: null,
    updatedBy: "amine",
    updatedDate: "2021-06-24T15:20:40.535548",
    valuation: {
      id: "1"
    },
    valuationRequest: {
      id: "2"
    }
  }


  beforeEach(() => {
    popupServiceMock= {

      popupInfo: jest.fn().mockReturnValue(of(true)),
      onConfirm: of(true)
    };
    valuationSharedServiceMock = TestBed.inject(ValuationSharedService);

    routerMock = {
      navigate: jest.fn()
    };
      formBuilderMock = new FormBuilder();

    processEquipementValuationServiceMock = {
      
      getValuationResult: jest.fn().mockReturnValue(of([mockPlansResult])),
      financialPlans: new BehaviorSubject<FinancialPlanModel[]>(null),
      dateSubmit: new BehaviorSubject<Date>(null),


    }

    let resp = {
      id: '1',
      valuationRequest: {
        id : '2'
      } as ValuationRequestModel
    } as ValuationModel

    valuationServiceMock = {
      getValuationByIdRequest: jest.fn().mockReturnValue(of(resp)),
      saveValuation : jest.fn().mockReturnValue(of(true)),
      
    }


    financialPlanServiceMock = {
      saveFinancialPlan : jest.fn().mockReturnValue(of([mockSavedPlans])),
      removeFinancialPlan : jest.fn().mockReturnValue(of([mockSavedPlans]))
      
    }




    paramProductFamilyServiceMock = {
      getAllParamProductFamily: jest.fn().mockReturnValue(of([{ id: '1', label: 'Prêt' ,paramAmortizationProfiles:null},
      { id: '2', label: 'Location' ,paramAmortizationProfiles:null},
      { id: '3', label: 'Crédit-bail' ,paramAmortizationProfiles:null}])),
    }
    paramAmortizationProfileServiceMock = {
      getAllParamAmortizationProfile: jest.fn().mockReturnValue(of([
        { id: '1', label: 'Loyer ou échéance constant(e)', paramProductFamily: { id: '1', label: 'Prêt',paramAmortizationProfiles:null  } },
        { id: '2', label: 'Non-linéaire', paramProductFamily: { id: '2', label: 'Location' ,paramAmortizationProfiles:null } },
        { id: '3', label: 'Linéaire', paramProductFamily: { id: '3', label: 'Crédit-bail' ,paramAmortizationProfiles:null } },
      ]))

    }

    fixture = new ReceptionSubmitComponent(
      formBuilderMock,
      processEquipementValuationServiceMock,
      routerMock,
      financialPlanServiceMock,
      paramProductFamilyServiceMock,
      paramAmortizationProfileServiceMock,
      valuationServiceMock,
      valuationSharedServiceMock,
      popupServiceMock
    );
    let valuation:ValuationModel ={} as ValuationModel;
    valuation.id='5';
    fixture.valuation=valuation;

    let valuationRequest ={} as ValuationRequestModel;
    valuationRequest.id='2';
    fixture.valuation.valuationRequest= valuationRequest;
    
    fixture.ngOnInit();
  });


  describe('Test Component', () => {
    it('should Be Truthy', () => {
      expect(fixture).toBeTruthy();

    });
  });
  describe('Test ngOnInit', () => {
    it('should initialize form ', () => {
      const form = {
        valuationReceptionPlans: null,
      };
      expect(fixture.submitReceptionForm.value).toEqual(form);
    });
  });



  describe('Test resetSubmit', () => {
    it('should resetSubmit ', () => {
      fixture.resetSubmit(true);
      expect(fixture.isSubmited).toEqual(true);
      expect(fixture.planEstimatedOutsError).toEqual(false);


    });
  });

  describe('Test setDiscount', () => {
    it('should setDiscount ', () => {
      let test: any;
      fixture.setDiscount(test);

      expect(fixture.discount).toEqual(test);

    });
  });



  describe('Test checkForPlanEstimatedOuts', () => {
    it('should return  false ', () => {
      let mockPlanValue = {
        amortizationProfileId: "2",
        collateralAssetRating: null,
        createdBy: "amine",
        createdDate: "2021-06-24T11:23:42.324401",
        id: "34",
        paramAmortizationProfile: {
          id: "2",
          paramProductFamily: {
            id: "2"
          }
        },
        paramProductFamilyId: "2",
        planContrbAmount: 500,
        planDuration: 80,
        planEstimatedOut: {
          planEstimatedOuts: [
            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 80,
              estimatedOutPrincVal: null,
              estimatedOutYear: 1,
              financialPlan: null,
              id: "85",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            },
            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 85,
              estimatedOutPrincVal: null,
              estimatedOutYear: 2,
              financialPlan: null,
              id: "86",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            },

            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 70,
              estimatedOutPrincVal: null,
              estimatedOutYear: 3,
              financialPlan: null,
              id: "87",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            },

            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 65,
              estimatedOutPrincVal: null,
              estimatedOutYear: 4,
              financialPlan: null,
              id: "88",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            },

            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 40,
              estimatedOutPrincVal: null,
              estimatedOutYear: 5,
              financialPlan: null,
              id: "89",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            },

            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 30,
              estimatedOutPrincVal: null,
              estimatedOutYear: 6,
              financialPlan: null,
              id: "90",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            },

            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 10,
              estimatedOutPrincVal: null,
              estimatedOutYear: 7,
              financialPlan: null,
              id: "91",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            }
          ]
        },
        remainingAmount: null,
        remainingPercent: 7,
        status: null,
        updatedBy: "amine",
        updatedDate: "2021-06-24T15:20:40.535548",
        valuation: {
          id: "1"
        },
        valuationRequest: {
          id: "2"
        }
      }


      const form: any = {
        value: {
          valuationReceptionPlans: {
            financialPlans: [mockPlanValue]
          }
        }
      }
      fixture.submitReceptionForm.setValue(form.value);
      
      fixture.valuationReceptionPlansComponent = {} as ValuationReceptionPlansComponent
      fixture.valuationReceptionPlansComponent.value = { financialPlans: [mockPlanValue] }
      expect(fixture.checkForPlanEstimatedOuts()).toEqual(false);

    });
    it('should return  true ', () => {
      let mockPlanValue = {
        amortizationProfileId: "2",
        collateralAssetRating: null,
        createdBy: "amine",
        createdDate: "2021-06-24T11:23:42.324401",
        id: "34",
        paramAmortizationProfile: {
          id: "2",
          paramProductFamily: {
            id: "2"
          }
        },
        paramProductFamilyId: "2",
        planContrbAmount: 500,
        planDuration: 80,
        planEstimatedOut: {
          planEstimatedOuts: [{
            createdBy: "amine",
            createdDate: null,
            estimatedCollAssetVal: null,
            estimatedOutPrincRate: null,
            estimatedOutPrincVal: null,
            estimatedOutYear: 1,
            financialPlan: null,
            id: "85",
            potentialLoss: null,
            updatedBy: "amine",
            updatedDate: null
          },]
        },
        remainingAmount: null,
        remainingPercent: 7,
        status: null,
        updatedBy: "amine",
        updatedDate: "2021-06-24T15:20:40.535548",
        valuation: {
          id: "1"
        },
        valuationRequest: {
          id: "2"
        }
      }


      const form: any = {
        value: {
          valuationReceptionPlans: {
            financialPlans: [mockPlanValue]
          }
        }
      }
      fixture.submitReceptionForm.setValue(form.value);
      
      fixture.valuationReceptionPlansComponent = {} as ValuationReceptionPlansComponent
      fixture.valuationReceptionPlansComponent.value = { financialPlans: [mockPlanValue] }
      expect(fixture.checkForPlanEstimatedOuts()).toEqual(true);    });


  });
  describe('Test Submit', () => {
    it('should save and set saved finacial plan ', () => {
      let mockPlanValue = {
        amortizationProfileId: "2",
        collateralAssetRating: null,
        createdBy: "amine",
        createdDate: "2021-06-24T11:23:42.324401",
        id: "34",
        paramAmortizationProfile: {
          id: "2",
          paramProductFamily: {
            id: "2"
          }
        },
        paramProductFamilyId: "2",
        planContrbAmount: 500,
        planDuration: 80,
        planEstimatedOut: {
          planEstimatedOuts: [
            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 80,
              estimatedOutPrincVal: null,
              estimatedOutYear: 1,
              financialPlan: null,
              id: "85",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            },
            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 85,
              estimatedOutPrincVal: null,
              estimatedOutYear: 2,
              financialPlan: null,
              id: "86",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            },

            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 70,
              estimatedOutPrincVal: null,
              estimatedOutYear: 3,
              financialPlan: null,
              id: "87",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            },

            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 65,
              estimatedOutPrincVal: null,
              estimatedOutYear: 4,
              financialPlan: null,
              id: "88",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            },

            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 40,
              estimatedOutPrincVal: null,
              estimatedOutYear: 5,
              financialPlan: null,
              id: "89",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            },

            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 30,
              estimatedOutPrincVal: null,
              estimatedOutYear: 6,
              financialPlan: null,
              id: "90",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            },

            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 10,
              estimatedOutPrincVal: null,
              estimatedOutYear: 7,
              financialPlan: null,
              id: "91",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            }
          ]
        },
        remainingAmount: null,
        remainingPercent: 7,
        status: null,
        updatedBy: "amine",
        updatedDate: "2021-06-24T15:20:40.535548",
        valuation: {
          id: "1"
        },
        valuationRequest: {
          id: "2"
        }
      }



      const form: any = {
        value: {
          valuationReceptionPlans: {
            financialPlans: [mockPlanValue]
          }
        }
      }

      fixture.submitReceptionForm.setValue(form.value);
      
      fixture.valuationReceptionPlansComponent = {} as ValuationReceptionPlansComponent
      fixture.valuationReceptionPlansComponent.value = { financialPlans: [mockPlanValue] }
      fixture.submit();

      expect(fixture.financialPlan).toEqual([mockSavedPlans]);


    });

    it('should not save finacial plan ', () => {
    
      let mockPlanValue = {
        amortizationProfileId: "2",
        collateralAssetRating: null,
        createdBy: "amine",
        createdDate: "2021-06-24T11:23:42.324401",
        id: "34",
        paramAmortizationProfile: {
          id: "2",
          paramProductFamily: {
            id: "2"
          }
        },
        paramProductFamilyId: "2",
        planContrbAmount: 500,
        planDuration: 80,
        planEstimatedOut: {
          planEstimatedOuts: [{
            createdBy: "amine",
            createdDate: null,
            estimatedCollAssetVal: null,
            estimatedOutPrincRate: null,
            estimatedOutPrincVal: null,
            estimatedOutYear: 1,
            financialPlan: null,
            id: "85",
            potentialLoss: null,
            updatedBy: "amine",
            updatedDate: null
          },]
        },
        remainingAmount: null,
        remainingPercent: 7,
        status: null,
        updatedBy: "amine",
        updatedDate: "2021-06-24T15:20:40.535548",
        valuation: {
          id: "1"
        },
        valuationRequest: {
          id: "2"
        }
      }



      const form: any = {
        value: {
          valuationReceptionPlans: {
            financialPlans: [mockPlanValue]
          }
        }
      }

      fixture.submitReceptionForm.setValue(form.value);
      
      fixture.valuationReceptionPlansComponent = {} as ValuationReceptionPlansComponent
      fixture.valuationReceptionPlansComponent.value = { financialPlans: [mockPlanValue] }
      fixture.submit();

      expect(financialPlanServiceMock.saveFinancialPlan).toHaveBeenCalledTimes(0);


    });

    it('should process valuation', () => {
      let mockPlanValue = {
        amortizationProfileId: "2",
        collateralAssetRating: null,
        createdBy: "amine",
        createdDate: "2021-06-24T11:23:42.324401",
        id: "34",
        paramAmortizationProfile: {
          id: "2",
          paramProductFamily: {
            id: "2"
          }
        },
        paramProductFamilyId: "2",
        planContrbAmount: 500,
        planDuration: 80,
        planEstimatedOut: {
          planEstimatedOuts: [
            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 80,
              estimatedOutPrincVal: null,
              estimatedOutYear: 1,
              financialPlan: null,
              id: "85",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            },
            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 85,
              estimatedOutPrincVal: null,
              estimatedOutYear: 2,
              financialPlan: null,
              id: "86",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            },

            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 70,
              estimatedOutPrincVal: null,
              estimatedOutYear: 3,
              financialPlan: null,
              id: "87",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            },

            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 65,
              estimatedOutPrincVal: null,
              estimatedOutYear: 4,
              financialPlan: null,
              id: "88",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            },

            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 40,
              estimatedOutPrincVal: null,
              estimatedOutYear: 5,
              financialPlan: null,
              id: "89",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            },

            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 30,
              estimatedOutPrincVal: null,
              estimatedOutYear: 6,
              financialPlan: null,
              id: "90",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            },

            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 10,
              estimatedOutPrincVal: null,
              estimatedOutYear: 7,
              financialPlan: null,
              id: "91",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            }
          ]
        },
        remainingAmount: null,
        remainingPercent: 7,
        status: null,
        updatedBy: "amine",
        updatedDate: "2021-06-24T15:20:40.535548",
        valuation: {
          id: "1"
        },
        valuationRequest: {
          id: "2"
        }
      }



      const form: any = {
        value: {
          valuationReceptionPlans: {
            financialPlans: [mockPlanValue]
          }
        }
      }

      fixture.submitReceptionForm.setValue(form.value);
      fixture.discount = [{id : '2'} as ValuationDiscountModel]
      fixture.valuationReceptionPlansComponent = {} as ValuationReceptionPlansComponent
      fixture.valuationReceptionPlansComponent.value = { financialPlans: [mockPlanValue] }
      fixture.submit();

      expect(processEquipementValuationServiceMock.getValuationResult).toHaveBeenCalledWith({discounts : fixture.discount , plans :fixture.financialPlan , valuationRequest : mockPlanValue.valuationRequest})

    });

    it('should remove finacial plan ', () => {
    
      let mockPlanValue = {
        amortizationProfileId: "2",
        collateralAssetRating: null,
        createdBy: "amine",
        createdDate: "2021-06-24T11:23:42.324401",
        id: "34",
        paramAmortizationProfile: {
          id: "2",
          paramProductFamily: {
            id: "2"
          }
        },
        paramProductFamilyId: "2",
        planContrbAmount: 500,
        planDuration: 80,
        planEstimatedOut: {
          planEstimatedOuts: [
            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 80,
              estimatedOutPrincVal: null,
              estimatedOutYear: 1,
              financialPlan: null,
              id: "85",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            },
            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 85,
              estimatedOutPrincVal: null,
              estimatedOutYear: 2,
              financialPlan: null,
              id: "86",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            },

            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 70,
              estimatedOutPrincVal: null,
              estimatedOutYear: 3,
              financialPlan: null,
              id: "87",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            },

            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 65,
              estimatedOutPrincVal: null,
              estimatedOutYear: 4,
              financialPlan: null,
              id: "88",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            },

            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 40,
              estimatedOutPrincVal: null,
              estimatedOutYear: 5,
              financialPlan: null,
              id: "89",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            },

            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 30,
              estimatedOutPrincVal: null,
              estimatedOutYear: 6,
              financialPlan: null,
              id: "90",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            },

            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 10,
              estimatedOutPrincVal: null,
              estimatedOutYear: 7,
              financialPlan: null,
              id: "91",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            }
          ]
        },
        remainingAmount: null,
        remainingPercent: 7,
        status: null,
        updatedBy: "amine",
        updatedDate: "2021-06-24T15:20:40.535548",
        valuation: {
          id: "1"
        },
        valuationRequest: {
          id: "2"
        }
      }



      const form: any = {
        value: {
          valuationReceptionPlans: {
            financialPlans: [mockPlanValue]
          }
        }
      }

      fixture.submitReceptionForm.setValue(form.value);
      fixture.valuationReceptionPlansComponent = {} as ValuationReceptionPlansComponent
      fixture.valuationReceptionPlansComponent.value = { financialPlans: [mockPlanValue] }
      fixture.removedfinancialPlans = [mockPlanValue]
      fixture.submit();

      expect(financialPlanServiceMock.removeFinancialPlan).toHaveBeenCalledTimes(1);


    });

    
   


  });



  describe('Test verifyIfDiscountValid', () => {
    it('should showFinancePlans be true when resp is true', () => {
      let resp = true;
      fixture.verifyIfDiscountValid(resp);
      expect(fixture.showFinancePlans).toEqual(true);

    });

    it('should showFinancePlans be false when resp is false', () => {
      let resp = false;
      fixture.verifyIfDiscountValid(resp);
      expect(fixture.showFinancePlans).toEqual(false);

    });

  });

  describe('Test set disabled()', () => {
    it('should disabledForm be true when disabled is true', () => {
      fixture.disabled = true;
      expect(fixture.disabledForm).toEqual(true);

    });
  });

  describe('Test resetplans()', () => {
    it('should submitReceptionForm.reset be called', () => {
      fixture.resetplans(true);
      expect(fixture.submitReceptionForm.reset).toHaveBeenCalled;

    });
  });
  describe('Test submitDraft()', () => {
    /*
    it('should submitDraft be called from discounts', () => {
      let mockPlanValue = {
        amortizationProfileId: "2",
        collateralAssetRating: null,
        createdBy: "amine",
        createdDate: "2021-06-24T11:23:42.324401",
        id: "34",
        paramAmortizationProfile: {
          id: "2",
          paramProductFamily: {
            id: "2"
          }
        },
        paramProductFamilyId: "2",
        planContrbAmount: 500,
        planDuration: 80,
        planEstimatedOut: {
          planEstimatedOuts: [
            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 80,
              estimatedOutPrincVal: null,
              estimatedOutYear: 1,
              financialPlan: null,
              id: "85",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            },
            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 85,
              estimatedOutPrincVal: null,
              estimatedOutYear: 2,
              financialPlan: null,
              id: "86",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            },

            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 70,
              estimatedOutPrincVal: null,
              estimatedOutYear: 3,
              financialPlan: null,
              id: "87",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            },

            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 65,
              estimatedOutPrincVal: null,
              estimatedOutYear: 4,
              financialPlan: null,
              id: "88",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            },

            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 40,
              estimatedOutPrincVal: null,
              estimatedOutYear: 5,
              financialPlan: null,
              id: "89",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            },

            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 30,
              estimatedOutPrincVal: null,
              estimatedOutYear: 6,
              financialPlan: null,
              id: "90",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            },

            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 10,
              estimatedOutPrincVal: null,
              estimatedOutYear: 7,
              financialPlan: null,
              id: "91",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            }
          ]
        },
        remainingAmount: null,
        remainingPercent: 7,
        status: null,
        updatedBy: "amine",
        updatedDate: "2021-06-24T15:20:40.535548",
        valuation: {
          id: "1"
        },
        valuationRequest: {
          id: "2"
        }
      }



      fixture.showFinancePlans = false;
      fixture.valuationReceptionPlansComponent = {} as ValuationReceptionPlansComponent
      fixture.valuationReceptionPlansComponent.value = { financialPlans: [mockPlanValue] }
      spyOn(fixture.receptionDecoteComponent, 'saveValuationDiscounts').and.returnValue(false);

      fixture.submitDraft(true);
      expect(fixture.receptionDecoteComponent.saveValuationDiscounts(true)).toHaveBeenCalled;

    });*/
    it('should submitDraft be called from plans', () => {
      fixture.showFinancePlans = true;
      let mockPlanValue = {
        amortizationProfileId: "2",
        collateralAssetRating: null,
        createdBy: "amine",
        createdDate: "2021-06-24T11:23:42.324401",
        id: "34",
        paramAmortizationProfile: {
          id: "2",
          paramProductFamily: {
            id: "2"
          }
        },
        paramProductFamilyId: "2",
        planContrbAmount: 500,
        planDuration: 80,
        planEstimatedOut: {
          planEstimatedOuts: [
            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 80,
              estimatedOutPrincVal: null,
              estimatedOutYear: 1,
              financialPlan: null,
              id: "85",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            },
            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 85,
              estimatedOutPrincVal: null,
              estimatedOutYear: 2,
              financialPlan: null,
              id: "86",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            },

            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 70,
              estimatedOutPrincVal: null,
              estimatedOutYear: 3,
              financialPlan: null,
              id: "87",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            },

            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 65,
              estimatedOutPrincVal: null,
              estimatedOutYear: 4,
              financialPlan: null,
              id: "88",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            },

            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 40,
              estimatedOutPrincVal: null,
              estimatedOutYear: 5,
              financialPlan: null,
              id: "89",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            },

            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 30,
              estimatedOutPrincVal: null,
              estimatedOutYear: 6,
              financialPlan: null,
              id: "90",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            },

            {
              createdBy: "amine",
              createdDate: null,
              estimatedCollAssetVal: null,
              estimatedOutPrincRate: 10,
              estimatedOutPrincVal: null,
              estimatedOutYear: 7,
              financialPlan: null,
              id: "91",
              potentialLoss: null,
              updatedBy: "amine",
              updatedDate: null
            }
          ]
        },
        remainingAmount: null,
        remainingPercent: 7,
        status: null,
        updatedBy: "amine",
        updatedDate: "2021-06-24T15:20:40.535548",
        valuation: {
          id: "1"
        },
        valuationRequest: {
          id: "2"
        }
      }



      fixture.valuationReceptionPlansComponent = {} as ValuationReceptionPlansComponent
      fixture.valuationReceptionPlansComponent.value = { financialPlans: [mockPlanValue] }

      fixture.submitDraft(false);
      expect(fixture.prepareFinancialPlans(true)).toHaveBeenCalled;

    });
  });
});

