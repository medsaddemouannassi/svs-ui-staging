import { Component, OnInit, ViewChild, AfterViewChecked } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { forkJoin, of, Subscription } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { EntrepriseInfoService } from 'src/app/shared/services/entreprise-info.service';
import { ValuationService } from 'src/app/shared/services/valuation.service';
import { getThemesByRoleAndStatusAndAction } from '../../../shared/communication-themes';
import { ConfirmationModalComponent } from "../../../shared/confirmation-modal/confirmation-modal.component";
import { CatActivitySectorModel } from '../../../shared/models/cat-activity-sector.model';
import { CatAssetTypeModel } from '../../../shared/models/cat-asset-type.model';
import { CatBrandModel } from '../../../shared/models/cat-brand.model';
import { CatNatureModel } from '../../../shared/models/cat-nature.model';
import { MdmInformationsModel } from '../../../shared/models/mdm-informations.model';
import { ParamEnergyModel } from '../../../shared/models/param-energy.model';
import { ParamSettingModel } from '../../../shared/models/param-setting.model';
import { ParamUsageUnitModel } from '../../../shared/models/param-usage-unit.model';
import { Role } from '../../../shared/models/roles.enum';
import { ValuationRequestWorkflowModel } from '../../../shared/models/valuation-request-workflow.model';
import { ValuationRequestModel } from '../../../shared/models/valuation-request.model';
import { ValuationModel } from '../../../shared/models/valuation.model';
import { ValuationDocumentModel } from '../../../shared/models/valuationDocument.model';
import { WorkflowInstanceVariable } from '../../../shared/models/workflow-instance-variable.model';
import { PagesConstants } from '../../../shared/pages-constant';
import { AuthService } from '../../../shared/services/auth.service';
import { CatActivitySectorService } from '../../../shared/services/cat-activity-sector.service';
import { CatAssetTypeService } from '../../../shared/services/cat-asset-type.service';
import { CatBrandService } from '../../../shared/services/cat-brand.service';
import { CatNatureService } from '../../../shared/services/cat-nature.service';
import { CatalogAddRequestWorkflowService } from '../../../shared/services/catalog-add-request-workflow.service';
import { DecoteService } from '../../../shared/services/decote.service';
import { MdmInformationService } from '../../../shared/services/mdm-informations.service';
import { ParamSettingService } from '../../../shared/services/param-setting.service';
import { PopupService } from "../../../shared/services/popup.service";
import { SharedCommunicationDataService } from '../../../shared/services/shared-communication-data.service';
import { SvsDescCollabService } from '../../../shared/services/svs-desc-collab.service';
import { ValuationRequestDocsService } from '../../../shared/services/valuation-request-docs.service';
import { ValuationRequestWorkflowService } from '../../../shared/services/valuation-request-workflow.service';
import { ValuationSharedService } from '../../../shared/services/valuation-shared-service';
import { StatusConstant } from '../../../shared/status-constant';
import { ValuationRequestReceptionComponent } from '../shared/valuation-request-reception/valuation-request-reception.component';
import { ReceptionSubmitComponent } from './reception-submit/reception-submit.component';
import { MaterialQualificationComponent } from './material-qualification-component/material-qualification.component';
import { CatalogAddRequestService } from '../../../shared/services/catalog-add-request.service';
import { LoadingService } from 'src/app/shared/services/loading-service.service';

@Component({
  selector: 'app-reception',
  templateUrl: './reception.component.html',
  styleUrls: ['./reception.component.css']
})
export class ReceptionComponent implements OnInit, AfterViewChecked {
  @ViewChild(ReceptionSubmitComponent) receptionSubmitComponent: ReceptionSubmitComponent;
  @ViewChild(ValuationRequestReceptionComponent) valuationRequestReceptionComponent: ValuationRequestReceptionComponent;
  @ViewChild(MaterialQualificationComponent) materialQualificationComponent: MaterialQualificationComponent;

  isSimulation = false;
  idRet: string;
  equipmentForm: FormGroup;
  equipmentDraftForm: FormGroup;

  idValuationRequest: string;
  valuationReception: ValuationModel = {} as ValuationModel;
  valuationRequest: ValuationRequestModel = {} as ValuationRequestModel;

  activitySectorOptions: CatActivitySectorModel[] = [];
  brandOptions: any;
  natureOptions: any;
  assetTypeOptions: any;
  documents: ValuationDocumentModel[] = [];
  clientInfoLoaded: boolean = false;
  updated: boolean = false;
  public valide = false;
  isReset = false;
  submited = false;
  buttonvaliderLabel = "Valider";
  blockValidation = false;
  validRequestForm = true;

  lockBtnCommunication: string;
  communicationSubscription: Subscription;
  valuationRequestStatus: string;
  valuationRequestHistory: ValuationRequestWorkflowModel[]
  statusList: ParamSettingModel[];
  subscriptions: Subscription[] = [];
  valuationRequestWorkflow: ValuationRequestWorkflowModel;
  descInfo: MdmInformationsModel;
  adminInfo: MdmInformationsModel;
  disabled = false;
  selectedValuation: ValuationModel;
  isAddMode = false;
  expertDesc: MdmInformationsModel;
  isWorflowInitialized = false;
  afterViewCheckedRuning = false;
  showPopupDraft=true;
  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private valuationService: ValuationService,
    private valuationRequestDocsService: ValuationRequestDocsService,
    public entrepriseInfoService: EntrepriseInfoService,
    private catActivitySectorService: CatActivitySectorService,
    private popupService: PopupService,
    private svsDescCollabService: SvsDescCollabService,
    private authService: AuthService,
    private dataSharing: SharedCommunicationDataService,
    private catNatureService: CatNatureService,
    private catBrandService: CatBrandService,
    private catAssetTypeService: CatAssetTypeService,
    private mdmInformationService: MdmInformationService,
    private paramSettingService: ParamSettingService,
    private valuationRequestWorkflowService: ValuationRequestWorkflowService,
    public router: Router,
    public valuationSharedService: ValuationSharedService,
    private decoteService: DecoteService,
    private catRequestWorkflowService: CatalogAddRequestWorkflowService,
    private catalogAddRequestService: CatalogAddRequestService,
    private catalogAddRequestWorkflowService: CatalogAddRequestWorkflowService,
    private loadingService:LoadingService
  ) {
  }
  ngAfterViewChecked() {
    if (!this.afterViewCheckedRuning && !this.isWorflowInitialized && this.valuationReception && this.valuationReception.valuationRequest && this.valuationReception.valuationRequest.isSimulated && this.valuationReception.valuationRequest.id && !this.valuationReception.valuationRequest.workflowBuisinessKey && !this.valuationReception.valuationRequest.workflowInstanceId) {
      this.afterViewCheckedRuning = true;
      this.valuationService.getValuationByIdRequest(this.valuationReception.valuationRequest.id).subscribe(data => {
        if (data.valuationRequest.workflowBuisinessKey && data.valuationRequest.workflowInstanceId) {

          this.valuationService.valuation = data;
          this.valuationReception = data;
          this.valuationRequest=this.valuationReception.valuationRequest
          //  this.valuationRequest = data.valuationRequest;

          this.initWorkflowProcess()
          if (this.valuationSharedService.getCatRequest()) {
            let catRequest = this.valuationSharedService.getCatRequest()
            catRequest.workflowCode = data.valuationRequest.workflowInstanceId;
            catRequest.buisinessKey = data.valuationRequest.workflowBuisinessKey;
            catRequest.valuationRequest=this.valuationReception.valuationRequest;
            this.saveCatRequest(catRequest);
          }
        }
        else {
          this.afterViewCheckedRuning = false;

        }
      })
    }

  }
  ngOnInit(): void {
    if (this.valuationService.valuation.id) {
      this.valuationService.valuation = {} as ValuationModel;
    }
    window.scrollTo(0, 0);
    this.dataSharing.changeStatus("false");

    this.paramSettingService.getParamByCode(StatusConstant.STATUS_VAL_WKFL).subscribe(res => {
      this.statusList = res;
    })
    this.isSimulationValo();



    this.mdmInformationService.getMdmCaInformation(this.authService.identityClaims['mat']).subscribe((data) => {
      if (this.isExpert()) {
        this.descInfo = data

      }
      if (this.authService.currentUserRolesList.includes('ROLE_SVS_ADMIN_SE')) {
        this.adminInfo = data
      }
    });

    this.idValuationRequest = this.route.snapshot.paramMap.get('id');

    this.loadValuationRequestWorkflow();

    this.equipmentForm = this.fb.group({
      clientInfo: [],
      valuationRequest: [],
      materialQualification: [],
      valuationReceptionPlans: []

    });
    this.equipmentDraftForm = this.fb.group({
      clientInfo: [],
      valuationRequest: [],
      materialQualification: [],
      valuationReceptionPlans: []

        });


    this.catActivitySectorService.getActivitySectorList().subscribe((data) => {
      this.activitySectorOptions = data.filter(sector => sector.isActive);

    });
    if (!this.isSimulation) { this.checkIfCollabIsAllowed(); }
    else {
      this.dataSharing.changeStatus('true');
    }

    this.valuationSharedService.addCatRequestSubject.subscribe(data => {
      if (!this.valuationReception || !this.valuationReception.id) {
        this.showPopupDraft=false;
        this.saveDraft();

      }

    })
  }


  isSimulationValo() {
    this.idRet = this.route.snapshot.paramMap.get('idRet');
    if (this.idRet) {
      this.isAddMode = true;
      this.isSimulation = true;
      this.valuationRequest.underwriterId = this.idRet;
      this.valuationRequest.isRetUnderwriter = JSON.parse(this.route.snapshot.paramMap.get('isRetUnderwriter'));
      this.valuationRequest.bssResponsible = this.authService.identityClaims['mat'];
      this.valuationRequest.isSimulated = true;
      this.valuationRequest.externalSystemRef = 'CRM';
      this.valuationRequest.bssResponsibleFullName = "Desc";
    }
    else {
      this.isAddMode = false;
      this.isSimulation = false;
    }

  }
  isExpert() {
    if (this.authService.currentUserRolesList.includes(Role.SVS_EXPERT_SE)) {
      return true;
    }
    return false;
  }

  deployCommunicatioPopup(theme) {
    this.popupService.popupCommunication(this.authService.currentUserRolesList, (this.valuationReception && this.valuationReception.valuationRequest && this.valuationReception.valuationRequest.isSimulated) ? PagesConstants.RECEPTION_SIMULATION : PagesConstants.RECEPTION, this.valuationRequestStatus, this.valuationRequestHistory[1].status.value, this.valuationRequestHistory, theme);
    let subscription = this.popupService.onConfirmCommunication.subscribe(data => {
      if (data) {
        let status = this.statusList.find(expectedStatus => expectedStatus.value == data.theme.Specs[0].action);

        this.saveValuationRequestWorkflow(status, data.comment);
      }
      subscription.unsubscribe();

    })
  }
  deleteOldDiscountsAndResetComment() {
    this.decoteService.deleteValuationDiscounts(this.valuationReception).subscribe();

  }

  submit(): void {
    if (this.valuationReception && this.valuationReception.id && this.valuationSharedService.getIsFromDuplicatedReception()) {
      this.deleteOldDiscountsAndResetComment();
    }


    this.submited = true;
    /*  if (this.receptionSubmitComponent) {

        this.receptionSubmitComponent.receptionDecoteComponent.resetDecote();

      }*/
    if (this.equipmentForm.value.materialQualification
      && this.equipmentForm.value.materialQualification.catActivitySector
      && this.equipmentForm.value.materialQualification.catBrand
      && this.equipmentForm.value.materialQualification.catNature
      && this.equipmentForm.value.materialQualification.catAssetType
      && this.equipmentForm.value.valuationRequest
      && this.equipmentForm.value.valuationRequest.financingAmount
      && this.equipmentForm.value.valuationRequest.paramEnergy

      && this.isUsedEquipment()) {

      this.prepareValaution();
      this.saveValuation(this.equipmentForm);

    }
  }
  prepareValaution() {
    //should update the valuation request and should save the valuation
    let catSector = { id: null, label: (this.equipmentForm.getRawValue().materialQualification) ? this.equipmentForm.getRawValue().materialQualification.catActivitySector : null, } as CatActivitySectorModel;
    this.valuationReception.catActivitySector = catSector;
    let catBrand = { id: null, label: (this.equipmentForm.getRawValue().materialQualification) ? this.equipmentForm.getRawValue().materialQualification.catBrand : null } as CatBrandModel;
    this.valuationReception.catBrand = catBrand;
    let catNature = { id: null, label: (this.equipmentForm.getRawValue().materialQualification) ? this.equipmentForm.getRawValue().materialQualification.catNature : null } as CatNatureModel
    this.valuationReception.catNature = catNature;
    let catAssetType = { id: null, label: (this.equipmentForm.getRawValue().materialQualification) ? this.equipmentForm.getRawValue().materialQualification.catAssetType : null } as CatAssetTypeModel
    this.valuationReception.catAssetType = catAssetType;

    this.valuationReception.experId = this.authService.identityClaims['mat'];

    if (this.valuationService.valuation.id) {
      this.valuationRequest = this.valuationService.valuation.valuationRequest
    }
    //prix, quantite, energie, equip immat, materiel d'occasion, materiel à l'etranger
    this.valuationRequest.financingAmount = this.equipmentForm.value.valuationRequest.financingAmount;
    this.valuationRequest.unitaryQuantity = this.equipmentForm.value.valuationRequest.unitaryQuantity;

    //paramEnergy
    if (this.equipmentForm.getRawValue().valuationRequest) {
      let paramEnergy: ParamEnergyModel = {} as ParamEnergyModel;
      paramEnergy = this.equipmentForm.getRawValue().valuationRequest.paramEnergy;
      this.valuationRequest.paramEnergy = paramEnergy;



      this.valuationRequest.isLicencePlateAvaileble = this.equipmentForm.getRawValue().valuationRequest.isLicencePlateAvaileble;
      this.valuationRequest.isANewAsset = !this.equipmentForm.getRawValue().valuationRequest.isAOldAsset;
      this.valuationRequest.isAssetUseAbroad = this.equipmentForm.getRawValue().valuationRequest.isAssetUseAbroad;
      this.valuationRequest.requestComment = this.equipmentForm.getRawValue().valuationRequest.requestComment;

      //Année de mise en service
      this.valuationRequest.collAssetEstimYear = this.equipmentForm.getRawValue().valuationRequest.collAssetEstimYear;

      //loi de roulage select
      if (this.equipmentForm.getRawValue().valuationRequest.paramUsageUnitId != null && this.equipmentForm.getRawValue().valuationRequest.paramUsageUnitId != undefined) {
        let paramUsageUnit: ParamUsageUnitModel = {} as ParamUsageUnitModel;
        paramUsageUnit.id = this.equipmentForm.getRawValue().valuationRequest.paramUsageUnitId;
        this.valuationRequest.paramUsageUnit = paramUsageUnit;

      }
      //loi de roulage input
      this.valuationRequest.assetUsVolume = this.equipmentForm.value.valuationRequest.assetUsVolume;
      //get valuation request from valuation if valuation is saved

      this.valuationReception.valuationRequest = this.valuationRequest;

    }


  }

  isUsedEquipment(): boolean {
    return !this.equipmentForm.value.valuationRequest.isAOldAsset
      || (this.equipmentForm.value.valuationRequest.collAssetEstimYear != null
        && this.equipmentForm.value.valuationRequest.paramUsageUnitId != null
        && this.equipmentForm.value.valuationRequest.assetUsVolume != null);
  }

  resetForm(): void {
    this.equipmentForm.reset();
  }

  getValuationReception(valuation: any) {
    this.valuationReception = valuation;
    this.valuationRequest = this.valuationReception.valuationRequest;
    if(this.valuationReception && this.valuationReception.createdBy && this.valuationReception.createdBy!=' '){
      this.mdmInformationService.getMdmCaInformation(this.valuationReception.createdBy).subscribe(data => {
        this.expertDesc = data;
      })
    }
  


    if (this.valuationSharedService.getIsFromDuplicatedReception()) {

      this.valuationService.getValuationById(this.valuationSharedService.getSelectedValuationId()).subscribe(data => {
        this.selectedValuation = data;
      }, () => {

      }, () => {
        this.getValuationEquipmentQualificationToDuplicate();

      })
    }
    else if (this.valuationSharedService.getIsFromUndoDuplicatedReception()) {

      this.getValuationEquipentQualificationToUndoDuplication();




    }
    // implements qualif materiel section with saved data
    else if (this.valuationReception.id) {

      this.getValuationEquipmentQualification();

      //draft
      this.valuationRequestReceptionComponent.form.statusChanges.subscribe(
        result => {
          this.validRequestForm = (result == 'VALID');
        }
      );

      // to hide Valider btn if the valuation exist
      //this.valide = true;
      this.checkButtonValiderLabel();
      // "gestion des pouvoir"
      this.checkIfCollabIsAllowed();
    }

    if (!this.isSimulation || this.valuationRequest.id) {
      this.initWorkflowProcess()

    }

  }

  initWorkflowProcess() {
    if (!this.isWorflowInitialized) {
      this.isWorflowInitialized = true
      this.dataSharing.changeStatus('false');

      forkJoin([this.valuationRequestWorkflowService.getValuationRequestStatus(this.idValuationRequest).pipe(catchError(error => of(null))), this.valuationRequestWorkflowService.getValuationRequestHistory(this.idValuationRequest).pipe(catchError(error => of(null)))]).subscribe(res => {

        if (res[0]) {
          this.valuationRequestStatus = res[0].status.value;
        }
        if (res[1]) {
          this.valuationRequestHistory = res[1];
        }



        if (this.valuationRequestStatus && this.valuationRequestHistory && this.valuationRequestHistory.length > 0) {

          if (StatusConstant.WAIT_EXP_VAL == this.valuationRequestStatus && this.valuationRequestHistory.length > 1 && this.valuationRequestHistory[1].status.value == StatusConstant.STATUS_CREATED) {
            this.valuationRequestWorkflowService.assigneValuationRequest(res[0].valuationRequest.workflowInstanceId, res[0].valuationRequest.workflowBuisinessKey, this.valuationReception.valuationRequest.isSimulated).subscribe();
          }

          let theme;


          if (this.isExpert() && this.valuationRequestHistory.length > 1 &&
            (this.valuationRequestHistory[1].status.value == StatusConstant.RESP_INFO_CA_EXP || this.valuationRequestHistory[1].status.value == StatusConstant.RESP_INFO_FIL_EXP)) {

            theme = getThemesByRoleAndStatusAndAction(this.authService.currentUserRolesList as Role[], this.valuationRequestStatus, this.valuationRequestHistory[1].status.value, (this.valuationReception && this.valuationReception.valuationRequest && this.valuationReception.valuationRequest.isSimulated) ? PagesConstants.RECEPTION_SIMULATION : PagesConstants.RECEPTION, null)

          }

          if (this.authService.currentUserRolesList.includes(Role.ADMIN_SE) && this.valuationRequestHistory.length > 1 && (this.valuationRequestHistory[1].status.value == StatusConstant.RESP_INFO_EXP_FIL)) {

            theme = getThemesByRoleAndStatusAndAction(this.authService.currentUserRolesList as Role[], this.valuationRequestStatus, this.valuationRequestHistory[1].status.value, (this.valuationReception && this.valuationReception.valuationRequest && this.valuationReception.valuationRequest.isSimulated) ? PagesConstants.RECEPTION_SIMULATION : PagesConstants.RECEPTION, null)

          }

          if (this.valuationRequestStatus == StatusConstant.WAIT_INFO_EXP_FIL && this.authService.currentUserRolesList.includes(Role.ADMIN_SE)) {

            theme = getThemesByRoleAndStatusAndAction(this.authService.currentUserRolesList as Role[], this.valuationRequestStatus, this.valuationRequestHistory[1].status.value, (this.valuationReception && this.valuationReception.valuationRequest && this.valuationReception.valuationRequest.isSimulated) ? PagesConstants.RECEPTION_SIMULATION : PagesConstants.RECEPTION, StatusConstant.RESP_INFO_FIL_EXP)

          }

          if (this.valuationRequestStatus == StatusConstant.WAIT_INFO_FIL_EXP && this.authService.currentUserRolesList.includes(Role.SVS_EXPERT_SE)) {

            theme = getThemesByRoleAndStatusAndAction(this.authService.currentUserRolesList as Role[], this.valuationRequestStatus, this.valuationRequestHistory[1].status.value, (this.valuationReception && this.valuationReception.valuationRequest && this.valuationReception.valuationRequest.isSimulated) ? PagesConstants.RECEPTION_SIMULATION : PagesConstants.RECEPTION, StatusConstant.RESP_INFO_EXP_FIL)
          }


          if (theme) {

            this.deployCommunicatioPopup(theme)

          } else {

            if (this.isExpert()) {
              this.catRequestWorkflowService.getCatRequestStatus(res[0].valuationRequest.workflowInstanceId).subscribe(catStatus => {

                if (catStatus && catStatus.status.value == StatusConstant.STATUS_RQST_WKFL_REJ) {

                  theme = getThemesByRoleAndStatusAndAction(this.authService.currentUserRolesList as Role[], catStatus.status.value, null, (this.valuationReception && this.valuationReception.valuationRequest && this.valuationReception.valuationRequest.isSimulated) ? PagesConstants.RECEPTION_SIMULATION : PagesConstants.RECEPTION, null)


                  this.popupService.popupCommunication(this.authService.currentUserRolesList, (this.valuationReception && this.valuationReception.valuationRequest && this.valuationReception.valuationRequest.isSimulated) ? PagesConstants.RECEPTION_SIMULATION : PagesConstants.RECEPTION, catStatus.status.value, null, [catStatus], theme);

                }
              })
            }

          }



          this.communicationSubscription = this.dataSharing.triggerCommunication.subscribe(() => {
            this.deployCommunicatioPopup(null)

          })
        }

      }, (err) => {
        console.error(err);

    }, () => {
      if (this.valuationRequestStatus == StatusConstant.REJECTED || (this.valuationRequestWorkflow && (this.valuationRequestWorkflow.status.value == StatusConstant.EXP_VALIDATED
        || this.valuationRequestWorkflow.status.value == StatusConstant.FIL_VALIDATED ||
        (this.valuationRequestWorkflow.status.value == StatusConstant.WAIT_VALID_FIL && !this.authService.currentUserRolesList.includes('ROLE_SVS_ADMIN_SE')
          && this.isExpert())))
      ) {
        this.equipmentForm.disable();
        this.dataSharing.changeStatus("true");
        this.disabled = true;
      }
    })
  }
}

  changeClientInfoLoaded() {
    this.clientInfoLoaded = true;
  }
  checkButtonValiderLabel() {
    if (this.valide && !this.isReset) {
      this.buttonvaliderLabel = "Modifier";
    }
    else {
      this.buttonvaliderLabel = "Valider"
    }
  }
  updateValuation() {
    //show validate button
    this.isReset = true;
    this.checkButtonValiderLabel();
    //show reset button
    this.updated = true;
  }
  resetValuation() {
    // show Modifier Button
    this.valide = true;
    this.isReset = false;
    this.checkButtonValiderLabel();
    // hide reset button
    this.updated = false;
    // relaod old qualification
    this.getValuationReception(this.valuationReception);
  }
  checkMaterialQualificationValididty() {
    return (
      this.equipmentForm.controls['materialQualification'] && this.equipmentForm.controls['materialQualification'].value &&
      this.equipmentForm.controls['materialQualification'].value.catActivitySector != null &&
      this.equipmentForm.controls['materialQualification'].value.catAssetType != null &&
      this.equipmentForm.controls['materialQualification'].value.catBrand != null &&
      this.equipmentForm.controls['materialQualification'].value.catNature != null)
  }
  checkIsOldAssetValidity() {

    if (this.equipmentForm.controls['valuationRequest'].value.isAOldAsset != null && this.equipmentForm.controls['valuationRequest'].value.isAOldAsset) {
      return (
        this.equipmentForm.controls['valuationRequest'].value.assetUsVolume != null &&
        this.equipmentForm.controls['valuationRequest'].value.collAssetEstimYear != null &&
        this.equipmentForm.controls['valuationRequest'].value.paramUsageUnitId != null)
    }
    else {
      return true;
    }
  }
  submitOrupdate() {
    this.equipmentForm.markAllAsTouched();

    if (this.buttonvaliderLabel === "Modifier") {
      this.updateValuation();

    }
    else if (this.buttonvaliderLabel === "Valider") {
      this.submit();
      this.updated = false;

    }
  }
  checkIfCollabIsAllowed() {
    let val = {} as ValuationModel;

    this.valuationService.getValuationByIdRequest(this.idValuationRequest).subscribe((res) => {
      val = res;
    }, err => {
      console.error('error in getValuationByIdRequest ', err)
    }, () => {
      //if the valuation is not over the desired amount
      if (Number(val.valuationRequest.financingAmount) <= 2_000_000 && !this.authService.currentUserRolesList.includes('ROLE_SVS_ADMIN_SE')) {
        this.svsDescCollabService.getSvsDescCollab(this.authService.identityClaims['mat']).subscribe(res => {
          //if expert isnt registered in any desc
          if (res == null) {
            this.blockValidation = true;
            this.popupService.popupInfo("Vous n'êtes affilié à aucun réseau, merci de bien vouloir vous renseigner auprès de votre manager.", 'Fermer', null, null, ConfirmationModalComponent);
            this.dataSharing.changeStatus("true");
          }
          //if expert does not have the sufficent amount || is not of the same desc
          else {
            this.mdmInformationService.getMdmCaInformation(val.valuationRequest.bssResponsible).subscribe((data) => {
              val.valuationRequest.networkID = data.idReseau;
            }, (error) => { console.error(error) }, () => {
              if ((val.valuationRequest.networkID != res.desc.id)) {
                this.blockValidation = true;
                this.popupService.popupInfo("Vous n'avez pas les pouvoirs nécessaires pour traiter cette demande de valorisation.", 'Fermer', null, null, ConfirmationModalComponent);
              }
            });
          }
        });
      }
      //valuation is over the setted amount
      else {

        this.blockValidation = (this.valuationRequestStatus == StatusConstant.REJECTED || this.valuationRequestStatus == StatusConstant.EXP_VALIDATED ||
          this.valuationRequestStatus == StatusConstant.FIL_VALIDATED) ? true : false;
      }
      this.dataSharing.changeStatus(String(this.blockValidation));
    });
  }


  ngOnDestroy() {
    if (this.communicationSubscription) {
      this.communicationSubscription.unsubscribe();
    }
    this.subscriptions.forEach(s => s.unsubscribe());

  }
  getValuationEquipmentQualificationToDuplicate() {
    forkJoin([
      this.catNatureService.getNatureListByCatActivitySectorID(this.selectedValuation.catActivitySector.id).pipe(catchError(error => of([]))),
      this.catBrandService.getCatBrandListByCatNatureId(this.selectedValuation.catNature.id).pipe(catchError(error => of([]))),
      this.catAssetTypeService.getAssetTypeListByCatBrandId(this.selectedValuation.catBrand.id).pipe(catchError(error => of([])))

    ]).subscribe(resp => {
      this.natureOptions = resp[0];
      this.brandOptions = resp[1];
      this.assetTypeOptions = resp[2];
    }, err => {
      console.error('error at getValuationEquipmentQualification ', err);
    }, () => {
      this.equipmentForm.controls['materialQualification'].setValue(
        {
          catActivitySector: this.activitySectorOptions.find(element => this.selectedValuation.catActivitySector.id === element.id).label,
          catNature: this.natureOptions.find(element => this.selectedValuation.catNature.id === element.id).label,
          catBrand: this.brandOptions.find(element => this.selectedValuation.catBrand.id === element.id).label,
          catAssetType: this.assetTypeOptions.find(element => this.selectedValuation.catAssetType.id === element.id).label,
          isSubmitted: false

        });
    })
  }
  getValuationEquipentQualificationToUndoDuplication() {
    let valuationQulifList: any[] = []
    if (this.valuationSharedService.getCatActivitySector() && this.valuationSharedService.getCatActivitySector().id)
      valuationQulifList.push(this.catNatureService.getNatureListByCatActivitySectorID(this.valuationSharedService.getCatActivitySector().id).pipe(catchError(error => of([]))),
      )
    if (this.valuationSharedService.getCatNature() && this.valuationSharedService.getCatNature().id)
      valuationQulifList.push(this.catBrandService.getCatBrandListByCatNatureId(this.valuationSharedService.getCatNature().id).pipe(catchError(error => of([]))),
      )
    if (this.valuationSharedService.getCatBrand() && this.valuationSharedService.getCatBrand().id)
      valuationQulifList.push(this.catAssetTypeService.getAssetTypeListByCatBrandId(this.valuationSharedService.getCatBrand().id).pipe(catchError(error => of([]))),
      )
    forkJoin(valuationQulifList).subscribe(resp => {
      this.natureOptions = resp[0];
      this.brandOptions = resp[1];
      this.assetTypeOptions = resp[2];
    }, err => {
      console.error('error at getValuationEquipmentQualification ', err);
    }, () => {
      this.equipmentForm.controls['materialQualification'].setValue(
        {
          catActivitySector: (this.valuationSharedService.getCatActivitySector() && this.valuationSharedService.getCatActivitySector().id) ?
            this.activitySectorOptions.find(element => this.valuationSharedService.getCatActivitySector().id === element.id).label : null,

          catNature: (this.valuationSharedService.getCatNature() && this.valuationSharedService.getCatNature().id) ?
            this.natureOptions.find(element => this.valuationSharedService.getCatNature().id === element.id).label : null,

          catBrand: (this.valuationSharedService.getCatBrand() && this.valuationSharedService.getCatBrand().id) ?
            this.brandOptions.find(element => this.valuationSharedService.getCatBrand().id === element.id).label : null,

          catAssetType: (this.valuationSharedService.getCatAssetType() && this.valuationSharedService.getCatAssetType().id) ?
            this.assetTypeOptions.find(element => this.valuationSharedService.getCatAssetType().id === element.id).label : null,

          isSubmitted: false
        });
      if (this.valuationSharedService.getDiscounts() && this.valuationSharedService.getDiscounts().length > 0) {
        this.submit();

      }
    })
  }
  getValuationEquipmentQualification() {

    forkJoin([
      this.catNatureService.getNatureListByCatActivitySectorID((this.valuationReception.catActivitySector && this.valuationReception.catActivitySector.id ?
        this.valuationReception.catActivitySector.id : null)).pipe(catchError(error => of([]))),
      this.catBrandService.getCatBrandListByCatNatureId((this.valuationReception.catNature && this.valuationReception.catNature.id ?
        this.valuationReception.catNature.id : null)).pipe(catchError(error => of([]))),
      this.catAssetTypeService.getAssetTypeListByCatBrandId((this.valuationReception.catBrand && this.valuationReception.catBrand.id ?
        this.valuationReception.catBrand.id : null)).pipe(catchError(error => of([])))

    ]).subscribe(resp => {
      this.natureOptions = resp[0];
      this.brandOptions = resp[1];
      this.assetTypeOptions = resp[2];
    }, err => {
      console.error('error at getValuationEquipmentQualification ', err);
    }, () => {

      this.equipmentForm.controls['materialQualification'].setValue(
        {
          catActivitySector: this.valuationReception.catActivitySector && this.activitySectorOptions ? this.activitySectorOptions.find(element => this.valuationReception.catActivitySector.id === element.id).label : null,
          catNature: this.valuationReception.catNature && this.natureOptions ? this.natureOptions.find(element => this.valuationReception.catNature.id === element.id).label : null,
          catBrand: this.valuationReception.catBrand && this.brandOptions ? this.brandOptions.find(element => this.valuationReception.catBrand.id === element.id).label : null,
          catAssetType: this.valuationReception.catAssetType && this.assetTypeOptions ? this.assetTypeOptions.find(element => this.valuationReception.catAssetType.id === element.id).label : null,
          isSubmitted: this.valuationReception.isSubmitted
        });
      this.equipmentForm.markAllAsTouched();
      //enable form if draft else disable
      if (!this.checkMaterialQualificationValididty() || !this.validRequestForm || !this.checkIsOldAssetValidity()) {
        this.materialQualificationComponent.loadFromDraft(this.valuationReception.catActivitySector ? this.valuationReception.catActivitySector.label : null,
          this.valuationReception.catNature ? this.valuationReception.catNature.label : null,
          this.valuationReception.catBrand ? this.valuationReception.catBrand.label : null,
          this.valuationReception.catAssetType ? this.valuationReception.catAssetType.label : null)
        this.equipmentForm.markAllAsTouched();
        this.valide = false;
        this.checkButtonValiderLabel();
      }
      else {
        this.valide = true;
        this.checkButtonValiderLabel();
      }
    })
  }

  updateDocuments() {
    this.documents = this.equipmentForm.value.valuationRequest.documentsList;

    let docsToUpdate: ValuationDocumentModel[] = [];
    if (this.equipmentForm.value.valuationRequest.documentsList) {
      this.equipmentForm.value.valuationRequest.documentsList.forEach(doc => {
        if (doc.isModified) {

          const { isModified, ...docToUpdate } = doc;
          if (docToUpdate.docSubType == 'E3031') {
            docToUpdate.docSubType = docToUpdate.docSubType = 'E303';
          } else {
            docToUpdate.isQualified = true;
          }

          docsToUpdate.push(docToUpdate);

        }
      });
    }

    if (docsToUpdate.length > 0) {
      this.valuationRequestDocsService.updateBssRespDocuments(docsToUpdate).subscribe((data) => {
        // This is intentional
      });
    }


  }

  saveValuation(form: any) {
    this.loadingService.show();
    this.valuationService.saveValuation(this.valuationReception).subscribe((data: ValuationModel) => {
      this.valuationService.valuation = data;
      this.valuationReception = data;


      if (form.value.valuationRequest.docsToAdd && form.value.valuationRequest.docsToAdd.length > 0) {

        let requests = [];

        form.value.valuationRequest.docsToAdd.forEach(doc => requests.push(this.valuationRequestDocsService.uploadRequestDocumentFromReception(doc.doc, this.valuationService.valuation.valuationRequest,
          this.entrepriseInfoService.entrepriseInfo,
          doc.docIndex.docFamily, doc.docIndex.docType,
          doc.docIndex.docSubType, false).pipe(catchError(error => of(null)))

        ));

        forkJoin(requests).subscribe((res) => {
          // this is intentional
        }, (err) => console.error(err), () => {
          this.valuationRequestReceptionComponent.valuationRequest = data.valuationRequest;
          this.equipmentForm.get('valuationRequest').patchValue({ docsToAdd: [] });
          this.valuationRequestReceptionComponent.initDocumentList();
          form.get('valuationRequest').patchValue({ docsToAdd: [] });

        })
      }

      // updateDocuments
      this.updateDocuments();
      if (this.receptionSubmitComponent) {

        this.receptionSubmitComponent.receptionDecoteComponent.resetDecote();

      }


      if (this.isAddMode) {
        this.idValuationRequest = data.valuationRequest.id;
        this.isAddMode = false;
        let taskCreate = {} as ValuationRequestWorkflowModel;
        taskCreate.status = this.statusList.find(status => status.value == StatusConstant.STATUS_CREATED);
        taskCreate.valuationRequest = data.valuationRequest;
        taskCreate.variable = {} as WorkflowInstanceVariable;
        taskCreate.variable.expertDesc = this.descInfo;
        taskCreate.variable.administrator = this.adminInfo
        taskCreate.variable.companyName = this.entrepriseInfoService.entrepriseInfo.socialReason;
        this.valuationRequestWorkflowService.saveValuationRequestWorkflow(taskCreate).subscribe(task => {
          let taskWaitExpert = {} as ValuationRequestWorkflowModel;
          taskWaitExpert.status = this.statusList.find(status => status.value == StatusConstant.WAIT_EXP_VAL);
          taskWaitExpert.valuationRequest = data.valuationRequest;
          this.valuationRequestWorkflowService.saveValuationRequestWorkflow(taskWaitExpert).subscribe();
        });
      }
    }, err => {
      console.error('Error saveValuation')
    }, () => {
      this.loadingService.hide();
      if (this.checkMaterialQualificationValididty() && this.validRequestForm && this.checkIsOldAssetValidity()) {
        this.valide = true;
        this.isReset = false;
      }
      else {
        if(this.showPopupDraft){
          this.popupService.popupInfo("Votre valorisation " + this.valuationReception.id + " a été enregistrée en mode brouillon.", 'Fermer', null, null, ConfirmationModalComponent);

        }
        else{
          this.showPopupDraft=false;
        }
      }
      this.checkButtonValiderLabel();


    });
  }

  saveValuationRequestWorkflow(status: ParamSettingModel, comment?: string) {
    if (status) {
      let valuationRequestWorkflow = {} as ValuationRequestWorkflowModel;
      valuationRequestWorkflow.valuationRequest = this.valuationReception.valuationRequest;
      valuationRequestWorkflow.status = status;
      valuationRequestWorkflow.comment = comment;
      switch (status.value) {

        case StatusConstant.WAIT_INFO_EXP_CA: {
          valuationRequestWorkflow.variable = {} as WorkflowInstanceVariable;
          valuationRequestWorkflow.variable.expertDesc = this.descInfo;
          //valuationRequestWorkflow.variable.expertDesc = {mat:'M60976',mail:'M60976@bpifrance.fr',idReseau:'106'} as MdmInformationsModel;

          valuationRequestWorkflow.variable.urlRequest = location.origin + '/#' + this.router.serializeUrl(this.router.createUrlTree(['request', this.valuationRequest.id]));
          valuationRequestWorkflow.variable.urlResponse = location.href;

          break;
        }
        case StatusConstant.WAIT_INFO_EXP_FIL: {
          valuationRequestWorkflow.variable = {} as WorkflowInstanceVariable;
          valuationRequestWorkflow.variable.expertDesc = this.descInfo;
          //valuationRequestWorkflow.variable.expertDesc = {mat:'M60976',mail:'M60976@bpifrance.fr',idReseau:'106'} as MdmInformationsModel;
          valuationRequestWorkflow.variable.urlRequest = location.href.includes('simulation') ? location.origin + '/#' + this.router.serializeUrl(this.router.createUrlTree(['reception', 'expert', this.valuationReception.valuationRequest.id])) : location.href;
          valuationRequestWorkflow.variable.urlResponse = location.href.includes('simulation') ? location.origin + '/#' + this.router.serializeUrl(this.router.createUrlTree(['reception', 'expert', this.valuationReception.valuationRequest.id])) : location.href;

          break;
        }
        case StatusConstant.WAIT_INFO_FIL_EXP: {
          valuationRequestWorkflow.variable = {} as WorkflowInstanceVariable;


          valuationRequestWorkflow.variable.expertDesc = this.expertDesc;
          valuationRequestWorkflow.variable.administrator = this.adminInfo;
          valuationRequestWorkflow.variable.urlRequest = location.href;
          valuationRequestWorkflow.variable.urlResponse = location.href;


          break;
        }
        case StatusConstant.REJECTED: {
          valuationRequestWorkflow.variable = {} as WorkflowInstanceVariable;
          valuationRequestWorkflow.variable.urlRequest = location.origin + '/#' + this.router.serializeUrl(this.router.createUrlTree(['request', this.valuationRequest.id]));
          valuationRequestWorkflow.variable.urlRefused = location.origin + '/#' + this.router.serializeUrl(this.router.createUrlTree(['request', this.valuationRequest.id]));

          break;
        }
      }

      this.valuationRequestWorkflowService.saveValuationRequestWorkflow(valuationRequestWorkflow).subscribe(resp => {
        this.valuationRequestWorkflow = resp;
        this.valuationRequestStatus = resp.status.value

      }, err => {
        console.error('error in ', err)
      }, () => {
        let nextStatus = {} as ParamSettingModel;

        switch (this.valuationRequestWorkflow.status.value) {

          case StatusConstant.WAIT_INFO_EXP_FIL: {
            this.popupService.popupInfo("La demande de renseignements n°" + this.valuationRequest.id + " a été envoyée à la filière.", 'Fermer', null, null, ConfirmationModalComponent);
            break;
          }

          case StatusConstant.WAIT_INFO_FIL_EXP: {
            this.popupService.popupInfo("La demande de renseignements n°" + this.idValuationRequest + " a été envoyée au DESC.", 'Fermer', null, null, ConfirmationModalComponent);
            break;
          }
          case StatusConstant.WAIT_INFO_EXP_CA: {
            this.popupService.popupInfo("La demande de valorisation n°" + this.idValuationRequest + " a été envoyée en complétude auprès du chargé d'affaires.", 'Fermer', null, null, ConfirmationModalComponent);
            const subscription = this.popupService.onConfirm.subscribe((t) => {
              this.router.navigate(['/search'])
              subscription.unsubscribe();

            });
            break;
          }

          case StatusConstant.RESP_INFO_FIL_EXP:
          case StatusConstant.RESP_INFO_CA_EXP: {
            // when it's Information response it should set status to waiting
            nextStatus = this.statusList.find(expectedStatus => expectedStatus.value == StatusConstant.WAIT_EXP_VAL);
            this.saveValuationRequestWorkflow(nextStatus);
            break;
          }
          case StatusConstant.RESP_INFO_EXP_FIL: {
            nextStatus = this.statusList.find(expectedStatus => expectedStatus.value == StatusConstant.WAIT_VALID_FIL);
            this.saveValuationRequestWorkflow(nextStatus);
            break;
          }



          case StatusConstant.REJECTED: {
            this.popupService.popupInfo("La demande de valorisation n° " + this.valuationRequest.id + " a été refusée.", 'Fermer', null, null, ConfirmationModalComponent);
            const subscription = this.popupService.onConfirm.subscribe((t) => {
              this.router.navigate(['/search'])
              subscription.unsubscribe();

            });
            break;

          }

          default:



        }


      });
    } else {
      console.error('error : cannot find required status');
    }
  }


  loadValuationRequestWorkflow() {
    this.valuationRequestWorkflowService.getValuationRequestStatus(this.idValuationRequest).subscribe(data => {
      this.valuationRequestWorkflow = data
    });
  }
  duplicateValuation() {
    if (!this.valuationSharedService.getIsFromDuplicatedReception()) {
      this.prepareValaution();
      this.valuationSharedService.setDocsToAdd(this.equipmentForm.value.valuationRequest.docsToAdd)
      this.valuationSharedService.setIsFromDuplicatedReception(true);
      this.valuationSharedService.setIsFromUndoDuplicatedReception(false);
      this.valuationSharedService.setValuation(Object.assign({}, this.valuationReception))
      this.valuationSharedService.setCatActivitySector(Object.assign({}, this.valuationReception.catActivitySector))
      this.valuationSharedService.setCatNature(Object.assign({}, this.valuationReception.catNature))
      this.valuationSharedService.setCatBrand(Object.assign({}, this.valuationReception.catBrand))
      this.valuationSharedService.setCatAssetType(Object.assign({}, this.valuationReception.catAssetType))
      this.valuationSharedService.setFinancialPlans((this.receptionSubmitComponent && this.receptionSubmitComponent.valuationReceptionPlansComponent && this.receptionSubmitComponent.valuationReceptionPlansComponent.value.financialPlans) ? this.receptionSubmitComponent.prepareFinancialPlans(false) : []);
      if (this.valide && !this.isReset) {
        this.valuationSharedService.setDiscounts((this.receptionSubmitComponent && this.receptionSubmitComponent.receptionDecoteComponent) ? this.receptionSubmitComponent.receptionDecoteComponent.prepareDiscountsForSave(this.receptionSubmitComponent.receptionDecoteComponent.discounts) : []);
        this.valuationSharedService.setDiscountValueChanged((this.receptionSubmitComponent && this.receptionSubmitComponent.receptionDecoteComponent) ? this.receptionSubmitComponent.receptionDecoteComponent.valueChanged : false)
      }
      this.router.navigate(['/search'])
    }
    else {
      this.valuationSharedService.setIsFromDuplicatedReception(false);
      this.valuationSharedService.setIsFromUndoDuplicatedReception(true);
      this.valuationSharedService.setSelectedValuationId(null);
      let valuationRequestId = this.valuationSharedService.getValuation().valuationRequest.id;
      this.router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
        this.router.navigate(['/reception/expert/' + valuationRequestId]));
    }
  }
  getDuplicateButtonName() {
    if (!this.valuationSharedService.getIsFromDuplicatedReception()) {
      return 'Dupliquer'
    }
    else {
      return 'Annuler la duplication'
    }
  }
  saveDraft() {

    if (this.buttonvaliderLabel == "Valider") {
      this.prepareValaution();
      this.equipmentDraftForm.patchValue(this.equipmentForm.value);
      this.saveValuation(this.equipmentDraftForm);

    } else {
      if(this.showPopupDraft){
        this.popupService.popupInfo("Votre valorisation " + this.valuationReception.id + " a été enregistrée en mode brouillon", 'Fermer', null, null, ConfirmationModalComponent);
      }
      else{
        this.showPopupDraft=true;
      }
      this.receptionSubmitComponent.submitDraft(true);
    }
  }

  saveCatRequest(catRequest: any){
    this.catalogAddRequestService.saveCatRequest(catRequest).subscribe(data => {
      let catRequestWorkflow = this.valuationSharedService.getWorkFlowRequest();
      if (this.valuationReception.valuationRequest.isSimulated) {
        catRequestWorkflow.isSimulated = true;
      }
      this.catalogAddRequestWorkflowService.saveCatRequestWorkflow(catRequestWorkflow).subscribe();

    })
  }
}
