import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { threeDigitDecimalRound, twoDigitDecimalRound } from '../../../../../shared/ValuationUtils';
import { FinancingPlanProfileComponent } from './financing-plan-profile.component';

describe('FinancingPlanProfileComponent', () => {
  let fixture: FinancingPlanProfileComponent;
  let formBuilderMock: FormBuilder;
  let subscriptionMock: Subscription;

  beforeEach(() => {
    subscriptionMock = new Subscription();
    formBuilderMock = new FormBuilder();


    fixture = new FinancingPlanProfileComponent(
      formBuilderMock,

    );
    fixture.monthNumber = 24;
    fixture.amount = 1000;
    fixture.ngOnInit();


  });

  describe('Test yearsNumber', () => {
    it('should initialise the number of years from month', () => {
      expect(fixture.yearsNumber).toEqual(2);
    })

  })


  describe('Test: ngOnInit', () => {
    it('should initialize form with two outstandings field', () => {

      const form = {
        planEstimatedOuts: [
          {
            id: null,
            estimatedOutYear: 1,
            estimatedOutPrincRate: null,
            estimatedOutPrincVal: null,
            estimatedCollAssetVal: null,
            potentialLoss: null,
            financialPlan: null,
            createdBy: null,
            createdDate: null,
            updatedBy: null,
            updatedDate: null,
          },
          {
            id: null,
            estimatedOutYear: 2,
            estimatedOutPrincRate: null,
            estimatedOutPrincVal: null,
            estimatedCollAssetVal: null,
            potentialLoss: null,
            financialPlan: null,
            createdBy: null,
            createdDate: null,
            updatedBy: null,
            updatedDate: null,
          }
        ]
      };
      expect(fixture.planEstimatedOutForm.value).toEqual(form);

    });




  });



  describe('Test ngOnDestroy', () => {
    it('should unsubscribe', () => {
      fixture.ngOnDestroy();
      const spyunsubscribe = jest.spyOn(subscriptionMock, 'unsubscribe');
      expect(spyunsubscribe).toBeDefined();
    });
  });


  describe('Test registerOnChange', () => {
    it('should get register', () => {
      let test = (test) => { return test };
      let size = fixture.subscriptions.length;
      fixture.registerOnChange(test);
      expect(fixture.onChange).not.toEqual(() => {
        //this is intentional
      });
      expect(fixture.subscriptions.length).toEqual(size + 1);


    });
  });

  describe('Test registerOnTouched', () => {
    it('should get register', () => {
      fixture.registerOnTouched(test);
      expect(fixture.onTouched).not.toEqual(() => {
        //this is intentional
      });
      expect(fixture.onTouched).toEqual(test);
    });
  });



  describe('Test writeValue', () => {
    it('should get value', () => {
      const value = {
        planEstimatedOuts: [
          {
            id: null,
            estimatedOutYear: 1,
            estimatedOutPrincRate: 4,
            estimatedOutPrincVal: null,
            estimatedCollAssetVal: null,
            potentialLoss: null,
            financialPlan: null,
            createdBy: null,
            createdDate: null,
            updatedBy: null,
            updatedDate: null
          },
          {
            id: null,
            estimatedOutYear: 2,
            estimatedOutPrincRate: 15.6,
            estimatedOutPrincVal: null,
            estimatedCollAssetVal: null,
            potentialLoss: null,
            financialPlan: null,
            createdBy: null,
            createdDate: null,
            updatedBy: null,
            updatedDate: null
          }
        ]
      };

      fixture.writeValue(value);
      expect(fixture.value).toEqual(value)
    });

    it('should not set  value', () => {
      const value = {
        planEstimatedOuts: [
          {
            id: null,
            estimatedOutYear: 1,
            estimatedOutPrincRate: null,
            estimatedOutPrincVal: null,
            estimatedCollAssetVal: null,
            potentialLoss: null,
            financialPlan: null,
            createdBy: null,
            createdDate: null,
            updatedBy: null,
            updatedDate: null
          },
          {
            id: null,
            estimatedOutYear: 2,
            estimatedOutPrincRate: null,
            estimatedOutPrincVal: null,
            estimatedCollAssetVal: null,
            potentialLoss: null,
            financialPlan: null,
            createdBy: null,
            createdDate: null,
            updatedBy: null,
            updatedDate: null
          }
        ]
      };

      fixture.writeValue(value);
      expect(fixture.value).toEqual(value)
      fixture.writeValue(null);
      expect(fixture.value).not.toEqual(value)


    });
  });


  describe('Test round method rule number 2', () => {
    it('detect blur', () => {

      expect(twoDigitDecimalRound("56.678")).toEqual("56.68");
      expect(twoDigitDecimalRound("0.674")).toEqual("0.67");


    })

  })


  describe('Test round method rule number 3', () => {
    it('detect blur', () => {

      expect(threeDigitDecimalRound(56.6786)).toEqual(56.679);
      expect(threeDigitDecimalRound(0.6742)).toEqual(0.674);


    })

  })

  describe('Test on blur rate', () => {
    it('should stay same', () => {
      (fixture.planEstimatedOutForm.get('planEstimatedOuts') as FormArray).at(0).get('estimatedOutPrincRate').setValue(56.8);
      fixture.onBlurOutstandingRate(0);
      expect(fixture.planEstimatedOutForm.value.planEstimatedOuts[0].estimatedOutPrincRate).toEqual(56.8);
    })

    it('should reset', () => {
      (fixture.planEstimatedOutForm.get('planEstimatedOuts') as FormArray).at(0).get('estimatedOutPrincRate').setValue(130);
      fixture.onBlurOutstandingRate(0);
      expect(fixture.planEstimatedOutForm.value.planEstimatedOuts[0].estimatedOutPrincRate).toEqual(null);
    })

    it('should round to major', () => {
      (fixture.planEstimatedOutForm.get('planEstimatedOuts') as FormArray).at(0).get('estimatedOutPrincRate').setValue(0.6786);
      fixture.onBlurOutstandingRate(0);
      expect(fixture.planEstimatedOutForm.value.planEstimatedOuts[0].estimatedOutPrincRate).toEqual(0.679);
      expect(fixture.planEstimatedOutForm.value.planEstimatedOuts[0].estimatedOutPrincVal).toEqual("6.79");

    })

    it('should round to minor', () => {
      (fixture.planEstimatedOutForm.get('planEstimatedOuts') as FormArray).at(0).get('estimatedOutPrincRate').setValue(0.6742);
      fixture.onBlurOutstandingRate(0);
      expect(fixture.planEstimatedOutForm.value.planEstimatedOuts[0].estimatedOutPrincRate).toEqual(0.674);
      expect(fixture.planEstimatedOutForm.value.planEstimatedOuts[0].estimatedOutPrincVal).toEqual("6.74");

    })

    it('should round to zero', () => {
      (fixture.planEstimatedOutForm.get('planEstimatedOuts') as FormArray).at(0).get('estimatedOutPrincRate').setValue(0);
      fixture.onBlurOutstandingRate(0);
      expect(fixture.planEstimatedOutForm.value.planEstimatedOuts[0].estimatedOutPrincRate).toEqual(0);
      expect(fixture.planEstimatedOutForm.value.planEstimatedOuts[0].estimatedOutPrincVal).toEqual("0");

    })

    it('should round to null', () => {
      (fixture.planEstimatedOutForm.get('planEstimatedOuts') as FormArray).at(0).get('estimatedOutPrincRate').setValue(null);
      fixture.onBlurOutstandingRate(0);
      expect(fixture.planEstimatedOutForm.value.planEstimatedOuts[0].estimatedOutPrincRate).toEqual(null);
      expect(fixture.planEstimatedOutForm.value.planEstimatedOuts[0].estimatedOutPrincVal).toEqual(null);

    })



  })

  describe('Test on blur value', () => {
    it('should set null', () => {
      (fixture.planEstimatedOutForm.get('planEstimatedOuts') as FormArray).at(0).get('estimatedOutPrincVal').setValue(null);
      fixture.onBlurOutstandingVal(0);
      expect(fixture.planEstimatedOutForm.value.planEstimatedOuts[0].estimatedOutPrincRate).toEqual(null);

    })


    it('should round to major', () => {
      (fixture.planEstimatedOutForm.get('planEstimatedOuts') as FormArray).at(0).get('estimatedOutPrincVal').setValue("100.6786");
      fixture.onBlurOutstandingVal(0);
      expect(fixture.planEstimatedOutForm.value.planEstimatedOuts[0].estimatedOutPrincVal).toEqual("100.68");
      expect(fixture.planEstimatedOutForm.value.planEstimatedOuts[0].estimatedOutPrincRate).toEqual(10.068);

    })

    it('should round to minor', () => {
      (fixture.planEstimatedOutForm.get('planEstimatedOuts') as FormArray).at(0).get('estimatedOutPrincVal').setValue("100.6742");
      fixture.onBlurOutstandingVal(0);
      expect(fixture.planEstimatedOutForm.value.planEstimatedOuts[0].estimatedOutPrincVal).toEqual("100.67");
      expect(fixture.planEstimatedOutForm.value.planEstimatedOuts[0].estimatedOutPrincRate).toEqual(10.067);

    })

    it('should round to zero', () => {
      (fixture.planEstimatedOutForm.get('planEstimatedOuts') as FormArray).at(0).get('estimatedOutPrincVal').setValue("0");
      fixture.onBlurOutstandingVal(0);
      expect(fixture.planEstimatedOutForm.value.planEstimatedOuts[0].estimatedOutPrincVal).toEqual("0");
      expect(fixture.planEstimatedOutForm.value.planEstimatedOuts[0].estimatedOutPrincRate).toEqual(0);

    })



  })


  describe('Test setDisabledState', () => {
    it('should  disable planEstimatedOutForm', () => {

      fixture.setDisabledState(true);
      expect(fixture.planEstimatedOutForm.disabled).toEqual(true);

    })
    it('should  enable planEstimatedOutForm', () => {

      fixture.setDisabledState(false);
      expect(fixture.planEstimatedOutForm.disabled).toEqual(false);


    })

  })

  describe('Test switch', () => {
    it('should  switch isRate', () => {

      fixture.switch();
      expect(fixture.isRate).toEqual(false);

    })
    it('should  enable planEstimatedOutForm', () => {

      fixture.setDisabledState(false);
      expect(fixture.planEstimatedOutForm.disabled).toEqual(false);


    })

  })


  describe('Test validate', () => {
    it('should retrun null', () => {

      const form = {
        planEstimatedOuts: [
          {
            estimatedOutPrincRate: null,
            estimatedOutYear: 1
          },
          {
            estimatedOutPrincRate: null,
            estimatedOutYear: 2
          }
        ]
      };
      let formControl: FormControl = new FormControl(form);

      fixture.planEstimatedOutForm = new FormGroup({
        planEstimatedOuts: new FormControl(form),
      });
      expect(fixture.validate(formControl)).toEqual(null);

    });

    it('should not retrun null', () => {
      let form: any;
      fixture.planEstimatedOutForm.controls['planEstimatedOuts'].setErrors({ 'incorrect': true });
      expect(fixture.validate(form)).toEqual({ profile: { valid: false, }, });

    });
  });
  describe('Test ngAfterContentInit', () => {
    it('should setControl', () => {



      let array: FormGroup[] = [];
      array.push(new FormGroup({
        id: new FormControl(null),
        estimatedOutYear: new FormControl('1'),
        estimatedOutPrincRate: new FormControl('1'),
        estimatedOutPrincVal: new FormControl('1'),
        estimatedCollAssetVal: new FormControl(null),
        potentialLoss: new FormControl(null),
        financialPlan: new FormControl(null),
        createdBy: new FormControl(null),
        createdDate: new FormControl(null),
        updatedBy: new FormControl(null),
        updatedDate: new FormControl(null),
      }));
      jest.spyOn(fixture, 'initiateFormArray').mockReturnValue(array);
      fixture.ngAfterContentInit();
      expect(fixture.planEstimatedOutForm.get('planEstimatedOuts').value[0]).toEqual(array[0].value);

    });
  });

  describe('Test calculateYearNumberFromMonthNumber', () => {
    it('should yearsNumber equal to 1 without round ', () => {

      fixture.monthNumber = 12;
      fixture.calculateYearNumberFromMonthNumber();
      expect(fixture.yearsNumber).toEqual(1);

    });

    it('should yearsNumber equal to 2 with round ', () => {

      fixture.monthNumber = 11;
      fixture.calculateYearNumberFromMonthNumber();
      expect(fixture.yearsNumber).toEqual(1);

    });
  });
  describe('Test submited', () => {
    it('should change value ', () => {

      fixture.submited = true;
      expect(fixture.isSubmited).toBeTruthy();
    });
  });

  describe('Test initiateFormArray', () => {
    it('should set all value null ', () => {

      fixture.submited = true;
      fixture.planEstimatedOut = [{
        id: null,
        estimatedOutYear: null,
        estimatedOutPrincRate: null,
        estimatedOutPrincVal: null,
        estimatedCollAssetVal: null,
        potentialLoss: null,
        financialPlan: null,
        createdBy: null,
        createdDate: null,
        updatedBy: null,
        updatedDate: null
      }]
      let array: FormGroup[] = [];
      array.push(new FormGroup({
        id: new FormControl(null),
        estimatedOutYear: new FormControl(1),
        estimatedOutPrincRate: new FormControl(null),
        estimatedOutPrincVal: new FormControl("0"),
        estimatedCollAssetVal: new FormControl(null),
        potentialLoss: new FormControl(null),
        financialPlan: new FormControl(null),
        createdBy: new FormControl(null),
        createdDate: new FormControl(null),
        updatedBy: new FormControl(null),
        updatedDate: new FormControl(null),
      }));
      fixture.yearsNumber=1;
      expect(fixture.initiateFormArray()[0].getRawValue()).toEqual(array[0].value);
    });

    it('should set all value not null ', () => {

      fixture.submited = true;
      fixture.planEstimatedOut = [{
        id: '5',
        estimatedOutYear: '5',
        estimatedOutPrincRate: '5',
        estimatedOutPrincVal: '5',
        estimatedCollAssetVal: '5',
        potentialLoss: '5',
        financialPlan: '5',
        createdBy: '5',
        createdDate: '5',
        updatedBy: '5',
        updatedDate: '5'
      }]
      let array: FormGroup[] = [];
      array.push(new FormGroup({
        id: new FormControl('5'),
        estimatedOutYear: new FormControl(1),
        estimatedOutPrincRate: new FormControl('5'),
        estimatedOutPrincVal: new FormControl('5'),
        estimatedCollAssetVal: new FormControl('5'),
        potentialLoss: new FormControl('5'),
        financialPlan: new FormControl('5'),
        createdBy: new FormControl('5'),
        createdDate: new FormControl('5'),
        updatedBy: new FormControl('5'),
        updatedDate: new FormControl('5'),
      }));
      fixture.yearsNumber=1;
      expect(fixture.initiateFormArray()[0].getRawValue()).toEqual(array[0].value);
    });
  });



});
