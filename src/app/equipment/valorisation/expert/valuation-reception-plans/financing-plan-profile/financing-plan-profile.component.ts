import { Component, forwardRef, Input, OnDestroy, OnInit } from '@angular/core';
import { ControlValueAccessor, FormArray, FormBuilder, FormControl, FormGroup, NG_VALIDATORS, NG_VALUE_ACCESSOR, Validator, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { AmortizationProfileConstants } from '../../../../../shared/param-amortization-profile';
import { threeDigitDecimalRound, twoDigitDecimalRound } from '../../../../../shared/ValuationUtils';

@Component({
  selector: 'app-financing-plan-profile',
  templateUrl: './financing-plan-profile.component.html',
  styleUrls: ['./financing-plan-profile.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => FinancingPlanProfileComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => FinancingPlanProfileComponent),
      multi: true
    }
  ]
})
export class FinancingPlanProfileComponent implements OnInit, ControlValueAccessor, OnDestroy, Validator {

  @Input() monthNumber: number;
  @Input() planEstimatedOut:any;
  @Input() amortizationProfileId:any;
  @Input() amount:number;



  yearsNumber: number;
  planEstimatedOutForm: FormGroup;
  subscriptions: Subscription[] = [];
  isSubmited: boolean;
  isRate = true;
  @Input() set submited(value: boolean) {
    this.isSubmited = value;
    if (this.isSubmited && this.planEstimatedOutForm!=null && this.planEstimatedOutForm!=undefined) {
      this.planEstimatedOutForm.markAllAsTouched();

    }

  }


  get value() {

    return this.planEstimatedOutForm.getRawValue();
  }

  set value(value) {
    this.planEstimatedOutForm.setValue(value);
    this.onChange(value);
    this.onTouched();
  }

  get f() {
    return this.planEstimatedOutForm.controls;
  }


  get planEstimatedOuts() {
    return (this.planEstimatedOutForm.controls["planEstimatedOuts"] as FormArray).controls;
  }
  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {

    this.calculateYearNumberFromMonthNumber();
    this.initiateForm();
  }

  ngAfterContentInit() {
    this.planEstimatedOutForm.setControl('planEstimatedOuts', this.fb.array([...this.initiateFormArray()]));


}




  initiateForm() {
    this.planEstimatedOutForm = this.fb.group({ planEstimatedOuts: this.fb.array([...this.initiateFormArray()]) });
    this.subscriptions.push(
      this.planEstimatedOutForm.valueChanges.subscribe(value => {
        this.onTouched();
      })
    );

  }

  initiateFormArray(): FormGroup[] {

    let formArray: FormGroup[] = [];
    for (let i = 0; i < this.yearsNumber; i++) {

      const planEstimatedOutForm = this.fb.group({
        estimatedOutPrincRate :[{value :(this.planEstimatedOut!=null && this.planEstimatedOut[i]!=null) ? this.planEstimatedOut[i].estimatedOutPrincRate:null , disabled : this.amortizationProfileId && this.amortizationProfileId != AmortizationProfileConstants.PLAN_LIBRE_ID}, [Validators.required,Validators.min(0),Validators.max(100)]],
        estimatedOutYear: [i + 1],
        id: [this.planEstimatedOut!=null && this.planEstimatedOut[i] !=null && this.planEstimatedOut[i].id!=null ?this.planEstimatedOut[i].id:null],
        estimatedOutPrincVal: [{value : (this.planEstimatedOut!=null && this.planEstimatedOut[i]!=null) ? this.initEstimatedOutPrincVal(this.planEstimatedOut[i]) : null , disabled : this.amortizationProfileId && this.amortizationProfileId != AmortizationProfileConstants.PLAN_LIBRE_ID },[Validators.required,Validators.min(0),Validators.max(this.amount)]],
        potentialLoss:[this.planEstimatedOut!=null && this.planEstimatedOut[i] !=null && this.planEstimatedOut[i].potentialLoss!=null ?this.planEstimatedOut[i].potentialLoss:null],
        financialPlan:[this.planEstimatedOut!=null && this.planEstimatedOut[i] !=null && this.planEstimatedOut[i].financialPlan!=null ?this.planEstimatedOut[i].financialPlan:null],
        estimatedCollAssetVal:[this.planEstimatedOut!=null && this.planEstimatedOut[i] !=null&& this.planEstimatedOut[i].estimatedCollAssetVal!=null ?this.planEstimatedOut[i].estimatedCollAssetVal:null],
        updatedDate:[this.planEstimatedOut!=null && this.planEstimatedOut[i] !=null && this.planEstimatedOut[i].updatedDate!=null ?this.planEstimatedOut[i].updatedDate:null],
        createdDate:[this.planEstimatedOut!=null && this.planEstimatedOut[i] !=null && this.planEstimatedOut[i].createdDate!=null ?this.planEstimatedOut[i].createdDate:null],
        createdBy:[this.planEstimatedOut!=null && this.planEstimatedOut[i] !=null && this.planEstimatedOut[i].createdBy!=null ?this.planEstimatedOut[i].createdBy:null],
        updatedBy:[this.planEstimatedOut!=null && this.planEstimatedOut[i] !=null && this.planEstimatedOut[i].updatedBy!=null ?this.planEstimatedOut[i].updatedBy:null],
      });


      formArray.push(planEstimatedOutForm);
    }


    return formArray;

  }

  initEstimatedOutPrincVal(planEstimatedOut){

   return   planEstimatedOut.estimatedOutPrincVal ? twoDigitDecimalRound(planEstimatedOut.estimatedOutPrincVal.toString()):this.calculateValueFromRate( this.planEstimatedOut.estimatedOutPrincRate )
  }

  calculateValueFromRate(rate) : string{


    if(rate || rate == 0){

    return twoDigitDecimalRound((this.amount * rate/100).toString());
    }
    return "0";
  }
  calculateRateFromValue(value): number{
    if(value || value ==0){
    return threeDigitDecimalRound( 100 -  ((this.amount - value) /this.amount) * 100);
    }
    return null


  }

  switch(){
    this.isRate = !this.isRate;
  }


  onBlurOutstandingRate(i) {

    if (this.planEstimatedOuts[i].value.estimatedOutPrincRate || this.planEstimatedOuts[i].value.estimatedOutPrincRate == 0) {

      if (this.planEstimatedOuts[i].value.estimatedOutPrincRate < 0 || this.planEstimatedOuts[i].value.estimatedOutPrincRate > 100) {
        this.planEstimatedOuts[i].get('estimatedOutPrincRate').setValue(null);
        this.planEstimatedOuts[i].get('estimatedOutPrincVal').setValue(null);

      } else {
        this.planEstimatedOuts[i].get('estimatedOutPrincRate').setValue(threeDigitDecimalRound(this.planEstimatedOuts[i].value.estimatedOutPrincRate));


        this.planEstimatedOuts[i].get('estimatedOutPrincVal').setValue( this.calculateValueFromRate( threeDigitDecimalRound(this.planEstimatedOuts[i].value.estimatedOutPrincRate)));

      }
    } else{
      this.planEstimatedOuts[i].get('estimatedOutPrincVal').setValue(null);

    }




  }

  onBlurOutstandingVal(i) {

    if (this.planEstimatedOuts[i].value.estimatedOutPrincVal || this.planEstimatedOuts[i].value.estimatedOutPrincVal == 0 ) {


        this.planEstimatedOuts[i].get('estimatedOutPrincVal').setValue(twoDigitDecimalRound(this.planEstimatedOuts[i].value.estimatedOutPrincVal))
        this.planEstimatedOuts[i].get('estimatedOutPrincRate').setValue( this.calculateRateFromValue(twoDigitDecimalRound(this.planEstimatedOuts[i].value.estimatedOutPrincVal)));

    }else{
      this.planEstimatedOuts[i].get('estimatedOutPrincRate').setValue(null);

    }




  }

  calculateYearNumberFromMonthNumber() {
    this.yearsNumber = Math.floor(this.monthNumber / 12) + (this.monthNumber % 12 != 0 ? 1 : 0);

  }


  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  onChange: any = () => {
    // This is intentional
   };
  onTouched: any = () => {
    // This is intentional
    };

  registerOnChange(fn) {
    this.subscriptions.push(    this.planEstimatedOutForm.valueChanges.pipe(map(_ => this.planEstimatedOutForm.getRawValue())).subscribe(fn)
  );
  }

  checkAllValueNull(value): boolean {
    let isNull = false;
    value.planEstimatedOuts.forEach(val => isNull = isNull || val.planEstimatedOut || val.estimatedOutYear);

    return isNull;
  }

  writeValue(value) {

    if (value && value.planEstimatedOuts && this.checkAllValueNull(value)) {
      this.planEstimatedOutForm.setValue(value);
    }

    if (value === null) {
      this.planEstimatedOutForm.reset();
    }
  }

  registerOnTouched(fn) {
    this.onTouched = fn;
  }

  setDisabledState(disabled: boolean) {

    if (disabled) {
      this.planEstimatedOutForm.disable();
    }
    else {
      this.planEstimatedOutForm.enable();
    }


  }




  validate(_: FormControl) {

    return this.planEstimatedOutForm.valid ? null : { profile: { valid: false, }, };
  }

}
