
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { of, Subscription } from 'rxjs';
import { FinancialPlanModel } from '../../../../shared/models/financial-plan.model';
import { ParamAmortizationProfileModel } from '../../../../shared/models/param-amortization-profile.model';
import { ParamProductFamilyModel } from '../../../../shared/models/param-product-family.model';
import { PlanEstimatedOutModel } from '../../../../shared/models/plan-estimated-out.model';
import { ValuationRequestModel } from '../../../../shared/models/valuation-request.model';
import { ValuationModel } from '../../../../shared/models/valuation.model';
import { AmortizationProfileConstants } from '../../../../shared/param-amortization-profile';
import { ValuationReceptionPlansComponent } from './valuation-reception-plans.component';
import { ValuationSharedService } from '../../../../shared/services/valuation-shared-service';
import { TestBed } from '@angular/core/testing';
import { ParamProductFamilyConstants } from 'src/app/shared/param-product-family-constants';




describe('ValuationReceptionPlansComponent', () => {
  let fixture: ValuationReceptionPlansComponent;
  let popupServiceMock: any;
  let formBuilderMock: FormBuilder;
  let routerMock: any;
  let subscriptionMock: Subscription;

  let financialPlanServiceMock: any;
  let paramProductFamilyServiceMock: any;
  let paramAmortizationProfileServiceMock: any;
  let valuationSharedServiceMock:any
  let financialPlanModel: FinancialPlanModel
  let valuationRequestModel: ValuationRequestModel = {id:'1'} as ValuationRequestModel;
  let valuation: ValuationModel = {valuationRequest: valuationRequestModel } as ValuationModel;
  let paramAmortizationProfile: ParamAmortizationProfileModel
  let paramProductFamily: ParamProductFamilyModel = {} as ParamProductFamilyModel;

  beforeEach(() => {
    valuationSharedServiceMock = TestBed.inject(ValuationSharedService);

    valuation.id = '1';
    subscriptionMock = new Subscription();

    valuationRequestModel.financingAmount = 50;
    paramAmortizationProfile = {} as ParamAmortizationProfileModel;
    paramAmortizationProfile.id = '1';
    paramProductFamily.id = '2';
    paramProductFamily.paramAmortizationProfiles = []
    financialPlanModel = {
      id: null,
      planDuration: 1,
      planContrbAmount: 1,
      paramAmortizationProfile: paramAmortizationProfile,
      paramProductFamily:paramProductFamily,
      collateralAssetRating: null,
      status: null,
      valuation: valuation,
      valuationRequest: valuationRequestModel,
      remainingAmount: null,
      remainingPercent: null,
      planEstimatedOut: null,
      isDuplicated : null
    };

    popupServiceMock = {
      popupInfo: jest.fn().mockReturnValue(of(true)),
      onConfirm: of(true)
    };
    financialPlanServiceMock = {
      loadFinancialPlanByValuationRequestId: jest.fn().mockReturnValue(of([{ valuationRequest: valuationRequestModel } as FinancialPlanModel])),
      loadFinancialPlanByValuationId: jest.fn().mockReturnValue(of([{ id: '1', valuationRequest: valuationRequestModel } as FinancialPlanModel])),
      planEstimatedOutRateWithFinancialProcessing: jest.fn().mockReturnValue(of([
        { estimatedOutYear:1 , estimatedOutPrincRate:80 } as PlanEstimatedOutModel,
        { estimatedOutYear:2 , estimatedOutPrincRate:60 } as PlanEstimatedOutModel,
      ])),
      planEstimatedOutRatePreCalculated: jest.fn().mockReturnValue(of([
        { estimatedOutYear:1 , estimatedOutPrincRate:80 } as PlanEstimatedOutModel,
        { estimatedOutYear:2 , estimatedOutPrincRate:60 } as PlanEstimatedOutModel,
      ])),
      removePlanEstimatedOut: jest.fn().mockReturnValue(of(true)),
    }

    paramProductFamilyServiceMock = {
      getAllParamProductFamily: jest.fn().mockReturnValue(of([{ id: '1', label: 'Prêt' ,paramAmortizationProfiles:null, code: ParamProductFamilyConstants.PRET_CODE},
      { id: '2', label: 'Location' ,paramAmortizationProfiles:null, code: ParamProductFamilyConstants.LOCATION_CODE},
      { id: '3', label: 'Crédit-bail' ,paramAmortizationProfiles:null, code: ParamProductFamilyConstants.CREDIT_BAIL_CODE}])),
    }
    paramAmortizationProfileServiceMock = {
      getAllParamAmortizationProfile: jest.fn().mockReturnValue(of([
        { id: '1', label: 'Loyer ou échéance constant(e)', paramProductFamily: { id: '1', label: 'Prêt',paramAmortizationProfiles:null  } },
        { id: '2', label: 'Non-linéaire', paramProductFamily: { id: '2', label: 'Location' ,paramAmortizationProfiles:null } },
        { id: '3', label: 'Linéaire', paramProductFamily: { id: '3', label: 'Crédit-bail' ,paramAmortizationProfiles:null } },
      ]))

    }

    formBuilderMock = new FormBuilder();
    routerMock = jest.fn();
    routerMock = {
      snapshot: {
        paramMap: {
          get: jest.fn().mockReturnValue('1'),
          id: '525'
        },
      }

    };

    fixture = new ValuationReceptionPlansComponent(
      formBuilderMock,
      routerMock,
      popupServiceMock,
      financialPlanServiceMock,
      paramProductFamilyServiceMock,
      paramAmortizationProfileServiceMock,
      valuationSharedServiceMock
    );

    fixture.valuation = valuation;


    fixture.ngOnInit();
  });

  describe('Test: ngOnInit', () => {

    it('should initialise form', () => {
      let array: FormGroup[] = [];
      array.push(new FormGroup({
        planDuration: new FormControl('5'),
        planContrbAmount: new FormControl('5'),
        amortizationProfileId: new FormControl('1'),
        paramProductFamilyId: new FormControl('1'),
        remainingAmount: new FormControl('5'),
        outstandings: new FormControl(null)
      }));
      jest.spyOn(fixture, 'initialiseFinancialPlans').mockReturnValue(array);

      fixture.ngOnInit();
      expect(fixture.form.get('financialPlans').value[0]).toEqual(array[0].value);

    });

    it('should from duplicated', () => {
      let array: FormGroup[] = [];
      array.push(new FormGroup({
        planDuration: new FormControl('5'),
        planContrbAmount: new FormControl('5'),
        amortizationProfileId: new FormControl('1'),
        paramProductFamilyId: new FormControl('1'),
        remainingAmount: new FormControl('5'),
        outstandings: new FormControl(null)
      }));
      jest.spyOn(fixture, 'initialiseFinancialPlans').mockReturnValue(array);
      jest.spyOn(fixture.valuationSharedService, 'getIsFromDuplicatedReception').mockReturnValue(true);
      jest.spyOn(fixture.valuationSharedService, 'getFinancialPlans').mockReturnValue(array);

      fixture.ngOnInit();
      expect(fixture.form.get('financialPlans').value[0]).toEqual(array[0].value);

    });

    it('should from duplicated CASE WHEN DUPLICATING', () => {
      let array: FormGroup[] = [];
      array.push(new FormGroup({
        planDuration: new FormControl('5'),
        planContrbAmount: new FormControl('5'),
        amortizationProfileId: new FormControl('1'),
        paramProductFamilyId: new FormControl('1'),
        remainingAmount: new FormControl('5'),
        outstandings: new FormControl(null)
      }));
      jest.spyOn(fixture, 'initialiseFinancialPlans').mockReturnValue(array);
      jest.spyOn(fixture.valuationSharedService, 'getIsFromDuplicatedReception').mockReturnValue(true);
      jest.spyOn(fixture.valuationSharedService, 'getIsFromUndoDuplicatedReception').mockReturnValue(false);
      jest.spyOn(fixture.valuationSharedService, 'getFinancialPlans').mockReturnValue(array);
      financialPlanServiceMock = {
        loadFinancialPlanByValuationId: jest.fn().mockReturnValue(of(array)),
      }



      fixture.ngOnInit();
      expect(fixture.form.get('financialPlans').value[0]).toEqual(array[0].value);

    });

    it('should from UNDO', () => {
      let array: FormGroup[] = [];
      array.push(new FormGroup({
        planDuration: new FormControl('5'),
        planContrbAmount: new FormControl('5'),
        amortizationProfileId: new FormControl('1'),
        paramProductFamilyId: new FormControl('1'),
        remainingAmount: new FormControl('5'),
        outstandings: new FormControl(null)
      }));
      jest.spyOn(fixture.valuationSharedService, 'getIsFromUndoDuplicatedReception').mockReturnValue(true);
      jest.spyOn(fixture, 'initialiseFinancialPlans').mockReturnValue(array);

      fixture.ngOnInit();
      expect(fixture.form.get('financialPlans').value[0]).toEqual(array[0].value);

    });

    it('should fill productOptions ', () => {
      expect(fixture.productOptions).toEqual([{ id: '1', label: 'Prêt' ,paramAmortizationProfiles:null, code: ParamProductFamilyConstants.PRET_CODE },
      { id: '2', label: 'Location',paramAmortizationProfiles:null, code: ParamProductFamilyConstants.LOCATION_CODE },
      { id: '3', label: 'Crédit-bail' ,paramAmortizationProfiles:null, code: ParamProductFamilyConstants.CREDIT_BAIL_CODE }])
    });


    it('should fill allProfilOptions  ', () => {
      expect(fixture.allProfilOptions).toEqual([
        { id: '1', label: 'Loyer ou échéance constant(e)', paramProductFamily: { id: '1', label: 'Prêt',paramAmortizationProfiles:null  }as ParamProductFamilyModel },
        { id: '2', label: 'Non-linéaire', paramProductFamily: { id: '2', label: 'Location',paramAmortizationProfiles:null  }as ParamProductFamilyModel },
        { id: '3', label: 'Linéaire', paramProductFamily: { id: '3', label: 'Crédit-bail' ,paramAmortizationProfiles:null }as ParamProductFamilyModel },
      ])
    });

    it('should fill financialPlans   ', () => {
      expect(fixture.financialPlans).toEqual([{ id: '1', valuationRequest: valuationRequestModel } as FinancialPlanModel])
    });

    it('should create financialPlans   from request', () => {
      financialPlanServiceMock = {
        loadFinancialPlanByValuationRequestId: jest.fn().mockReturnValue(of([{ id: '1', valuationRequest: valuationRequestModel } as FinancialPlanModel])),
        loadFinancialPlanByValuationId: jest.fn().mockReturnValue(of([])),
        removePlanEstimatedOut: jest.fn().mockReturnValue(of(true)),
      }
      fixture = new ValuationReceptionPlansComponent(
        formBuilderMock,
        routerMock,
        popupServiceMock,
        financialPlanServiceMock,
        paramProductFamilyServiceMock,
        paramAmortizationProfileServiceMock,
        valuationSharedServiceMock
      );

      fixture.valuation = valuation;
      let array: FormGroup[] = [];
      array.push(new FormGroup({
        planDuration: new FormControl('5'),
        planContrbAmount: new FormControl('5'),
        amortizationProfileId: new FormControl('1'),
        paramProductFamilyId: new FormControl('1'),
        remainingAmount: new FormControl('5'),
        outstandings: new FormControl(null)
      }));
      jest.spyOn(fixture, 'initialiseFinancialPlans').mockReturnValue(array);

      fixture.ngOnInit();

      expect(fixture.financialPlans).toEqual([{ id: null, valuation: valuation, valuationRequest: valuationRequestModel } as FinancialPlanModel])


      expect(fixture.form.get('financialPlans').value[0]).toEqual(array[0].value);
    });


  });


  describe('Test initialiseFinancialPlans', () => {
    it('should return FormGroup array', () => {

      let arr: FormGroup[] = [];
      let plans: FinancialPlanModel[] = [];

      const financialPlanModel: FinancialPlanModel = {
        id: null,
        planDuration: 1,
        planContrbAmount: 1,
        paramAmortizationProfile: paramAmortizationProfile,
        paramProductFamily:paramProductFamily,
        collateralAssetRating: null,
        status: null,
        valuation: valuation,
        valuationRequest: valuationRequestModel,
        remainingAmount: null,
        remainingPercent: 5,
        planEstimatedOut: null,
        isDuplicated : null
      };

      plans.push(financialPlanModel);
      arr = fixture.initialiseFinancialPlans(plans)
      expect(arr.length).toEqual(1)
    });
    it('should set isResidualValueValid to false', () => {

      let plans: FinancialPlanModel[] = [];
      let paramProductFamily: ParamProductFamilyModel = {} as ParamProductFamilyModel;
      paramProductFamily.id = '1';
      paramAmortizationProfile.paramProductFamily = paramProductFamily;
      const financialPlanModel: FinancialPlanModel = {
        id: null,
        planDuration: 1,
        planContrbAmount: 1,
        paramAmortizationProfile: paramAmortizationProfile,
        paramProductFamily:paramProductFamily,

        collateralAssetRating: null,
        status: null,
        valuation: valuation,
        valuationRequest: valuationRequestModel,
        remainingAmount: null,
        remainingPercent: 5,
        planEstimatedOut: null,
        isDuplicated : true,
      };

      plans.push(financialPlanModel);
      fixture.initialiseFinancialPlans(plans)
      expect(fixture.isRemainingPercentValid[0]).toEqual(false)
    });
    it('should set isResidualValueValid to true', () => {

      let plans: FinancialPlanModel[] = [];

      const financialPlanModel: FinancialPlanModel = {
        id: null,
        planDuration: 1,
        planContrbAmount: 1,
        paramAmortizationProfile: paramAmortizationProfile,
        paramProductFamily:paramProductFamily,
        collateralAssetRating: null,
        status: null,
        valuation: valuation,
        valuationRequest: valuationRequestModel,
        remainingAmount: null,
        remainingPercent: 5,
        planEstimatedOut: null,
        isDuplicated : null ,
      };

      plans.push(financialPlanModel);
      fixture.initialiseFinancialPlans(plans)
      expect(fixture.isRemainingPercentValid[0]).toEqual(true)
    });


  });
  describe('TestngAfterViewInit', () => {
    it('should call methode', () => {
      fixture.adjustPlanTabsGrid = jest.fn();
      fixture.ngAfterViewInit();
      expect(fixture.adjustPlanTabsGrid).toHaveBeenCalled();

    });
  });




  describe('Test onChangeSelect', () => {
    let array: FormGroup[] = [];
    array.push(new FormGroup({
      planDuration: new FormControl('5'),
      planContrbAmount: new FormControl('5'),
      amortizationProfileId: new FormControl('1'),
      paramProductFamilyId: new FormControl('1'),
      remainingAmount: new FormControl('5'),
      planEstimatedOut: new FormControl(null),
      remainingPercent: new FormControl('5')
    }));
    it('should add validator required if iDRental) || idFurnitureLease', () => {



      jest.spyOn(fixture, 'initialiseFinancialPlans').mockReturnValue(array);

      fixture.ngOnInit();
      fixture.selectedPlan = 0;
      (fixture.form.controls['financialPlans'] as FormArray).at(0).get('remainingPercent').setValue('');
      expect((fixture.form.controls['financialPlans'] as FormArray).at(0).get('remainingPercent').hasError('required')).toBeFalsy();
      fixture.onChangeSelect(2);
      expect((fixture.form.controls['financialPlans'] as FormArray).at(0).get('remainingPercent').hasError('required')).toBeTruthy();
      expect(fixture.plansControls[0].get('amortizationProfileId').value).toEqual(null);
    });

    it('should delete validator required if idLoan', () => {


      jest.spyOn(fixture, 'initialiseFinancialPlans').mockReturnValue(array);
      fixture.ngOnInit();

      fixture.onChangeSelect(1);
      expect((fixture.form.controls['financialPlans'] as FormArray).at(0).get('remainingPercent').hasError('required')).toBeFalsy();

    });

  });

  describe('Test registerOnChange', () => {
    it('should register ', () => {

      let test;
      fixture.registerOnChange(test);

      expect(fixture.onChangeSub).toBeDefined();
    })
  });


  describe('Test ngOnDestroy', () => {
    it('should unsubscribe', () => {
      let test;
      fixture.registerOnChange(test);
      fixture.ngOnDestroy();
      const spyunsubscribe = jest.spyOn(subscriptionMock, 'unsubscribe');
      expect(spyunsubscribe).toBeDefined();
    });
  });

  describe('Test writeValue', () => {
    it('should  write value', () => {
      let value = ({
        financialPlans: []
      });


      fixture.writeValue(value);

      expect(fixture.value).toEqual(value);



    });
    it('should not write value', () => {
      let value = ({
        financialPlans: []
      });

      fixture.writeValue(value);
      value = null;

      fixture.writeValue(value);
      expect(fixture.value).not.toEqual(value);


    });
  });

  describe('Test registerOnTouched', () => {
    it('should get register', () => {
      fixture.registerOnTouched(test);
      expect(fixture.onTouched).not.toEqual(() => { });
      expect(fixture.onTouched).toEqual(test);
    });

  });

  describe('Test setDisabledState', () => {
    it('should  disable form', () => {

      fixture.setDisabledState(true);
      expect(fixture.form.disabled).toEqual(true);

    })
    it('should  enable form', () => {

      fixture.setDisabledState(false);
      expect(fixture.form.disabled).toEqual(false);


    })

  })

  describe('Test validate', () => {
    it('should retrun null', () => {

      let formControl: FormControl = new FormControl();

      expect(fixture.validate(formControl)).toEqual(null);

    });

    it('should not retrun null', () => {
      let form: any;
      fixture.form.controls['financialPlans'].setErrors({ 'incorrect': true });
      expect(fixture.validate(form)).toEqual({ profile: { valid: false, }, });

    });
  });

  describe('Test changeTab', () => {
    it('should  change tab', () => {
      let array: FormGroup[] = [];
      array.push(new FormGroup({
        planDuration: new FormControl('5'),
        planContrbAmount: new FormControl('5'),
        amortizationProfileId: new FormControl('1'),
        paramProductFamilyId: new FormControl('1'),
        remainingAmount: new FormControl('5'),
        planEstimatedOut: new FormControl(null),
        remainingPercent: new FormControl('5')
      }));
      array.push(new FormGroup({
        planDuration: new FormControl('5'),
        planContrbAmount: new FormControl('5'),
        amortizationProfileId: new FormControl('1'),
        paramProductFamilyId: new FormControl('1'),
        remainingAmount: new FormControl('5'),
        planEstimatedOut: new FormControl(null),
        remainingPercent: new FormControl('5')
      }));
      array.push(new FormGroup({
        planDuration: new FormControl('5'),
        planContrbAmount: new FormControl('5'),
        amortizationProfileId: new FormControl('1'),
        paramProductFamilyId: new FormControl(null),
        remainingAmount: new FormControl('5'),
        planEstimatedOut: new FormControl(null),
        remainingPercent: new FormControl('5')
      }));


      jest.spyOn(fixture, 'initialiseFinancialPlans').mockReturnValue(array);
      fixture.ngOnInit();
      fixture.selectedPlan = 0;
      fixture.isPlansValid = [false, false];
      fixture.financialPlans = [{
        id: '5',
        planDuration: 1,
        planContrbAmount: 1,
        paramAmortizationProfile: paramAmortizationProfile,
        paramProductFamily:paramProductFamily,
        collateralAssetRating: null,
        status: null,
        valuation: valuation,
        valuationRequest: valuationRequestModel,
        remainingAmount: null,
        remainingPercent: 5,
        planEstimatedOut: null,
        isDuplicated : false,
      },
      {
        id: '5',
        planDuration: 1,
        planContrbAmount: 1,
        paramAmortizationProfile: paramAmortizationProfile,
        paramProductFamily:paramProductFamily,
        collateralAssetRating: null,
        status: null,
        valuation: valuation,
        valuationRequest: valuationRequestModel,
        remainingAmount: null,
        remainingPercent: 5,
        planEstimatedOut: null,
        isDuplicated : true,
      }]
      fixture.changeTab(1);
      expect(fixture.selectedPlan).toEqual(0);
      expect(popupServiceMock.popupInfo).toHaveBeenCalled();

      fixture.isPlansValid = [true, false];

      fixture.changeTab(1);
      expect(fixture.selectedPlan).toEqual(1);
      fixture.changeTab(2);
      expect(fixture.profilOptions).toEqual([]);



    });
  });

  describe('Test isPlansControlValid', () => {
    it('should  chould return true when plans valid other wise false', () => {
      let array: FormGroup[] = [];
      array.push(new FormGroup({
        planDuration: new FormControl('5'),
        planContrbAmount: new FormControl('5'),
        amortizationProfileId: new FormControl('1'),
        paramProductFamilyId: new FormControl('1'),
        remainingAmount: new FormControl('5'),
        planEstimatedOut: new FormControl(null),
        remainingPercent: new FormControl('5')
      }));

      jest.spyOn(fixture, 'initialiseFinancialPlans').mockReturnValue(array);
      fixture.ngOnInit();
      fixture.selectedPlan = 0;
      fixture.isPlansValid = [true];


      expect(fixture.isPlanControlsValid()).toEqual(true);
      fixture.isPlansValid = [false];
      expect(fixture.isPlanControlsValid()).toEqual(false);
      fixture.isPlansValid = [true];
      (fixture.form.controls['financialPlans'] as FormArray).at(0).setErrors({ planEstimatedOuts: false })
      expect(fixture.isPlanControlsValid()).toEqual(false);

    });
  });

  describe('Add new plan', () => {
    it('should  add new plan when other plans valid', () => {
      let array: FormGroup[] = [];
      array.push(new FormGroup({
        planDuration: new FormControl('5'),
        planContrbAmount: new FormControl('5'),
        amortizationProfileId: new FormControl('1'),
        paramProductFamilyId: new FormControl('1'),
        remainingAmount: new FormControl('5'),
        planEstimatedOut: new FormControl(null),
        remainingPercent: new FormControl('5')
      }));


      jest.spyOn(fixture, 'initialiseFinancialPlans').mockReturnValue(array);
      jest.spyOn(fixture, 'isPlanControlsValid').mockReturnValue(true);
      jest.spyOn(fixture, 'adjustPlanTabsGrid').mockImplementation(() => { })

      fixture.selectedPlan = 0;

      fixture.isPlansValid = [false];
      fixture.addNewFundingPlan();
      expect(fixture.plansControls.length).toEqual(1);


      fixture.isPlansValid = [true];
      fixture.addNewFundingPlan();
      expect(fixture.plansControls.length).toEqual(2);

      fixture.isPlansValid = [true, true];
      fixture.addNewFundingPlan();
      fixture.isPlansValid = [true, true, true];
      fixture.addNewFundingPlan();
      fixture.isPlansValid = [true, true, true, true];
      fixture.addNewFundingPlan();
      fixture.isPlansValid = [true, true, true, true, true];
      fixture.addNewFundingPlan();
      expect(fixture.plansControls.length).toEqual(6);
      fixture.isPlansValid = [true, true, true, true, true, true];
      fixture.addNewFundingPlan();
      expect(fixture.plansControls.length).toEqual(6);





    });

    it('should  not add new plan when other plans is invalid', () => {
      let array: FormGroup[] = [];
      array.push(new FormGroup({
        planDuration: new FormControl('5'),
        planContrbAmount: new FormControl('5'),
        amortizationProfileId: new FormControl('1'),
        paramProductFamilyId: new FormControl('1'),
        remainingAmount: new FormControl('5'),
        planEstimatedOut: new FormControl(null),
        remainingPercent: new FormControl('5')
      }));


      jest.spyOn(fixture, 'initialiseFinancialPlans').mockReturnValue(array);
      jest.spyOn(fixture, 'isPlanControlsValid').mockReturnValue(true);
      jest.spyOn(fixture, 'adjustPlanTabsGrid').mockImplementation(() => { })
      jest.spyOn(fixture, 'isPlanControlsValid').mockReturnValue(false)

      fixture.addNewFundingPlan();
      expect(fixture.plansControls.length).toEqual(0);
      expect(fixture.isSubmited).toEqual(true);


    });
  });

  describe('Test deleteFundingPlan', () => {
    it('should delete plan and not add  to removedListe', () => {

      const financialPlanModel: FinancialPlanModel = {
        id: null,
        planDuration: 1,
        planContrbAmount: 1,
        paramAmortizationProfile: paramAmortizationProfile,
        paramProductFamily:paramProductFamily,
        collateralAssetRating: null,
        status: null,
        valuation: valuation,
        valuationRequest: valuationRequestModel,
        remainingAmount: null,
        remainingPercent: 5,
        planEstimatedOut: null,
        isDuplicated : false,
      };

      fixture.getPlansFormArray().value[0] = financialPlanModel;
      fixture.removedfinancialPlans = [];
      fixture.adjustPlanTabsGrid = jest.fn();

      fixture.deleteFundingPlan(0);

      expect(fixture.removedfinancialPlans.length).toEqual(0)
    });

    it('should delete plan and not add  to removedListe', () => {

      const financialPlanModel: FinancialPlanModel = {
        id: '5',
        planDuration: 1,
        planContrbAmount: 1,
        paramAmortizationProfile: paramAmortizationProfile,
        paramProductFamily:paramProductFamily,
        collateralAssetRating: null,
        status: null,
        valuation: valuation,
        valuationRequest: valuationRequestModel,
        remainingAmount: null,
        remainingPercent: 5,
        planEstimatedOut: null,
        isDuplicated : false ,
      };

      fixture.getPlansFormArray().value[0] = financialPlanModel;
      fixture.removedfinancialPlans = [];
      fixture.adjustPlanTabsGrid = jest.fn();

      fixture.deleteFundingPlan(0);

      expect(fixture.removedfinancialPlans.length).toEqual(1)
    });

  });

  describe('Test validate Plan', () => {
    it('should retrun popup residual value > 10', () => {
      let array: FormGroup[] = [];
      array.push(new FormGroup({
        planDuration: new FormControl('5'),
        planContrbAmount: new FormControl('5'),
        amortizationProfileId: new FormControl('1'),
        paramProductFamilyId: new FormControl('1'),
        remainingAmount: new FormControl('5'),
        planEstimatedOut: new FormControl(null),
        remainingPercent: new FormControl('5')
      }));

      jest.spyOn(fixture, 'initialiseFinancialPlans').mockReturnValue(array);
      fixture.ngOnInit();

      (fixture.form.controls['financialPlans'] as FormArray).at(0).get('remainingPercent').setValue('12');
      (fixture.form.controls['financialPlans'] as FormArray).at(0).get('remainingPercent').setErrors({ 'remainingPercentValidators': false });
      fixture.onValidatePlan(0)
      expect(popupServiceMock.popupInfo).toHaveBeenCalled();


    });

    it('should retrun popup residual value > 100', () => {
      let array: FormGroup[] = [];
      array.push(new FormGroup({
        planDuration: new FormControl('5'),
        planContrbAmount: new FormControl('5'),
        amortizationProfileId: new FormControl('1'),
        paramProductFamilyId: new FormControl('1'),
        remainingAmount: new FormControl('5'),
        planEstimatedOut: new FormControl(null),
        remainingPercent: new FormControl('5')
      }));

      jest.spyOn(fixture, 'initialiseFinancialPlans').mockReturnValue(array);
      fixture.ngOnInit();

      (fixture.form.controls['financialPlans'] as FormArray).at(0).get('remainingPercent').setErrors({ 'percentageValidator': true });
      fixture.onValidatePlan(0)

      expect(popupServiceMock.popupInfo).toHaveBeenCalled();


    });

    it('should set isPlansValid', () => {
      let array: FormGroup[] = [];
      array.push(new FormGroup({
        planDuration: new FormControl('5'),
        planContrbAmount: new FormControl('5'),
        amortizationProfileId: new FormControl('1'),
        paramProductFamilyId: new FormControl('1'),
        remainingAmount: new FormControl('5'),
        planEstimatedOut: new FormControl(null),
        remainingPercent: new FormControl('5')
      }));

      jest.spyOn(fixture, 'initialiseFinancialPlans').mockReturnValue(array);
      fixture.ngOnInit();

      (fixture.form.controls['financialPlans'] as FormArray).at(0).get('remainingPercent').setErrors({ 'percentageValidator': false });
      (fixture.form.controls['financialPlans'] as FormArray).at(0).get('remainingPercent').setValue('8');
      let isPlansValid: boolean[] = [];
      isPlansValid.push((fixture.form.controls['financialPlans'] as FormArray).at(0).get('remainingPercent').valid)
      fixture.onValidatePlan(0)

      expect(isPlansValid[0]).toEqual(true);

    });

    it('should call financial processed esimated outs rates', () => {
      let array: FormGroup[] = [];
      array.push(new FormGroup({
        planDuration: new FormControl('5'),
        planContrbAmount: new FormControl('5'),
        amortizationProfileId: new FormControl(AmortizationProfileConstants.LINEARE_ID),
        paramProductFamilyId: new FormControl('1'),
        remainingAmount: new FormControl('5'),
        planEstimatedOut: new FormControl(null),
        remainingPercent: new FormControl('5')
      }));

      jest.spyOn(fixture, 'initialiseFinancialPlans').mockReturnValue(array);
      fixture.ngOnInit();

      (fixture.form.controls['financialPlans'] as FormArray).at(0).get('remainingPercent').setErrors({ 'percentageValidator': false });
      (fixture.form.controls['financialPlans'] as FormArray).at(0).get('remainingPercent').setValue('8');
      let isPlansValid: boolean[] = [];
      isPlansValid.push((fixture.form.controls['financialPlans'] as FormArray).at(0).get('remainingPercent').valid)
      fixture.onValidatePlan(0)

      expect(financialPlanServiceMock.planEstimatedOutRateWithFinancialProcessing).toHaveBeenCalled();

    });

    it('should call pre calculated esimated outs rates', () => {
      let array: FormGroup[] = [];
      array.push(new FormGroup({
        planDuration: new FormControl('5'),
        planContrbAmount: new FormControl('5'),
        amortizationProfileId: new FormControl(AmortizationProfileConstants.D1VR1),
        paramProductFamilyId: new FormControl('1'),
        remainingAmount: new FormControl('5'),
        planEstimatedOut: new FormControl(null),
        remainingPercent: new FormControl('5')
      }));

      jest.spyOn(fixture, 'initialiseFinancialPlans').mockReturnValue(array);
      fixture.ngOnInit();

      (fixture.form.controls['financialPlans'] as FormArray).at(0).get('remainingPercent').setErrors({ 'percentageValidator': false });
      (fixture.form.controls['financialPlans'] as FormArray).at(0).get('remainingPercent').setValue('8');
      let isPlansValid: boolean[] = [];
      isPlansValid.push((fixture.form.controls['financialPlans'] as FormArray).at(0).get('remainingPercent').valid)
      fixture.onValidatePlan(0)

      expect(financialPlanServiceMock.planEstimatedOutRatePreCalculated).toHaveBeenCalled();

    });

    it('should get initial value', () => {
      popupServiceMock = {
        popupInfo: jest.fn().mockReturnValue(of(true)),
        onConfirm: of(false)
      };
      fixture = new ValuationReceptionPlansComponent(
        formBuilderMock,
        routerMock,
        popupServiceMock,
        financialPlanServiceMock,
        paramProductFamilyServiceMock,
        paramAmortizationProfileServiceMock,
        valuationSharedServiceMock
      );
      let array: FormGroup[] = [];
      array.push(new FormGroup({
        planDuration: new FormControl('5'),
        planContrbAmount: new FormControl('5'),
        amortizationProfileId: new FormControl('1'),
        paramProductFamilyId: new FormControl('1'),
        remainingAmount: new FormControl('5'),
        planEstimatedOut: new FormControl(null),
        remainingPercent: new FormControl('5')
      }));

      fixture.valuation = valuation;
      jest.spyOn(fixture, 'initialiseFinancialPlans').mockReturnValue(array);



      fixture.ngOnInit();

      (fixture.form.controls['financialPlans'] as FormArray).at(0).get('remainingPercent').setValue('12');
      (fixture.form.controls['financialPlans'] as FormArray).at(0).get('remainingPercent').setErrors({ 'percentageValidator': false });
      fixture.plans = [];
      fixture.plans[0] = financialPlanModel;
      fixture.onValidatePlan(0)
      expect((fixture.form.controls['financialPlans'] as FormArray).at(0).get('remainingPercent').value).toEqual(undefined);

    });


  })


  describe('Test reset', () => {
    it('should return validator required', () => {


      let array: FormGroup[] = [];
      array.push(new FormGroup({
        planDuration: new FormControl('5'),
        planContrbAmount: new FormControl('5'),
        amortizationProfileId: new FormControl('1'),
        paramProductFamilyId: new FormControl('1'),
        remainingAmount: new FormControl('5'),
        planEstimatedOut: new FormControl(null),
        remainingPercent: new FormControl('5')
      }));
      let financementPlans: FinancialPlanModel[] = [];



      financementPlans.push(financialPlanModel);
      jest.spyOn(fixture, 'initialiseFinancialPlans').mockReturnValue(array);
      fixture.ngOnInit();

      fixture.resetPlan(0)
      expect((fixture.form.controls['financialPlans'] as FormArray).at(0).get('remainingPercent').hasError('required')).toBeFalsy();
    });

    it('should reset from expert', () => {


      fixture.financialPlan = [{'id': "1"} as FinancialPlanModel];
      fixture.financialPlansValuationRequest = [];
      fixture.resetPlan(0);
      expect(fixture.valuationSharedService.getFinancialPlans()).toBeTruthy();
      expect(fixture.valuationSharedService.getFinancialPlans().length).toEqual(0);
    });



    it('should reset all field in plan', () => {


      let array: FormGroup[] = [];
      array.push(new FormGroup({
        planDuration: new FormControl('5'),
        planContrbAmount: new FormControl('5'),
        amortizationProfileId: new FormControl('1'),
        paramProductFamilyId: new FormControl('1'),
        remainingAmount: new FormControl('5'),
        planEstimatedOut: new FormControl(null),
        remainingPercent: new FormControl('5')
      }));

      array.push(new FormGroup({
        planDuration: new FormControl('5'),
        planContrbAmount: new FormControl('5'),
        amortizationProfileId: new FormControl('1'),
        paramProductFamilyId: new FormControl('1'),
        remainingAmount: new FormControl('5'),
        planEstimatedOut: new FormControl(null),
        remainingPercent: new FormControl('5')
      }));
      let financementPlans: FinancialPlanModel[] = [];



      financementPlans.push(financialPlanModel);
      jest.spyOn(fixture, 'initialiseFinancialPlans').mockReturnValue(array);
      fixture.ngOnInit();

      fixture.resetPlan(2)
      expect((fixture.form.controls['financialPlans'] as FormArray).at(0).get('remainingPercent').hasError('required')).toBeFalsy();
    });


  });

  describe('Test:  loadProfilListByProductId', () => {
    it('should update profile list', () => {

  fixture.pramAmortizationProfileOptionsByProductId = [];
      let listProfil = [{id:"1",label:"test",paramProductFamily:null}
      ,{id:"2",label:"sss",paramProductFamily:null}];
      fixture.productOptions =[{id:"1",label:"tesy", paramAmortizationProfiles :listProfil} as ParamProductFamilyModel,
      {id:"2",label:"tesy", paramAmortizationProfiles :listProfil} as ParamProductFamilyModel,
      {id:"3",label:"tesy", paramAmortizationProfiles :listProfil} as ParamProductFamilyModel];
      let productForm: FormControl = new FormControl('1')

      fixture.loadProfilListByProductId(productForm);

      expect(fixture.pramAmortizationProfileOptionsByProductId).toEqual(listProfil);

    });

  });
  describe('Test:  changeApportLabel', () => {
    it('should update apprl label ', () => {

  fixture.apportLabelList= ["Apport ou premier loyer majoré"];
      let productForm: FormControl = new FormControl('2')  ;

      fixture.changeApportLabel(productForm,0);

      expect(fixture.apportLabelList[0]).toEqual("Premier loyer majoré");

    });
    it('should update apport label  to apport', () => {

      fixture.apportLabelList= ["Apport ou premier loyer majoré"];
          let productForm: FormControl = new FormControl('1')  ;

          fixture.changeApportLabel(productForm,0);

          expect(fixture.apportLabelList[0]).toEqual("Apport ");

        });



  });

  describe('Test:  set financialPlan', () => {
    it('should update plans list', () => {

      let plans = [] as FinancialPlanModel[];
      fixture.financialPlan = plans;
      expect(fixture.financialPlans).toEqual(plans);

    });

  });

  describe('Test:  set submited', () => {
    it('should isSubmited be true', () => {

      fixture.submited = true;
      expect(fixture.isSubmited).toEqual(true);

    });

  });
  describe('Test:  onBlurDuration', () => {
    it('should enabled amortizationProfileId', () => {
      const financialPlanModel: FinancialPlanModel = {
        id: null,
        planDuration: 1,
        planContrbAmount: 1,
        paramAmortizationProfile: paramAmortizationProfile,
        paramProductFamily:paramProductFamily,
        collateralAssetRating: null,
        status: null,
        valuation: valuation,
        valuationRequest: valuationRequestModel,
        remainingAmount: null,
        remainingPercent: 5,
        planEstimatedOut: null,
        isDuplicated : false,
      };

      fixture.getPlansFormArray().push(fixture.newPlan(financialPlanModel, false));
      fixture.adjustPlanTabsGrid = jest.fn();
      let planControl={
        value:'24'
      } as FormControl

      fixture.onBlurDuration(planControl,0)
      expect((fixture.form.controls['financialPlans'] as FormArray).at(0).get('amortizationProfileId').enabled).toEqual(true)
    });
    it('should disabled amortizationProfileId', () => {
      const financialPlanModel: FinancialPlanModel = {
        id: null,
        planDuration: 1,
        planContrbAmount: 1,
        paramAmortizationProfile: paramAmortizationProfile,
        paramProductFamily:paramProductFamily,
        collateralAssetRating: null,
        status: null,
        valuation: valuation,
        valuationRequest: valuationRequestModel,
        remainingAmount: null,
        remainingPercent: 5,
        planEstimatedOut: null,
        isDuplicated : false,
      };

      fixture.getPlansFormArray().push(fixture.newPlan(financialPlanModel, false));
      fixture.adjustPlanTabsGrid = jest.fn();
      let planControl={
        value:'25'
      } as FormControl

      fixture.onBlurDuration(planControl,0)
      expect((fixture.form.controls['financialPlans'] as FormArray).at(0).get('amortizationProfileId').disabled).toEqual(true)
    });
    it('should enabled amortizationProfileId when value is null', () => {
      const financialPlanModel: FinancialPlanModel = {
        id: null,
        planDuration: 1,
        planContrbAmount: 1,
        paramAmortizationProfile: paramAmortizationProfile,
        paramProductFamily:paramProductFamily,
        collateralAssetRating: null,
        status: null,
        valuation: valuation,
        valuationRequest: valuationRequestModel,
        remainingAmount: null,
        remainingPercent: 5,
        planEstimatedOut: null,
        isDuplicated : false,
      };
      fixture.getPlansFormArray().push(fixture.newPlan(financialPlanModel, false));

      fixture.adjustPlanTabsGrid = jest.fn();
      let planControl={
        value:null
      } as FormControl

      fixture.onBlurDuration(planControl,0)
      expect((fixture.form.controls['financialPlans'] as FormArray).at(0).get('amortizationProfileId').enabled).toEqual(true)
    });
  });
  describe('Test writeValue', () => {
    it('should  write value', () => {
      const financialPlanModel: FinancialPlanModel = {
        id: null,
        planDuration: 1,
        planContrbAmount: 1,
        paramAmortizationProfile: paramAmortizationProfile,
        paramProductFamily:paramProductFamily,
        collateralAssetRating: null,
        status: null,
        valuation: valuation,
        valuationRequest: valuationRequestModel,
        remainingAmount: null,
        remainingPercent: 5,
        planEstimatedOut: null,
        isDuplicated : false,
      };
      fixture.writeValue(financialPlanModel);
      expect(fixture.value).toBeDefined();

    });


  });

  describe('Test onBlurContrbAmount', () => {
    it('should  get value', () => {
      const financialPlanModel: FinancialPlanModel = {
        id: null,
        planDuration: 1,
        planContrbAmount: 1,
        paramAmortizationProfile: paramAmortizationProfile,
        paramProductFamily:paramProductFamily,
        collateralAssetRating: null,
        status: null,
        valuation: valuation,
        valuationRequest: valuationRequestModel,
        remainingAmount: null,
        remainingPercent: 5,
        planEstimatedOut: null,
        isDuplicated : false,
      };
      fixture.getPlansFormArray().push(fixture.newPlan(financialPlanModel, false));
      fixture.onBlurContrbAmount(0);
      expect(fixture.getPlansFormArray().controls[0].get('planContrbAmount').value).toEqual(financialPlanModel.planContrbAmount.toString());

    });


  });


  describe('Test:  verifDisplayLoanMsgInfo', () => {

    it('should displayLoanMsgInfo be true when Product is a Loan', () => {

      fixture.productOptions = [{ id: '1', label: 'Prêt', paramAmortizationProfiles: null, code: ParamProductFamilyConstants.PRET_CODE },
      { id: '2', label: 'Location', paramAmortizationProfiles: null, code: ParamProductFamilyConstants.LOCATION_CODE },
      { id: '3', label: 'Crédit-bail', paramAmortizationProfiles: null, code: ParamProductFamilyConstants.CREDIT_BAIL_CODE }];

      //loan = pret
      //loan id is 1 in the productOptions list
      fixture.verifDisplayLoanMsgInfo('1');
      expect(fixture.displayLoanMsgInfo).toEqual(true);

    });

    it('should displayLoanMsgInfo be false when Product is not a Loan', () => {

      fixture.productOptions = [{ id: '1', label: 'Prêt', paramAmortizationProfiles: null, code: ParamProductFamilyConstants.PRET_CODE },
      { id: '2', label: 'Location', paramAmortizationProfiles: null, code: ParamProductFamilyConstants.LOCATION_CODE },
      { id: '3', label: 'Crédit-bail', paramAmortizationProfiles: null, code: ParamProductFamilyConstants.CREDIT_BAIL_CODE }];

      fixture.verifDisplayLoanMsgInfo('2');
      expect(fixture.displayLoanMsgInfo).toEqual(false);

    });

  });

  describe('Test set value', () => {
    it('should set value', () => {
        let test={
          financialPlans : []
        }
        fixture.value=test;
        expect(fixture.form.value).toEqual(test);
    })
  });

  describe('Test get f()', () => {
    it('should return form controls', () => {
        expect(fixture.f).toEqual(fixture.form.controls);
    })
  });


  describe('Test displayLoanMsgForFirstfinancialPlansValReq', () => {
    it('should call verifDisplayLoanMsgInfo', () => {
      fixture.financialPlansValuationRequest= [{
        paramProductFamily:{
          id : '1'
        } as ParamProductFamilyModel
      } as FinancialPlanModel
    ] as FinancialPlanModel[]
      fixture.displayLoanMsgForFirstfinancialPlansValReq();
      expect(fixture.verifDisplayLoanMsgInfo).toHaveBeenCalled;
    })
  });

});





