import { AfterViewInit, Component, ElementRef, EventEmitter, forwardRef, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { ControlValueAccessor, FormArray, FormBuilder, FormControl, FormGroup, NG_VALIDATORS, NG_VALUE_ACCESSOR, Validator, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { ConfirmationModalComponent } from 'src/app/shared/confirmation-modal/confirmation-modal.component';
import { apportValidator, percentageValidator } from 'src/app/shared/CustomValidators';
import { ErrorModalComponent } from 'src/app/shared/error-modal/error-modal.component';
import { ParamProductFamilyConstants } from 'src/app/shared/param-product-family-constants';
import { PopupService } from 'src/app/shared/services/popup.service';
import { FinancialPlanModel } from '../../../../shared/models/financial-plan.model';
import { ParamAmortizationProfileModel } from '../../../../shared/models/param-amortization-profile.model';
import { ParamProductFamilyModel } from '../../../../shared/models/param-product-family.model';
import { ValuationRequestModel } from '../../../../shared/models/valuation-request.model';
import { ValuationModel } from '../../../../shared/models/valuation.model';
import { AmortizationProfileConstants } from '../../../../shared/param-amortization-profile';
import { FinancialPlanService } from '../../../../shared/services/financial-plan.service';
import { ParamAmortizationProfileService } from '../../../../shared/services/param-amortization-profile.service';
import { ParamProductFamilyService } from '../../../../shared/services/param-product-family.service';
import { ValuationSharedService } from '../../../../shared/services/valuation-shared-service';
import { getResidualValueEuro, parseToInteger, threeDigitDecimalRound, twoDigitDecimalRound } from '../../../../shared/ValuationUtils';



@Component({
  selector: 'app-valuation-reception-plans',
  templateUrl: './valuation-reception-plans.component.html',
  styleUrls: ['./valuation-reception-plans.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ValuationReceptionPlansComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => ValuationReceptionPlansComponent),
      multi: true
    }
  ]
})
export class ValuationReceptionPlansComponent implements OnInit, ControlValueAccessor, OnDestroy, AfterViewInit, Validator {
  isPlansValid: boolean[] = [];
  isRemainingPercentValid: boolean[] = [];
  limitContributionRemaining: number = 10;
  form: FormGroup;
  onTouched = () => {
    // This is intentional
  };
  isSubmited: boolean;
  showAddBtn: boolean = true;
  iDRental: string = '2';
  idFurnitureLease: string = '3';
  idLoan: string = '1';

  @Input() blockValidation: any;

  pramAmortizationProfileOptionsByProductId: ParamAmortizationProfileModel[] = [];
  apportLabelList: string[] = ["Apport ou premier loyer majoré", "Apport ou premier loyer majoré", "Apport ou premier loyer majoré", "Apport ou premier loyer majoré", "Apport ou premier loyer majoré", "Apport ou premier loyer majoré"];

  isRemainingPercentIsRequired: boolean = false;
  subscriptions: Subscription[] = [];
  productOptions: ParamProductFamilyModel[] = [];
  profilOptions: ParamAmortizationProfileModel[] = [];
  allProfilOptions: ParamAmortizationProfileModel[] = [];
  isFormDisabled = false;

  @Input() removedfinancialPlans: any[];
  @ViewChild('planTabs') navTabsElement: ElementRef;
  @Input() plans;
  @Input() readOnly: boolean;
  @Output() onResetSubmit = new EventEmitter<boolean>();
  @Input() valuation: ValuationModel;
  @Input() financialPlans: FinancialPlanModel[];
  financialPlansValuationRequest: FinancialPlanModel[] = [];

  displayLoanMsgInfo = false;

  @Input() set financialPlan(value: FinancialPlanModel[]) {
    if (this.form != null && this.form != undefined) {

      this.form.setControl('financialPlans', this.fb.array([...this.initialiseFinancialPlans(value)]));
      this.financialPlans = value;
      this.doCheckDurationMutipleAllItems()

    } else {
      this.financialPlans = [];
    }
  }
  @Input() set submited(value: boolean) {

    this.isSubmited = value;
    if (this.isSubmited && this.form != null && this.form != undefined) {

      this.form.markAllAsTouched()
    }

  }
  get value() {

    return this.form.getRawValue();
  }

  set value(value) {
    this.form.setValue(value);
    this.onChange(value);
    this.onTouched();
  }

  get f() {
    return this.form.controls;
  }

  get plansControls() {
    return this.getPlansFormArray().controls;
  }

  get plansList(): { name: string }[] {
    const result: { name: string }[] = [];
    for (let i = 0; i < this.plansControls.length; i++) {
      result.push({ name: `Plan de financement ${i + 1}` });
    }
    return result;
  }
  onChangeSub: Subscription

  constructor(private fb: FormBuilder,
    private route: ActivatedRoute,
    private popupService: PopupService,
    private financialPlanService: FinancialPlanService,
    private paramProductFamilyService: ParamProductFamilyService,
    private paramAmortizationProfileService: ParamAmortizationProfileService, public valuationSharedService: ValuationSharedService
  ) {
  }


  ngOnInit(): void {


    this.form = this.fb.group({
      financialPlans: this.fb.array([...this.initialiseFinancialPlans([])]),
    });
    this.subscriptions.push(
      this.form.valueChanges.subscribe(value => {
        this.onChange(value);
        this.onTouched();
      })
    );


    //valuation request id
    const id: string = this.route.snapshot.paramMap.get('id');
    this.paramProductFamilyService.getAllParamProductFamily().subscribe(data => {
      this.productOptions = data;
    })
    this.paramAmortizationProfileService.getAllParamAmortizationProfile().subscribe(data => {
      this.allProfilOptions = data;
    })
    if (id) {
      //default case (no duplicating && no canceling) OR (Cancelling AND no Data)
      if ((!this.valuationSharedService.getIsFromDuplicatedReception() && !this.valuationSharedService.getIsFromUndoDuplicatedReception())
        /*  || (this.valuationSharedService.getIsFromUndoDuplicatedReception() &&
           !(this.valuationSharedService.getDiscounts() && this.valuationSharedService.getDiscounts().length > 0))
         */
      ) {

        this.financialPlanService.loadFinancialPlanByValuationRequestId(id).subscribe(dataValuationRequest => {
          this.financialPlansValuationRequest = dataValuationRequest;
          this.financialPlanService.loadFinancialPlanByValuationId(this.valuation.id).subscribe(dataValuation => {
            if (dataValuation != null && dataValuation.length > 0) {
              this.financialPlans = dataValuation;
              this.financialPlans[this.selectedPlan] && this.financialPlans[this.selectedPlan].paramProductFamily ?
                this.pramAmortizationProfileOptionsByProductId = this.financialPlans[this.selectedPlan].paramProductFamily.paramAmortizationProfiles :
                this.pramAmortizationProfileOptionsByProductId = null;
              this.financialPlans.forEach((element, index) => {
                if (element && element.paramProductFamily && element.paramProductFamily.id)
                  this.changeApportLabel(new FormControl(element.paramProductFamily.id), index);
              })
              if (this.financialPlans[0] && this.financialPlans[0].paramProductFamily && this.financialPlans[0].paramProductFamily.code) {
                if (this.financialPlans[0].paramProductFamily.code === ParamProductFamilyConstants.PRET_CODE) {
                  this.displayLoanMsgInfo = true;
                }
              }
            }
            else {
              let newPlanList: FinancialPlanModel[] = [];
              if (dataValuationRequest) {
                dataValuationRequest.forEach(plan => {
                  let newPlan = Object.assign({}, plan)
                  newPlan.id = null;
                  newPlan.valuation = this.valuation;
                  newPlanList.push(newPlan)
                })
              }
              this.financialPlans = newPlanList;

            }
            if (this.financialPlansValuationRequest && this.financialPlansValuationRequest.length) {
              this.pramAmortizationProfileOptionsByProductId = this.financialPlansValuationRequest[this.selectedPlan].paramProductFamily.paramAmortizationProfiles;
              this.financialPlansValuationRequest.forEach((financialplan, index) => {
                this.changeApportLabel(new FormControl(financialplan.paramProductFamily.id), index);
              })
              this.displayLoanMsgForFirstfinancialPlansValReq();

            }
          }, (err) => {
            console.error(err)
          }, () => {
            this.form.setControl('financialPlans', this.fb.array([...this.initialiseFinancialPlans(this.financialPlans)]));
            if (!this.financialPlans || this.financialPlans.length === 0) {

              this.addNewFundingPlan();

            }
            else {
              this.doCheckDurationMutipleAllItems()

            }
          })


        });

      }

      else {

        this.financialPlanService.loadFinancialPlanByValuationRequestId(id).subscribe(dataValuationRequest => {
          this.financialPlansValuationRequest = dataValuationRequest;
        }, () => {
          //this is intentional

        }, () => {
          let oldFinancialPlans = this.valuationSharedService.getFinancialPlans();
          if (oldFinancialPlans != null && oldFinancialPlans.length > 0) {
            oldFinancialPlans.forEach((element, index) => {
              if (element && element.planEstimatedOut && element.planEstimatedOut.planEstimatedOuts) {
                element.planEstimatedOut = element.planEstimatedOut.planEstimatedOuts;

              }
            })
          }
          else {

            let newPlanList: FinancialPlanModel[] = [];
            if (this.financialPlansValuationRequest && this.financialPlansValuationRequest.length) {
              this.financialPlansValuationRequest.forEach(plan => {
                let newPlan = Object.assign({}, plan)
                newPlan.id = null;
                newPlan.valuation = this.valuation;
                newPlanList.push(newPlan)
              })
            }
            oldFinancialPlans = newPlanList;


          }


          if (this.financialPlansValuationRequest && this.financialPlansValuationRequest.length && this.financialPlansValuationRequest[this.selectedPlan]) {
            this.pramAmortizationProfileOptionsByProductId = this.financialPlansValuationRequest[this.selectedPlan].paramProductFamily.paramAmortizationProfiles;
            this.financialPlansValuationRequest.forEach((financialplan, index) => {
              this.changeApportLabel(new FormControl(financialplan.paramProductFamily.id), index);
            })
            this.displayLoanMsgForFirstfinancialPlansValReq();

          }

          if (this.valuationSharedService.getIsFromUndoDuplicatedReception()) {
            //case when canceling duplication AND data exists


            this.financialPlans = oldFinancialPlans;
            this.form.setControl('financialPlans', this.fb.array([...this.initialiseFinancialPlans(this.financialPlans)]));
            this.doCheckDurationMutipleAllItems()

          }
          else {
            //case when duplicating

            this.financialPlanService.loadFinancialPlanByValuationId(this.valuationSharedService.getSelectedValuationId()).subscribe(dataValuation => {
              if (dataValuation != null && dataValuation.length > 0) {
                this.financialPlans = dataValuation;
                this.pramAmortizationProfileOptionsByProductId = this.financialPlans[this.selectedPlan].paramProductFamily.paramAmortizationProfiles;
                this.financialPlans.forEach((element, index) => {
                  element.id = null;
                  element.planEstimatedOut = null;
                  element.isDuplicated = true
                  element.valuationRequest = this.valuation.valuationRequest
                  element.valuation = this.valuation

                  this.changeApportLabel(new FormControl(element.paramProductFamily.id), index);
                })


                this.verifDisplayLoanMsgInfo(this.financialPlans[0].paramProductFamily.id)
                this.financialPlans = oldFinancialPlans.concat(this.financialPlans);
              }
            }, () => {
              console.error("cannot get financial plan ")
            }, () => {

              this.form.setControl('financialPlans', this.fb.array([...this.initialiseFinancialPlans(this.financialPlans)]));
              this.doCheckDurationMutipleAllItems()

              if (this.financialPlans.length > 6) {

                this.popupService.popupInfo('Vous avez saisi plus que 6 plans de financement, merci de vérifier', 'Confirmer', null, null, ConfirmationModalComponent);

              }
            });
          }

        });

      }
    }
    else {
      // simulation case
      this.addNewFundingPlan();
    }
  }

  initialiseFinancialPlans(financialPlans: FinancialPlanModel[]): FormGroup[] {
    let arr: FormGroup[] = [];


    financialPlans.forEach((plan: FinancialPlanModel, i) => {
      let newPlan: FormGroup
      if (plan && plan.paramProductFamily && plan.paramProductFamily.id && plan.paramProductFamily.id == this.idLoan) {
        this.isRemainingPercentValid.push(false);
        plan.remainingPercent = null;
        newPlan = this.newPlan(plan, false);


      } else {
        this.isRemainingPercentValid.push(true);
        newPlan = this.newPlan(plan, false);
        newPlan.get('remainingPercent').setValidators(this.remainingPercentValidators.concat(Validators.required));

      }

      arr.push(newPlan);

    });
    return arr;
  }

  ngAfterViewInit() {
    this.adjustPlanTabsGrid();
  }



  onChangeSelect(newValue) {
    this.verifDisplayLoanMsgInfo(newValue);

    if (this.plansControls[this.selectedPlan].get('amortizationProfileId').enabled) {
      this.plansControls[this.selectedPlan].get('amortizationProfileId').setValue(null)

    }


    this.updateRisidualValueInputStatus(newValue);


  }

  verifDisplayLoanMsgInfo(newValue: any) {
    this.displayLoanMsgInfo = false;
    let productObjectSelected = this.productOptions.find(prod => prod.id === newValue);
    if (productObjectSelected && productObjectSelected.code == ParamProductFamilyConstants.PRET_CODE) {
      this.displayLoanMsgInfo = true;
    }
  }

  onChange: any = () => {
    // This is intentional
  };


  registerOnChange(onChange: any) {
    this.onChangeSub = this.form.valueChanges.subscribe(onChange);
  }

  ngOnDestroy() {
    if (this.onChangeSub) {
      this.onChangeSub.unsubscribe();
    }
  }


  writeValue(value: any) {
    window.setTimeout(() => {
      if (value) {
        this.form.setValue(value);
      }
    }, 100);
  }

  registerOnTouched(onTouched: any) {
    this.onTouched = onTouched;



  }


  setDisabledState(disabled: boolean) {

    if (disabled) {
      this.form.setErrors(null)
      this.form.disable();
      this.isFormDisabled = true
    }
    else {
      this.form.enable();
    }


  }




  validate(_: FormControl) {

    return this.form.valid ? null : { profile: { valid: false, }, };
  }

  selectedPlan = 0;

  changeTab(i: number) {
    if (this.plansControls[this.selectedPlan] && this.plansControls[this.selectedPlan].valid && this.isPlansValid[this.selectedPlan]) {
      this.selectedPlan = i;
      if (this.financialPlans[i]) {
        this.pramAmortizationProfileOptionsByProductId = this.financialPlans[i].paramProductFamily.paramAmortizationProfiles;
        this.updateResidialValue(new FormControl(this.financialPlans[i].paramProductFamily.id), new FormControl(this.financialPlans[i].paramAmortizationProfile.id), i)
        this.changeApportLabel(new FormControl(this.financialPlans[i].paramProductFamily.id), i);

      }


      this.verifDisplayLoanMsgInfo((this.form.controls['financialPlans'] as FormArray).at(i).get('paramProductFamilyId').value)

      if (this.form.getRawValue() && this.form.getRawValue().plans && this.form.getRawValue().plans[i] && this.form.getRawValue().plans[i].paramAmortizationProfileId !== null && (this.form.getRawValue().plans[i].paramAmortizationProfileId === "3")) {

        this.isRemainingPercentValid[this.selectedPlan] = false

      }
    } else {

      this.popupService.popupInfo('Merci de renseigner le plan actuel avant de changer le plan', '', null, '', ErrorModalComponent);

      this.form.markAllAsTouched()
      this.isSubmited = true;
      setTimeout(() => { this.isSubmited = false }, 10)
    }
  }
  remainingPercentValidators = [
    percentageValidator
  ];

  newPlan(plan: FinancialPlanModel, isForReset): FormGroup {
    let valuationRequestModel: ValuationRequestModel = {} as ValuationRequestModel;
    valuationRequestModel.id = this.valuation.valuationRequest.id;
    if (plan && plan.planEstimatedOut) {
      plan.planEstimatedOut.forEach(el => el.financialPlan = null);
    }
    let isValid = false;

    if (!isForReset) {
      isValid = (plan != null && plan.planEstimatedOut != null);
      this.isPlansValid.push(isValid);
    }
    return this.fb.group({
      planDuration: [{ value: plan ? plan.planDuration : null, disabled: isValid }, [Validators.min(24), Validators.max(180), Validators.required]],
      planContrbAmount: [{ value: (plan && plan.planContrbAmount != null) ? twoDigitDecimalRound(plan.planContrbAmount.toString()) : null, disabled: isValid }, [apportValidator(Number(this.valuation.valuationRequest.financingAmount))]],
      amortizationProfileId: [{ value: plan && plan.paramAmortizationProfile ? plan.paramAmortizationProfile.id : null, disabled: isValid }, Validators.required],
      paramProductFamilyId: [{ value: plan && plan.paramProductFamily ? plan.paramProductFamily.id : null, disabled: isValid }, Validators.required],
      remainingPercent: [{ value: plan ? plan.remainingPercent : null, disabled: isValid }, percentageValidator],
      planEstimatedOut: plan != null && plan.planEstimatedOut != null ? [{ value: { planEstimatedOuts: plan.planEstimatedOut }, disabled: this.readOnly }] : [],
      id: [(plan != null && plan.id != null) ? plan.id : null],
      remainingAmount: [(plan != null && plan.remainingAmount != null) ? twoDigitDecimalRound(plan.remainingAmount.toString()) : null],
      collateralAssetRating: [(plan != null && plan.collateralAssetRating != null) ? plan.collateralAssetRating : null],
      status: [(plan != null && plan.status != null) ? plan.status : null],
      valuation: this.valuation,
      valuationRequest: valuationRequestModel,
      updatedDate: [(plan != null && plan.updatedDate != null) ? plan.updatedDate : null],
      createdDate: [(plan != null && plan.createdDate != null) ? plan.createdDate : null],
      createdBy: [(plan != null && plan.createdBy != null) ? plan.createdBy : null],
      updatedBy: [(plan != null && plan.updatedBy != null) ? plan.updatedBy : null],
      isDuplicated: [(plan != null && plan.isDuplicated != null) ? plan.isDuplicated : false],
    });


  }

  isPlanControlsValid(): boolean {
    let isValid = true;
    this.isPlansValid.forEach(planValidate => isValid = isValid && planValidate);

    return isValid && this.form.valid;
  }


  addNewFundingPlan() {
    this.displayLoanMsgInfo = false;
    if (this.plansList.length == 6) {
      this.popupService.popupInfo('Vous avez atteint le nombre maximal de plans pour cette demande', '', null, '', ErrorModalComponent);
      return;
    }


    if (!this.isPlanControlsValid()) {
      this.popupService.popupInfo('Merci de renseigner le plan actuel avant de créer un nouveau plan', '', null, '', ErrorModalComponent);
      this.form.markAllAsTouched()
      this.isSubmited = true;
      setTimeout(() => { this.isSubmited = false }, 10)
      return;
    }
    this.getPlansFormArray().push(this.newPlan(null, false));
    if (this.getPlansFormArray().length > 1) {
      this.changeTab(this.getPlansFormArray().length - 1);
      this.adjustPlanTabsGrid();
    }
  }

  deleteFundingPlan(index: number) {
    let removedPlan = this.getPlansFormArray().value[index];
    if (removedPlan.id != null) {
      this.removedfinancialPlans.push(removedPlan);
    }
    this.getPlansFormArray().removeAt(index);
    if (index <= this.selectedPlan && this.selectedPlan > 0) {
      this.selectedPlan -= 1;

    }
    this.isPlansValid.splice(index, 1);

    this.adjustPlanTabsGrid();
  }

  adjustPlanTabsGrid() {
    this.navTabsElement.nativeElement.style.setProperty('grid-template-columns', `repeat(${this.plansList.length}, 1fr)`);
  }

  getPlansFormArray(): FormArray {

    return (this.form.get('financialPlans') as FormArray);

  }

  get plansFormArray(): FormArray {

    return (this.form.get('financialPlans') as FormArray);

  }

  async onValidatePlan(i: number) {

    this.plansControls[i].markAllAsTouched();

    if ((this.plansControls[this.selectedPlan].get('remainingPercent').value > this.limitContributionRemaining) && (!this.plansControls[this.selectedPlan].get('remainingPercent').getError('percentageValidator'))) {
      this.popupService.popupInfo('La valeur résiduelle renseignée est soumis à accord préalable sur le plan correspondant', '', null, '', ConfirmationModalComponent);
      const subscription = this.popupService.onConfirm.subscribe((t) => {
        if (t) {
          this.validatePlan(i);
        } else {

          (this.form.controls['financialPlans'] as FormArray).at(i).get('remainingPercent').setValue(this.financialPlans[i].remainingPercent);
        }
        subscription.unsubscribe();
      });
    } else {
      this.validatePlan(i);
    }
    if ((this.plansControls[this.selectedPlan].get('remainingPercent').getError('percentageValidator'))) {
      this.popupService.popupInfo('Merci de renseigner une valeur comprise entre 0 et 100', '', null, '', ErrorModalComponent);
    }
    if (this.isPlansValid[i]) {
      this.showAddBtn = false;

      await (this.form.controls['financialPlans'] as FormArray).at(i).get('planEstimatedOut').setValidators(Validators.required);
      await (this.form.controls['financialPlans'] as FormArray).at(i).get('planEstimatedOut').updateValueAndValidity();


      this.showAddBtn = true;



    }
  }

  validatePlan(i) {
    this.isPlansValid[i] = this.plansControls[i].valid;

    if (this.plansControls[i].valid) {
      let value = (this.form.controls['financialPlans'] as FormArray).at(i).value;

      let amortizationProfileId = value.amortizationProfileId;
      if (this.financialPlans && this.financialPlans[i]) {
        this.financialPlans[i].planEstimatedOut = null

      }
      if (amortizationProfileId != AmortizationProfileConstants.PLAN_LIBRE_ID) {
        if (amortizationProfileId == AmortizationProfileConstants.LINEARE_ID && value.remainingPercent != 1) {
          this.planEstimatedOutRateWithFinancialProcessing(value, i);

        } else {

          this.planEstimatedOutRatePreCalculated(value, amortizationProfileId, i);

        }


      }
      (this.form.controls['financialPlans'] as FormArray).at(i).get('planDuration').disable();
      (this.form.controls['financialPlans'] as FormArray).at(i).get('planContrbAmount').disable();
      (this.form.controls['financialPlans'] as FormArray).at(i).get('amortizationProfileId').disable();
      (this.form.controls['financialPlans'] as FormArray).at(i).get('paramProductFamilyId').disable();
      (this.form.controls['financialPlans'] as FormArray).at(i).get('remainingPercent').disable();

    }

  }

  planEstimatedOutRateWithFinancialProcessing(value: any, i: any) {
    this.financialPlanService.planEstimatedOutRateWithFinancialProcessing(value.planDuration, value.remainingPercent).subscribe(res => {
      res.forEach(rslt => {
        rslt.financialPlan = null;
        rslt.estimatedOutPrincVal = Math.round((Number(this.valuation.valuationRequest.financingAmount) - value.planContrbAmount) * rslt.estimatedOutPrincRate) / 100
      });
      (this.form.controls['financialPlans'] as FormArray).at(i).get('planEstimatedOut').patchValue({ planEstimatedOuts: res });

    });
  }

  planEstimatedOutRatePreCalculated(value: any, amortizationProfileId: any, i: any) {
    this.financialPlanService.planEstimatedOutRatePreCalculated(value.planDuration, amortizationProfileId).subscribe(resp => {
      if (resp.length > 0) {
        resp.forEach(result => {
          result.financialPlan = null;
          result.estimatedOutPrincVal = Math.round((Number(this.valuation.valuationRequest.financingAmount) - value.planContrbAmount) * result.estimatedOutPrincRate) / 100
        });
        (this.form.controls['financialPlans'] as FormArray).at(i).get('planEstimatedOut').patchValue({ planEstimatedOuts: resp });
      } else {
        console.error('There is no calculated outstandings for profile id ' + amortizationProfileId + ' and period ' + value.planDuration)
      }
    });
  }

  resetPlan(i: number) {

    this.popupService.popupInfo('Êtes-vous sûr de vouloir réinitialiser les éléments de votre plan de financement ?', '', null, '', ConfirmationModalComponent);
    const subscription = this.popupService.onConfirm.subscribe((t) => {


      if (t) {


        this.displayLoanMsgInfo = false;

        this.isPlansValid[i] = false;
        this.isSubmited = false;
        let resetPlan: FormGroup;
        this.onResetSubmit.emit(false);
        let plans = this.financialPlans[i];

        if (plans != null) {
          //en cas de changement d'un plan
          if (plans.planEstimatedOut != null) {
            this.financialPlanService.removePlanEstimatedOut(plans.planEstimatedOut).subscribe(data => {
              //this is intentional
            });
          }

          if (i <= (this.financialPlansValuationRequest.length - 1)) {
            //en cas ou le plan est d'origine chargé d'affaire
            let id = plans.id;
            plans = Object.assign({}, this.financialPlansValuationRequest[i]);
            plans.id = id;
            plans.valuation = this.valuation;
            let product = this.productOptions.find(prod => prod.id === plans.paramProductFamily.id);
            this.pramAmortizationProfileOptionsByProductId = product.paramAmortizationProfiles;

          }
          else {
            //en cas ou le plan est ajouter par un expert
            plans.paramAmortizationProfile = null;
            plans.paramProductFamily = null;
            plans.planContrbAmount = null;
            plans.planDuration = null;
            plans.planEstimatedOut = null;
            plans.remainingAmount = null;
            plans.remainingPercent = null;
            plans.status = null;
          }



        }

        resetPlan = this.newPlan(plans, true);
        (this.form.controls['financialPlans'] as FormArray).at(i).get('planDuration').enable();
        (this.form.controls['financialPlans'] as FormArray).at(i).get('planContrbAmount').enable();
        (this.form.controls['financialPlans'] as FormArray).at(i).get('amortizationProfileId').enable();
        (this.form.controls['financialPlans'] as FormArray).at(i).get('paramProductFamilyId').enable();
        (this.form.controls['financialPlans'] as FormArray).at(i).get('remainingPercent').enable();


        (this.form.controls['financialPlans'] as FormArray).at(i).setValue(resetPlan.value);
        ((this.form.controls['financialPlans'] as FormArray).at(i) as FormGroup).markAsUntouched();


        if (resetPlan.get('paramProductFamilyId').value != null && resetPlan.get('paramProductFamilyId').value == this.idLoan) {



          (this.form.controls['financialPlans'] as FormArray).at(i).get('remainingPercent').setValidators(this.remainingPercentValidators);

          this.isRemainingPercentValid[i] = false

        } else {
          (this.form.controls['financialPlans'] as FormArray).at(i).get('remainingPercent').setValidators(this.remainingPercentValidators.concat(Validators.required));
          this.isRemainingPercentValid[i] = true

        }
        this.plansControls[i].get('remainingPercent').updateValueAndValidity();
        (this.form.controls['financialPlans'] as FormArray).at(i).get('planEstimatedOut').clearValidators();
        (this.form.controls['financialPlans'] as FormArray).at(i).get('planEstimatedOut').updateValueAndValidity();


        this.updateResidialValue((this.form.controls['financialPlans'] as FormArray).at(i).get('paramProductFamilyId') as FormControl, (this.form.controls['financialPlans'] as FormArray).at(i).get('amortizationProfileId'), i)



        this.disableAmortizationProfileAndSetValue(i);

      }
      subscription.unsubscribe();
    });

  }

  onBlurResidualValue(planControl: FormControl, i) {

    if (this.plansControls[i].get('remainingPercent').getError('percentageValidator')) {
      this.popupService.popupInfo("Merci de renseigner une valeur comprise entre 0 et 100!", '', null, '', ErrorModalComponent);
    } else {
      (this.form.controls['financialPlans'] as FormArray).at(i).get('remainingPercent').setValue(threeDigitDecimalRound((this.form.controls['financialPlans'] as FormArray).at(i).get('remainingPercent').value))

      this.getPlansFormArray().controls[i].patchValue({
        remainingAmount: getResidualValueEuro(Number(this.valuation.valuationRequest.financingAmount), (this.form.controls['financialPlans'] as FormArray).at(i).get('remainingPercent').value)
      });
    }
  }
  loadProfilListByProductId(productId: FormControl) {

    let product = this.productOptions.find(prod => prod.id === productId.value);
    this.pramAmortizationProfileOptionsByProductId = product.paramAmortizationProfiles;
  }

  changeApportLabel(productId: FormControl, index) {
    if ((productId.value == this.iDRental) || productId.value === this.idFurnitureLease) {
      this.apportLabelList[index] = 'Premier loyer majoré'
    }
    else if (productId.value == this.idLoan) {
      this.apportLabelList[index] = 'Apport '
    }
  }

  updateResidialValue(product: FormControl, profileId, index) {
    if (profileId !== null && profileId.value === "3") {
      this.isRemainingPercentValid[this.selectedPlan] = false
      this.getPlansFormArray().controls[index].patchValue({
        remainingPercent: 1,
        remainingAmount: getResidualValueEuro(Number(this.valuation.valuationRequest.financingAmount), 1)

      });

    }
    else {
      this.updateRisidualValueInputStatus(product.value);
    }
  }
  updateRisidualValueInputStatus(productId) {

    if ((productId == this.iDRental) || (productId == this.idFurnitureLease)) { // Location ou Crédit-bail
      this.plansControls[this.selectedPlan].get('remainingPercent').setValidators([percentageValidator, Validators.required])

      this.isRemainingPercentValid[this.selectedPlan] = true
    } else if (productId == this.idLoan) { //Prêt
      this.isRemainingPercentValid[this.selectedPlan] = false

      if (this.remainingPercentValidators.indexOf(Validators.required) == -1) {

        this.plansControls[this.selectedPlan].get('remainingPercent').setValidators(null)
      }


      this.plansControls[this.selectedPlan].get('remainingPercent').reset()
    }
    this.plansControls[this.selectedPlan].get('remainingPercent').updateValueAndValidity();
  }


  onBlurContrbAmount(index) {
    if (this.getPlansFormArray().controls[index].get('planContrbAmount').value) {
      this.getPlansFormArray().controls[index].get('planContrbAmount').setValue(twoDigitDecimalRound(this.getPlansFormArray().controls[index].get('planContrbAmount').value));
    }
  }

  onBlurDuration(planControl: FormControl, index: number) {
    if (planControl && planControl.value != null && planControl.value != undefined) {
      let value = planControl.value;
      this.getPlansFormArray().controls[index].patchValue({
        planDuration: parseToInteger(value)
      });
      if (value % 12 != 0) {
        if (this.pramAmortizationProfileOptionsByProductId == null || this.pramAmortizationProfileOptionsByProductId.length == 0 ) {
          this.pramAmortizationProfileOptionsByProductId = this.allProfilOptions;
        }

        this.getPlansFormArray().controls[index].patchValue({
          amortizationProfileId: '6'
        });
        (this.form.controls['financialPlans'] as FormArray).at(index).get('amortizationProfileId').disable();

      }
      else {
        (this.form.controls['financialPlans'] as FormArray).at(index).get('amortizationProfileId').enable();

      }
    }
    else {
      (this.form.controls['financialPlans'] as FormArray).at(index).get('amortizationProfileId').enable();
      if (!(this.form.controls['financialPlans'] as FormArray).at(index).get('amortizationProfileId').value) {
        this.getPlansFormArray().controls[index].patchValue({
          amortizationProfileId: null
        });
      }
    }

  }
  disableAmortizationProfileAndSetValue(index) {
    let value = (this.form.controls['financialPlans'] as FormArray).at(index).get('planDuration').value;
    if (value % 12 != 0) {
      this.getPlansFormArray().controls[index].patchValue({
        amortizationProfileId: '6'
      });
      (this.form.controls['financialPlans'] as FormArray).at(index).get('amortizationProfileId').disable();
    }
  }
  doCheckDurationMutipleAllItems() {
    if (this.form && this.form.controls['financialPlans'] && (this.form.controls['financialPlans'] as FormArray).length) {
      for (let i = 0; i < (this.form.controls['financialPlans'] as FormArray).length; i++) {
        this.disableAmortizationProfileAndSetValue(i);


      }
    }

  }

  displayLoanMsgForFirstfinancialPlansValReq() {

    this.verifDisplayLoanMsgInfo(this.financialPlansValuationRequest[0].paramProductFamily.id)

  }

}
