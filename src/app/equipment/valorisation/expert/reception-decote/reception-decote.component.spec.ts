import { EventEmitter } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { of, throwError } from 'rxjs';
import { Role } from '../../../../shared/models/roles.enum';
import { ValuationModel } from '../../../../shared/models/valuation.model';
import { ValuationDiscountModel } from '../../../../shared/models/valuationDiscount.model';
import { ValuationSharedService } from '../../../../shared/services/valuation-shared-service';
import { ReceptionDecoteComponent } from './reception-decote.component';




describe('ReceptionDecoteComponent', () => {
  let fixture: ReceptionDecoteComponent;
  let decoteServiceMock: any;
  let popupServiceMock: any;
  let authServiceMock: any;
  let valuationSharedServiceMock: any;


  let valuation: ValuationModel = {
    "id": "1",
    "discountRateComment": "comment",
    "experId": "1",
    "controllerId": "1",
    "validatorId": "1",
    "isDuplicated" : null ,
    "isSubmitted" : null,
    "catNature": null,
    "paramValidationType": null,
    'catBrand': null,
    "catAssetType": null,
    "valuationRequest": null
    , "catActivitySector": null,
    "createdDate": null,
    "descNetwork": null,
    "descName": null,


  } as ValuationModel
  let listDiscountRatValues = [
    {
      "id": "1",
      "discountRateYear": 4,
      "discountRateValue": 2,
      "collAssetEstimVal": 1,
      "valuation": valuation,
      "catDiscountRate": null,
      "catDiscount": null
    } as ValuationDiscountModel,
    {
      "id": "2",
      "discountRateYear": 4,
      "discountRateValue": 2,
      "collAssetEstimVal": 1,
      "valuation": valuation,
      "catDiscountRate": null,
      "catDiscount": null
    } as ValuationDiscountModel,
    {
      "id": "3",
      "discountRateYear": 4,
      "discountRateValue": 2,
      "collAssetEstimVal": 1,
      "valuation": valuation,
      "catDiscountRate": null,
      "catDiscount": null
    } as ValuationDiscountModel,
  ];

  beforeEach(() => {
    decoteServiceMock = {
      getDiscount: jest.fn().mockReturnValue(of(true)),
      getDiscountRate: jest.fn().mockReturnValue(of(1)),
      getValuationDiscount: jest.fn().mockReturnValue(of(true)),
      saveValuationDiscounts: jest.fn().mockReturnValue(of(true)),
      prepareDiscountsForSave: jest.fn().mockReturnValue(of(true)),
      deleteOldDiscountsAndResetComment: jest.fn().mockReturnValue(of(true)),
      getDiscountByValuationIdOrFromCat: jest.fn().mockReturnValue(of((listDiscountRatValues))),
      deleteValuationDiscounts: jest.fn().mockReturnValue(of(true)),
      checkDiscountIsChanged: jest.fn().mockReturnValue(of('NO_CHANGES')),
    };
    valuationSharedServiceMock = TestBed.inject(ValuationSharedService);



    popupServiceMock = {
      popupInfo: jest.fn().mockReturnValue(of(true)),
      onConfirm: of(true)
    };

    authServiceMock = {
      'identityClaims': {
        'mat': ''
      },
      'currentUserRolesList': ['ROLE_SVS_ADMIN_SE']
    }
    fixture = new ReceptionDecoteComponent(
      decoteServiceMock,
      authServiceMock,
      popupServiceMock,
      valuationSharedServiceMock
    );
    fixture.ngOnInit();
  });

  afterEach(() => jest.clearAllMocks());

  describe('Test: ngOnInit', () => {

    it('should get discounts', () => {


      expect(fixture.discounts).toEqual(listDiscountRatValues);
    });

    it('should get discounts', () => {
      const discounts = ['ca', 'ca'];
      valuationSharedServiceMock = {
        getIsFromUndoDuplicatedReception: jest.fn(() => true),
        getDiscounts: jest.fn(() => discounts),
        getFinancialPlans: jest.fn(() => discounts),
        getDiscountValueChanged: jest.fn(() => true)
      };
      fixture = new ReceptionDecoteComponent(
        decoteServiceMock,
        authServiceMock,
        popupServiceMock,
        valuationSharedServiceMock
      );
      fixture.ngOnInit();
      expect(valuationSharedServiceMock.getDiscountValueChanged).not.toHaveBeenCalled();
    });

    it('should get discounts', () => {
      const discounts = ['ca', 'ca'];
      valuationSharedServiceMock = {
        getIsFromUndoDuplicatedReception: jest.fn(() => true),
        getDiscounts: jest.fn(() => discounts),
        getFinancialPlans: jest.fn(),
        getDiscountValueChanged: jest.fn(() => true)
      };
      fixture = new ReceptionDecoteComponent(
        decoteServiceMock,
        authServiceMock,
        popupServiceMock,
        valuationSharedServiceMock
      );
      fixture.ngOnInit();
      expect(valuationSharedServiceMock.getDiscountValueChanged).toHaveBeenCalled();
    });

    it('should set initial Discounts ', () => {


      expect(fixture.initialDiscount).toEqual(2);
    });

    it('should throws error ', () => {

      spyOn(decoteServiceMock, 'getDiscountByValuationIdOrFromCat').and.returnValue(throwError({ status: 404 }));

      fixture.ngOnInit();
    });

  });

  describe('Test: isAdmin', () => {

    it('should isAdmin true ', () => {



      expect(fixture.isAdmin).toEqual(true);
    });

    it('should isAdmin false ', () => {
      authServiceMock = authServiceMock = {
        'identityClaims': {
          'mat': ''
        },
        'currentUserRolesList': [Role.SVS_EXPERT_SE]
      }

      fixture = new ReceptionDecoteComponent(
        decoteServiceMock,
        authServiceMock,
        popupServiceMock,
        valuationSharedServiceMock
      );
      fixture.setIsAdmin()

      expect(fixture.isAdmin).toEqual(false);
    });

  });


  describe('Test: changeValue', () => {

    it('should changeValue set valueChanged to true', () => {



      expect(fixture.valueChanged).toEqual(false);
      fixture.changeValue(10, 0);
      expect(fixture.valueChanged).toEqual(true);
    });

    it('should changeValue set value on index', () => {



      fixture.changeValue(50, 0);
      expect(fixture.discounts[0].discountRateValue).toEqual(50);
      fixture.changeValue(70, 1);
      expect(fixture.discounts[1].discountRateValue).toEqual(70);
    });

  });

  describe('Test: verifyDiscount', () => {

    it('should not validate discount when value is changed and commentArea is whitespace', () => {

      fixture.valueChanged = true;
      fixture.discountComment = "   ";
      fixture.verifyDiscount();
      expect(fixture.showErrorMsgForComment).toEqual(true);
      let expectedEvent = new EventEmitter<boolean>();
      expectedEvent.emit(true);
      expect(fixture.isDiscountValid).toEqual(expectedEvent);
    });

    it('should not validate discount when value is changed and commentArea is empty', () => {

      fixture.valueChanged = true;
      fixture.discountComment = "";
      fixture.verifyDiscount();
      expect(fixture.showErrorMsgForComment).toEqual(true);
      let expectedEvent = new EventEmitter<boolean>();
      expectedEvent.emit(true);
      expect(fixture.isDiscountValid).toEqual(expectedEvent);
    });

    it('should validate discount when value is not changed', () => {

      fixture.valueChanged = false;
      fixture.verifyDiscount();
      decoteServiceMock.saveValuationDiscounts.mockReturnValue();
      expect(fixture.showErrorMsgForComment).toEqual(false);
      let expectedEvent = new EventEmitter<boolean>();
      expectedEvent.emit(false);
      expect(fixture.isDiscountValid).toEqual(expectedEvent);
    });
    it('should throws error ', () => {

      spyOn(decoteServiceMock, 'saveValuationDiscounts').and.returnValue(throwError({ status: 404 }));

      fixture.verifyDiscount();
    });
    it('should call prepare discounts to save', () => {

      fixture.valueChanged = false;
      expect(fixture.approuveDiscountChangement()).toEqual(false);

      fixture.verifyDiscount();
      decoteServiceMock.saveValuationDiscounts.mockReturnValue();
      expect(decoteServiceMock.prepareDiscountsForSave()).toHaveBeenCalled;
    });

    it('should show popup to confirm when Math.abs(this.discounts[0].discountRateValue-this.initialDiscount)>10', () => {

      fixture.valueChanged = false;
      fixture.discounts[0] = {
        discountRateYear: 1,
        discountRateValue: 10
      } as ValuationDiscountModel;
      fixture.initialDiscount = 25;
      expect(fixture.approuveDiscountChangement()).toEqual(true);
      fixture.verifyDiscount();
      expect(popupServiceMock.popupInfo).toHaveBeenCalled();

    });

    it('should show popup to confirm when Math.abs(this.discounts[0].discountRateValue-this.initialDiscount)>10', () => {
      popupServiceMock = {
        popupInfo: jest.fn().mockReturnValue(of(true)),
        onConfirm: of(false)
      };
      fixture = new ReceptionDecoteComponent(
        decoteServiceMock,
        authServiceMock,
        popupServiceMock,
        valuationSharedServiceMock
      );
      fixture.ngOnInit();
      fixture.valueChanged = false;
      fixture.discounts[0] = {
        discountRateYear: 1,
        discountRateValue: 10
      } as ValuationDiscountModel;
      fixture.initialDiscount = 25;
      const spyInstance = jest.spyOn(fixture, 'resetDecote');
      fixture.verifyDiscount();
      expect(spyInstance).toHaveBeenCalled();

    });

  });

  describe('Test: resetDiscount', () => {
    it('should show popup when resetDiscount function is called ', () => {
      fixture.resetDiscount();
      expect(popupServiceMock.popupInfo).toHaveBeenCalled();
    });
    it('Should set discount comment to empty ', () => {
      fixture.resetDiscount();
      expect(fixture.discountComment).toEqual('');
    });
    it('Should emit false from discountValid ', () => {
      fixture.resetDiscount();
      fixture.deleteOldDiscountsAndResetComment();
      let expectedEvent = new EventEmitter<boolean>();
      expectedEvent.emit(true);
      expect(fixture.isDiscountValid).toEqual(expectedEvent);
    });

  });

  describe('Test: loadDiscountFromCatalog', () => {
    it('should throws error ', () => {

      spyOn(decoteServiceMock, 'getDiscountByValuationIdOrFromCat').and.returnValue(throwError({ status: 404 }));

      fixture.loadDiscountFromCatalog();
    });

  });


  describe('Test set approuveDiscountChangementDraft()', () => {
    it('should approuveDiscountChangementDraft from a valuationDiscounts', () => {
      decoteServiceMock = {
        getDiscount: jest.fn().mockReturnValue(of(true)),
        getDiscountRate: jest.fn().mockReturnValue(of(1)),
        getValuationDiscount: jest.fn().mockReturnValue(of(true)),
        saveValuationDiscounts: jest.fn().mockReturnValue(of(true)),
        prepareDiscountsForSave: jest.fn().mockReturnValue(of(true)),
        deleteOldDiscountsAndResetComment: jest.fn().mockReturnValue(of(true)),
        getDiscountByValuationIdOrFromCat: jest.fn().mockReturnValue(of((listDiscountRatValues))),
        deleteValuationDiscounts: jest.fn().mockReturnValue(of(true)),
        checkDiscountIsChanged: jest.fn().mockReturnValue(of('CHANGED')),
      };

      fixture = new ReceptionDecoteComponent(
        decoteServiceMock,
        authServiceMock,
        popupServiceMock,
        valuationSharedServiceMock
      );
      fixture.ngOnInit();
      fixture.approuveDiscountChangementDraft(listDiscountRatValues);
      expect(fixture.valueChanged).toBeTruthy();

    });
  });

  describe('Test: saveValuationDiscounts', () => {
    it('should save valuation discounts', () => {
      fixture.valueChanged = true;
      fixture.discountComment = null;
      fixture.saveValuationDiscounts(true);
      expect(fixture.validated).toEqual(false);
    });
  });


});

