import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ConfirmationModalComponent } from '../../../../shared/confirmation-modal/confirmation-modal.component';
import { ValuationModel } from '../../../../shared/models/valuation.model';
import { ValuationDiscountModel } from '../../../../shared/models/valuationDiscount.model';
import { AuthService } from '../../../../shared/services/auth.service';
import { DecoteService } from '../../../../shared/services/decote.service';
import { PopupService } from '../../../../shared/services/popup.service';
import { ValuationSharedService } from '../../../../shared/services/valuation-shared-service';
import { isBlankOrEmpty } from '../../../../shared/StringUtils';
import { DiscountsConstants } from '../../../../shared/discounts-constant';




@Component({
  selector: 'app-reception-decote',
  templateUrl: './reception-decote.component.html',
  styleUrls: ['./reception-decote.component.css']
})
export class ReceptionDecoteComponent implements OnInit {

  percentSymbol = "%";
  @Input() activitySectorId: string;
  @Input() natureId: string;
  @Input() brandId: string;
  @Input() assetTypeId: string;
  @Input() valuationId: string;
  @Input() isNew: boolean;
  @Input() blockValidation: any;
  @Input() paramEnergyId: string
  disabledForm: false;
  isAdmin: boolean = false
  decoteLoaded: boolean = true;
  @Input() set disabled(disabled) {

    if (disabled != null && disabled != undefined) {
      this.disabledForm = disabled;

    }
  }

  @Output() discountEvent = new EventEmitter<ValuationDiscountModel[]>();
  @Output() resetplansEvent = new EventEmitter<boolean>();
  discounts: ValuationDiscountModel[];
  valueChanged: boolean = false;
  extends = false;
  validated = false;
  showErrorMsgForComment = false;
  @Output() isDiscountValid = new EventEmitter<boolean>();
  discountComment: string;
  discountRate: number;
  initialDiscount: number;
  isDiscountValidated = false;
  fromCat: boolean = true;
  disocuntDraftChanges: string ;
  constructor(private decoteService: DecoteService, private authService: AuthService, private popupService: PopupService,
    private valuationSharedService: ValuationSharedService) {
  }

  ngOnInit(): void {
    this.isDiscountValid.emit(false);
    this.setIsAdmin()
    if (this.valuationSharedService.getIsFromUndoDuplicatedReception() && this.valuationSharedService.getDiscounts()
      && this.valuationSharedService.getDiscounts().length > 0) {
      this.discounts = this.valuationSharedService.getDiscounts();
      this.afterLoadDiscounts();

      if (!(this.valuationSharedService.getFinancialPlans() && this.valuationSharedService.getFinancialPlans().length > 0)) {
        this.isDiscountValidated = false;
        this.disabledForm = false;
        this.validated = false;
        this.valueChanged = this.valuationSharedService.getDiscountValueChanged();
        this.isDiscountValid.emit(false);


      }
      else {
        this.saveValuationDiscounts();

      }

    }
    else {
      this.decoteService.getDiscountByValuationIdOrFromCat(this.valuationSharedService.getIsFromDuplicatedReception() ? null : this.valuationId, this.assetTypeId, this.brandId, this.natureId, this.activitySectorId, this.isNew, this.paramEnergyId).subscribe((discount: ValuationDiscountModel[]) => {
        this.discounts = discount;
        this.emitDiscount();
      }, (err) => {
        console.error(err);
        this.popupService.popupInfo("Aucune pondération énergie n'est paramétrée. Merci d'adresser une demande à la filière", 'Ok', null, null, ConfirmationModalComponent);
        this.decoteLoaded = false;
        this.discounts = null;

      }, () => {
        this.decoteLoaded = true;
        //to load discounts
        this.afterLoadDiscounts();
      });
    }

  }
  afterLoadDiscounts() {
    // first
    if (this.discounts) {
      this.initialDiscount = this.discounts[0].discountRateValue;

    }    //Not From Catalogue
    if (this.discounts[0].valuation != null) {
      this.approuveDiscountChangementDraft(this.discounts)
      this.fromCat = false;
      this.discountComment = this.discounts[0].valuation.discountRateComment;


    }
  }

  setIsAdmin() {
    this.isAdmin = this.authService.currentUserRolesList.includes('ROLE_SVS_ADMIN_SE');
  }

  changeValue(value: number, i: number): void {
    this.valueChanged = true;
    this.discounts[i].discountRateValue = value;
    this.emitDiscount();
  }

  emitDiscount(): void {
    this.discountEvent.emit(this.discounts);

  }

  verifyDiscount(): void {

    this.showErrorMsgForComment = false;


    if (!((this.valueChanged || (this.disocuntDraftChanges!= null && this.disocuntDraftChanges != DiscountsConstants.NO_CHANGES))
     && isBlankOrEmpty(this.discountComment))) {

      if (this.approuveDiscountChangement() || this.disocuntDraftChanges == DiscountsConstants.CHANGED_WITH_POPUP) {

        this.popupService.popupInfo('Le taux de décote que vous venez de renseigner à un écart de plus de 10 points par rapport au taux initial. Confirmez-vous ce taux ?',
          'Confirmer', null, 'Annuler', ConfirmationModalComponent);
        const subscription = this.popupService.onConfirm.subscribe((t) => {
          if (t) {
            this.saveValuationDiscounts();

          } else {
            this.resetDecote();

          }
          subscription.unsubscribe();
        });
      } else {
        this.saveValuationDiscounts();
      }
    } else {
      this.showErrorMsgForComment = true;
      this.isDiscountValid.emit(false);
    }

  }

  resetDiscount(): void {
    this.popupService.popupInfo('Les taux de décotes seront réinitialisés et cela pourra impacter le calcul de vos plans de financement. Confirmez-vous cette action ?',
      'Oui', null, 'Annuler', ConfirmationModalComponent);
    const subscription = this.popupService.onConfirm.subscribe((t) => {
      if (t) {

        this.resetDecote();
      }
      subscription.unsubscribe();
    });
  }

  resetDecote() {
    // hide btn reset and display btn validate
    this.isDiscountValidated = false;

    // comment area is empty and disabled
    this.discountComment = '';
    this.valueChanged = false;

    // arrow up dow enabled
    this.validated = false;

    // delete old discounts and plans and reset comment for the valuation
    this.deleteOldDiscountsAndResetComment();


    // réinitialiser plans de financement
    this.isDiscountValid.emit(false);

    //return default
    this.disocuntDraftChanges = DiscountsConstants.NO_CHANGES;
  }

  loadDiscountFromCatalog(): void {
    this.decoteService.getDiscountByValuationIdOrFromCat(this.valuationId, this.assetTypeId, this.brandId, this.natureId, this.activitySectorId, this.isNew, this.paramEnergyId).subscribe(discount => {
      this.discounts = discount;
      this.emitDiscount();
    }, (err) => {
      console.error(err);
      this.popupService.popupInfo("Aucune pondération énergie n'est paramétrée. Merci d'adresser une demande à la filière", 'Ok', null, null, ConfirmationModalComponent);
      this.decoteLoaded = false;
      this.discounts = null;


    }, () => {
      this.decoteLoaded = true;
      // first
      if (this.discounts) {
        this.initialDiscount = this.discounts[0].discountRateValue;
      }
      // from catalogue
      this.fromCat = true;
    });
  }
  deleteOldDiscountsAndResetComment() {
    let valuation = {} as ValuationModel;
    valuation.id = this.valuationId
    this.decoteService.deleteValuationDiscounts(valuation).subscribe(() => {
      this.discounts = null;
      // This is intentional
    }, (err) => {
      console.error(err);
    }, () => {
      // load discout from catalog
      this.loadDiscountFromCatalog();
      this.resetplansEvent.emit(true);
    });

  }
  prepareDiscountsForSave(oldDiscounts: ValuationDiscountModel[]) {
    let newModels: ValuationDiscountModel[] = [];
    let valuation: ValuationModel = {} as ValuationModel;

    //to update with current valiation Id
    valuation.id = this.valuationId;
    //to update The Comment
    valuation.discountRateComment = this.discountComment

    if (oldDiscounts != null && oldDiscounts.length > 0) {
      oldDiscounts.forEach(function (discount) {
        let newModel: ValuationDiscountModel = {} as ValuationDiscountModel;
        newModel.discountRateValue = discount.discountRateValue;
        newModel.discountRateYear = Number(discount.discountRateYear);
        newModel.valuation = valuation;
        //check if from catalogue ? (if true) insert id into cat-discount Id else copy the same cat-discount

        newModel.catDiscount = discount.catDiscount;
        //add new model
        newModels.push(newModel);
      });
    }

    return newModels;
  }

  approuveDiscountChangement() {

    if (this.discounts && this.initialDiscount) {

      //Le taux de décote de la première année  saisi est strictement inférieure ou supérieure de 10 points par rapport à la donnée catalogue
      if (Math.abs(this.discounts[0].discountRateValue - this.initialDiscount) > 10) {
        return true;
      }

    }
    return false;
  }

  saveValuationDiscounts(fromDraft?: boolean) {


    //if nothing changed and saved for draft
    if (!(fromDraft && !this.valueChanged && (this.disocuntDraftChanges!= null && this.disocuntDraftChanges != DiscountsConstants.NO_CHANGES))) {
      // service to save all discounts on validated
      this.decoteService.saveValuationDiscounts(this.prepareDiscountsForSave(this.discounts)).subscribe((data) => {
        this.discounts = data;
      }, err => {
        console.error('Error saving reception discounts');
        this.validated = false;
        this.isDiscountValid.emit(false);
        this.popupService.popupInfo('Un problème technique est survenu, merci de contacter votre administrateur via Help!', 'Ok', null, null, ConfirmationModalComponent);
      }, () => {

        //to check for valid Draft
        if ((fromDraft && this.valueChanged && (this.discountComment == null || this.discountComment == ''))) {
          this.isDiscountValid.emit(false);
          this.validated = false;
          this.isDiscountValidated = false;
          this.emitDiscount();
          this.fromCat = false;

        }
        else {
          this.isDiscountValid.emit(true);
          this.validated = true;
          this.isDiscountValidated = true;
          this.valueChanged = false;
          this.emitDiscount();
          this.fromCat = false;

        }
      });
    }
  }

  approuveDiscountChangementDraft(valuationDiscounts: ValuationDiscountModel[]) {
    this.decoteService.checkDiscountIsChanged(valuationDiscounts).subscribe(data => {

      this.disocuntDraftChanges = data;
      if (data && data != DiscountsConstants.NO_CHANGES) {

      this.valueChanged = true;
      return data;
      }

    },(err)=>{
      console.error(err);
    },()=>{
      if (this.disocuntDraftChanges != DiscountsConstants.NO_CHANGES && (this.discountComment == null || this.discountComment == '')) {
        this.isDiscountValid.emit(false);
        this.validated = false;
        this.isDiscountValidated = false;
      }
      else {
        this.isDiscountValid.emit(true);
        this.validated = true;
        this.isDiscountValidated = true;
      }

    });
  }


}

