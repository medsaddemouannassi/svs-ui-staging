import { AfterViewInit, Component, ElementRef, EventEmitter, forwardRef, Input, OnDestroy, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { AbstractControl, ControlValueAccessor, FormArray, FormBuilder, FormControl, FormGroup, NG_VALIDATORS, NG_VALUE_ACCESSOR, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { forkJoin, of, Subscription } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { apportValidator, percentageValidator } from 'src/app/shared/CustomValidators';
import { ErrorModalComponent } from 'src/app/shared/error-modal/error-modal.component';
import { markFormGroupTouched } from 'src/app/shared/utils';
import { ConfirmationModalComponent } from "../../shared/confirmation-modal/confirmation-modal.component";
import { ParamAmortizationProfileModel } from '../../shared/models/param-amortization-profile.model';
import { ParamProductFamilyModel } from '../../shared/models/param-product-family.model';
import { ParamAmortizationProfileService } from '../../shared/services/param-amortization-profile.service';
import { ParamProductFamilyService } from '../../shared/services/param-product-family.service';
import { PopupService } from "../../shared/services/popup.service";
import { ValuationSharedService } from '../../shared/services/valuation-shared-service';
import { getResidualValueEuro, getResidualValuePercent, parseToInteger, residualValuePercentageLimitValidator, threeDigitDecimalRound, twoDigitDecimalRound } from '../../shared/ValuationUtils';

@Component({
  selector: 'app-plan-list',
  templateUrl: './plan-list.component.html',
  styleUrls: ['./plan-list.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PlanListComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => PlanListComponent),
      multi: true
    }
  ]
})
export class PlanListComponent implements OnInit, ControlValueAccessor, OnDestroy, AfterViewInit {

  form: FormGroup;
  subscriptions: Subscription[] = [];
  productOptions: ParamProductFamilyModel[] = [];

  profilOptions: ParamAmortizationProfileModel[] = [];
  allProfilOptions: ParamAmortizationProfileModel[] = [];
  pramAmortizationProfileOptionsByProductId: ParamAmortizationProfileModel[] = [];
  apportLabelList: string[] = ["Apport ou premier loyer majoré", "Apport ou premier loyer majoré", "Apport ou premier loyer majoré", "Apport ou premier loyer majoré", "Apport ou premier loyer majoré", "Apport ou premier loyer majoré"];
  iDRental: string = '2';
  idFurnitureLease: string = '3';
  idLoan: string = '1';

  priceAmount: number = 0;
  productWithResidualValue = [];
  disableUpdateResidualValue = true;
  @ViewChild('planTabs') navTabsElement: ElementRef;
  plans;

  @Input() set plansListModel(plans) {

    this.plans = plans;

    if (plans && this.form) {




      this.setPlansForm();


      this.loadProfilListByProductId(this.plansControls[this.selectedPlan].get('paramProductFamilyId') as FormControl);
      this.changeApportLabel(this.plansControls[this.selectedPlan].get('paramProductFamilyId') as FormControl, this.selectedPlan);


      if (
        !this.readOnly &&
        this.plansControls[this.selectedPlan].get('paramProductFamilyId').value &&
        this.productWithResidualValue.indexOf(this.plansControls[this.selectedPlan].get('paramProductFamilyId').value.toString()) != -1
      ) {
        this.disableUpdateResidualValue = false;
      }

      this.adjustPlanTabsGrid();



    }
  }
  @Input() readOnly: boolean;
  @Input() set displayOnly(displayOnly: boolean) {
    if (displayOnly) {
      this.form.disable();
      this.readOnly = true;
    }

  }

  @Input() set price(price: number) {
    this.priceAmount = price;
    if (this.form && this.plansControls && this.plansControls.length) {
      this.plansControls.forEach(ctr => {
        ctr.get('planContrbAmount').clearValidators();
        ctr.get('planContrbAmount').setValidators([apportValidator(this.priceAmount)]);
        ctr.get('planContrbAmount').updateValueAndValidity();
      })
    }
  }
  @Output() isInvalidPrice = new EventEmitter<boolean>();

  get value() {
    return this.form.getRawValue();
  }

  set value(value) {

    this.form.setValue(value);
    this.onChange(value);
    this.onTouched();
  }

  get f() {
    return this.form.controls;
  }

  get plansControls(): AbstractControl[] {
    return this.getPlansFormArray().controls;
  }


  plansList: { name: string }[] = [];

  updatePlansList() {
    const result: { name: string }[] = [];
    for (let i = 0; i < this.plansControls.length; i++) {
      result.push({ name: `Plan ${i + 1}` });
    }
    this.plansList = result;
  }


  constructor(private fb: FormBuilder,
    private modalService: NgbModal,
    private popupService: PopupService,
    private paramAmortizationProfileService: ParamAmortizationProfileService,
    private paramProductFamilyService: ParamProductFamilyService,
    private valuationSharedService: ValuationSharedService) {
  }

  ngOnInit(): void {


    forkJoin(
      [this.paramAmortizationProfileService.getAllParamAmortizationProfile().pipe(catchError(error => of([]))), this.paramProductFamilyService.getAllParamProductFamily().pipe(catchError(error => of([])))]

    ).subscribe((value: any[]) => {

      this.allProfilOptions = value[0];
      this.productOptions = value[1];
    }, (err) => console.error(err), () => {
      this.initProductListWithResidualValue();

    });
    this.form = this.fb.group({
      plans: this.fb.array([this.newPlan()]),
      plansToDelete: this.fb.array([])

    });
    this.subscriptions.push(
      this.form.valueChanges.subscribe(value => {
        this.onChange(this.form.getRawValue());
        this.onTouched();
      })
    );


    if (this.plans) {

      this.setPlansForm();

    }



    this.valuationSharedService.undoRequestDuplicationSubject.subscribe(undo => {
      if (undo) {
        this.form.removeControl('plans');

        this.form.addControl('plans', this.fb.array([this.newPlan()]));

        this.plans = { plans: this.valuationSharedService.getFinancialPlans() }
        this.setPlansForm();
        this.changeTab(0);
        if (this.form && this.form.getRawValue()) {


          for (let j = 0; j < this.form.getRawValue().plans.length; j++) {
            this.disableParamAmortizationProfile(j);
          }
        }

      }
    })



  }

  setPlansForm() {
    for (let i = 2; i <= Object.keys(this.plans.plans).length; i++) {
      this.getPlansFormArray().push(this.newPlan());
    }
    this.form.patchValue({ plans: this.plans.plans })
    if (this.form && this.form.getRawValue()) {
      for (let j = 0; j < this.form.getRawValue().plans.length; j++) {
        this.disableParamAmortizationProfile(j);
      }
    }
    this.updatePlansList();
    this.adjustPlanTabsGrid();
  }

  disableParamAmortizationProfile(index) {
    if (this.form.getRawValue().plans[index]) {
      let value = (this.form.getRawValue().plans[index].planDuration)
      if (value && value % 12 != 0) {
        if (this.pramAmortizationProfileOptionsByProductId.length == 0) {

          this.pramAmortizationProfileOptionsByProductId = this.allProfilOptions;
        }

        this.getPlansFormArray().controls[index].patchValue({
          paramAmortizationProfileId: '6'
        });
        (this.getPlansFormArray().controls[index].get('paramAmortizationProfileId').disable());

      }

    }
  }
  ngAfterViewInit() {
    this.updatePlansList();
    this.adjustPlanTabsGrid();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  ngOnChanges(changes: SimpleChanges) {

    if (changes.price && changes.price.currentValue) {
      for (let value of this.plansControls) {
        if (value.get('remainingPercent').value) {
          value.patchValue({
            remainingAmount: getResidualValueEuro(changes.price.currentValue, value.get('remainingPercent').value)
          });
        }
      }
    }

  }

  onBlurPlanContrbAmount(index: number) {
    let control = this.getPlansFormArray().at(index);
    if (control && control.value && control.value.planContrbAmount != null) {
      control.patchValue({ planContrbAmount: twoDigitDecimalRound(control.value.planContrbAmount) })

    }
  }


  onChange: any = () => {
    // This is intentional
  };
  onTouched: any = () => {
    // This is intentional
  };

  registerOnChange(fn) {
    this.subscriptions.push(this.form.valueChanges.pipe(map(_ => this.form.getRawValue())).subscribe(fn));
  }

  writeValue(value) {
    if (value) {
      this.value = value;


    }

    if (value === null) {
      this.form.reset();
    }
  }

  registerOnTouched(fn) {
    this.onTouched = fn;
  }

  validate(_: FormControl) {
    return this.form.valid ? null : { plans: { valid: false, }, };
  }

  selectedPlan = 0;

  changeTab(i: number) {
    this.pramAmortizationProfileOptionsByProductId = [];

    if (this.plans != null && this.plans.plans[i] != null && this.plans.plans[i].paramProductFamily != null) {

      let product = this.productOptions.find(element => element.id === this.plans.plans[i].paramProductFamily.id);
      this.pramAmortizationProfileOptionsByProductId = product.paramAmortizationProfiles;


    }
    if (this.plans != null && this.plans.plans[i] != null && this.plans.plans[i].paramProductFamilyId != null) {

      let product = this.productOptions.find(element => element.id === this.plans.plans[i].paramProductFamilyId);
      this.pramAmortizationProfileOptionsByProductId = product.paramAmortizationProfiles;
      this.changeApportLabel(this.plansControls[i].get('paramProductFamilyId') as FormControl, i)
      if (this.productWithResidualValue.indexOf(product.id.toString()) != -1 && !this.readOnly) {
        this.disableUpdateResidualValue = false;
      } else {
        this.disableUpdateResidualValue = true;

      }

    }
    this.selectedPlan = i;

    if (this.form.getRawValue() && this.form.getRawValue().plans && this.form.getRawValue().plans[i] && this.form.getRawValue().plans[i].paramAmortizationProfileId !== null && (this.form.getRawValue().plans[i].paramAmortizationProfileId === "3")) {

      this.disableUpdateResidualValue = true;

    }

    this.form.updateValueAndValidity()

  }

  private newPlan(): FormGroup {
    // [apportValidator(this.price), Validators.required]
    this.disableUpdateResidualValue = true;
    return this.fb.group({
      planDuration: [{ value: '', disabled: this.readOnly }, [Validators.min(24), Validators.max(180), Validators.required]],
      planContrbAmount: [{ value: '', disabled: this.readOnly }, [apportValidator(this.priceAmount)]],
      paramAmortizationProfileId: [{ value: null, disabled: this.readOnly }, Validators.required],
      paramProductFamilyId: [{ value: null, disabled: this.readOnly }, Validators.required],
      remainingAmount: [{ value: '', disabled: this.readOnly }],
      remainingPercent: [{ value: '', disabled: this.readOnly }, percentageValidator],
      id: [''],
      isDuplicated: [false]

    });
  }

  addNewFundingPlan() {
    if (this.getPlansFormArray().length === 6) {
      const modalRef = this.modalService.open(ErrorModalComponent, { centered: true });
      modalRef.componentInstance.content = 'Vous avez atteint le nombre maximal de plans pour cette demande';
      return;
    }

    if (!this.isPlanControlsValid()) {
      const modal = this.modalService.open(ErrorModalComponent, { centered: true });
      modal.componentInstance.content = 'Merci de renseigner le plan actuel avant de créer un nouveau plan';
      return;
    }
    this.getPlansFormArray().push(this.newPlan());
    this.updatePlansList();

    this.changeTab(this.getPlansFormArray().length - 1);
    this.adjustPlanTabsGrid();
  }

  deleteFundingPlan(index: number) {
    let plansToDelete = this.getPlansFormArray().at(index)
    if (plansToDelete.value.id) {
      this.getPlansToDeleteFormArray().push(plansToDelete);

    }
    this.getPlansFormArray().removeAt(index);
    this.updatePlansList();

    this.changeTab(index - 1);
    this.adjustPlanTabsGrid();
  }

  adjustPlanTabsGrid() {

    if (this.navTabsElement) {
      this.navTabsElement.nativeElement.style.setProperty('grid-template-columns', `repeat(${this.plansList.length > 6 ? 6 : this.plansList.length}, 1fr)`);
      if (this.plansList.length > 6) {

        this.navTabsElement.nativeElement.style.setProperty('height', '70 px !important')
      }
    }
  }

  private getPlansFormArray(): FormArray {
    return (this.form.get('plans') as FormArray);
  }

  private getPlansToDeleteFormArray(): FormArray {
    return (this.form.get('plansToDelete') as FormArray);
  }

  public isPlanControlsValid(): boolean {
    return this.getPlansFormArray().valid;
  }

  onBlurResidualValueAmount(planControl: FormControl, index: number) {

    this.getPlansFormArray().controls[index].patchValue({
      remainingAmount: planControl.value ? (twoDigitDecimalRound(planControl.value.toString())) : null,
    });
    this.updateResidualValuePercent(index, planControl);

    if (this.priceAmount && planControl.value) {
      //check for the   100> % > 0
      if (getResidualValuePercent(this.priceAmount, planControl.value) > 100) {

        if (this.plansControls[index].get('remainingPercent').getError('percentageValidator')) {
          this.popupService.popupInfo("Merci de renseigner une valeur comprise entre 0 et prix d'achat!", '', null, '', ErrorModalComponent);
        }

      } else if (residualValuePercentageLimitValidator(getResidualValuePercent(this.priceAmount, planControl.value))) {

        //logic of confirmation of the percentage limit
        this.checkForPopup(index);
      }
    }

  }

  onBlurDuration(planControl: FormControl, index: number) {
    if (planControl && planControl.value != null && planControl.value != undefined) {
      let value = planControl.value;
      this.getPlansFormArray().controls[index].patchValue({
        planDuration: parseToInteger(value)
      });
      if (value % 12 != 0) {
        if (this.pramAmortizationProfileOptionsByProductId.length == 0) {
          this.pramAmortizationProfileOptionsByProductId = this.allProfilOptions;
        }

        this.getPlansFormArray().controls[index].patchValue({
          paramAmortizationProfileId: '6'
        });
        (this.form.controls['plans'] as FormArray).at(index).get('paramAmortizationProfileId').disable();

      }
      else {
        (this.form.controls['plans'] as FormArray).at(index).get('paramAmortizationProfileId').enable();

      }
    }
    else {
      (this.form.controls['plans'] as FormArray).at(index).get('paramAmortizationProfileId').enable();
      if (!(this.form.controls['plans'] as FormArray).at(index).get('paramProductFamilyId').value) {
        this.pramAmortizationProfileOptionsByProductId = [];
        this.getPlansFormArray().controls[index].patchValue({
          amortizationProfileId: ''
        });
      }
    }

  }
  onBlurResidualValuePercent(planControl: FormControl, index: number) {

    if (this.plansControls[index].get('remainingPercent').getError('percentageValidator')) {
      this.popupService.popupInfo("Merci de renseigner une valeur comprise entre 0 et 100!", '', null, '', ErrorModalComponent);
    }

    if (residualValuePercentageLimitValidator(planControl.value))
      //check for confirmation of the new percentage if its above desired amount
      this.checkForPopup(index);
    this.getPlansFormArray().controls[index].patchValue({
      remainingPercent: threeDigitDecimalRound(planControl.value)

    });
    this.updateResidualValueAmount(index, planControl);

  }


  updateResidualValueAmount(index: number, plansControls: FormControl) {
    this.isInvalidPrice.emit(false);
    if (!this.priceAmount) {
      this.isInvalidPrice.emit(true);
    } else {
      // calculate RV euro from Price and RV %

      this.getPlansFormArray().controls[index].patchValue({
        remainingAmount: getResidualValueEuro(this.priceAmount, plansControls.value)
      });
    }
  }

  updateResidualValuePercent(index: number, plansControls: FormControl) {
    this.isInvalidPrice.emit(false);
    if (!this.priceAmount) {
      this.isInvalidPrice.emit(true);
    } else {
      // calculate RV % from Price and RV euro
      this.getPlansFormArray().controls[index].patchValue({
        remainingPercent: getResidualValuePercent(this.priceAmount, plansControls.value)
      });
    }
  }

  initProductListWithResidualValue() {
    if (this.productOptions.length > 0) {
      this.productOptions.forEach(x => {
        if ((x.label == 'Location') || x.label === 'Crédit-bail') {
          this.productWithResidualValue.push(x.id);
        }
      });
    }
  }

  checkProductType(plansControls: FormControl, index: number) {
    let productId = plansControls.value;


    if (this.plansControls[index].get('paramAmortizationProfileId').enabled) {
      this.plansControls[index].get('paramAmortizationProfileId').reset()

    }

    this.updateRisidualValueInputStatus(productId, index);
  }


  updateRisidualValueInputStatus(productId, index) {
    if (productId !== null && this.productWithResidualValue.indexOf(productId.toString()) != -1) {
      this.disableUpdateResidualValue = false;
    } else {
      this.disableUpdateResidualValue = true;
      this.getPlansFormArray().controls[index].patchValue({
        remainingPercent: null,
        remainingAmount: null

      });
    }
  }
  checkForPopup(index: number) {
    this.popupService.popupInfo("votre garant est-il agréé? Si non, merci de procéder à l'agrément selon les procédures en vigueur.", 'Confirmer', null, 'Annuler', ConfirmationModalComponent);
    this.popupService.onConfirm.subscribe((t) => {
      //if false
      if (!t) {
        //return valueAmout & percentage to 0
        this.getPlansFormArray().controls[index].patchValue({
          remainingAmount: 0,
          remainingPercent: 0,
        });
      }
    });

  }

  markAsTouched() {
    markFormGroupTouched(this.form);
  }
  // SVS -115

  loadProfilListByProductId(productId: FormControl) {
    if (productId && productId.value) {
      let product = this.productOptions.find(element => element.id === productId.value);

      this.pramAmortizationProfileOptionsByProductId = product ? product.paramAmortizationProfiles : [];
    }
  }

  checkProfilList(productId: FormControl) {
    if (this.pramAmortizationProfileOptionsByProductId.length === 0) {
      this.loadProfilListByProductId(productId);
    }
  }
  changeApportLabel(productId: FormControl, index) {
    if ((productId.value == this.iDRental) || productId.value === this.idFurnitureLease) {
      this.apportLabelList[index] = 'Premier loyer majoré'
    }
    else if (productId.value == this.idLoan) {
      this.apportLabelList[index] = 'Apport '
    }
  }
  updateResidialValue(product, profileId, index) {
    if (profileId !== null && profileId.value === "3") {

      this.disableUpdateResidualValue = true;
      this.getPlansFormArray().controls[index].patchValue({
        remainingPercent: 1,
        remainingAmount: getResidualValueEuro(this.priceAmount, 1)

      });
    }
    else {
      this.updateRisidualValueInputStatus(product.value, index);
    }
  }

  setDisabledState(disabled: boolean) {
    if (disabled) {
      this.form.disable()
    } else {
      this.form.enable()
    }
  }


}
