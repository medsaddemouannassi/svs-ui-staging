import { EventEmitter, SimpleChanges, ElementRef } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { forkJoin, Subscription } from 'rxjs';
import { of } from 'rxjs/internal/observable/of';
import { ParamProductFamilyModel } from 'src/app/shared/models/param-product-family.model';
import { ValuationSharedService } from '../../shared/services/valuation-shared-service';
import { PlanListComponent } from './plan-list.component';

describe('PlanListComponent', () => {
  let fixture: PlanListComponent;
  let formBuilderMock: FormBuilder;
  let modalServiceMock: any;
  let popupServiceMock: any;
  let paramAmortizationProfileServiceMock: any;
  let paramProductFamilyServiceMock: any
  let listParamProductFamily;
  let listParamAmortizationProfile;
  let plansValue;
  let subscriptionMock: Subscription;
  let valuationSharedServiceMock:any

  beforeEach(() => {
    subscriptionMock = new Subscription();

    plansValue = [{
      planDuration: 50,
      planContrbAmount: 50,
      paramAmortizationProfileId: '2',
      paramProductFamilyId: '1',
      remainingAmount: '5',
      remainingPercent: '10',
      id: '',
      isDuplicated:false,
    },
    {
      planDuration: 50,
      planContrbAmount: 50,
      paramAmortizationProfileId: '2',
      paramProductFamilyId: '1',
      remainingAmount: '5',
      remainingPercent: '10',
      id: '',
      isDuplicated:false,


    }]
    listParamProductFamily = [{ id: '1', label: 'Prêt',paramAmortizationProfiles:[] },
    { id: '2', label: 'Location',paramAmortizationProfiles:[] },
    { id: '3', label: 'Crédit-bail',paramAmortizationProfiles:[] }];

    listParamAmortizationProfile = [
      { id: '1', label: 'Loyer ou échéance constant(e)' },
      { id: '2', label: 'Non-linéaire' },
      { id: '3', label: 'Linéaire' },
    ];

    modalServiceMock = {
      open: jest.fn().mockReturnValue({
        componentInstance: {
          content: ''
        }
      }),
    };


    formBuilderMock = new FormBuilder();

    popupServiceMock = {
      popupInfo: jest.fn().mockReturnValue(of(true)),
      onConfirm: of(true)
    };

    paramProductFamilyServiceMock = {
      getAllParamProductFamily: jest.fn().mockReturnValue(of(listParamProductFamily)),
    }
    paramAmortizationProfileServiceMock = {
      getAllParamAmortizationProfile: jest.fn().mockReturnValue(of(listParamAmortizationProfile))

    }

    valuationSharedServiceMock = TestBed.inject(ValuationSharedService);


    fixture = new PlanListComponent(
      formBuilderMock,
      modalServiceMock,
      popupServiceMock,
      paramAmortizationProfileServiceMock,
      paramProductFamilyServiceMock,
      valuationSharedServiceMock

    );
    fixture.plans = {
      plans: plansValue
    }
    spyOn(fixture, 'adjustPlanTabsGrid');

    fixture.ngOnInit();
  });

  describe('Test: ngOnInit', () => {
    it('should initialize form', async () => {

      forkJoin([
        paramProductFamilyServiceMock.getAllParamProductFamily,
        paramAmortizationProfileServiceMock.getAllParamProductFamily
      ]).subscribe((x) => {
        expect(fixture.form.value.plans[0]).toEqual({
          planDuration: 50,
          planContrbAmount: 50,
          paramAmortizationProfileId: '2',
          paramProductFamilyId: '1',
          remainingAmount: '5',
          remainingPercent: '10',
          isDuplicated:false,

        });
      })



    });

    it('should fill list', () => {

      expect(fixture.allProfilOptions).toEqual(listParamAmortizationProfile)
      expect(fixture.productOptions).toEqual(listParamProductFamily)

    });
  });

  describe('Test: value', () => {
    it('should get value of from', () => {
      forkJoin([
        paramProductFamilyServiceMock.getAllParamProductFamily,
        paramAmortizationProfileServiceMock.getAllParamProductFamily
      ]).subscribe((x) => {
        expect(fixture.value.plans).toEqual(plansValue)
      })
    });
  });

  describe('Test ngOnDestroy', () => {
    it('should unsubscribe', () => {
      fixture.ngOnDestroy();
      const spyunsubscribe = jest.spyOn(subscriptionMock, 'unsubscribe');
      expect(spyunsubscribe).toBeDefined();
    });
  });

  describe('Test:  checkProductType', () => {
    it('should  return false when  product = 1 ', () => {
      forkJoin([
        paramProductFamilyServiceMock.getAllParamProductFamily,
        paramAmortizationProfileServiceMock.getAllParamProductFamily
      ]).subscribe((x) => {
        let planControl: FormControl = new FormControl({ id: '1' });
        fixture.checkProductType(planControl, 0);

        expect(fixture.disableUpdateResidualValue).toEqual(false);
      })

    });
    it('should  return false when  product =2', () => {
      forkJoin([
        paramProductFamilyServiceMock.getAllParamProductFamily,
        paramAmortizationProfileServiceMock.getAllParamProductFamily
      ]).subscribe((x) => {
        let planControl2: FormControl = new FormControl({ id: '2' });
        fixture.checkProductType(planControl2, 0);

        expect(fixture.disableUpdateResidualValue).toEqual(false);
      })
    });

    it('should  return true when  product =3', () => {
      forkJoin([
        paramProductFamilyServiceMock.getAllParamProductFamily,
        paramAmortizationProfileServiceMock.getAllParamProductFamily
      ]).subscribe((x) => {
        let planControl3: FormControl = new FormControl({ id: '3' });
        fixture.checkProductType(planControl3, 0);

        expect(fixture.disableUpdateResidualValue).toEqual(true);
      })
    });
    it('should  return true when  product is null', () => {
      forkJoin([
        paramProductFamilyServiceMock.getAllParamProductFamily,
        paramAmortizationProfileServiceMock.getAllParamProductFamily
      ]).subscribe((x) => {
        let planControl4: FormControl = new FormControl(null);
        fixture.checkProductType(planControl4, 0);

        expect(fixture.disableUpdateResidualValue).toEqual(true);
      })

    });
  });





  describe('Test:  updateResidualValuePercent', () => {
    it('should true when price=0', () => {

      fixture.priceAmount = 0;

      let planControl: FormControl = new FormControl('20000');
      fixture.updateResidualValuePercent(0, planControl);

      let expectedEvent = new EventEmitter<boolean>();
      expectedEvent.emit(true);

      expect(fixture.isInvalidPrice).toEqual(expectedEvent);

    });

    it('should true when price=undefined', () => {

      fixture.priceAmount = undefined;

      let planControl: FormControl = new FormControl('20000');
      fixture.updateResidualValuePercent(0, planControl);

      let expectedEvent = new EventEmitter<boolean>();
      expectedEvent.emit(true);

      expect(fixture.isInvalidPrice).toEqual(expectedEvent);

    });

    it('should 20 when price=100000 and residualValueAmount=20000', () => {

      fixture.priceAmount = 100000;

      fixture.plansControls.push(new FormGroup({
        remainingPercent: new FormControl(10),
      }));

      let planControl: FormControl = new FormControl('20000');
      fixture.updateResidualValuePercent(0, planControl);
      expect(fixture.plansControls[0].get('remainingPercent').value).toEqual(20);


    });


  });

  describe('Test:  updateResidualValueAmount', () => {

    it('should 3000 when price=100000 and residualValuePercent=30', () => {

      fixture.priceAmount = 100000;

      fixture.plansControls.push(new FormGroup({
        remainingAmount: new FormControl(1000),
      }));


      let planControl: FormControl = new FormControl('30');
      fixture.updateResidualValueAmount(0, planControl);
      expect(fixture.plansControls[0].get('remainingAmount').value).toEqual("30000");


    });

    it('should true when price=0', () => {

      fixture.priceAmount = 0;

      let planControl: FormControl = new FormControl('10');
      fixture.updateResidualValueAmount(0, planControl);

      let expectedEvent = new EventEmitter<boolean>();
      expectedEvent.emit(true);

      expect(fixture.isInvalidPrice).toEqual(expectedEvent);

    });

    it('should true when price=undefined', () => {

      fixture.priceAmount = undefined;

      let planControl: FormControl = new FormControl('10');
      fixture.updateResidualValueAmount(0, planControl);

      let expectedEvent = new EventEmitter<boolean>();
      expectedEvent.emit(true);

      expect(fixture.isInvalidPrice).toEqual(expectedEvent);

    });


  });



  describe('Test ngOnChanges()', () => {
    it('should update all residualValueAmount value when priceAmount is changed', () => {

      fixture.priceAmount = 10000;

      fixture.readOnly = false;

      fixture.ngOnInit();
      let changes: SimpleChanges = {
        'price': {
          'previousValue': 20000,
          'currentValue': 200000,
          'firstChange': false,
          'isFirstChange': null
        }
      }
      valuationSharedServiceMock.undoRequestDuplicationSubject.next(true);

      fixture.ngOnChanges(changes);
      forkJoin([
        paramProductFamilyServiceMock.getAllParamProductFamily,
        paramAmortizationProfileServiceMock.getAllParamProductFamily
      ]).subscribe((x) => {
        fixture.form.setValue(fixture.plans);
        fixture.plansControls.push(new FormGroup({
          remainingAmount: new FormControl(0),
          remainingPercent: new FormControl(10)
        }));
        let pcToAdd1 = new FormGroup({
          remainingAmount: new FormControl(0),
          remainingPercent: new FormControl(20)
        });
        let pcToAdd2 = new FormGroup({
          remainingAmount: new FormControl(0),
          remainingPercent: new FormControl(30)
        });
        fixture.plansControls.push(pcToAdd1);
        fixture.plansControls.push(pcToAdd2);

        let changes: SimpleChanges = {
          'price': {
            'previousValue': 20000,
            'currentValue': 200000,
            'firstChange': false,
            'isFirstChange': null
          }
        }
        fixture.ngOnChanges(changes);
        expect(fixture.plansControls[1].get('remainingAmount').value).toEqual(20000);
        expect(fixture.plansControls[2].get('remainingAmount').value).toEqual(20000);
      })
    });

  });

  describe('Test registerOnChange', () => {
    it('should get register', () => {
      let test = (test) => { return test };
      let size = fixture.subscriptions.length;
      fixture.registerOnChange(test);
      expect(fixture.onChange).not.toEqual(() => {
        //this is intentional
      });
      expect(fixture.subscriptions.length).toEqual(size + 1);


    });
  });

  describe('Test registerOnTouched', () => {
    it('should get register', () => {
      let test;
      fixture.registerOnTouched(test);
      expect(fixture.onTouched).not.toEqual(() => {
        //this is intentional
      });
      expect(fixture.onTouched).toEqual(test);

    });
  });


  describe('Test addNewFundingPlan', () => {
    it('should call adjust plan tabs grid', () => {
      fixture.ngOnInit();

      forkJoin([
        paramProductFamilyServiceMock.getAllParamProductFamily,
        paramAmortizationProfileServiceMock.getAllParamProductFamily
      ]).subscribe((x) => {
        fixture.form.enable();
        fixture.form.setValue(fixture.plans)
        jest.spyOn(fixture, 'adjustPlanTabsGrid').mockImplementation(() => {
          //this is intentional
        })
        fixture.addNewFundingPlan();
        expect(fixture.adjustPlanTabsGrid).toHaveBeenCalled();

      })



    });
    it('should open modalif for max plans', () => {

      plansValue = [{
        planDuration: 50,
        planContrbAmount: 50,
        paramAmortizationProfileId: '2',
        paramProductFamilyId: '1',
        remainingAmount: '5',
        remainingPercent: '10'
      },
      {
        planDuration: 50,
        planContrbAmount: 50,
        paramAmortizationProfileId: '2',
        paramProductFamilyId: '1',
        remainingAmount: '5',
        remainingPercent: '10'
      },
      {
        planDuration: 50,
        planContrbAmount: 50,
        paramAmortizationProfileId: '2',
        paramProductFamilyId: '1',
        remainingAmount: '5',
        remainingPercent: '10'
      },
      {
        planDuration: 50,
        planContrbAmount: 50,
        paramAmortizationProfileId: '2',
        paramProductFamilyId: '1',
        remainingAmount: '5',
        remainingPercent: '10'
      },
      {
        planDuration: 50,
        planContrbAmount: 50,
        paramAmortizationProfileId: '2',
        paramProductFamilyId: '1',
        remainingAmount: '5',
        remainingPercent: '10'
      },
      {
        planDuration: 50,
        planContrbAmount: 50,
        paramAmortizationProfileId: '2',
        paramProductFamilyId: '1',
        remainingAmount: '5',
        remainingPercent: '10'
      }]
      fixture.plans = {
        plans: plansValue
      }
      jest.spyOn(fixture, 'adjustPlanTabsGrid').mockImplementation(() => {
        //this is intentional
      })
      fixture.ngOnInit();

      expect(fixture.addNewFundingPlan()).toEqual(undefined)

    });

    it('should open modalif for invalid plans', () => {

      plansValue = [{
        planDuration: null,
        planContrbAmount: null,
        paramAmortizationProfileId: null,
        paramProductFamilyId: null,
        remainingAmount: null,
        remainingPercent: null
      }]
      fixture.plans = {
        plans: plansValue
      }
      jest.spyOn(fixture, 'adjustPlanTabsGrid').mockImplementation(() => {
        //this is intentional
      })
      fixture.ngOnInit();

      expect(fixture.addNewFundingPlan()).toEqual(undefined)

    });

    it('should adjustPlanTabsGrid', () => {
      fixture.ngOnInit();

      jest.spyOn(fixture, 'isPlanControlsValid').mockReturnValue(true)
      jest.spyOn(fixture, 'adjustPlanTabsGrid').mockImplementation(() => {
        //this is intentional
      })
      fixture.addNewFundingPlan();
      fixture.updatePlansList();
      expect(fixture.adjustPlanTabsGrid).toHaveBeenCalled();

    })




  });


  describe('Test deleteFundingPlan', () => {
    it('should remove plan ', () => {
      plansValue = [{
        planDuration: null,
        planContrbAmount: null,
        paramAmortizationProfileId: null,
        paramProductFamilyId: null,
        remainingAmount: null,
        remainingPercent: null,
        id:'5'
      }]
      jest.spyOn(fixture, 'adjustPlanTabsGrid').mockImplementation(() => {
        //this is intentional
      })
      fixture.form.patchValue({ plans: plansValue });
      fixture.form.patchValue({ plansToDelete: plansValue });

      fixture.deleteFundingPlan(0);
      let planArray = (fixture.form.get('plans') as FormArray);
      expect(planArray.length).toEqual(1);


    });
  });

  describe('Test ngAfterViewInit', () => {
    it('should call adjustPlanTabsGrid ', () => {
      jest.spyOn(fixture, 'adjustPlanTabsGrid').mockImplementation(() => {
        //this is intentional
      });
      fixture.ngAfterViewInit();
      expect(fixture.adjustPlanTabsGrid).toHaveBeenCalled();

    });
  });



  describe('Test changeTab', () => {
    it('should fill pramAmortizationProfileOptionsByProductId ', () => {
      fixture.changeTab(0);
      expect(fixture.selectedPlan).toEqual(0);
    });

    it('should remove profilOptions ', () => {
      fixture.changeTab(3);
      expect(fixture.profilOptions).toEqual([]);
    });
  });


  describe('Test onBlurPlanContrbAmount', () => {
    it('should round planCtrbAmount', () => {

      fixture.form.patchValue({plans:[{planContrbAmount : "5.6333"}]})

      fixture.onBlurPlanContrbAmount(0);
      expect(fixture.value.plans[0].planContrbAmount).toEqual("5.63");
    });


  });



  describe('Test:  onBlurResidualValueAmount', () => {
    it('should return 10 when priceAmount=100000 residualValueAmount=10000', () => {

      fixture.priceAmount = 100000;

      fixture.plansControls.push(new FormGroup({
        remainingPercent: new FormControl(null),
      }));

      let planControl: FormControl = new FormControl('10000');

      fixture.onBlurResidualValueAmount(planControl, 0);

      expect(fixture.plansControls[0].get('remainingPercent').value).toEqual(10);



    });

  });
  describe('Test:  checkProfilList', () => {
    it('should return 10 when priceAmount=100000 residualValueAmount=10000', () => {
      const id = new FormControl(7);
      const spyInstance = jest.spyOn(fixture, 'loadProfilListByProductId');
      fixture.pramAmortizationProfileOptionsByProductId = [];
      fixture.checkProfilList(id);
      expect(spyInstance).toHaveBeenCalledWith(id);
    });

  });

  describe('Test:  checkForPopup', () => {
    it('should not change value', () => {
      forkJoin([
        paramProductFamilyServiceMock.getAllParamProductFamily,
        paramAmortizationProfileServiceMock.getAllParamProductFamily
      ]).subscribe((x) => {
        fixture.checkForPopup(0)
        expect(fixture.form.value.plans[0].remainingAmount).toEqual("5");
      })
    });

    it('should  change value', () => {
      popupServiceMock = {
        popupInfo: jest.fn().mockReturnValue(of(true)),
        onConfirm: of(false)
      };
      fixture = new PlanListComponent(
        formBuilderMock,
        modalServiceMock,
        popupServiceMock,
        paramAmortizationProfileServiceMock,
        paramProductFamilyServiceMock,
        valuationSharedServiceMock

      );
      fixture.plans = {
        plans: plansValue
      }
      jest.spyOn(fixture, 'adjustPlanTabsGrid').mockImplementation(() => {
        //this is intentional
      })
    fixture.ngOnInit();

      fixture.checkForPopup(0)
      expect(fixture.form.value.plans[0].remainingAmount).toEqual(0);
    });

  });


  describe('Test:  onBlurResidualValuePercent', () => {
    it('should return 10000 when price=100000 residualValuePercent=10', () => {

      fixture.priceAmount = 100000;

      fixture.plansControls.push(new FormGroup({
        remainingAmount: new FormControl(null),
      }));

      let planControl: FormControl = new FormControl('10');

      fixture.onBlurResidualValuePercent(planControl, 0);

      expect(fixture.plansControls[0].get('remainingAmount').value).toEqual("10000");

    });

  });


  describe('Test:  plansList', () => {
    it('should return plansList', () => {
      forkJoin([
        paramProductFamilyServiceMock.getAllParamProductFamily,
        paramAmortizationProfileServiceMock.getAllParamProductFamily
      ]).subscribe((x) => {
        expect(fixture.plansList).toEqual([{ "name": "Plan 1" }, { "name": "Plan 2" }]);
      })

    });

  });

  describe('Test:  validate', () => {
    it('should return null', () => {
      fixture.pramAmortizationProfileOptionsByProductId = []
      fixture.ngOnInit();

      forkJoin([
        paramProductFamilyServiceMock.getAllParamProductFamily,
        paramAmortizationProfileServiceMock.getAllParamProductFamily
      ]).subscribe((x) => {
        let form;
        fixture.form.enable();
        fixture.form.setValue(fixture.plans)
        expect(fixture.validate(form)).toEqual(null);
      })

    });
    it('should not retrun null', () => {
      forkJoin([
        paramProductFamilyServiceMock.getAllParamProductFamily,
        paramAmortizationProfileServiceMock.getAllParamProductFamily
      ]).subscribe((x) => {
        let form: any;
        fixture.form.controls['plans'].setErrors({ 'incorrect': true });
        expect(fixture.validate(form)).toEqual({ "plans": { "valid": false } });
      })


    });
  });

  describe('Test:  checkProductType', () => {
    it('should enable residual value', () => {

      fixture.productWithResidualValue = ["1", "3"];
      fixture.plansControls.push(new FormGroup({
        paramProductFamilyId: new FormControl('1'),
      }));

      let planControl: FormControl = new FormControl('1');

      fixture.checkProductType(planControl, 0);

      expect(fixture.disableUpdateResidualValue).toEqual(false);

    });
    it('should disable residual value', () => {

      fixture.productWithResidualValue = ["2", "3"];
      fixture.plansControls.push(new FormGroup({
        paramProductFamilyId: new FormControl({ id: "1", label: "tesy", paramAmortizationProfiles: null })
      }));

      let planControl: FormControl = new FormControl({ id: '1' });

      fixture.checkProductType(planControl, 0);

      expect(fixture.disableUpdateResidualValue).toEqual(true);

    });
  });
  describe('Test:  loadProfilListByProductId', () => {
    it('should update profile list', () => {

      fixture.pramAmortizationProfileOptionsByProductId = [];
      let listProfil = [{ id: "1", label: "test", paramProductFamily: null }
        , { id: "2", label: "sss", paramProductFamily: null }];
      fixture.productOptions = [{ id: "1", label: "tesy", paramAmortizationProfiles: listProfil } as ParamProductFamilyModel,
      { id: "2", label: "tesy", paramAmortizationProfiles: listProfil } as ParamProductFamilyModel,
      { id: "3", label: "tesy", paramAmortizationProfiles: listProfil } as ParamProductFamilyModel];
      let productForm: FormControl = new FormControl('1')

      fixture.loadProfilListByProductId(productForm);

      expect(fixture.pramAmortizationProfileOptionsByProductId).toEqual(listProfil);

    });

  });
  describe('Test:  changeApportLabel', () => {
    it('should update apport label ', () => {

      fixture.apportLabelList = ["Apport ou premier loyer majoré"];
      let productForm: FormControl = new FormControl('2');

      fixture.changeApportLabel(productForm, 0);

      expect(fixture.apportLabelList[0]).toEqual("Premier loyer majoré");

    });
    it('should update apport label  to apport', () => {

      fixture.apportLabelList = ["Apport ou premier loyer majoré"];
      let productForm: FormControl = new FormControl('1');

      fixture.changeApportLabel(productForm, 0);

      expect(fixture.apportLabelList[0]).toEqual("Apport ");

    });



  });

  describe('Test:  updateResidialValue', () => {
    it('should update apport label ', () => {

      fixture.apportLabelList = ["Apport ou premier loyer majoré"];
      let productForm: FormControl = new FormControl('3');
      let profileId: FormControl = new FormControl("3");
      fixture.updateResidialValue(productForm, profileId, 0);

      expect(fixture.disableUpdateResidualValue).toEqual(true);

    });
    it('should not update apport label ', () => {

      fixture.apportLabelList = ["Apport ou premier loyer majoré"];
      let productForm: FormControl = new FormControl('3');
      fixture.disableUpdateResidualValue = false;
      fixture.initProductListWithResidualValue()
      fixture.updateResidialValue(productForm, null, 0);

      expect(fixture.disableUpdateResidualValue).toEqual(false);

    });

  });

  describe('Test get value()', () => {
    it('should return form value', () => {
      expect(fixture.value).toEqual(fixture.form.getRawValue());
    })
  });

  describe('Test get f()', () => {
    it('should return form controls', () => {
      expect(fixture.f).toEqual(fixture.form.controls);
    })
  });

  describe('Test validate()', () => {
    it('should return false', () => {
      fixture.validate(null);
      expect(fixture.form.valid).toEqual(false);
    });
  });


  describe('Test writeValue()', () => {
    it('should  write value', () => {
      spyOn(fixture.form, 'reset').and.returnValue(true);

      let value = { "plans": [{ "paramAmortizationProfileId": "2", "paramProductFamilyId": "1", "planContrbAmount": 50, "planDuration": 50, "remainingAmount": "5", "remainingPercent": "10", "id": "1" , "isDuplicated":"false"},
       { "paramAmortizationProfileId": "2", "paramProductFamilyId": "1", "planContrbAmount": 50, "planDuration": 50, "remainingAmount": "5", "remainingPercent": "10", "id": "1" , "isDuplicated":"false"
    }], "plansToDelete": [] };

      fixture.writeValue(value);
      expect(fixture.value).toEqual(value);
    });

    it('should reset be truthy', () => {
      spyOn(fixture.form, 'reset').and.returnValue(true);

      let value = null;
      fixture.writeValue(value);
      expect(fixture.form.reset).toBeTruthy();
    });
  });

  describe('Test get plansList()', () => {
    it('should be truthy', () => {
      expect(fixture.plansList).toBeTruthy();
    })
  });

  describe('Test  set plansListModel', () => {
    it('should set  set plansList', () => {
      jest.spyOn(fixture, 'adjustPlanTabsGrid').mockImplementation(() => {
      })
      fixture.plansListModel = { "plans": plansValue };
      expect(fixture.form.value.plans).toBeDefined()

    });
  });

  describe('Test  set price', () => {
    it('should set  priceAmount ', () => {
      fixture.price = 5;
      expect(fixture.priceAmount).toEqual(5)


    });
  });

  describe('Test markAsTouched()', () => {
    it('should set  form Touched', () => {
      fixture.markAsTouched();
      expect(fixture.form.touched).toEqual(true);
    });
  });

  describe('Test setDisabledState', () => {
    it('should lock form', () => {
      fixture.setDisabledState(true);
      expect(fixture.form).toHaveBeenCalled
    });

    it('should unlock form', () => {
        fixture.setDisabledState(false);
        expect(fixture.form).toHaveBeenCalled
      });

  });

  describe('Test set displayOnly', () => {
    it('should set form disabled when displayOnly is setted to true ', () => {
      fixture.displayOnly = true;
      expect(fixture.form.disabled).toEqual(true);
      expect(fixture.readOnly).toEqual(true);
    });
  });

  describe('Test:  onBlurDuration', () => {
    it('should enabled paramAmortizationProfileId', () => {
      let planControl={
        value:'24'
      } as FormControl

      fixture.onBlurDuration(planControl,0)
      expect((fixture.form.controls['plans'] as FormArray).at(0).get('paramAmortizationProfileId').enabled).toEqual(true)
    });
    it('should disabled paramAmortizationProfileId', () => {
      let planControl={
        value:'25'
      } as FormControl

      fixture.onBlurDuration(planControl,0)
      expect((fixture.form.controls['plans'] as FormArray).at(0).get('paramAmortizationProfileId').disabled).toEqual(true)
    });
    it('should enabled paramAmortizationProfileId when value is null', () => {
      let planControl={
        value:null
      } as FormControl

      fixture.onBlurDuration(planControl,0)
      expect((fixture.form.controls['plans'] as FormArray).at(0).get('paramAmortizationProfileId').enabled).toEqual(true)
    });
  });

});



