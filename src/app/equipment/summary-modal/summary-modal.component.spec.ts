import {TestBed} from '@angular/core/testing';
import {UrlTree} from '@angular/router';
import {of} from 'rxjs';
import {FinancialPlanModel} from '../../shared/models/financial-plan.model';
import {ParamSettingModel} from '../../shared/models/param-setting.model';
import {ValuationRequestModel} from '../../shared/models/valuation-request.model';
import {ValuationDocumentModel} from '../../shared/models/valuationDocument.model';
import {ValuationSharedService} from '../../shared/services/valuation-shared-service';
import {StatusConstant} from '../../shared/status-constant';
import {SummaryModalComponent} from './summary-modal.component';

describe('SummaryModalComponent', () => {
  let fixture: SummaryModalComponent;
  let activeModal: any;
  let valuationRequestServiceMock: any;
  let valuationRequestDocsServiceMock: any;

  let popupServiceMock: any;
  let routerMock: any;
  let financialPlanServiceMock: any
  let plan: FinancialPlanModel;
  let entrepriseInfoServiceMock: any;
  let refUnderwriterServiceMock: any;
  let paramSettingServiceMock: any;
  let valuationRequestWorkflowMock: any;
  let mdmInformationServiceMock: any;
  let valuationSharedServiceMock: any;
  let loadingService: any;


  beforeEach(() => {


    valuationRequestServiceMock = {
      addValuationRequest: jest.fn().mockReturnValue(of({id: '2'})),
    };
    valuationRequestDocsServiceMock = {
      uploadRequestDocument: jest.fn().mockReturnValue(of(true)),
      updateBssRespDocuments: jest.fn().mockReturnValue(of(true)),
      deleteDocuments: jest.fn().mockReturnValue(of(true)),

    };

    valuationRequestWorkflowMock = {
      saveValuationRequestWorkflow: jest.fn().mockReturnValue(of({
        id: '11',
        label: 'En attente',
        value: StatusConstant.STATUS_CREATED
      } as ParamSettingModel))
    }
    popupServiceMock = {
      popupInfo: jest.fn().mockReturnValue(of(true)),
      onConfirm: of(true)
    };
    activeModal = {
      close: jest.fn(),
      dismiss: jest.fn()
    };
    financialPlanServiceMock = {
      saveFinancialPlan: jest.fn().mockReturnValue(of(true)),
      removeFinancialPlan: jest.fn().mockReturnValue(of([true])),


    }
    paramSettingServiceMock = {}
    refUnderwriterServiceMock = {
      saveRefUnderwriter: jest.fn().mockReturnValue(of(true))
    }
    let clientInfo = {
      id: '1',
      subsidiary: false,
      cp: '1',
      description: 'description',
      siren: '555',
      sirenType: 'xx',
      socialReason: 'client name',
      delegation: 'delegation',
      regionalDirection: 'regional direction',
      network: 'TCP'
    }
    entrepriseInfoServiceMock = {
      entrepriseInfo: clientInfo

    }
    paramSettingServiceMock = {
      getParamByCode: jest.fn().mockReturnValue(of([{
        id: '11',
        label: 'En attente',
        value: StatusConstant.WAIT_EXP_VAL
      } as ParamSettingModel]))
    }
    mdmInformationServiceMock = {
      getMdmCaInformation: jest.fn().mockReturnValue(of({
        desc: {
          id: '52'
        }
      })),
    }

    routerMock = {
      navigateByUrl: jest.fn().mockImplementation(() => Promise.resolve(true)),

      navigate: jest.fn().mockImplementation(() => Promise.resolve(true)),
      url: jest.fn(() => '/search'),
      serializeUrl: jest.fn().mockReturnValue('url/url'),
      createUrlTree: jest.fn().mockReturnValue(new UrlTree())
    };

    loadingService = {
      show: jest.fn().mockReturnValue(of(true)),
      hide: jest.fn().mockReturnValue(of(true)),
    }


    valuationSharedServiceMock = TestBed.inject(ValuationSharedService);

    fixture = new SummaryModalComponent(
      activeModal,
      valuationRequestServiceMock,
      valuationRequestDocsServiceMock,
      popupServiceMock,
      routerMock,
      financialPlanServiceMock,
      entrepriseInfoServiceMock,
      refUnderwriterServiceMock,
      paramSettingServiceMock,
      valuationRequestWorkflowMock,
      mdmInformationServiceMock,
      valuationSharedServiceMock,
      loadingService
    );
    plan = {} as FinancialPlanModel;
    fixture.valuationRequest = {} as ValuationRequestModel;
    fixture.financialPlanRequest = {
      plans: [plan]
    }
    fixture.ngOnInit();
  });

  describe('Test: submit SummaryModalComponent', () => {
    it('should submit value and init workflow', () => {
      let plan: any;
      fixture.financialPlanRequest.plansToDelete = [plan];
      fixture.documentsListToDelete = [{

        "docName": "document1",
      } as ValuationDocumentModel, {

        "docName": "document2"
      } as ValuationDocumentModel]
      fixture.documentsListToEdit = [{

        "docName": "document1",
      } as ValuationDocumentModel, {

        "docName": "document2"
      } as ValuationDocumentModel]
      let file: any;
      fixture.files = [file];
      jest.spyOn(fixture, 'reloadCurrentRoute').mockImplementation(() => {
        //this is intentional
      })
      fixture.isAddMode = true;
      fixture.submit();
      setTimeout(() => {
        expect(popupServiceMock.popupInfo).toHaveBeenCalled();
        expect(fixture.financialPlanRequest.plans[0].valuationRequest).toEqual({id: '2'});
      }, 10);


    });

    it('should submit value and dont init workflow', () => {
      let plan: any;
      fixture.financialPlanRequest.plansToDelete = [plan];
      fixture.documentsListToDelete = [{

        "docName": "document1",
      } as ValuationDocumentModel, {

        "docName": "document2"
      } as ValuationDocumentModel]
      fixture.documentsListToEdit = [{

        "docName": "document1",
      } as ValuationDocumentModel, {

        "docName": "document2"
      } as ValuationDocumentModel]
      let file: any;
      fixture.files = [file];
      jest.spyOn(fixture, 'reloadCurrentRoute').mockImplementation(() => {
        //this is intentional
      })
      fixture.isAddMode = false;
      fixture.submit();
      setTimeout(() => {
        expect(popupServiceMock.popupInfo).toHaveBeenCalled();
        expect(fixture.financialPlanRequest.plans[0].valuationRequest).toEqual({id: '2'});
      }, 10);


    });


    it('should submit value and init workflow', () => {
      let plan: any;
      popupServiceMock = {
        popupInfo: jest.fn().mockReturnValue(of(false)),
        onConfirm: of(false)
      };
      fixture = new SummaryModalComponent(
        activeModal,
        valuationRequestServiceMock,
        valuationRequestDocsServiceMock,
        popupServiceMock,
        routerMock,
        financialPlanServiceMock,
        entrepriseInfoServiceMock,
        refUnderwriterServiceMock,
        paramSettingServiceMock,
        valuationRequestWorkflowMock,
        mdmInformationServiceMock,
        valuationSharedServiceMock,
        loadingService
      );
      plan = {} as FinancialPlanModel;
      fixture.valuationRequest = {} as ValuationRequestModel;
      fixture.financialPlanRequest = {
        plans: [plan]
      }
      fixture.ngOnInit();
      fixture.financialPlanRequest.plansToDelete = [plan];
      fixture.documentsListToDelete = [{

        "docName": "document1",
      } as ValuationDocumentModel, {

        "docName": "document2"
      } as ValuationDocumentModel]
      fixture.documentsListToEdit = [{

        "docName": "document1",
      } as ValuationDocumentModel, {

        "docName": "document2"
      } as ValuationDocumentModel]
      let file: any;
      fixture.files = [file];
      jest.spyOn(fixture, 'reloadCurrentRoute').mockImplementation(() => {
        //this is intentional
      })
      fixture.isAddMode = true;
      fixture.submit();
      setTimeout(() => {
        expect(popupServiceMock.popupInfo).toHaveBeenCalled();
        expect(fixture.disabled).toBeTruthy();
      }, 10);


    });


  });

  describe('Test: reloadCurrentRoute SummaryModalComponent', () => {
    it('should reload ihm', () => {
      fixture.reloadCurrentRoute();
      setTimeout(() => {
        expect(routerMock.navigate).toHaveBeenCalledWith([routerMock.url]);
      }, 10);


    });
  });


  describe('Test: press exit', () => {
    it('should exit', () => {

      spyOn(fixture.onExit, "emit");
      fixture.disabled = true;

      fixture.exit();
      expect(fixture.onExit.emit).toHaveBeenCalled()


    });
  });


});

