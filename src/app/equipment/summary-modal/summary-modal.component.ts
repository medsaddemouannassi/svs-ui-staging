import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { forkJoin } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ValuationRequestModel } from 'src/app/shared/models/valuation-request.model';
import { ConfirmationModalComponent } from '../../shared/confirmation-modal/confirmation-modal.component';
import { MdmInformationsModel } from '../../shared/models/mdm-informations.model';
import { ParamAmortizationProfileModel } from '../../shared/models/param-amortization-profile.model';
import { ParamProductFamilyModel } from '../../shared/models/param-product-family.model';
import { ParamSettingModel } from '../../shared/models/param-setting.model';
import { RefUnderwriterModel } from '../../shared/models/ref-underwriter.model';
import { ValuationRequestWorkflowModel } from '../../shared/models/valuation-request-workflow.model';
import { ValuationDocumentModel } from '../../shared/models/valuationDocument.model';
import { WorkflowInstanceVariable } from '../../shared/models/workflow-instance-variable.model';
import { EntrepriseInfoService } from '../../shared/services/entreprise-info.service';
import { FinancialPlanService } from '../../shared/services/financial-plan.service';
import { MdmInformationService } from '../../shared/services/mdm-informations.service';
import { ParamSettingService } from '../../shared/services/param-setting.service';
import { PopupService } from '../../shared/services/popup.service';
import { RefUnderwriterService } from '../../shared/services/ref-underwriter.service';
import { ValuationRequestDocsService } from '../../shared/services/valuation-request-docs.service';
import { ValuationRequestWorkflowService } from '../../shared/services/valuation-request-workflow.service';
import { ValuationSharedService } from '../../shared/services/valuation-shared-service';
import { ValuationRequestService } from '../../shared/services/valuation.request.service';
import { StatusConstant } from '../../shared/status-constant';
import { LoadingService } from '../../shared/services/loading-service.service';

@Component({
  selector: 'app-summary-modal',
  templateUrl: './summary-modal.component.html',
  styleUrls: ['./summary-modal.component.css']
})
export class SummaryModalComponent implements OnInit {

  valuationRequest: ValuationRequestModel;
  financialPlanRequest: any;
  statusList: ParamSettingModel[];
  bssResponsible: MdmInformationsModel;

  files: File[];
  refUnderwrite: RefUnderwriterModel;
  isAddMode: any;
  documentsListToEdit: ValuationDocumentModel[] = [];
  documentsListToDelete: ValuationDocumentModel[] = [];
  disabled = false;

  @Output() onExit = new EventEmitter<boolean>();

  constructor(public activeModal: NgbActiveModal,
    private valuationRequestService: ValuationRequestService,
    private valuationRequestDocsService: ValuationRequestDocsService,

    private popupService: PopupService,
    private router: Router,
    private financialPlanService: FinancialPlanService,
    private entrepriseInfoService: EntrepriseInfoService,
    private refUnderwriterService: RefUnderwriterService,
    private paramSettingService: ParamSettingService,
    private valuationRequestWorkflow: ValuationRequestWorkflowService,
    private mdmInformationService: MdmInformationService,

    private valuationSharedService: ValuationSharedService,
    private loadingService: LoadingService
  ) { }

  ngOnInit(): void {
    this.mdmInformationService.getMdmCaInformation(this.valuationRequest.bssResponsible).subscribe(resp => {
      this.bssResponsible = resp;

    })


    this.paramSettingService.getParamByCode(StatusConstant.STATUS_VAL_WKFL).subscribe(res => {
      this.statusList = res;
    })

  }

  submit(): void {
    console.log("started");

    this.showLoading()
    this.valuationRequestService.addValuationRequest(this.valuationRequest).subscribe(data => {

      console.log(data.id);




      this.financialPlanRequest.plans.forEach(plan => {
        plan.valuationRequest = data;
        let paramAmortizationProfile: ParamAmortizationProfileModel = {} as ParamAmortizationProfileModel;
        paramAmortizationProfile.id = plan.paramAmortizationProfileId;

        plan.paramAmortizationProfile = paramAmortizationProfile;


        let paramProductFamily: ParamProductFamilyModel = {} as ParamProductFamilyModel;
        paramProductFamily.id = plan.paramProductFamilyId;

        plan.paramProductFamily = paramProductFamily;
      });
      this.financialPlanService.saveFinancialPlan(this.financialPlanRequest.plans).subscribe();
      if (this.isAddMode) {
        let taskCreate = {} as ValuationRequestWorkflowModel;
        taskCreate.status = this.statusList.find(status => status.value == StatusConstant.STATUS_CREATED);
        taskCreate.valuationRequest = data;
        taskCreate.variable = {} as WorkflowInstanceVariable;
        taskCreate.variable.bssResponsible = this.bssResponsible;
        taskCreate.variable.companyName = this.entrepriseInfoService.entrepriseInfo.socialReason;
        taskCreate.variable.urlRequest = location.origin + '/#' + this.router.serializeUrl(this.router.createUrlTree(['reception', 'expert', data.id]))



        this.valuationRequestWorkflow.saveValuationRequestWorkflow(taskCreate).subscribe(task => {
          let taskWaitExpert = {} as ValuationRequestWorkflowModel;
          taskWaitExpert.status = this.statusList.find(status => status.value == StatusConstant.WAIT_EXP_VAL);
          taskWaitExpert.valuationRequest = data;

          this.valuationRequestWorkflow.saveValuationRequestWorkflow(taskWaitExpert).subscribe();
        });
      }
      if (this.financialPlanRequest.plansToDelete && this.financialPlanRequest.plansToDelete.length > 0) {
        this.financialPlanService.removeFinancialPlan(this.financialPlanRequest.plansToDelete).subscribe();

      }
      if (this.documentsListToEdit && this.documentsListToEdit.length > 0) {
        this.valuationRequestDocsService.updateBssRespDocuments(this.documentsListToEdit).subscribe();

      }

      if (this.documentsListToDelete && this.documentsListToDelete.length > 0) {
        this.valuationRequestDocsService.deleteDocuments(this.documentsListToDelete).subscribe();

      }
      if (this.files && this.files.length > 0) {
        let requests = [];
        for (let i = 0; i < this.files.length; i++) {



          requests.push(this.valuationRequestDocsService.uploadRequestDocument(this.files[i], data, this.entrepriseInfoService.entrepriseInfo).pipe(catchError(() => null)));

        }

        forkJoin(requests).subscribe()


      }



    },
      () => {
        this.hideLoading();
       },
      () => {
        if (this.isAddMode) {
          /*
        this.popupService.popupInfo('Votre demande de valorisation équipement a bien été prise en compte. Vous pouvez dorénavant fermer la fenêtre ou saisir :'
          , 'Nouvelle demande', '(pour le même client)', null, ConfirmationModalComponent);
        const subscription = this.popupService.onConfirm.subscribe((t) => {
          }
          subscription.unsubscribe();
        });
        */
          this.popupService.popupInfo("Votre demande de valorisation équipement a bien été prise en compte."
            , "Créer", null, "Fermer", ConfirmationModalComponent, false, "Voulez-vous créer une nouvelle demande pour le même client?");
          const confirmSubscription = this.popupService.onConfirm.subscribe((c) => {
            if (c) {
              this.reloadCurrentRoute();
              this.valuationSharedService.resetAllValue();
            } else {
              this.disabled = true;
              this.valuationSharedService.resetAllValue();
              this.activeModal.dismiss('done');
              this.activeModal.close()
              this.exit();
            }
            confirmSubscription.unsubscribe();
            this.activeModal.close()
          });

        } else {
          this.activeModal.close()
        }
        console.log("ended");
        this.hideLoading();

      });
  }
  //reload same route with same vars
  reloadCurrentRoute() {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate([currentUrl]).then(r => r);
    });

  }

  exit() {
    this.onExit.emit(this.disabled);
  }
  private showLoading() {
    this.loadingService.show();

  }


  private hideLoading() {
    this.loadingService.hide();

  }
}
