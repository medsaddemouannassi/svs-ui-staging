import { FormBuilder } from '@angular/forms';
import { of, Subscription ,throwError} from 'rxjs';

import { ClientInfoComponent } from './client-info-component.component';
import { ValuationRequestModel } from '../../shared/models/valuation-request.model';
import { EntrepriseInfoModel } from '../../shared/models/entreprise-info.model';
import { MdmInformationsModel } from '../../shared/models/mdm-informations.model';

describe('ClientInfoComponentComponent', () => {
  let fixture: ClientInfoComponent;
  let entrepriseInfoServiceMock: any;
  let formBuilderMock: FormBuilder;
  let routerMock: any;
  let subscriptionMock: Subscription;
  let mdmInformationServiceMock: any;
  let authServiceMock: any;

  beforeEach(() => {
    subscriptionMock = new Subscription();

    entrepriseInfoServiceMock = {
      affiliateChanged: of(true),
      loadEntrepriseInfo: jest.fn().mockReturnValue(of({mainUnderwId:'55' } as EntrepriseInfoModel)),
      noResultFound: false,
      entrepriseInfo :null
    };
    formBuilderMock = new FormBuilder();
    routerMock = {
      snapshot: {
        paramMap: {
          get: (key: string) => {
            switch (key) {
              case 'idRefTiers': return '1';
              case 'idBcp': return '1';
            }
          },
        },
      }

    };
    const mdminfo = {
      reseau:'test',
      direction:'dd',
      delegation:'',

    } as MdmInformationsModel
    mdmInformationServiceMock ={
      getMdmCaInformation: jest.fn().mockReturnValue(of(mdminfo)),
    }
    authServiceMock = {
      'identityClaims' : {
          'mat': 'M102'
      },
      'currentUserRolesList': ['ROLE_SVS_ADMIN_SE']
    }

    fixture = new ClientInfoComponent(
      formBuilderMock,
      entrepriseInfoServiceMock,
      routerMock,
      mdmInformationServiceMock,
      authServiceMock
    );

    fixture.ngOnInit();
  });


  describe('Test: ngOnInit', () => {
    it('should initialize form', () => {
      const form = {
        affiliate: true,
      };
      expect(fixture.form.value).toEqual(form);
    });

    it('should change form', () => {
      const form = {
        affiliate: false,
      };
      fixture.form.controls['affiliate'].setValue(false);

      expect(fixture.form.value).toEqual(form);
    });
    it('should set Affiliat true', () => {
       //entrepriseInfoServiceMock.entrepriseInfo.mainUnderwId ='55'

        fixture.ngOnInit();
        expect(fixture.isAffiliate).toEqual(true);
    });

    describe('Test: setvaluationRequest', () => {
      it('should  load entrepriseInfo', () => {
        fixture.valuationRequest={
          isRetUnderwriter:false,
          underwriterId:'1'
        } as ValuationRequestModel;
         expect(fixture.entrepriseInfoService.loadEntrepriseInfo).toHaveBeenCalled();
      });

      it('should enter if block ', () => {
        fixture.valuationRequest={
          isRetUnderwriter:true,
          underwriterId:'1'
        } as ValuationRequestModel;
        expect(fixture.entrepriseInfoService.loadEntrepriseInfo).toHaveBeenCalled();
      });
    });



    describe('Test: setvaluationRequest', () => {
      it('should  load entrepriseInfo', () => {
        fixture.valuationRequest={} as ValuationRequestModel;
        expect(fixture.entrepriseInfoService.loadEntrepriseInfo).toHaveBeenCalled();
      });


    });


    it('should noResultFound is true', () => {
      routerMock = {
        snapshot: {
          paramMap: {
            get: (key: string) => {
              switch (key) {
                case 'idRefTiers': return 'null';
                case 'idBcp': return 'null';
              }
            },
          },
        }


      };
      entrepriseInfoServiceMock.entrepriseInfo=null;
      fixture = new ClientInfoComponent(
        formBuilderMock,
        entrepriseInfoServiceMock,
        routerMock,
        mdmInformationServiceMock,
        authServiceMock
      );

      fixture.ngOnInit();
      expect(fixture.entrepriseInfoService.noResultFound).toEqual(true);
    });

    it('should entrepriseInfoServiceMock return error', () => {
      entrepriseInfoServiceMock = {
        affiliateChanged: jest.fn(),
        loadEntrepriseInfo: jest.fn().mockImplementation(() => {
          return throwError(new Error('my error message'));
        }),
        noResultFound: false,
        entrepriseInfo:null
      };
      fixture = new ClientInfoComponent(
        formBuilderMock,
        entrepriseInfoServiceMock,
        routerMock,
        mdmInformationServiceMock,
        authServiceMock
      );

      fixture.ngOnInit();
      expect(fixture.entrepriseInfoService.noResultFound).toEqual(true);
    });

  });
  describe('Test ngOnDestroy', () => {
    it('should unsubscribe', () => {
      fixture.ngOnDestroy();
      const spyunsubscribe = jest.spyOn(subscriptionMock, 'unsubscribe');
      expect(spyunsubscribe).toBeDefined();
    });
  });


  describe('Test onTouched', () => {
    it('should get touched', () => {
      let test;
      fixture.registerOnTouched(test);
      expect(fixture.onTouched).not.toEqual(() => {       //this is intentional
});
      expect(fixture.onTouched).toEqual(test);

    });
  });



  describe('Test writeValue', () => {
    it('should  write value', () => {
      let value = ({
        affiliate: [true]
      });

      fixture.writeValue(value);
      expect(fixture.value).toEqual(value);



    });
    it('should not write value', () => {
      let value = ({
        affiliate: [true]
      });

      fixture.writeValue(value);
      value = null;
      fixture.writeValue(value);
      expect(fixture.value).not.toEqual(value);


    });
  });
  describe('Test registerOnChange', () => {
    it('should get register', () => {
      let test;
      fixture.registerOnChange(test);
      expect(fixture.onChange).not.toEqual(() => {       //this is intentional
});
      expect(fixture.onChange).toEqual(test);

    });
  });







  describe('Test validate', () => {
    it('should retrun null', () => {
      let form: any;
      expect(fixture.validate(form)).toEqual(null);

    });

    it('should not retrun null', () => {
      let form: any;
      fixture.form.controls['affiliate'].setErrors({ 'incorrect': true });
      expect(fixture.validate(form)).toEqual({ "clientInfo": { "valid": false } });

    });
  });



  describe('Test spy', () => {
    it('should return false', () => {
      fixture.test();
       jest.spyOn(fixture, 'test').mockReturnValue(false);
      expect(fixture.test()).toBe(false)

    });
  });
  describe('Test loadMdmInformation', () => {
    it('should call getMdmCaInformation', () => {
      let idCa='1';
      const mdminfo = {
        reseau:'test',
        direction:'dd',
        delegation:'',
      } as MdmInformationsModel

      fixture.loadMdmInformation(idCa);


      expect(fixture.mdmInformation).toEqual(mdminfo);

    });
  });
});

