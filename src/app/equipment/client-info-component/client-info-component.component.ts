import { Component, forwardRef, Input, OnDestroy, OnInit, Output,EventEmitter } from '@angular/core';
import { ControlValueAccessor, FormBuilder, FormControl, FormGroup, NG_VALIDATORS, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { EntrepriseInfoModel } from 'src/app/shared/models/entreprise-info.model';
import { EntrepriseInfoService } from 'src/app/shared/services/entreprise-info.service';
import { ValuationRequestModel } from '../../shared/models/valuation-request.model';
import { MdmInformationService } from '../../shared/services/mdm-informations.service';
import { MdmInformationsModel } from '../../shared/models/mdm-informations.model';
import { AuthService } from '../../shared/services/auth.service';
@Component({
  selector: 'app-client-info-component',
  templateUrl: './client-info-component.component.html',
  styleUrls: ['./client-info-component.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ClientInfoComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => ClientInfoComponent),
      multi: true
    }
  ]
})
export class ClientInfoComponent implements OnInit, ControlValueAccessor, OnDestroy {

  get value() {
    return this.form.value;
  }

  set value(value) {
    this.form.setValue(value);
    this.onChange(value);
    this.onTouched();
  }



  constructor(private fb: FormBuilder,
    public entrepriseInfoService: EntrepriseInfoService,
    private route: ActivatedRoute, private mdmInformationService : MdmInformationService,  private authService: AuthService) { }

  form: FormGroup;
  subscriptions: Subscription[] = [];
  matCA:string ;
  mdmInformation: MdmInformationsModel;
  isAffiliate:boolean = false;
  @Output() clientInfoLoaded = new EventEmitter();

  @Input() set valuationRequest(valuationRequest: ValuationRequestModel) {
    if (valuationRequest !== null && valuationRequest!==undefined && valuationRequest.underwriterId!=null && valuationRequest.underwriterId!=undefined && !this.entrepriseInfoService.entrepriseInfo) {
      let idRefTiers = null;
      let idBcp = null;

      if(valuationRequest.isRetUnderwriter){
        idRefTiers=valuationRequest.underwriterId;
      }else{
        idBcp=valuationRequest.underwriterId;
      }
      this.entrepriseInfoService.loadEntrepriseInfo(idRefTiers, idBcp).subscribe(
        (data: EntrepriseInfoModel) => {
          this.entrepriseInfoService.entrepriseInfo = data;
        },
        (error) => {
          this.entrepriseInfoService.noResultFound = true;
          this.clientInfoLoaded.emit();
        },()=>{
          this.loadMdmInformation(valuationRequest.bssResponsible);
          
          // test if contrepartie value is present

          if (this.entrepriseInfoService.entrepriseInfo.mainUnderwId) {
            this.isAffiliate = true;
            this.form.patchValue({
              affiliate: true
            });
          }
          this.clientInfoLoaded.emit();

        }
      )
    }


  }

  ngOnInit(): void {

    this.form = this.fb.group({
      affiliate: [true]
    });
    this.subscriptions.push(
      this.form.valueChanges.subscribe(value => {
        this.entrepriseInfoService.affiliateChanged.next(value.affiliate);
        this.onChange(value);
        this.onTouched();
      })
    );
    const idRefTiers: string = this.route.snapshot.paramMap.get('idRefTiers');
    const idBcp: string = this.route.snapshot.paramMap.get('idBcp');
    if (idRefTiers !== null && idBcp !== null && idRefTiers != undefined && idBcp !=undefined && !this.entrepriseInfoService.entrepriseInfo) {
      this.matCA = this.authService.identityClaims['mat'];
      if (idRefTiers === 'null' && idBcp === 'null') {
        this.entrepriseInfoService.noResultFound = true;
      } else {
        this.entrepriseInfoService.loadEntrepriseInfo(idRefTiers, idBcp).subscribe(
          (data: EntrepriseInfoModel) => {
            this.entrepriseInfoService.entrepriseInfo = data;
            this.clientInfoLoaded.emit();

          },
          (error) => {
            this.entrepriseInfoService.noResultFound = true;
            this.clientInfoLoaded.emit();

          },
          ()=>{
           this.loadMdmInformation(this.matCA);
           // test if contrepartie value is present
           
           if(this.entrepriseInfoService.entrepriseInfo.mainUnderwId){
             this.isAffiliate = true;
             this.form.patchValue({
               affiliate:true
             });
           }
          }
        );
      }
    }

  }

  ngOnDestroy() {

    this.subscriptions.forEach(s => s.unsubscribe());
    this.entrepriseInfoService.entrepriseInfo=null;
  }

  onChange: any = () => {
    // This is intentional
   };
  onTouched: any = () => {
   // This is intentional
  };

  registerOnChange(fn) {
    this.onChange = fn;
  }

  writeValue(value) {
    if (value) {
      this.value = value;
    }

    if (value === null) {
      this.form.reset();
    }
  }

  registerOnTouched(fn) {
    this.onTouched = fn;
  }

  validate(_: FormControl) {
    return this.form.valid ? null : { clientInfo: { valid: false, }, };
  }

  test(): boolean {
    return false;
  }
  loadMdmInformation(idCA: string){
    this.mdmInformationService.getMdmCaInformation(idCA).subscribe( data =>{
      this.mdmInformation = data;
      this.entrepriseInfoService.entrepriseInfo.delegation = this.mdmInformation.delegation;
      this.entrepriseInfoService.entrepriseInfo.regionalDirection= this.mdmInformation.direction;
      this.entrepriseInfoService.entrepriseInfo.network = this.mdmInformation.reseau;
    })
  }
}
