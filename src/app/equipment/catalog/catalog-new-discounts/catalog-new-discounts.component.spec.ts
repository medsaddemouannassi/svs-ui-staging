
import { FormBuilder } from '@angular/forms';
import { of, Subscription } from 'rxjs';
import { CatDiscountRateModel } from '../../../shared/models/cat-discount-rate.model';
import { CatDiscountModel } from '../../../shared/models/cat-discount.model';
import { CatDocumentModel } from '../../../shared/models/cat-document.model';
import { CatalogNewDiscountsComponent } from './catalog-new-discounts.component';
import { CatNatureModel } from '../../../shared/models/cat-nature.model';
import { CatBrandModel } from 'src/app/shared/models/cat-brand.model';
import { CatActivitySectorModel } from 'src/app/shared/models/cat-activity-sector.model';

describe('CatalogNewDiscountsComponent', () => {
  let fixture: CatalogNewDiscountsComponent;
  let formBuilderMock: any;
  let popupServiceMock: any;
  let catDiscountServiceMock: any;
  let catDiscountObject: CatDiscountModel = {} as any;
  let catdiscount: CatDiscountModel = {} as any;
  let catdiscountRateList: CatDiscountRateModel[] = []
  let catDocumentServiceMock: any;
  let subscriptionMock:Subscription;
  let documentsList: CatDocumentModel[] = [{

    "docName": "document1",
  } as CatDocumentModel, {

    "docName": "document2"

  } as CatDocumentModel];

  beforeEach(() => {
    subscriptionMock = new Subscription();

    let  inheritedDocBrand = [{
      docName: 'file3.png', id: '1', catActivitySector: null, catNature: {
      id: '133',
      label: 'nature label',
      catActivitySector: null },
       catBrand: null,
      catAssetType: null,
      discriminator: 'N',
      docVersion: 1,
       gedPath: '17/file3.png'
    } as CatDocumentModel]

    catDiscountObject = {
      id: '66',
      maxFinPer: 52,
      minFinPer: 33,
      discount: 25,
      catBrand: null,
      catNature: null,
      catActivitySector: null,
      isPerm: true,
      activationDate: null,
      endDate: null,
      catAssetType: null,
      enableRecov: null,
      enableValuation: null,
      ponderationRate: null,
      creation: '20',
      lastValuationDone: null,
      isReadjust: false,
      isNew: true,
      comment: 'comment nature',
      startDateAverLabel:'',
       endDateAverLabel :'',
      nbMinValAver:3
    }

    catDiscountObject.catNature = {
      id: '133',
      label: 'nature label',
      catActivitySector: {
        id: '1',
        label: 'Transport',
        comment: 'comment act sector',
        isActive: true
      },
      comment: 'comment nature',
      paramNap: null,
      isActive:true,
      isValid: false,
      isEnrg: false

    } as CatNatureModel;

    catDiscountObject.catActivitySector = {
      id: '1',
      label: 'Transport',
      comment: 'comment act sector',
      isActive: true
    }
    catdiscountRateList = [{
      id: "1",
      catDiscount: catdiscount,
      discountRateYear: 2012,
      discountRateValue: 12
    },{
      id: "2",
      catDiscount: null,
      discountRateYear: 2012,
      discountRateValue: 12},
      {
        id: "3",
        catDiscount: null,
        discountRateYear: 2012,
        discountRateValue: 20
      },
      {
        id: "4",
        catDiscount: null,
        discountRateYear: 2012,
        discountRateValue: 5
      }
    ];

    popupServiceMock = {
      popupInfo: jest.fn().mockReturnValue(of(true)),
      onConfirm: of(true)
    };

    catDiscountServiceMock = {
      getDiscountDetailsByIdAndDesc: jest.fn().mockReturnValue(of(catDiscountObject))
    }
    catDocumentServiceMock={
      uploadDocument:jest.fn().mockReturnValue(of(true)),
      downloadDocument:jest.fn().mockReturnValue(of(true)),
      searchInheratedDocuments:jest.fn().mockReturnValue(of(inheritedDocBrand)),
      updateDocument:jest.fn().mockReturnValue(of(true)),
      deleteCatDocument:jest.fn().mockReturnValue(of(true)),
    }

    formBuilderMock = new FormBuilder();

    fixture = new CatalogNewDiscountsComponent(formBuilderMock, popupServiceMock, catDiscountServiceMock, catDocumentServiceMock);
    fixture.catDiscountRateList = catdiscountRateList
    fixture.catDiscountObject = catDiscountObject

    fixture.ngOnInit();
    fixture.createForm()
  });
  describe('Test cat-new-discount', () => {
    it('should Be Truthy', () => {
      expect(fixture).toBeTruthy();
    })
  });
  describe('Test: createForm', () => {
    it('should create form ', () => {
      fixture.createForm();
      expect(fixture.catNewDiscForm).toBeTruthy();
    });


  });
  describe('Test: initComponent', () => {
    it('should   call initForm  ', () => {
      fixture.initComponent();
      fixture.isUnlocked = true;

      expect(fixture.initForm()).toHaveBeenCalled;
    });
    it('should  unlock form ', () => {
      fixture.initComponent();

      fixture.isUnlocked = true;
      expect(fixture.unlockForm()).toHaveBeenCalled;
    });
    it('should  lock form ', () => {
      fixture.initComponent();

      fixture.isUnlocked = false;
      expect(fixture.lockForm()).toHaveBeenCalled;
    });
  });
  describe('Test: onBlurPercentage', () => {
    it('should   update set percet to null if value > 100 ', () => {
      fixture.createForm();
      fixture.catNewDiscForm.patchValue({ percentage: 120 });
      fixture.onBlurPercentage();
      expect(fixture.catNewDiscForm.controls['percentage'].value).toEqual(null)
    });
  });

  describe('Test: initDiscounts', () => {
    it('should   fill catDiscountRateInitialList ', () => {

      fixture.initDiscounts();
      expect(fixture.catDiscountRateInitialList).toBeDefined();
      expect(catdiscountRateList.length).toEqual(4);
    });

    it('  should initDiscountLock called  ', () => {
      fixture.initDiscounts();
      expect(fixture.initDiscountLock).toHaveBeenCalled
    });

  });


  describe('Test: isAdmin', () => {

    it('should isAdmin false ', () => {

      expect(fixture.isAdmin()).toEqual(false);
    });



  });
  describe("test :   lockForm()", () => {
    it('should update editing mode to true ', () => {
      fixture.lockForm();
      expect(fixture.editingMode).toEqual(false);
    });

  });

  describe("test :   inlockForm()", () => {

    it('should  update editing mode to true ', () => {

      fixture.unlockForm();
      expect(fixture.editingMode).toEqual(true);
    });
    it('should  disable endDate ', () => {
      fixture.catDiscountObject.isPerm = true;
      fixture.unlockForm();
    });
  });

  describe('Test: undo', () => {
    it('should   set value ', () => {
      fixture.createForm();
      fixture.undo();
      expect(fixture.catDiscountObject.comment).toEqual(fixture.catNewDiscForm.get('comment').value);

    });
    it('if node type brand should set commentHerit value ', () => {
      fixture.nodeType = 'brand';
      fixture.createForm();
      fixture.undo();
      expect(catDiscountServiceMock.getDiscountDetailsByIdAndDesc).toHaveBeenCalled();


    });
  });
  describe("test :   initForm()", () => {

    it('should  update comment herit to null  ', () => {
      fixture.nodeType = "brand";
      fixture.initForm();
      expect(catDiscountServiceMock.getDiscountDetailsByIdAndDesc).toHaveBeenCalled;
    });

  });
  describe("test :   readjust()", () => {

    it('should  show pop up   ', () => {
      fixture.formChanged = true;
      fixture.readjust(true);
      expect(popupServiceMock.popupInfo).toHaveBeenCalled;
    });

    it('should  call disableTauxfield   ', () => {
      fixture.formChanged = true;
      fixture.readjust(false);
      expect(fixture.disableTauxFields).toHaveBeenCalled;
    });

    it('should  call disableTauxfield   ', () => {
      fixture.formChanged = false;
      fixture.readjust(true);
      expect(fixture.enableTauxFields).toHaveBeenCalled;
    });
  });

  describe("test :   sinceYear()", () => {
    it('if start year  cal resetDiscount  and    update starting number ', () => {
      fixture.catNewDiscForm.patchValue({ startingYear1: '01/01/2020' });
      fixture.sinceYear();
      expect(fixture.catNewDiscForm.controls['startingNumber'].value).toEqual(null);
      expect(fixture.catNewDiscForm.controls['percentage'].value).toEqual(null);
      expect(fixture.resetDiscounts()).toHaveBeenCalled;

    });
    it(' should call initDiscountLock and update starting number 2', () => {
      fixture.sinceYear();
      expect(fixture.catNewDiscForm.controls['startingNumber2'].value).toEqual(null);
      expect(fixture.duplicateDiscounts).toEqual(false);
      expect(fixture.initDiscountLock()).toHaveBeenCalled;
    });
  });


  describe("test :   duplicateYear()", () => {
    it('if start year  cal resetDiscount  and update starting number 2    ', () => {
      fixture.catNewDiscForm.patchValue({ startingYear2: '01/01/2020' });
      fixture.duplicateYear();
      expect(fixture.catNewDiscForm.controls['startingNumber2'].value).toEqual(null);
      expect(fixture.resetDiscounts()).toHaveBeenCalled;

    });
    it(' should call initDiscounLock and update starting number  ', () => {
      fixture.duplicateYear();
      expect(fixture.catNewDiscForm.controls['startingYear1'].value).toEqual(false);
      expect(fixture.catNewDiscForm.controls['percentage'].value).toEqual(null);
      expect(fixture.duplicateDiscounts).toEqual(true);
      expect(fixture.initDiscountLock()).toHaveBeenCalled;
    });
  });

  describe("test :   sinceYearCalcul()", () => {
    it('should  call resetDiscount  and initDiscountLock    ', () => {
      fixture.catNewDiscForm.patchValue({ startingNumber: '2', percentage: '10' });
      fixture.sinceYearCalcul();
      expect(fixture.initDiscountLock()).toHaveBeenCalled;
      expect(fixture.resetDiscounts()).toHaveBeenCalled;

    });
     it('Should update catDiscountRateValue     ', () => {
      fixture.catNewDiscForm.patchValue({ startingNumber: '1', percentage: '10' });
      fixture.catNewDiscForm.get('catDiscountRateList').enable()

      fixture.sinceYearCalcul();
      expect(fixture.catNewDiscForm.getRawValue().catDiscountRateList[1].discountRateValue).toEqual(2);
      expect(fixture.catDiscountRateListControls[1].disabled).toEqual(true);
    });

  });
  describe("test :   duplicateCalcul()", () => {
    it('should call  initDiscountLock  and resetDiscounts    ', () => {
      fixture.catNewDiscForm.get('catDiscountRateList').enable()

      fixture.catNewDiscForm.patchValue({ startingNumber2: '2', percentage: '10' });
      fixture.duplicateCalcul();
      expect(fixture.initDiscountLock()).toHaveBeenCalled;
      expect(fixture.resetDiscounts()).toHaveBeenCalled;

    });

  });
  describe("test :   changedDate()", () => {
    it('should set FormChanged To true      ', () => {
      fixture.changedDate();
      expect(fixture.formChanged).toEqual(true);
    });
  });
  describe("test :   resetForm()", () => {
    it('should update  FormChanged to false and call initComponent, lockForm,ResetDiscounts        ', () => {
      fixture.resetForm();
      expect(fixture.formChanged).toEqual(false);
      expect(fixture.initComponent()).toHaveBeenCalled;
      expect(fixture.lockForm()).toHaveBeenCalled;
      expect(fixture.resetDiscounts()).toHaveBeenCalled;

    });
  });

  describe('Test delete document', () => {
    it('should delete the document ', () => {
      fixture.deletedNewdiscountDocs = [];
      let files = [{ docName: 'file3.png', id: '1', catActivitySector : null, catNature: null, catBrand:null,
      catAssetType:null, discriminator:'G', docVersion: 1, gedPath: '17/file3.png' } as CatDocumentModel,
      { docName: 'file2.png', id: '2', catActivitySector : null, catNature: null, catBrand:null,
      catAssetType:null, discriminator:'G', docVersion: 1, gedPath: '17/file2.png' } as CatDocumentModel]


      fixture.updatedNewDiscountDocs = files;


      fixture.deleteDocument(1);

      expect(popupServiceMock.popupInfo).toHaveBeenCalled();
      popupServiceMock.onConfirm.subscribe((t)=>{
      expect(fixture.deletedNewdiscountDocs.length).toEqual(1);
      })


    });
  });

  describe('Test duplicateYear()', () => {

    it('should formChanged be true', () => {
      fixture.duplicateYear();
      expect(fixture.formChanged).toEqual(true);

    });

  });
  describe('Test:  onBlurDocName', () => {
    it('should update edited value of doc index ', () => {

      fixture.updatedNewDiscountDocs = documentsList;
      fixture.editedDocumentList = [false, true];

      fixture.onBlurDocument(1);
      expect(fixture.editedDocumentList[1]).toEqual(false);

    });

  });

  describe('Test:  editDocumentName', () => {
    it('should set doc edited value to true ', () => {
      fixture.updatedNewDiscountDocs = documentsList;
      fixture.editedDocumentList = [false, false];
      fixture.documentName = "test";
      fixture.editDocumentName(1);
      expect(fixture.editedDocumentList[1]).toEqual(true);

    });

  });

  describe('Test:  tempOrPerm', () => {
    it('should enable catNewDiscForm.get(endDate) ', () => {
      fixture.catNewDiscForm.patchValue({ endDate: true, disable:false });
      fixture.tempOrPerm();
      expect(fixture.catNewDiscForm.get('temporary').enable()).toBeTruthy

    });
    it('should disable catNewDiscForm.get(endDate) ', () => {
      fixture.catNewDiscForm.patchValue({ temporary: true, disable:false });
      fixture.tempOrPerm();
      expect(fixture.catNewDiscForm.get('endDate').disable()).toBeTruthy

    });

  });


  describe('Test:  download doc', () => {
    it('should download doc ', () => {
      let documentToDownload = {
        docName: 'file3.png', id: '1', catActivitySector: null, catNature: null, catBrand: null,
        catAssetType: null, discriminator: 'N', docVersion: 1, gedPath: '17/file3.png'
      } as CatDocumentModel


      fixture.downloadDocument(documentToDownload);

    });

  });


  describe('Test:   searchNewDiscountInheritesDocs', () => {
    it('should searchNewDiscountInheritesDocs ', () => {

       fixture.nodeType='brand';
      let inheritedDocBrand = [{
        docName: 'file3.png', id: '1', catActivitySector: null, catNature: {
        id: '133',
        label: 'nature label',
        catActivitySector: null },
         catBrand: null,
        catAssetType: null,
        discriminator: 'N',
        docVersion: 1,
         gedPath: '17/file3.png'
      } as CatDocumentModel]

      fixture.searchNewDiscountInheritesDocs();
 expect(fixture.inheritedNewDiscountDocsNature).toEqual(inheritedDocBrand)


  });

  });
  describe("test :   initDiscountLock()", () => {
    it('should disable form    ', () => {
      fixture.editingMode=true;
      fixture.disableDiscounts = false
      fixture.initDiscountLock();


    });
  });
  describe('Test:  updateNewDiscountDocs ', () => {
    it(' test update NewDiscounts DOc ', () => {

            fixture.catNewDiscForm.controls['files'].patchValue({

                    files: [{ name: 'file13.png', size: 17567 } as File,
                    { name: 'file14.png', size: 17567 } as File]

            });
            fixture.catDiscountObject ={
              id:'1',
              catBrand: {
                id :'101'
              } as CatBrandModel,
              catActivitySector: {
                id: '102'
              } as CatActivitySectorModel,
              catNature:{
                id: '103'
              } as CatNatureModel
            } as CatDiscountModel
        fixture.updatedNewDiscountDocsIndex =[1,2];

        fixture.deletedNewdiscountDocs = [
          {
              id : '1001'
          } as CatDocumentModel
        ] as CatDocumentModel[];
        fixture.updateNewDiscountDocs();
        expect(catDocumentServiceMock.updateDocument).toHaveBeenCalled
        expect(catDocumentServiceMock.deleteCatDocument).toHaveBeenCalled

    });
});
describe('Test ngOnDestroy', () => {

  it('should unsubscribe', () => {
    fixture.onChangeSub = new Subscription();
    fixture.ngOnDestroy();
    const spyunsubscribe = jest.spyOn(subscriptionMock, 'unsubscribe');
    expect(spyunsubscribe).toBeDefined();
  });
});
describe('Test getNewDiscountDocs', () => {

  it('should intialise updatedNewDiscountDocs List', () => {
    fixture.newDiscountDocs = [{ docName: 'file3.png', id: '1', catActivitySector: null, catNature: {
      id: '133',
      label: 'nature label',
      catActivitySector: null },
       catBrand: null,
      catAssetType: null,
      discriminator: 'N',
      docVersion: 1,
       gedPath: '17/file3.png'
    } as CatDocumentModel

  ]
    fixture.getNewDiscountDocs();
    expect(fixture.editedDocumentList[0] ).toEqual(false);
  });
});
describe('test :displayPlusBtns ',()=>{

  it('should return true',()=>{
    let catDoc = {
      catNature:{
        id:'23'
      }as CatNatureModel
    } as CatDocumentModel
      fixture.catDiscountObject ={
        id:'1',
      } as CatDiscountModel
      expect(fixture.displayPlusBtns(catDoc)).toEqual(true);
  });
  
  it('should return true',()=>{
    let catDoc = {
      catNature:{
        id:'23'
      }as CatNatureModel,
      catBrand: {
        id:'802'
      } as CatBrandModel
    } as CatDocumentModel
      fixture.catDiscountObject ={
        id:'1',
        catBrand: {
            id:'802'
        } as CatBrandModel
      } as CatDiscountModel;
      let resp = fixture.displayPlusBtns(catDoc);
      expect(resp).toEqual(true);
  })

  it('should return true when idDiscount is null',()=>{
    let catDoc = {
      catNature:{
        id:'23'
      }as CatNatureModel
    } as CatDocumentModel
      fixture.catDiscountObject ={} as CatDiscountModel
      expect(fixture.displayPlusBtns(catDoc)).toEqual(true);
  })

});

describe('Test registerOnChange', () => {
  it('should get register', () => {
    let test = (test) => { return test };
    let size = fixture.subscriptions.length;
    fixture.registerOnChange(test);
    expect(fixture.onChange).not.toEqual(() => {
      //this is intentional
    });
    expect(fixture.subscriptions.length).toEqual(size + 1);


  });
});
describe('Test writeValue', () => {
  it('should  write value', () => {
    let value = {
      maxFinPer: 52,
      isPerm: true,
      endDate: null,
      ponderationRate: null,
      isReadjust: false,
      comment: 'comment nature',
      temporary: null,
      startingYear1: null,
      startingNumber: null,
      percentage: 50,
      startingYear2: null,
      startingNumber2: null,
      commentHerit: null,
      files: null,
      catDiscountRateOldList: catdiscountRateList


    };


    fixture.writeValue(value);

    expect(fixture.value).toBeDefined();



  });

});
describe('Test registerOnTouched', () => {
  it('should get register', () => {
    fixture.registerOnTouched(test);
    expect(fixture.onTouched).not.toEqual(() => {
    //this is intentional
    });
    expect(fixture.onTouched).toEqual(test);
  });

});

  describe('Test set catDiscount', () => {
    it('should formChanged be false', () => {
      fixture.formChanged = true;
      let catDiscount = {
        id: '11'
      } as CatDiscountModel;
      fixture.catDiscount = catDiscount;
      expect(fixture.formChanged).toEqual(false);
    });

  });

  describe('Test set isUnlock', () => {
    it('should lock form when isUnlock is true', () => {
      fixture.isUnlock = true;
      expect(fixture.isUnlocked).toEqual(true);
    });

  });

  describe('Test eventCheck', () => {
    it('should isReadjust be true', () => {
      fixture.catNewDiscForm.patchValue({ isReadjust: false });
      fixture.catDiscountObject.id = null;
      fixture.eventCheck(null);
      expect(fixture.catNewDiscForm.controls['isReadjust'].value).toEqual(true);
    });

  });

  describe('Test set docs()', () => {
    it('should newDiscountDocs be setted', () => {
      let documents = [];
      fixture.docs = documents;
      expect(fixture.newDiscountDocs).toEqual(documents);
    });

  });
});
