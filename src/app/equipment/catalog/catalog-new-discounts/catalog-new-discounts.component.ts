import { DatePipe } from '@angular/common';
import { Component, EventEmitter, forwardRef, Input, OnInit, Output } from '@angular/core';
import { ControlValueAccessor, FormArray, FormBuilder, FormControl, FormGroup, NG_VALIDATORS, NG_VALUE_ACCESSOR, Validators } from '@angular/forms';
import { forkJoin, Subscription, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ConfirmationModalComponent } from '../../../shared/confirmation-modal/confirmation-modal.component';
import { documentsValidator } from '../../../shared/documents/document-validator';
import { CatBrandModel } from '../../../shared/models/cat-brand.model';
import { CatDiscountRateModel } from '../../../shared/models/cat-discount-rate.model';
import { CatDiscountModel } from '../../../shared/models/cat-discount.model';
import { CatDocumentModel } from '../../../shared/models/cat-document.model';
import { CatNatureModel } from '../../../shared/models/cat-nature.model';
import { CatDiscountService } from '../../../shared/services/cat-discount.service';
import { CatDocumentService } from '../../../shared/services/cat-document.service';
import { PopupService } from '../../../shared/services/popup.service';

@Component({
  selector: 'app-catalog-new-discounts',
  templateUrl: './catalog-new-discounts.component.html',
  styleUrls: ['./catalog-new-discounts.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CatalogNewDiscountsComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => CatalogNewDiscountsComponent),
      multi: true
    }
  ]
})
export class CatalogNewDiscountsComponent implements OnInit, ControlValueAccessor {

  catDiscountObject: CatDiscountModel;
  catDiscountObjectInitial: CatDiscountModel;

  @Input() set catDiscount(catDiscount: CatDiscountModel) {


    if (catDiscount != null && catDiscount != undefined) {

      this.formChanged = false;
      this.catDiscountObject = catDiscount;
      this.catDiscountObjectInitial = catDiscount;
      if (this.formCreated) {
        this.initDiscountLock();

        this.catNewDiscForm.patchValue({ isPerm: catDiscount ? catDiscount.isPerm : false })
        this.catNewDiscForm.patchValue({ temporary: catDiscount ? !catDiscount.isPerm : true })
      }

    }
  }
  //list to save discount after every modification 
  //lastModifOfCatDiscountRate: CatDiscountRateModel[] = [];
  @Input() catDiscountRateList: CatDiscountRateModel[] = [];
  catDiscountRateInitialList: CatDiscountRateModel[] = [];
  showForm: boolean = false;
  showDiscounts: boolean = false;
  initialDiscount: any;
  isDiscountValid: any;
  discountRate: number;

  catNewDiscForm: FormGroup;
  formChanged: boolean = false;
  formCreated: boolean = false;
  disableDiscounts: boolean = false;
  duplicateDiscounts: boolean = false;
  editingMode: boolean = false;
  subscriptions: Subscription[] = [];

  isUnlocked: boolean = false;
  isNew: boolean = true;
  catBrandObject: CatBrandModel = {} as any;
  catNatureObject: CatNatureModel = {} as any;
  onChangeSub: Subscription;
  inheritedNewDiscountDocsNature: CatDocumentModel[];
  inheritedNewDiscountDocsSector: CatDocumentModel[] = [];
  activitySectorTitle: string;
  natureTitle: string;
  percentSymbol = "%";

  @Input() set docs(docs) {
    if (docs) {
      this.newDiscountDocs = docs;
      this.getNewDiscountDocs();
    }

  }
  newDiscountDocs: CatDocumentModel[] = [];
  updatedNewDiscountDocs: CatDocumentModel[] = [];
  deletedNewdiscountDocs: CatDocumentModel[] = [];

  updatedNewDiscountDocsIndex: number[] = [];
  editedDocumentList: boolean[] = [];
  documentName: string;


  onTouched = () => {
    // This is intentional
  };


  onChange: any = () => {
    // This is intentional
  };
  @Input() nodeType: string;
  @Input() idCateg: string;
  @Output() changeNewDiscount = new EventEmitter<number>();
  @Output() refreshNewDicountDocs = new EventEmitter();
  @Input() isSubmitted = false;


  get value() {
    return this.catNewDiscForm.getRawValue();
  }

  set value(value) {
    this.catNewDiscForm.setValue(value);
    this.onChange(value);
    this.onTouched();
  }
  get f() {
    return this.catNewDiscForm.controls;
  }

  get catDiscountRateListControls() {
    return (this.catNewDiscForm.controls["catDiscountRateList"] as FormArray).controls;

  }


  @Input() set isUnlock(isUnlock: boolean) {
    this.isUnlocked = isUnlock;

    if (this.catNewDiscForm != undefined) {
      if (isUnlock) {
        this.initDiscountLock();
        this.unlockForm()
      } else {
        this.lockForm();
      }
      this.catNewDiscForm.patchValue({ isPerm: this.catDiscountObject.isPerm != null ? this.catDiscountObject.isPerm : false })
      this.catNewDiscForm.patchValue({ temporary: this.catDiscountObject.isPerm != null ? !this.catDiscountObject.isPerm : true })
    }
  }

  constructor(private fb: FormBuilder, private popupService: PopupService, private catDiscountService: CatDiscountService,
    private catDocumentService: CatDocumentService) { }

  ngOnInit(): void {
    this.createForm();
    this.initComponent();
    this.initDiscounts();
    this.catNewDiscForm.patchValue({ isPerm: this.catDiscountObject.isPerm != null ? this.catDiscountObject.isPerm : false })
    this.catNewDiscForm.patchValue({ temporary: this.catDiscountObject.isPerm != null ? !this.catDiscountObject.isPerm : true })
  }

  startingNumberValidators = [
    Validators.min(1)
  ];



  createForm() {

    this.catNewDiscForm = this.fb.group({
      isReadjust: [{ value: '', disabled: true },],
      isPerm: [{ value: '', disabled: true },],
      temporary: [{ value: '', disabled: true },],
      endDate: [{ value: '31/12/9999', disabled: true },
      Validators.pattern(new RegExp("([0-9]{4}[/](0[1-9]|1[0-2])[/]([0-2]{1}[0-9]{1}|3[0-1]{1})|([0-2]{1}[0-9]{1}|3[0-1]{1})[/](0[1-9]|1[0-2])[/][0-9]{4})"))],

      startingYear1: [{ value: '', disabled: true }],
      startingNumber: [{ value: '', disabled: true }, this.startingNumberValidators],
      percentage: [{ value: '', disabled: true }, [Validators.min(1), Validators.max(100)]],
      startingYear2: [{ value: '', disabled: true }],
      startingNumber2: [{ value: '', disabled: true }, this.startingNumberValidators],
      comment: [{ value: '', disabled: true }],
      commentHerit: [{ value: '', disabled: true }],
      files: [{ value: '', disabled: true }, { validators: [documentsValidator] }],
      catDiscountRateList: this.fb.array([...this.initiateRateListFormArray()])


    });

    this.subscriptions.push(
      this.catNewDiscForm.valueChanges.subscribe(value => {
        this.onChange(this.catNewDiscForm.getRawValue());
        this.onTouched();
      })
    );

    this.subscriptions.push(
      this.catNewDiscForm.get('catDiscountRateList').valueChanges.subscribe(value => {
        this.formChanged = true;
      })
    );


    this.showForm = true;
    this.formCreated = true;

    this.catNewDiscForm.patchValue({ isPerm: (this.catDiscountObject && this.catDiscountObject.isPerm) ? this.catDiscountObject.isPerm : false })
    this.catNewDiscForm.patchValue({ temporary: (this.catDiscountObject && this.catDiscountObject.isPerm) ? !this.catDiscountObject.isPerm : true })


  }
  eventCheck(event) {
    if (this.catDiscountObject.id) {
      this.readjust(event.checked);
    }
    else {
      this.catNewDiscForm.patchValue({ isReadjust: true });


    }
  }
  initForm() {

    this.catNewDiscForm.patchValue({ isReadjust: this.catDiscountObject ? this.catDiscountObject.isReadjust : null })
    this.catNewDiscForm.patchValue({ isPerm: true })
    this.catNewDiscForm.patchValue({ temporary: this.catDiscountObject ? (this.catDiscountObject.endDate != null) : false })
    this.catNewDiscForm.patchValue({ startingYear1: this.catDiscountObject ? false : null })
    this.catNewDiscForm.patchValue({ startingNumber: '' })
    this.catNewDiscForm.patchValue({ percentage: '' })
    this.catNewDiscForm.patchValue({ startingYear2: false })
    this.catNewDiscForm.patchValue({ startingNumber2: '' })
    this.catNewDiscForm.patchValue({ comment: (this.catDiscountObject != null) ? this.catDiscountObject.comment : null })


    var datePipe = new DatePipe("en-US");

    this.catNewDiscForm.patchValue({ endDate: this.catDiscountObject ? datePipe.transform(this.catDiscountObject.endDate, 'dd/MM/yyyy') : '31/12/9999' })


    this.catNewDiscForm.setControl('catDiscountRateList', this.fb.array([...this.initiateRateListFormArray()]));


    this.readjust(this.catDiscountObject.isReadjust);


    if (this.nodeType == 'brand' && this.catDiscountObject && this.catDiscountObject.catNature && this.catDiscountObject.catNature.id) {
      this.catDiscountService.getDiscountDetailsByIdAndDesc(this.catDiscountObject.catNature.id, 'nature', this.isNew).subscribe(data => {
        this.catNewDiscForm.patchValue({ commentHerit: (data != null) ? data.comment : null })

      })

    }

  }
  initiateRateListFormArray() {
    let formArray: FormGroup[] = [];
    for (let i = 0; i < this.catDiscountRateList.length; i++) {


      const discountRateForm = this.createRateForm(this.catDiscountRateList[i]);
      if (i == 0) {
        discountRateForm.valueChanges.subscribe(val => {

          this.changeNewDiscount.emit(val.discountRateValue);

        })
      }
      formArray.push(discountRateForm);
    }


    return formArray;
  }

  createRateForm(catDiscountRateElement: any): any {
    return this.fb.group({
      discountRateValue: [{ value: (catDiscountRateElement.discountRateValue != null) ? catDiscountRateElement.discountRateValue : null, disabled: !this.editingMode || this.disableDiscounts }, Validators.required],
      discountRateYear: [catDiscountRateElement.discountRateYear != null ? catDiscountRateElement.discountRateYear : null],
      id: [catDiscountRateElement.id != null ? catDiscountRateElement.id : null],
      catDiscount: [catDiscountRateElement.catDiscount != null ? catDiscountRateElement.catDiscount : null],
      updatedDate: [catDiscountRateElement.updatedDate != null ? catDiscountRateElement.updatedDate : null],
      createdDate: [catDiscountRateElement.createdDate != null ? catDiscountRateElement.createdDate : null],
      createdBy: [catDiscountRateElement.createdBy != null ? catDiscountRateElement.createdBy : null],
      updatedBy: [catDiscountRateElement.updatedBy != null ? catDiscountRateElement.updatedBy : null],
    });
  }

  undo() {
    this.catNewDiscForm.patchValue({ isReadjust: this.catDiscountObject ? this.catDiscountObject.isReadjust : null })
    // this.catNewDiscForm.patchValue({ isPerm: true })
    this.catNewDiscForm.patchValue({ temporary: this.catDiscountObject ? (!this.catDiscountObject.isPerm) : false })
    var datePipe = new DatePipe("en-US");
    this.catNewDiscForm.patchValue({ endDate: this.catDiscountObject ? datePipe.transform(this.catDiscountObject.endDate, 'dd/MM/yyyy') : '31/12/9999' })
    this.catNewDiscForm.patchValue({ startingYear1: this.catDiscountObject ? false : null })
    this.catNewDiscForm.patchValue({ startingNumber: '' })
    this.catNewDiscForm.patchValue({ percentage: '' })
    this.catNewDiscForm.patchValue({ startingYear2: false })
    this.catNewDiscForm.patchValue({ startingNumber2: '' })
    this.catNewDiscForm.patchValue({ comment: (this.catDiscountObject != null) ? this.catDiscountObject.comment : null });
    this.catNewDiscForm.patchValue({ files: { files: [] } });
    if (this.nodeType == 'brand') {
      this.catDiscountService.getDiscountDetailsByIdAndDesc(this.catDiscountObject.catNature.id, 'nature', this.isNew).subscribe(data => {
        this.catNewDiscForm.patchValue({ commentHerit: (data != null) ? data.comment : null })
      });
      //should add nature comment comment for old discount
    }
    this.lockForm();
    this.resetDiscounts();
    this.initDiscountLock();
  }
  initComponent() {

    if (this.catDiscountObject) {
      this.initForm();
      this.searchNewDiscountInheritesDocs()
      if (this.catDiscountObject.catActivitySector) {
        this.activitySectorTitle = this.catDiscountObject.catActivitySector.label;

      }
      if (this.catDiscountObject.catNature) {
        this.natureTitle = this.catDiscountObject.catNature.label;

      }
      if (this.catNewDiscForm != undefined) {
        if (this.isUnlocked) {
          this.unlockForm()
        } else {
          this.lockForm();
        }
      }
    }
  }


  initDiscounts() {
    // load Cotations
    if (this.catDiscountRateList != null && this.catDiscountRateList != undefined && this.catDiscountRateList.length > 0) {

      this.catDiscountRateList.forEach((element, index) => {
        this.catDiscountRateInitialList.push(Object.assign({}, element))
      });

      this.initialDiscount = this.catDiscountRateList[0].discountRateValue;
      this.showDiscounts = true;
      this.initDiscountLock();
    }

  }



  initDiscountLock() {

    if (this.editingMode && !this.disableDiscounts) {
      this.catNewDiscForm.get('catDiscountRateList').enable()
    }
  }
  isAdmin() {
    return false;
  }


  onBlurPercentage() {
    if (1 > this.catNewDiscForm.get('percentage').value || this.catNewDiscForm.get('percentage').value > 100)
      this.catNewDiscForm.patchValue({ percentage: null })
    this.catNewDiscForm.get('startingNumber').setValidators(this.startingNumberValidators.concat(Validators.max(this.catDiscountRateInitialList.length - 1)));
    this.catNewDiscForm.get('startingNumber').updateValueAndValidity();


  }
  lockForm() {
    this.catNewDiscForm.disable();
    this.disableManipulators();
    this.editingMode = false;
  }
  unlockForm() {
    this.formChanged = false;
    this.catNewDiscForm.enable();
    this.editingMode = true;

    this.readjust(this.catDiscountObject.isReadjust);
    if (this.catDiscountObject.isPerm) {
      this.catNewDiscForm.get('endDate').disable();
    }
    else {
      this.catNewDiscForm.get('endDate').enable();
    }
    this.catNewDiscForm.get('commentHerit').disable();
    this.catNewDiscForm.updateValueAndValidity();

  }

  readjust(isReadjusted) {
    if (this.formChanged) {
      this.popupService.popupInfo(
        'Les paramètres initiaux seront restaurés. Voulez-vous confirmer ?', 'OUI', null, 'NON', ConfirmationModalComponent);
      const subscription = this.popupService.onConfirm.subscribe((t) => {
        if (t) {
          if (isReadjusted) {
            this.enableTauxFields();
          } else {
            this.disableTauxFields();
          }
          this.catNewDiscForm.updateValueAndValidity();
        }
        else {
          this.catNewDiscForm.patchValue({ isReadjust: !isReadjusted })

        }
        subscription.unsubscribe();
      });
    }
    else {
      if (isReadjusted) {
        this.enableTauxFields();
      } else {
        this.disableTauxFields();
      }
    }
  }

  tempOrPerm() {
    if (this.catNewDiscForm.get('temporary').value) {
      this.catNewDiscForm.get('endDate').disable();
      this.catNewDiscForm.controls['endDate'].setValidators([]);
      this.catNewDiscForm.patchValue({ temporary: false })
      this.catNewDiscForm.patchValue({ isPerm: true })
      this.catNewDiscForm.patchValue({ endDate: '31/12/9999' })
      this.catNewDiscForm.updateValueAndValidity();

    }
    else {
      this.catNewDiscForm.get('endDate').enable();
      this.catNewDiscForm.patchValue({ temporary: true })
      this.catNewDiscForm.patchValue({ isPerm: false })
      this.catNewDiscForm.controls['endDate'].setValidators([Validators.required,
      Validators.pattern(new RegExp("([0-9]{4}[/](0[1-9]|1[0-2])[/]([0-2]{1}[0-9]{1}|3[0-1]{1})|([0-2]{1}[0-9]{1}|3[0-1]{1})[/](0[1-9]|1[0-2])[/][0-9]{4})"))])
      this.catNewDiscForm.updateValueAndValidity();

    }
  }
  sinceYear() {
    this.formChanged = true;
    if (this.catNewDiscForm.get('startingYear1').value) {
      this.resetDiscounts();// to delete
      //restore last modification done
      //this.catNewDiscForm.get('catDiscountRateList').patchValue(this.lastModifOfCatDiscountRate)

      this.initDiscountLock();
      this.catNewDiscForm.patchValue({ startingNumber: null })
      this.catNewDiscForm.patchValue({ percentage: null })
      this.catNewDiscForm.get('startingNumber').disable();
      this.catNewDiscForm.get('percentage').disable();
    }
    else {
      this.catNewDiscForm.patchValue({ startingYear2: false })
      this.initDiscountLock();
      this.duplicateDiscounts = false;
      this.catNewDiscForm.get('startingNumber').enable();
      this.catNewDiscForm.get('percentage').enable();

      this.catNewDiscForm.get('startingNumber2').disable();

      this.catNewDiscForm.patchValue({ startingNumber2: null })
      this.resetDiscounts();//to delete
      this.initDiscountLock();


    }
    this.catNewDiscForm.updateValueAndValidity();
  }
  duplicateYear() {
    this.formChanged = true;
    if (this.catNewDiscForm.get('startingYear2').value) {
      this.resetDiscounts();//to delete

      //restore last modification done
      //this.catNewDiscForm.get('catDiscountRateList').patchValue(this.lastModifOfCatDiscountRate)

      this.initDiscountLock();
      this.catNewDiscForm.patchValue({ startingNumber2: null })
      this.catNewDiscForm.get('startingNumber2').disable();

    }
    else {
      this.catNewDiscForm.patchValue({ startingYear1: false })
      this.initDiscountLock();
      this.duplicateDiscounts = true;
      this.catNewDiscForm.get('startingNumber').disable();
      this.catNewDiscForm.get('percentage').disable();

      this.catNewDiscForm.get('startingNumber2').enable();

      this.catNewDiscForm.patchValue({ startingNumber: null })
      this.catNewDiscForm.patchValue({ percentage: null })
      this.resetDiscounts(); // to delete
      this.initDiscountLock();

    }
    this.catNewDiscForm.updateValueAndValidity();
  }
  sinceYearCalcul() {
    if (this.catNewDiscForm.get('startingNumber').value && this.catNewDiscForm.get('percentage').value) {
      this.resetDiscounts();// to delete
      //store before the modification
      //this.lastModifOfCatDiscountRate = this.catNewDiscForm.value.catDiscountRateList;

      this.initDiscountLock();
      var index = this.catNewDiscForm.get('startingNumber').value;
      var percentage = this.catNewDiscForm.get('percentage').value;
      this.catDiscountRateListControls[index - 1].disable();
      for (let i = index; i < this.catDiscountRateList.length; i++) {
        //calculate the discounts into next ones.
        this.catDiscountRateListControls[i].patchValue({
          discountRateValue: ((this.catDiscountRateListControls[i - 1].value.discountRateValue - percentage) > 0
            ? (this.catDiscountRateListControls[i - 1].value.discountRateValue - percentage) : 0)
        })
        this.catDiscountRateListControls[i].disable();
      }

    }
  }
  duplicateCalcul() {
    this.resetDiscounts();//to delete
    this.initDiscountLock();
    var index = this.catNewDiscForm.get('startingNumber2').value;
    if (index) {
      //store before the modification
      //this.lastModifOfCatDiscountRate = this.catNewDiscForm.value.catDiscountRateList;

      for (let i = index; i < this.catDiscountRateList.length; i++) {

        this.catDiscountRateListControls[i].patchValue({ discountRateValue: this.catDiscountRateList[index - 1].discountRateValue });//to delete
        //duplicate the discounts into next ones.
        //this.catDiscountRateListControls[i].patchValue({ discountRateValue: this.catDiscountRateListControls[index - 1].value.discountRateValue });
        this.catDiscountRateListControls[i].disable();
      }
      this.catDiscountRateListControls[index - 1].disable();

      this.catNewDiscForm.get('startingNumber2').setValidators(this.startingNumberValidators.concat(Validators.max(this.catDiscountRateInitialList.length - 1)));
      this.catNewDiscForm.get('startingNumber2').updateValueAndValidity();
    }
  }
  enableTauxFields() {
    this.catNewDiscForm.get('endDate').enable();
    this.catNewDiscForm.controls['endDate'].setValidators([Validators.required,
    Validators.pattern(new RegExp("([0-9]{4}[/](0[1-9]|1[0-2])[/]([0-2]{1}[0-9]{1}|3[0-1]{1})|([0-2]{1}[0-9]{1}|3[0-1]{1})[/](0[1-9]|1[0-2])[/][0-9]{4})"))])
    this.catNewDiscForm.get('startingYear1').enable();
    this.catNewDiscForm.get('startingYear2').enable();
    this.catNewDiscForm.get('startingNumber').disable();
    this.catNewDiscForm.get('percentage').disable();
    this.catNewDiscForm.get('startingNumber2').disable();
    this.disableDiscounts = false;
    this.catNewDiscForm.get('catDiscountRateList').enable();
    this.catNewDiscForm.patchValue({ temporary: true })
    this.catNewDiscForm.patchValue({ isPerm: false });
    this.catNewDiscForm.patchValue({ isReadjust: true });
    this.formChanged = false;
    this.catNewDiscForm.updateValueAndValidity();
    if (!this.catDiscountObject.id) {
      this.catNewDiscForm.get('isReadjust').disable();

    }
  }
  disableTauxFields() {
    this.catNewDiscForm.get('endDate').disable();
    this.catNewDiscForm.controls['endDate'].setValidators([]);
    this.catNewDiscForm.get('startingYear1').disable();
    this.catNewDiscForm.get('startingYear2').disable();
    this.catNewDiscForm.get('startingNumber').disable();
    this.catNewDiscForm.get('percentage').disable();
    this.catNewDiscForm.get('startingNumber2').disable();
    this.catNewDiscForm.patchValue({ startingYear1: false })
    this.catNewDiscForm.patchValue({ startingYear2: false })
    this.catNewDiscForm.patchValue({ startingNumber: null })
    this.catNewDiscForm.patchValue({ percentage: null })
    this.catNewDiscForm.patchValue({ startingNumber2: null })
    var datePipe = new DatePipe("en-US");

    this.catNewDiscForm.patchValue({ endDate: this.catDiscountObject ? datePipe.transform(this.catDiscountObject.endDate, 'dd/MM/yyyy') : '31/12/9999' })
    this.catNewDiscForm.get('catDiscountRateList').disable();

    this.disableDiscounts = true;

    this.initDiscountLock();
    this.resetDiscounts();
    this.catNewDiscForm.patchValue({ temporary: false })
    this.catNewDiscForm.patchValue({ isPerm: true });
    this.catNewDiscForm.patchValue({ isReadjust: false });
    this.formChanged = false;
    this.catNewDiscForm.updateValueAndValidity();
  }
  disableManipulators() {
    this.catNewDiscForm.get('startingYear1').disable();
    this.catNewDiscForm.get('startingYear2').disable();
    this.catNewDiscForm.get('startingNumber').disable();
    this.catNewDiscForm.get('percentage').disable();
    this.catNewDiscForm.get('startingNumber2').disable();
    this.catNewDiscForm.patchValue({ startingYear1: false })
    this.catNewDiscForm.patchValue({ startingYear2: false })
    this.catNewDiscForm.patchValue({ startingNumber: null })
    this.catNewDiscForm.patchValue({ percentage: null })
    this.catNewDiscForm.patchValue({ startingNumber2: null })
  }

  changedDate() {
    this.formChanged = true;
  }
  resetDiscounts() {
    this.catNewDiscForm.get('catDiscountRateList').patchValue(this.catDiscountRateInitialList)
  }

  resetForm() {
    this.initComponent();
    this.lockForm();
    this.resetDiscounts();
    this.formChanged = false;
  }
  registerOnChange(fn: any) {
    this.subscriptions.push(this.catNewDiscForm.valueChanges.pipe(map(_ => this.catNewDiscForm.getRawValue())).subscribe(fn));

  }



  ngOnDestroy() {
    if (this.onChangeSub) {
      this.onChangeSub.unsubscribe();
    }
  }

  writeValue(value: any) {
    window.setTimeout(() => {
      if (value) {
        this.catNewDiscForm.setValue(value);
      }
    }, 100);
  }

  registerOnTouched(onTouched: any) {
    this.onTouched = onTouched;
  }
  validate(_: FormControl) {
    return this.catNewDiscForm.valid ? null : { profile: { valid: false, }, };
  }

  deleteDocument(index: number) {
    this.popupService.popupInfo('Êtes-vous sûr(e) de vouloir supprimer la pièce jointe ?', 'Confirmer', null, 'Annuler', ConfirmationModalComponent);
    const subscription = this.popupService.onConfirm.subscribe((t) => {
      if (t) {
        this.deletedNewdiscountDocs.push(this.updatedNewDiscountDocs[index])

        this.editedDocumentList.splice(index, 1);
        this.updatedNewDiscountDocs.splice(index, 1);
        let indexIndex = this.updatedNewDiscountDocsIndex.findIndex(val => val == index);
        if (indexIndex > 0) {
          this.updatedNewDiscountDocsIndex.splice(indexIndex, 1);
        }
      }
      subscription.unsubscribe();

    }
    )

  }
  editDocumentName(index: number) {
    this.editedDocumentList[index] = true;
    this.documentName = this.updatedNewDiscountDocs[index].docName;

  }
  onBlurDocument(index: number) {

    this.updatedNewDiscountDocs[index].docName = this.documentName;
    if (this.updatedNewDiscountDocsIndex.findIndex(val => val == index) == -1) {
      this.updatedNewDiscountDocsIndex.push(index);
    }
    this.editedDocumentList[index] = false;
  }

  //save delete update docs
  updateNewDiscountDocs() {
    let files: File[] = this.catNewDiscForm.controls['files'].value.files;

    let request = [];
    if (files && files.length > 0) {
      let brandId = null;
      if (this.catDiscountObject.catBrand) {
        brandId = this.catDiscountObject.catBrand.id;
      }
      files.forEach(file => request.push(this.catDocumentService.uploadDocument(file, this.catDiscountObject.catActivitySector.id,
        this.catDiscountObject.catNature.id, brandId, null, 'N').pipe(catchError(error => of(null)))));
    }

    // updated docs titles
    if (this.updatedNewDiscountDocsIndex.length > 0) {
      let docs: any = []
      this.updatedNewDiscountDocsIndex.forEach(index => docs.push(this.updatedNewDiscountDocs[index]))
      docs.forEach(doc => request.push(this.catDocumentService.updateDocument(doc).pipe(catchError(error => of(null)))));
      /*
      request.push(this.catActivitySectorService.updateActivitySectorDocument(docs))
      */
    }

    if (this.deletedNewdiscountDocs.length > 0) {

      this.deletedNewdiscountDocs.forEach(docs => request.push(this.catDocumentService.deleteCatDocument(docs).pipe(catchError(error => of(null)))))
    }

    forkJoin(request).subscribe(res => {
      //this is intentional
    }, () => {
      //this is intentional

    }, () => {
      this.catNewDiscForm.get('files').reset();
      this.refreshNewDicountDocs.emit();
    });
  }

  getNewDiscountDocs() {
    this.updatedNewDiscountDocs = [];
    this.editedDocumentList = [];

    this.newDiscountDocs.forEach((element, index) => {

      this.updatedNewDiscountDocs.push(Object.assign({}, element))
      this.editedDocumentList[index] = false;
    });
  }

  downloadDocument(documentToDownload: CatDocumentModel) {
    this.catDocumentService.downloadDocument(documentToDownload.gedPath, documentToDownload.docName).subscribe(response => {
      var file = new Blob([response], { type: 'application/*' })
      var fileURL = URL.createObjectURL(file);
      const doc = document.createElement('a');
      doc.download = documentToDownload.docName;
      doc.href = fileURL;
      doc.dataset.downloadurl = [doc.download, doc.href].join(':');
      document.body.appendChild(doc);
      doc.click();

    });
  }

  displayPlusBtns(doc: CatDocumentModel): boolean {
    if (!this.catDiscountObject.id)
      return true;
    if (this.catDiscountObject.catBrand)
      return doc.catBrand != null;

    return doc.catNature != null;
  }

  searchNewDiscountInheritesDocs() {
    if (this.nodeType == 'brand') {
      if (this.catDiscountObject && this.catDiscountObject.catNature && this.catDiscountObject.catNature.catActivitySector) {
        this.catDocumentService.searchInheratedDocuments(this.catDiscountObject.catNature.catActivitySector.id, this.catDiscountObject.catNature.id, null, 'N').subscribe(data => {
          if (data) {
            this.inheritedNewDiscountDocsNature = data.filter(doc => doc.catNature != null && doc.catBrand == null);
            this.inheritedNewDiscountDocsSector = data.filter(doc => doc.catNature == null && doc.catBrand == null);
          }

          this.natureTitle = this.catDiscountObject.catNature.label;
        });
      }

    }
    else {
      if (this.catDiscountObject && this.catDiscountObject.catActivitySector) {
        this.catDocumentService.searchInheratedDocuments(this.catDiscountObject.catActivitySector.id, null, null, 'N').subscribe(data => {
          if (data) {
            this.inheritedNewDiscountDocsSector = data.filter(doc => doc.catNature == null && doc.catBrand == null);
          }
        });
        this.activitySectorTitle = this.catDiscountObject.catActivitySector.label;

      }


    }


  }



}
