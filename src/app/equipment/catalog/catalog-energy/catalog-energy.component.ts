import { Component, OnInit, forwardRef, Input } from '@angular/core';
import { CatEnergyNatureWeightingModel } from '../../../shared/models/cat-energy-nature-weighting.model';
import { CatEnergyNatureModel } from '../../../shared/models/cat-energy-nature.model';
import { ParamEnergyModel } from '../../../shared/models/param-energy.model';
import { FormGroup, FormBuilder, FormArray, NG_VALUE_ACCESSOR, ControlValueAccessor, Validators } from '@angular/forms';
import { CatEnergyService } from '../../../shared/services/cat-energy.service';
import { ParamEnergyService } from '../../../shared/services/param-energy.servie';
import { CatNatureModel } from '../../../shared/models/cat-nature.model';
import { ParamSettingModel } from 'src/app/shared/models/param-setting.model';
import { map } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { PopupService } from '../../../shared/services/popup.service';
import { ConfirmationModalComponent } from '../../../shared/confirmation-modal/confirmation-modal.component';
import { Router } from '@angular/router';
import { CatBrandModel } from 'src/app/shared/models/cat-brand.model';

@Component({
  selector: 'app-catalog-energy',
  templateUrl: './catalog-energy.component.html',
  styleUrls: ['./catalog-energy.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => CatalogEnergyComponent),
    }
  ]

})
export class CatalogEnergyComponent implements OnInit, ControlValueAccessor {

  //availableEnergy: any[];
  catNatureEnergyList: CatEnergyNatureModel[] = []; //energies effected to selected nature, loaded from db
  catEnergyNatureWeightingList: CatEnergyNatureWeightingModel[] = [];
  catNatureEnergyListToBeDeleted: CatEnergyNatureModel[] = [];

  catNatureEnergyArrayForm: FormGroup;
  mainForm: FormGroup;
  displayOptionsSelect = false;
  @Input() idNature: string;
  @Input() idBrand: string;
  @Input() nodeType: string;
  @Input() isSubmitted = false;

  unusedEnergy = []; // energies that could be added
  showEnergies = false;

  disableAddEnergy = true;
  showDeleteIcon = false;

  subscriptions: Subscription[] = [];

  isUnlocked: boolean = false;
  selectedEnergy:any;

  isBrandAddMode = false;

  get value() {
    return this.mainForm.getRawValue();
  }

  set value(value) {
    if (value) {
      this.mainForm.setValue(value);
      this.onChange(value);
      this.onTouched();
    }
  }

  onChange: any = () => {
    // This is intentional
   };
  onTouched: any = () => {
   // This is intentional
  };

  registerOnChange(fn: any) {
    if (this.mainForm) {
      this.subscriptions.push(this.mainForm.valueChanges.pipe(map(_ => this.mainForm.getRawValue())).subscribe(fn));
    }
  }

  registerOnTouched(fn) {
    this.onTouched = fn;
  }

  get energyLinesControls() {
    if (this.mainForm) {
      return (this.mainForm.controls["paramLines"] as FormArray).controls;
    }

  }

  constructor(private fb: FormBuilder, private catEnergyService: CatEnergyService,
          private paramEnergyService: ParamEnergyService, private popupService :PopupService,
          private route: Router) { }

  ngOnInit(): void {
   
    this.checkIfBrandToAdd();

    if(this.isBrandAddMode){
      // load nature energies
      this.getBrandUnusedParamEnergy(this.idNature, null);
      this.catNatureEnergyList = [];
      this.createMainFormGroup();

    } else {
      //CASE: is not the ADD MODE
      //check if the selected is a brand
      if (this.idBrand) {

        this.getCatEnergyByBrandId(this.idBrand);
        this.getBrandUnusedParamEnergy(this.idNature, this.idBrand);

      } else {
        //the selected item is a nature
        //load energy affected to given nature
        this.getCatEnergyByNatureId(this.idNature);
        // load energy not affected to given nature
        this.getNatureUnusedParamEnergy(this.idNature);
      }

    }

    if (this.mainForm != undefined) {
      if (this.isUnlocked) {
        this.unlockForm()
      } else {
        this.lockForm();
      }
    }


  }

  showEnergyDiv() {
    this.displayOptionsSelect = !this.displayOptionsSelect;
  }

  createMainFormGroup() {
    this.mainForm = this.fb.group({
      paramLines: this.fb.array([...this.initiateParamLinesFormArray(this.catNatureEnergyList, true)]),
    });

    this.subscriptions.push(
      this.mainForm.valueChanges.subscribe(value => {
        this.onChange(this.mainForm.getRawValue());
        this.onTouched();
      })
    );
  }

  initiateParamLinesFormArray(catNatureEnergyList: CatEnergyNatureModel[], isDisabled: boolean) {
    let formArray: FormGroup[] = [];
    if (catNatureEnergyList && this.catNatureEnergyList.length>0) {
      for (let catNatureEnergy of catNatureEnergyList) {
        const paramLinesForm = this.initiateParamLinesFormBuilder({ ...catNatureEnergy }, isDisabled);
        formArray.push(paramLinesForm);
      }
    }
    return formArray;
  }

  initiateParamLinesFormBuilder(catNatureEnergy: CatEnergyNatureModel, isDisabled: boolean) {
    return this.fb.group({
      id: [catNatureEnergy.id != null ? catNatureEnergy.id : null],
      label: [catNatureEnergy.paramEnergy && catNatureEnergy.paramEnergy.label != null ? catNatureEnergy.paramEnergy.label : null],
      isTouched: false,
      isActive: true,
      catNature: [catNatureEnergy.catNature != null ? catNatureEnergy.catNature : null],
      catBrand: [catNatureEnergy.catBrand != null ? catNatureEnergy.catBrand : null],
      paramEnergy: [catNatureEnergy.paramEnergy != null ? catNatureEnergy.paramEnergy : null],
      catEnergyNatureWeightingList: this.fb.array([...this.initiateRateListFormArray(catNatureEnergy.catEnergyNatureWeightingList, isDisabled)]),
      activationDate:[catNatureEnergy.activationDate!=null ? catNatureEnergy.activationDate :null ]

    });
  }

  initiateRateListFormArray(catEnergyNatureWeightingList: CatEnergyNatureWeightingModel[], isDisabled: boolean) {
    let formArray: FormGroup[] = [];
    if (catEnergyNatureWeightingList && catEnergyNatureWeightingList.length > 0) {
      for (let energyWeighting of catEnergyNatureWeightingList) {
        const weightRateForm = this.initiateRateFormBuilder({ ...energyWeighting }, isDisabled);
        formArray.push(weightRateForm);
      }
    }
    return formArray;
  }

  initiateRateFormBuilder(energyWeighting: CatEnergyNatureWeightingModel, isDisabled: boolean) {
    return this.fb.group({
      id: [ energyWeighting.id != null? energyWeighting.id : null],
      catEnergyNature: [energyWeighting.catEnergyNature != null? energyWeighting.catEnergyNature : null],
      weightingRateValue: [{ value: (energyWeighting.weightingRateValue != null) ? energyWeighting.weightingRateValue : null, disabled: isDisabled},Validators.required],
      weightingRateYear: [energyWeighting.weightingRateYear != null ? energyWeighting.weightingRateYear : null],
    });

  }

  energyLinesRateControls(lineIndex: number): FormArray {
    return ((this.mainForm.controls["paramLines"] as FormArray).at(lineIndex).get('catEnergyNatureWeightingList') as FormArray);
  }

  addEnergy(event : any){
    //remove this selected energy from options list
    this.removeItemFromList(this.unusedEnergy, event.value);
    this.displayOptionsSelect = false;

    (this.mainForm.controls["paramLines"] as FormArray).push(this.newEnergy(event.value));

    if(!this.showEnergies){
      this.showEnergies = true;
    }

  }

  newEnergy(paramToAdd: ParamSettingModel): FormGroup {
    let weightingListtoAdd = [
    { id: null, weightingRateValue: null, weightingRateYear: 1 } as CatEnergyNatureWeightingModel,
    { id: null, weightingRateValue: null, weightingRateYear: 2 } as CatEnergyNatureWeightingModel,
    { id: null, weightingRateValue: null, weightingRateYear: 3 } as CatEnergyNatureWeightingModel,
    ];
    return this.fb.group({
      catNature: [{id: this.idNature} as CatNatureModel],
      catBrand: [this.idBrand? {id: this.idBrand} as CatBrandModel : null],
      paramEnergy: [{id  : paramToAdd.id, label: paramToAdd.label} as ParamEnergyModel],
      id: null,
      isTouched: true,
      isActive: true,
      label: [paramToAdd.label != null ? paramToAdd.label : null],
      catEnergyNatureWeightingList: this.fb.array([...this.initiateRateListFormArray(weightingListtoAdd, false)]),
      activationDate:[null]

    });
  }

  unlockForm() {
    if(this.mainForm){
      this.mainForm.enable({emitEvent:false , onlySelf:true});
    }
    this.disableAddEnergy = false;
    this.showDeleteIcon = true;
  }

  lockForm() {
    this.mainForm.disable({emitEvent:false , onlySelf:true});
    this.disableAddEnergy = true;
    this.showDeleteIcon = false;
    this.displayOptionsSelect = false;
  }

  saveCatEnergyList(list: any) {
       this.catEnergyService.saveCatEnergy(list).subscribe(data => {
         this.catNatureEnergyList = data;

        }, err => {
            //this is intentional
      }, () => {

        this.mainForm = this.fb.group({
          paramLines: this.fb.array([...this.initiateParamLinesFormArray(this.catNatureEnergyList, true)]),
        });

      });
  }

  deleteCatEnergyList(list: CatEnergyNatureModel[]) {
    this.catEnergyService.deleteCatEnergy(list).subscribe(data => {

    }, err => {
        //this is intentional
    }, () => {
      this.catNatureEnergyListToBeDeleted = [];
    });
  }


  getCatEnergyByNatureId(idNature: string){
    this.catEnergyService.getCatEnergyByNatureId(idNature).subscribe(data => {
      this.catNatureEnergyList = data;
    },err=>{
        //this is intentional
    },()=>{
      this.createMainFormGroup();
      if(this.catNatureEnergyList && this.catNatureEnergyList.length>0){
        this.showEnergies = true;
      }

    });
  }

  getCatEnergyByBrandId(idBrand: string){
    this.catEnergyService.getCatEnergyByBrandId(idBrand).subscribe(data => {
      this.catNatureEnergyList = data;
    },err=>{
        //this is intentional
    },() => {
      this.createMainFormGroup();
      if(this.catNatureEnergyList && this.catNatureEnergyList.length>0){
        this.showEnergies = true;
      }
    });
  }

  getNatureUnusedParamEnergy(idNature: string){
    this.paramEnergyService.getNatureUnusedParamEnergy(idNature).subscribe(
      data => {
        this.unusedEnergy = data;
      }
    );
  }

  getBrandUnusedParamEnergy(idNature:string, idBrand: string){
    this.paramEnergyService.getBrandUnusedParamEnergy(idNature, idBrand).subscribe(
      data => {
        this.unusedEnergy = data;
      }
    );
  }


  writeValue(value) {
    if (value) {
      this.value = value;
    }

    if (value === null && this.mainForm) {
      this.mainForm.reset();
    }
  }

  setDisabledState(disabled: boolean) {

    if (disabled) {
      this.lockForm()
    }
    else {
      this.unlockForm();
    }

  }

  removeItemFromList(list: any, item: any){
    const index = list.indexOf(item, 0);
    if (index > -1) {
       list.splice(index, 1);
    }
  }

  deleteEnergy(line: any) {
    //remove the blue color of highlight
    this.selectedEnergy = null;
    this.displayOptionsSelect = false;
    this.popupService.popupInfo('Êtes-vous sûr(e) de vouloir supprimer ? <br>'
      + 'En cliquant sur le boutton « confirmer », <br> Vous perdez toutes les modifications apportées.', 'Confirmer', null, 'Annuler', ConfirmationModalComponent);
    const subscription = this.popupService.onConfirm.subscribe((t) => {
      if (t) {

        //that means this item is already saved in the data base
        //then it should be added to the list to be delete when user launch the save
        if (line.value.id != null) {
          this.catNatureEnergyListToBeDeleted.push(line.value);
        }

        //the item must be pushed again to unusedEnergy
        this.unusedEnergy.push({ id: line.value.paramEnergy.id, label: line.value.label } as ParamEnergyModel);
        this.sortUnusedEnergyList();
        // remove it from the form
        this.removeEnergyFromParamLinesFormArray(line.value);

        if ((this.mainForm.controls["paramLines"] as FormArray).length === 0) {
          this.showEnergies = false;
        }

      }

      subscription.unsubscribe();
    });

  }

  removeEnergyFromParamLinesFormArray(item: any){
    let arrayForm = (this.mainForm.controls["paramLines"] as FormArray);

    arrayForm.removeAt(arrayForm.value.findIndex(e => e.paramEnergy.id === item.paramEnergy.id));
    this.mainForm.updateValueAndValidity();
  }

  undo(){
    this.resetUnusedEnergyList();
    this.mainForm.reset();
    this.createMainFormGroup();
    if (this.catNatureEnergyList && this.catNatureEnergyList.length > 0) {
      this.showEnergies = true;
    } else {
      this.showEnergies = false;
    }
    this.displayOptionsSelect = false;
  }

  setTouchedTrue(i: any): void {
    this.energyLinesControls[i].patchValue({
      isTouched: true
    })
  }

  saveModification(){

    if(this.catNatureEnergyListToBeDeleted.length>0){
      this.deleteCatEnergyList(this.catNatureEnergyListToBeDeleted);
    }
    if(this.mainForm.controls['paramLines'].value && this.mainForm.controls['paramLines'].value.length >0)
   { this.saveCatEnergyList(this.mainForm.controls['paramLines'].value);
  }
    this.lockForm();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  resetUnusedEnergyList() {
    //remove the blue color of highlight
    this.selectedEnergy = null;
    if (this.energyLinesControls && this.energyLinesControls.length > 0) {
      (this.energyLinesControls).forEach(element => {
        if (!element.value.id) {
          this.unusedEnergy.push({ id: element.value.paramEnergy.id, label: element.value.label } as ParamEnergyModel);
        }

      });
    }
    this.sortUnusedEnergyList();
  }

  sortUnusedEnergyList() {
    this.unusedEnergy.sort(function (a, b) {
      return a.label.localeCompare(b.label);
    });
  }

  checkIfBrandToAdd() {
    let path = this.route.url.split('/')[3];
    if (path == 'add' && this.route.url.split('/')[2] == 'brand') {
        this.idNature = this.route.url.split('/')[4];
        this.isBrandAddMode = true;
    }
    else {
        this.isBrandAddMode = false;
    }
  }

}
