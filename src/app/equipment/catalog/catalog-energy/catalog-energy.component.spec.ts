import { FormArray, FormBuilder } from '@angular/forms';
import { of, Subscription } from 'rxjs';
import { CatEnergyNatureWeightingModel } from 'src/app/shared/models/cat-energy-nature-weighting.model';
import { CatNatureModel } from 'src/app/shared/models/cat-nature.model';
import { ParamEnergyModel } from 'src/app/shared/models/param-energy.model';
import { CatEnergyNatureModel } from '../../../shared/models/cat-energy-nature.model';

import { CatalogEnergyComponent } from './catalog-energy.component';

describe('CatalogEnergyComponent', () => {
  let fixture: CatalogEnergyComponent;
  let formBuilderMock: any;
  let catEnergyServiceMock: any;
  let paramEnergyServiceMock: any;
  let subscriptionMock:Subscription[] = [];
  let popupServiceMock: any;
  let routerMock: any;

  let natureUnusedList = [{
    id: '1',
    label: 'Biocarburant',
  } as ParamEnergyModel,
  {
    id: '2',
    label: 'Diesel',
  } as ParamEnergyModel];

  let loadedCatEnergyList = [
    {
      id: '1',
      catNature: { id: '11'} as CatNatureModel,
      catBrand: null,
      isActive: true,
      paramEnergy: { id: '4', label: 'Biocarburant'} as ParamEnergyModel,
      catEnergyNatureWeightingList: [
        { id: '1', weightingRateYear: 1, weightingRateValue: 25}as CatEnergyNatureWeightingModel
      ] as CatEnergyNatureWeightingModel[]
    } as CatEnergyNatureModel
  ] as CatEnergyNatureModel[];


  beforeEach(() => {
    formBuilderMock = new FormBuilder();

    routerMock = {
      url: '/catalog/nature/447'
    }

    catEnergyServiceMock = {
      saveCatEnergy: jest.fn().mockReturnValue(of(true)),
      deleteCatEnergy: jest.fn().mockReturnValue(of(true)),
      getCatEnergyByNatureId: jest.fn().mockReturnValue(of(true)),
      getCatEnergyByBrandId: jest.fn().mockReturnValue(of(true))
    };
    popupServiceMock = {
      popupInfo: jest.fn().mockReturnValue(of(true)),
      onConfirm: of(true)
    };

    paramEnergyServiceMock = {
      getNatureUnusedParamEnergy: jest.fn().mockReturnValue(of(natureUnusedList)),
      getBrandUnusedParamEnergy: jest.fn().mockReturnValue(of(natureUnusedList)),
    };

    fixture = new CatalogEnergyComponent(
      formBuilderMock,
      catEnergyServiceMock,
      paramEnergyServiceMock, popupServiceMock, routerMock
    );
    fixture.subscriptions = subscriptionMock;
    fixture.idNature="125"
    fixture.ngOnInit();
  });

  describe('Test Component', () => {
    it('should Be Truthy', () => {
      expect(fixture).toBeTruthy();
    });
  });

  describe('Test: createForm', () => {
    it('should create form ', () => {
      fixture.createMainFormGroup();
      expect(fixture.mainForm).toBeTruthy();
    });
  });

  describe('Test showEnergyDiv', () => {
    it('should change displayOptionsSelect to false', () => {
      fixture.displayOptionsSelect = true;
      fixture.showEnergyDiv();
      expect(fixture.displayOptionsSelect).toEqual(false);
    });
  });

  describe('Test unlockForm', () => {
    it('should disableAddEnergy be false and showDeleteIcon be true', () => {
      fixture.disableAddEnergy = true;
      fixture.showDeleteIcon = false;
      fixture.unlockForm();
      expect(fixture.disableAddEnergy).toEqual(false);
      expect(fixture.showDeleteIcon).toEqual(true);

    });
  });

  describe('Test lockForm', () => {
    it('should disableAddEnergy be true and showDeleteIcon be false', () => {
      fixture.disableAddEnergy = false;
      fixture.showDeleteIcon = true;
      fixture.lockForm();
      expect(fixture.disableAddEnergy).toEqual(true);
      expect(fixture.showDeleteIcon).toEqual(false);

    });
  });

  describe('Test setDisabledState', () => {
    it('should lock form', () => {
      fixture.setDisabledState(true);
      expect(fixture.lockForm).toHaveBeenCalled;
    });

    it('should unlock form', () => {
      fixture.setDisabledState(false);
      expect(fixture.unlockForm).toHaveBeenCalled;
    });

  });
  describe('Test setTouchedTrue', () => {

    it('should set touched True', () => {
      fixture.addEnergy({ value: { id: "1", label: "label" } as ParamEnergyModel });
      fixture.setTouchedTrue(0);
      expect(fixture.energyLinesControls[0].get("isTouched").value).toEqual(true);
    });

  });

  describe('Test undo', () => {
    it('should showEnergies be true when catNatureEnergyList>0', () => {
      fixture.showEnergies = false;
      fixture.catNatureEnergyList = [
        {
          id: '1',
          paramEnergy: {
            label: 'ELECTRIQUE'
          } as ParamEnergyModel
        } as CatEnergyNatureModel
      ] as CatEnergyNatureModel[];

      fixture.undo();
      expect(fixture.createMainFormGroup).toHaveBeenCalled;
      expect(fixture.showEnergies).toEqual(true);
    });

    it('should showEnergies be false when catNatureEnergyList = 0', () => {
      fixture.showEnergies = false;
      fixture.catNatureEnergyList = [] as CatEnergyNatureModel[];

      fixture.undo();
      expect(fixture.createMainFormGroup).toHaveBeenCalled;
      expect(fixture.showEnergies).toEqual(false);
    });
  });

  describe('Test undo', () => {
    it('should showEnergies be true', () => {
      expect(fixture.energyLinesControls).toEqual((fixture.mainForm.controls["paramLines"] as FormArray).controls);
    });
  });

  describe('Test removeItemFromList', () => {
    it('should remove given item from unusedEnergy list', () => {
      expect(fixture.unusedEnergy.length).toEqual(2);
      fixture.removeItemFromList(fixture.unusedEnergy, fixture.unusedEnergy[1]);
      expect(fixture.unusedEnergy.length).toEqual(1);
    });
  });


  describe('Test writeValue', () => {
    it('should  write value', () => {
      let value = {
        paramLines: []
      };
      fixture.writeValue(value);
      expect(fixture.value).toBeDefined();
    });

    it('should reset mainForm when value is null', () => {
      let value = null;
      fixture.writeValue(value);
      expect(fixture.mainForm.reset).toHaveBeenCalled;
    });
  });


  describe('Test saveModification', () => {
    it('should remove given item from unusedEnergy list', () => {
      fixture.catNatureEnergyListToBeDeleted = [
        {
          id :'1'
        }as CatEnergyNatureModel
      ] as CatEnergyNatureModel[];

      fixture.saveModification();
      expect(fixture.saveCatEnergyList).toHaveBeenCalled;
      expect(fixture.lockForm).toHaveBeenCalled;

    });
  });


  describe('Test ngOnDestroy', () => {
    it('should remove subscriptions', () => {
      const cleanSub = [];
      fixture.subscriptions.map(s => cleanSub.push(s));
      fixture.ngOnDestroy();
      cleanSub.map(s => s.unsubscribe);
      expect(fixture.subscriptions).toEqual(cleanSub);
    });
  });

  describe('Test deleteCatEnergyList', () => {
    it('should reset catNatureEnergyListToBeDeleted list', () => {
      fixture.catNatureEnergyListToBeDeleted = [
        {
          id :'1'
        }as CatEnergyNatureModel
      ] as CatEnergyNatureModel[];
      fixture.deleteCatEnergyList(fixture.catNatureEnergyListToBeDeleted);
      expect(fixture.catNatureEnergyListToBeDeleted.length).toEqual(0);
    });
  });

  describe('Test deleteEnergy', () => {
    it('should set displayOptionsSelect to false and catNatureEnergyListToBeDeleted.length be 1', () => {
      //reset the delete list
      fixture.catNatureEnergyListToBeDeleted = [];
      let energyToDelet = {
        value: {
          id: '1',
          label: 'Diesel',
          paramEnergy: {
            id: '1'
          }
        }
      };
      fixture.deleteEnergy(energyToDelet);
      expect(fixture.displayOptionsSelect).toEqual(false);
      expect(fixture.catNatureEnergyListToBeDeleted.length).toEqual(1);
    });
  });


  describe('Test checkIfBrandToAdd', () => {
    it('should isBrandAddMode be true when route url contains brand/add', () => {
        routerMock.url = "/catalog/brand/add/124";
        fixture.checkIfBrandToAdd();
      expect(fixture.isBrandAddMode).toEqual(true);
    });
  });

  describe('Test ngOnInit Brand add mode', () => {
    it('should unusedEnergy=natureUnusedList when route url contains brand/add', () => {
        routerMock.url = "/catalog/brand/add/124";
        fixture.ngOnInit();
      expect(fixture.isBrandAddMode).toEqual(true);
      expect(fixture.getBrandUnusedParamEnergy).toHaveBeenCalled;
      expect(fixture.unusedEnergy).toEqual(natureUnusedList);
    });
  });

  describe('Test ngOnInit Brand add mode', () => {
    it('should unusedEnergy=brandUnusedList when route url contains brand/add', () => {
        routerMock.url = "/catalog/brand/524";
        fixture.idBrand="524"
        fixture.unusedEnergy= []
        fixture.ngOnInit();
      expect(fixture.isBrandAddMode).toEqual(false);
      expect(fixture.getCatEnergyByBrandId).toHaveBeenCalled;
      expect(fixture.unusedEnergy).toEqual(natureUnusedList);
    });
  });

  describe('Test ngOnInit Brand add mode', () => {
    it('should unusedEnergy=brandUnusedList when route url contains brand/add', () => {
        routerMock.url = "/catalog/brand/524";
        fixture.idBrand="524"
        fixture.unusedEnergy= []
        fixture.ngOnInit();
      expect(fixture.isBrandAddMode).toEqual(false);
      expect(fixture.getCatEnergyByBrandId).toHaveBeenCalled;
      expect(fixture.unusedEnergy).toEqual(natureUnusedList);
    });
  });

  describe('Test ngOnInit Brand add mode', () => {
    it('should unusedEnergy=brandUnusedList when route url contains brand/add', () => {
      spyOn(catEnergyServiceMock, 'saveCatEnergy').and.returnValue(of(loadedCatEnergyList))
      fixture.isBrandAddMode = true;
      fixture.saveCatEnergyList(null);
      expect(fixture.isBrandAddMode).toEqual(true);
    });
  });


});
