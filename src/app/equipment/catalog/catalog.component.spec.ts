
import { of, Subject } from 'rxjs';
import { CatalogComponent } from './catalog.component';


describe('CatalogueComponent', () => {
  let fixture: CatalogComponent;
  let nodeServiceMock: any;
  let routerMock: any;
  let dataSharingMock: any;


  beforeEach(() => {
    routerMock = {
      data: ['catalog', 'brand', 'data'],
      navigate: jest.fn().mockReturnValue(new Promise(() => {
      //this is intentional
       })),
      url: '/catalog/nature/447'
    }

    dataSharingMock = {
      changeStatus: jest.fn().mockReturnValue((true))
    };
  
    nodeServiceMock = {
      getBrand: jest.fn().mockReturnValue(of([
        {
          data: '0', label: 'QUERCY', expandedIcon: null, collapsedIcon: null, children: [
            { type: 'type', data: '0', label: 'TYPE', parent: { data: '0' } }
          ], leaf: true, nodeType: 'brand'
        },
        {
          data: '1', label: 'MCI', expandedIcon: null, collapsedIcon: null, children: [
            { type: 'type', data: '0', label: 'TYPE', parent: { data: '0' } }
          ], leaf: true, nodeType: 'brand'
        }

      ])),
      getAssetType: jest.fn().mockReturnValue(of([
        { data: '0', label: 'NS', icon: null },
        { data: '1', label: 'REFROIDISSUER A2P ', icon: null }

      ])),
      getNature: jest.fn().mockReturnValue(of([
        { data: '0', label: 'Abatteuse', icon: null },
        { data: '1', label: 'Abatteuse A2P ', icon: null }

      ])),
      getActivitySectorsWithNature: jest.fn().mockReturnValue(of(
        {
          data: [
            {
              type: 'sector', data: '0', label: 'ACT SECT', children: [
                {
                  type: 'nature', data: '0', label: 'NAT', parent: { data: '0' }, children: [
                    {
                      type: 'brand', data: '0', label: 'BRAND', parent: { data: '0' }, children: [
                        { type: 'type', data: '0', label: 'TYPE', parent: { data: '0' } }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        }

      )),
      nodeAddSubject: new Subject(),
      nodeSubject: new Subject(),
      nodeDeleteSubject: new Subject()

    }

    fixture = new CatalogComponent(
      nodeServiceMock,
      routerMock,dataSharingMock
    );
    fixture.ngOnInit();
  });



  describe('Test:  ngOnInit', () => {
    it('should return brand nodeAddSubject', () => {

      nodeServiceMock.nodeAddSubject.next({
        type: 'brand', data: '0', label: 'BRAND', parent: { data: '0', parent: { data: '0' } }, children: [
          { type: 'type', data: '0', label: 'TYPE', parent: { data: '0' } }
        ]
      })
      fixture.ngOnInit();
      expect(fixture.tree.find).toBeTruthy();


    });

    it('should return type nodeAddSubject', () => {
      nodeServiceMock.nodeAddSubject.next({ type: 'type', data: '0', label: 'TYPE', parent: { data: '0', parent: { data: '0', parent: { data: '0' } } } })

      fixture.ngOnInit();
      expect(fixture.tree.find).toBeTruthy();


    });

    it('should return nature nodeAddSubject', () => {
      nodeServiceMock.nodeAddSubject.next({ type: 'nature', data: '0', label: 'nature', parent: { data: '0', parent: { data: '0', parent: { data: '0' } } } })

      fixture.ngOnInit();
      expect(fixture.tree.find).toBeTruthy();


    });

    it('should return type nodeDeleteSubject', () => {
      nodeServiceMock.nodeDeleteSubject.next({ type: 'type', data: '0', label: 'TYPE', parent: { data: '0', parent: { data: '0', parent: { data: '0' } } } })

      fixture.ngOnInit();
      expect(fixture.tree.find).toBeTruthy();


    });

    it('should return node nodeDeleteSubject', () => {
      nodeServiceMock.nodeDeleteSubject.next({
        type: 'brand', data: '0', label: 'BRAND', parent: { data: '0', parent: { data: '0' } }, children: [
          { type: 'type', data: '0', label: 'TYPE', parent: { data: '0' } }
        ]
      })
      fixture.ngOnInit();
      expect(fixture.tree.find).toBeTruthy();


    });

  });

  describe('Test:  nodeExpand', () => {
    it('should return brand', () => {
      let evt = ({
        node: ({
          nodeType: 'brand'
        })

      });

      fixture.nodeExpand(evt);
      expect(nodeServiceMock.getAssetType).toHaveBeenCalled();


    });
    it('should return nature', () => {
      let evt = ({
        node: ({
          nodeType: 'nature'
        })

      });

      fixture.nodeExpand(evt);
      expect(nodeServiceMock.getBrand).toHaveBeenCalled();


    });
  });

  describe('Test:  nodeSelect', () => {
    it('should be called', () => {
      let evt = ({
        node: ({
          nodeType: 'brand',
          data: 'data'
        })

      });

      fixture.nodeSelect(evt);
      expect(routerMock.navigate).toHaveBeenCalledWith(routerMock.data);

    });

    it('should isAddDisabled be false', () => {
      let evt = ({
        node: ({
          nodeType: 'type',
          data: 'data'
        })

      });

      fixture.nodeSelect(evt);
      expect(fixture.isAddDisabled).toEqual(true);

    });


  });

  describe('Test:  label change', () => {
    it('label sector change', () => {
      nodeServiceMock.nodeSubject.next({ type: 'sector', data: '0', label: 'ACT SECT TEST' })
      expect(fixture.tree[0].label).toEqual('ACT SECT TEST')

    });

    it('label nature change', () => {
      nodeServiceMock.nodeSubject.next({ type: 'nature', data: '0', label: 'NATURE TEST', parent: { data: '0' } })
      expect(fixture.tree[0].children[0].label).toEqual('NATURE TEST')

    });

    it('label brand change', () => {
      nodeServiceMock.nodeSubject.next({ type: 'brand', data: '0', label: 'BRAND TEST', parent: { data: '0', parent: { data: '0' } } })
      expect(fixture.tree[0].children[0].children[0].label).toEqual('BRAND TEST')

    });

    it('label type change', () => {
      nodeServiceMock.nodeSubject.next({ type: 'type', data: '0', label: 'TYPE TEST', parent: { data: '0', parent: { data: '0', parent: { data: '0' } } } })
      expect(fixture.tree[0].children[0].children[0].children[0].label).toEqual('TYPE TEST')

    });

  });

  describe('Test:  add', () => {
    it('should navigate brand be called', () => {
      fixture.add();
      expect(routerMock.navigate).toHaveBeenCalledWith(['catalog', 'brand', 'add', '447']);
    });
    it('should navigate type be called', () => {
      routerMock.url = '/catalog/brand/447'

      fixture = new CatalogComponent(
        nodeServiceMock,
        routerMock,dataSharingMock
      );
      fixture.add();
      expect(routerMock.navigate).toHaveBeenCalledWith(['catalog', 'type', 'add', '447']);
    });
    it('should navigate to nature', () => {
      routerMock.url = '/catalog/sector/107'

      fixture = new CatalogComponent(
        nodeServiceMock,
        routerMock,dataSharingMock
      );
      fixture.add();
      expect(routerMock.navigate).toHaveBeenCalledWith(['catalog', 'nature', 'add', '107']);
    });
  });

  
  describe('Test:  onClickHome', () => {
    it('should redirect to home', () => {
      fixture.onClickHome();
      expect(routerMock.navigate).toHaveBeenCalledWith(['catalog']);
    });

  });

  describe('Test:  ngOnDestroy', () => {
    it('should change status', () => {
      fixture.ngOnDestroy();
      expect(dataSharingMock.changeStatus).toHaveBeenCalled();
    });

  });

  

});
