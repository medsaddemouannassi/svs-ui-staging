import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { BehaviorSubject, forkJoin, of, Subject, Subscription, throwError } from 'rxjs';
import { CatBrandModel } from '../../../shared/models/cat-brand.model';
import { CatDiscountRateModel } from '../../../shared/models/cat-discount-rate.model';
import { CatDiscountModel } from '../../../shared/models/cat-discount.model';
import { CatDocumentModel } from '../../../shared/models/cat-document.model';
import { CatNatureModel } from '../../../shared/models/cat-nature.model';
import { CatRequestModel } from '../../../shared/models/cat-request.model';
import { CatRequestWaiting } from '../../../shared/models/catalog-add-request.model';
import { ParamSettingModel } from '../../../shared/models/param-setting.model';
import { StatusConstant } from '../../../shared/status-constant';
import { CatalogEnergyComponent } from '../catalog-energy/catalog-energy.component';
import { CatalogNewDiscountsComponent } from '../catalog-new-discounts/catalog-new-discounts.component';
import { CatalogOldDiscountsComponent } from '../catalog-old-discounts/catalog-old-discounts.component';
import { MenuCatBrandComponent } from "./menu-cat-brand.component";

describe('MenuCatBrandComponent', () => {
    let fixture: MenuCatBrandComponent;
    let popupServiceMock: any;
    let routerMock: any;
    let catBrandServiceMock: any;
    let valuationServiceMock: any;
    let formBuilderMock: any;
    let subscriptionMock: Subscription;
    let catDiscountServiceMock: any;
    let catBrandObject: CatBrandModel = {} as any;
    let catDiscountObject: CatDiscountModel = {} as any;

    let nodeServiceMock: any;
    let CatDiscountRateServiceMock: any;
    let routeMock;
    let catNatureServiceMock: any;
    let catActivitySectorServiceMock: any;
    let catRequestServiceMock: any;

    let catalogAddRequestWorkflowServiceMock: any;

    let catalogServiceMock: any;
    let paramSettingServiceMock: any;
    let catNature: any;
    let catSecors: any[];
    let catdiscountRateList: CatDiscountRateModel[];
    let fullCatDiscount: any;
    let catDocumentServiceMock: any;
    let dataSharingMock: any;

    let documentsList: CatDocumentModel[] = [{

        "docName": "document1",
    } as CatDocumentModel, {

        "docName": "document2"

    } as CatDocumentModel];

    let documentsListInherated: CatDocumentModel[] = [{

        "docName": "document1",
        catBrand: null,
        catNature: { id: '133' }
    } as CatDocumentModel, {

        "docName": "document2",
        catBrand: null,
        catNature: null

    } as CatDocumentModel];

    beforeEach(() => {
        catdiscountRateList = [{
            id: "1",
            catDiscount: catDiscountObject,
            discountRateYear: 2012,
            discountRateValue: 12
        }, {
            id: "2",
            catDiscount: null,
            discountRateYear: 2012,
            discountRateValue: 12
        },
        {
            id: "3",
            catDiscount: null,
            discountRateYear: 2012,
            discountRateValue: 20
        },
        {
            id: "4",
            catDiscount: null,
            discountRateYear: 2012,
            discountRateValue: 5
        }
        ];
        catDiscountObject = {
            id: '66',
            maxFinPer: 52,
            minFinPer: 33,
            discount: 25,
            catBrand: null,
            catNature: {
                id: '133',
                label: 'nature label',
                catActivitySector: {
                    id: '1',
                    label: 'Transport',
                    comment: 'comment act sector',
                    isActive: true
                },
                comment: 'comment nature',
                paramNap: null,
                isActive: true
            } as CatNatureModel,
            catActivitySector: null,
            isPerm: true,
            activationDate: null,
            endDate: null,
            catAssetType: null,
            enableRecov: null,
            enableValuation: null,
            ponderationRate: null,
            creation: '20',
            lastValuationDone: null,
            isReadjust: false,
            isNew: true,
            comment: 'comment nature',
            endDateAverLabel: null,
            nbMinValAver: 3,
            startDateAverLabel: "14/10/2015",
        }
        catBrandObject = {
            id: '122',
            label: 'QSD',
            catNature: {
                id: '133',
                label: 'nature label',
                catActivitySector: {
                    id: '1',
                    label: 'Transport',
                    comment: 'comment act sector',
                    isActive: true
                },
                comment: 'comment nature',
                paramNap: null,
                isActive: true
            } as CatNatureModel,
            comment: 'commentaire brand',
            isActive: true,
            catRequest: null,
        }
        catBrandObject.catNature = {
            id: '133',
            label: 'nature label',
            catActivitySector: null,
            comment: 'comment nature',
            paramNap: null,
            isActive: true

        }as CatNatureModel;

        catBrandObject.catNature.catActivitySector = {
            id: '1',
            label: 'Transport',
            comment: 'comment act sector',
            isActive: true
        }

        nodeServiceMock = {
            nodeSubject: new Subject<any>(),
            nodeAddSubject: new Subject<any>()
        }

        subscriptionMock = new Subscription();

        catBrandServiceMock = {
            getCatBrandById: jest.fn().mockReturnValue(of(catBrandObject)),
            saveCatBrand: jest.fn().mockReturnValue(of(catBrandObject)),
            deleteCatBrandById: jest.fn().mockReturnValue(of(true))

        };
        valuationServiceMock = {
            getLast10ValuationsByBrandId: jest.fn().mockReturnValue(of(true)),

        };
        catDocumentServiceMock = {
            uploadDocument: jest.fn().mockReturnValue(of(true)),
            searchDocument: jest.fn().mockReturnValue(of(documentsList)),
            downloadDocument: jest.fn().mockReturnValue(of(true)),
            updateDocument: jest.fn().mockReturnValue(of(true)),
            searchInheratedDocuments: jest.fn().mockReturnValue(of(documentsListInherated)),

        }
        popupServiceMock = {
            popupInfo: jest.fn().mockReturnValue(of(true)),
            popupCommunication: jest.fn().mockReturnValue(of(true)),
            onConfirmCommunication: new Subject(),
            onConfirm: of(true)
        };
        formBuilderMock = new FormBuilder();

        routeMock = {
            params: of('1')

        }
        dataSharingMock = {
            changeStatus: jest.fn().mockImplementation(() => { }),
            lockBtnStatus: new BehaviorSubject(false),
            triggerCommunication: new Subject()

        }

        fullCatDiscount = {
            catDiscount: catDiscountObject,
            catDiscountRateList: catdiscountRateList
        }
        catDiscountServiceMock = {
            getDiscountDetailsByTypeIdOrBrandIdOrNatureIdOrSectorId: jest.fn().mockReturnValue(of(catDiscountObject)),
            saveCatDiscountAndRate: jest.fn().mockReturnValue(of(fullCatDiscount))

        }
        CatDiscountRateServiceMock = {
            getCatDiscountRateByCatDiscountId: jest.fn().mockReturnValue(of(catdiscountRateList))
        }
        routerMock = {
            navigate: jest.fn(),
            url: '/catalog/brand/9158'
        };

        catNature = {
            id: '133',
            label: 'nature label',
            catActivitySector: {
                id: '133',
                label: 'nature label',

            },
            comment: 'comment nature',
            paramNap: null,
            isActive: true
        }

        catSecors = [{ id: '1', label: 'setc label' }]
        catNatureServiceMock = {
            getCatNatureById: jest.fn().mockReturnValue(of(catNature)),
            getNatureListByCatActivitySectorID: jest.fn().mockReturnValue(of([catNature]))

        }

        catActivitySectorServiceMock = {
            getActivitySectorList: jest.fn().mockReturnValue(of(catSecors)),
        }

        catRequestServiceMock = {
            loadCatRequestDocs: jest.fn().mockReturnValue(of([{ docName: 'xxx' }])),
        }
        catalogServiceMock = {
            fillInfoCreationUser: jest.fn().mockReturnValue(of("Batch"))
          }
        catalogAddRequestWorkflowServiceMock = {
            saveCatRequestWorkflow: jest.fn().mockReturnValue(of(true)),
        }
        paramSettingServiceMock = {
            getParamByCode: jest.fn().mockReturnValue(of([{
                id: '11',
                label: 'En attente',
                value: StatusConstant.STATUS_RQST_WKFL
            } as ParamSettingModel,
            {
                id: '12',
                label: 'En attente d\'info',
                value: StatusConstant.STATUS_RQST_WKFL
            } as ParamSettingModel]))
        }

        fixture = new MenuCatBrandComponent(
            formBuilderMock,
            routeMock,
            catBrandServiceMock,
            valuationServiceMock,
            popupServiceMock,
            catDiscountServiceMock,
            nodeServiceMock,
            CatDiscountRateServiceMock,
            routerMock,
            catNatureServiceMock,
            catActivitySectorServiceMock,
            catDocumentServiceMock,
            catRequestServiceMock,
            catalogAddRequestWorkflowServiceMock,
            paramSettingServiceMock,
            dataSharingMock,
            catalogServiceMock
        );
        fixture.ngOnInit();
        fixture.formMenu.controls['decoteNew'].patchValue({
            catDiscountRateList: catdiscountRateList
        });
        fixture.formMenu.controls['decoteOld'].patchValue({
            catDiscountRateList: catdiscountRateList
        });

        let engPopupServiceMock: any;
        let engFormBuilderMock: any;
        let engCatEnergyServiceMock: any;
        let engParamEnergyServiceMock: any;
        let engRouterMock: any;

        engFormBuilderMock = new FormBuilder();
        fixture.catalogEnergyComponent = new CatalogEnergyComponent(
            engFormBuilderMock,
            engCatEnergyServiceMock,
            engParamEnergyServiceMock,
            engPopupServiceMock, engRouterMock);
        fixture.catalogEnergyComponent.mainForm = new FormGroup({});

    });

    describe('Test: ngOnInit', () => {

        it('should variables be setted ', () => {
            expect(fixture.brandTitle).toEqual(fixture.catBrandObject.label);
            expect(fixture.natureTitle).toEqual(fixture.catBrandObject.catNature.label);
            expect(fixture.activitySectorTitle).toEqual(fixture.catBrandObject.catNature.catActivitySector.label);
        });

        it('should catBrandServiceMock throws error ', () => {
            spyOn(catBrandServiceMock, 'getCatBrandById').and.returnValue(throwError({ status: 404 }));
            fixture.ngOnInit();
        });

        it('should valuationServiceMock throws error ', () => {
            spyOn(valuationServiceMock, 'getLast10ValuationsByBrandId').and.returnValue(throwError({ status: 404 }));
            fixture.ngOnInit();
        });

    });

    describe('Test ngOnDestroy', () => {

        it('should unsubscribe', () => {
            fixture.ngOnDestroy();
            const spyunsubscribe = jest.spyOn(subscriptionMock, 'unsubscribe');
            expect(spyunsubscribe).toBeDefined();
        });
    });

    describe('Test MenuCatBrandComponent', () => {
        it('should Be Truthy', () => {
            expect(fixture).toBeTruthy();
        })
    });

    describe('Test: resetCatSector', () => {
        it('resetCatSector ', () => {
            fixture.resetCatSector();
            expect(fixture.catBrandObject.catNature).toBeNull();
        });
    });

    describe('Test: resetCatNature', () => {
        it('resetCatNature ', () => {
            fixture.resetCatNature();
            expect(fixture.catBrandObject.catNature).toBeNull();
        });
    });

    describe('Test initComponent', () => {
        it('should Be Truthy', () => {
            fixture.isAddMode = true;
            let param = { id: 1 }
            fixture.initComponent(param);
        })
        it('shouldthrow error', () => {
            fixture.isAddMode = true;
            let param = { id: 1 }
            fixture.catBrandObject = {} as CatBrandModel;
            spyOn(catNatureServiceMock, 'getCatNatureById').and.returnValue(throwError({ status: 404 }));

            fixture.initComponent(param);

        })

    });

    describe('Test editBrand()', () => {
        it('should modifModeActivatedOld be true', () => {
            fixture.editBrand();
            expect(fixture.modifModeActivatedOld).toEqual(true);
        })
    });

    describe('Test generateCatDiscount()', () => {
        it('should return form value', () => {
            let decoteForm = new FormControl({
                isReadjust: null,
                isPerm: false,
                temporary: '',
                startingYear1: '',
                startingNumber: '',
                percentage: null,
                startingYear2: '',
                startingNumber2: null,
                comment: 'comment'
            });
            fixture.catDiscountObject = {
                id: '66',
                maxFinPer: 52,
                minFinPer: 33,
                discount: 25,
                catBrand: null,
                catNature: null,
                catActivitySector: null,
                isPerm: null,
                activationDate: null,
                endDate: null,
                catAssetType: null,
                enableRecov: null,
                enableValuation: null,
                ponderationRate: null,
                creation: '20',
                lastValuationDone: null,
                isReadjust: true,
                isNew: true,
                comment: 'comment cat discount test'
            } as CatDiscountModel
            let avgdecoteForm = new FormControl({

            })
            fixture.generateCatDiscount(decoteForm, avgdecoteForm);
            expect(fixture.catDiscountObject.isReadjust).toEqual(false);
        });

    });

    describe('Test undo()', () => {
        it('should navigate', () => {
            fixture.isAddMode = true
            fixture.undo();
            expect(popupServiceMock.popupInfo).toHaveBeenCalled();
        })
        it('should call popup', () => {
            fixture.undo();
            expect(popupServiceMock.popupInfo).toHaveBeenCalled();
        })
        it('should update form', () => {
           
            fixture.catalogOldDiscountsComponent = new CatalogOldDiscountsComponent(formBuilderMock, popupServiceMock, catDiscountServiceMock, CatDiscountRateServiceMock
                , catDocumentServiceMock, routerMock);
            fixture.catalogNewDiscountsComponent = new CatalogNewDiscountsComponent(formBuilderMock, popupServiceMock, catDiscountServiceMock, catDocumentServiceMock);
            spyOn(fixture.catalogOldDiscountsComponent, 'undo').and.returnValue(of(true));
            spyOn(fixture.catalogNewDiscountsComponent, 'undo').and.returnValue(of(true));

            fixture.undo();
            expect(popupServiceMock.popupInfo).toHaveBeenCalled();
        })
    });

    describe('Test changeTab()', () => {
        it('should be 3', () => {
            fixture.changeTab(3);
            expect(fixture.selectedPlan).toEqual(3);
        })
    });

    describe('Test updateCatBrandObject()', () => {
        it('should change values of variables', () => {
            fixture.formMenu.controls['generalities'].patchValue({
                brandLabel: 'MybrandLabel',
                brandComment: 'MybrandComment',
                dureeMin: 24,
                dureeMax: 12,
                seuilVr: 52,
                creation: '21'
            });
            fixture.catBrandObject = {
                id: '122',
                label: 'QSD',
                catNature: null,
                comment: 'commentaire noo',
                catRequest: null,
                isActive: true
            }
            fixture.catDiscountObject = {
                id: '66',
                maxFinPer: 52,
                minFinPer: 33,
                discount: 25,
                catBrand: null,
                catNature: null,
                catActivitySector: null,
                isPerm: null,
                activationDate: null,
                endDate: null,
                catAssetType: null,
                enableRecov: null,
                enableValuation: null,
                ponderationRate: null,
                creation: '20',
                lastValuationDone: null,
                isReadjust: false,
                isNew: true,
                comment: 'comment cat discount test',
                startDateAverLabel: '',
                endDateAverLabel: '',
                nbMinValAver: 3
            }
            fixture.updateCatBrandObject();
            expect(fixture.catBrandObject.label).toEqual(fixture.formMenu.controls['generalities'].get('brandLabel').value);
            expect(fixture.catBrandObject.comment).toEqual(fixture.formMenu.controls['generalities'].get('brandComment').value);
        })
        it('should change values of variables when catRequest', () => {
            fixture.formMenu.controls['generalities'].patchValue({
                brandLabel: 'MybrandLabel',
                brandComment: 'MybrandComment',
                dureeMin: 24,
                dureeMax: 12,
                seuilVr: 52,
                creation: '21'
            });
            fixture.catBrandObject = {
                id: '122',
                label: 'QSD',
                catNature: null,
                comment: 'commentaire noo',
                catRequest: null,
                isActive: true
            }
            fixture.catDiscountObject = {
                id: '66',
                maxFinPer: 52,
                minFinPer: 33,
                discount: 25,
                catBrand: null,
                catNature: null,
                catActivitySector: null,
                isPerm: null,
                activationDate: null,
                endDate: null,
                catAssetType: null,
                enableRecov: null,
                enableValuation: null,
                ponderationRate: null,
                creation: '20',
                lastValuationDone: null,
                isReadjust: false,
                isNew: true,
                comment: 'comment cat discount test',
                startDateAverLabel: '',
                endDateAverLabel: '',
                nbMinValAver: 3
            }

            fixture.catRequest = {
                id: '15',
                catActivitySector: ' sector',
                catNature: 'nature',
                idSector: '1',
                idNature: '133',
                label: 'labell',
                comment: 'comment'
            } as CatRequestWaiting
            fixture.isAddMode = true
            fixture.updateCatBrandObject();
            expect(fixture.catBrandObject.catRequest).toEqual({ id: '15' } as CatRequestModel);
        })
    });

    describe('Test saveBrand()', () => {
        it('should call popup', () => {

            fixture.saveBrand();
            expect(popupServiceMock.popupInfo).toHaveBeenCalled();
        });
        it('should saveBrand change value ', () => {

            fixture.saveBrand();
            expect(fixture.catDiscountObject).toEqual(fullCatDiscount.catDiscount)
            expect(fixture.initialCatDiscountObject).toEqual(catDiscountObject)
            expect(fixture.catDiscountRateList).toEqual(fullCatDiscount.catDiscountRateList)
            expect(fixture.initialCatDiscountRateList).toEqual(catdiscountRateList)

        });
        it('should saveBrand change value when isAddMode is true', () => {
            fixture.isAddMode = true;
            fixture.saveBrand();
            expect(fixture.catDiscountObject).toEqual(fullCatDiscount.catDiscount)
            expect(fixture.initialCatDiscountObject).toEqual(catDiscountObject)
            expect(fixture.catDiscountRateList).toEqual(fullCatDiscount.catDiscountRateList)
            expect(fixture.initialCatDiscountRateList).toEqual(catdiscountRateList)

        });

        it('should saveBrand change value when isAddMode is true && is catRequest', () => {
            fixture.isAddMode = true;
            fixture.catRequest = {
                id: '15',
                catActivitySector: ' sector',
                catNature: 'nature',
                idSector: '1',
                idNature: '133',
                label: 'labell',
                comment: 'comment'
            } as CatRequestWaiting

            fixture.statusList = [{ id: '22', label: 'validé', value: StatusConstant.STATUS_RQST_WKFL_VAL } as ParamSettingModel]
            fixture.saveBrand();

            catBrandServiceMock.saveCatBrand().subscribe((data) => {
                expect(catalogAddRequestWorkflowServiceMock.saveCatRequestWorkflow).toHaveBeenCalled()
            })

        });
        it('should saveBrand throws error ', () => {
            spyOn(catBrandServiceMock, 'saveCatBrand').and.returnValue(throwError({ status: 404 }));
            fixture.saveBrand();
        });

    });

    describe('Test get f()', () => {
        it('should return form controls', () => {
            expect(fixture.f).toEqual(fixture.formMenu.controls);
        })
    });

    describe('Test get value()', () => {
        it('should return form value', () => {
            expect(fixture.value).toEqual(fixture.formMenu.value);
        })
    });

    describe('Test deleteBrand', () => {
        it('should call deleteBrand', () => {
            fixture.deleteBrand();
            expect(popupServiceMock.popupInfo).toHaveBeenCalled();
        })
    });

    describe('Test changeNewDiscount', () => {
        it('should set rateNewDiscountFirstYear ', () => {
            let test = 2;
            fixture.changeNewDiscount(test);
            expect(fixture.rateNewDiscountFirstYear).toEqual(test);
        })
    });

    describe('Test isFormValid', () => {

        it('should test validity on each tab ', () => {

            expect(fixture.isFormValid(0)).toEqual(fixture.formMenu.controls.generalities.valid);
            expect(fixture.isFormValid(1)).toEqual(fixture.formMenu.controls.decoteNew.valid);
            expect(fixture.isFormValid(2)).toEqual(fixture.formMenu.controls.decoteOld.valid);
            expect(fixture.isFormValid(3)).toEqual(true);


        })
    });


    describe('Test editBrandToAdd', () => {

        it('should editBrand when is add mode ', () => {
            fixture.isAddMode = true;
            fixture.editBrandToAdd();
            expect(fixture.modifModeActivatedOld).toEqual(true);



        })

        it('should not editBrand when is not add mode ', () => {
            fixture.isAddMode = false;
            fixture.modifModeActivatedOld = false
            fixture.editBrandToAdd();
            expect(fixture.modifModeActivatedOld).toEqual(false);



        })
    });

    describe('Test:  deleteDocs', () => {
        it('should remove docs ', () => {
            fixture.deleteDocument(1);

            expect(popupServiceMock.popupInfo).toHaveBeenCalled();
            popupServiceMock.onConfirm.subscribe((t) => {

                expect(fixture.deletedBrandDocs.length).toEqual(1);
            })



        });
        it('should remove docs ', () => {
            popupServiceMock = {
                onConfirm: jest.fn().mockReturnValue(of(true))
            }

            jest.spyOn(subscriptionMock, 'unsubscribe');

            fixture.deleteDocument(1);





        });

    });
    describe('Test arraysEqual', () => {
        it('should return false when size is not equal', () => {
            let arrayOne = [1, 2];
            let arraytwo = [1];

            fixture.changeNewDiscount(test);
            expect(fixture.arraysEqual(arrayOne, arraytwo)).toEqual(false);
        })
        it('should return false when array is not equal', () => {
            let arrayOne = [2];
            let arraytwo = [1];

            fixture.changeNewDiscount(test);
            expect(fixture.arraysEqual(arrayOne, arraytwo)).toEqual(false);
        })
        it('should return true when array is equal', () => {
            let arrayOne = [1];
            let arraytwo = [1];

            fixture.changeNewDiscount(test);
            expect(fixture.arraysEqual(arrayOne, arraytwo)).toEqual(true);
        })
    });

    describe('Test loadBrandInfo', () => {
        it('should set variables ', () => {
            fixture.loadBrandInfo();
            expect(fixture.catBrandObject).toEqual(catBrandObject);
            expect(fixture.brandTitle).toEqual(catBrandObject.label);
            expect(fixture.natureTitle).toEqual(catBrandObject.catNature.label);
            expect(fixture.comment).toEqual(catBrandObject.catNature.comment);
            expect(fixture.activitySectorTitle).toEqual(catBrandObject.catNature.catActivitySector.label);

        })
    });



    describe('Test generateCatOldDiscount', () => {
        it('should set catOldDiscountObject from decoteForm ', () => {
            let decoteForm = new FormControl({
                ponderationRate: 50,
                maxFinPer: '',
                isReadjust: '',
                isPerm: '',
                comment: '',
                endDate: '',

            });
            let avgdecoteForm = new FormControl({

            })
            fixture.generateCatOldDiscount(decoteForm, avgdecoteForm);
            expect(fixture.catOldDiscountObject.ponderationRate).toEqual(50);


        });

    });

    describe('Test initComponent when isAddMode is true', () => {
        it('should initialise catBrandObject ', () => {
            let param = {
                id: 50
            }

            routeMock = {
                navigate: jest.fn(),
                url: '/catalog/brand/add/50'
            };


            fixture = new MenuCatBrandComponent(
                formBuilderMock,
                routerMock,
                catBrandServiceMock,
                valuationServiceMock,
                popupServiceMock,
                catDiscountServiceMock,
                nodeServiceMock,
                CatDiscountRateServiceMock,
                routeMock,
                catNatureServiceMock,
                catActivitySectorServiceMock,
                catDocumentServiceMock,
                catRequestServiceMock,
                catalogAddRequestWorkflowServiceMock,
                paramSettingServiceMock,
                dataSharingMock,
                catalogServiceMock
            );
            fixture.initIsAddMode();
            fixture.initComponent(param);
            expect(fixture.natureTitle).toEqual(catNature.label);
            expect(fixture.catBrandObject.id).toBeUndefined();


        });

        it('should initialise catRequest ', () => {
            let param = {
                id: 50
            }

            routeMock = {
                navigate: jest.fn(),
                url: '/catalog/brand/add/50'
            };

            let catRequest = {
                id: '15',
                catActivitySector: ' sector',
                catNature: 'nature',
                idSector: '1',
                idNature: '133',
                label: 'labell',
                comment: 'comment'
            } as CatRequestWaiting
            window.history.pushState({ catRequest: catRequest }, '', '');

            fixture = new MenuCatBrandComponent(
                formBuilderMock,
                routerMock,
                catBrandServiceMock,
                valuationServiceMock,
                popupServiceMock,
                catDiscountServiceMock,
                nodeServiceMock,
                CatDiscountRateServiceMock,
                routeMock,
                catNatureServiceMock,
                catActivitySectorServiceMock,
                catDocumentServiceMock,
                catRequestServiceMock,
                catalogAddRequestWorkflowServiceMock,
                paramSettingServiceMock,
                dataSharingMock,
                catalogServiceMock
            );
            fixture.initIsAddMode();
            fixture.statusList = [{ id: '22', label: 'validé', value: StatusConstant.STATUS_RQST_WKFL } as ParamSettingModel]

            dataSharingMock.triggerCommunication.next(true);

            popupServiceMock.onConfirmCommunication.next({
                theme: {
                    Specs: [{ action: StatusConstant.STATUS_RQST_WKFL }]
                }
            })

            expect(fixture.catRequest).toEqual(catRequest);


            fixture.initComponent(param);


            forkJoin([
                catActivitySectorServiceMock.getActivitySectorList(),
                catNatureServiceMock.getNatureListByCatActivitySectorID(''),
                catRequestServiceMock.loadCatRequestDocs(''), paramSettingServiceMock.getParamByCode('')]
            ).subscribe(data => {



                expect(fixture.formMenu.value.generalities.brandLabel).toEqual(catRequest.label);
                expect(fixture.formMenu.value.generalities.brandComment).toEqual(catRequest.comment);
            })





        });

    });

    describe('Test:  download doc', () => {
        it('should download doc ', () => {
            let documentToDownload = {
                docName: 'file3.png', id: '1', catActivitySector: null, catNature: null, catBrand: null,
                catAssetType: null, discriminator: 'G', docVersion: 1, gedPath: '17/file3.png'
            } as CatDocumentModel


            fixture.downloadDocument(documentToDownload);

        });

    });


    describe('Test searchBrandNativeDocs()', () => {

        it('should searchNatureNativeDocs throws error ', () => {
            spyOn(catDocumentServiceMock, 'searchDocument').and.returnValue(throwError({ status: 404 }));
            fixture.searchBrandNativeDocs();
        });


    });


    describe('test:partInheritedDocs ', () => {
        it('should part inheritedDocs', () => {
            let files = [{
                docName: 'file3.png', id: '1', catActivitySector: {
                    id: '1',
                    label: 'Transport',
                    comment: 'comment act sector',
                    isActive: true
                }
                , catNature: null, catBrand: null,
                catAssetType: null, discriminator: 'G', docVersion: 1, gedPath: '17/file3.png'
            } as CatDocumentModel,
            {
                docName: 'file4.png', id: '2', catActivitySector: {
                    id: '1',
                    label: 'Transport',
                    comment: 'comment act sector',
                    isActive: true
                }, catNature: null, catBrand: null,
                catAssetType: null, discriminator: 'G', docVersion: 1, gedPath: '17/file4.png'
            } as CatDocumentModel,
            {
                docName: 'file5.png', id: '3', catActivitySector: {
                    id: '1',
                    label: 'Transport',
                    comment: 'comment act sector',
                    isActive: true
                }, catNature: null, catBrand: null,
                catAssetType: null, discriminator: 'G', docVersion: 1, gedPath: '17/file5.png'
            } as CatDocumentModel,
            {
                docName: 'file6.png', id: '4', catActivitySector: {
                    id: '1',
                    label: 'Transport',
                    comment: 'comment act sector',
                    isActive: true
                }, catNature: null, catBrand: null,
                catAssetType: null, discriminator: 'G', docVersion: 1, gedPath: '17/file6.png'
            } as CatDocumentModel
            ]

            fixture.partInheritedDocs(files);
            expect(fixture.inheritedDocsSector).toEqual(files);
        });



    });
    describe('Test:  editDocumentName doc', () => {
        it('should editDocumentName  ', () => {
            fixture.editedDocumentList = [true];
            fixture.updatedBrandDocs = [{
                docName: 'file3.png', id: '1', catActivitySector: null, catNature: null, catBrand: null,
                catAssetType: null, discriminator: 'G', docVersion: 1, gedPath: '17/file3.png'
            } as CatDocumentModel]

            fixture.editDocumentName(0);
            expect(fixture.documentName).toEqual("file3.png");
        });

    });
    describe('Test:  onBlurDocument doc', () => {
        it('should add Document to list  ', () => {

            fixture.documentName = 'fileToAdd';
            fixture.updatedBrandDocsIndex = [];

            fixture.onBlurDocument(0);
            expect(fixture.updatedBrandDocsIndex).toEqual([0]);
        });
        it('should set editedDocumentList   ', () => {
            fixture.editedDocumentList = [true];

            fixture.onBlurDocument(0);
            expect(fixture.editedDocumentList[0]).toEqual(false);
        });
    });
    describe('Test:  updateGeneralitiesDocs doc', () => {
        it('  updateGeneralitiesDocs ', () => {

            fixture.formMenu.controls['generalities'].patchValue({
                files: new FormControl({
                    files: []
                })
            })
            fixture.updatedBrandDocsIndex = [1, 2];
            fixture.updateGeneralitiesDocs();

        });

    });

    describe('Test: onChangeSector', () => {
        it('should update nature liste', () => {
            fixture.formMenu.controls.generalities.patchValue({
                catActivitySector: {
                    id: '55'
                }
            })

            fixture.onChangeSector();

            expect(catNatureServiceMock.getNatureListByCatActivitySectorID).toHaveBeenCalledWith('55');
        })
    });

    describe('Test: onChangeNature', () => {
        it('should update decote', () => {
            fixture.formMenu.controls.generalities.patchValue({
                catNature: {
                    id: '55',
                    label: 'nature label',
                    catActivitySector: {
                        id: '33',
                        label: 'sector'
                    }
                },
                brandLabel: 'brandLabel',
                brandComment: 'brandComment'
            })

            fixture.catRequest = {
                id: '15',
                catActivitySector: ' sector',
                catNature: 'nature',
                idSector: '1',
                idNature: '133',
                label: 'labell',
                comment: 'comment'
            } as CatRequestWaiting


            fixture.onChangeNature();

            expect(catDiscountServiceMock.getDiscountDetailsByTypeIdOrBrandIdOrNatureIdOrSectorId).toHaveBeenCalled();
        })
    });
    describe('Test navigate ', () => {

        it('should call router navigate  ', () => {

          fixture.navigate("215");
          expect(routerMock.navigate.toHaveBeenCalled)


        })
      });
});
