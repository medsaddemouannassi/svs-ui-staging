import { Component, forwardRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, NG_VALIDATORS, NG_VALUE_ACCESSOR, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TreeNode } from 'primeng/api/treenode';
import { forkJoin, Subscription, of } from 'rxjs';
import { CatDiscountModel } from 'src/app/shared/models/cat-discount.model';
import { CatDiscountService } from 'src/app/shared/services/cat-discount.service';
import { ConfirmationModalComponent } from '../../../shared/confirmation-modal/confirmation-modal.component';
import { documentsValidator } from '../../../shared/documents/document-validator';
import { CatActivitySectorModel } from '../../../shared/models/cat-activity-sector.model';
import { CatBrandModel } from '../../../shared/models/cat-brand.model';
import { CatDiscountRateModel } from '../../../shared/models/cat-discount-rate.model';
import { CatDocumentModel } from '../../../shared/models/cat-document.model';
import { CatNatureModel } from '../../../shared/models/cat-nature.model';
import { CatRequestModel } from '../../../shared/models/cat-request.model';
import { CatRequestWorkFlow } from '../../../shared/models/catalog-add-request-workflow.model';
import { CatRequestWaiting } from '../../../shared/models/catalog-add-request.model';
import { CatRequestDoc } from '../../../shared/models/catalog-request-doc.model';
import { FullCatDiscountModel } from '../../../shared/models/full-cat-discount.model';
import { ParamSettingModel } from '../../../shared/models/param-setting.model';
import { Role } from '../../../shared/models/roles.enum';
import { ValuationModel } from '../../../shared/models/valuation.model';
import { PagesConstants } from '../../../shared/pages-constant';
import { CatActivitySectorService } from '../../../shared/services/cat-activity-sector.service';
import { CatBrandService } from '../../../shared/services/cat-brand.service';
import { CatDiscountRateService } from '../../../shared/services/cat-discount-rate.service';
import { CatDocumentService } from '../../../shared/services/cat-document.service';
import { CatNatureService } from '../../../shared/services/cat-nature.service';
import { CatalogAddRequestWorkflowService } from '../../../shared/services/catalog-add-request-workflow.service';
import { CatalogAddRequestService } from '../../../shared/services/catalog-add-request.service';
import { NodeService } from '../../../shared/services/node.service';
import { ParamSettingService } from '../../../shared/services/param-setting.service';
import { PopupService } from '../../../shared/services/popup.service';
import { SharedCommunicationDataService } from '../../../shared/services/shared-communication-data.service';
import { ValuationService } from '../../../shared/services/valuation.service';
import { StatusConstant } from '../../../shared/status-constant';
import { CatalogNewDiscountsComponent } from '../catalog-new-discounts/catalog-new-discounts.component';
import { CatalogOldDiscountsComponent } from '../catalog-old-discounts/catalog-old-discounts.component';
import { catchError } from 'rxjs/operators';
import { CatalogEnergyComponent } from '../catalog-energy/catalog-energy.component';
import { CatalogService } from '../../../shared/services/catalog.service';

@Component({
  selector: 'app-menu-cat-brand',
  templateUrl: './menu-cat-brand.component.html',
  styleUrls: ['./menu-cat-brand.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MenuCatBrandComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => MenuCatBrandComponent),
      multi: true
    }
  ]
})
export class MenuCatBrandComponent implements OnInit, OnDestroy {
  isAddMode = false;
  catRequest: CatRequestWaiting = null;
  modifModeActivatedGeneralite = false;
  modifModeActivatedNew = false;
  modifModeActivatedOld = false;

  brandTitle: string;
  natureTitle: string;
  comment: string;
  activitySectorTitle: string;
  headersList: any[];
  selectedPlan = 0;
  rateNewDiscountFirstYear = -1;
  formMenu: FormGroup;
  loadNewDiscount = false;
  loadOldDiscount = false;
  catBrandObject: CatBrandModel = {} as any;
  catNatureObject: CatNatureModel = {} as any;
  isSubmited = false;
  brandId: string;
  valuationsList: ValuationModel[];
  catDiscountObject: CatDiscountModel = {} as any;
  catOldDiscountObject: CatDiscountModel = {} as any;
  creationUser: string;
  updateUser: string;
  initialCatDiscountObject: CatDiscountModel;
  initialCatOldDiscountObject: CatDiscountModel;
  subscriptions: Subscription[] = [];
  catDiscountRateList: CatDiscountRateModel[];
  catDiscountRateOldList: CatDiscountRateModel[];
  initialCatDiscountRateList: CatDiscountRateModel[];
  initialCatDiscountRateOldList: CatDiscountRateModel[];
  catBrandLoaded = false;
  @ViewChild(CatalogNewDiscountsComponent) catalogNewDiscountsComponent: CatalogNewDiscountsComponent;
  @ViewChild(CatalogOldDiscountsComponent) catalogOldDiscountsComponent: CatalogOldDiscountsComponent;
  @ViewChild(CatalogEnergyComponent) catalogEnergyComponent: CatalogEnergyComponent;

  catRequestDocs: CatRequestDoc[] = [];
  brandDocs: CatDocumentModel[] = [];
  updatedBrandDocs: CatDocumentModel[] = [];
  deletedBrandDocs: CatDocumentModel[] = [];

  updatedBrandDocsIndex: number[] = [];
  editedDocumentList: boolean[] = [];
  documentName: string;

  newDiscountDocs: CatDocumentModel[] = [];
  oldDiscountDocs: CatDocumentModel[] = [];

  inheritedDocsSector: CatDocumentModel[];
  inheritedDocsNature: CatDocumentModel[];

  activitySectorOptions: CatActivitySectorModel[]
  natureOptions: CatNatureModel[];

  statusList: ParamSettingModel[]
  showForm = false;
  communicationSubscription: Subscription;

  //energy
  energyForm: FormGroup;
  modifModeEnergy = false;


  get value() {
    return this.formMenu.value;
  }

  get f() {
    return this.formMenu.controls;
  }

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private catBrandService: CatBrandService,
    private valuationService: ValuationService,
    private popupService: PopupService,
    private catDiscountService: CatDiscountService,
    private nodeService: NodeService,
    private catDiscountRateService: CatDiscountRateService,
    private router: Router,
    private catNatureService: CatNatureService,
    private catActivitySectorService: CatActivitySectorService,
    private catDocumentService: CatDocumentService,
    private catRequestService: CatalogAddRequestService,
    private catalogAddRequestWorkflowService: CatalogAddRequestWorkflowService,
    private paramSettingService: ParamSettingService,
    private dataSharing: SharedCommunicationDataService,
    private catalogService: CatalogService




  ) { }
  ngOnInit(): void {
    this.headersList = [
      'Généralités', 'Décote neuf', 'Décote occasion', 'Ajustement décotes énergie'
    ];
    this.subscriptions.push(this.route.params.subscribe(param => {
      // select generalities Tab whenever component is initalised
      this.loadNewDiscount = false;
      this.loadOldDiscount = false;
      this.catBrandLoaded = false;
      this.isSubmited = false;
      this.selectedPlan = 0;
      this.initIsAddMode();
      this.initComponent(param);
      this.modifModeActivatedGeneralite = false;
      this.modifModeActivatedNew = false;
      this.modifModeActivatedOld = false;
      this.modifModeEnergy = false;
      this.formMenu.updateValueAndValidity();
      this.creationUser ="";
      this.updateUser ="";
    }));
    
  }
  initIsAddMode() {
    let path = this.router.url.split('/')[3];
    if (path == 'add') {
      this.isAddMode = true;

      if (history.state && history.state.catRequest) {
        this.catRequest = history.state.catRequest;
        this.dataSharing.changeStatus("false");
        this.communicationSubscription = this.dataSharing.triggerCommunication.subscribe(() => {
          this.popupService.popupCommunication([Role.ADMIN_SE], PagesConstants.CATALOG_ADD, '', '', [], null, this.catRequest);
          let subscription = this.popupService.onConfirmCommunication.subscribe(data => {
            subscription.unsubscribe();

            let status = this.statusList.find(stat => stat.value == data.theme.Specs[0].action);
            var date = new Date();
            date.setDate(date.getDate() + 1);
            let workFlowRequest = { catRequest: { id: this.catRequest.id, activeDate: date }, status: status, comment: data.comment } as CatRequestWorkFlow;
            //service WF
            this.catalogAddRequestWorkflowService.saveCatRequestWorkflow(workFlowRequest).subscribe(res => {
              //to lock the icon communication
              this.dataSharing.changeStatus("true");
              this.communicationSubscription.unsubscribe();
              //redirection after confirming
              this.router.navigate(['/catalog'])

            });
          })

        })

      }




    }
    else {
      this.isAddMode = false;

    }
  }

  initComponent(param) {
    this.formMenu = this.fb.group({
      generalities: this.newGeneralities(),
      decoteNew: [],
      decoteOld: [],
      decoteAvgNew: [],
      decoteAvgOld: [],
      energyForm: []
    });

    this.showForm = true;
    if (this.isAddMode) {
      this.catBrandObject = {} as CatBrandModel;


      if (this.catRequest) {



        forkJoin([this.catActivitySectorService.getActivitySectorList().pipe(catchError(error => of([]))), this.catNatureService.getNatureListByCatActivitySectorID(this.catRequest.idSector).pipe(catchError(error => of([]))), this.catRequestService.loadCatRequestDocs(this.catRequest.id).pipe(catchError(error => of([]))), this.paramSettingService.getParamByCode(StatusConstant.STATUS_RQST_WKFL).pipe(catchError(error => of([])))]).subscribe((data) => {
          this.activitySectorOptions = data[0];
          this.natureOptions = data[1];

          this.catRequestDocs = data[2];
          this.statusList = data[3];

          if (this.catRequestDocs) {
            this.catRequestDocs.forEach((element, index) => {

              this.editedDocumentList[index] = false;
            });
          }

          this.formMenu.controls['generalities'].patchValue({

            brandLabel: this.catRequest.label,
            brandComment: this.catRequest.comment,
          })




          let nature: CatNatureModel = this.natureOptions.find(n => n.id == this.catRequest.idNature);



          this.initNatureInformation(nature);
          this.editBrand();
          this.initNewDiscountInformation(null);
          this.initOldDiscountInformation(null);
        });

      } else {

        this.catNatureService.getCatNatureById(param.id).subscribe((data) => {
          this.initNatureInformation(data)

        }, (err) => {
          console.error(err);
        }, () => {
          this.editBrand();
          this.initNewDiscountInformation(null)
          this.initOldDiscountInformation(null);

        });

      }
    } else {
      this.brandId = param.id;


      this.catBrandService.getCatBrandById(this.brandId).subscribe(data => {
        this.catBrandObject = data;
        this.catBrandLoaded = true;

      }, (err) => { console.error(err); },
        () => {
          this.brandTitle = this.catBrandObject.label;
          this.natureTitle = this.catBrandObject.catNature.label;
          this.comment = this.catBrandObject.catNature.comment;
          this.activitySectorTitle = this.catBrandObject.catNature.catActivitySector.label;

          this.formMenu.controls['generalities'].patchValue({
            brandLabel: this.catBrandObject.label,
            brandComment: this.catBrandObject.comment,
            natureComment: this.catBrandObject.catNature.comment,
            activitySectorComment: this.catBrandObject.catNature.catActivitySector.comment,
          });

          this.initNewDiscountInformation(this.catBrandObject.id);
          this.initOldDiscountInformation(this.catBrandObject.id)

          //END OF load all discounts in isAdd = false
          this.searchBrandNativeDocs();
          this.searchNewDiscountDocs();
          this.searchOldDiscountDocs();
          this.searchInhertedDocs(this.catBrandObject);

          this.catalogService.fillInfoCreationUser(this.catBrandObject.createdBy).subscribe(res => {
            this.creationUser = res.collaboratorFullName;
          })

          this.catalogService.fillInfoCreationUser(this.catBrandObject.updatedBy).subscribe(res => {
            this.updateUser = res.collaboratorFullName;
          })
        });

      this.valuationService.getLast10ValuationsByBrandId(this.brandId).subscribe(data => {
        this.valuationsList = data;
      });



    }
  }

  private initNatureInformation(data: CatNatureModel) {


    this.catNatureObject = data;
    if (!this.catRequest) {
      this.natureOptions = [this.catNatureObject];
      this.activitySectorOptions = [this.catNatureObject.catActivitySector];
    }

    this.natureTitle = this.catNatureObject.label;
    this.activitySectorTitle = this.catNatureObject.catActivitySector.label;
    this.catBrandObject.catNature = this.catNatureObject;
    this.formMenu.controls['generalities'].patchValue({
      natureComment: this.catNatureObject.comment,
      activitySectorComment: this.catNatureObject.catActivitySector.comment,
      catNature: this.catNatureObject,
      catActivitySector: this.catNatureObject.catActivitySector,
    });
    this.searchInhertedDocs(this.catBrandObject);
  }

  private initNewDiscountInformation(brandId) {


    //load new discount
    this.catDiscountService.
      getDiscountDetailsByTypeIdOrBrandIdOrNatureIdOrSectorId(null, brandId, this.catBrandObject.catNature.id, this.catBrandObject.catNature.catActivitySector.id, true)
      .subscribe((data) => {

        this.catDiscountObject = { ...data };

        this.initialCatDiscountObject = { ...data };

        this.initAvgDecoteNewValue()

      }, (err) => { console.error(err); },
        () => {
          this.formMenu.controls['generalities'].patchValue({
            dureeMax: this.catDiscountObject.maxFinPer,
            dureeMin: this.catDiscountObject.minFinPer,
            seuilVr: this.catDiscountObject.discount,
            creation: this.catDiscountObject.creation,
          });
          this.loadDiscountRateListByCatDiscountId(this.catDiscountObject.id);

        });
  }

  private initOldDiscountInformation(brandId) {

    this.catDiscountService
      .getDiscountDetailsByTypeIdOrBrandIdOrNatureIdOrSectorId(null, brandId, this.catBrandObject.catNature.id, this.catBrandObject.catNature.catActivitySector.id, false)
      .subscribe((data) => {
        this.catOldDiscountObject = { ...data };
        this.initialCatOldDiscountObject = { ...data }
        this.initAvgDecoteOldValue();
      }, (err) => { console.error(err); }, () => {
        this.loadDiscountRateOldListByCatDiscountId(this.catOldDiscountObject.id);

      });
  }


  loadDiscountRateListByCatDiscountId(id) {


    this.catDiscountRateService.getCatDiscountRateByCatDiscountId(id).subscribe((data) => {

      this.catDiscountRateList = data.map(a => {
        if (this.catDiscountObject.catBrand == null) { a.id = null } return { ...a }
      });


      this.initialCatDiscountRateList = data.map(a => { if (this.catDiscountObject.catBrand == null) { a.id = null } return { ...a } });

    }, (err) => {
      console.error(err);
    }, () => {
      if (this.isAddMode || this.catDiscountObject.catBrand == null) {
        this.inheritBrandUncheckIsAdjusted();
      }
      this.loadNewDiscount = true;


    });
  }

  loadDiscountRateOldListByCatDiscountId(id) {

    this.catDiscountRateService.getCatDiscountRateByCatDiscountId(id).subscribe((data) => {
      this.catDiscountRateOldList = data.map(a => { if (this.catOldDiscountObject.catBrand == null) { a.id = null } return { ...a } });
      this.initialCatDiscountRateOldList = data.map(a => { if (this.catOldDiscountObject.catBrand == null) { a.id = null } return { ...a } });
    }, (err) => {
      console.error(err);
    }, () => {

      if (this.isAddMode || this.catOldDiscountObject.catBrand == null) {
        this.inheritBrandUncheckIsAdjusted();
      }
      this.loadOldDiscount = true;

    });
  }

  inheritBrandUncheckIsAdjusted() {
    this.catDiscountObject.isReadjust = false;
    this.catOldDiscountObject.isReadjust = false;
  }

  changeTab(i: number) {
    if (this.selectedPlan == 0) {
      this.formMenu.get('generalities').markAllAsTouched()
    }
    if (!(i != 0 && this.formMenu.controls.generalities.get('catNature').invalid) || !this.isAddMode) {
      this.selectedPlan = i;

    }
  }
  resetCatSector() {

    this.formMenu.get('generalities').get('catNature').patchValue(null)
    this.formMenu.get('generalities').get('catNature').disable();

    this.resetFileAndComment();
  }
  resetCatNature() {
    this.resetFileAndComment();

  }
  resetFileAndComment() {
    this.catBrandObject.catNature = null
    this.formMenu.get('generalities').get('natureComment').patchValue("")
    this.formMenu.get('generalities').get('activitySectorComment').patchValue("")

    this.inheritedDocsNature = null;
    this.inheritedDocsSector = null;
  }

  private newGeneralities(): FormGroup {

    return this.fb.group({
      brandLabel: [{ value: '', disabled: true }, Validators.required],
      brandComment: [{ value: '', disabled: true }],
      natureComment: [{ value: '', disabled: true }],
      activitySectorComment: [{ value: '', disabled: true }],
      dureeMin: [{ value: '', disabled: true }],
      dureeMax: [{ value: null, disabled: true }],
      creation: [{ value: null, disabled: true }, Validators.required],
      seuilVr: [{ value: null, disabled: true }],
      catNature: [{ value: null, disabled: !this.catRequest }, Validators.required],
      catActivitySector: [{ value: null, disabled: !this.catRequest }, Validators.required],
      files: [{ value: null, disabled: true }, { validators: [documentsValidator] }],
    });

  }

  editBrand() {
    this.modifModeActivatedGeneralite = true;
    this.modifModeActivatedNew = true;
    this.modifModeActivatedOld = true;
    this.modifModeEnergy = true;

    this.formMenu.controls['generalities'].get('brandLabel').enable();
    this.formMenu.controls['generalities'].get('brandComment').enable();
    this.formMenu.controls['generalities'].get('dureeMin').enable();
    this.formMenu.controls['generalities'].get('dureeMax').enable();
    this.formMenu.controls['generalities'].get('seuilVr').enable();
    this.formMenu.controls['generalities'].get('files').enable();
    this.formMenu.controls['decoteAvgNew'].enable();
    this.formMenu.controls['decoteAvgOld'].enable();
    this.formMenu.controls['energyForm'].enable();

  }
  generateCatDiscount(decoteForm: any, decoteAvgForm: any) {
    if (decoteForm) {
      let catDiscount: CatDiscountModel = decoteForm as CatDiscountModel;
      if (catDiscount && catDiscount.isReadjust == null) {
        catDiscount.isReadjust = false;
      }
      if (catDiscount && catDiscount.isPerm == null) {
        catDiscount.isReadjust = false;
      }

      this.catDiscountObject.isReadjust = decoteForm.isReadjust;
      this.catDiscountObject.isPerm = decoteForm.isPerm;
      this.catDiscountObject.comment = decoteForm.comment;
      if (decoteForm.endDate != null && decoteForm.endDate != undefined && typeof decoteForm.endDate === 'string') {
        let splitedDate = decoteForm.endDate.split("/");
        if (splitedDate != null && splitedDate != undefined && splitedDate.length > 0) {
          this.catDiscountObject.endDate = decoteForm.endDate.split("/").reverse().join("-");

        }
      }
      else {
        this.catDiscountObject.endDate = new Date('9999-12-31');

      }


    }


    this.catDiscountObject.minFinPer = this.formMenu.controls['generalities'].get('dureeMin').value;
    this.catDiscountObject.maxFinPer = this.formMenu.controls['generalities'].get('dureeMax').value;
    this.catDiscountObject.discount = this.formMenu.controls['generalities'].get('seuilVr').value;
    this.catDiscountObject.creation = this.formMenu.controls['generalities'].get('creation').value;

    this.catDiscountObject.startDateAverLabel = decoteAvgForm.startDate;
    this.catDiscountObject.endDateAverLabel = decoteAvgForm.endDate;
    this.catDiscountObject.nbMinValAver = decoteAvgForm.nbMinToConsider;
  }

  generateCatOldDiscount(decoteForm: any, decoteAvgOldForm: any) {


    this.catOldDiscountObject.ponderationRate = (decoteForm.ponderationRate != null && decoteForm.ponderationRate != undefined) ? decoteForm.ponderationRate : 50;
    this.catOldDiscountObject.maxFinPer = (decoteForm.maxFinPer != null && decoteForm.maxFinPer != undefined) ? decoteForm.maxFinPer : ((this.catDiscountObject.maxFinPer != null && this.catDiscountObject.maxFinPer != undefined) ? this.catDiscountObject.maxFinPer : 72);
    this.catOldDiscountObject.isReadjust = decoteForm.isReadjust;
    this.catOldDiscountObject.isPerm = decoteForm.isPerm;
    this.catOldDiscountObject.comment = decoteForm.comment;
    if (decoteForm.endDate != null && decoteForm.endDate != undefined && typeof decoteForm.endDate === 'string') {
      let splitedDate = decoteForm.endDate.split("/");
      if (splitedDate != null && splitedDate != undefined && splitedDate.length > 0) {
        this.catOldDiscountObject.endDate = decoteForm.endDate.split("/").reverse().join("-");

      }
    }
    else {
      this.catOldDiscountObject.endDate = new Date('9999-12-31');

    }

    this.catOldDiscountObject.startDateAverLabel = decoteAvgOldForm.startDate;
    this.catOldDiscountObject.endDateAverLabel = decoteAvgOldForm.endDate;
    this.catOldDiscountObject.nbMinValAver = decoteAvgOldForm.nbMinToConsider;


  }

  markAsTouched() {
    this.formMenu.controls['generalities'].get('brandLabel').markAsTouched();

  }
  editBrandToAdd() {
    if (this.isAddMode) {
      this.editBrand();
      this.modifModeEnergy = true;
    }
  }
  saveBrand() {
    this.isSubmited = true;
    this.markAsTouched();
    this.formMenu.markAllAsTouched();



    if (this.formMenu.valid && this.catalogEnergyComponent.mainForm.valid) {
      this.popupService.popupInfo(
        'Êtes-vous certain de vouloir enregistrer votre saisie ?', 'OUI', null, 'NON', ConfirmationModalComponent);
      const subscription = this.popupService.onConfirm.subscribe((t) => {
        // OUI
        if (t) {
          this.modifModeEnergy = false;
          this.modifModeActivatedGeneralite = false;
          this.formMenu.controls['generalities'].disable();
          this.updateCatBrandObject();
          //
          this.catBrandService.saveCatBrand(this.catBrandObject).subscribe(data => {
            this.catBrandObject = data;
            let node: any = {};
            node.data = this.catBrandObject.id;
            node.label = this.catBrandObject.label;
            node.type = 'brand';
            node.nodeType = 'brand';
            node.parent = {} as TreeNode;
            node.parent.data = this.catBrandObject.catNature.id;
            node.parent.parent = {} as TreeNode;
            node.parent.parent.data = this.catBrandObject.catNature.catActivitySector.id;

            node.icon = 'pi pi-folder'
            if (this.isAddMode) {

              if (this.catRequest) {
                let status = this.statusList.find(stat => stat.value == StatusConstant.STATUS_RQST_WKFL_VAL);
                var date = new Date();
                date.setDate(date.getDate());


                let workFlowRequest = { catRequest: { id: this.catRequest.id, activeDate: date }, status: status, updateDate: new Date() } as CatRequestWorkFlow;

                this.catalogAddRequestWorkflowService.saveCatRequestWorkflow(workFlowRequest).subscribe(res => {

                  this.catRequest = null;
                  this.catRequestDocs = [];

                });


              }
              this.nodeService.nodeAddSubject.next(node);

            } else {
              this.nodeService.nodeSubject.next(node);
            }


          }, (err) => {
            console.error(err);
          }, () => {
            this.updateGeneralitiesDocs();
            this.setGenralitiesFormValue();
            let fullCatDiscount: FullCatDiscountModel = {} as FullCatDiscountModel;
            this.generateCatDiscount(this.formMenu.getRawValue().decoteNew, this.formMenu.getRawValue().decoteAvgNew);
            if (this.isAddMode || this.catDiscountObject.catBrand == null) {
              this.catDiscountObject.id = null;
              this.catDiscountObject.catBrand = this.catBrandObject;
              this.catDiscountObject.catNature = this.catBrandObject.catNature;
              this.catDiscountObject.catActivitySector = this.catBrandObject.catNature.catActivitySector;
            }
            fullCatDiscount.catDiscount = this.catDiscountObject;
            fullCatDiscount.catDiscountRateList = this.formMenu.getRawValue().decoteNew.catDiscountRateList;
            this.catDiscountRateList = fullCatDiscount.catDiscountRateList;
            this.initialCatDiscountObject.endDate = this.catDiscountObject.endDate
            if (JSON.stringify(this.catDiscountObject) !== JSON.stringify(this.initialCatDiscountObject) || (!this.arraysEqual(this.catDiscountRateList, this.initialCatDiscountRateList)) || this.isAddMode) {

              this.catDiscountService.saveCatDiscountAndRate(fullCatDiscount, 'brand', true).subscribe(data => {
                Object.assign(this.catDiscountObject, data.catDiscount);
                Object.assign(this.initialCatDiscountObject, data.catDiscount);
                this.catDiscountRateList = data.catDiscountRateList.map(a => { return { ...a } });
                this.initialCatDiscountRateList = data.catDiscountRateList.map(a => { return { ...a } });
              }, (err) => {
                console.error(err);
              }, () => {
                this.catalogNewDiscountsComponent.updateNewDiscountDocs();
                this.modifModeActivatedNew = false;

              });
            } else {
              this.modifModeActivatedNew = false;

            }

            let fullOldCatDiscount: FullCatDiscountModel = {} as FullCatDiscountModel;
            this.generateCatOldDiscount(this.formMenu.getRawValue().decoteOld, this.formMenu.getRawValue().decoteAvgOld);
            if (this.isAddMode || this.catOldDiscountObject.catBrand == null) {
              this.catOldDiscountObject.id = null;
              this.catOldDiscountObject.catBrand = this.catBrandObject;
              this.catOldDiscountObject.catNature = this.catBrandObject.catNature;
              this.catOldDiscountObject.catActivitySector = this.catBrandObject.catNature.catActivitySector;

            }
            fullOldCatDiscount.catDiscount = this.catOldDiscountObject;
            fullOldCatDiscount.catDiscountRateList = this.formMenu.getRawValue().decoteOld.catDiscountRateOldList;
            this.catDiscountRateOldList = fullOldCatDiscount.catDiscountRateList;
            this.initialCatOldDiscountObject.endDate = this.catOldDiscountObject.endDate
            if (JSON.stringify(this.initialCatOldDiscountObject) !== JSON.stringify(this.catOldDiscountObject) || (!this.arraysEqual(this.catDiscountRateOldList, this.initialCatDiscountRateOldList)) || this.isAddMode) {

              this.catDiscountService.saveCatDiscountAndRate(fullOldCatDiscount, 'brand', false).subscribe(data => {
                Object.assign(this.catOldDiscountObject, data.catDiscount);
                Object.assign(this.initialCatOldDiscountObject, data.catDiscount);
                this.catDiscountRateOldList = data.catDiscountRateList.map(a => { return { ...a } });
                this.initialCatDiscountRateOldList = data.catDiscountRateList.map(a => { return { ...a } });


              }, (err) => {
                console.error(err);
              }, () => {
                this.modifModeActivatedOld = false;

                this.catalogOldDiscountsComponent.updateOldDiscountDocs(this.isAddMode, this.catBrandObject.id, 'brand');


              });

            }
            else {
              this.modifModeActivatedOld = false;

            }


            this.formMenu.controls['decoteAvgNew'].disable();
            this.formMenu.controls['decoteAvgOld'].disable();
            this.formMenu.controls['energyForm'].disable();
            this.lockEnergyComponent();
          });


        }
        this.isSubmited = false;

        subscription.unsubscribe();
      });
    }
    else {
      this.popupService.popupInfo(
        "Merci de bien vouloir renseigner l'intégralité des informations demandées.", null, null, null, ConfirmationModalComponent);
      const subscriptionPopup = this.popupService.onConfirm.subscribe((t) => {

        subscriptionPopup.unsubscribe();

      });
    }



  }


  undo() {
    this.popupService.popupInfo(
      'Êtes-vous certain de vouloir annuler votre saisie ?', 'OUI', null, 'NON', ConfirmationModalComponent);
    const subscription = this.popupService.onConfirm.subscribe((t) => {
      // OUI
      if (t) {
        if (this.isAddMode) {
          if (this.catRequest) {
            this.router.navigate(['catalog']);

          } else {
            this.router.navigate(['catalog', 'nature', this.catNatureObject.id]);

          }

        }
        else {
          this.modifModeActivatedGeneralite = false;
          this.modifModeActivatedNew = false;
          this.modifModeActivatedOld = false;

          this.formMenu.controls['generalities'].disable();

          this.formMenu.controls['decoteAvgNew'].disable();
          this.formMenu.controls['decoteAvgOld'].disable();
          this.formMenu.controls['energyForm'].disable();

          //load brand object
          this.loadBrandInfo();

          // reset new discount component
          this.catalogNewDiscountsComponent.undo();
          //reset old discount component
          this.catalogOldDiscountsComponent.undo();

          this.initAvgDecoteOldValue();
          this.initAvgDecoteNewValue();

          //reset energy component
          this.catalogEnergyComponent.undo();

          //reset form 
          this.isSubmited = false;
          //reset plan financier table
          this.formMenu.controls['generalities'].patchValue({
            dureeMax: this.catDiscountObject.maxFinPer,
            dureeMin: this.catDiscountObject.minFinPer,
            seuilVr: this.catDiscountObject.discount,
            creation: this.catDiscountObject.creation,
            files: { files: [] }
          });

          if (this.catDiscountRateList && this.catDiscountRateList.length) {
            this.rateNewDiscountFirstYear = this.catDiscountRateList[0].discountRateValue;
          }
          //reset files variables
          this.updatedBrandDocs = [];
          this.updatedBrandDocsIndex = [];
          this.deletedBrandDocs = [];

          if (this.brandDocs) {
            this.brandDocs.forEach((element, index) => {

              this.updatedBrandDocs.push(Object.assign({}, element))
              this.editedDocumentList[index] = false;
            });
          }
        }
        this.modifModeEnergy = false;
        //reset errors
        this.isSubmited = false;


      }

      subscription.unsubscribe();
    });
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
    if (this.communicationSubscription && !this.communicationSubscription.closed) {
      this.communicationSubscription.unsubscribe();

    }
  }

  updateCatBrandObject() {
    this.catBrandObject.label = this.formMenu.controls['generalities'].get('brandLabel').value;
    this.catBrandObject.comment = this.formMenu.controls['generalities'].get('brandComment').value;
    this.catBrandObject.isActive = true;
    if (this.isAddMode && this.catRequest) {
      this.catBrandObject.catRequest = { id: this.catRequest.id } as CatRequestModel
    }

  }


  initAvgDecoteOldValue() {
    this.formMenu.controls['decoteAvgOld'].patchValue({
      startDate: this.initialCatOldDiscountObject.startDateAverLabel,
      endDate: this.initialCatOldDiscountObject.endDateAverLabel,
      nbMinToConsider: this.initialCatOldDiscountObject.nbMinValAver ? this.initialCatOldDiscountObject.nbMinValAver : 3
    });
  }

  initAvgDecoteNewValue() {
    this.formMenu.controls['decoteAvgNew'].patchValue({

      startDate: this.initialCatDiscountObject.startDateAverLabel,
      endDate: this.initialCatDiscountObject.endDateAverLabel,
      nbMinToConsider: this.initialCatDiscountObject.nbMinValAver ? this.initialCatDiscountObject.nbMinValAver : 3
    });
  }

  setGenralitiesFormValue() {
    this.formMenu.controls['generalities'].patchValue({
      brandLabel: this.catBrandObject.label,
      brandComment: this.catBrandObject.comment,
      natureComment: this.catBrandObject.catNature.comment,
      activitySectorComment: this.catBrandObject.catNature.catActivitySector.comment,
    });
    this.brandTitle = this.catBrandObject.label;
    this.natureTitle = this.catBrandObject.catNature.label;
  }

  changeNewDiscount(rateNewDiscountFirstYear) {
    this.rateNewDiscountFirstYear = rateNewDiscountFirstYear;
  }

  arraysEqual(arr1, arr2) {
    let isEqual = true;
    if (arr1.length != arr2.length) {
      return false;
    }
    arr1.forEach((element, index) => {
      if ((JSON.stringify(element) !== JSON.stringify(arr2[index]))) {
        isEqual = false;
        return;
      }

    });
    return isEqual;
  }

  loadBrandInfo() {
    this.catBrandService.getCatBrandById(this.brandId).subscribe(data => this.catBrandObject = data, (err) => {
      console.error(err);
    },
      () => {
        this.brandTitle = this.catBrandObject.label;
        this.natureTitle = this.catBrandObject.catNature.label;
        this.comment = this.catBrandObject.catNature.comment;
        this.activitySectorTitle = this.catBrandObject.catNature.catActivitySector.label;

        this.setGenralitiesFormValue();

      });
    this.valuationService.getLast10ValuationsByBrandId(this.brandId).subscribe(data => {
      this.valuationsList = data;
    });
  }

  deleteBrand() {
    if (new Date(this.catBrandObject.createdDate).setHours(0, 0, 0, 0) == new Date().setHours(0, 0, 0, 0)) {
      this.popupService.popupInfo(
        'Voulez-vous supprimer cet élément ? Si vous confirmez, l\'élément sera supprimé et les types correspondants seront également supprimés ?', 'OUI', null, 'NON', ConfirmationModalComponent);

    } else {
      this.popupService.popupInfo(
        'Voulez-vous désactiver cet élément ? Si vous confirmez, l\'élément sera conservé dans le catalogue mais ne sera plus visible et les types correspondants seront également désactivés ?', 'OUI', null, 'NON', ConfirmationModalComponent);

    }
    const subscription = this.popupService.onConfirm.subscribe((t) => {
      // OUI
      if (t) {
        this.catBrandService.deleteCatBrandById(this.brandId).subscribe(data => {
          // This is intentional
        },
          (err) => { console.error(err); },
          () => {
            let node = {} as TreeNode;
            node.data = this.catBrandObject.id;
            node.label = this.catBrandObject.label;
            node.type = 'brand';

            node.parent = {} as TreeNode;
            node.parent.data = this.catBrandObject.catNature.id;
            node.parent.parent = {} as TreeNode;
            node.parent.parent.data = this.catBrandObject.catNature.catActivitySector.id;

            this.nodeService.nodeDeleteSubject.next(node);

            this.router.navigate(['catalog', 'nature', this.catBrandObject.catNature.id]);
          });

      }

      subscription.unsubscribe();
    });
  }


  deleteDocument(index: number) {
    this.popupService.popupInfo('Êtes-vous sûr(e) de vouloir supprimer la pièce jointe ?', 'Confirmer', null, 'Annuler', ConfirmationModalComponent);

    const subscription = this.popupService.onConfirm.subscribe((t) => {

      if (t) {
        if (this.catRequest) {

          this.editedDocumentList.splice(index, 1);
          this.catRequestDocs.splice(index, 1);

        } else {
          this.deletedBrandDocs.push(this.updatedBrandDocs[index])

          this.editedDocumentList.splice(index, 1);
          this.updatedBrandDocs.splice(index, 1);
          let indexIndex = this.updatedBrandDocsIndex.findIndex(val => val == index);
          if (indexIndex > 0) {
            this.updatedBrandDocsIndex.splice(indexIndex, 1);
          }
        }
      }
      subscription.unsubscribe();

    });
  }
  editDocumentName(index: number) {
    this.editedDocumentList[index] = true;
    if (this.catRequest) {
      this.documentName = this.catRequestDocs[index].docName;

    } else {
      this.documentName = this.updatedBrandDocs[index].docName;
    }
  }
  onBlurDocument(index: number) {
    if (this.catRequest) {
      this.catRequestDocs[index].docName = this.documentName;


    } else {
      this.updatedBrandDocs[index].docName = this.documentName;
      if (this.updatedBrandDocsIndex.findIndex(val => val == index) == -1) {
        this.updatedBrandDocsIndex.push(index);
      }
    }
    this.editedDocumentList[index] = false;

  }

  //save delete update docs
  updateGeneralitiesDocs() {
    if (this.formMenu.controls['generalities'].get('files').value) {


      let files: File[] = this.formMenu.controls['generalities'].get('files').value.files;

      let request = [];

      if (files && files.length > 0) {
        files.forEach(file => request.push(this.catDocumentService.uploadDocument(file, this.catBrandObject.catNature.catActivitySector.id,
          this.catBrandObject.catNature.id, this.catBrandObject.id, null, 'G').pipe(catchError(error => of(null)))));
      }

      if (this.updatedBrandDocsIndex.length > 0) {
        let docs = []
        this.updatedBrandDocsIndex.forEach(index => docs.push(this.updatedBrandDocs[index]))
        docs.forEach(doc => request.push(this.catDocumentService.updateDocument(doc).pipe(catchError(error => of(null)))));
        /*
        request.push(this.catActivitySectorService.updateActivitySectorDocument(docs))
        */
      }
      if (this.deletedBrandDocs.length > 0) {

        this.deletedBrandDocs.forEach(docs => request.push(this.catDocumentService.deleteCatDocument(docs).pipe(catchError(error => of(null)))))
      }

      if (this.catRequest) {
        if (this.catRequestDocs && this.catRequestDocs.length > 0) {
          this.catRequestDocs.forEach(res => {

            request.push(this.catDocumentService.saveDocumentRequest(this.catBrandObject.catNature.catActivitySector.id,
              this.catBrandObject.catNature.id, this.catBrandObject.id, null, 'G', res.gedPath, res.docName).pipe(catchError(error => of(null))))
          }
          )

        }
      }

      forkJoin(request).subscribe(res => {
        //this is intentional

      }, () => {
        //this is intentional

      }, () => {
        this.formMenu.controls['generalities'].get('files').reset();
        this.searchBrandNativeDocs();
      });
    }
  }
  searchBrandNativeDocs() {
    this.catDocumentService.searchDocument(this.catBrandObject.catNature.catActivitySector.id, this.catBrandObject.catNature.id, this.brandId, null, 'G').subscribe(res => {
      this.brandDocs = res;
    }, err => {
      console.error(err);
    }, () => {

      this.updatedBrandDocs = [];
      this.updatedBrandDocsIndex = [];
      this.deletedBrandDocs = [];

      this.brandDocs.forEach((element, index) => {

        this.updatedBrandDocs.push(Object.assign({}, element))
        this.editedDocumentList[index] = false;
      });
    });
  }


  searchNewDiscountDocs() {
    this.catDocumentService.searchDocument(this.catBrandObject.catNature.catActivitySector.id, this.catBrandObject.catNature.id, this.brandId, null, 'N').subscribe(res => {
      this.newDiscountDocs = res;
    })
  }

  searchOldDiscountDocs() {
    this.catDocumentService.searchDocument(this.catBrandObject.catNature.catActivitySector.id, this.catBrandObject.catNature.id, this.brandId, null, 'O').subscribe(res => {
      this.oldDiscountDocs = res;
    })
  }
  searchInhertedDocs(catBrand: CatBrandModel) {
    this.catDocumentService.searchInheratedDocuments(catBrand.catNature.catActivitySector.id,
      catBrand.catNature.id, null, 'G').subscribe(data => {

        this.partInheritedDocs(data);
      });
  }
  downloadDocument(documentToDownload: CatDocumentModel) {
    this.catDocumentService.downloadDocument(documentToDownload.gedPath, documentToDownload.docName).subscribe(response => {
      var file = new Blob([response], { type: 'application/*' })
      var fileURL = URL.createObjectURL(file);
      const doc = document.createElement('a');
      doc.download = documentToDownload.docName;
      doc.href = fileURL;
      doc.dataset.downloadurl = [doc.download, doc.href].join(':');
      document.body.appendChild(doc);
      doc.click();
    });
  }

  partInheritedDocs(inheritedDocs: CatDocumentModel[]) {
    if (inheritedDocs) {
      this.inheritedDocsNature = inheritedDocs.filter(doc => doc.catNature != null && doc.catBrand == null);
      this.inheritedDocsSector = inheritedDocs.filter(doc => doc.catNature == null && doc.catBrand == null);
    }


  }

  onChangeSector() {
    this.natureOptions = [];
    this.formMenu.controls['generalities'].patchValue({ catNature: null });
    this.formMenu.controls['generalities'].get('catNature').markAsTouched();
    this.formMenu.controls['generalities'].get('catActivitySector').markAsTouched();
    let sector = this.formMenu.getRawValue().generalities.catActivitySector;
    if (sector) {
      this.formMenu.controls['generalities'].get('catNature').enable();
      this.catNatureService.getNatureListByCatActivitySectorID(sector.id).subscribe(data => {
        this.natureOptions = data;
      });
    }
  }

  onChangeNature() {
    let generalities = this.formMenu.getRawValue().generalities;
    let nature = generalities.catNature;

    if (nature) {


      this.catRequest.catNature = nature.label;
      this.catRequest.idNature = nature.id;

      this.catRequest.catActivitySector = nature.catActivitySector.label;
      this.catRequest.idSector = nature.catActivitySector.id;

      this.catRequest.label = generalities.brandLabel;
      this.catRequest.comment = generalities.brandComment;


      this.loadNewDiscount = false;
      this.loadOldDiscount = false;

      this.formMenu.controls['decoteNew'].reset();
      this.formMenu.controls['decoteOld'].reset();

      this.formMenu.controls['decoteAvgNew'].reset();
      this.formMenu.controls['decoteAvgOld'].reset();

      this.initNatureInformation(nature);
      this.initNewDiscountInformation(null);
      this.initOldDiscountInformation(null);



    }
  }

  isFormValid(index) {

    switch (index.toString()) {


      case '0': {
        return this.formMenu.controls.generalities.valid;
      }

      case '1': {
        return this.formMenu.controls.decoteNew.valid;
      }

      case '2': {
        return this.formMenu.controls.decoteOld.valid;
      }

      case '3': {

        return this.catalogEnergyComponent && this.catalogEnergyComponent.mainForm ? this.catalogEnergyComponent.mainForm.valid : null;

      }

      default: {
        return null;
      }

    }


  }
  navigate(idValuation) {
    this.router.navigate(['/result/expert/' + idValuation]);

  }

  lockEnergyComponent() {
    //if add mode, should set the id of the saved catNature
    if (this.isAddMode) {
      (this.catalogEnergyComponent.energyLinesControls).forEach(element => {
        element.patchValue({ catBrand: this.catBrandObject })
      })
    }
    this.catalogEnergyComponent.saveModification();
    //close energies options, it can be opened
    this.catalogEnergyComponent.displayOptionsSelect = false;

  }



}
