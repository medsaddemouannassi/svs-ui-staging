
import { FormBuilder, FormControl } from '@angular/forms';
import { of, Subscription } from 'rxjs';
import { CatDiscountRateModel } from '../../../shared/models/cat-discount-rate.model';
import { CatDiscountModel } from '../../../shared/models/cat-discount.model';
import { CatDocumentModel } from '../../../shared/models/cat-document.model';
import { CatNatureModel } from '../../../shared/models/cat-nature.model';
import { CatalogOldDiscountsComponent } from './catalog-old-discounts.component';

describe('CatalogOldDiscountsComponent', () => {

  let fixture: CatalogOldDiscountsComponent;
  let FormBuilderMock: any;
  let catDiscountRateServiceMock: any;
  let PopupServiceMock: any;
  let CatDiscountServiceMock: any;
  let catDiscountObject: CatDiscountModel = {} as any;
  let catdiscount: CatDiscountModel = {} as any;
  let catdiscountRateList: CatDiscountRateModel[] = []
  let catDocumentServiceMock: any;
  let routerMoock: any;
  let subscriptionMock: Subscription;
  let documentsList: CatDocumentModel[] = [{

    "docName": "document1",
  } as CatDocumentModel, {

    "docName": "document2"

  } as CatDocumentModel];

  beforeEach(() => {
    subscriptionMock = new Subscription();

    catDiscountObject = {
      id: '66',
      maxFinPer: 52,
      minFinPer: 33,
      discount: 25,
      catBrand: null,
      catNature: null,
      catActivitySector: null,
      isPerm: true,
      activationDate: null,
      endDate: null,
      catAssetType: null,
      enableRecov: null,
      enableValuation: null,
      ponderationRate: null,
      creation: '20',
      lastValuationDone: null,
      isReadjust: false,
      comment: 'comment nature',
      isNew: false,
      startDateAverLabel: '',
      endDateAverLabel: '',
      nbMinValAver: 3
    }

    catDiscountObject.catNature = {
      id: '133',
      label: 'nature label',
      catActivitySector: {
        id: '1',
        label: 'Transport',
        comment: 'comment act sector',
        isActive: true
      },
      comment: 'comment nature',
      paramNap: null,
      isActive: true,
      isValid: false,
      isEnrg: false

    }
    catDiscountObject.catBrand = {
      id: '133',
      label: 'nature label',
      catNature: null,
      comment: 'comment nature',
      isActive: true,
      catRequest:null

    }
    catDiscountObject.catActivitySector = {
      id: '1',
      label: 'Transport',
      comment: 'comment act sector',
      isActive: true
    }
    catdiscountRateList = [{
      id: "1",
      catDiscount: catdiscount,
      discountRateYear: 2012,
      discountRateValue: 12,
      updatedDate: null,
      createdDate: null,
      createdBy: null,
      updatedBy: null
    }, {
      id: "2",
      catDiscount: null,
      discountRateYear: 2012,
      discountRateValue: 12,
      updatedDate: null,
      createdDate: null,
      createdBy: null,
      updatedBy: null
    },
    {
      id: "3",
      catDiscount: null,
      discountRateYear: 2012,
      discountRateValue: 20,
      updatedDate: null,
      createdDate: null,
      createdBy: null,
      updatedBy: null
    },
    {
      id: "4",
      catDiscount: null,
      discountRateYear: 2012,
      discountRateValue: 5,
      updatedDate: null,
      createdDate: null,
      createdBy: null,
      updatedBy: null
    }
    ];

    PopupServiceMock = {
      popupInfo: jest.fn().mockReturnValue(of(true)),
      onConfirm: of(true)
    };
    catDiscountRateServiceMock = {
      getCatDiscountRateByCatDiscountId: jest.fn().mockReturnValue(of(catdiscountRateList))
  }
    CatDiscountServiceMock = {
      getDiscountDetailsByIdAndDesc: jest.fn().mockReturnValue(of(catDiscountObject))
    }
    let inheritedDocBrand = [{
      docName: 'file3.png', id: '1', catActivitySector: null, catNature: {
        id: '133',
        label: 'nature label',
        catActivitySector: null
      },
      catBrand: null,
      catAssetType: null,
      discriminator: 'N',
      docVersion: 1,
      gedPath: '17/file3.png'
    } as CatDocumentModel];

    catDocumentServiceMock = {
      uploadDocument: jest.fn().mockReturnValue(of(true)),
      searchInheratedDocuments: jest.fn().mockReturnValue(of(inheritedDocBrand)),
      downloadDocument: jest.fn().mockReturnValue(of(true)),
      updateDocument: jest.fn().mockReturnValue(of(true)),
      deleteCatDocument: jest.fn().mockReturnValue(of(true))

    }
    FormBuilderMock = new FormBuilder();

    fixture = new CatalogOldDiscountsComponent(FormBuilderMock, PopupServiceMock, CatDiscountServiceMock, catDiscountRateServiceMock,
      catDocumentServiceMock, routerMoock);
    fixture.catDiscountRateOldList = catdiscountRateList
    fixture.catDiscountObject = catDiscountObject

    fixture.ngOnInit();
    fixture.createForm()
  });

  describe('Test writeValue', () => {
    it('should  write value', () => {
      let value = {
        maxFinPer: 52,
        isPerm: true,
        endDate: null,
        ponderationRate: null,
        isReadjust: false,
        comment: 'comment nature',
        temporary: null,
        startingYear1: null,
        startingNumber: null,
        percentage: 50,
        startingYear2: null,
        startingNumber2: null,
        commentHerit: null,
        files: null,


      };
      fixture.writeValue(value);

      expect(fixture.value).toBeDefined();
    });
  });

  describe('Test cat-old-discount', () => {
    it('should Be Truthy', () => {
      expect(fixture).toBeTruthy();
    })
  });

  describe('Test catDiscountRateOld', () => {
    it('should  call lockForm', () => {
      fixture.catDiscountObject.id=null
      fixture.catDiscountRateOld=catdiscountRateList
      expect(fixture.editingMode).toBeFalsy()

    })

    it('should  call lockForm', () => {
      fixture.isUnlocked=true
      fixture.catDiscountObject.id=null
      fixture.catDiscountRateOld=catdiscountRateList
      expect(fixture.editingMode).toBeTruthy()

    })
  });
  describe('Test: createForm', () => {
    it('should create form ', () => {
      fixture.createForm();
      expect(fixture.catOldDiscForm).toBeTruthy();
    });
  });

  describe('Test: initComponent', () => {
    it('should   call initForm  ', () => {
      fixture.initComponent();
      fixture.isUnlocked = true;

      expect(fixture.initForm()).toHaveBeenCalled;
    });
    it('should  unlock form ', () => {
      fixture.initComponent();

      fixture.isUnlocked = true;
      expect(fixture.unlockForm()).toHaveBeenCalled;
    });
    it('should  lock form ', () => {
      fixture.initComponent();

      fixture.isUnlocked = false;
      expect(fixture.lockForm()).toHaveBeenCalled;
    });
  });
  describe('Test: onBlurPercentage', () => {
    it('should   update set percet to null if value > 100 ', () => {
      fixture.createForm();
      fixture.catOldDiscForm.patchValue({ percentage: 120 });
      fixture.onBlurPercentage();
      expect(fixture.catOldDiscForm.controls['percentage'].value).toEqual(null)
    });
  });

  describe('Test: initDiscounts', () => {
    it('should   fill catDiscountRateInitialList ', () => {

      fixture.initDiscounts();
      expect(fixture.catDiscountRateInitialList).toBeDefined();
    });

    it('  should initDiscountLock called  ', () => {
      fixture.initDiscounts();
      expect(fixture.initDiscountLock).toHaveBeenCalled
    });

  });

  describe('Test: isAdmin', () => {

    it('should isAdmin false ', () => {

      expect(fixture.isAdmin()).toEqual(false);
    });



  });
  describe("test :   lockForm()", () => {
    it('should update editing mode to true ', () => {
      fixture.lockForm();
      expect(fixture.editingMode).toEqual(false);
    });

  });

  describe("test :   inlockForm()", () => {

    it('should  update editing mode to true ', () => {

      fixture.unlockForm();
      expect(fixture.editingMode).toEqual(true);
    });
    it('should  disable endDate ', () => {
      fixture.catDiscountObject.isPerm = true;
      fixture.unlockForm();
    });
  });

  describe('Test: undo', () => {
    it('should   set value ', () => {
      fixture.createForm();
      fixture.undo();
      expect(fixture.catDiscountObject.comment).toEqual(fixture.catOldDiscForm.get('comment').value);

    });
    it('if node type brand should set commentHerit value ', () => {
      fixture.nodeType = 'brand';
      fixture.createForm();
      fixture.undo();
      expect(CatDiscountServiceMock.getDiscountDetailsByIdAndDesc).toHaveBeenCalled();


    });
  });
  describe("test :   initForm()", () => {

    it('should  update comment herit to null  ', () => {
      fixture.nodeType = "brand";
      fixture.initForm();
      expect(CatDiscountServiceMock.getDiscountDetailsByIdAndDesc).toHaveBeenCalled;
    });

  });
  describe("test :   readjust()", () => {

    it('should  show pop up   ', () => {
      fixture.readjust(true);
      expect(PopupServiceMock.popupInfo).toHaveBeenCalled;
    });

    it('should  call disableTauxfield   ', () => {
      fixture.readjust(false);
      expect(fixture.disableTauxFields).toHaveBeenCalled;
    });

    it('should  call disableTauxfield   ', () => {
      fixture.readjust(true);
      expect(fixture.enableTauxFields).toHaveBeenCalled;
    });
  });
  describe("test :   tempOrPerm()", () => {
    it('if prem is true set prem to true    ', () => {
      fixture.catOldDiscForm.patchValue({ temporary: true });
      fixture.tempOrPerm();
      expect(fixture.catOldDiscForm.controls['isPerm'].value).toEqual(true);
    });
    it('if !prem set prem to true    ', () => {
      fixture.catDiscountObject.isPerm = false;
      fixture.tempOrPerm();
      expect(fixture.catOldDiscForm.controls['isPerm'].value).toEqual(false);
    });
  });

  describe("test :   sinceYear()", () => {

    it(' should call initDiscountLock and update starting number 2', () => {
      fixture.sinceYear();
      expect(fixture.catOldDiscForm.controls['startingNumber2'].value).toEqual(null);
      expect(fixture.duplicateDiscounts).toEqual(false);
      expect(fixture.initDiscountLock()).toHaveBeenCalled;
    });
    it(' should call initDiscountLock and update starting number 2', () => {
      fixture.catOldDiscForm.patchValue({ startingYear1: true });
      fixture.sinceYear();
      expect(fixture.catOldDiscForm.controls['startingNumber'].value).toEqual(null);
      expect(fixture.initDiscountLock()).toHaveBeenCalled;
    });
  });


  describe("test :   duplicateYear()", () => {

    it(' should call initDiscounLock and update starting number  ', () => {
      fixture.duplicateYear();
      expect(fixture.catOldDiscForm.controls['startingYear1'].value).toEqual(false);
      expect(fixture.catOldDiscForm.controls['percentage'].value).toEqual(null);
      expect(fixture.duplicateDiscounts).toEqual(true);
      expect(fixture.initDiscountLock()).toHaveBeenCalled;
    });

    it(' should call initDiscounLock and update starting number  ', () => {
      fixture.catOldDiscForm.patchValue({startingYear2: true});
      fixture.duplicateYear();
      expect(fixture.initDiscountLock()).toHaveBeenCalled;
    });
  });

  describe("test :   sinceYearCalcul()", () => {
    it('should  call resetDiscount  and initDiscountLock    ', () => {
      fixture.catOldDiscForm.patchValue({ startingNumber: '2', percentage: '10' });
      fixture.sinceYearCalcul();
      expect(fixture.initDiscountLock()).toHaveBeenCalled;
      expect(fixture.resetDiscounts()).toHaveBeenCalled;

    });
    it('Should update catDiscountRateValue     ', () => {
      fixture.catOldDiscForm.patchValue({ startingNumber: '1', percentage: '10' });
      fixture.catOldDiscForm.get('catDiscountRateOldList').enable();
      fixture.sinceYearCalcul();
      expect(fixture.catOldDiscForm.getRawValue().catDiscountRateOldList[1].discountRateValue).toEqual(2);
      expect(fixture.catDiscountRateOldListControls[1].disabled).toEqual(true);

    });

  });
  describe("test :   duplicateCalcul()", () => {
    it('should call  initDiscountLock  and resetDiscounts    ', () => {
      fixture.catOldDiscForm.get('catDiscountRateOldList').enable();

      fixture.catOldDiscForm.patchValue({ startingNumber2: '2', percentage: '10' });
      fixture.duplicateCalcul();
      expect(fixture.initDiscountLock()).toHaveBeenCalled;
      expect(fixture.resetDiscounts()).toHaveBeenCalled;

    });

  });

  describe('test :   initDiscountLock()', () => {
    it('should call  initDiscountLock  and resetDiscounts    ', () => {
      fixture.catOldDiscForm.patchValue({ percentage: 120 });
      fixture.editingMode = true;
      fixture.disableDiscounts = false;
      const spyInstance = jest.spyOn(fixture.catOldDiscForm, 'get');
      fixture.initDiscountLock();

      expect(spyInstance).toHaveBeenCalledWith('catDiscountRateOldList');

    });

  });

  describe("test :   resetForm()", () => {
    it('should update  formChanged to false and call initComponent, lockForm,ResetDiscounts        ', () => {
      fixture.resetForm();
      expect(fixture.initComponent()).toHaveBeenCalled;
      expect(fixture.lockForm()).toHaveBeenCalled;
      expect(fixture.resetDiscounts()).toHaveBeenCalled;

    });
  });
  describe("test: ponderationChange()", () => {
    it('should set discountRateValue  when value < 100      ', () => {
      fixture.catOldDiscForm.patchValue({ ponderationRate: 10 });
      fixture.ponderationChange();
      expect(fixture.calculateNewYearlyRate(10)).toHaveBeenCalled;
    });
    it('should set discountRateValue  when value > 100      ', () => {
      fixture.newDiscountRateValueFirstYear = 100;
      fixture.catOldDiscForm.patchValue({ ponderationRate: 10 });
      fixture.ponderationChange();
      expect(fixture.catOldDiscForm.getRawValue().catDiscountRateOldList[0].discountRateValue).toEqual(100);
    });
  });

  describe('Test:  deleteDocs', () => {
    it('should remove docs ', () => {
      fixture.deleteDocument(1);

      expect(PopupServiceMock.popupInfo).toHaveBeenCalled();
      PopupServiceMock.onConfirm.subscribe((t) => {

        expect(fixture.deletedOlddiscountDocs.length).toEqual(1);
      })



    });

  });

  describe('Test:  download doc', () => {
    it('should download doc ', () => {
      let documentToDownload = {
        docName: 'file3.png', id: '1', catActivitySector: null, catNature: null, catBrand: null,
        catAssetType: null, discriminator: 'O', docVersion: 1, gedPath: '17/file3.png'
      } as CatDocumentModel


      fixture.downloadDocument(documentToDownload);

    });

  });

  describe('Test:  displayPlusBtns', () => {
    it('should return  true ', () => {
      let doc: any;
      fixture.catDiscountObject.id = null;
      expect(fixture.displayPlusBtns(doc)).toBeTruthy();
    });

    it('should return  false  ', () => {
      let catBrand: any;
      let doc = {
        id: '5',
        catBrand: catBrand
      } as CatDocumentModel;
      fixture.catDiscountObject.id = '5'
      expect(fixture.displayPlusBtns(doc)).toBeFalsy();
    });


    it('should return  false  ', () => {
      let catNature: any;
      let doc = {
        id: '5',
        catNature: catNature,

      } as CatDocumentModel;
      fixture.catDiscountObject.catBrand = undefined;
      fixture.catDiscountObject.id = '5'
      expect(fixture.displayPlusBtns(doc)).toBeFalsy();
    });



  });
  describe('Test set docs()', () => {

    it('should set docs ', () => {
      let doc = [{} as CatDocumentModel]
      fixture.docs = doc;
      expect(fixture.oldDiscountDocs).toEqual(doc);

    });
  });
  describe('Test set catDiscount()', () => {

    it('should set catDiscount ', () => {
      let catDiscount = {
        catNature: {
          id: '5', catActivitySector: {
            id: '5', label: 'label'
          }
        }, catActivitySector: {
          id: '5', label: 'label'

        }
      } as CatDiscountModel;
      fixture.catDiscount = catDiscount;
      expect(fixture.catDiscountObject).toEqual(catDiscount);

    });
  });


  describe('Test set value()', () => {

    it('should set value ', () => {
      let value = {
        maxFinPer: 52,
        isPerm: true,
        endDate: null,
        ponderationRate: null,
        isReadjust: false,
        comment: 'comment nature',
        temporary: null,
        startingYear1: null,
        startingNumber: null,
        percentage: 50,
        startingYear2: null,
        startingNumber2: null,
        commentHerit: null,
        files: null,
        catDiscountRateOldList: catdiscountRateList


      };
      fixture.value = value;
      expect(fixture.catOldDiscForm.getRawValue()).toEqual(value)
      expect(fixture.value).toBeDefined()
      expect(fixture.f).toBeDefined()

    });
  });
  describe('Test: initComponent', () => {
    it('should   call initForm  ', () => {
      fixture.isUnlocked = true;
      fixture.initComponent();
      fixture.isUnlock = true;

      expect(fixture.initForm()).toHaveBeenCalled;
    });
    it('should  unlock form ', () => {
      fixture.isUnlocked = true;

      fixture.initComponent();

      fixture.isUnlock = true;
      expect(fixture.unlockForm()).toHaveBeenCalled;
    });
    it('should  lock form ', () => {
      fixture.isUnlocked = true;

      fixture.initComponent();

      fixture.isUnlock = false;
      expect(fixture.lockForm()).toHaveBeenCalled;
    });
  });

  describe('Test: unlockForm', () => {
    it('should  unlock form ', () => {
      fixture.catDiscountObject.isPerm = false;
      const spyInstance = jest.spyOn(fixture.catOldDiscForm, 'get');
      fixture.unlockForm();
      expect(spyInstance).toHaveBeenCalledWith('endDate');
    });
  });

  describe('Test set rateNewDiscountFirstYear()', () => {

    it('should set newDiscountRateValueFirstYear  ', () => {
      fixture.rateNewDiscountFirstYear = 5;
      expect(fixture.newDiscountRateValueFirstYear).toEqual(5)

    });
  });

  describe("test :   readjust()", () => {

    it('should  show pop up   ', () => {
      fixture.catOldDiscForm.markAllAsTouched();
      fixture.readjust(true);
      expect(PopupServiceMock.popupInfo).toHaveBeenCalled();
      const spyInstance = jest.spyOn(fixture, 'readjust');
      fixture.eventCheck(true);
      expect(spyInstance).toHaveBeenCalled();
    });

    it('should  call disableTauxfield', () => {
      fixture.catOldDiscForm.markAllAsTouched();
      const spyInstance = jest.spyOn(fixture, 'disableTauxFields');

      fixture.readjust(false);
      expect(spyInstance).toHaveBeenCalled();
    });

    it('should  call disableTauxfield ', () => {
      fixture.catOldDiscForm.markAllAsTouched();
      const spyInstance = jest.spyOn(fixture, 'enableTauxFields');

      fixture.readjust(true);
      expect(spyInstance).toHaveBeenCalled();
    });

    it('should  call disableTauxfield   ', () => {
      PopupServiceMock = {
        popupInfo: jest.fn().mockReturnValue(of(true)),
        onConfirm: of(false)
      };
      fixture = new CatalogOldDiscountsComponent(FormBuilderMock, PopupServiceMock, CatDiscountServiceMock, catDiscountRateServiceMock,
        catDocumentServiceMock, routerMoock);
      fixture.ngOnInit();
      fixture.catOldDiscForm.markAllAsTouched();
      const spyInstance = jest.spyOn(fixture.catOldDiscForm, 'patchValue');
      fixture.readjust(true);
      expect(spyInstance).toHaveBeenCalled();
    });
  });
  describe("test :   eventCheck()", () => {

    it('should call readjust', () => {
      fixture.catDiscountObject.id = '7';
      const spyInstance = jest.spyOn(fixture, 'readjust');
      fixture.eventCheck(true);
      expect(spyInstance).toHaveBeenCalled();
    });
    it('should not call readjust', () => {
      fixture.catDiscountObject.id = null;
      const spyInstance = jest.spyOn(fixture, 'readjust');
      fixture.eventCheck(true);
      expect(spyInstance).not.toHaveBeenCalled();
    });

  });

  describe('Test change value()', () => {

    it('should set ponderationRate in   catOldDiscForm', () => {
      fixture.newDiscountRateValueFirstYear = 25;
      fixture.changeValue(50, 0);
      expect(fixture.catOldDiscForm.controls['ponderationRate'].value).toEqual(100)

    });
  });



  describe('Test registerOnChange', () => {
    it('should get register', () => {
      let test = (test) => { return test };
      let size = fixture.subscriptions.length;
      fixture.registerOnChange(test);
      expect(fixture.onChange).not.toEqual(() => {
        //this is intentional
      });
      expect(fixture.subscriptions.length).toEqual(size + 1);


    });
  });

  describe('Test ngOnDestroy', () => {

    it('should unsubscribe', () => {
      fixture.onChangeSub = new Subscription();
      fixture.ngOnDestroy();
      const spyunsubscribe = jest.spyOn(subscriptionMock, 'unsubscribe');
      expect(spyunsubscribe).toBeDefined();
    });
  });

  describe('Test writeValue', () => {
    it('should  write value', () => {
      let value = {
        maxFinPer: 52,
        isPerm: true,
        endDate: null,
        ponderationRate: null,
        isReadjust: false,
        comment: 'comment nature',
        temporary: null,
        startingYear1: null,
        startingNumber: null,
        percentage: 50,
        startingYear2: null,
        startingNumber2: null,
        commentHerit: null,
        files: null,
        catDiscountRateOldList: catdiscountRateList


      };


      fixture.writeValue(value);

      expect(fixture.value).toBeDefined();



    });

  });
  describe('Test registerOnTouched', () => {
    it('should get register', () => {
      fixture.registerOnTouched(test);
      expect(fixture.onTouched).not.toEqual(() => {
        //this is intentional
      });
      expect(fixture.onTouched).toEqual(test);
    });

  });

  describe('Test validate', () => {
    it('should retrun null', () => {

      let formControl: FormControl = new FormControl();

      expect(fixture.validate(formControl)).toEqual(null);

    });


  });

  describe('Test:  editDocumentName', () => {
    it('should set doc edited value to true ', () => {
      fixture.updatedOldDiscountDocs = documentsList;
      fixture.editedDocumentList = [false, false];
      fixture.documentName = "test";
      fixture.editDocumentName(1);
      expect(fixture.editedDocumentList[1]).toEqual(true);

    });

  });

  describe('Test:  onBlurDocName', () => {
    it('should update edited value of doc index ', () => {

      fixture.updatedOldDiscountDocs = documentsList;
      fixture.editedDocumentList = [false, true];

      fixture.onBlurDocument(1);
      expect(fixture.editedDocumentList[1]).toEqual(false);

    });

  });

  describe('Test:  updateNewDiscountDocs ', () => {
    it(' test update NewDiscounts DOc ', () => {

      let file = { 'lastModified': 23, 'name': 'fichier1' }

      fixture.catOldDiscForm.controls['files'].patchValue({

        files: [file]

      })
      fixture.updatedOldDiscountDocsIndex = [1, 2];
      fixture.deletedOlddiscountDocs = [{

        "docName": "document1",
      } as CatDocumentModel, {

        "docName": "document2"

      } as CatDocumentModel];
      fixture.updateOldDiscountDocs(true, 1, 'brand');
    });
  });

  describe('Test:   searchNewDiscountInheritesDocs', () => {
    it('should searchOldDiscountInheritesDocs ', () => {

      fixture.nodeType = 'brand';
      let catNature = {
        id: '133',
        label: 'nature label',
        catActivitySector: {
          id: '1',
          label: 'Transport',
          comment: 'comment act sector',
          isActive: true
        },
      } as CatNatureModel;
      fixture.searchOldDiscountInheritesDocs();
      expect(fixture.natureTitle).toEqual('nature label');
    });
  });

  describe('Test:   enableTauxFields', () => {
    it('should enableTauxFields ', () => {
      fixture.catDiscountObject.id = null;
      const spyInstance = jest.spyOn(fixture.catOldDiscForm, 'patchValue');
      fixture.enableTauxFields();
      expect(spyInstance).toHaveBeenCalled();
    });
  });

  describe('Test:   defaultPonderationAndDuration', () => {
    it('should enableTauxFields ', () => {
      fixture.catDiscountRateOldList = [];
      const spyInstance = jest.spyOn(fixture.catOldDiscForm, 'patchValue');
      fixture.defaultPonderationAndDuration();
      expect(spyInstance).toHaveBeenCalled();
    });
  });

});
