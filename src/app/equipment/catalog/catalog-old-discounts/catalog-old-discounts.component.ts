import { DatePipe } from '@angular/common';
import { Component, EventEmitter, forwardRef, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, NG_VALIDATORS, NG_VALUE_ACCESSOR, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { forkJoin, Subscription, of } from 'rxjs';
import { ConfirmationModalComponent } from '../../../shared/confirmation-modal/confirmation-modal.component';
import { documentsValidator } from '../../../shared/documents/document-validator';
import { CatBrandModel } from '../../../shared/models/cat-brand.model';
import { CatDiscountRateModel } from '../../../shared/models/cat-discount-rate.model';
import { CatDiscountModel } from '../../../shared/models/cat-discount.model';
import { CatDocumentModel } from '../../../shared/models/cat-document.model';
import { CatNatureModel } from '../../../shared/models/cat-nature.model';
import { CatDiscountRateService } from '../../../shared/services/cat-discount-rate.service';
import { CatDiscountService } from '../../../shared/services/cat-discount.service';
import { CatDocumentService } from '../../../shared/services/cat-document.service';
import { PopupService } from '../../../shared/services/popup.service';
import { map, catchError } from 'rxjs/operators';
import { threeDigitDecimalRound } from '../../../shared/ValuationUtils';

@Component({
  selector: 'app-catalog-old-discounts',
  templateUrl: './catalog-old-discounts.component.html',
  styleUrls: ['./catalog-old-discounts.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CatalogOldDiscountsComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => CatalogOldDiscountsComponent),
      multi: true
    }
  ]
})
export class CatalogOldDiscountsComponent implements OnInit {

  //list to save discount after every modification 
  //lastModifOfCatDiscountRate: CatDiscountRateModel[] = [];
  percentSymbol = "%";
  catDiscountObject: CatDiscountModel;
  catDiscountObjectInitial: CatDiscountModel;
  catDiscountRateOldList: CatDiscountRateModel[] = [];
  @Input() set catDiscountRateOld(catDiscountRateList) {
    if (catDiscountRateList && this.catOldDiscForm) {
      this.catDiscountRateOldList = catDiscountRateList;

      if (this.catOldDiscForm != undefined && !this.catDiscountObject.id) {
        this.initForm();

        if (this.isUnlocked) {
          this.initDiscountLock();
          this.unlockForm();
        } else {
          this.lockForm();
        }
        this.catOldDiscForm.patchValue({ isPerm: this.catDiscountObject.isPerm != null ? this.catDiscountObject.isPerm : false })
        this.catOldDiscForm.patchValue({ temporary: this.catDiscountObject.isPerm != null ? !this.catDiscountObject.isPerm : true })
      }

    }

  }


  @Output() refreshOldDicountDocs = new EventEmitter();
  @Output() editCatalogue = new EventEmitter();

  @Input() set catDiscount(catDiscount: CatDiscountModel) {

    if (catDiscount != null && catDiscount != undefined) {
      this.initDiscountLock();

      this.catDiscountObject = catDiscount;
      this.catDiscountObjectInitial = catDiscount;
      this.createForm();
      this.initComponent();

      this.catOldDiscForm.patchValue({ isPerm: this.catDiscountObject ? this.catDiscountObject.isPerm : false })
      this.catOldDiscForm.patchValue({ temporary: this.catDiscountObject ? !this.catDiscountObject.isPerm : true })
      this.catOldDiscForm.get('catDiscountRateOldList').markAsUntouched()


    }
  }
  catDiscountRateInitialList: CatDiscountRateModel[] = [];
  showForm: boolean = false;
  showDiscounts: boolean = false;
  discountComment: string;
  isDiscountValid: any;
  discountRate: number;

  catOldDiscForm: FormGroup;

  disableDiscounts: boolean = false;
  duplicateDiscounts: boolean = false;
  editingMode: boolean = false;
  subscriptions: Subscription[] = [];

  lockedDiscounts: boolean[];
  isUnlocked: boolean = false;
  catBrandObject: CatBrandModel = {} as any;
  catNatureObject: CatNatureModel = {} as any;
  onChangeSub: Subscription;
  isNew = false;
  // the new discounts first yearly rate
  newDiscountRateValueFirstYear: number;


  @Input() oldDiscountDocs: CatDocumentModel[] = [];
  @Input() isSubmitted = false;

  @Input() set docs(docs) {
    if (docs) {
      this.oldDiscountDocs = docs;
      this.getOldDiscountDocs();
    }

  }
  updatedOldDiscountDocs: CatDocumentModel[] = [];
  deletedOlddiscountDocs: CatDocumentModel[] = [];

  updatedOldDiscountDocsIndex: number[] = [];
  editedDocumentList: boolean[] = [];
  documentName: string;
  inheritedOldDiscountDocsNature: CatDocumentModel[] = [];
  inheritedOldDiscountDocsSector: CatDocumentModel[] = [];
  activitySectorTitle: string;
  natureTitle: string;

  //max duration for generalities
  @Input() defaultMaxDuration: number;
  onTouched = () => {
    // This is intentional
  };
  @ViewChild(CatalogOldDiscountsComponent) catalogOldDiscountsComponent: CatalogOldDiscountsComponent;

  onChange: any = () => {
    // This is intentional
  };
  @Input() nodeType: string;
  @Input() idCateg: string;

  get value() {
    return this.catOldDiscForm.getRawValue();
  }
  set value(value) {
    this.catOldDiscForm.setValue(value);
    this.onChange(value);
    this.onTouched();
  }
  get f() {
    return this.catOldDiscForm.controls;
  }
  get catDiscountRateOldListControls() {
    return (this.catOldDiscForm.controls["catDiscountRateOldList"] as FormArray).controls;

  }

  @Input() set isUnlock(isUnlock: boolean) {

    this.isUnlocked = isUnlock;

    if (this.catOldDiscForm != undefined) {
      if (isUnlock) {
        this.initDiscountLock();
        this.unlockForm();
      } else {
        this.lockForm();
      }

      this.catOldDiscForm.patchValue({ isPerm: this.catDiscountObject.isPerm != null ? this.catDiscountObject.isPerm : false })
      this.catOldDiscForm.patchValue({ temporary: this.catDiscountObject.isPerm != null ? !this.catDiscountObject.isPerm : true })
    }
  }

  constructor(private fb: FormBuilder, private popupService: PopupService,
    private catDiscountService: CatDiscountService, private catDiscountRateService: CatDiscountRateService,
    private catDocumentService: CatDocumentService, private router: Router) { }
  @Input() set rateNewDiscountFirstYear(newRate: number) {
    if (newRate > 0) {
      this.newDiscountRateValueFirstYear = newRate;
      if (this.catDiscountRateOldListControls.length > 0) {
        this.ponderationChange();
      }
    }

  }

  ngOnInit(): void {
    this.createForm();
    this.initComponent();
    this.initDiscounts();
    this.ponderationChange();
    this.editCatalogue.emit();


  }

  startingNumberValidators = [
    Validators.min(1)
  ];

  createForm() {

    this.catOldDiscForm = this.fb.group({
      isReadjust: [{ value: '', disabled: true }],
      isPerm: [{ value: '', disabled: true }],
      temporary: [{ value: '', disabled: true }],
      endDate: [{ value: '31/12/9999', disabled: true },
      Validators.pattern(new RegExp("([0-9]{4}[/](0[1-9]|1[0-2])[/]([0-2]{1}[0-9]{1}|3[0-1]{1})|([0-2]{1}[0-9]{1}|3[0-1]{1})[/](0[1-9]|1[0-2])[/][0-9]{4})"))],

      startingYear1: [{ value: '', disabled: true }],
      startingNumber: [{ value: '', disabled: true }, this.startingNumberValidators],
      percentage: [{ value: '', disabled: true }, [Validators.min(1), Validators.max(100)]],
      startingYear2: [{ value: '', disabled: true }],
      startingNumber2: [{ value: '', disabled: true }, this.startingNumberValidators],
      comment: [{ value: '', disabled: true }],
      commentHerit: [{ value: '', disabled: true }],
      files: [{ value: '', disabled: true }, { validators: [documentsValidator] }],
      ponderationRate: [{ value: null, disabled: true }],
      maxFinPer: [{ value: '', disabled: true }],
      catDiscountRateOldList: this.fb.array([...this.initiateRateOldListFormArray()])

    });

    this.subscriptions.push(
      this.catOldDiscForm.valueChanges.subscribe(value => {
        this.onChange(this.catOldDiscForm.getRawValue());
        this.onTouched();
      })
    );
    this.showForm = true;

    this.catOldDiscForm.patchValue({ isPerm: (this.catDiscountObject && this.catDiscountObject.isPerm) ? this.catDiscountObject.isPerm : false })
    this.catOldDiscForm.patchValue({ temporary: (this.catDiscountObject && this.catDiscountObject.isPerm) ? !this.catDiscountObject.isPerm : true })
  }

  initiateRateOldListFormArray() {
    let formArray: FormGroup[] = [];
    for (let catDiscountRate of this.catDiscountRateOldList) {
      const discountRateForm = this.initiateFormBuilder({ ...catDiscountRate });
      formArray.push(discountRateForm);
    }

    return formArray;
  }
  initiateFormBuilder(catDiscountRate: CatDiscountRateModel) {
    return this.fb.group({
      discountRateValue: [{ value: (catDiscountRate.discountRateValue != null) ? catDiscountRate.discountRateValue : null, disabled: true }, Validators.required],
      discountRateYear: [catDiscountRate.discountRateYear != null ? catDiscountRate.discountRateYear : null],
      id: [catDiscountRate.id != null ? catDiscountRate.id : null],
      catDiscount: [catDiscountRate.catDiscount != null ? catDiscountRate.catDiscount : null],
      updatedDate: [catDiscountRate.updatedDate != null ? catDiscountRate.updatedDate : null],
      createdDate: [catDiscountRate.createdDate != null ? catDiscountRate.createdDate : null],
      createdBy: [catDiscountRate.createdBy != null ? catDiscountRate.createdBy : null],
      updatedBy: [catDiscountRate.updatedBy != null ? catDiscountRate.updatedBy : null],
    });
  }

  changeValue(value: number, i: number): void {
    if (i == 0) {
      //first year changed
      this.catOldDiscForm.patchValue({ ponderationRate: this.calculateNewWeighting(value) });
    }
  }


  eventCheck(event) {
    if (this.catDiscountObject.id) {
      this.readjust(event.checked);
    }
    else {
      this.catOldDiscForm.patchValue({ isReadjust: true });


    }
  }
  initForm() {

    this.catOldDiscForm.patchValue({ isReadjust: this.catDiscountObject ? this.catDiscountObject.isReadjust : null })
    this.catOldDiscForm.patchValue({ isPerm: true })
    this.catOldDiscForm.patchValue({ temporary: this.catDiscountObject ? (this.catDiscountObject.endDate != null) : false })
    this.catOldDiscForm.patchValue({ startingYear1: this.catDiscountObject ? false : null })
    this.catOldDiscForm.patchValue({ startingNumber: '' })
    this.catOldDiscForm.patchValue({ percentage: '' })
    this.catOldDiscForm.patchValue({ startingYear2: false })
    this.catOldDiscForm.patchValue({ startingNumber2: '' })
    this.catOldDiscForm.patchValue({ comment: (this.catDiscountObject != null) ? this.catDiscountObject.comment : null })

    var datePipe = new DatePipe("en-US");

    this.catOldDiscForm.patchValue({ endDate: this.catDiscountObject ? datePipe.transform(this.catDiscountObject.endDate, 'dd/MM/yyyy') : '31/12/9999' })
    this.catOldDiscForm.setControl('catDiscountRateOldList', this.fb.array([...this.initiateRateOldListFormArray()]));


    this.readjust(this.catDiscountObject.isReadjust);
    if (!this.catDiscountObject.id && this.catOldDiscForm.disabled) {
      this.catOldDiscForm.patchValue({ isPerm: true });

    }
    if (this.nodeType == 'brand' && this.catDiscountObject && this.catDiscountObject.catNature && this.catDiscountObject.catNature.id) {
      this.catDiscountService.getDiscountDetailsByIdAndDesc(this.catDiscountObject.catNature.id, 'nature', this.isNew).subscribe(data => {
        this.catOldDiscForm.patchValue({ commentHerit: (data != null) ? data.comment : null })
      })
    }
    this.catOldDiscForm.patchValue({ isPerm: this.catDiscountObject.isPerm ? this.catDiscountObject.isPerm : false })
    this.catOldDiscForm.patchValue({ temporary: this.catDiscountObject.isPerm ? !this.catDiscountObject.isPerm : true })


  }

  undo() {

    this.catOldDiscForm.patchValue({ isReadjust: this.catDiscountObject ? this.catDiscountObject.isReadjust : null })
    this.catOldDiscForm.patchValue({ isPerm: true })
    this.catOldDiscForm.patchValue({ temporary: this.catDiscountObject ? (!this.catDiscountObject.isPerm) : false })
    var datePipe = new DatePipe("en-US");
    this.catOldDiscForm.patchValue({ endDate: this.catDiscountObject ? datePipe.transform(this.catDiscountObject.endDate, 'dd/MM/yyyy') : '31/12/9999' })
    this.catOldDiscForm.patchValue({ startingYear1: this.catDiscountObject ? false : null })
    this.catOldDiscForm.patchValue({ startingNumber: '' })
    this.catOldDiscForm.patchValue({ percentage: '' })
    this.catOldDiscForm.patchValue({ startingYear2: false })
    this.catOldDiscForm.patchValue({ startingNumber2: '' })
    this.catOldDiscForm.patchValue({ comment: (this.catDiscountObject != null) ? this.catDiscountObject.comment : null });
    this.catOldDiscForm.patchValue({ files: { files: [] } });
    if (this.nodeType == 'brand') {
      this.catDiscountService.getDiscountDetailsByIdAndDesc(this.catDiscountObject.catNature.id, 'nature', this.isNew).subscribe(data => {
        this.catOldDiscForm.patchValue({ commentHerit: (data != null) ? data.comment : null })

      })
    }
    this.lockForm();
    this.resetDiscounts();
    this.resetPonderationAndDuration();
    this.initDiscountLock();
  }

  initComponent() {
    this.initDiscounts();
    if (this.catDiscountObject) {
      this.searchOldDiscountInheritesDocs()

      this.initForm();
      if (this.catOldDiscForm != undefined) {
        if (this.isUnlocked) {
          this.unlockForm()
        } else {
          this.lockForm();
        }
      }
    }


  }

  initDiscounts() {
    //if max duration dosent exist then 72
    // load Cotations
    if (this.catDiscountRateOldList != null && this.catDiscountRateOldList != undefined && this.catDiscountRateOldList.length > 0) {

      this.catDiscountRateOldList.forEach((element, index) => {
        this.catDiscountRateInitialList.push(Object.assign({}, element))
      });

      this.showDiscounts = true;
      this.initDiscountLock();
    }

  }
  initDiscountLock() {


    if (this.catDiscountRateOldList && this.catOldDiscForm && this.editingMode && !this.disableDiscounts) {
      this.catOldDiscForm.get('catDiscountRateOldList').enable();
    }
  }
  isAdmin() {
    return false;
  }

  onBlurPercentage() {
    if (1 > this.catOldDiscForm.get('percentage').value || this.catOldDiscForm.get('percentage').value > 100)
      this.catOldDiscForm.patchValue({ percentage: null })
    this.catOldDiscForm.get('startingNumber').setValidators(this.startingNumberValidators.concat(Validators.max(this.catDiscountRateInitialList.length - 1)));
    this.catOldDiscForm.get('startingNumber').updateValueAndValidity();
  }
  lockForm() {
    this.catOldDiscForm.disable();
    this.disableManipulators();
    this.editingMode = false;
  }
  unlockForm() {
    this.catOldDiscForm.enable();
    this.editingMode = true;
    this.catOldDiscForm.get('catDiscountRateOldList').markAsUntouched()

    this.readjust(this.catDiscountObject.isReadjust);
    if (this.catDiscountObject.isPerm) {
      this.catOldDiscForm.get('endDate').disable();
    }
    else {
      this.catOldDiscForm.get('endDate').enable();
    }
    this.catOldDiscForm.get('commentHerit').disable();

    this.catOldDiscForm.updateValueAndValidity();
  }

  readjust(isReadjustement) {
    if (this.catOldDiscForm.get('catDiscountRateOldList').touched) {
      this.popupService.popupInfo(
        'Les paramètres initiaux seront restaurés. Voulez-vous confirmer ?', 'OUI', null, 'NON', ConfirmationModalComponent);
      const subscription = this.popupService.onConfirm.subscribe((t) => {
        if (t) {
          if (isReadjustement) {
            this.enableTauxFields();
          } else {
            this.disableTauxFields();
          }
          this.catOldDiscForm.updateValueAndValidity();
        }
        else {

          this.catOldDiscForm.patchValue({ isReadjust: !isReadjustement })

        }
        subscription.unsubscribe();
      });
    } else {
      if (isReadjustement) {
        this.enableTauxFields();
      } else {
        this.disableTauxFields();
      }
    }
  }


  tempOrPerm() {
    if (this.catOldDiscForm.get('temporary').value) {
      this.catOldDiscForm.get('endDate').disable();
      this.catOldDiscForm.controls['endDate'].setValidators([]);
      this.catOldDiscForm.patchValue({ temporary: false })
      this.catOldDiscForm.patchValue({ isPerm: true })
      this.catOldDiscForm.patchValue({ endDate: '31/12/9999' });
      this.catOldDiscForm.updateValueAndValidity();
    }
    else {
      this.catOldDiscForm.get('endDate').enable();
      this.catOldDiscForm.patchValue({ temporary: true })
      this.catOldDiscForm.patchValue({ isPerm: false })
      this.catOldDiscForm.controls['endDate'].setValidators([Validators.required,
      Validators.pattern(new RegExp("([0-9]{4}[/](0[1-9]|1[0-2])[/]([0-2]{1}[0-9]{1}|3[0-1]{1})|([0-2]{1}[0-9]{1}|3[0-1]{1})[/](0[1-9]|1[0-2])[/][0-9]{4})"))])
      this.catOldDiscForm.updateValueAndValidity();
    }
  }
  sinceYear() {
    if (this.catOldDiscForm.get('startingYear1').value) {
      this.resetDiscounts();//to delete
      //restore last modification done
      //this.catOldDiscForm.get('catDiscountRateList').patchValue(this.lastModifOfCatDiscountRate)

      this.initDiscountLock();
      this.catOldDiscForm.patchValue({ startingNumber: null })
      this.catOldDiscForm.patchValue({ percentage: null })
      this.catOldDiscForm.get('startingNumber').disable();
      this.catOldDiscForm.get('percentage').disable();
    }
    else {
      this.catOldDiscForm.patchValue({ startingYear2: false })
      this.initDiscountLock();
      this.duplicateDiscounts = false;
      this.catOldDiscForm.get('startingNumber').enable();
      this.catOldDiscForm.get('percentage').enable();

      this.catOldDiscForm.get('startingNumber2').disable();

      this.catOldDiscForm.patchValue({ startingNumber2: null })
      this.resetDiscounts();//to delete
      this.initDiscountLock();
    }
    if (this.catDiscountRateInitialList[0] && this.catDiscountRateInitialList[0].discountRateValue) {

      this.ponderationChange();
    }
    this.catOldDiscForm.updateValueAndValidity();
  }
  duplicateYear() {
    if (this.catOldDiscForm.get('startingYear2').value) {
      this.resetDiscounts();//to delete
      //restore last modification done
      //this.catOldDiscForm.get('catDiscountRateList').patchValue(this.lastModifOfCatDiscountRate)

      this.initDiscountLock();
      this.catOldDiscForm.patchValue({ startingNumber2: null })
      this.catOldDiscForm.get('startingNumber2').disable();

    }
    else {
      this.catOldDiscForm.patchValue({ startingYear1: false })
      this.initDiscountLock();
      this.duplicateDiscounts = true;
      this.catOldDiscForm.get('startingNumber').disable();
      this.catOldDiscForm.get('percentage').disable();

      this.catOldDiscForm.get('startingNumber2').enable();

      this.catOldDiscForm.patchValue({ startingNumber: null })
      this.catOldDiscForm.patchValue({ percentage: null });
      this.resetDiscounts();//to delete
      this.initDiscountLock();

    }
    if (this.catDiscountRateInitialList[0] && this.catDiscountRateInitialList[0].discountRateValue)
      this.ponderationChange();
    this.catOldDiscForm.updateValueAndValidity();
  }


  sinceYearCalcul() {
    if (this.catOldDiscForm.get('startingNumber').value && this.catOldDiscForm.get('percentage').value) {
      this.resetDiscounts();//to delete
      //store before the modification
      //this.lastModifOfCatDiscountRate = this.catOldDiscForm.value.catDiscountRateOldList;

      this.initDiscountLock();
      var index = this.catOldDiscForm.get('startingNumber').value;
      var percentage = this.catOldDiscForm.get('percentage').value;
      this.catDiscountRateOldListControls[index - 1].disable()

      for (let i = index; i < this.catDiscountRateOldList.length; i++) {
        //calculate the discounts into next ones.
        this.catDiscountRateOldListControls[i].patchValue({
          discountRateValue: ((this.catDiscountRateOldListControls[i - 1].value.discountRateValue - percentage) > 0
            ? (this.catDiscountRateOldListControls[i - 1].value.discountRateValue - percentage) : 0)
        })
        this.catDiscountRateOldListControls[i].disable()
      }
      this.ponderationChange();
    }
  }
  duplicateCalcul() {
    this.resetDiscounts();//to delete
    this.initDiscountLock();
    var index = this.catOldDiscForm.get('startingNumber2').value;
    if (index) {
      //store before the modification
      //this.lastModifOfCatDiscountRate = this.catOldDiscForm.value.catDiscountRateOldList;

      this.catDiscountRateOldListControls[index - 1].disable();
      for (let i = index; i < this.catDiscountRateOldList.length; i++) {
        //duplicate the discounts into next ones.
        this.catDiscountRateOldListControls[i].patchValue({ discountRateValue: this.catDiscountRateOldListControls[index - 1].value.discountRateValue });
        this.catDiscountRateOldListControls[i].disable();
      }
      this.ponderationChange();
      this.catOldDiscForm.get('startingNumber2').setValidators(this.startingNumberValidators.concat(Validators.max(this.catDiscountRateInitialList.length - 1)));
      this.catOldDiscForm.get('startingNumber2').updateValueAndValidity();
    }
  }

  enableTauxFields() {
    this.catOldDiscForm.get('endDate').enable();
    this.catOldDiscForm.controls['endDate'].setValidators([Validators.required,
    Validators.pattern(new RegExp("([0-9]{4}[/](0[1-9]|1[0-2])[/]([0-2]{1}[0-9]{1}|3[0-1]{1})|([0-2]{1}[0-9]{1}|3[0-1]{1})[/](0[1-9]|1[0-2])[/][0-9]{4})"))])
    this.catOldDiscForm.get('startingYear1').enable();
    this.catOldDiscForm.get('startingYear2').enable();
    this.catOldDiscForm.get('startingNumber').disable();
    this.catOldDiscForm.get('percentage').disable();
    this.catOldDiscForm.get('startingNumber2').disable();
    this.disableDiscounts = false;
    this.catOldDiscForm.get('catDiscountRateOldList').enable();
    this.catOldDiscForm.get('isPerm').enable();

    this.catOldDiscForm.patchValue({ temporary: true })
    if (!this.catDiscountObject.id) {
      this.catOldDiscForm.patchValue({ isPerm: true });

    }
    else {
      this.catOldDiscForm.patchValue({ isPerm: false });

    }
    this.catOldDiscForm.patchValue({ isReadjust: true });
    this.resetPonderationAndDuration();
    this.catOldDiscForm.get('ponderationRate').enable();
    this.catOldDiscForm.get('maxFinPer').enable();
    if (this.catDiscountRateOldList[0] && this.catDiscountRateOldList[0].discountRateValue) {
      this.catDiscountRateOldListControls[0].patchValue({ discountRateValue: this.calculateNewYearlyRate(this.catDiscountObject.ponderationRate) })
    }
    this.catOldDiscForm.get('catDiscountRateOldList').markAsUntouched()
    this.catOldDiscForm.updateValueAndValidity();
    if (!this.catDiscountObject.id) {
      this.catOldDiscForm.get('isReadjust').disable();

    }

  }
  disableTauxFields() {
    this.catOldDiscForm.get('endDate').disable();
    this.catOldDiscForm.controls['endDate'].setValidators([]);
    this.catOldDiscForm.get('startingYear1').disable();
    this.catOldDiscForm.get('startingYear2').disable();
    this.catOldDiscForm.get('startingNumber').disable();
    this.catOldDiscForm.get('percentage').disable();
    this.catOldDiscForm.get('startingNumber2').disable();
    this.catOldDiscForm.patchValue({ startingYear1: false })
    this.catOldDiscForm.patchValue({ startingYear2: false })
    this.catOldDiscForm.patchValue({ startingNumber: null })
    this.catOldDiscForm.patchValue({ percentage: null })
    this.catOldDiscForm.patchValue({ startingNumber2: null })
    this.catOldDiscForm.get('ponderationRate').disable();
    this.catOldDiscForm.get('maxFinPer').disable();
    var datePipe = new DatePipe("en-US");


    this.catOldDiscForm.patchValue({ endDate: this.catDiscountObject ? datePipe.transform(this.catDiscountObject.endDate, 'dd/MM/yyyy') : '31/12/9999' })
    this.catOldDiscForm.get('catDiscountRateOldList').markAsUntouched()
    this.catOldDiscForm.get('catDiscountRateOldList').disable();
    this.disableDiscounts = true;



    this.initDiscountLock();
    this.resetDiscounts();
    this.defaultPonderationAndDuration();
    this.catOldDiscForm.patchValue({ temporary: false })
    this.catOldDiscForm.patchValue({ isPerm: true });
    this.catOldDiscForm.patchValue({ isReadjust: false });
    this.catOldDiscForm.updateValueAndValidity();




  }
  ponderationChange() {
    if (this.catDiscountRateOldListControls[0]) {
      if (this.calculateNewYearlyRate(this.catOldDiscForm.get('ponderationRate').value) > 100) {

        this.catDiscountRateOldListControls[0].patchValue({ discountRateValue: 100 });
      } else {
        this.catDiscountRateOldListControls[0].patchValue({ discountRateValue: this.calculateNewYearlyRate(this.catOldDiscForm.get('ponderationRate').value) })
      }
    }


  }

  //calculate the new yearly rate  (discount)
  calculateNewYearlyRate(ponderation: number) {

    return threeDigitDecimalRound((+this.newDiscountRateValueFirstYear) + (+((this.newDiscountRateValueFirstYear / 100) * ponderation)));
  }
  //calculate the new Ponderation
  calculateNewWeighting(yearlyRateValue: number) {
    return threeDigitDecimalRound (((yearlyRateValue / this.newDiscountRateValueFirstYear) - 1) * 100)
  }


  disableManipulators() {
    this.catOldDiscForm.get('startingYear1').disable();
    this.catOldDiscForm.get('startingYear2').disable();
    this.catOldDiscForm.get('startingNumber').disable();
    this.catOldDiscForm.get('percentage').disable();
    this.catOldDiscForm.get('startingNumber2').disable();
    this.catOldDiscForm.patchValue({ startingYear1: false })
    this.catOldDiscForm.patchValue({ startingYear2: false })
    this.catOldDiscForm.patchValue({ startingNumber: null })
    this.catOldDiscForm.patchValue({ percentage: null })
    this.catOldDiscForm.patchValue({ startingNumber2: null })
  }

  defaultPonderationAndDuration() {

    if (this.catDiscountRateOldList[0] && this.catDiscountRateOldList[0].catDiscount) {
      this.catOldDiscForm.patchValue({ ponderationRate: this.catDiscountRateOldList[0].catDiscount.ponderationRate });

    }else{
      this.catOldDiscForm.patchValue({ ponderationRate: 50 });
    }
    this.catOldDiscForm.patchValue({ maxFinPer: this.defaultMaxDuration ? this.defaultMaxDuration : 72 });
    if (this.catDiscountRateOldList[0] && this.catDiscountRateOldList[0].discountRateValue) {
      this.catDiscountRateOldListControls[0].patchValue({ discountRateValue: this.calculateNewYearlyRate(this.catDiscountRateOldList[0].catDiscount.ponderationRate) });
    }
    this.catOldDiscForm.updateValueAndValidity();
  }
  resetDiscounts() {
    this.catOldDiscForm.patchValue({ catDiscountRateOldList: this.catDiscountRateInitialList });
    this.catOldDiscForm.get('catDiscountRateOldList').markAsUntouched()

  }
  resetPonderationAndDuration() {


    this.catOldDiscForm.patchValue({ ponderationRate: this.catDiscountObject.ponderationRate });
    this.catOldDiscForm.patchValue({ maxFinPer: this.catDiscountObject ? this.catDiscountObject.maxFinPer : this.defaultMaxDuration });
    this.catOldDiscForm.updateValueAndValidity();
  }
  resetForm() {
    this.initComponent();
    this.lockForm();
    this.resetDiscounts();
    this.resetPonderationAndDuration();
  }
  registerOnChange(fn: any) {

    this.subscriptions.push(this.catOldDiscForm.valueChanges.pipe(map(_ => this.catOldDiscForm.getRawValue())).subscribe(fn));

  }



  ngOnDestroy() {
    if (this.onChangeSub) {
      this.onChangeSub.unsubscribe();
    }
  }

  writeValue(value: any) {
    window.setTimeout(() => {
      if (value) {

        this.catOldDiscForm.setValue(value);
      }
    }, 100);
  }

  registerOnTouched(onTouched: any) {
    this.onTouched = onTouched;
  }
  validate(_: FormControl) {

    return this.catOldDiscForm.valid ? null : { profile: { valid: false, } };
  }


  deleteDocument(index: number) {

    this.popupService.popupInfo('Êtes-vous sûr(e) de vouloir supprimer la pièce jointe ?', 'Confirmer', null, 'Annuler', ConfirmationModalComponent);
    const subscription = this.popupService.onConfirm.subscribe((t) => {
      if (t) {
        this.deletedOlddiscountDocs.push(this.updatedOldDiscountDocs[index])

        this.editedDocumentList.splice(index, 1);
        this.updatedOldDiscountDocs.splice(index, 1);
        let indexIndex = this.updatedOldDiscountDocsIndex.findIndex(val => val == index);
        if (indexIndex > 0) {
          this.updatedOldDiscountDocsIndex.splice(indexIndex, 1);
        }
      }
      subscription.unsubscribe();

    })

  }
  editDocumentName(index: number) {
    this.editedDocumentList[index] = true;
    this.documentName = this.updatedOldDiscountDocs[index].docName;

  }
  onBlurDocument(index: number) {

    this.updatedOldDiscountDocs[index].docName = this.documentName;
    if (this.updatedOldDiscountDocsIndex.findIndex(val => val == index) == -1) {
      this.updatedOldDiscountDocsIndex.push(index);
    }
    this.editedDocumentList[index] = false;
  }

  //save delete update docs
  updateOldDiscountDocs(isAddMode, id, path) {
    let files: File[] = this.catOldDiscForm.controls['files'].value.files;

    let request = [];
    // save new docs
    if (files && files.length > 0) {
      let brandId = null;
      if (this.catDiscountObject.catBrand) {
        brandId = this.catDiscountObject.catBrand.id;
      }
      files.forEach(file => request.push(this.catDocumentService.uploadDocument(file, this.catDiscountObject.catActivitySector.id,
        this.catDiscountObject.catNature.id, brandId, null, 'O').pipe(catchError(error => of(null)))));
    }

    //update docs titles
    if (this.updatedOldDiscountDocsIndex.length > 0) {
      let docs = []
      this.updatedOldDiscountDocsIndex.forEach(index => docs.push(this.updatedOldDiscountDocs[index]))
      docs.forEach(doc => request.push(this.catDocumentService.updateDocument(doc).pipe(catchError(error => of(null)))));
      /*
      request.push(this.catActivitySectorService.updateActivitySectorDocument(docs))
      */
    }
    // delete files
    if (this.deletedOlddiscountDocs.length > 0) {

      this.deletedOlddiscountDocs.forEach(doc => request.push(this.catDocumentService.deleteCatDocument(doc).pipe(catchError(error => of(null)))))
    }
    forkJoin(request).subscribe(res => {
      //this is intentional

    }, err => {
      console.error(err);
    }, () => {
      this.catOldDiscForm.get('files').reset();
      this.refreshOldDicountDocs.emit();
      if (isAddMode) {
        this.router.navigate(['catalog', path, id]);
      }
    });
  }

  getOldDiscountDocs() {
    this.updatedOldDiscountDocs = [];
    this.editedDocumentList = [];
    this.oldDiscountDocs.forEach((element, index) => {

      this.updatedOldDiscountDocs.push(Object.assign({}, element))
      this.editedDocumentList[index] = false;
    });
  }


  downloadDocument(documentToDownload: CatDocumentModel) {
    this.catDocumentService.downloadDocument(documentToDownload.gedPath, documentToDownload.docName).subscribe(response => {
      var file = new Blob([response], { type: 'application/*' })
      var fileURL = URL.createObjectURL(file);
      const doc = document.createElement('a');
      doc.download = documentToDownload.docName;
      doc.href = fileURL;
      doc.dataset.downloadurl = [doc.download, doc.href].join(':');
      document.body.appendChild(doc);
      doc.click();
    });
  }
  displayPlusBtns(doc: CatDocumentModel): boolean {
    if (!this.catDiscountObject.id)
      return true;
    if (this.catDiscountObject.catBrand)
      return doc.catBrand != null;

    return doc.catNature != null;
  }
  searchOldDiscountInheritesDocs() {


    if (this.nodeType == 'brand') {
      if (this.catDiscountObject && this.catDiscountObject.catActivitySector && this.catDiscountObject.catNature) {
        this.catDocumentService.searchInheratedDocuments(this.catDiscountObject.catActivitySector.id, this.catDiscountObject.catNature.id, null, 'O').subscribe(data => {
          if (data) {
            this.inheritedOldDiscountDocsNature = data.filter(doc => doc.catNature != null && doc.catBrand == null);
            this.inheritedOldDiscountDocsSector = data.filter(doc => doc.catNature == null && doc.catBrand == null);
          }
          this.natureTitle = this.catDiscountObject.catNature.label;
        });
      }

    }
    else {

      if (this.catDiscountObject && this.catDiscountObject.catActivitySector) {
        this.catDocumentService.searchInheratedDocuments(this.catDiscountObject.catActivitySector.id, null, null, 'O').subscribe(data => {
          if (data) {
            this.inheritedOldDiscountDocsSector = data.filter(doc => doc.catNature == null && doc.catBrand == null);
          }
        });
        this.activitySectorTitle = this.catDiscountObject.catActivitySector.label;

      }




    }
  }




}

