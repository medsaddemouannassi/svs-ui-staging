import { DatePipe } from '@angular/common';
import { Component, forwardRef, Input, OnDestroy, OnInit } from '@angular/core';
import { ControlValueAccessor, FormBuilder, FormControl, FormGroup, NG_VALIDATORS, NG_VALUE_ACCESSOR, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { CatDiscountAverageProcessElementModel } from '../../../shared/models/cat-discount-average-process-element.model';
import { CatDiscountAverageModel } from '../../../shared/models/cat-discount-average.model';
import { DiscountAverageResultModel } from '../../../shared/models/cat-discout-average-result.model';
import { DecoteService } from '../../../shared/services/decote.service';
import { formatStringtoDate } from '../../../shared/utils';

@Component({
    selector: 'app-cat-discount-average',
    templateUrl: './cat-discount-average.component.html',
    styleUrls: ['./cat-discount-average.component.css'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => CatDiscountsAverageComponent),
            multi: true
        },
        {
            provide: NG_VALIDATORS,
            useExisting: forwardRef(() => CatDiscountsAverageComponent),
            multi: true
        }
    ]

})
export class CatDiscountsAverageComponent implements OnInit, OnDestroy, ControlValueAccessor, Validators {


    catDiscountAvgForm: FormGroup;
    subscriptions: Subscription[] = [];
    isUnlocked: boolean = false;
    catdiscountAverage: CatDiscountAverageProcessElementModel = {} as CatDiscountAverageProcessElementModel;

    catDiscountAvgsResult: CatDiscountAverageModel = {
        discountAvgs: []
    } as CatDiscountAverageModel;
    catDiscountAverageList: DiscountAverageResultModel[] = [];


    @Input() nodeType: string;
    @Input() idNature: string;
    @Input() isNew: boolean;
    @Input() idSector: string;
    @Input() idBrand: string;

    percentSymbol = "%";

    constructor(private fb: FormBuilder, private decoteService: DecoteService, private route: Router) {

    }

    ngOnInit(): void {
        this.updateCatDiscountAverageList();
        this.createForm();
    }


    createForm() {

        this.catDiscountAvgForm = this.fb.group({
            nbForms: { value: this.catDiscountAvgsResult ? this.catDiscountAvgsResult.valuationsNumber : 0, disabled: true },
            nbMinToConsider: [{ value: null, disabled: true }],
            startDate: [{ value: null, disabled: true },
            Validators.pattern(/^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/)],

            endDate: [{ value: null, disabled: true },
            Validators.pattern(/^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/)],

            discountAvgs: this.fb.array([...this.initiateAveragesListFormArray()])
        });

        this.subscriptions.push(
            this.catDiscountAvgForm.valueChanges.subscribe(value => {
                this.onChange(this.catDiscountAvgForm.getRawValue());
                this.onTouched();

                if (this.idSector && this.idNature) {
                    this.loadAvg();
                }

            })
        );


    }

    loadAvg() {


        if (this.catDiscountAvgForm && (this.catDiscountAvgForm.valid || this.catDiscountAvgForm.disabled)) {

            this.generateCatdiscountAverageObject();
            if (!this.isAddMode()) {
                this.loadCatDiscountAverageList(this.catdiscountAverage);
            } 
        }
    }

    updateDiscountAvgsControl() {
        if (this.catDiscountAvgForm) {
        
            let list = [];
            this.catDiscountAverageList.forEach(avg => list.push({discountRateValue :avg.discountRateValue }))
            this.catDiscountAvgForm.patchValue({discountAvgs:list},{emitEvent:false})
        }

    }

    initiateAveragesListFormArray() {
        let formArray: FormGroup[] = [];
        if (this.catDiscountAverageList) {
            for (let catDiscountAvg of this.catDiscountAverageList) {
                formArray.push(this.fb.group({
                    discountRateValue: [{ value: catDiscountAvg.discountRateValue, disabled: true }]
                }));
            }
        }

        return formArray;
    }






    loadCatDiscountAverageList(catdiscountAverage: CatDiscountAverageProcessElementModel) {

        this.decoteService.getDiscountsAverage(catdiscountAverage).subscribe(res => {
            this.catDiscountAverageList = [];
            this.catDiscountAvgsResult = res;


        }, err => {
            console.error('error on loadCatDiscountAverageList');
        }, () => {
            this.updateCatDiscountAverageList();

            this.catDiscountAvgForm.patchValue({
                nbForms: this.catDiscountAvgsResult.valuationsNumber
            },{emitEvent:false});

            this.updateDiscountAvgsControl();
        })
    }

    updateCatDiscountAverageList() {
        let size = 0;
        if (this.catDiscountAvgsResult.discountAvgs != null) {
            size = this.catDiscountAvgsResult.discountAvgs.length
            this.catDiscountAvgsResult.discountAvgs.forEach((element, index) => {
                this.catDiscountAverageList[index] = element;
            })
        }

        if (!this.isAddMode()) {
            for (let i = size + 1; i <= 15; i++) {
                this.catDiscountAverageList.push({ discountRateYear: i, discountRateValue: 'NA' } as DiscountAverageResultModel)
            }
        }

    }

    unlockForm() {
        this.catDiscountAvgForm.enable({emitEvent:false , onlySelf:true});
        this.catDiscountAvgForm.get('nbForms').disable({emitEvent:false , onlySelf:true});
        this.catDiscountAvgForm.get('discountAvgs').disable({emitEvent:false , onlySelf:true});
        this.catDiscountAvgForm.updateValueAndValidity()
    }
    lockForm() {
        this.catDiscountAvgForm.disable({emitEvent:false , onlySelf:true});

    }

    generateCatdiscountAverageObject() {
        this.catdiscountAverage.catActivitySectorId = this.idSector;
        this.catdiscountAverage.catNatureId = this.idNature;
        this.catdiscountAverage.catBrandId = this.idBrand;
        this.catdiscountAverage.isAnewAssets = this.isNew;
        this.catdiscountAverage.minumumValuationToCount = 3;

        if (this.catDiscountAvgForm) {

            let formValue = this.catDiscountAvgForm.getRawValue();
            this.catdiscountAverage.minumumValuationToCount = formValue.nbMinToConsider;

            var datePipe = new DatePipe("en-US");
            this.catdiscountAverage.startDateLabel = formValue.startDate && formValue.startDate != "" ? datePipe.transform(formatStringtoDate(formValue.startDate), 'dd/MM/yyyy h:mm a') : null;
            this.catdiscountAverage.endDateLabel = formValue.endDate && formValue.endDate != "" ? datePipe.transform(formatStringtoDate(formValue.endDate), 'dd/MM/yyyy h:mm a') : null
        }


    }

    isAddMode() {
        let path = this.route.url.split('/')[3];
        if (path == 'add') {
            return true;
        }
        else {
            return false;
        }
    }

    onTouched = () => {
        // This is intentional
    };

    onChange: any = () => {
        // This is intentional
    };


    get value() {
        return this.catDiscountAvgForm.getRawValue();
    }

    set value(value) {
        this.catDiscountAvgForm.setValue(value);
        this.onChange(value);
        this.onTouched();
    }


    get f() {
        return this.catDiscountAvgForm.controls;
    }


    writeValue(value: any) {
        window.setTimeout(() => {
            if (value) {
                this.catDiscountAvgForm.patchValue(value);
            }
        }, 100);
    }
    registerOnChange(fn: any) {
        this.subscriptions.push(this.catDiscountAvgForm.valueChanges.pipe(map(_ => this.catDiscountAvgForm.getRawValue())).subscribe(fn));

    }
    registerOnTouched(onTouched: any) {
        this.onTouched = onTouched;
    }

    ngOnDestroy() {
        this.subscriptions.forEach(s => s.unsubscribe());

    }
    validate(_: FormControl) {
        
        return this.catDiscountAvgForm.valid ? null : { average: { valid: false, }, };
      }

      setDisabledState(disabled: boolean) {

        if (disabled) {
          this.lockForm()
        }
        else {
          this.unlockForm();
        }
    
    
      }


}
