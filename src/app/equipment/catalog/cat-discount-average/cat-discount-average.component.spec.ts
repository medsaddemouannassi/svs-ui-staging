import { CatDiscountsAverageComponent } from "./cat-discount-average.component";
import { of, throwError } from 'rxjs';
import { FormBuilder } from '@angular/forms';
import { CatDiscountAverageProcessElementModel } from '../../../shared/models/cat-discount-average-process-element.model';
import { DiscountAverageResultModel } from '../../../shared/models/cat-discout-average-result.model';
import { CatDiscountAverageModel } from '../../../shared/models/cat-discount-average.model';

describe('CatDiscountsAverageComponent', () => {

    let fixture: CatDiscountsAverageComponent;
    let formBuilderMock: any;
    let decoteServiceMock: any;
    let routerMock: any;

    let catdiscountAverage = {
        id: '11',
        discountAverageValue: 90552,
        startDateLabel: '02/02/2005',
        endDateLabel:'02/12/2021',
        catActivitySectorId: '12',
        catNatureId: '1',
        catBrandId: '115',
        isAnewAssets: false,
        minumumValuationToCount: 5
    } as CatDiscountAverageProcessElementModel;


    beforeEach(() => {
        formBuilderMock = new FormBuilder();

        let result = {
            valuationsNumber: 3,
            discountAvgs: [
                { discountRateYear: 1, discountRateValue: '25' },
                { discountRateYear: 2, discountRateValue: '30' }
            ]
        } as CatDiscountAverageModel;

        decoteServiceMock = {
            getDiscountsAverage: jest.fn().mockReturnValue(of(result))
        }

        routerMock = {
            url: '/catalog/nature/447'
        }

        fixture = new CatDiscountsAverageComponent(formBuilderMock, decoteServiceMock, routerMock);

        fixture.idSector = '1';
        fixture.idNature = '2';
        fixture.ngOnInit();
    });

    describe('Test Component', () => {
        it('should Be Truthy', () => {
            expect(fixture).toBeTruthy();
        });
    });

    describe('Test: createForm', () => {
        it('should create form ', () => {
            fixture.createForm();
            expect(fixture.catDiscountAvgForm).toBeTruthy();
        });
    });

    describe('Test get f()', () => {
        it('should return form controls', () => {
            expect(fixture.f).toEqual(fixture.catDiscountAvgForm.controls);
        })
    });

    describe('Test: generateCatdiscountAverageobject', () => {
        it('should fill catDiscountAvgForm object', () => {
            fixture.catDiscountAvgForm = formBuilderMock.group({
                nbForms: { value: '', disabled: true },
                nbMinToConsider: [{ value: 3, disabled: true }],
                startDate: [{ value: '10/01/2006', disabled: true }],
                endDate: [{ value: '12/12/2021', disabled: true }],
                discountAvgs: null
            });
            fixture.idSector = '1';
            fixture.generateCatdiscountAverageObject();
            expect(fixture.catdiscountAverage.catActivitySectorId).toEqual('1');

            expect(fixture.catDiscountAvgForm.get('endDate').value).toEqual('12/12/2021');
        });
    })

    describe('Test: unlockForm', () => {
        it('should enable nbMinToConsider, startDate, endDate', () => {
            fixture.catDiscountAvgForm = formBuilderMock.group({
                nbForms: { value: '', disabled: true },
                nbMinToConsider: [{ value: 3, disabled: true }],
                startDate: [{ value: '10/01/2006', disabled: true }],
                endDate: [{ value: '12/12/2021', disabled: true }],
                discountAvgs: null
            });
            fixture.unlockForm();
            expect(fixture.catDiscountAvgForm.get('nbMinToConsider').enable).toBeTruthy();
            expect(fixture.catDiscountAvgForm.get('startDate').enable).toBeTruthy();
            expect(fixture.catDiscountAvgForm.get('endDate').enable).toBeTruthy();
        });
    });

    describe('Test: lockForm', () => {
        it('should lock startDate', () => {
            fixture.catDiscountAvgForm = formBuilderMock.group({
                nbForms: { value: '', disabled: false },
                nbMinToConsider: [{ value: 3, disabled: false }],
                startDate: [{ value: '10/01/2006', disabled: false }],
                endDate: [{ value: '12/12/2021', disabled: false }],
                discountAvgs: null
            });
            fixture.lockForm();
            expect(fixture.catDiscountAvgForm.get('nbMinToConsider').disable).toBeTruthy();
            expect(fixture.catDiscountAvgForm.get('startDate').disable).toBeTruthy();
            expect(fixture.catDiscountAvgForm.get('endDate').disable).toBeTruthy();
        });
    });

    describe('Test: initiateFormBuilder', () => {
        it('should disable nbForms', () => {
            fixture.catDiscountAverageList= [ {
                discountRateYear: 1,
                discountRateValue: '3'
            } as DiscountAverageResultModel];
            let resp = fixture.initiateAveragesListFormArray();
            expect(resp[0].get('discountRateValue').value).toEqual('3');
        });
    });


    describe('Test: loadCatDiscountAverageList', () => {
        it('should catDiscountAverageList index value of 0 be 25', () => {
            fixture.loadCatDiscountAverageList(catdiscountAverage);
            expect(fixture.catDiscountAverageList[0].discountRateValue).toEqual('25');
        });

        it('should decoteServiceMock throws error ', () => {
            spyOn(decoteServiceMock, 'getDiscountsAverage').and.returnValue(throwError({ status: 404 }));
            fixture.loadCatDiscountAverageList(catdiscountAverage);
        });
    });

    describe('Test: loadAvg', () => {
        it('should catDiscountAverageList index value of 0 be 25', () => {
            fixture.catDiscountAvgForm = formBuilderMock.group({
                nbForms: { value: '', disabled: false },
                nbMinToConsider: [{ value: 3, disabled: false }],
                startDate: [{ value: '10/01/2006', disabled: false }],
                endDate: [{ value: '12/12/2021', disabled: false }],
                discountAvgs: []
            });
            fixture.loadAvg();
            expect(fixture.catDiscountAverageList[0].discountRateValue).toEqual('25');
        });
    });

    describe('Test set isUnlock()', () => {

        it('should isUnlocked be true ', () => {
            let isUnlock = true;
            fixture.isUnlocked = isUnlock;
            expect(fixture.isUnlocked).toEqual(true);
        });
    });
    describe('Test registerOnChange', () => {
        it('should get register', () => {
          let test = (test) => { return test };
          let size = fixture.subscriptions.length;
          fixture.registerOnChange(test);
          expect(fixture.onChange).not.toEqual(() => {
            //this is intentional
          });
          expect(fixture.subscriptions.length).toEqual(size + 1);


        });
      });
      describe('Test writeValue', () => {
        it('should  write value', () => {
         let    value = {
                id: '11',
                discountAverageValue: 90552,
                startDateLabel: '02/02/2005',
                endDateLabel:'02/12/2021',
                catActivitySectorId: '12',
                catNatureId: '1',
                catBrandId: '115',
                isAnewAssets: false,
                minumumValuationToCount: 5
            } as CatDiscountAverageProcessElementModel;

          fixture.writeValue(value);

          expect(fixture.value).toBeDefined();



        });

      });
      describe('Test registerOnTouched', () => {
        it('should get register', () => {
          fixture.registerOnTouched(test);
          expect(fixture.onTouched).not.toEqual(() => {
          //this is intentional
          });
          expect(fixture.onTouched).toEqual(test);
        });

      });

      describe('Test setDisabledState', () => {
        it('should lock form', () => {
          fixture.setDisabledState(true);
          expect(fixture.lockForm).toHaveBeenCalled
        });

        it('should unlock form', () => {
            fixture.setDisabledState(false);
            expect(fixture.unlockForm).toHaveBeenCalled
          });

      });

      describe('Test isAddMode', () => {
        it('should return true when route url contains add', () => {
            routerMock.url = "/catalog/nature/add/124";
          expect(fixture.isAddMode()).toEqual(true);
        });
      });


});
