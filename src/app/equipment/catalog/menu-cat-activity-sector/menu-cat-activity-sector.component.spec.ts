
import { FormBuilder } from '@angular/forms';
import { of, Subject } from 'rxjs';
import { CatDocumentModel } from '../../../shared/models/cat-document.model';
import { MenuCatActivitySectorComponent } from './menu-cat-activity-sector.component';

let actSector = { id: '1', comment: '', label: 'Agricole', isActive: true, updatedDate: new Date(), createdDate: new Date(), createdBy: 'M0000', updatedBy: 'M0000' };
let documentsList: CatDocumentModel[] = [{

    "docName": "document1",
} as CatDocumentModel, {

    "docName": "document2"

} as CatDocumentModel];


describe('CatalogueComponent', () => {
    let fixture: MenuCatActivitySectorComponent;
    let formBuilderMock: FormBuilder;
    let catActivitySectorServiceMock: any;
    let catNatureServiceMock
    let popupServiceMock: any;

    let routeMock: any;
    let nodeServiceMock: any;
    let catDocumentServiceMock: any;

    beforeEach(() => {
        formBuilderMock = new FormBuilder();


        popupServiceMock = {
            popupInfo: jest.fn().mockReturnValue(of(true)),
            onConfirm: of(true)
        };
        routeMock = {
            params: of('1')

        }

        catActivitySectorServiceMock = {
            getActivitySectorById: jest.fn().mockReturnValue(of(actSector)),
            submitActivitySector: jest.fn().mockReturnValue(of(actSector)),
        }

        catNatureServiceMock = {
            getNatureNapBySectorIdList: jest.fn().mockReturnValue(of(
                ['code-nap-1', 'code-nap-2']

            ))
        }

        catDocumentServiceMock = {
            uploadDocument: jest.fn().mockReturnValue(of(documentsList)),
            searchDocument: jest.fn().mockReturnValue(of(documentsList)),
            downloadDocument: jest.fn().mockReturnValue(of(true)),
            deleteCatDocument: jest.fn().mockReturnValue(of(true)),
            updateDocument: jest.fn().mockReturnValue(of(documentsList)),

        }

        nodeServiceMock = {
            nodeSubject: new Subject<any>()

        }

        fixture = new MenuCatActivitySectorComponent(
            formBuilderMock,
            catActivitySectorServiceMock,
            catNatureServiceMock,
            routeMock,
            popupServiceMock,
            nodeServiceMock, catDocumentServiceMock
        );
        fixture.ngOnInit();
    });

    describe('Test Component', () => {
        it('should Be Truthy', () => {
            expect(fixture).toBeTruthy();
        });
    });

    describe('Test ngOnInit', () => {
        it('should ge activity sector and init disabled form', async () => {
            await routeMock.params;
            await catActivitySectorServiceMock.getActivitySectorById('1');
            await catNatureServiceMock.getNatureNapBySectorIdList('1');
            expect(fixture.activitySector).toEqual(actSector);
            expect(fixture.activitySectorDocs).toEqual(documentsList);
            expect(fixture.updatedActivitySectorDocs).toEqual(documentsList);


            expect(fixture.napOptions).toEqual(['code-nap-1', 'code-nap-2'])
            expect(fixture.sectorForm.get('generalities').get('libelle').disabled).toEqual(true);
            expect(fixture.sectorForm.get('generalities').get('comment').disabled).toEqual(true);
            expect(fixture.sectorForm.get('generalities').get('files').disabled).toEqual(true);
        });
    });

    describe('Test toggle modif state', () => {
        it('should enable form', () => {
            fixture.toggleModifState(true)

            expect(fixture.sectorForm.get('generalities').get('libelle').enabled).toEqual(true);
            expect(fixture.sectorForm.get('generalities').get('libelle').value).toEqual(fixture.activitySector.label);

            expect(fixture.sectorForm.get('generalities').get('comment').enabled).toEqual(true);
            expect(fixture.sectorForm.get('generalities').get('files').enabled).toEqual(true);
        });

        it('should disable form', async () => {
            fixture.toggleModifState(false)
            await popupServiceMock.popupInfo;
            await popupServiceMock.onConfirm;
            expect(popupServiceMock.popupInfo).toHaveBeenCalled()
            expect(fixture.sectorForm.get('generalities').get('libelle').disabled).toEqual(true);
            expect(fixture.sectorForm.get('generalities').get('comment').disabled).toEqual(true);
            expect(fixture.sectorForm.get('generalities').get('files').disabled).toEqual(true);
        });
    });

    describe('save', () => {
        it('should save form', async () => {
            fixture.toggleModifState(true);
            fixture.sectorForm = formBuilderMock.group({
                generalities: formBuilderMock.group({
                    libelle: [fixture.activitySector.label],
                    comment: [fixture.activitySector.comment],
                    files: [{ files: [{ name: 'dd' } as File] }],
                    napCodes: [null]
                })
            });
            let file = { 'lastModified': 23, 'name': 'fichier1' }
            fixture.sectorForm.get('generalities').get('files').patchValue({

                files: [file]

            })
            fixture.updatedActivitySectorDocsIndex = [1, 2];
            fixture.deletedActivitySectorDocs = [{

                "docName": "document1",
            } as CatDocumentModel, {

                "docName": "document2"

            } as CatDocumentModel];
            spyOn(fixture, 'updateSectorDocs').and.returnValue([]);

            fixture.save();
            await popupServiceMock.popupInfo
            await popupServiceMock.onConfirm
            expect(fixture.modifModeActivated).toBeFalsy();

        });

        it('should not save form', async () => {
            fixture.toggleModifState(true);

            fixture.sectorForm.get('generalities').reset();
            fixture.updatedActivitySectorDocsIndex = [1, 2];
            fixture.deletedActivitySectorDocs = [{

                "docName": "document1",
            } as CatDocumentModel, {

                "docName": "document2"

            } as CatDocumentModel];
            spyOn(fixture, 'updateSectorDocs').and.returnValue([]);

            fixture.save();
            await popupServiceMock.popupInfo
            await popupServiceMock.onConfirm
            expect(fixture.modifModeActivated).toBeTruthy();

        });


    });

    describe('Test:  onBlurDocName', () => {
        it('should update edited value of doc index ', () => {

            fixture.activitySectorDocs = documentsList;
            fixture.editedDocumentList = [false, true];

            fixture.onBlurDocument(1);
            expect(fixture.editedDocumentList[1]).toEqual(false);

        });

    });

    describe('Test:  updateSectorDocs', () => {
        it('should update edited value of doc index ', () => {

            fixture.toggleModifState(true);
            fixture.sectorForm = formBuilderMock.group({
                generalities: formBuilderMock.group({
                    libelle: [fixture.activitySector.label],
                    comment: [fixture.activitySector.comment],
                    files: [{ files: [{ name: 'dd' } as File] }],
                    napCodes: [null]
                })
            });
            let file = { 'lastModified': 23, 'name': 'fichier1' }
            fixture.sectorForm.get('generalities').get('files').patchValue({

                files: [file]

            })
            fixture.updatedActivitySectorDocsIndex = [1, 2];
            fixture.deletedActivitySectorDocs = [{

                "docName": "document1",
            } as CatDocumentModel, {

                "docName": "document2"

            } as CatDocumentModel];

            fixture.updateSectorDocs();

        });

    });

    describe('Test:  napsChange', () => {
        it('should reset code nap ', () => {

            fixture.napsChange();
            expect(fixture.sectorForm.get('generalities').get('napCodes').value).toBeNull();

        });

    });
    describe('Test:  editDocumentName', () => {
        it('should set doc edited value to true ', () => {
            fixture.activitySectorDocs = documentsList;
            fixture.editedDocumentList = [false, false];
            fixture.documentName = "test";
            fixture.editDocumentName(1);
            expect(fixture.editedDocumentList[1]).toEqual(true);

        });

    });


    describe('Test:  deleteDocs', () => {
        it('should remove docs ', async () => {

            fixture.toggleModifState(false)
            await popupServiceMock.popupInfo
            await popupServiceMock.onConfirm
            fixture.deleteDocument(1);

            expect(popupServiceMock.popupInfo).toHaveBeenCalled();
            popupServiceMock.onConfirm.subscribe((t) => {

                expect(fixture.deletedActivitySectorDocs.length).toEqual(1);
            })



        });

    });

    describe('Test:  download doc', () => {
        it('should download doc ', () => {
            let files = [{
                docName: 'file3.png', id: '1', catActivitySector: null, catNature: null, catBrand: null,
                catAssetType: null, discriminator: 'G', docVersion: 1, gedPath: '17/file3.png'
            } as CatDocumentModel,
            {
                docName: 'file2.png', id: '2', catActivitySector: null, catNature: null, catBrand: null,
                catAssetType: null, discriminator: 'G', docVersion: 1, gedPath: '17/file2.png'
            } as CatDocumentModel]


            fixture.activitySectorDocs = files;
            fixture.downloadDocument(0);

        });

    });

    describe('Test isFormValid', () => {

        it('should test validity on each tab ', () => {

            expect(fixture.isFormValid(0)).toEqual(fixture.sectorForm.controls.generalities.valid);
            expect(fixture.isFormValid(3)).toEqual(null);


        })
    });


});
