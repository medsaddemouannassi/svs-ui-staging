import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { TreeNode } from 'primeng/api/treenode';
import { forkJoin, Subscription, of } from 'rxjs';
import { ConfirmationModalComponent } from '../../../shared/confirmation-modal/confirmation-modal.component';
import { documentsValidator } from '../../../shared/documents/document-validator';
import { CatActivitySectorModel } from '../../../shared/models/cat-activity-sector.model';
import { CatDocumentModel } from '../../../shared/models/cat-document.model';
import { CatActivitySectorService } from '../../../shared/services/cat-activity-sector.service';
import { CatDocumentService } from '../../../shared/services/cat-document.service';
import { CatNatureService } from '../../../shared/services/cat-nature.service';
import { NodeService } from '../../../shared/services/node.service';
import { PopupService } from '../../../shared/services/popup.service';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'app-menu-cat-activity',
  templateUrl: './menu-cat-activity-sector.component.html',
  styleUrls: ['./menu-cat-activity-sector.component.css'],
  providers: [

  ]
})
export class MenuCatActivitySectorComponent implements OnInit {

  modifModeActivated = false;
  activitySector: CatActivitySectorModel;
  activitySectorDocs: CatDocumentModel[];
  updatedActivitySectorDocs: CatDocumentModel[];
  deletedActivitySectorDocs: CatDocumentModel[];

  updatedActivitySectorDocsIndex: number[];
  editedDocumentList: boolean[] = [];

  isSubmitted=false;
  sectorForm: FormGroup;
  subscriptions: Subscription[] = [];
  headersList: any[];
  selectedTabulation = 0;
  showForm = false
  napOptions: string[] = [];
  documentName: string;


  constructor(
    private fb: FormBuilder,
    private catActivitySectorService: CatActivitySectorService,
    private catNatureService: CatNatureService,
    private activatedRoute: ActivatedRoute,
    private popupService: PopupService,
    private nodeService: NodeService,
    private catDocumentService: CatDocumentService
  ) { }

  ngOnInit(): void {
    this.headersList = ['Généralités'];
    this.activatedRoute.params.subscribe(param => {
      this.initComponent(param);
      this.modifModeActivated = false;

    })

  }

  private initComponent(param) {
    this.showForm = false;

   

    forkJoin([
      this.catActivitySectorService.getActivitySectorById(param.id).pipe(catchError(error => of(null))),
      this.catNatureService.getNatureNapBySectorIdList(param.id).pipe(catchError(error => of([]))),
      //search sector natif docs
      this.catDocumentService.searchDocument(param.id, null, null, null, 'G').pipe(catchError(error => of([])))

    ])
      .subscribe(data => {
        this.activitySector = data[0];
        this.sectorForm = this.fb.group({
          generalities: this.newGeneralities(this.activitySector)
        });

        this.napOptions = data[1];
        this.activitySectorDocs = data[2];
        this.updatedActivitySectorDocs = [];
        this.updatedActivitySectorDocsIndex = [];
        this.deletedActivitySectorDocs = [];

        this.activitySectorDocs.forEach((element, index) => {

          this.updatedActivitySectorDocs.push(Object.assign({}, element))
          this.editedDocumentList[index] = false;
        });
        this.showForm = true;

      });
  }


  private newGeneralities(activitySector: CatActivitySectorModel): FormGroup {

    return this.fb.group({
      libelle: [{ value: activitySector ? activitySector.label : '', disabled: true }, Validators.required],
      comment: [{ value: activitySector ? activitySector.comment : '', disabled: true }],
      files: [{ value: null, disabled: true }, { validators: [documentsValidator] }],
      napCodes: [null]
    });

  }

  toggleModifState(isModif) {
    if (isModif) {
      this.modifModeActivated = isModif;

      this.sectorForm.get('generalities').get('libelle').enable();
      this.sectorForm.get('generalities').get('comment').enable();
      this.sectorForm.get('generalities').get('files').enable();

    } else {
      this.popupService.popupInfo(
        'Êtes-vous certain de vouloir annuler votre saisie ? </br> Les paramètres initiaux seront restaurés.', 'OUI', null, 'NON', ConfirmationModalComponent);
      const subscription = this.popupService.onConfirm.subscribe((t) => {
        if (t) {
          this.modifModeActivated = isModif;

          this.sectorForm.get('generalities').reset({
            libelle: this.activitySector ? this.activitySector.label : '',
            comment: this.activitySector ? this.activitySector.comment : '',
            files: { files: [] },
            napCodes: null
          });
          this.sectorForm.get('generalities').get('libelle').disable();
          this.sectorForm.get('generalities').get('comment').disable();
          this.sectorForm.get('generalities').get('files').disable();
          this.updatedActivitySectorDocs = [];
          this.updatedActivitySectorDocsIndex = [];
          this.deletedActivitySectorDocs = [];

          this.activitySectorDocs.forEach((element, index) => {

            this.updatedActivitySectorDocs.push(Object.assign({}, element))
            this.editedDocumentList[index] = false;
          });
          subscription.unsubscribe();
        }

      })
    }
  }

  save() {
    this.sectorForm.markAllAsTouched();
    this.isSubmitted=true;

    if (this.sectorForm.valid) {
      this.popupService.popupInfo(
        'Êtes-vous certain de vouloir enregistrer votre saisie ?  </br> Les paramètres initiaux seront remplacés.', 'OUI', null, 'NON', ConfirmationModalComponent);
      const subscription = this.popupService.onConfirm.subscribe((t) => {
        if (t) {
          this.activitySector.label = this.sectorForm.value.generalities.libelle;
          this.activitySector.comment = this.sectorForm.value.generalities.comment;


          let request = this.updateSectorDocs();

          request.push(this.catActivitySectorService.submitActivitySector(this.activitySector).pipe(catchError(error => of(null))))

          forkJoin(request).subscribe(res => {

            let node = {} as TreeNode;
            node.data = this.activitySector.id;
            node.label = this.activitySector.label;
            node.type = 'sector';

            this.nodeService.nodeSubject.next(node);

            this.modifModeActivated = false;
            this.initComponent({ id: this.activitySector.id })

          })


        }

        subscription.unsubscribe();
      });
      this.isSubmitted=false;

    }
    else{
      this.popupService.popupInfo(
        "Merci de bien vouloir renseigner l'intégralité des informations demandées.", null, null, null, ConfirmationModalComponent);
      const subscriptionPopup = this.popupService.onConfirm.subscribe((t) => {

        subscriptionPopup.unsubscribe();

      });


    }

  }
  updateSectorDocs() {
    let files: File[] = this.sectorForm.value.generalities.files.files;
    let request = [];

    if (files && files.length > 0) {
      files.forEach(file => request.push(this.catDocumentService.uploadDocument(file, this.activitySector.id,
        null, null, null, 'G').pipe(catchError(error => of(null)))));
    }

    if (this.updatedActivitySectorDocsIndex.length > 0) {
      let docs = []
      this.updatedActivitySectorDocsIndex.forEach(index => docs.push(this.updatedActivitySectorDocs[index]))
      docs.forEach(doc => request.push(this.catDocumentService.updateDocument(doc).pipe(catchError(error => of(null)))))
      /*
      request.push(this.catDocumentService.updateDocument(docs))
      */
    }
    if (this.deletedActivitySectorDocs.length > 0) {

      this.deletedActivitySectorDocs.forEach(docs => request.push(this.catDocumentService.deleteCatDocument(docs).pipe(catchError(error => of(null)))))
    }
    return request;

  }

  napsChange() {
    this.sectorForm.get('generalities').get('napCodes').reset()
  }

  deleteDocument(index: number) {

    this.popupService.popupInfo('Êtes-vous sûr(e) de vouloir supprimer la pièce jointe ?', 'Confirmer', null, 'Annuler', ConfirmationModalComponent);
    const subscription = this.popupService.onConfirm.subscribe((t) => {
      
      if(t){
      this.deletedActivitySectorDocs.push(this.updatedActivitySectorDocs[index])

      this.editedDocumentList.splice(index, 1);
      this.updatedActivitySectorDocs.splice(index, 1);
      let indexIndex = this.updatedActivitySectorDocsIndex.findIndex(val => val == index);
      if (indexIndex > 0) {
        this.updatedActivitySectorDocsIndex.splice(indexIndex, 1);
      }
    }
    subscription.unsubscribe()
    })

  }
  editDocumentName(index: number) {
    this.editedDocumentList[index] = true;
    this.documentName = this.updatedActivitySectorDocs[index].docName;

  }
  onBlurDocument(index: number) {

    this.updatedActivitySectorDocs[index].docName = this.documentName;
    if (this.updatedActivitySectorDocsIndex.findIndex(val => val == index) == -1) {
      this.updatedActivitySectorDocsIndex.push(index);
    }
    this.editedDocumentList[index] = false;
  }

  downloadDocument(index: number) {
    this.catDocumentService.downloadDocument(this.activitySectorDocs[index].gedPath, this.activitySectorDocs[index].docName).subscribe(response => {
      var file = new Blob([response], { type: 'application/*' })
      var fileURL = URL.createObjectURL(file);
      const doc = document.createElement('a');
      doc.download = this.activitySectorDocs[index].docName;
      doc.href = fileURL;
      doc.dataset.downloadurl = [doc.download, doc.href].join(':');
      document.body.appendChild(doc);
      doc.click();

    });
  }
  isFormValid(index) {

    if(index.toString() == '0'){
      return this.sectorForm.controls.generalities.valid;

    }
    return null;

 
  }
}
