import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { of, Subject, throwError } from 'rxjs';
import { CatDiscountRateModel } from '../../../shared/models/cat-discount-rate.model';
import { CatDiscountModel } from '../../../shared/models/cat-discount.model';
import { CatDocumentModel } from '../../../shared/models/cat-document.model';
import { CatNatureModel } from '../../../shared/models/cat-nature.model';
import { ParamNapModel } from '../../../shared/models/param-nap.model';
import { ValuationModel } from '../../../shared/models/valuation.model';
import { CatalogNewDiscountsComponent } from '../catalog-new-discounts/catalog-new-discounts.component';
import { CatalogOldDiscountsComponent } from '../catalog-old-discounts/catalog-old-discounts.component';
import { MenuCatNatureComponent } from "./menu-cat-nature.component";
import { FullCatDiscountModel } from '../../../shared/models/full-cat-discount.model';
import { CatalogEnergyComponent } from '../catalog-energy/catalog-energy.component';
import { ParamEnergyModel } from 'src/app/shared/models/param-energy.model';

describe('MenuCatNatureComponent', () => {
  let actSector = { id: '1', comment: '', label: 'Agricole', isActive: true, updatedDate: new Date(), createdDate: new Date(), createdBy: 'M0000', updatedBy: 'M0000' };

  let fixture: MenuCatNatureComponent;
  let routeMock: any;
  let valuationServiceMock: any
  let popupServiceMock: any;
  let catNatureServiceMock: any;
  let formBuilderMock: any;
  let catDiscountServiceMock: any;
  let valuationList: ValuationModel[];
  let catNature: CatNatureModel;
  let catDiscountObject: CatDiscountModel;
  let nodeServiceMock: any;
  let paramNap: ParamNapModel;
  let catDiscountRateServiceMock: any;
  let catDocumentServiceMock: any;
  let routerMock: any;
  let paramNapServiceMock: any;
  let catActivitySectorServiceMock: any;
  let catalogServiceMock: any;
  let catEnergyServiceMock: any;
  let paramEnergyServiceMock: any;
  let paramNapList: ParamNapModel[] = [];
  let catDiscountRateList: CatDiscountRateModel[];

  let documentsList: CatDocumentModel[] = [{

    "docName": "document1",
  } as CatDocumentModel, {

    "docName": "document2"

  } as CatDocumentModel];

  beforeEach(() => {

    catActivitySectorServiceMock = {
      getActivitySectorById: jest.fn().mockReturnValue(of(actSector))
    }

    catalogServiceMock = {
      fillInfoCreationUser: jest.fn().mockReturnValue(of("Batch"))
    }

    catDiscountRateList = [{
      id: "1",
      catDiscount: catDiscountObject,
      discountRateYear: 2012,
      discountRateValue: 12
    }, {
      id: "2",
      catDiscount: null,
      discountRateYear: 2012,
      discountRateValue: 12
    },
    {
      id: "3",
      catDiscount: null,
      discountRateYear: 2012,
      discountRateValue: 20
    },
    {
      id: "4",
      catDiscount: null,
      discountRateYear: 2012,
      discountRateValue: 5
    }
    ];

    paramNapList = [
      {
        "id": "1",
        "label": "label"
      }
    ]

    valuationList = [
      {
        "id": "1",
        "discountRateComment": "comment",
        "experId": "1",
        "controllerId": "1",
        "validatorId": "1",
        "catNature": null,
        "paramValidationType": null,
        'catBrand': null,
        "catAssetType": null,
        "valuationRequest": null
        , "catActivitySector": null,
        "createdDate": null,
        "descNetwork": null,
        "descName": null,
        "isSubmitted": null,
        "isDuplicated": null

      },
      {
        "id": "2",
        "discountRateComment": "comment",
        "experId": "1",
        "controllerId": "1",
        "validatorId": "1",
        "catNature": null,
        "paramValidationType": null,
        'catBrand': null,
        "catAssetType": null,
        "valuationRequest": null
        , "catActivitySector": null,
        "createdDate": null,
        "descNetwork": null,
        "descName": null,
        "isSubmitted": null,
        "isDuplicated": null


      }
    ];

    paramNap = {
      id: '1',
      label: 'label'
    }
    catNature = {
      id: '133',
      label: 'nature label',
      catActivitySector: null,
      comment: 'comment nature',
      paramNap: paramNap,
      isActive: true,
      isValid: false,
      isEnrg: false
    }
    catNature.catActivitySector = {
      id: '1',
      label: 'Transport',
      comment: 'comment act sector',
      isActive: true
    }

    catDiscountObject = {
      id: '66',
      maxFinPer: 52,
      minFinPer: 33,
      discount: 25,
      catBrand: null,
      catNature: null,
      catActivitySector: null,
      isPerm: null,
      activationDate: null,
      endDate: null,
      catAssetType: null,
      enableRecov: null,
      enableValuation: null,
      ponderationRate: null,
      creation: '20',
      lastValuationDone: null,
      isReadjust: false,
      isNew: true,
      comment: 'comment cat discount test',
      startDateAverLabel: '',
      endDateAverLabel: '',
      nbMinValAver: 3
    }

    popupServiceMock = {
      popupInfo: jest.fn().mockReturnValue(of(true)),
      onConfirm: of(true)
    };

    paramNapServiceMock = {
      getAllParamNAPListByQuery: jest.fn().mockReturnValue(of(paramNapList))
    }
    routeMock = {
      params: of('1')

    }
    routerMock = {
      navigate: jest.fn(),
      url: '/catalog/brand/9158'
    };
    catDiscountRateServiceMock = {
      getCatDiscountRateByCatDiscountId: jest.fn().mockReturnValue(of(catDiscountRateList))
    }

    formBuilderMock = new FormBuilder();
    valuationServiceMock = {
      getLast10ValuationByNatureId: jest.fn().mockReturnValue(of(valuationList))
    }
    catNatureServiceMock = {
      getCatNatureById: jest.fn().mockReturnValue(of(catNature)),
      saveCatNature: jest.fn().mockReturnValue(of(catNature)),
    }


    let fullCatDiscount = {
      catDiscount: catDiscountObject,
      catDiscountRateList: [{

      } as CatDiscountRateModel]
    } as FullCatDiscountModel;

    catDiscountServiceMock = {
      saveDiscount: jest.fn().mockReturnValue(of(catDiscountObject)),
      saveCatDiscountAndRate: jest.fn().mockReturnValue(of(fullCatDiscount)),
      getDiscountDetailsByTypeIdOrBrandIdOrNatureIdOrSectorId: jest.fn().mockReturnValue(of(catDiscountObject)),

    }

    catDocumentServiceMock = {
      uploadDocument: jest.fn().mockReturnValue(of(documentsList)),
      searchDocument: jest.fn().mockReturnValue(of(documentsList)),
      downloadDocument: jest.fn().mockReturnValue(of(true)),
      deleteCatDocument: jest.fn().mockReturnValue(of(true)),
      updateDocument: jest.fn().mockReturnValue(of(true)),
      searchInheratedDocuments: jest.fn().mockReturnValue(of(documentsList))
    }

    nodeServiceMock = {
      nodeSubject: new Subject<any>()

    }

    fixture = new MenuCatNatureComponent(
      formBuilderMock,
      popupServiceMock,
      catNatureServiceMock,
      routeMock,
      valuationServiceMock,
      catDiscountServiceMock,
      nodeServiceMock,
      catDiscountRateServiceMock,
      catDocumentServiceMock,
      routerMock,
      paramNapServiceMock,
      catActivitySectorServiceMock,
      catalogServiceMock

    );
    fixture.ngOnInit();


  });


  describe('Test MenuCatBrandComponent', () => {
    it('should Be Truthy', () => {
      expect(fixture).toBeTruthy();
    })
  });

  describe('Test initComponent', () => {
    it('should get inherited decote', () => {
      fixture.isAddMode = true;
      fixture.initComponent('5');
      expect(fixture.catDiscountObject.id).toBeTruthy();


    })

    it('should not get inherited decote', () => {
      catDiscountServiceMock = {
        saveDiscount: jest.fn().mockReturnValue(of(null)),
        saveCatDiscountAndRate: jest.fn().mockReturnValue(of(null)),
        getDiscountDetailsByTypeIdOrBrandIdOrNatureIdOrSectorId: jest.fn().mockReturnValue(of(null)),

      }
      fixture = new MenuCatNatureComponent(
        formBuilderMock,
        popupServiceMock,
        catNatureServiceMock,
        routeMock,
        valuationServiceMock,
        catDiscountServiceMock,
        nodeServiceMock,
        catDiscountRateServiceMock,
        catDocumentServiceMock,
        routerMock,
        paramNapServiceMock,
        catActivitySectorServiceMock,
        catalogServiceMock

      );
      fixture.isAddMode = true;
      fixture.initComponent('5');
      expect(fixture.catDiscountObject.id).toBeFalsy();


    })


  });



  describe('Test: ngOnInit', () => {

    it('should variables be setted ', () => {
      expect(fixture.natureTitle).toEqual(fixture.catNature.label);
      expect(fixture.sectorTitle).toEqual(fixture.catNature.catActivitySector.label);

    });

    it('should catNatureServiceMock throws error ', () => {
      spyOn(catNatureServiceMock, 'getCatNatureById').and.returnValue(throwError({ status: 404 }));
      fixture.ngOnInit();
    });

    it('should valuationServiceMock throws error ', () => {
      spyOn(valuationServiceMock, 'getLast10ValuationByNatureId').and.returnValue(throwError({ status: 404 }));
      fixture.ngOnInit();
    });

  });


  describe('Test editBrand()', () => {
    it('should modifModeActivatedNew be true', () => {
      fixture.editNature();
      expect(fixture.modifModeActivatedNew).toEqual(true);
    })
  });
  describe('Test undo()', () => {
    it('should navigate', () => {
      fixture.isAddMode = true
      fixture.undo();
      expect(popupServiceMock.popupInfo).toHaveBeenCalled();
    })
    it('should call popup', () => {
      fixture.undo();
      expect(popupServiceMock.popupInfo).toHaveBeenCalled();
    })
    it('should update form', () => {
      fixture.catalogOldDiscountsComponent = new CatalogOldDiscountsComponent(formBuilderMock, popupServiceMock, catDiscountServiceMock, catDiscountRateServiceMock
        , catDocumentServiceMock, routerMock);
      fixture.catalogNewDiscountsComponent = new CatalogNewDiscountsComponent(formBuilderMock, popupServiceMock, catDiscountServiceMock, catDocumentServiceMock);
      spyOn(fixture.catalogOldDiscountsComponent, 'undo').and.returnValue(of(true));
      spyOn(fixture.catalogNewDiscountsComponent, 'undo').and.returnValue(of(true));

      fixture.undo();
      expect(popupServiceMock.popupInfo).toHaveBeenCalled();
    })
  });

  describe('Test changeTab()', () => {
    it('should be 3', () => {
      fixture.changeTab(3);
      expect(fixture.selectedPlan).toEqual(3);
    })
  });
  describe('Test updateCatBrandObject()', () => {
    it('should change values of variables', () => {
      fixture.formMenu.controls['generalities'].patchValue({
        label: 'MyNaturelabel',
        comment: 'MybrandComment',
        dureeMin: 24,
        dureeMax: 12,
        seuilVr: 52,
        creation: '21'
      });
      fixture.catNature = {
        id: '133',
        label: 'nature label',
        catActivitySector: null,
        comment: 'comment nature',
        paramNap: paramNap,
        isActive: true,
        isValid: false,
        isEnrg: false

      }
      fixture.catDiscountObject = {
        id: '66',
        maxFinPer: 52,
        minFinPer: 33,
        discount: 25,
        catBrand: null,
        catNature: null,
        catActivitySector: null,
        isPerm: null,
        activationDate: null,
        endDate: null,
        catAssetType: null,
        enableRecov: null,
        enableValuation: null,
        ponderationRate: null,
        creation: '20',
        lastValuationDone: null,
        isReadjust: false,
        isNew: true,
        comment: 'comment cat discount test',
        startDateAverLabel: '',
        endDateAverLabel: '',
        nbMinValAver: 3
      }
      fixture.updateNatureInfo();
      expect(fixture.catNature.label).toEqual(fixture.formMenu.controls['generalities'].get('label').value);
      expect(fixture.catNature.comment).toEqual(fixture.formMenu.controls['generalities'].get('comment').value);
    })
  });

  describe('Test saveNature()', () => {
    let engPopupServiceMock: any;
    let engFormBuilderMock: any;
    let engCatEnergyServiceMock: any;
    let engParamEnergyServiceMock: any;
    let engRouterMock: any;

    engFormBuilderMock = new FormBuilder();

    engCatEnergyServiceMock = {
      saveCatEnergy: jest.fn().mockReturnValue(of(true)),
      deleteCatEnergy: jest.fn().mockReturnValue(of(true)),
      getCatEnergyByNatureId: jest.fn().mockReturnValue(of(true))
    };
    engPopupServiceMock = {
      popupInfo: jest.fn().mockReturnValue(of(true)),
      onConfirm: of(true)
    };

    let unusedList = [{
      id: '1',
      label: 'Biocarburant',
    } as ParamEnergyModel,
    {
      id: '2',
      label: 'Diesel',
    } as ParamEnergyModel];

    engParamEnergyServiceMock = {
      getUnusedParamEnergy: jest.fn().mockReturnValue(of(unusedList))
    };
    beforeEach(() => {
      fixture.catalogEnergyComponent = new CatalogEnergyComponent(
        engFormBuilderMock,
        engCatEnergyServiceMock,
        engParamEnergyServiceMock,
        engPopupServiceMock, engRouterMock);
        fixture.catalogEnergyComponent.mainForm =  new FormGroup({});
    })
    it('should call popup', () => {
      fixture.saveNature();
      expect(popupServiceMock.popupInfo).toHaveBeenCalled();
    });
    it('should saveNature throws error ', () => {
      spyOn(catNatureServiceMock, 'saveCatNature').and.returnValue(throwError({ status: 404 }));
      fixture.saveNature();
    });
    it('should call  saveCatDiscountAndRate ', () => {

      fixture.updatedNatureDocsIndex = [0, 1];
      fixture.catDiscountObject.id = null
      fixture.catDiscountObject.endDate = new Date()
      fixture.formMenu = formBuilderMock.group({
        generalities: formBuilderMock.group({
          libelle: [fixture.catNature.label],
          comment: [fixture.catNature.comment],
          dureeMin: [24],
          dureeMax: [25],
          seuilVr: [5],
          creation: [5],
          decoteNew: [{ catDiscountRateList: [{ discountRateYear: 1, discountRateValue: 5 } as CatDiscountRateModel] }],
          files: [{ files: [{ name: 'dd' } as File] }],
        }),
        decoteNew: [{ catDiscountRateList: [] }],
        decoteOld: [{ catDiscountRateOldList: [] }]
      });

      fixture.deletedNatureDocs = [{
        docName: 'file3.png', id: '1', catActivitySector: null, catNature: null, catBrand: null,
        catAssetType: null, discriminator: 'G', docVersion: 1, gedPath: '17/file3.png'
      } as CatDocumentModel]
      fixture.saveNature();
      expect(catDiscountServiceMock.saveCatDiscountAndRate).toHaveBeenCalled;
    });

    it('should call  saveCatDiscountAndRate when isAddMode true ', () => {

      fixture.isAddMode = true;

      fixture.saveNature();
      expect(catDiscountServiceMock.saveCatDiscountAndRate).toHaveBeenCalled;
    });

  });
  describe('Test generateCatDiscount()', () => {
    it('should return form value', () => {
      let decoteForm = new FormControl({
        isReadjust: null,
        isPerm: false,
        temporary: '',
        startingYear1: '',
        startingNumber: '',
        percentage: null,
        startingYear2: '',
        startingNumber2: null,
        comment: 'comment'
      });
      fixture.catDiscountObject = {
        id: '66',
        maxFinPer: 52,
        minFinPer: 33,
        discount: 25,
        catBrand: null,
        catNature: null,
        catActivitySector: null,
        isPerm: null,
        activationDate: null,
        endDate: null,
        catAssetType: null,
        enableRecov: null,
        enableValuation: null,
        ponderationRate: null,
        creation: '20',
        lastValuationDone: null,
        isReadjust: true,
        isNew: true,
        comment: 'comment cat discount test',
        startDateAverLabel: '',
        endDateAverLabel: '',
        nbMinValAver: 3
      }
      let avgdecoteForm = new FormControl({

      })
      fixture.generateCatDiscount(decoteForm, avgdecoteForm);
      expect(fixture.catDiscountObject.isReadjust).toEqual(false);
    });

  });


  describe('Test changeNewDiscount', () => {

    it('should set rateNewDiscountFirstYear ', () => {

      let test = 2;

      fixture.changeNewDiscount(test);

      expect(fixture.rateNewDiscountFirstYear).toEqual(test);

    })

  });



  describe('Test arraysEqual', () => {

    it('should return false when size is not equal', () => {

      let arrayOne = [1, 2];

      let arraytwo = [1];



      fixture.changeNewDiscount(test);

      expect(fixture.arraysEqual(arrayOne, arraytwo)).toEqual(false);

    })

    it('should return false when array is not equal', () => {

      let arrayOne = [2];

      let arraytwo = [1];



      fixture.changeNewDiscount(test);

      expect(fixture.arraysEqual(arrayOne, arraytwo)).toEqual(false);

    })

    it('should return true when array is equal', () => {

      let arrayOne = [1];

      let arraytwo = [1];



      fixture.changeNewDiscount(test);

      expect(fixture.arraysEqual(arrayOne, arraytwo)).toEqual(true);

    })

  });

  describe('Test generateCatOldDiscount', () => {

    it('should return changed comment', () => {
      let decoteForm = {
        isReadjust: true,
        isPerm: true,
        comment: 'comment de test',
        ponderationRate: '10',
        maxFinPer: '70',
        endDate: '07/02/2022'
      }
      let avgdecoteForm = new FormControl({

      })
      fixture.generateCatOldDiscount(decoteForm, avgdecoteForm);
      expect(fixture.catOldDiscountObject.comment).toEqual(decoteForm.comment);
    });

    it('should return ponderation is equal to 50 and maxfinPer is equal to 72', () => {
      let decoteForm = {
        isReadjust: true,
        isPerm: true,
        comment: 'comment de test',
        ponderationRate: null,
        maxFinPer: null,
        endDate: '07/02/2022'
      }
      fixture.catDiscountObject.maxFinPer = null;
      let avgdecoteForm = new FormControl({

      })
      fixture.generateCatOldDiscount(decoteForm, avgdecoteForm);
      expect(fixture.catOldDiscountObject.ponderationRate).toEqual(50);
      expect(fixture.catOldDiscountObject.maxFinPer).toEqual(72);
    });

    it('should return ponderation is equal to 50 and maxfinPer is equal to 33', () => {
      let decoteForm = {
        isReadjust: true,
        isPerm: true,
        comment: 'comment de test',
        ponderationRate: null,
        maxFinPer: null,
        endDate: '07/02/2022'
      }
      let avgdecoteForm = new FormControl({

      })
      fixture.catDiscountObject.maxFinPer = 33;
      fixture.generateCatOldDiscount(decoteForm, avgdecoteForm);
      expect(fixture.catOldDiscountObject.ponderationRate).toEqual(50);
      expect(fixture.catOldDiscountObject.maxFinPer).toEqual(33);
    });

    it('should return endDate is equal to  9999-12-31', () => {
      let decoteForm = {
        isReadjust: true,
        isPerm: true,
        comment: 'comment de test',
        ponderationRate: '10',
        maxFinPer: '70',
        endDate: null
      }
      let avgdecoteForm = new FormControl({

      })
      fixture.generateCatOldDiscount(decoteForm, avgdecoteForm);
      expect(fixture.catOldDiscountObject.endDate).toEqual(new Date('9999-12-31'));
    });

  });

  describe('Test: loadNatureData', () => {

    it('should catNatureServce throws error ', () => {
      spyOn(catNatureServiceMock, 'getCatNatureById').and.returnValue(throwError({ status: 404 }));
      fixture.loadNatureData();
    });
  });

  describe('Test:  download doc', () => {
    it('should download doc ', () => {
      let documentToDownload = {
        docName: 'file3.png', id: '1', catActivitySector: null, catNature: null, catBrand: null,
        catAssetType: null, discriminator: 'G', docVersion: 1, gedPath: '17/file3.png'
      } as CatDocumentModel


      fixture.downloadDocument(documentToDownload);

    });

  });

  describe('Test:  onBlurDocName', () => {
    it('should update edited value of doc index ', () => {

      fixture.natureDocs = documentsList;
      fixture.editedDocumentList = [false, true];

      fixture.onBlurDocument(1);
      expect(fixture.editedDocumentList[1]).toEqual(false);

    });

  });

  describe('Test:  editDocumentName', () => {
    it('should set doc edited value to true ', () => {
      fixture.natureDocs = documentsList;
      fixture.editedDocumentList = [false, false];
      fixture.documentName = "test";
      fixture.editDocumentName(1);
      expect(fixture.editedDocumentList[1]).toEqual(true);

    });

  });


  describe('Test:  deleteDocs', () => {
    it('should remove docs ', () => {
      fixture.deleteDocument(1);

      fixture.updatedNatureDocsIndex = [0, 1];
      expect(popupServiceMock.popupInfo).toHaveBeenCalled();
      popupServiceMock.onConfirm.subscribe((t) => {

        expect(fixture.deletedNatureDocs.length).toEqual(1);
      })



    });

  });


  describe('Test:  updateGeneralitiesDocs', () => {
    it('should updateGeneralitiesDocs ', () => {
      fixture.updatedNatureDocsIndex = [0, 1];
      fixture.formMenu = formBuilderMock.group({
        generalities: formBuilderMock.group({
          libelle: [fixture.catNature.label],
          comment: [fixture.catNature.comment],
          files: [{ files: [{ name: 'dd' } as File] }],
        })
      });

      fixture.deletedNatureDocs = [{
        docName: 'file3.png', id: '1', catActivitySector: null, catNature: null, catBrand: null,
        catAssetType: null, discriminator: 'G', docVersion: 1, gedPath: '17/file3.png'
      } as CatDocumentModel]
      fixture.updateGeneralitiesDocs();

    });

  });

  describe('Test searchNatureNativeDocs()', () => {

    it('should searchNatureNativeDocs throws error ', () => {
      spyOn(catDocumentServiceMock, 'searchDocument').and.returnValue(throwError({ status: 404 }));
      fixture.searchNatureNativeDocs();
    });


  });

  describe('Test editNatureToAdd()', () => {

    it('should  nothing do ', () => {
      fixture.isPageFirstLoaded = false;

      fixture.isAddMode = false;
      fixture.editNatureToAdd();
      expect(fixture.isPageFirstLoaded).toEqual(false);

    });

    it('should set isPageFirstLoaded true', () => {
      fixture.isPageFirstLoaded = false;

      fixture.isAddMode = true;
      fixture.editNatureToAdd();
      expect(fixture.isPageFirstLoaded).toEqual(true);

    });

    it('should subscribe to nex decote', () => {
      fixture.isPageFirstLoaded = false;

      fixture.isAddMode = true;
      fixture.catDiscountObject.id = null
      fixture.editNatureToAdd();
      expect(fixture.isPageFirstLoaded).toEqual(true);

      fixture.formMenu.controls['decoteNew'].patchValue(null)
      fixture.formMenu.controls['decoteNew'].setValidators(Validators.required)
      expect(fixture.isNewValid).toEqual(false);


    });

    it('should subscribe to nex decote', () => {
      fixture.isPageFirstLoaded = false;

      fixture.isAddMode = true;
      fixture.catDiscountObject.id = null
      fixture.editNatureToAdd();
      expect(fixture.isPageFirstLoaded).toEqual(true);

      fixture.formMenu.controls['decoteNew'].patchValue({ catDiscountRateList: [{}] })
      fixture.formMenu.controls['decoteNew'].setValidators(Validators.required)
      expect(fixture.isNewValid).toEqual(true);


    });

    it('should subscribe to nex decote 2', () => {
      fixture.isPageFirstLoaded = false;

      fixture.isAddMode = true;
      fixture.catDiscountObject.id = null
      fixture.editNatureToAdd();
      expect(fixture.isPageFirstLoaded).toEqual(true);
      fixture.formMenu.controls['decoteNew'].setValidators(Validators.required)

      fixture.formMenu.controls['decoteNew'].patchValue(null)
      fixture.formMenu.updateValueAndValidity()
      expect(fixture.isNewValid).toEqual(false);


    });


  });

  describe('Test changeCodeNapeList', () => {

    it('should get codeNapList ', () => {
      fixture.changeCodeNapeList(null);
      expect(fixture.codeNapList).toEqual(paramNapList)
    });


  });

  describe('Test changeCodeNapeListChange', () => {

    it('should get codeNapList ', () => {
      fixture.isChange = false;
      fixture.changeCodeNapeListChange(null);
      expect(fixture.codeNapList).toEqual(paramNapList)

    });
  });

  describe('Test changeCodeNapeListFocus', () => {

    it('should get codeNapList ', () => {
      fixture.isChange = false;
      fixture.changeCodeNapeListFocus();
      expect(fixture.codeNapList).toEqual(paramNapList)

    });
  });

  describe('Test getOldDiscountDetails', () => {

    it('should get catOldDiscountObject ', () => {
      fixture.getOldDiscountDetails();
      expect(fixture.catOldDiscountObject).toEqual(catDiscountObject)

    });
  });

  describe('Test loadDiscountRateOldListByCatDiscountId', () => {

    it('should get catDiscountRateOldList ', () => {
      fixture.loadDiscountRateOldListByCatDiscountId('5');
      expect(fixture.catDiscountRateOldList).toEqual(catDiscountRateList)

    });
  });


  describe('Test loadDiscountRateListByCatDiscountId', () => {

    it('should get catDiscountRateList ', () => {
      fixture.loadDiscountRateListByCatDiscountId('5');
      expect(fixture.catDiscountRateList).toEqual(catDiscountRateList)

    });
  });

  describe('Test isFormValid', () => {

    it('should test validity on each tab ', () => {

      expect(fixture.isFormValid(0)).toEqual(fixture.formMenu.controls.generalities.valid);
      expect(fixture.isFormValid(1)).toEqual(fixture.formMenu.controls.decoteNew.valid);
      expect(fixture.isFormValid(2)).toEqual(fixture.formMenu.controls.decoteOld.valid);
      expect(fixture.isFormValid(3)).toEqual(null);


    })
  });


  describe('Test navigate ', () => {

    it('should call router navigate  ', () => {

      fixture.navigate("215");
      expect(routerMock.navigate.toHaveBeenCalled)


    })
  });


  describe('Test lockEnergyComponent', () => {

    it('should change displayOptionsSelect of catalogEnergyComponent to false ', () => {
      fixture.isAddMode = true;
      let engPopupServiceMock: any;
      let engFormBuilderMock: any;
      let engCatEnergyServiceMock: any;
      let engParamEnergyServiceMock: any;
      let engRouterMock: any;

      engRouterMock = {
        url: '/catalog/nature/447'
      }

      engFormBuilderMock = new FormBuilder();

      engCatEnergyServiceMock = {
        saveCatEnergy: jest.fn().mockReturnValue(of(true)),
        deleteCatEnergy: jest.fn().mockReturnValue(of(true)),
        getCatEnergyByNatureId: jest.fn().mockReturnValue(of(true))
      };
      engPopupServiceMock = {
        popupInfo: jest.fn().mockReturnValue(of(true)),
        onConfirm: of(true)
      };

      let unusedList = [{
        id: '1',
        label: 'Biocarburant',
      } as ParamEnergyModel,
      {
        id: '2',
        label: 'Diesel',
      } as ParamEnergyModel];

      engParamEnergyServiceMock = {
        getNatureUnusedParamEnergy: jest.fn().mockReturnValue(of(unusedList)),
        getBrandUnusedParamEnergy: jest.fn().mockReturnValue(of(unusedList))

      };
      fixture.catalogEnergyComponent = new CatalogEnergyComponent(
        engFormBuilderMock,
        engCatEnergyServiceMock,
        engParamEnergyServiceMock,
        engPopupServiceMock, engRouterMock);
      fixture.catalogEnergyComponent.ngOnInit();
      fixture.catalogEnergyComponent.displayOptionsSelect = true;
      fixture.lockEnergyComponent();
      expect(fixture.catalogEnergyComponent.displayOptionsSelect).toEqual(false);

    });


  });
});
