import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TreeNode } from 'primeng/api/treenode';
import { forkJoin, of } from 'rxjs';
import { ValuationModel } from 'src/app/shared/models/valuation.model';
import { ValuationService } from 'src/app/shared/services/valuation.service';
import { ConfirmationModalComponent } from '../../../shared/confirmation-modal/confirmation-modal.component';
import { documentsValidator } from '../../../shared/documents/document-validator';
import { CatActivitySectorModel } from '../../../shared/models/cat-activity-sector.model';
import { CatDiscountRateModel } from '../../../shared/models/cat-discount-rate.model';
import { CatDiscountModel } from '../../../shared/models/cat-discount.model';
import { CatDocumentModel } from '../../../shared/models/cat-document.model';
import { CatNatureModel } from '../../../shared/models/cat-nature.model';
import { FullCatDiscountModel } from '../../../shared/models/full-cat-discount.model';
import { ParamNapModel } from '../../../shared/models/param-nap.model';
import { CatActivitySectorService } from '../../../shared/services/cat-activity-sector.service';
import { CatDiscountRateService } from '../../../shared/services/cat-discount-rate.service';
import { CatDiscountService } from '../../../shared/services/cat-discount.service';
import { CatDocumentService } from '../../../shared/services/cat-document.service';
import { CatNatureService } from '../../../shared/services/cat-nature.service';
import { NodeService } from '../../../shared/services/node.service';
import { ParamNapService } from '../../../shared/services/param-nap.service';
import { PopupService } from '../../../shared/services/popup.service';
import { CatalogNewDiscountsComponent } from '../catalog-new-discounts/catalog-new-discounts.component';
import { CatalogOldDiscountsComponent } from '../catalog-old-discounts/catalog-old-discounts.component';
import { catchError } from 'rxjs/operators';
import { CatalogEnergyComponent } from '../catalog-energy/catalog-energy.component';
import { CatalogService } from '../../../shared/services/catalog.service';

@Component({
  selector: 'app-menu-cat-nature',
  templateUrl: './menu-cat-nature.component.html',
  styleUrls: ['./menu-cat-nature.component.css'],
  providers: [

  ]
})
export class MenuCatNatureComponent implements OnInit {
  @ViewChild(CatalogNewDiscountsComponent) catalogNewDiscountsComponent: CatalogNewDiscountsComponent;
  @ViewChild(CatalogOldDiscountsComponent) catalogOldDiscountsComponent: CatalogOldDiscountsComponent;
  @ViewChild(CatalogEnergyComponent) catalogEnergyComponent: CatalogEnergyComponent;

  isAddMode = false;
  isPageFirstLoaded = false;
  isNewValid = false;
  headersList: any[];
  selectedPlan = 0;
  formMenu: FormGroup;
  generalities: FormGroup;

  decoteNew: FormGroup;
  decoteOld: FormGroup;
  cotationsList: any[];
  modifModeActivatedGeneralite = false;
  modifModeActivatedNew = false;
  modifModeActivatedOld = false;
  modifModeEnergy = false;

  loadNewDiscount = false;
  loadOldDiscount = false;
  rateNewDiscountFirstYear = -1;
  sectorTitle: string;
  natureTitle: string;
  natureId: string;
  isNew = true;
  activitySector: CatActivitySectorModel;
  catNature: CatNatureModel;
  catDiscountObject: CatDiscountModel;
  catOldDiscountObject: CatDiscountModel;

  initialCatDiscountObject: CatDiscountModel;
  initialCatOldDiscountObject: CatDiscountModel;

  valuationsList: ValuationModel[];
  catDiscountRateList: CatDiscountRateModel[];
  catDiscountRateOldList: CatDiscountRateModel[];

  initialCatDiscountRateList: CatDiscountRateModel[];
  initialCatDiscountRateOldList: CatDiscountRateModel[];

  natureDocs: CatDocumentModel[] = [];
  updatedNatureDocs: CatDocumentModel[] = [];
  deletedNatureDocs: CatDocumentModel[] = [];
  inheritedDocs: CatDocumentModel[];
  updatedNatureDocsIndex: number[] = [];
  editedDocumentList: boolean[] = [];
  documentName: string;
  creationUser: string;
  updateUser: string;
  // discount docs
  newDiscountDocs: CatDocumentModel[] = [];
  oldDiscountDocs: CatDocumentModel[] = [];
  catNatureLoaded: boolean = false;
  codeNapList: ParamNapModel[] = [];
  isSubmitted = false;
  codeNapkeyword = 'label';
  isChange = false;
  
  //energy
  energyForm: FormGroup;

  constructor(private fb: FormBuilder, private popupService: PopupService,
    private catNatureServce: CatNatureService,
    private route: ActivatedRoute, private valuationService: ValuationService, private catDiscountService: CatDiscountService,
    private nodeService: NodeService, private catDiscountRateService: CatDiscountRateService,
    private catDocumentService: CatDocumentService, private router: Router, private paramNapService: ParamNapService, private catActivitySectorService: CatActivitySectorService,
    private catalogService: CatalogService
  ) { }

  changeCodeNapeList(query) {

    this.paramNapService.getAllParamNAPListByQuery((query != null && query != undefined && query != '') ? query : null).subscribe(data => {
      this.codeNapList = data;

    })


  }


  changeCodeNapeListChange(query) {
    if (!this.isChange) {
      this.isChange = true;
    }

    this.changeCodeNapeList(query);

  }
  changeCodeNapeListFocus() {
    if (!this.isChange) {
      this.changeCodeNapeList('');
    }
  }

  initComponent(param) {
    this.generalities = this.newGeneralities();
    this.formMenu = this.fb.group({
      generalities: this.generalities,
      decoteNew: [],
      decoteOld: [],
      decoteAvgNew: [],
      decoteAvgOld: [],
      energyForm: []
    });

    if (this.isAddMode) {


      this.catNature = {} as CatNatureModel;
      this.catActivitySectorService.getActivitySectorById(param.id).subscribe((data) => {
        this.activitySector = data;
        this.catNature.catActivitySector = this.activitySector;
        this.catNature.isActive = true;
        this.formMenu.controls['generalities'].patchValue({
          secteurName: this.activitySector.label,
          sectorComment: this.catNature.catActivitySector.comment

        });
        this.searchInheritedDocs(this.catNature);

      }, (err) => {
        console.error(err);
      }, () => {
        //load new discount

        this.catDiscountService.getDiscountDetailsByTypeIdOrBrandIdOrNatureIdOrSectorId(null, null, null, this.catNature.catActivitySector.id, this.isNew).subscribe((data) => {
          this.catDiscountObject = { ...data };
          this.initialCatDiscountObject = { ...data };
        }, (err) => { console.error(err); },
          () => {
            //heritage
            if (this.catDiscountObject && this.catDiscountObject.id) {


              this.formMenu.controls['generalities'].patchValue({
                dureeMax: this.catDiscountObject.maxFinPer,
                dureeMin: this.catDiscountObject.minFinPer,
                seuilVr: this.catDiscountObject.discount,
                creation: this.catDiscountObject.creation,
              });
              this.initAvgDecoteNewValue()

              this.loadDiscountRateListByCatDiscountId(this.catDiscountObject.id);
            }
            else {
              this.popupService.popupInfo(
                "Il n'y a pas de taux de décote renseignés pour le secteur appartenant à cette nouvelle nature . Merci de bien vouloir renseigner les taux de décote souhaités pour créer cette nature.", null, null, null, ConfirmationModalComponent);
              const subscriptionPopup = this.popupService.onConfirm.subscribe((t) => {

                subscriptionPopup.unsubscribe();

              });
              // pas de héritage
              this.catDiscountObject = {} as CatDiscountModel;
              this.catDiscountObject.endDate = new Date("9999-12-31")
              this.catDiscountObject.isPerm = true;
              this.catDiscountObject.isReadjust = true;
              this.catDiscountObject.catActivitySector = this.activitySector;
              this.catDiscountObject.isNew = true;
              this.catDiscountRateList = [];
              for (let i = 0; i < 15; i++) {
                let catDiscountRate = {} as CatDiscountRateModel;
                catDiscountRate.discountRateYear = i + 1;
                this.catDiscountRateList.push(catDiscountRate);
              }
              this.loadNewDiscount = true;
              this.initialCatDiscountObject = { ...this.catDiscountObject }
              this.catOldDiscountObject = { ...this.catDiscountObject }
              this.catOldDiscountObject.isNew = false;
              this.catOldDiscountObject.ponderationRate = 50;
              this.initialCatOldDiscountObject = { ...this.catOldDiscountObject }
              this.catDiscountRateOldList = this.catDiscountRateList.map(a => { return { ...a } })
              this.loadOldDiscount = true;
              this.initAvgDecoteNewValue();
              this.initAvgDecoteOldValue();
            }
          });


      });

    }
    else {
      this.natureId = param.id;
      this.catNatureServce.getCatNatureById(this.natureId).subscribe((data) => {
        this.catNature = data;
        this.catNatureLoaded = true;
      },
        (err) => {
          console.error('error load Nature', err)
        },
        () => {
          this.setGenralitiesFormValue();
          this.searchNatureNativeDocs();
          this.searchNewDiscountDocs();
          this.searchOldDiscountDocs();
          this.searchInheritedDocs(this.catNature);
          
           this.catalogService.fillInfoCreationUser(this.catNature.createdBy).subscribe(res =>{
            this.creationUser = res.collaboratorFullName;
          })
         
         this.catalogService.fillInfoCreationUser(this.catNature.updatedBy).subscribe(res => {
          this.updateUser = res.collaboratorFullName;
         })
        

          // load Cotations
          this.valuationService.getLast10ValuationByNatureId(this.natureId).subscribe((data) => {
            this.valuationsList = data;
          });

          //load new discount
          this.catDiscountService.getDiscountDetailsByTypeIdOrBrandIdOrNatureIdOrSectorId(null, null, this.catNature.id, this.catNature.catActivitySector.id, this.isNew).subscribe((data) => {
            this.catDiscountObject = { ...data };
            this.initialCatDiscountObject = { ...data };



          }, (err) => { console.error(err) },
            () => {
              this.generalities.patchValue({
                dureeMax: this.catDiscountObject.maxFinPer,
                dureeMin: this.catDiscountObject.minFinPer,
                seuilVr: this.catDiscountObject.discount,
                creation: this.catDiscountObject.creation,
                //files: [{ value: null, disabled: true }, { validators: [documentsValidator] }]
              });
              this.loadDiscountRateListByCatDiscountId(this.catDiscountObject.id);

              this.initAvgDecoteNewValue()

            });
        });



    }


  }

  ngOnInit(): void {
    this.headersList = [
      'Généralités', 'Décote neuf', 'Décote occasion', 'Ajustement décotes énergie'
    ];
    this.route.params.subscribe(param => {
      // select generalities Tab whenever component is initalised
      this.selectedPlan = 0;
      this.loadNewDiscount = false;
      this.loadOldDiscount = false;
      this.catNatureLoaded = false;
      this.creationUser ="";
      this.updateUser ="";
      this.initIsAddMode();
      this.initComponent(param);
      this.modifModeActivatedGeneralite = false;
      this.modifModeActivatedNew = false;
      this.modifModeActivatedOld = false;
      this.modifModeEnergy = false;
      this.formMenu.updateValueAndValidity();

    })

  }
  initIsAddMode() {
    let path = this.router.url.split('/')[3];
    if (path == 'add') {
      this.isAddMode = true;
    }
    else {
      this.isAddMode = false;

    }
  }
  changeTab(i: number) {
    this.selectedPlan = i;
  }
  getOldDiscountDetails() {
    this.catDiscountService.getDiscountDetailsByTypeIdOrBrandIdOrNatureIdOrSectorId(null, null, this.catNature.id, this.catNature.catActivitySector.id, false).subscribe((data) => {
      this.catOldDiscountObject = { ...data };
      this.initialCatOldDiscountObject = { ...data }
      this.initAvgDecoteOldValue()

    }, (err) => { console.error(err); }, () => {
      this.loadDiscountRateOldListByCatDiscountId(this.catOldDiscountObject.id);

    });
  }

  loadDiscountRateOldListByCatDiscountId(id) {

    this.catDiscountRateService.getCatDiscountRateByCatDiscountId(id).subscribe((data) => {
      this.catDiscountRateOldList = data.map(a => { if (this.catOldDiscountObject.catNature == null) { a.id = null } return { ...a } });
      this.initialCatDiscountRateOldList = data.map(a => { if (this.catOldDiscountObject.catNature == null) { a.id = null } return { ...a } });
    }, (err) => {
      console.error(err);
    }, () => {

      this.loadOldDiscount = true;
      if (this.isAddMode && !this.catDiscountObject.id) {
        this.catOldDiscountObject.isReadjust = false;
      }

    });
  }

  setGenralitiesFormValue() {
    this.generalities.patchValue({
      label: this.catNature.label,
      comment: this.catNature.comment,
      sectorComment: this.catNature.catActivitySector.comment,
      codeNapLabel: (this.catNature && this.catNature.paramNap) ? this.catNature.paramNap.id + ' - ' + this.catNature.paramNap.label : null,
      //files : null
    })
    this.sectorTitle = this.catNature.catActivitySector.label;
    this.natureTitle = this.catNature.label;
  }
  private newGeneralities(): FormGroup {

    return this.fb.group({
      label: [{ value: null, disabled: true }, Validators.required],
      comment: [{ value: null, disabled: true }],
      sectorComment: [{ value: null, disabled: true }, Validators.required],
      codeNapLabel: [{ value: null, disabled: true }],
      codeNap: [{ value: null, disabled: true }, Validators.required],
      dureeMin: [{ value: null, disabled: true }, Validators.required],
      dureeMax: [{ value: null, disabled: true }, Validators.required],
      creation: [{ value: null, disabled: true }, Validators.required],
      seuilVr: [{ value: null, disabled: true }],
      files: [{ value: null, disabled: true }, { validators: [documentsValidator] }],
      secteurName: [{ value: null, disabled: true }],

    });

  }

  editNature() {
    this.modifModeActivatedNew = true;
    this.modifModeActivatedOld = true;
    this.modifModeActivatedGeneralite = true;
    this.modifModeEnergy = true;

    this.generalities.controls.label.enable();
    this.generalities.controls.comment.enable();
    this.formMenu.controls['generalities'].get('dureeMin').enable();
    this.formMenu.controls['generalities'].get('dureeMax').enable();
    this.formMenu.controls['generalities'].get('seuilVr').enable();
    this.formMenu.controls['generalities'].get('files').enable();
    this.formMenu.controls['decoteAvgNew'].enable();
    this.formMenu.controls['decoteAvgOld'].enable();
    this.formMenu.controls['energyForm'].enable();

  }


  loadDiscountRateListByCatDiscountId(id) {

    this.catDiscountRateService.getCatDiscountRateByCatDiscountId(id).subscribe((data) => {

      this.catDiscountRateList = data.map(a => { if (this.catDiscountObject.catNature == null) { a.id = null } return { ...a } });
      this.initialCatDiscountRateList = data.map(a => { if (this.catDiscountObject.catNature == null) { a.id = null } return { ...a } });
    }, (err) => {
      console.error(err);
    }, () => {
      if (this.isAddMode && !this.catDiscountObject.id) {
        this.catDiscountObject.isReadjust = false;

      }
      this.loadNewDiscount = true;

      //load old discount
      this.getOldDiscountDetails();
    });
  }


  editNatureToAdd() {
    if (this.isAddMode && !this.isPageFirstLoaded) {
      this.isPageFirstLoaded = true;
      this.editNature();
      if (!this.catDiscountObject.id) {
        this.formMenu.get('decoteNew').valueChanges.subscribe(value => {
          if (this.formMenu.get('decoteNew').valid) {
            if (!this.isNewValid) {
              this.catDiscountRateOldList = this.formMenu.getRawValue().decoteNew.catDiscountRateList.map(a => { return { ...a } });
              this.isNewValid = true;
              this.modifModeActivatedOld = true


              this.formMenu.controls['decoteAvgOld'].enable();
            }


          }
          else {
            this.isNewValid = false;
            this.catDiscountRateOldList = this.formMenu.getRawValue().decoteNew.catDiscountRateList.map(a => { return { ...a } });
            this.modifModeActivatedOld = false
            this.formMenu.controls['decoteAvgOld'].disable();

          }
        })
      }
      this.generalities.controls.codeNap.enable();
      this.changeCodeNapeList('');
      this.modifModeEnergy = true;

    }

  }
  isFormValid(index) {
    switch (index.toString()) {
      case '0': {


        return this.formMenu.controls.generalities.valid;
      }
      case '1': {


        return this.formMenu.controls.decoteNew.valid;

      }
      case '2': {


        return this.formMenu.controls.decoteOld.valid;

      }
      case '3': {


        return this.catalogEnergyComponent && this.catalogEnergyComponent.mainForm ? this.catalogEnergyComponent.mainForm.valid : null;

      }
      default: {
        return null;
      }
    }
  }

  saveNature() {
    this.formMenu.controls['generalities'].markAllAsTouched();
    this.isSubmitted = true
    if (this.formMenu.valid && this.catalogEnergyComponent.mainForm.valid) {
      this.generateCatDiscount(this.formMenu.getRawValue().decoteNew, this.formMenu.getRawValue().decoteAvgNew);

      this.updateNatureInfo();
      this.popupService.popupInfo(
        'Êtes-vous certain de vouloir enregistrer votre saisie ? Les paramètres initiaux seront remplacés.', 'OUI', null, 'NON', ConfirmationModalComponent);
      const subscriptionPopup = this.popupService.onConfirm.subscribe((t) => {
        if (t) {
          this.modifModeActivatedGeneralite = false;
          this.modifModeEnergy = false;
          this.generalities.controls.label.disable();
          this.generalities.controls.comment.disable();


          this.catNatureServce.saveCatNature(this.catNature).subscribe((data) => {
            this.catNature = data;

            let node = {} as TreeNode;
            node.data = this.catNature.id;
            node.label = this.catNature.label;
            node.type = 'nature';

            node.parent = {} as TreeNode;
            node.parent.data = this.catNature.catActivitySector.id;
            node.icon = 'pi pi-folder'

            if (this.isAddMode) {
              this.nodeService.nodeAddSubject.next(node);
            } else { this.nodeService.nodeSubject.next(node); }

          }, (err) => { console.error("can't save update this.natureId", err) },
            () => {

              this.updateGeneralitiesDocs();

              this.setGenralitiesFormValue();

              let fullCatDiscount: FullCatDiscountModel = {} as FullCatDiscountModel;
              this.generateCatDiscount(this.formMenu.value.decoteNew, this.formMenu.value.decoteAvgNew);

              if (this.isAddMode || this.catDiscountObject.catNature == null) {
                if (!this.catDiscountObject.id) {
                  this.catOldDiscountObject.isReadjust = true;
                }
                this.catDiscountObject.id = null;
                this.catDiscountObject.catNature = this.catNature;
                this.catDiscountObject.catActivitySector = this.catNature.catActivitySector;
              }
              fullCatDiscount.catDiscount = this.catDiscountObject
              fullCatDiscount.catDiscountRateList = this.formMenu.getRawValue().decoteNew.catDiscountRateList;
              this.catDiscountRateList = fullCatDiscount.catDiscountRateList;

              this.initialCatDiscountObject.endDate = this.catDiscountObject.endDate

              if (JSON.stringify(this.catDiscountObject) !== JSON.stringify(this.initialCatDiscountObject) || (!this.arraysEqual(this.catDiscountRateList, this.initialCatDiscountRateList)) || this.isAddMode) {

                this.catDiscountService.saveCatDiscountAndRate(fullCatDiscount, 'nature', true).subscribe(data => {

                  Object.assign(this.catDiscountObject, data.catDiscount)
                  Object.assign(this.initialCatDiscountObject, data.catDiscount)
                  this.catDiscountRateList = data.catDiscountRateList.map(a => { return { ...a } });
                  this.initialCatDiscountRateList = data.catDiscountRateList.map(a => { return { ...a } });

                }, (err) => {
                  console.error(err);
                }, () => {
                  //should load updated old discount after new discount modification

                  this.modifModeActivatedNew = false;

                  //save, delete, update new discount docs
                  this.catalogNewDiscountsComponent.updateNewDiscountDocs();

                });
              }
              else {
                this.modifModeActivatedNew = false;

              }



              let fullOldCatDiscount: FullCatDiscountModel = {} as FullCatDiscountModel;
              this.generateCatOldDiscount(this.formMenu.getRawValue().decoteOld, this.formMenu.getRawValue().decoteAvgOld);


              if (this.isAddMode || this.catOldDiscountObject.catNature == null) {
                if (!this.catOldDiscountObject.id) {
                  this.catOldDiscountObject.isReadjust = true;
                }
                this.catOldDiscountObject.id = null;
                this.catOldDiscountObject.catNature = this.catNature;
                this.catOldDiscountObject.catActivitySector = this.catNature.catActivitySector;

              }
              fullOldCatDiscount.catDiscount = this.catOldDiscountObject
              fullOldCatDiscount.catDiscountRateList = this.formMenu.getRawValue().decoteOld.catDiscountRateOldList;
              this.catDiscountRateOldList = fullOldCatDiscount.catDiscountRateList;
              this.initialCatOldDiscountObject.endDate = this.catOldDiscountObject.endDate
              if (JSON.stringify(this.initialCatOldDiscountObject) !== JSON.stringify(this.catOldDiscountObject) || (!this.arraysEqual(this.catDiscountRateOldList, this.initialCatDiscountRateOldList)) || this.isAddMode) {


                this.catDiscountService.saveCatDiscountAndRate(fullOldCatDiscount, 'nature', false).subscribe(data => {

                  Object.assign(this.catOldDiscountObject, data.catDiscount)
                  Object.assign(this.initialCatOldDiscountObject, data.catDiscount)

                  this.catDiscountRateOldList = data.catDiscountRateList.map(a => { return { ...a } });
                  this.initialCatDiscountRateOldList = data.catDiscountRateList.map(a => { return { ...a } });

                }, (err) => {
                  console.error(err);
                }, () => {
                  this.modifModeActivatedOld = false;
                  this.formMenu.controls.decoteOld.disable()

                  //save, delete, update old discount docs
                  this.catalogOldDiscountsComponent.updateOldDiscountDocs(this.isAddMode, this.catNature.id, 'nature');

                });
              }
              else {
                this.modifModeActivatedOld = false;
              }
              this.formMenu.controls['decoteAvgNew'].disable();
              this.formMenu.controls['decoteAvgOld'].disable();

              this.lockEnergyComponent();
            });

        }

        subscriptionPopup.unsubscribe();
      });
      this.isSubmitted = false;

    }
    else {

      this.popupService.popupInfo(
        "Merci de bien vouloir renseigner l'intégralité des informations demandées.", null, null, null, ConfirmationModalComponent);
      const subscriptionPopup = this.popupService.onConfirm.subscribe((t) => {

        subscriptionPopup.unsubscribe();

      });

    }
  }
  generateCatDiscount(decoteForm: any, decoteAvgForm: any) {
    if (decoteForm) {


      let catDiscount: CatDiscountModel = decoteForm as CatDiscountModel;
      if (catDiscount && catDiscount.isReadjust == null) {
        catDiscount.isReadjust = false;
      }
      if (catDiscount && catDiscount.isPerm == null) {
        catDiscount.isReadjust = false;
      }

      this.catDiscountObject.isReadjust = decoteForm.isReadjust;
      this.catDiscountObject.isPerm = decoteForm.isPerm;
      this.catDiscountObject.comment = decoteForm.comment;

      if (decoteForm.endDate != null && decoteForm.endDate != undefined && typeof decoteForm.endDate === 'string') {
        let splitedDate = decoteForm.endDate.split("/");
        if (splitedDate != null && splitedDate != undefined && splitedDate.length > 0) {
          this.catDiscountObject.endDate = decoteForm.endDate.split("/").reverse().join("-");

        }
      }
      else {
        this.catDiscountObject.endDate = new Date('9999-12-31');

      }

    }



    this.catDiscountObject.minFinPer = this.formMenu.controls['generalities'].get('dureeMin').value;
    this.catDiscountObject.maxFinPer = this.formMenu.controls['generalities'].get('dureeMax').value;
    this.catDiscountObject.discount = this.formMenu.controls['generalities'].get('seuilVr').value;
    this.catDiscountObject.creation = this.formMenu.controls['generalities'].get('creation').value;
    if (decoteAvgForm) {
      this.catDiscountObject.startDateAverLabel = decoteAvgForm.startDate;
      this.catDiscountObject.endDateAverLabel = decoteAvgForm.endDate;
      this.catDiscountObject.nbMinValAver = decoteAvgForm.nbMinToConsider;
    }

  }

  generateCatOldDiscount(decoteForm: any, decoteAvgOldForm: any) {

    if (decoteForm) {
      this.catOldDiscountObject.isReadjust = decoteForm.isReadjust;
      this.catOldDiscountObject.isPerm = decoteForm.isPerm;
      this.catOldDiscountObject.comment = decoteForm.comment;
      this.catOldDiscountObject.ponderationRate = decoteForm.ponderationRate ? decoteForm.ponderationRate : 50;
      this.catOldDiscountObject.maxFinPer = decoteForm.maxFinPer ? decoteForm.maxFinPer : (this.catDiscountObject.maxFinPer ? this.catDiscountObject.maxFinPer : 72);


      if (decoteForm.endDate && typeof decoteForm.endDate === 'string') {

        let splitedDate = decoteForm.endDate.split("/");
        if (splitedDate != null && splitedDate != undefined && splitedDate.length > 0) {
          this.catOldDiscountObject.endDate = decoteForm.endDate.split("/").reverse().join("-");

        }
      }
      else {
        this.catOldDiscountObject.endDate = new Date('9999-12-31');

      }


    }
    if (decoteAvgOldForm) {
      this.catOldDiscountObject.startDateAverLabel = decoteAvgOldForm.startDate;
      this.catOldDiscountObject.endDateAverLabel = decoteAvgOldForm.endDate;
      this.catOldDiscountObject.nbMinValAver = decoteAvgOldForm.nbMinToConsider;
    }


  }

  undo() {

    this.popupService.popupInfo(
      'Êtes-vous certain de vouloir annuler votre saisie ?', 'OUI', null, 'NON', ConfirmationModalComponent);
    const subscription = this.popupService.onConfirm.subscribe((t) => {
      if (t) {
        if (this.isAddMode) {
          this.router.navigate(['catalog', 'sector', this.catNature.catActivitySector.id]);

        }
        this.modifModeActivatedGeneralite = false;
        this.modifModeActivatedNew = false;
        this.modifModeActivatedOld = false;
        this.modifModeEnergy = false;

        this.formMenu.controls['decoteAvgNew'].disable();
        this.formMenu.controls['decoteAvgOld'].disable();
        this.formMenu.controls['energyForm'].disable();

        this.generalities.disable();
        // reload Info
        this.loadNatureData();
        // reset new discount component
        this.catalogNewDiscountsComponent.undo();
        //reset old discount component
        this.catalogOldDiscountsComponent.undo();

        this.initAvgDecoteOldValue();
        this.initAvgDecoteNewValue();

        //reset plan financier table
        this.formMenu.controls['generalities'].patchValue({
          dureeMax: this.catDiscountObject.maxFinPer,
          dureeMin: this.catDiscountObject.minFinPer,
          seuilVr: this.catDiscountObject.discount,
          creation: this.catDiscountObject.creation,
          files: { files: [] }
        });

        if (this.catDiscountRateList && this.catDiscountRateList.length) {
          this.rateNewDiscountFirstYear = this.catDiscountRateList[0].discountRateValue;
        }
        //reset files variables
        this.updatedNatureDocs = [];
        this.updatedNatureDocsIndex = [];
        this.deletedNatureDocs = [];

        if (this.natureDocs) {
          this.natureDocs.forEach((element, index) => {

            this.updatedNatureDocs.push(Object.assign({}, element))
            this.editedDocumentList[index] = false;
          });
        }

        //reset energy component
        this.catalogEnergyComponent.undo();
        //reset errors
        this.isSubmitted = false;
      }

      subscription.unsubscribe();
    });
  }
  updateNatureInfo() {
    this.catNature.comment = this.generalities.controls.comment.value;
    this.catNature.label = this.generalities.controls.label.value;
    if (this.isAddMode) {
      this.catNature.paramNap = this.generalities.controls.codeNap.value;
    }

  }
  loadNatureData() {
    this.catNatureServce.getCatNatureById(this.natureId).subscribe((data) => {
      this.catNature = data;
    },
      (err) => {
        console.error('error load Nature', err)
      },
      () => {
        this.setGenralitiesFormValue();
      });
    // load Cotations
    this.valuationService.getLast10ValuationByNatureId(this.natureId).subscribe((data) => {
      this.valuationsList = data;
    });
  }







  changeNewDiscount(rateNewDiscountFirstYear) {
    this.rateNewDiscountFirstYear = rateNewDiscountFirstYear;
  }


  arraysEqual(arr1, arr2) {
    let isEqual = true;
    if (arr1.length != arr2.length) {
      return false;
    }
    arr1.forEach((element, index) => {
      if ((JSON.stringify(element) !== JSON.stringify(arr2[index]))) {
        isEqual = false;
        return false;
      }

    });
    return isEqual;
  }

  deleteDocument(index: number) {
    this.popupService.popupInfo('Êtes-vous sûr(e) de vouloir supprimer la pièce jointe ?', 'Confirmer', null, 'Annuler', ConfirmationModalComponent);
    const subscription = this.popupService.onConfirm.subscribe((t) => {
      if (t) {
        this.deletedNatureDocs.push(this.updatedNatureDocs[index])

        this.editedDocumentList.splice(index, 1);
        this.updatedNatureDocs.splice(index, 1);
        let indexIndex = this.updatedNatureDocsIndex.findIndex(val => val == index);
        if (indexIndex > 0) {
          this.updatedNatureDocsIndex.splice(indexIndex, 1);
        }
      }
      subscription.unsubscribe();

    })
  }
  editDocumentName(index: number) {
    this.editedDocumentList[index] = true;
    this.documentName = this.updatedNatureDocs[index].docName;

  }

  onBlurDocument(index: number) {

    this.updatedNatureDocs[index].docName = this.documentName;
    if (this.updatedNatureDocsIndex.findIndex(val => val == index) == -1) {
      this.updatedNatureDocsIndex.push(index);
    }
    this.editedDocumentList[index] = false;
  }

  //save delete update docs
  updateGeneralitiesDocs() {
    if (this.formMenu.value.generalities.files) {

      let files: File[] = this.formMenu.value.generalities.files.files;

      let request = [];

      if (files && files.length > 0) {
        files.forEach(file => request.push(this.catDocumentService.uploadDocument(file, this.catNature.catActivitySector.id,
          this.catNature.id, null, null, 'G').pipe(catchError(error => of(null)))))
      }

      if (this.updatedNatureDocsIndex.length > 0) {
        let docs = []
        this.updatedNatureDocsIndex.forEach(index => docs.push(this.updatedNatureDocs[index]))
        docs.forEach(doc => request.push(this.catDocumentService.updateDocument(doc).pipe(catchError(error => of(null)))));

      }
      if (this.deletedNatureDocs.length > 0) {

        this.deletedNatureDocs.forEach(docs => request.push(this.catDocumentService.deleteCatDocument(docs).pipe(catchError(error => of(null)))))
      }


      forkJoin(request).subscribe(res => {
        // this is intentional
      }, () => {
        //this is intentional

      }, () => {
        this.formMenu.controls['generalities'].get('files').reset();
        this.searchNatureNativeDocs();
      });
    }
  }

  searchNatureNativeDocs() {
    this.catDocumentService.searchDocument(this.catNature.catActivitySector.id, this.catNature.id, null, null, 'G').subscribe(res => {
      this.natureDocs = res;

    }, err => {
      console.error(err);
    }, () => {

      this.updatedNatureDocs = [];
      this.updatedNatureDocsIndex = [];
      this.deletedNatureDocs = [];

      this.natureDocs.forEach((element, index) => {

        this.updatedNatureDocs.push(Object.assign({}, element))
        this.editedDocumentList[index] = false;
      });
    });
  }
  searchInheritedDocs(catNature: CatNatureModel) {
    this.catDocumentService.searchInheratedDocuments(catNature.catActivitySector.id, null, null, 'G').subscribe(data => {
      this.inheritedDocs = data;
    });

  }
  searchNewDiscountDocs() {
    this.catDocumentService.searchDocument(this.catNature.catActivitySector.id, this.catNature.id, null, null, 'N').subscribe(res => {
      this.newDiscountDocs = res;
    })
  }

  searchOldDiscountDocs() {
    this.catDocumentService.searchDocument(this.catNature.catActivitySector.id, this.catNature.id, null, null, 'O').subscribe(res => {
      this.oldDiscountDocs = res;
    })
  }

  downloadDocument(documentToDownload: CatDocumentModel) {
    this.catDocumentService.downloadDocument(documentToDownload.gedPath, documentToDownload.docName).subscribe(response => {
      var file = new Blob([response], { type: 'application/*' })
      var fileURL = URL.createObjectURL(file);
      const doc = document.createElement('a');
      doc.download = documentToDownload.docName;
      doc.href = fileURL;
      doc.dataset.downloadurl = [doc.download, doc.href].join(':');
      document.body.appendChild(doc);
      doc.click();
    });
  }

  initAvgDecoteOldValue() {
    this.formMenu.controls['decoteAvgOld'].patchValue({
      startDate: this.initialCatOldDiscountObject.startDateAverLabel ? this.initialCatOldDiscountObject.startDateAverLabel : null,
      endDate: this.initialCatOldDiscountObject.endDateAverLabel ? this.initialCatOldDiscountObject.endDateAverLabel : null,
      nbMinToConsider: this.initialCatOldDiscountObject.nbMinValAver ? this.initialCatOldDiscountObject.nbMinValAver : 3
    });
  }

  initAvgDecoteNewValue() {
    this.formMenu.controls['decoteAvgNew'].patchValue({

      startDate: this.initialCatDiscountObject.startDateAverLabel ? this.initialCatDiscountObject.startDateAverLabel : null,
      endDate: this.initialCatDiscountObject.endDateAverLabel ? this.initialCatDiscountObject.endDateAverLabel : null,
      nbMinToConsider: this.initialCatDiscountObject.nbMinValAver ? this.initialCatDiscountObject.nbMinValAver : 3
    });
  }
  navigate(idValuation) {
    this.router.navigate(['/result/expert/' + idValuation]);

  }

  lockEnergyComponent() {
    //if add mode, should set the id of the saved catNature
    if (this.isAddMode) {
      (this.catalogEnergyComponent.energyLinesControls).forEach(element => {
        element.patchValue({ catNature: this.catNature })
      })
    }
    this.catalogEnergyComponent.saveModification();
    //close energies options, it can be opened
    this.catalogEnergyComponent.displayOptionsSelect = false;

  }
}
