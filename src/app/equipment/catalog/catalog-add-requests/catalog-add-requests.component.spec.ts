import { of, throwError, Subject } from 'rxjs';
import { CatRequestWaiting } from '../../../shared/models/catalog-add-request.model';
import { CatalogAddRequestsComponent } from "./catalog-add-requests.component";

describe('Test CatalogAddRequestsComponent', () => {

    let fixture: CatalogAddRequestsComponent;
    let catalogAddRequestServiceMock: any;
    let catBrandServiceMock: any;
    let catAssetTypeServiceMock: any;
    let routerMock: any;
    let activatedRouteMock: any;

    beforeEach(() => {

        catalogAddRequestServiceMock = {
            loadCatAddRequests: jest.fn().mockReturnValue(of(true))
        }

        routerMock = {
            navigate: jest.fn().mockReturnValue(new Promise(() => {
                //this is intentional
            })),
            url: '/catalog'
        }

        catBrandServiceMock = {
            getCatBrandByCatReqstId: jest.fn().mockReturnValue(of({ id: '201' }))
        }

        catAssetTypeServiceMock = {
            getAssetTypeByCatReqstId: jest.fn().mockReturnValue(of({ id: '201' }))
        }
        activatedRouteMock = {
            params: new Subject()

        };
        activatedRouteMock = {
            params: of({id : '1'})

        }

        fixture = new CatalogAddRequestsComponent(catalogAddRequestServiceMock, routerMock, catBrandServiceMock, catAssetTypeServiceMock, activatedRouteMock);

        fixture.ngOnInit();
    });

    describe('Test Component', () => {
        it('should Be Truthy', () => {
            expect(fixture).toBeTruthy();
        });

        it('should catalogAddRequestServiceMock throws error ', () => {
            spyOn(catalogAddRequestServiceMock, 'loadCatAddRequests').and.returnValue(throwError({ status: 404 }));
            fixture.ngOnInit();
        });
    });

    describe('Test navigate()', () => {
        it('should be navigated to Add Type page', () => {
            let request = {
                idBrand: '1854'
            } as CatRequestWaiting;
            fixture.navigate(request);
            expect(routerMock.navigate).toHaveBeenCalledWith(['catalog', 'type', 'add'], { state: { catRequest: request } });

        });

        it('should be navigated to Add Brand page', () => {
            let request = {
                idBrand: null
            } as CatRequestWaiting;
            fixture.navigate(request);
            expect(routerMock.navigate).toHaveBeenCalledWith(['catalog', 'brand', 'add'], { state: { catRequest: request } });

        });

    });

    describe('Test navigateToCreated()', () => {
        it('should be navigated to  Type page', () => {
            let request = {
                id: '15',
                idBrand: '1854'
            } as CatRequestWaiting;
            fixture.navigateToCreated(request);
            expect(catAssetTypeServiceMock.getAssetTypeByCatReqstId).toHaveBeenCalled();
            catAssetTypeServiceMock.getAssetTypeByCatReqstId().subscribe((x) => {
                expect(routerMock.navigate).toHaveBeenCalledWith(['catalog', 'type', '201']);

            })

        });

        it('should be navigated to  Brand page', () => {
            let request = {
                id: '15',
                idBrand: null
            } as CatRequestWaiting;
            fixture.navigateToCreated(request);
            expect(catBrandServiceMock.getCatBrandByCatReqstId).toHaveBeenCalled();
            catBrandServiceMock.getCatBrandByCatReqstId().subscribe((x) => {
                expect(routerMock.navigate).toHaveBeenCalledWith(['catalog', 'brand', '201']);

            })
        });

    });

    describe('Test navigateToCreated()', () => {
        it('should be navigated to  Type page', () => {
            let request = {
                id: '1',
                idBrand: null
            } as CatRequestWaiting;
            fixture.addRequests = [];
            fixture.addRequests.push(request)

            fixture.redirectRequest();



        });
    });

});
