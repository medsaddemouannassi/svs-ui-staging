import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CatRequestWaiting } from '../../../shared/models/catalog-add-request.model';
import { CatAssetTypeService } from '../../../shared/services/cat-asset-type.service';
import { CatBrandService } from '../../../shared/services/cat-brand.service';
import { CatalogAddRequestService } from '../../../shared/services/catalog-add-request.service';

@Component({
    selector: 'app-catalog-add-requests',
    templateUrl: './catalog-add-requests.component.html',
    styleUrls: ['./catalog-add-requests.component.css'],

})
export class CatalogAddRequestsComponent implements OnInit {

    title = "Aucune demande d'ajout à traiter";
    tooltipLength = false;
    addRequests: CatRequestWaiting[];

    constructor(private catalogAddRequestService: CatalogAddRequestService,
        private router: Router, private catBrandService: CatBrandService,
        private catAssetTypeService: CatAssetTypeService,
        private activatedRoute: ActivatedRoute ) {
    }

    ngOnInit() {
        this.loadCatAddRequests();
    }

    loadCatAddRequests() {
        this.catalogAddRequestService.loadCatAddRequests().subscribe(resp => {

            this.addRequests = resp;

        }, err => {

            console.error(err);

        }, () => {
            if (this.addRequests && this.addRequests.length != 0) {
                this.title = 'Demande d\'ajout marque \/ type';
                this.redirectRequest();
            }
        });
    }
    redirectRequest(){
        this.activatedRoute.params.subscribe(param => {
            if ((this.addRequests.map(r => r.id)).includes(param.id)) {
                this.navigate(this.addRequests.find(r => r.id == param.id))
            }
        })
    }
    navigate(request: CatRequestWaiting) {

        if (request.idBrand) {
            this.router.navigate(['catalog', 'type', 'add'], { state: { catRequest: request } });
        } else {
            this.router.navigate(['catalog', 'brand', 'add'], { state: { catRequest: request } });
        }

    }

    navigateToCreated(request: CatRequestWaiting) {
        if (request.idBrand) {

            this.catAssetTypeService.getAssetTypeByCatReqstId(request.id).subscribe(assetType => {
                if (assetType) {
                    this.router.navigate(['catalog', 'type', assetType.id]);

                }
            })

        } else {
            this.catBrandService.getCatBrandByCatReqstId(request.id).subscribe(brand => {

                if (brand) {

                    this.router.navigate(['catalog', 'brand', brand.id]);

                }
            })

        }
    }
}