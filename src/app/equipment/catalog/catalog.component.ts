import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { TreeNode } from 'primeng/api';
import { NodeService } from '../../shared/services/node.service';
import { SharedCommunicationDataService } from '../../shared/services/shared-communication-data.service';


@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.css']
})
export class CatalogComponent {

  tree: TreeNode[];
  breadCrumb: string[] = [];
  selectednode: TreeNode;
  isAddDisabled: boolean = false;

  constructor(
    private nodeService: NodeService,
    private router: Router,
    private dataSharing: SharedCommunicationDataService,

  ) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.nodeService.getActivitySectorsWithNature().subscribe(sectors => this.tree = sectors.data);

    this.nodeService.nodeAddSubject.subscribe(node => {
      switch (node.type) {

        case 'nature': {

          this.nodeService.getNature(node.parent.data).subscribe(
            res => {
              this.tree.find(sect => sect.data == node.parent.data)
                .children = res;

            }, err => console.error(err),
            () => {
              this.tree.find(sect => sect.data == node.parent.data).expanded = true;
            }
          )


          break;
        }
        case 'brand': {

          this.nodeService.getBrand(node.parent.data).subscribe(
            res => {
              this.tree.find(sect => sect.data == node.parent.parent.data)
                .children.find(nat => nat.data == node.parent.data)
                .children = res

            }, err => console.error(err),
            () => {
              this.tree.find(sect => sect.data == node.parent.parent.data)
                .children.find(nat => nat.data == node.parent.data).expanded = true
            }
          )


          break;
        }
        case 'type': {
          this.nodeService.getAssetType(node.parent.data).subscribe(
            res => {

              this.tree.find(sect => sect.data == node.parent.parent.parent.data)
                .children.find(nat => nat.data == node.parent.parent.data)
                .children.find(brand => brand.data == node.parent.data)
                .children = res;

            }, err => console.error(err)
            ,
            () => {
              this.tree.find(sect => sect.data == node.parent.parent.parent.data)
                .children.find(nat => nat.data == node.parent.parent.data)
                .children.find(brand => brand.data == node.parent.data).expanded = true
            }
          )


          break;
        }

      }
    })


    this.nodeService.nodeDeleteSubject.subscribe(node => {

      switch (node.type) {

        case 'brand': {

          let index = this.tree.find(sect => sect.data == node.parent.parent.data)
            .children.find(nat => nat.data == node.parent.data)
            .children.findIndex(brand => brand.data == node.data);
          this.tree.find(sect => sect.data == node.parent.parent.data)
            .children.find(nat => nat.data == node.parent.data)
            .children.splice(index, 1)
          break;
        }
        case 'type': {

          let index = this.tree.find(sect => sect.data == node.parent.parent.parent.data)
            .children.find(nat => nat.data == node.parent.parent.data)
            .children.find(brand => brand.data == node.parent.data)
            .children.findIndex(type => type.data == node.data);
          this.tree.find(sect => sect.data == node.parent.parent.parent.data)
            .children.find(nat => nat.data == node.parent.parent.data)
            .children.find(brand => brand.data == node.parent.data)
            .children.splice(index, 1);
          break;
        }

      }
    })

    this.nodeService.nodeSubject.subscribe(node => {

      switch (node.type) {
        case 'sector': {

          this.tree.find(sect => sect.data == node.data).label = node.label;
          break;
        }

        case 'nature': {

          this.tree.find(sect => sect.data == node.parent.data)
            .children.find(nat => nat.data == node.data).label = node.label;
          break;
        }

        case 'brand': {

          this.tree.find(sect => sect.data == node.parent.parent.data)
            .children.find(nat => nat.data == node.parent.data)
            .children.find(brand => brand.data == node.data).label = node.label;
          break;
        }
        case 'type': {
          try {
            this.tree.find(sect => sect.data == node.parent.parent.parent.data)
              .children.find(nat => nat.data == node.parent.parent.data)
              .children.find(brand => brand.data == node.parent.data)
              .children.find(type => type.data == node.data).label = node.label;
          }
          catch (error) {

          }


        }

      }
    })
    this.dataSharing.changeStatus("true");
  }



  nodeExpand(evt: any): void {
    if (evt.node.nodeType == 'nature') {

      this.nodeService.getBrand(evt.node.data).subscribe(
        res => {
          evt.node.children = res;
        }
      )

    }
    if (evt.node.nodeType == 'brand') {
      this.nodeService.getAssetType(evt.node.data).subscribe(
        res => {
          evt.node.children = res;
        }
      )
    }
  }

  nodeSelect(evt: any): void {
    this.addPathToBreadCrumb(evt.node);
    if (evt.node.nodeType == 'type') { this.isAddDisabled = true } else {
      this.isAddDisabled = false

    }

    this.router.navigate(['catalog', evt.node.nodeType, evt.node.data])
  }
  addPathToBreadCrumb(path: any) {
    this.breadCrumb = [];
    while (path) {
      this.breadCrumb.push(path)
      path = path.parent;
    }
    this.breadCrumb.reverse();
  }
  add() {
    let path = this.router.url.split('/')[2];
    if (path != undefined && path != null && !this.router.url.split('/').includes('add')) {
      this.dataSharing.changeStatus("false");
      switch (path) {
        case 'sector':
          this.router.navigate(['catalog', 'nature', 'add', this.router.url.split('/')[3]])
          break;

        case 'nature':

         this.router.navigate(['catalog', 'brand', 'add', this.router.url.split('/')[3]])


          break;

        case 'brand':
          this.router.navigate(['catalog', 'type', 'add', this.router.url.split('/')[3]])
          break;
      }
    }



  }

  onClickHome() {
    this.router.navigate(['catalog']);
    this.breadCrumb = [];
    this.collapseAllTreeNodes();
  }

  collapseAllTreeNodes() {
    for (let el of this.tree) {
      el.expanded = false;
    }
  }

  ngOnDestroy() {
    this.dataSharing.changeStatus("false");
  }

}
