import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TreeNode } from 'primeng/api/treenode';
import { forkJoin, Subscription, of } from 'rxjs';
import { ConfirmationModalComponent } from '../../../shared/confirmation-modal/confirmation-modal.component';
import { documentsValidator } from '../../../shared/documents/document-validator';
import { CatActivitySectorModel } from '../../../shared/models/cat-activity-sector.model';
import { CatAssetTypeModel } from '../../../shared/models/cat-asset-type.model';
import { CatBrandModel } from '../../../shared/models/cat-brand.model';
import { CatDocumentModel } from '../../../shared/models/cat-document.model';
import { CatNatureModel } from '../../../shared/models/cat-nature.model';
import { CatRequestModel } from '../../../shared/models/cat-request.model';
import { CatRequestWorkFlow } from '../../../shared/models/catalog-add-request-workflow.model';
import { CatRequestWaiting } from '../../../shared/models/catalog-add-request.model';
import { CatRequestDoc } from '../../../shared/models/catalog-request-doc.model';
import { ParamSettingModel } from '../../../shared/models/param-setting.model';
import { Role } from '../../../shared/models/roles.enum';
import { ValuationModel } from '../../../shared/models/valuation.model';
import { PagesConstants } from '../../../shared/pages-constant';
import { CatActivitySectorService } from '../../../shared/services/cat-activity-sector.service';
import { CatAssetTypeService } from '../../../shared/services/cat-asset-type.service';
import { CatBrandService } from '../../../shared/services/cat-brand.service';
import { CatDocumentService } from '../../../shared/services/cat-document.service';
import { CatNatureService } from '../../../shared/services/cat-nature.service';
import { CatalogAddRequestWorkflowService } from '../../../shared/services/catalog-add-request-workflow.service';
import { CatalogAddRequestService } from '../../../shared/services/catalog-add-request.service';
import { NodeService } from '../../../shared/services/node.service';
import { ParamSettingService } from '../../../shared/services/param-setting.service';
import { PopupService } from '../../../shared/services/popup.service';
import { SharedCommunicationDataService } from '../../../shared/services/shared-communication-data.service';
import { StatusConstant } from '../../../shared/status-constant';
import { catchError } from 'rxjs/operators';
import { CatalogService } from '../../../shared/services/catalog.service';




@Component({
  selector: 'app-menu-cat-type',
  templateUrl: './menu-cat-type.component.html',
  styleUrls: ['./menu-cat-type.component.css']
})
export class MenuCatTypeComponent implements OnInit {
  isAddMode = false;
  isActiv: boolean = true;
  isAddRequest: boolean = false;
  modifModeActivated = false;
  assetType: CatAssetTypeModel = {} as any;
  catBrand: CatBrandModel;
  typeForm: FormGroup;
  subscriptions: Subscription[] = [];
  headersList: any[];
  selectedTabulation = 0;
  showForm = false
  option = ['56.10A - Restauration traditionnelle', '56.58A - Tesst TesssssT'];
  valuationsList: ValuationModel[];
  assertTypeTitle: string;
  natureTitle: string;
  sectorTitle: string;
  catBrandTitle: string;
  node: any = {};

  assetTypeDocs: CatDocumentModel[];
  updatedAssetTypeDocs: CatDocumentModel[] = [];
  assetTypeRequestDocs: CatRequestDoc[];
  updatedAssetTypeRequestDocs: CatRequestDoc[] = [];

  updatedAssetTypeDocsIndex: number[];
  editedDocumentList: boolean[] = [];
  deletedDocumentList: CatDocumentModel[] = [];
  isSubmited = false;
  documentName: string;
  cactivitySectorOptions: CatActivitySectorModel[] = [];
  natureOptions: CatNatureModel[] = [];
  brandOptions: CatBrandModel[] = [];
  inheritedDocsSector: CatDocumentModel[];
  inheritedDocsNature: CatDocumentModel[];
  inheritedDocsBrand: CatDocumentModel[];
  creationUser: string;
  updateUser: string;
  catRequest: CatRequestWaiting = null;
  communicationSubscription: Subscription;
  statusList: ParamSettingModel[];

  constructor(private fb: FormBuilder,
    private catAssetTypeService: CatAssetTypeService,
    private route: ActivatedRoute,
    private popupService: PopupService,
    private catBrandService: CatBrandService,
    private catNatureService: CatNatureService,
    private catActivitySectorService: CatActivitySectorService,
    private nodeService: NodeService,
    private router: Router,
    private catDocumentService: CatDocumentService,
    private catalogAddRequestWorkflowService: CatalogAddRequestWorkflowService,
    private catRequestService: CatalogAddRequestService,
    private paramSettingService: ParamSettingService,
    private dataSharing: SharedCommunicationDataService,
    private catalogService: CatalogService
  ) { }

  ngOnInit(): void {
    this.headersList = ['Généralités'];
    this.route.params.subscribe(param => {
      this.initIsAddMode();
      this.initComponent(param);
      this.creationUser ="";
      this.updateUser ="";
    })

  }

  initIsAddMode() {

    if (history.state && history.state.catRequest) {
      this.isAddRequest = true;
      this.catRequest = history.state.catRequest;
      this.dataSharing.changeStatus("false");

      this.communicationSubscription = this.dataSharing.triggerCommunication.subscribe(() => {
        this.popupService.popupCommunication([Role.ADMIN_SE], PagesConstants.CATALOG_ADD, "", '', [], null, this.catRequest);
        let subscription = this.popupService.onConfirmCommunication.subscribe(resp => {
          subscription.unsubscribe();
          let status = this.statusList.find(stat => stat.value == resp.theme.Specs[0].action);
          var date = new Date();
          date.setDate(date.getDate() + 1);
          let workFlowRequest = { catRequest: { id: this.catRequest.id, activeDate: date }, status: status, comment: resp.comment } as CatRequestWorkFlow;
          //service WF
          this.catalogAddRequestWorkflowService.saveCatRequestWorkflow(workFlowRequest).subscribe(data => {
            //to lock the icon communication
            this.dataSharing.changeStatus("true");
            this.communicationSubscription.unsubscribe();
            //redirection after confirming
            this.router.navigate(['/catalog'])
          });
        })
      })
    } else {
      this.isAddRequest = false;
      let path = this.router.url.split('/')[3];
      if (path == 'add') {
        this.isAddMode = true;
      }
      else {
        this.isAddMode = false;

      }
    }

  }
  initComponent(param) {
    if (this.isAddRequest) {
      this.isAddMode = false
    }
    if (this.isAddMode) {
      this.assetType = {} as CatAssetTypeModel;

      this.showForm = false;
      forkJoin([
        this.catBrandService.getCatBrandById(param.id).pipe(catchError(error => of(null))),

      ]).subscribe(res => {

        this.catBrand = res[0];
        this.initComponentForkJoin(this.catBrand)

      }, (err) => {
        console.error(err);
      }, () => {
        this.modifModeActivated = false;
        this.toggleModifState(true)
      });



    } else if (this.isAddRequest) {

      forkJoin([
        this.catActivitySectorService.getActivitySectorList().pipe(catchError(error => of([])))
        , this.catNatureService.getNatureListByCatActivitySectorID(history.state.catRequest.idSector).pipe(catchError(error => of([])))
        , this.catBrandService.getCatBrandListByCatNatureId(history.state.catRequest.idNature).pipe(catchError(error => of([])))
        , this.paramSettingService.getParamByCode(StatusConstant.STATUS_RQST_WKFL).pipe(catchError(error => of([])))

      ]).subscribe(res => {


        this.cactivitySectorOptions = res[0];
        this.natureOptions = res[1];
        this.brandOptions = res[2];
        this.statusList = res[3];
        let catBrandModel: CatBrandModel = this.brandOptions.find(n => n.id == history.state.catRequest.idBrand);
        this.catBrand = catBrandModel;

        this.initComponentForkJoin(this.catBrand)
      }, (err) => {
        console.error(err);
      }, () => {
        this.searchAssettypeNatifDocsForRequest()
        this.modifModeActivated = false;
        this.toggleModifState(true)
      });
    }
    else {
      // view mode

      this.showForm = false;

      this.catAssetTypeService.getAssetTypeById(param.id).subscribe(res => {

        this.assetType = res;
        this.assertTypeTitle = this.assetType.catBrand.catNature.catActivitySector.label;
        // Set titles to herited Doc
        this.natureTitle = this.assetType.catBrand.catNature.label;
        this.sectorTitle = this.assetType.catBrand.catNature.catActivitySector.label;
        this.catBrandTitle = this.assetType.catBrand.label;
        this.catalogService.fillInfoCreationUser(this.assetType.createdBy).subscribe(result => {
          this.creationUser = result.collaboratorFullName;
        })

        this.catalogService.fillInfoCreationUser(this.assetType.updatedBy).subscribe(rest => {
          this.updateUser = rest.collaboratorFullName;
        })

      }, err => {
        console.error(err);
      }, () => {
        this.typeForm = this.fb.group({
          generalities: this.newGeneralities(this.assetType)
        });
        this.searchAssettypeNatifDocs();
        this.showForm = true;

      });
      this.modifModeActivated = false;




    }
  }

  initComponentForkJoin(catBrand: CatBrandModel) {
    this.catBrand = catBrand;
    this.assetType.catBrand = this.catBrand
    if (this.catBrand && this.catBrand.catNature && this.catBrand.catNature.catActivitySector) {
      this.assertTypeTitle = this.catBrand.catNature.catActivitySector.label;
    }

    // Set titles to herited Doc

    this.natureTitle = this.catBrand.catNature.label;
    this.sectorTitle = this.catBrand.catNature.catActivitySector.label;
    this.catBrandTitle = this.catBrand.label;

    this.typeForm = this.fb.group({
      generalities: this.addGeneralities(this.catBrand)

    });

    this.updatedAssetTypeDocs = [];
    this.updatedAssetTypeDocsIndex = [];
    this.deletedDocumentList = [];
    if (this.assetTypeDocs != null && this.assetTypeDocs.length > 0) {
      this.assetTypeDocs.forEach((element, index) => {

        this.updatedAssetTypeDocs.push(Object.assign({}, element))
        this.editedDocumentList[index] = false;
      });
    }
    this.getInhertedDocsForAdd(this.catBrand);

    this.showForm = true;
  }
  changeTab(i: number) {
    this.selectedTabulation = i;
  }

  private newGeneralities(assetType: CatAssetTypeModel): FormGroup {
    return this.fb.group({
      libelle: [{ value: assetType ? assetType.label : '', disabled: true }, Validators.required],
      comment: [{ value: assetType ? assetType.comment : '', disabled: true }],
      files: [{ value: null, disabled: true }, { validators: [documentsValidator] }],
      commentBrand: [{ value: assetType ? assetType.catBrand.comment : '', disabled: true },],
      commentNature: [{ value: assetType ? assetType.catBrand.catNature.comment : '', disabled: true },],
      commentSector: [{ value: assetType ? assetType.catBrand.catNature.catActivitySector.comment : '', disabled: true },],
    });

  }
  private addGeneralities(catBrand: CatBrandModel): FormGroup {
    return this.fb.group({
      libelle: [{ value: history.state.catRequest ? history.state.catRequest.label : '', disabled: true }, Validators.required],
      comment: [{ value: history.state.catRequest ? history.state.catRequest.comment : '', disabled: true }],
      files: [{ value: null, disabled: true }, { validators: [documentsValidator] }],
      commentBrand: [{ value: catBrand ? catBrand.comment : '', disabled: true },],
      commentNature: [{ value: catBrand ? catBrand.catNature.comment : '', disabled: true },],
      commentSector: [{ value: catBrand ? catBrand.catNature.catActivitySector.comment : '', disabled: true },],
      natureName: [{ value: catBrand ? catBrand.catNature.label : '', disabled: true }],
      secteurName: [{ value: catBrand ? catBrand.catNature.catActivitySector.label : '', disabled: true }],
      brandName: [{ value: catBrand ? catBrand.label : '', disabled: true }],
      sector: [{ value: catBrand ? catBrand.catNature.catActivitySector.label : '', disabled: false }, Validators.required],
      nature: [{ value: catBrand ? catBrand.catNature.label : '', disabled: false }, Validators.required],
      brand: [{ value: catBrand ? catBrand.label : '', disabled: false }, Validators.required],

    });

  }

  toggleModifState(isModif) {
    this.modifModeActivated = isModif;
    if (this.modifModeActivated) {
      this.typeForm.get('generalities').get('libelle').enable();
      this.typeForm.get('generalities').get('comment').enable();
      this.typeForm.get('generalities').get('files').enable();
    } else {
      this.modifModeActivated = true;
      this.popupService.popupInfo(
        'Êtes-vous certain de vouloir annuler votre saisie ? Les paramètres initiaux seront restaurés.', 'OUI', null, 'NON', ConfirmationModalComponent);
      const subscription = this.popupService.onConfirm.subscribe((t) => {
        if (t) {
          if (!this.isAddMode) {
            if (this.isAddRequest) {
              this.router.navigate(['catalog']);

            } else {
              this.typeForm.get('generalities').reset({
                libelle: this.assetType ? this.assetType.label : '',
                comment: this.assetType ? this.assetType.comment : '',
                files: { files: [] },
                commentBrand: this.assetType ? this.assetType.catBrand.comment : '',
                commentNature: this.assetType ? this.assetType.catBrand.catNature.comment : '',
                commentSector: this.assetType ? this.assetType.catBrand.catNature.catActivitySector.comment : '',

              });
              this.typeForm.get('generalities').get('libelle').disable();
              this.typeForm.get('generalities').get('comment').disable();
              this.typeForm.get('generalities').get('files').disable();
            }

            this.updatedAssetTypeDocs = [];
            this.updatedAssetTypeDocsIndex = [];
            this.deletedDocumentList = [];
            this.assetTypeDocs.forEach((element, index) => {

              this.updatedAssetTypeDocs.push(Object.assign({}, element))
              this.editedDocumentList[index] = false;
            });

            subscription.unsubscribe();
          } else {

            this.router.navigate(['catalog', 'brand', this.catBrand.id]);
          }
          this.modifModeActivated = false;

        }
      })
    }
  }


  save() {
    this.typeForm.markAllAsTouched();
    this.isSubmited = true;

    if (this.typeForm.valid) {
      this.popupService.popupInfo(
        'Êtes-vous certain de vouloir enregistrer votre saisie ? Les paramètres initiaux seront remplacés.', 'OUI', null, 'NON', ConfirmationModalComponent);
      const subscription = this.popupService.onConfirm.subscribe((t) => {
        if (t) {


          this.assetType.label = this.typeForm.getRawValue().generalities.libelle;
          this.assetType.comment = this.typeForm.getRawValue().generalities.comment;
          this.assetType.isActive = true;
          if (this.catRequest && this.catRequest.id) {
            this.assetType.catRequest = { id: this.catRequest.id } as CatRequestModel

          }

          this.catAssetTypeService.saveAssetType(this.assetType).subscribe(data => {
            this.assetType = data;
            this.node.data = this.assetType.id;
            this.node.label = this.assetType.label;
            this.node.nodeType = 'type';
            this.node.type = 'type';
            this.node.parent = {} as TreeNode;


            this.node.parent.data = this.assetType.catBrand.id;
            this.node.parent.parent = {} as TreeNode;
            this.node.parent.parent.data = this.assetType.catBrand.catNature.id;
            this.node.parent.parent.parent = {} as TreeNode;
            this.node.parent.parent.parent.data = this.assetType.catBrand.catNature.catActivitySector.id;
            this.node.icon = 'pi pi-file';

            if (this.isAddRequest) {
              this.paramSettingService.getParamByCode(StatusConstant.STATUS_RQST_WKFL).subscribe(data => {
                let status = data.find(stat => stat.value == StatusConstant.STATUS_RQST_WKFL_VAL);
                var date = new Date();
                date.setDate(date.getDate());


                let workFlowRequest = { catRequest: { id: this.catRequest.id, activeDate: date }, status: status } as CatRequestWorkFlow;

                this.catalogAddRequestWorkflowService.saveCatRequestWorkflow(workFlowRequest).subscribe();
              })
              // this.saveTrest(this.assetType)
            }
          }, err => {
            console.error(err)
          }, () => {
            this.modifModeActivated = false;
            this.typeForm.get('generalities').get('libelle').disable();
            this.typeForm.get('generalities').get('comment').disable();
            this.typeForm.get('generalities').get('files').disable();
            this.updateGeneralitiesDocs(this.node);

          });



        }

        subscription.unsubscribe();
      });

      this.isSubmited = false;



    }
    else {
      this.popupService.popupInfo(
        "Merci de bien vouloir renseigner l'intégralité des informations demandées.", null, null, null, ConfirmationModalComponent);
      const subscriptionPopup = this.popupService.onConfirm.subscribe((t) => {

        subscriptionPopup.unsubscribe();

      });
    }
  }
  isFormValid(index) {
    if (index.toString() === '0') {
        return this.typeForm.controls.generalities.valid;
      
    }else{
      return null;
    }
  }
  deleteType() {

    this.route.params.subscribe(param => {

      if (new Date(this.assetType.createdDate).setHours(0, 0, 0, 0) == new Date().setHours(0, 0, 0, 0)) {
        this.popupService.popupInfo(
          'Voulez-vous supprimer cet élément ? Si vous confirmez, l\'élément sera supprimé ?', 'OUI', null, 'NON', ConfirmationModalComponent);

      } else {
        this.popupService.popupInfo(
          'Voulez-vous désactiver cet élément ? Si vous confirmez, l\'élément sera conservé dans le catalogue mais ne sera plus visible ?', 'OUI', null, 'NON', ConfirmationModalComponent);

      }
      const subscription = this.popupService.onConfirm.subscribe((t) => {
        // OUI
        if (t) {


          this.catAssetTypeService.deleteAssetType(param.id).subscribe(data => {
            //this is intentional
          },
            (err) => { console.error(err); },
            () => {

              let node = {} as TreeNode;
              node.data = this.assetType.id;
              node.label = this.assetType.label;
              node.type = 'type';

              node.parent = {} as TreeNode;
              node.parent.data = this.assetType.catBrand.id;
              node.parent.parent = {} as TreeNode;
              node.parent.parent.data = this.assetType.catBrand.catNature.id;
              node.parent.parent.parent = {} as TreeNode;
              node.parent.parent.parent.data = this.assetType.catBrand.catNature.catActivitySector.id;

              this.nodeService.nodeDeleteSubject.next(node);

              this.router.navigate(['catalog', 'brand', this.assetType.catBrand.id]);
            });
        }
        subscription.unsubscribe();
      });
    })
  }


  editDocumentName(index: number) {
    this.editedDocumentList[index] = true;
    if (this.isAddRequest) {
      this.documentName = this.updatedAssetTypeRequestDocs[index].docName;
    } else {
      this.documentName = this.updatedAssetTypeDocs[index].docName;
    }
  }





  deleteDocument(index: number) {

    this.popupService.popupInfo('Êtes-vous sûr(e) de vouloir supprimer la pièce jointe ?', 'Confirmer', null, 'Annuler', ConfirmationModalComponent);
    const subscription = this.popupService.onConfirm.subscribe((t) => {

      if (t) {

        if (this.isAddRequest) {
          if (this.updatedAssetTypeRequestDocs[index].id != null) {
            this.updatedAssetTypeRequestDocs.splice(index, 1);
          }

        } else {
          if (this.updatedAssetTypeDocs[index].id != null) {
            this.deletedDocumentList.push(this.updatedAssetTypeDocs[index]);
          }
          this.updatedAssetTypeDocs.splice(index, 1);
        }
      }
      subscription.unsubscribe();

    })
  }


  onBlurDocument(index: number) {

    if (this.isAddRequest) {
      this.updatedAssetTypeRequestDocs[index].docName = this.documentName;

    } else {
      this.updatedAssetTypeDocs[index].docName = this.documentName;
      if (this.updatedAssetTypeDocsIndex.findIndex(val => val == index) == -1) {
        this.updatedAssetTypeDocsIndex.push(index);
      }

    }

    this.editedDocumentList[index] = false;



  }


  searchInhertedDocs(assetType: CatAssetTypeModel) {
    this.catDocumentService.searchInheratedDocuments(assetType.catBrand.catNature.catActivitySector.id,
      assetType.catBrand.catNature.id, assetType.catBrand.id, 'G').subscribe(data => {
        this.partInheritedDocs(data);

      });

  }

  getInhertedDocsForAdd(catBrand: CatBrandModel) {

    this.catDocumentService.searchInheratedDocuments(catBrand.catNature.catActivitySector.id,
      catBrand.catNature.id, catBrand.id, 'G').subscribe(data => {
        this.partInheritedDocs(data);
      });

  }
  partInheritedDocs(inheritedDocs: CatDocumentModel[]) {
    if (inheritedDocs) {
      this.inheritedDocsBrand = inheritedDocs.filter(doc => doc.catBrand != null && doc.catAssetType == null);
      this.inheritedDocsNature = inheritedDocs.filter(doc => doc.catNature != null && doc.catBrand == null);
      this.inheritedDocsSector = inheritedDocs.filter(doc => doc.catNature == null && doc.catBrand == null && doc.catAssetType == null);
    }

  }



  //save delete update docs
  updateGeneralitiesDocs(node) {
    let files: File[]
    if (this.typeForm.getRawValue().generalities.files) {
      files = this.typeForm.getRawValue().generalities.files.files;
    }

    let request = [];
    if (files && files.length > 0) {
      files.forEach(file => request.push(this.catDocumentService.uploadDocument(file, this.assetType.catBrand.catNature.catActivitySector.id,
        this.assetType.catBrand.catNature.id, this.assetType.catBrand.id, this.assetType.id, 'G').pipe(catchError(error => of(null)))))
    }

    if (this.updatedAssetTypeDocsIndex.length > 0) {
      let docs = [];
      this.updatedAssetTypeDocsIndex.forEach(index => docs.push(this.updatedAssetTypeDocs[index]));
      docs.forEach(doc => request.push(this.catDocumentService.updateDocument(doc).pipe(catchError(error => of(null)))))

    }

    if (this.deletedDocumentList.length > 0) {
      this.deletedDocumentList.forEach(doc => request.push(this.catDocumentService.deleteCatDocument(doc).pipe(catchError(error => of(null)))))
    }

    if (this.isAddRequest) {
      if (this.updatedAssetTypeRequestDocs && this.updatedAssetTypeRequestDocs.length > 0) {
        this.updatedAssetTypeRequestDocs.forEach(res => {

          request.push(this.catDocumentService.saveDocumentRequest(this.assetType.catBrand.catNature.catActivitySector.id,
            this.assetType.catBrand.catNature.id, this.assetType.catBrand.id, this.assetType.id, 'G', res.gedPath, res.docName).pipe(catchError(error => of(null))))
        }
        )

      }
    }

    forkJoin(request).subscribe(res => {
      //this is intentional
    }, () => {
      //this is intentional

    }, () => {
      this.typeForm.controls['generalities'].get('files').reset();
      this.searchAssettypeNatifDocs();
      if (this.isAddMode || this.isAddRequest) {
        this.nodeService.nodeAddSubject.next(node);
        this.router.navigate(['catalog', 'type', this.assetType.id]);
      }

      if (!this.isAddMode) {
        this.nodeService.nodeSubject.next(node);
      }
    });
  }
  searchAssettypeNatifDocsForRequest() {
    if (this.isAddRequest) {

      this.catRequestService.loadCatRequestDocs(history.state.catRequest.id).subscribe(res => {

        this.assetTypeRequestDocs = res;
      }, err => {
        console.error(err);
      }, () => {
        this.updatedAssetTypeRequestDocs = [];
        this.updatedAssetTypeDocsIndex = [];
        this.deletedDocumentList = [];
        this.assetTypeRequestDocs.forEach((element, index) => {

          this.updatedAssetTypeRequestDocs.push(Object.assign({}, element))
          this.editedDocumentList[index] = false;
        });
        this.searchInhertedDocs(this.assetType);
      });

    }
  }
  searchAssettypeNatifDocs() {

    this.catDocumentService.searchDocument(this.assetType.catBrand.catNature.catActivitySector.id, this.assetType.catBrand.catNature.id,

      this.assetType.catBrand.id, this.assetType.id, 'G').subscribe(res => {
        this.assetTypeDocs = res;
      }, err => {
        console.error(err);
      }, () => {
        this.updatedAssetTypeDocs = [];
        this.updatedAssetTypeDocsIndex = [];
        this.deletedDocumentList = [];
        this.assetTypeDocs.forEach((element, index) => {

          this.updatedAssetTypeDocs.push(Object.assign({}, element))
          this.editedDocumentList[index] = false;
        });
        this.searchInhertedDocs(this.assetType);

      });
  }



  downloadDocument(documentToDownload: CatDocumentModel) {
    this.catDocumentService.downloadDocument(documentToDownload.gedPath, documentToDownload.docName).subscribe(response => {
      var file = new Blob([response], { type: 'application/*' })
      var fileURL = URL.createObjectURL(file);
      const doc = document.createElement('a');
      doc.download = documentToDownload.docName;
      doc.href = fileURL;
      doc.dataset.downloadurl = [doc.download, doc.href].join(':');
      document.body.appendChild(doc);
      doc.click();
    });
  }

  resetCatSector() {

    this.typeForm.get('generalities').get('nature').patchValue(null)
    this.typeForm.get('generalities').get('brand').patchValue(null)
    this.typeForm.get('generalities').get('nature').disable();
    this.typeForm.get('generalities').get('brand').disable();

    this.brandOptions = null
    this.natureOptions = null
    this.resetFileAndComment();
  }

  lodCatNatureListBySectorID() {
    this.typeForm.get('generalities').get('nature').enable();
    this.typeForm.get('generalities').get('nature').patchValue(null)
    this.typeForm.get('generalities').get('nature').markAsTouched();
    this.typeForm.get('generalities').get('brand').patchValue(null)
    this.typeForm.get('generalities').get('brand').disable();
    this.brandOptions = null
    let id = this.typeForm.get('generalities').get('sector').value;
    this.catNatureService.getNatureListByCatActivitySectorID(id).subscribe(data => {
      this.natureOptions = data;
    });
    this.resetFileAndComment();
  }


  lodCatBrandListByCatNatureID() {
    this.typeForm.get('generalities').get('brand').enable();
    this.typeForm.get('generalities').get('brand').patchValue(null)
    this.typeForm.get('generalities').get('brand').markAsTouched();
    let id = this.typeForm.get('generalities').get('nature').value;
    this.catBrandService.getCatBrandListByCatNatureId(id).subscribe(data => {
      this.brandOptions = data;

    });
    this.resetFileAndComment();
  }
  resetCatBrand() {
    this.resetFileAndComment();

  }
  resetCatNature() {

    this.typeForm.get('generalities').get('nature').patchValue(null)
    this.typeForm.get('generalities').get('brand').patchValue(null)
    this.typeForm.get('generalities').get('brand').disable();

    this.brandOptions = null

    this.resetFileAndComment();

  }
  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
    if (this.communicationSubscription && !this.communicationSubscription.closed) {
      this.communicationSubscription.unsubscribe();

    }
  }



  changeBrand() {
    if (this.brandOptions) {
      let catBrandModel: CatBrandModel = this.brandOptions.find(n => n.id == this.typeForm.get('generalities').get('brand').value);
      this.assetType.catBrand = catBrandModel
      this.typeForm.get('generalities').get('commentBrand').patchValue((catBrandModel.comment) ? catBrandModel.comment : null);
      this.typeForm.get('generalities').get('commentNature').patchValue((catBrandModel.catNature.comment) ? catBrandModel.catNature.comment : null)
      this.typeForm.get('generalities').get('commentSector').patchValue((catBrandModel.catNature.catActivitySector.comment) ? catBrandModel.catNature.catActivitySector.comment : null);
      this.getInhertedDocsForAdd(this.assetType.catBrand);


    }
  }

  resetFileAndComment() {
    this.assetType.catBrand = null
    this.typeForm.get('generalities').get('commentBrand').patchValue("")
    this.typeForm.get('generalities').get('commentNature').patchValue("")
    this.typeForm.get('generalities').get('commentSector').patchValue("")

    this.inheritedDocsBrand = null;
    this.inheritedDocsNature = null;
    this.inheritedDocsSector = null;
  }

}
