import { FormBuilder, FormGroup } from '@angular/forms';
import { BehaviorSubject, forkJoin, of, Subject } from 'rxjs';
import { CatActivitySectorModel } from '../../../shared/models/cat-activity-sector.model';
import { CatAssetTypeModel } from '../../../shared/models/cat-asset-type.model';
import { CatBrandModel } from '../../../shared/models/cat-brand.model';
import { CatDocumentModel } from '../../../shared/models/cat-document.model';
import { CatNatureModel } from '../../../shared/models/cat-nature.model';
import { CatRequestWaiting } from '../../../shared/models/catalog-add-request.model';
import { CatRequestDoc } from '../../../shared/models/catalog-request-doc.model';
import { ParamSettingModel } from '../../../shared/models/param-setting.model';
import { StatusConstant } from '../../../shared/status-constant';
import { MenuCatTypeComponent } from './menu-cat-type.component';




let catRequest: CatRequestWaiting = {} as any;

catRequest = {
    'id': '50',

    'catActivitySector': 'sector',

    'catNature': 'nature',

    'catBrand': 'brand',

    'idSector': '5',

    'idNature': '5',

    'idBrand': '5',

    'requestType': 'type',

    'workflowCode': '5',

    'activeDate': null,

    'label': 'test',

    'comment': 'comment',

    'workflowStatus': 'status'

} as CatRequestWaiting

let catassetType: CatAssetTypeModel = {} as any;

let catActivitySectorModelList: CatActivitySectorModel[] = [{

    id: '5',
    label: 'Transport',
    comment: 'comment act sector',
    isActive: true
} as CatActivitySectorModel];




let catRequestDocList: CatRequestDoc[] = [{

    'id': '5',
    'catRequest': catRequest,

    'discriminator': 'descr',
    'docName': 'doc',
    'docVersion': 1,
    'gedPath': 'path',


} as CatRequestDoc]

let updatedAssetTypeRequestDocs: CatRequestDoc[] = [{

    'id': '5',
    'catRequest': catRequest,

    'discriminator': 'descr',
    'docName': 'doc',
    'docVersion': 1,
    'gedPath': 'path',


} as CatRequestDoc]

let catNatureModelList: CatNatureModel[] = [{

    id: '5',
    label: 'nature label',
    catActivitySector: {
        id: '5',
        label: 'Transport',
        comment: 'comment act sector',
        isActive: true

    },
    comment: 'comment nature',
    paramNap: null,
    isActive: true
} as CatNatureModel];

let catBrandModelList: CatBrandModel[] = [{

    id: '5',
    label: 'QSD',
    catNature: {
        id: '5',
        label: 'nature label',
        catActivitySector: {
            id: '5',
            label: 'Transport',
            comment: 'comment act sector',
            isActive: true

        },
        comment: 'comment nature',
        paramNap: null,
        isActive: true

    },
    comment: 'commentaire brand',
    isActive: true
} as CatBrandModel];

catassetType = {
    catBrand: {
        id: '122',
        label: 'QSD',
        catNature: {
            id: '133',
            label: 'nature label',
            catActivitySector: {
                id: '1',
                label: 'Transport',
                comment: 'comment act sector',
                isActive: true

            },
            comment: 'comment nature',
            paramNap: null,
            isActive: true,
            isValid: false,
            isEnrg: false

        },
        comment: 'commentaire brand',
        isActive: true,
        catRequest: null

    },
    id: '122',
    label: 'QSD',
    comment: 'commentaire brand',
    isActive: true,
    catRequest: null


}

let documentsList: CatDocumentModel[] = [{
    "id": "1",
    "docName": "document1",
} as CatDocumentModel, {
    "id": "2",
    "docName": "document2"

} as CatDocumentModel];

let catDocumentServiceMock: any;




describe('MenuCatTypeComponent', () => {
    let fixture: MenuCatTypeComponent;
    let popupServiceMock: any;
    let routerMock: any;
    let formBuilderMock: FormBuilder;
    let catAssetTypeServiceMock: any;
    let nodeServiceMock: any;
    let routeMock: any;
    let catalogAddRequestWorkflowService: any;
    let catRequestServiceMock: any;
    let paramSettingServiceMock: any;
    let catBrandServiceMock: any;
    let catalogServiceMock: any;
    let catNatureServiceMock: any;
    let catActivitySectorServiceMock: any;
    let catBrandObject: any;
  
    let catNature: any;
    let catActivitySector: any;
    let paramNap: any;
    let dataSharingMock: any;
    let documents: CatDocumentModel[] = [{
        id: '1',
        catActivitySector: null,
        catNature: null,
        catBrand: null,
        catAssetType: null,
        discriminator: "G",
        docName: "testa doc aziz",
        docVersion: 2,
        gedPath: "string",
        isActive: true,
    } as CatDocumentModel]

    let paramSettingModel: ParamSettingModel[]=  [{
        'id': '5',
        'code': 'STATUS_VAL_WKFL',
        'label': 'Validée',
        'value': 'STATUS_RQST_WKFL_VAL',
        'effectiveDate': null,
        'isActiv': true,
    } as ParamSettingModel]
    
    beforeEach(() => {

      

        catBrandObject = {
            id: '5',
            label: 'QSD',
            catNature: catNature,
            comment: 'commentaire brand',
            isActive: true
        }
        catNature = {
            id: '5',
            label: 'nature label',
            catActivitySector: catActivitySector,
            comment: 'comment nature',
            paramNap: paramNap,
            isActive: true

        }
        paramNap = {
            id: '1',
            label: 'label'
        }
        catActivitySector = {
            id: '5',
            label: 'Transport',
            comment: 'comment act sector',
            isActive: true
        }

        nodeServiceMock = {
            nodeSubject: new Subject<any>()

        }


        popupServiceMock = {
            popupInfo: jest.fn().mockReturnValue(of(true)),
            popupCommunication: jest.fn().mockReturnValue(of(true)),
            onConfirmCommunication: new Subject(),
            onConfirm: of(true)
        };



        catDocumentServiceMock = {
            uploadDocument: jest.fn().mockReturnValue(of(true)),
            searchDocument: jest.fn().mockReturnValue(of(documents)),
            downloadDocument: jest.fn().mockReturnValue(of(true)),
            searchInheratedDocuments: jest.fn().mockReturnValue(of(documents))
        }

        catRequestServiceMock = {
            loadCatRequestDocs: jest.fn().mockReturnValue(of(catRequestDocList))
        }
        catActivitySectorServiceMock = {
            getActivitySectorList: jest.fn().mockReturnValue(of(catActivitySectorModelList))
        }
        catNatureServiceMock = {
            getNatureListByCatActivitySectorID: jest.fn().mockReturnValue(of(catNatureModelList)),
        }
        catBrandServiceMock = {
            getCatBrandById: jest.fn().mockReturnValue(of(catBrandObject)),
            getCatBrandListByCatNatureId: jest.fn().mockReturnValue(of(catBrandModelList)),
        }
        catalogServiceMock = {
            fillInfoCreationUser: jest.fn().mockReturnValue(of("Batch"))
          }
        catalogAddRequestWorkflowService = {
            saveCatRequestWorkflow: jest.fn().mockReturnValue(of(null)),
        }

        paramSettingServiceMock = {
            getParamByCode: jest.fn().mockReturnValue(of(paramSettingModel)),

        }
        dataSharingMock = {
            changeStatus: jest.fn().mockImplementation(() => { }),
            lockBtnStatus: new BehaviorSubject(false),
            triggerCommunication: new Subject()

        }
        formBuilderMock = new FormBuilder();

        routerMock = {

            url: '/catalog/type/9158',
            navigate: jest.fn().mockReturnValue(new Promise(() => {
                //this is intentional
            }))
        }
        routeMock = {
            params: of({ id: '1' })
        }
        catAssetTypeServiceMock = {
            getAssetTypeById: jest.fn().mockReturnValue(of(catassetType)),
            getCatAssetTypeDocByCatAssetTypeId: jest.fn().mockReturnValue(of(documentsList)),
            saveAssetType: jest.fn().mockReturnValue(of(catassetType)),
            deleteAssetType: jest.fn().mockReturnValue(of(true))
        }

        fixture = new MenuCatTypeComponent(formBuilderMock, catAssetTypeServiceMock, routeMock, popupServiceMock,
            catBrandServiceMock, catNatureServiceMock, catActivitySectorServiceMock,
            nodeServiceMock, routerMock, catDocumentServiceMock, catalogAddRequestWorkflowService,
            catRequestServiceMock, paramSettingServiceMock, dataSharingMock,
            catalogServiceMock
        );


        fixture.ngOnInit();
        fixture.assetTypeDocs = [{
            id: '1'
        }as CatDocumentModel] 
    });
    describe('Test Component', () => {
        it('should Be Truthy', () => {
            expect(fixture).toBeTruthy();
        });
    });

    describe('Test: ngOnInit', () => {

        it('should variables be setted ', () => {
            fixture.assetTypeDocs = documentsList;

            fixture.updatedAssetTypeDocs = [];
            fixture.updatedAssetTypeDocsIndex = [];
            fixture.deletedDocumentList = [];
            fixture.ngOnInit();
            expect(fixture.initComponent).toBeTruthy();
        });

        it('should variables is addmod be setted to true ', () => {

            routerMock = {

                url: '/catalog/type/add/9158'

            }
            fixture = new MenuCatTypeComponent(formBuilderMock, catAssetTypeServiceMock, routeMock, popupServiceMock,
                catBrandServiceMock, catNatureServiceMock, catActivitySectorServiceMock,
                nodeServiceMock, routerMock, catDocumentServiceMock, catalogAddRequestWorkflowService,
                catRequestServiceMock, paramSettingServiceMock, dataSharingMock,
                catalogServiceMock);


            fixture.ngOnInit();
            expect(fixture.initComponent).toBeTruthy();
        });



        it('should editedDocumentList be false ', () => {

            fixture.isAddMode = true;
            fixture.updatedAssetTypeDocs = [];
            fixture.updatedAssetTypeDocsIndex = [];
            fixture.deletedDocumentList = [];

            fixture.initComponent('1');
            expect(fixture.editedDocumentList[0]).toEqual(false);
        });

    });


    describe('Test changeTab()', () => {
        it('should be 3', () => {
            fixture.changeTab(3);
            expect(fixture.selectedTabulation).toEqual(3);
        })
    });
    describe('Test toggle modif state', () => {
        it('should enable form', async () => {


            await routerMock.param;
            forkJoin(catAssetTypeServiceMock.getAssetTypeById('1'),
                catAssetTypeServiceMock.getCatAssetTypeDocByCatAssetTypeId('1')).subscribe(res => {
                    fixture.toggleModifState(true);

                    expect(fixture.typeForm.get('generalities').get('libelle').enabled).toEqual(true);
                })


        });

        it('should disable form', async () => {
            await routerMock.param;
            forkJoin(catAssetTypeServiceMock.getAssetTypeById('1'),
                catAssetTypeServiceMock.getCatAssetTypeDocByCatAssetTypeId('1')).subscribe(async res => {
                    fixture.toggleModifState(false);
                    await popupServiceMock.popupInfo;
                    await popupServiceMock.onConfirm;

                    expect(fixture.typeForm.get('generalities').get('libelle').disabled).toEqual(true);
                })
        });
    });


    describe('Test save()', () => {
        it('should call popup', async () => {
            await routerMock.param;
            forkJoin(catAssetTypeServiceMock.getAssetTypeById('1'),
                catAssetTypeServiceMock.getCatAssetTypeDocByCatAssetTypeId('1')).subscribe(res => {
                    fixture.save();
                    expect(catDocumentServiceMock.uploadDocument).toHaveBeenCalled();
                    expect(catDocumentServiceMock.uploadDocument).toHaveBeenCalled();

                    expect(popupServiceMock.popupInfo).toHaveBeenCalled();
                });

        });

    });
    describe('Test save with pop()', () => {
        it('should call popup', async () => {
            await routerMock.param;
            fixture.toggleModifState(true);
            fixture.typeForm = formBuilderMock.group({
                generalities: formBuilderMock.group({
                    files: [{ files: [{ name: 'dd' } as File] }],
                })
            });
            fixture.assetTypeDocs = documentsList;
            fixture.deletedDocumentList[0] = documentsList[0];
            fixture.save();
            await popupServiceMock.popupInfo
            await popupServiceMock.onConfirm

            expect(popupServiceMock.popupInfo).toHaveBeenCalled();

        });

    });

    describe('Test editDocumentName', () => {
        it('should be 3', () => {
            fixture.editDocumentName(0);
            expect(fixture.editedDocumentList[0]).toEqual(true);
        })
    });
    describe('Test:  editDocumentName', () => {
        it('should set doc edited value to true ', () => {
            fixture.assetTypeDocs = documentsList;
            fixture.updatedAssetTypeDocs = documentsList;
            fixture.editedDocumentList = [false, false];
            fixture.documentName = "test"
            fixture.editDocumentName(1)
            expect(fixture.editedDocumentList[1]).toEqual(true);

        });

    });
    describe('Test:  deleteDocs', () => {
        it('should remove docs ', () => {
            fixture.isAddRequest = true;
            fixture.deleteDocument(0);

            expect(popupServiceMock.popupInfo).toHaveBeenCalled();
            /* popupServiceMock.onConfirm.subscribe((t) => {
 
                 expect(fixture.deletedDocumentList.length).toEqual(1);
             })*/



        });

    });

    describe('Test:  onBlurDocument', () => {
        it('should update edited value of doc index ', () => {

            fixture.updatedAssetTypeDocs = documentsList;
            fixture.editedDocumentList = [false, true];

            fixture.onBlurDocument(0);
            expect(fixture.editedDocumentList[0]).toEqual(false);

        });

    });

    describe('Test:  onBlurDocName', () => {
        it('should update edited value of doc index ', () => {

            fixture.assetTypeDocs = documentsList;
            fixture.editedDocumentList = [false, true];

            fixture.onBlurDocument(0);
            expect(fixture.editedDocumentList[0]).toEqual(false);

        });

    });

    describe('Test deleteBrand', () => {
        it('should call deleteBrand', () => {
            fixture.deleteType();
            expect(popupServiceMock.popupInfo).toHaveBeenCalled();
        })
    });

    describe('Test:  download doc', () => {
        it('should download doc ', () => {
            let documentToDownload = {
                docName: 'file3.png', id: '1', catActivitySector: null, catNature: null, catBrand: null,
                catAssetType: null, discriminator: 'G', docVersion: 1, gedPath: '17/file3.png'
            } as CatDocumentModel


            fixture.downloadDocument(documentToDownload);

        });

    });


    describe('test:partInheritedDocs ', () => {
        it('should part inheritedDocs', () => {
            let files = [{
                docName: 'file3.png', id: '1', catActivitySector: {
                    id: '1',
                    label: 'Transport',
                    comment: 'comment act sector',
                    isActive: true
                }
                , catNature: null, catBrand: null,
                catAssetType: null, discriminator: 'G', docVersion: 1, gedPath: '17/file3.png'
            } as CatDocumentModel,
            {
                docName: 'file4.png', id: '2', catActivitySector: {
                    id: '1',
                    label: 'Transport',
                    comment: 'comment act sector',
                    isActive: true
                }, catNature: null, catBrand: null,
                catAssetType: null, discriminator: 'G', docVersion: 1, gedPath: '17/file4.png'
            } as CatDocumentModel,
            {
                docName: 'file5.png', id: '3', catActivitySector: {
                    id: '1',
                    label: 'Transport',
                    comment: 'comment act sector',
                    isActive: true
                }, catNature: null, catBrand: null,
                catAssetType: null, discriminator: 'G', docVersion: 1, gedPath: '17/file5.png'
            } as CatDocumentModel,
            {
                docName: 'file6.png', id: '4', catActivitySector: {
                    id: '1',
                    label: 'Transport',
                    comment: 'comment act sector',
                    isActive: true
                }, catNature: null, catBrand: null,
                catAssetType: null, discriminator: 'G', docVersion: 1, gedPath: '17/file6.png'
            } as CatDocumentModel
            ]

            fixture.partInheritedDocs(files);
            expect(fixture.inheritedDocsSector).toEqual(files);
        });
    })




    describe('Test: isAddRequest', () => {
        it('ngOnInit for Add request ', () => {
            routerMock = {

                url: '/catalog/type/add'

            }


            fixture = new MenuCatTypeComponent(formBuilderMock, catAssetTypeServiceMock, routeMock, popupServiceMock,
                catBrandServiceMock, catNatureServiceMock, catActivitySectorServiceMock,
                nodeServiceMock, routerMock, catDocumentServiceMock, catalogAddRequestWorkflowService,
                catRequestServiceMock, paramSettingServiceMock, dataSharingMock,
                catalogServiceMock);

            window.history.pushState({ 'catRequest': catRequest }, '', '');


            fixture.ngOnInit();
            fixture.statusList = [{ id: '22', label: 'validé', value: StatusConstant.STATUS_RQST_WKFL_VAL } as ParamSettingModel]

            dataSharingMock.triggerCommunication.next(true);

            popupServiceMock.onConfirmCommunication.next({
                theme: {
                    Specs: [{ action: StatusConstant.STATUS_RQST_WKFL }]
                }
            })

            expect(fixture.initComponent).toBeTruthy();
        });

    });

    describe('all reset tests ', () => {
        beforeEach(() => {

            let catBrandModelObject = {
                id: '5',
                label: 'QSD',
                catNature: {
                    id: '133',
                    label: 'nature label',
                    catActivitySector: catActivitySector,
                    comment: 'comment nature',
                    paramNap: paramNap,
                    isActive: true

                },
                comment: 'commentaire brand',
                isActive: true,
            }
            catNature = {
                id: '133',
                label: 'nature label',
                catActivitySector: catActivitySector,
                comment: 'comment nature',
                paramNap: paramNap,
                isActive: true

            }

            let formbuild = new FormBuilder;
            let fixtureTypeForm: FormGroup;
            fixture.ngOnInit();
            function newGeneralities(): FormGroup {
                return formbuild.group({
                    libelle: [{ value: '' },],
                    comment: [{ value: '' }],
                    files: [{ value: null }],
                    commentBrand: [{ value: '', },],
                    commentNature: [{ value: '' },],
                    commentSector: [{ value: '', },],
                    sector: [catActivitySector,],
                    nature: [catNature.id,],
                    brand: [catBrandModelObject.id,],

                });
            }

            fixtureTypeForm = formbuild.group({
                generalities: newGeneralities(),
                decoteNew: [],
                decoteOld: [],
                decoteAvgNew: [],
                decoteAvgOld: []
            });
            fixture.typeForm = fixtureTypeForm

        });
        it('resetCatSector ', () => {

            fixture.resetCatSector();
            expect(fixture.brandOptions).toBeNull;
        });
        it('resetCatNature ', () => {


            fixture.resetCatNature();
            expect(fixture.brandOptions).toBeNull;
        });

        it('resetFileAndComment ', () => {

            fixture.resetFileAndComment();
            expect(fixture.typeForm.get('generalities').get('commentBrand').value).toEqual("");
        });

        it('changeBrand ', () => {

            fixture.changeBrand()
            expect(fixture.typeForm.get('generalities').get('commentBrand').value).toEqual('commentaire brand');

        });

        it('lodCatBrandListByCatNatureID ', () => {

            fixture.lodCatBrandListByCatNatureID();
            expect(fixture.typeForm.get('generalities').get('brand').enabled).toEqual(true);
            expect(fixture.typeForm.get('generalities').get('brand').value).toBeNull;

        });

        it('lodCatNatureListBySectorID ', () => {

            fixture.lodCatNatureListBySectorID();
            expect(fixture.typeForm.get('generalities').get('nature').enabled).toEqual(true);
            expect(fixture.typeForm.get('generalities').get('nature').value).toBeNull;

        });

        it('save for add request ', () => {

            fixture.save();

            expect(catalogAddRequestWorkflowService.saveCatRequestWorkflow).toHaveBeenCalled();

        });

    });

    describe('Test:  isFormValid', () => {
        it('should be equal to typeForm.controls.generalities.valid ', () => {
           expect(fixture.isFormValid(0)).toEqual(fixture.typeForm.controls.generalities.valid);
        });
        
        it('should be null', () => {
            expect(fixture.isFormValid(1)).toEqual(null);
        });

    });
});



