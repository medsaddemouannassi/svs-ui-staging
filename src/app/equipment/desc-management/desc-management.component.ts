import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { MdmInformationsModel } from 'src/app/shared/models/mdm-informations.model';
import { SvsDescModel } from 'src/app/shared/models/svs-desc.model';
import { ConfirmationModalComponent } from '../../shared/confirmation-modal/confirmation-modal.component';
import { autocompleteCustomValidator } from '../../shared/CustomValidators';
import { SvsDescCollabModel } from '../../shared/models/svs-desc-collab.model';
import { AuthService } from '../../shared/services/auth.service';
import { PopupService } from "../../shared/services/popup.service";
import { SharedCommunicationDataService } from '../../shared/services/shared-communication-data.service';
import { SvsDescCollabService } from '../../shared/services/svs-desc-collab.service';
import { SvsDescService } from '../../shared/services/svs-desc.service';
import { twoDigitDecimalRound } from '../../shared/ValuationUtils';


@Component({
  selector: 'app-desc-management',
  templateUrl: './desc-management.component.html',
  styleUrls: ['./desc-management.component.css']
})
export class DescManagementComponent implements OnInit {
  // List Networks in Svs
  descs: SvsDescModel[] = [];
  isLoading = false;
  managerNamesOptions: MdmInformationsModel[] = [];
  descCollabsOptions: MdmInformationsModel[] = [];
  // List Networks from Mdm
  mdmNetworks: SvsDescModel[] = [];
  selectedDesc: SvsDescModel;
  selectedDescCollab: SvsDescCollabModel;
  isDescSelected: boolean = false;
  descCollabForm: FormGroup;
  descForm: FormGroup;
  descCollabList: SvsDescCollabModel[];
  descCollabBtns = 'showNothing';
  descSubscription: Subscription;


  isDescChecked: boolean = false;
  isDescCollabChecked: boolean = false;
  isUpdate: boolean = false;
  isAddDesc = false;

  keyword = 'fullName';
  matKeyword = 'mat';
  maxAutorized = 2000000;
  mdmNetworksLoaded: boolean = false;
  userMat: string;

  constructor(
    private fb: FormBuilder,
    private svsDescService: SvsDescService,
    private svsDescCollabService: SvsDescCollabService,
    private popupService: PopupService,
    private authService: AuthService,
    private route: ActivatedRoute,
    private dataSharing: SharedCommunicationDataService,
  ) {
  }

  ngOnInit(): void {
    window.scrollTo(0, 0);
    this.dataSharing.changeStatus("true");

    // get current user matricule
    this.userMat = this.authService.identityClaims['mat'];

    this.descForm = this.fb.group({
      desc: [null, Validators.required],
      descName: ['', Validators.required],
      descManager: [null, [Validators.required, autocompleteCustomValidator]]
    });

    this.descCollabForm = this.fb.group({
      collaborator: [{ value: null, disabled: true }, [Validators.required, autocompleteCustomValidator]],
      desc: [{ value: '', disabled: true }, Validators.required],
      maxAmount: [{ value: null, disabled: true }, [Validators.required, Validators.min(0), Validators.max(this.maxAutorized)]],
      isManager: [false, Validators.required]
    });

    this.descCollabList = [];

    //check if role admin
    if (this.isAdmin()) {
      this.getAvailableDesc();

      let histId = this.route.snapshot.params.idHistory;

      if (histId) {
        this.svsDescService.getSvsDescList().subscribe(res => {
          this.descs = res;
          this.svsDescCollabService.getSvsDescHistory(histId).subscribe(history => {

            let desc = this.descs.find(expectedDesc => expectedDesc.id == history.descId);
            if (desc) {
              this.onSelectDesc(desc);

              if (history.operationCode != 'DELETE') {

                this.svsDescCollabService.getSvsDescCollabList(this.selectedDesc.id).subscribe(data => {
                  this.descCollabList = data;
                  let descCollab = this.descCollabList.find(expectedDescCollab => expectedDescCollab.collaboratorMatricul == history.collaboratorMatricul);
                  this.onSelectDescCollab(descCollab);

                })
              }

            }
          })
        })
      } else {
        this.loadDescs();


      }





    } else {
      //load all networks by Manager id
      this.loadDescByManagerId(this.userMat);

    }


  }

  getAvailableDesc() {
    this.svsDescService.getAvailableSvsDesc().subscribe((res) => {
      this.mdmNetworks = res;
    }, (err) => {
      console.error(err);
    }, () => {
      this.mdmNetworksLoaded = true;
    })
  }

  onBlurMaxAmount() {
    this.descCollabForm.patchValue({ maxAmount: twoDigitDecimalRound(this.descCollabForm.value.maxAmount.toString()) })
  }


  /*switching between Desc & desc Collab in details */
  onSelectDesc(selectedDesc: SvsDescModel): void {

    this.isDescSelected = true;
    let mdmManger = {} as MdmInformationsModel;
    if (selectedDesc.matriculeDescManager) {
      mdmManger.mat = selectedDesc.matriculeDescManager;
      mdmManger.fullName = selectedDesc.fullNameDescManager;
      this.managerNamesOptions = [mdmManger]

    } else {
      mdmManger = null;
    }



    this.descForm.patchValue({ 'desc': selectedDesc, 'descName': selectedDesc.descName, 'descManager': mdmManger });
    this.selectedDesc = selectedDesc;
    this.mdmNetworks = [selectedDesc];
    this.isDescChecked = true;
    this.isDescCollabChecked = false;
    this.descCollabBtns = 'editOrDeleteDescCollabBtns'
    this.descForm.disable();
    this.getDescCollabList(selectedDesc);
    this.selectedDescCollab = null;
  }
  onSelectDescCollab(selectedDescCollab: SvsDescCollabModel): void {

    let collaborator = {} as MdmInformationsModel;
    if (selectedDescCollab) {
      collaborator.mat = selectedDescCollab.collaboratorMatricul;
      collaborator.fullName = selectedDescCollab.collaboratorFullName;
      this.descCollabsOptions = [collaborator]

    } else {
      collaborator = null;
    }
    this.descCollabForm.patchValue({

      'collaborator': collaborator,
      'desc': this.selectedDesc.id, 'maxAmount': selectedDescCollab ? twoDigitDecimalRound(selectedDescCollab.maxAmount.toString()) : null, 'isManager': selectedDescCollab ? selectedDescCollab.isManager : null
    });
    this.descCollabForm.disable();
    this.descCollabForm.updateValueAndValidity();
    this.selectedDescCollab = selectedDescCollab;
    this.isDescCollabChecked = true;
    this.isDescChecked = false;
    this.descCollabBtns = 'editOrDeleteDescCollabBtns'

  }



  registrationNumberChange(query) {
    this.descCollabsOptions = [];
    this.svsDescCollabService.getDescCollabByQuery(this.selectedDesc.id, query).subscribe(data => this.descCollabsOptions = data);

  }

  managerNameAutocomplete(query) {
    if (query.length > 2) {
      this.managerNamesOptions = [];
      this.isLoading = true;
      this.svsDescService.getDescManagerByQuery(query).subscribe(data => this.managerNamesOptions = data, (err) => console.error(err), () => this.isLoading = false);
    }
  }


  // Prepare new add form for Desc or DescCollab
  addDesc() {
    this.isUpdate = false;
    this.isAddDesc = true;

    this.isDescChecked = true;
    this.isDescCollabChecked = false;
    this.descForm.reset()

    this.getAvailableDesc();
    this.descSubscription = this.descForm.get('desc').valueChanges.subscribe(change => {
      if (change) {
        this.descForm.patchValue({ descName: change.descName });
      }

    })
    this.descForm.enable();
    this.descCollabBtns = 'saveOrCancelDescCollabBtns';
  }
  addDescCollab() {
    if (this.selectedDesc) {
      this.isUpdate = false;
      this.descCollabBtns = 'saveOrCancelDescCollabBtns';
      this.descCollabForm.reset();
      this.descCollabForm.get('desc').setValue(this.selectedDesc.id);
      this.descCollabForm.get('isManager').setValue(false);

      this.descCollabForm.enable();
      this.descCollabForm.controls['desc'].disable();
      if (!this.isAdmin()) {
        this.descCollabForm.controls['isManager'].disable();

      }
      this.isDescChecked = false;
      this.isDescCollabChecked = true;
    }
  }
  // Prepare new Edit/Update form for Desc or DescCollab
  updateDesc() {
    this.isUpdate = true;
    if (this.isDescCollabChecked) {
      this.descCollabForm.disable();
      this.descCollabForm.controls['maxAmount'].enable();

      if (this.isAdmin()) {
        this.descCollabForm.controls['isManager'].enable();

      }

    }
    if (this.isDescChecked) {
      this.descForm.enable();
    }
    this.descCollabBtns = 'saveOrCancelDescCollabBtns';
  }
  // Save the new Desc or DescCollab
  save() {
    if (this.isDescChecked) {
      this.descForm.markAllAsTouched();

      if (this.descForm.valid) {

        let value = this.descForm.value;
        let body = {} as SvsDescModel;
        body.id = value.desc.id
        body.descName = value.descName;
        body.matriculeDescManager = value.descManager.mat;
        if (this.isAddDesc) {
          this.descSubscription.unsubscribe();
        }
        this.svsDescService.submitSvsDesc(body).subscribe(res => {
          this.svsDescService.getSvsDescList().subscribe(data => {
            this.descs = data;
            this.onSelectDesc(this.descs.find(desc => desc.id == res.id));
            this.isAddDesc = false;


          })
        })
      }
    }
    if (this.isDescCollabChecked) {
      this.descCollabForm.markAllAsTouched();

      if (this.descCollabForm.valid) {
        let value = this.descCollabForm.getRawValue();

        let body = {} as SvsDescCollabModel;
        if (this.isUpdate) {
          body = this.selectedDescCollab;
          body.maxAmount = value.maxAmount;
          body.isManager = value.isManager;
        } else {
          body.desc = {} as SvsDescModel;
          body.desc.id = this.selectedDesc.id;
          body.collaboratorMatricul = value.collaborator.mat;
          body.maxAmount = value.maxAmount;
          body.isManager = value.isManager;

        }
        this.svsDescCollabService.submitSvsDescCollab(body).subscribe(res => {
          this.descCollabList = [];
          this.isUpdate = false;

          this.svsDescCollabService.getSvsDescCollabList(this.selectedDesc.id).subscribe(data => {
            this.descCollabList = data;
            this.onSelectDescCollab(this.descCollabList.find(descCollab => descCollab.id == res.id))

          })

        }, err => {
          console.error(err)
        }, () => {
          this.showPopUpInsertionApproved();

        })
        this.descCollabForm.disable();
      }
    }

  }

  // Prepare new delte form + popup for Desc or DescCollab
  delete() {
    if (this.isDescChecked) {
      this.popupService.popupInfo(
        'Êtes-vous sûr(e) de vouloir supprimer ?  <br>'
        + 'En cliquant sur le bouton « Confirmer », '
        + 'vous supprimerez ' + this.selectedDesc.descName + ' et tous les collaborateurs afférents.'
        , 'Confirmer', null, 'Annuler', ConfirmationModalComponent);
      const subscription = this.popupService.onConfirm.subscribe((t) => {
        if (t) {
          this.descCollabList = [];
          this.svsDescService.removeSvsDesc(this.selectedDesc).subscribe((res) => {
            this.selectedDesc = null;
            this.isDescChecked = false;
            this.loadDescs();
          });

        }
        subscription.unsubscribe();
      });


    }
    // remove Desc // DescCollab
    else {
      this.popupService.popupInfo(
        'Êtes-vous sûr(e) de vouloir supprimer ?  <br>'
        + 'En cliquant sur le bouton « Confirmer », '
        + 'vous supprimerez les pouvoirs de ' + this.selectedDescCollab.collaboratorFullName + '.', 'Confirmer', null, 'Annuler', ConfirmationModalComponent);
      const subscription = this.popupService.onConfirm.subscribe((t) => {
        if (t) {
          // delete  descCollab
          this.svsDescCollabService.removeSvsDescCollab(this.selectedDescCollab).subscribe((res) => {
            this.selectedDescCollab = null;
            this.isDescCollabChecked = false;
            this.onSelectDesc(this.selectedDesc);
          }, err => {
            console.error(err);
          }, () => {
            this.showPopUpInsertionApproved();

          });
        }
        subscription.unsubscribe();
      });
    }
  }


  undo() {
    this.descCollabBtns = 'editOrDeleteDescCollabBtns';
    if (this.isDescChecked) {
      if (this.isAddDesc) {
        this.isDescChecked = false;
        this.isDescCollabChecked = false;
      } else {
        let mdmManger = {} as MdmInformationsModel;
        if (this.selectedDesc.matriculeDescManager) {
          mdmManger.mat = this.selectedDesc.matriculeDescManager;
          mdmManger.fullName = this.selectedDesc.fullNameDescManager;
          this.managerNamesOptions = [mdmManger]

        } else {
          mdmManger = null;
        }

        this.descForm.patchValue({ 'desc': this.selectedDesc, 'descName': this.selectedDesc.descName, 'descManager': mdmManger });
      }
      this.descForm.disable();

      this.isAddDesc = false;
    }
    if (this.isDescCollabChecked) {
      if (this.isUpdate) {
        this.onSelectDescCollab(this.selectedDescCollab)
      } else {
        this.onSelectDesc(this.selectedDesc);

      }

      this.descCollabForm.disable();
    }
    this.isUpdate = false;
  }


  loadDescs() {
    this.svsDescService.getSvsDescList().subscribe(res => {
      this.descs = res;
    }
    )
  }

  getDescCollabList(desc: SvsDescModel) {
    this.descCollabList = [];
    this.svsDescCollabService.getSvsDescCollabList(this.selectedDesc.id).subscribe(data => {
      this.descCollabList = data;
    })
  }


  isAdmin() {
    if (this.authService.currentUserRolesList.includes('ROLE_SVS_ADMIN_SE')) {
      return true;
    }

  }

  loadDescByManagerId(id: string) {
    this.svsDescService.getSvsNetworksListByManagerId(id).subscribe(res => {
      this.descs = res;
    }, err=>{
      //this is intentional
    }, ()=>{
      this.mdmNetworksLoaded = true;
    });
  }

  autorizeCollabModification() {
    if ((this.selectedDesc && this.isAdmin()) || (this.selectedDescCollab !== null && this.selectedDescCollab.collaboratorMatricul !== this.userMat)) {
      return true;
    } else {
      return false;
    }
  }

  showPopUpInsertionApproved() {
    this.popupService.popupInfo('Votre saisie a bien été prise en compte.', null, null, null, ConfirmationModalComponent);
    const subscription = this.popupService.onConfirm.subscribe((t) => {
      subscription.unsubscribe();
    });
  }


}
