import { FormBuilder } from '@angular/forms';
import { of, Subject } from 'rxjs';
import { MdmInformationsModel } from '../../shared/models/mdm-informations.model';
import { SvsDescCollabModel } from '../../shared/models/svs-desc-collab.model';
import { SvsDescModel } from '../../shared/models/svs-desc.model';
import { DescManagementComponent } from "./desc-management.component";

describe('DescManagementComponent', () => {

    let fixture: DescManagementComponent;
    let popupServiceMock: any;
    let formBuilderMock: FormBuilder;
    let svsDescServiceMock: any;
    let svsDescCollabServiceMock: any;
    let authServiceMock: any;
    let dataSharingMock: any;

    let routerMock : any;

    let descsMock = [
        {
            id: '106',
            descName: 'descName',
            fullNameDescManager: 'fullName',
            matriculeDescManager: 'M0000'
        } as SvsDescModel
        ,
        {
            id: '107',
            descName: 'descName',
            fullNameDescManager: 'fullName',
            matriculeDescManager: 'M0007'
        } as SvsDescModel

    ]

    let descCollabMocks: SvsDescCollabModel[] = [
        {
            id: '5555',
            desc: descsMock[0],
            isManager: false,
            maxAmount: 80000,
            collaboratorFullName: 'fullName',
            collaboratorMatricul: 'M0000'
        } as SvsDescCollabModel
    ]
    let collabListMock: MdmInformationsModel[] = [
        {
            fullName: 'fullName',
            mat: 'M0000'
        } as MdmInformationsModel
    ]

     routerMock = {
        snapshot: {
          params: {
          },
        }
      }


    beforeEach(() => {

        popupServiceMock = {
            popupInfo: jest.fn().mockReturnValue(of(true)),
            onConfirm: of(true)
        };
        svsDescServiceMock = {
            getSvsDescList: jest.fn().mockReturnValue(of(descsMock)),
            submitSvsDesc: jest.fn().mockReturnValue(of(descsMock[0])),
            removeSvsDesc: jest.fn().mockReturnValue(of(true)),
            getAvailableSvsDesc: jest.fn().mockReturnValue(of(descsMock)),
            getDescManagerByQuery: jest.fn().mockReturnValue(of(collabListMock)),
            getSvsNetworksListByManagerId: jest.fn().mockReturnValue(of([descsMock[1]]))
        }

        svsDescCollabServiceMock = {
            getSvsDescCollabList: jest.fn().mockReturnValue(of(descCollabMocks)),
            submitSvsDescCollab: jest.fn().mockReturnValue(of(descCollabMocks[0])),
            removeSvsDescCollab: jest.fn().mockReturnValue(of(true)),
            getDescCollabByQuery: jest.fn().mockReturnValue(of(collabListMock)),
            isDescManager: jest.fn().mockReturnValue(of(true)),
            getSvsDescHistory: jest.fn().mockReturnValue(of({
                descId : '106',
                operationCode :'UPDATE',
                collaboratorMatricul :'M0000'
            }))
        }

        authServiceMock = {
            'identityClaims' : {
                'mat': ''
            },
            'currentUserRolesList': ['ROLE_SVS_ADMIN_SE']
        }
        routerMock = {
            snapshot: {
              params: {
              },
            }
          }
          dataSharingMock = {
            changeStatus: jest.fn().mockImplementation(() => { }),
          }
      
        formBuilderMock = new FormBuilder();


        fixture = new DescManagementComponent(
            formBuilderMock,
            svsDescServiceMock,
            svsDescCollabServiceMock,
            popupServiceMock,
            authServiceMock,
            routerMock,
            dataSharingMock

        );

        fixture.ngOnInit();


    });

    describe('Test: ngOnInit', () => {

        it('should initialise descs', () => {
            expect(fixture.descs).toEqual(descsMock);
        });

        it('should initialise available descs', () => {
            expect(fixture.mdmNetworks).toEqual(descsMock);
        });

        it('should initialise available descs with manager profile', () => {

            authServiceMock = {
                'identityClaims' : {
                    'mat': 'Mxx'
                },
                'currentUserRolesList': []
            }
            fixture = new DescManagementComponent(
                formBuilderMock,
                svsDescServiceMock,
                svsDescCollabServiceMock,
                popupServiceMock,
                authServiceMock,
                routerMock,
                dataSharingMock
            );

            fixture.ngOnInit();
            expect(svsDescServiceMock.getSvsNetworksListByManagerId).toHaveBeenCalled();
            svsDescServiceMock.getSvsNetworksListByManagerId().subscribe(data=>{
                expect(fixture.descs).toEqual([descsMock[1]]);

            })
        });

        it('should  initialise  descs and collab by history', () => {

            routerMock = {
                snapshot: {
                  params: {
                    idHistory:'106'
                  },
                }
              }
            fixture = new DescManagementComponent(
                formBuilderMock,
                svsDescServiceMock,
                svsDescCollabServiceMock,
                popupServiceMock,
                authServiceMock,
                routerMock,
                dataSharingMock
            );

            fixture.ngOnInit();
            svsDescServiceMock.getSvsDescList().subscribe(data=>{

                expect(svsDescCollabServiceMock.getSvsDescHistory).toHaveBeenCalled();


            })
        });

        it('should  initialise  descs and not collab by history', () => {

            routerMock = {
                snapshot: {
                  params: {
                    idHistory:'106'
                  },
                }
              }

              svsDescCollabServiceMock.getSvsDescHistory = jest.fn().mockReturnValue(of({
                descId : '106',
                operationCode :'DELETE',
                collaboratorMatricul :'M0000'
            }))
            fixture = new DescManagementComponent(
                formBuilderMock,
                svsDescServiceMock,
                svsDescCollabServiceMock,
                popupServiceMock,
                authServiceMock,
                routerMock,
                dataSharingMock
            );

            fixture.ngOnInit();
            svsDescServiceMock.getSvsDescList().subscribe(data=>{

                expect(svsDescCollabServiceMock.getSvsDescHistory).toHaveBeenCalled();


            })
        });

        it('should NOT initialise  descs and not collab by history', () => {

            routerMock = {
                snapshot: {
                  params: {
                    idHistory:'106'
                  },
                }
              }

              svsDescCollabServiceMock.getSvsDescHistory = jest.fn().mockReturnValue(of({
                descId : '5454',
                operationCode :'DELETE',
                collaboratorMatricul :'M0000'
            }))
            fixture = new DescManagementComponent(
                formBuilderMock,
                svsDescServiceMock,
                svsDescCollabServiceMock,
                popupServiceMock,
                authServiceMock,
                routerMock,
                dataSharingMock
            );

            fixture.ngOnInit();
            svsDescServiceMock.getSvsDescList().subscribe(data=>{

                expect(svsDescCollabServiceMock.getSvsDescHistory).toHaveBeenCalled();


            })
        });
    })



    describe('Test: onSelectDesc', () => {

        it('should select desc form', () => {
            fixture.onSelectDesc(descsMock[0]);
            expect(fixture.descForm.value).toEqual(
                {
                    desc:descsMock[0],
                    descName:descsMock[0].descName,
                    descManager:{mat:descsMock[0].matriculeDescManager,fullName:descsMock[0].fullNameDescManager}

            });
            expect(fixture.selectedDesc).toEqual(descsMock[0]);
            expect(fixture.descCollabBtns).toEqual('editOrDeleteDescCollabBtns');
            expect(fixture.descCollabList ).toEqual(descCollabMocks);
            expect(fixture.descForm.disabled ).toEqual(true);
            expect(fixture.isDescChecked).toEqual(true);




        });


    })

    describe('Test: onSelectDescCollab', () => {

        it('should select desc collab form', () => {
            fixture.onSelectDesc(descsMock[0]);
            fixture.onSelectDescCollab(descCollabMocks[0]);

            expect(fixture.descCollabForm.value).toEqual(
                {
                    desc:descsMock[0].id,
                    maxAmount:"80000",
                    isManager: false,
                    collaborator:{mat:descCollabMocks[0].collaboratorMatricul,fullName:descCollabMocks[0].collaboratorFullName}

            });
            expect(fixture.selectedDescCollab).toEqual(descCollabMocks[0]);
            expect(fixture.descCollabBtns).toEqual('editOrDeleteDescCollabBtns');
            expect(fixture.descCollabForm.disabled ).toEqual(true);
            expect(fixture.isDescCollabChecked).toEqual(true);

        });

        it('should not select desc collab form', () => {
            fixture.onSelectDesc(descsMock[0]);
            fixture.onSelectDescCollab(null);

            expect(fixture.descCollabForm.value).toEqual(
                {
                    desc:descsMock[0].id,
                    maxAmount:null,
                    isManager: null,
                    collaborator:null

            });
        });

    })

    describe('Test: autocomplete methods', () => {

        it('should update registrations options', () => {
            fixture.selectedDesc = descsMock[0]
            fixture.registrationNumberChange('')
            ;
           setTimeout(()=>expect(fixture.descCollabsOptions).toEqual(collabListMock),20) ;

        });
        it('should update manager options', () => {
            fixture.managerNameAutocomplete('');
            setTimeout(()=>expect(fixture.managerNamesOptions).toEqual(collabListMock),20) ;

        });
        it('should update manager options', () => {
            fixture.managerNameAutocomplete('test');
            setTimeout(()=>expect(fixture.managerNamesOptions).toEqual(collabListMock),20) ;

        });


    })

    describe('Test: add desc', () => {

        it('should initialize desc form', () => {
            fixture.addDesc();
            expect(fixture.isAddDesc).toEqual(true);


        });



    })

    describe('Test: add desc', () => {

        it('should initialize desc form as admin', () => {
            fixture.selectedDesc = (descsMock[0]);
            fixture.addDescCollab();
            expect(fixture.isAddDesc).toEqual(false);
        });

        it('should initialize desc form as manager', () => {
            spyOn(fixture ,"isAdmin").and.returnValue(false);
            fixture.selectedDesc = (descsMock[0]);
            fixture.addDescCollab();
            expect(fixture.isAddDesc).toEqual(false);
        });
    })

    describe('Test: onBlurMaxAmount', () => {

        it('should round max amount', () => {
            fixture.onSelectDesc(descsMock[0]);
            fixture.onSelectDescCollab(descCollabMocks[0]);


            fixture.descCollabForm.patchValue({maxAmount : 6.0968})
            fixture.onBlurMaxAmount();
            expect(fixture.descCollabForm.value.maxAmount).toEqual("6.10");
        });
    })


    describe('Test: loadDescs', () => {

        it('should loadDescs', () => {
            fixture.loadDescs();
            expect(svsDescServiceMock.getSvsDescList).toHaveBeenCalled();
        });
    })
    describe('Test: save desc or descCollab', () => {

        it('should Save desc ', () => {
            fixture.isDescChecked = true;
            fixture.onSelectDesc(descsMock[0])
            fixture.descForm.patchValue(descsMock[0]);
            fixture.descForm.enable();
            expect(fixture.descForm.valid).toEqual(true)

            fixture.save();
            expect(svsDescServiceMock.submitSvsDesc).toHaveBeenCalled();
        });
        it('should Save desc with AddMode', () => {
            fixture.isDescChecked = true;
            fixture.onSelectDesc(descsMock[0])
            fixture.descForm.patchValue(descsMock[0]);
            fixture.descForm.enable();
            fixture.isAddDesc = true;
            fixture.descSubscription = new Subject().subscribe();


            fixture.save();
            expect(svsDescServiceMock.submitSvsDesc).toHaveBeenCalled();
        });
        it('should Save descCollab ', () => {
            fixture.isDescChecked = false;
            fixture.isDescCollabChecked = true;
            fixture.isUpdate = true;
            fixture.selectedDesc = descsMock[0];
            fixture.selectedDescCollab = descCollabMocks[0];
            fixture.descCollabForm.patchValue(descCollabMocks[0]);
            fixture.save();
            expect(svsDescServiceMock.getSvsDescList).toHaveBeenCalled();
        });

        it('should Save descCollab with add mode', () => {
            fixture.isDescChecked = false;
            fixture.isDescCollabChecked = true;
            fixture.selectedDesc = descsMock[0]
            fixture.descCollabForm.setErrors(null);
            fixture.descCollabForm.setValidators(null);
            fixture.descCollabForm.updateValueAndValidity();
            fixture.descCollabForm.enable()
            fixture.descCollabForm.patchValue(descCollabMocks[0]);
            fixture.descCollabForm.patchValue({maxAmount:1000,collaborator : {mat :'Mxx'},isManager:false});


            fixture.isUpdate = false
            expect(fixture.descCollabForm.valid).toEqual(true)



            fixture.save();
            expect(svsDescCollabServiceMock.submitSvsDescCollab).toHaveBeenCalled();
        });
        it('should Save desc with valid form', () => {
            fixture.descForm.setErrors(null);
            fixture.descForm.setValidators(null);
            fixture.descForm.updateValueAndValidity();
            fixture.isDescChecked = true;
            fixture.descForm.patchValue(descsMock[0]);
            fixture.save();
            expect(svsDescServiceMock.getSvsDescList).toHaveBeenCalled();
        });
        it('should Save descCollab  with valid form', () => {
            fixture.isDescChecked = false;
            fixture.descCollabForm.patchValue(descCollabMocks[0]);
            fixture.save();
            expect(svsDescServiceMock.getSvsDescList).toHaveBeenCalled();
        });

    })

    describe('Test: delete desc or descCollab', () => {

        it('should delete desc ', () => {
            fixture.selectedDesc = descsMock[0];
            fixture.isDescChecked = true;
            fixture.delete();
            expect(svsDescServiceMock.getSvsDescList).toHaveBeenCalled();
        });

        it('should delete descCollab ', () => {
            fixture.selectedDescCollab =descCollabMocks[0];
            fixture.isDescChecked = false;
            fixture.delete();
            expect(svsDescCollabServiceMock.removeSvsDescCollab).toHaveBeenCalled();
        });

    })

   describe('Test: undo', () => {

    it('should undo descchecked', () => {
        fixture.isDescChecked = true;
        fixture.selectedDesc = descsMock[0];
        fixture.undo();
        expect(fixture.descCollabBtns).toEqual("editOrDeleteDescCollabBtns");
    });

    it('should undo descchecked', () => {
        fixture.isDescChecked = true;
        fixture.isAddDesc = true;
        fixture.selectedDesc = descsMock[0];
        fixture.undo();
        expect(fixture.descCollabBtns).toEqual("editOrDeleteDescCollabBtns");
    });
    it('should undo descCollabchecked', () => {
        fixture.isDescCollabChecked = true;
        fixture.selectedDesc = descsMock[0];
        fixture.selectedDescCollab = descCollabMocks[0];


        fixture.undo();
        expect(fixture.descCollabBtns).toEqual("editOrDeleteDescCollabBtns");
    });
    })
    describe('Test: updateDesc', () => {

        it('should updateDesc descchecked', () => {
            fixture.isDescChecked = true;
            fixture.updateDesc();
            expect(fixture.descCollabBtns).toEqual("saveOrCancelDescCollabBtns");
        });
        it('should updateDesc descCollabchecked', () => {
            fixture.isDescCollabChecked = true;
            fixture.updateDesc();
            expect(fixture.descCollabBtns).toEqual("saveOrCancelDescCollabBtns");
        });
        });

    describe('Test: autorizeCollabModification', () => {

        it('should return true when selectedDescCollab == connected user and isAdmin true', () => {
            fixture.selectedDesc = { id : '1'} as SvsDescModel;
            fixture.userMat = "M40847";
            let selectedCollab :SvsDescCollabModel={
                collaboratorMatricul : 'M40847',
                collaboratorFullName :'',
                collaboratorInfo:null,
                id:'',
                desc: null,
                isManager: true,
                maxAmount : 2000
            }
            fixture.selectedDescCollab = selectedCollab;
            expect(fixture.autorizeCollabModification()).toEqual(true);
        });


        it('should return true when selectedDescCollab != connected user', () => {
            fixture.userMat = "M0000";
            let selectedCollab :SvsDescCollabModel={
                collaboratorMatricul : 'M40847',
                collaboratorFullName :'',
                collaboratorInfo:null,
                id:'',
                desc: null,
                isManager: true,
                maxAmount : 2000
            }
            fixture.selectedDescCollab = selectedCollab;
            expect(fixture.autorizeCollabModification()).toEqual(true);
        });
    });
    describe('test : disbable ajout button when  selectedDesc is false ',()=>{

        it('should set isDescSelected to false , disable  ajouter button ',()=>{
          expect(fixture.isDescSelected).toEqual(false);

        })
        it('should set isDescSelected to true , enable ajouter button ',()=>{

              let selectedDesc = {} as SvsDescModel;
               fixture.onSelectDesc(selectedDesc);
               expect(fixture.isDescSelected).toEqual(true);

             })
    })


})
