import { Component, Input, OnInit } from '@angular/core';
import { FinancialPlanModel } from '../../../../shared/models/financial-plan.model';
declare var $: any;

@Component({
  selector: 'app-result-plan-list',
  templateUrl: './result-plan-list.component.html',
  styleUrls: ['./result-plan-list.component.css']
})
export class ResultPlanListComponent implements OnInit {


  constructor() {
    //constructor

  }
  stylePlan: any;
  public selectedPlan = 0;
  @Input() financialPlans: FinancialPlanModel[];
  @Input() unitaryQuantity;



  ngOnInit(): void {

    if(this.financialPlans){

      this.stylePlan = {
        'grid-template-columns': 'repeat(' + this.financialPlans.length + ', 1fr)',
  
      }
    }


  }


  changeTab(i: number) {
    this.selectedPlan = i;
  }
}
