import { Component, OnInit, Input } from '@angular/core';
import { PlanEstimatedOutModel } from '../../../../../shared/models/plan-estimated-out.model';
import { twoDigitDecimalRound } from '../../../../../shared/ValuationUtils';

@Component({
  selector: 'app-result-plan-list-profile',
  templateUrl: './result-plan-list-profile.component.html',
  styleUrls: ['./result-plan-list-profile.component.css']
})
export class ResultPlanListProfileComponent implements OnInit {

  @Input() planEstimatedOut: PlanEstimatedOutModel[];
  constructor() {
   // This is intentional
   }

  ngOnInit(): void {
     // This is intentional

  }


  formatAmount(amount:any){
    if(amount){
      return twoDigitDecimalRound(amount.toString());
    }
    return amount;
  }
}
