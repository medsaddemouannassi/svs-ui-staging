import { ResultPlanListProfileComponent } from "./result-plan-list-profile.component";


describe('ResultPlanListProfileComponent', () => {
    let fixture: ResultPlanListProfileComponent;

    beforeEach(() => {

        fixture = new ResultPlanListProfileComponent(

          );
        fixture.ngOnInit();
    });

    describe('Test Component', () => {
      it('should Be Truthy', () => {
        expect(fixture).toBeTruthy();

      });
    });


    describe('Test formatAmount()', () => {
      it('should format amount', () => {
        expect(fixture.formatAmount(1000.999)).toEqual("1001");

      });

      it('should return null when amount is null', () => {
        expect(fixture.formatAmount(null)).toEqual(null);

      });
    });







});

