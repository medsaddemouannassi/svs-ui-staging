import { ResultPlanListComponent } from "./result-plan-list.component";
import { FinancialPlanModel } from '../../../../shared/models/financial-plan.model';


describe('ResultPlanListComponent', () => {
  let fixture: ResultPlanListComponent;


  beforeEach(() => {


    fixture = new ResultPlanListComponent(


    );
    fixture.financialPlans = [{} as FinancialPlanModel,{} as FinancialPlanModel];

    fixture.ngOnInit();
  });

  describe('Test Component', () => {
    it('should Be Truthy', () => {
      expect(fixture).toBeTruthy();

    });
  });

  describe('Test ngOnInit', () => {
    it('should fill style', () => {
      expect(fixture.stylePlan).toEqual({ 'grid-template-columns': 'repeat(2, 1fr)' });

    });
  });

  describe('Test changeTab', () => {
    it('should change value', () => {
      fixture.changeTab(3);
      expect(fixture.selectedPlan).toEqual(3);
    });
  });






});

