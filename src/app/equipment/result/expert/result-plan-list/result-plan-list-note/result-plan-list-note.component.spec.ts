import { ResultPlanListNoteComponent } from "./result-plan-list-note.component";
import { FinancialPlanModel } from '../../../../../shared/models/financial-plan.model';

describe('ResultPlanListNoteComponent', () => {
  let fixture: ResultPlanListNoteComponent;
  let planMock: FinancialPlanModel;


  beforeEach(() => {
    planMock = {} as FinancialPlanModel;
    fixture = new ResultPlanListNoteComponent(


    );

    fixture.financialPlan = planMock;


    fixture.ngOnInit();
  });

  describe('Test Component', () => {
    it('should Be Truthy', () => {
      expect(fixture).toBeTruthy();

    });
  });

  describe('Test formatAmount()', () => {
    it('should format amount', () => {
      expect(fixture.formatAmount(1000.999)).toEqual("1001");

    });

    it('should return null when amount is null', () => {
      expect(fixture.formatAmount(null)).toEqual(null);

    });
  });

});

