import { Component, Input, OnInit } from '@angular/core';
import { FinancialPlanModel } from '../../../../../shared/models/financial-plan.model';
import { twoDigitDecimalRound } from '../../../../../shared/ValuationUtils';

@Component({
  selector: 'app-result-plan-list-note',
  templateUrl: './result-plan-list-note.component.html',
  styleUrls: ['./result-plan-list-note.component.css']
})
export class ResultPlanListNoteComponent implements OnInit {
  @Input() financialPlan: FinancialPlanModel;
  @Input() unitaryQuantity:number;

  planContrbAmount: string;
  //remainingAmount ???


  constructor() {
        //constructor

  }

  ngOnInit(): void {

    this.financialPlan.collateralAssetRating = +this.financialPlan.collateralAssetRating;


  }

  formatAmount(amount :any){
    if(amount){
    return twoDigitDecimalRound(amount.toString());
    }else{
      return amount;
    }
  }
}
