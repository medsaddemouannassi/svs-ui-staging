import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { forkJoin, of, Subscription } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ConfirmationModalComponent } from 'src/app/shared/confirmation-modal/confirmation-modal.component';
import { DocumentConstant } from 'src/app/shared/document-constant';
import { MdmInformationService } from 'src/app/shared/services/mdm-informations.service';
import { PopupService } from 'src/app/shared/services/popup.service';
import { ProcessingEquipementValuationService } from 'src/app/shared/services/processing-equipement-valuation.service';
import { SvsDescCollabService } from 'src/app/shared/services/svs-desc-collab.service';
import { getThemesByRoleAndStatusAndAction } from '../../../shared/communication-themes';
import { CatNatureModel } from '../../../shared/models/cat-nature.model';
import { EntrepriseInfoModel } from '../../../shared/models/entreprise-info.model';
import { FinancialPlanModel } from '../../../shared/models/financial-plan.model';
import { MdmInformationsModel } from '../../../shared/models/mdm-informations.model';
import { ParamSettingModel } from '../../../shared/models/param-setting.model';
import { ReportElementModel } from '../../../shared/models/reportElementModel.model';
import { Role } from '../../../shared/models/roles.enum';
import { SvsDescCollabModel } from '../../../shared/models/svs-desc-collab.model';
import { ValuationProcessingElementsModel } from '../../../shared/models/valuation-processing-elements.model';
import { ValuationRequestWorkflowModel } from '../../../shared/models/valuation-request-workflow.model';
import { ValuationModel } from '../../../shared/models/valuation.model';
import { ValuationDiscountModel } from '../../../shared/models/valuationDiscount.model';
import { WorkflowInstanceVariable } from '../../../shared/models/workflow-instance-variable.model';
import { PagesConstants } from '../../../shared/pages-constant';
import { AuthService } from '../../../shared/services/auth.service';
import { CatDiscountRateService } from '../../../shared/services/cat-discount-rate.service';
import { CatNatureService } from '../../../shared/services/cat-nature.service';
import { DecoteService } from '../../../shared/services/decote.service';
import { EntrepriseInfoService } from '../../../shared/services/entreprise-info.service';
import { FinancialPlanService } from '../../../shared/services/financial-plan.service';
import { ParamSettingService } from '../../../shared/services/param-setting.service';
import { ReportServiceService } from '../../../shared/services/report-service.service';
import { SharedCommunicationDataService } from '../../../shared/services/shared-communication-data.service';
import { ValuationRequestDocsService } from '../../../shared/services/valuation-request-docs.service';
import { ValuationRequestWorkflowService } from '../../../shared/services/valuation-request-workflow.service';
import { ValuationService } from '../../../shared/services/valuation.service';
import { StatusConstant } from '../../../shared/status-constant';



@Component({
  selector: 'app-result-expert',
  templateUrl: './result-expert.component.html',
  styleUrls: ['./result-expert.component.css']
})
export class ResultExpertComponent implements OnInit {

  financialPlans: FinancialPlanModel[];
  discount: ValuationDiscountModel[];
  firstYearRate: number;
  valuation: ValuationModel;
  dateSubmit: Date;
  idValuation: string;
  status: string;
  descName: string;
  descInfo: MdmInformationsModel;
  adminInfo: MdmInformationsModel;
  communicationSubscription: Subscription;

  isAdminValidationNeeded: boolean = false;

  statusList: ParamSettingModel[];
  isCurrentUserAdmin = false;
  valuationRequestWorkflow: ValuationRequestWorkflowModel;
  valuationRequestHistory: ValuationRequestWorkflowModel[]
  disabled = false;
  catNaturesExceptionalList: CatNatureModel[];
  currentUserSVSInfo: SvsDescCollabModel = {} as SvsDescCollabModel;
  isExpertNotAllowed = false;

  constructor(
    private processingEquipementValuationService: ProcessingEquipementValuationService,
    private popupService: PopupService,
    public router: Router,
    private route: ActivatedRoute,
    private valuationService: ValuationService,
    private financialPlanService: FinancialPlanService,
    private mdmInformationService: MdmInformationService,

    private reportService: ReportServiceService,
    private entrepriseInfoService: EntrepriseInfoService,
    private decoteService: DecoteService,
    private valuationRequestDocsService: ValuationRequestDocsService,
    private dataSharing: SharedCommunicationDataService,
    private valuationRequestWorkflowService: ValuationRequestWorkflowService,
    private paramSettingService: ParamSettingService,
    private authService: AuthService,
    private catNatureService: CatNatureService,
    private catDiscountRateService: CatDiscountRateService,
    private svsDescCollabService: SvsDescCollabService
  ) { }

  ngOnInit(): void {
    // window.scrollTo(0, 0);
    this.isCurrentUserAdmin = this.isAdmin();
    this.isAdmin || this.isExpert() ? this.dataSharing.changeStatus("false") : this.dataSharing.changeStatus("true");

    this.idValuation = this.route.snapshot.paramMap.get('idValuation');

    this.paramSettingService.getParamByCode(StatusConstant.STATUS_VAL_WKFL).subscribe(res => {
      this.statusList = res;
    });

    this.communicationSubscription = this.dataSharing.triggerCommunication.subscribe(() => {
      this.deployCommunicatioPopup(null);

    })
    this.idValuation = this.route.snapshot.paramMap.get('idValuation');


    forkJoin([this.decoteService.getDiscountByValuationId(this.idValuation).pipe(catchError(error => of(null))), this.valuationService.getValuationById(this.idValuation).pipe(catchError(error => of(null))), this.catNatureService.getCatNatureValid().pipe(catchError(error => of(null)))]).subscribe(res => {
      this.discount = res[0];
      this.valuation = res[1];
      this.catNaturesExceptionalList = res[2];
    }, err => {
      console.error(err)
    }, () => {

      this.getSvsDescCollabInfo(this.authService.identityClaims['mat']);


      this.getFirstYearRateByCatParams();
      // to get valuation request last status
      this.loadValuationRequestWorkflow();


      // Get CA Name
      this.mdmInformationService.getMdmCaInformation(this.valuation.experId).subscribe((data) => {
        this.descInfo = data;
        this.descName = data.fullName;
      })
      if (this.isCurrentUserAdmin) {
        this.mdmInformationService.getMdmCaInformation(this.authService.identityClaims['mat']).subscribe((data) => {
          this.adminInfo = data;
        })
      }

      //when the admin is redirected to result page, there are missing data, because basically data should pass from reception page to result page
      this.loadMissingData();


    });

  }


  checkIfNotAllowedExpert() {
    this.isExpertNotAllowed = !this.isAdmin() && this.isExpert() && !this.isFinancingAmountExceptional() && this.currentUserSVSInfo.maxAmount < this.valuation.valuationRequest.financingAmount;
  }



  saveSimulation() {
    this.popupService.popupInfo('Êtes-vous sûr(e) de confirmer votre simulation?', 'Confirmer', null, 'Annuler', ConfirmationModalComponent);
    let subscription = this.popupService.onConfirm.subscribe((t) => {
      if (t) {
        this.financialPlanService.saveFinancialPlan(this.financialPlans).subscribe(data => {


          let reportElement = {} as ReportElementModel;
          reportElement.entrepriseInfo = this.entrepriseInfoService.entrepriseInfo;
          reportElement.valuation = this.valuation;
          reportElement.valuationDate = this.dateSubmit;
          reportElement.discounts = this.discount;
          reportElement.financialPlans = data;
          reportElement.descName = this.descName;
          reportElement.statutCotation = 'Cotation en Simulation'
          this.reportService.generateReport(reportElement).subscribe((res) => {

            this.popupService.popupPDF('Confirmer', 'Annuler', null, window.URL.createObjectURL(res), true);

            let subscriptionPDFConfirmation = this.popupService.onConfirm.subscribe((response) => {

              if (response) {
                this.valuationRequestDocsService.

                  uploadValuationSimulation(this.blobToFile(res), this.valuation.valuationRequest, this.entrepriseInfoService.entrepriseInfo, false).subscribe(() => {
                    //this is intentional
                  },
                    err => {
                      console.error('error at uploadValuationResult', err);
                    }, () => {

                      //when the expert vaidates then change valuation status to Simulation

                      let status = this.statusList.find(expectedStatus => expectedStatus.value == StatusConstant.VAL_SIMULATION);
                      this.saveValuationRequestWorkflow(status);
                      



                    });
              }
              subscriptionPDFConfirmation.unsubscribe();

            });
          })



        });

      }

      subscription.unsubscribe();




    });
  }
  saveResult() {

    if (this.isAdminValidationNeeded) {
      //this case is when Expert has no permission to validate quotation
      this.popupService.popupInfo("Êtes-vous sûr(e) de confirmer votre cotation et de l'envoyer à la filière pour validation?", 'Envoyer', null, 'Annuler', ConfirmationModalComponent);

      let subscription = this.popupService.onConfirm.subscribe((t) => {

        if (t) {
          this.financialPlanService.saveFinancialPlan(this.financialPlans).subscribe(data => {
            // this is intentional

          }, err => {
            console.error('error saving financial plan ', err);
          }, () => {

            //send mail notif to the ADMIN for validation and change valuation status to WAITING for ADMIN VALIDATION
            let status = this.statusList.find(expectedStatus => expectedStatus.value == StatusConstant.WAIT_VALID_FIL);
            this.saveValuationRequestWorkflow(status);

          });

        }
        subscription.unsubscribe();

      });
    } else {

      //ELSE means: when expert has parmission to validate OR the admin is validating

      this.popupService.popupInfo('Êtes-vous sûr(e) de confirmer votre cotation?', 'Confirmer', null, 'Annuler', ConfirmationModalComponent);
      let subscription = this.popupService.onConfirm.subscribe((t) => {
        if (t) {
          this.financialPlanService.saveFinancialPlan(this.financialPlans).subscribe(data => {


            let reportElement = {} as ReportElementModel;
            reportElement.entrepriseInfo = this.entrepriseInfoService.entrepriseInfo;
            reportElement.valuation = this.valuation;
            reportElement.valuationDate = this.dateSubmit;
            reportElement.discounts = this.discount;
            reportElement.financialPlans = data;
            reportElement.descName = this.descName;
            if (this.isCurrentUserAdmin) {
              reportElement.statutCotation = StatusConstant.FIL_VALIDATED
            }
            else {
              reportElement.statutCotation = StatusConstant.EXP_VALIDATED
            }

            this.reportService.generateReport(reportElement).subscribe((res) => {

              this.popupService.popupPDF('Confirmer', 'Annuler', null, window.URL.createObjectURL(res), true);

              let subscriptionPDFConfirmation = this.popupService.onConfirm.subscribe((response) => {

                if (response) {
                  this.valuationRequestDocsService.

                    uploadValuationResult(this.blobToFile(res), this.valuation.valuationRequest, this.entrepriseInfoService.entrepriseInfo, DocumentConstant.PHASE_MEP, true).subscribe(() => {
                      //this is intentional
                    },
                      err => {
                        console.error('error at uploadValuationResult', err);
                      }, () => {

                        //when the expert vaidates then change valuation status to VALIDATED
                        //when the admin validates then should send mail notif to the EXPERT and change valuation status to VALIDATED
                        let status = this.statusList.find(stat => this.isCurrentUserAdmin ? stat.value == StatusConstant.FIL_VALIDATED : stat.value == StatusConstant.EXP_VALIDATED);
                        this.saveValuationRequestWorkflow(status);

                      });
                }
                subscriptionPDFConfirmation.unsubscribe();

              });
            })



          });

        }

        subscription.unsubscribe();




      });
    }
  }
  returnToReception() {
    this.router.navigate(['/reception/expert', this.valuation.valuationRequest.id])
  }
  public blobToFile = (theBlob: Blob): File => {
    var b: any = theBlob;
    //A Blob() is almost a File() - it's just missing the two properties below which we will add
    b.lastModifiedDate = new Date();
    b.name = "Resultat_valorisation_" + this.valuation.valuationRequest.id + ".pdf";
    //Cast to a File() type
    return new File([<File>theBlob], b.name);
  }

  checkIfAdminValidationNeeded() {
    if (!this.isCurrentUserAdmin && (this.isFinancingAmountExceptional() || this.isValuationRequestNaturesExceptional()
      || this.isValuationRequestDurationExceptional() || this.isDiscountExceptional())) {
      this.isAdminValidationNeeded = true;
    }
  }

  isFinancingAmountExceptional() {
    if (2_000_000 < Number(this.valuation.valuationRequest.financingAmount)) {
      return true;
    } else {
      return false;
    }
  }

  // when one of plans duration is more than 7 years or less than 2 years, then valiadation should be by admin
  isValuationRequestDurationExceptional() {
    if (this.financialPlans) {
      for (const pl of this.financialPlans) {
        if (((pl.planDuration / 12) > 7) || ((pl.planDuration / 12) < 2)) {

          return true;
        }
      }
    }
    return false;
  }

  isValuationRequestNaturesExceptional() {


    return this.catNaturesExceptionalList.some(item => item.id === this.valuation.catNature.id);
  }

  isDiscountExceptional() {

    if (this.discount && this.firstYearRate) {

      //la décote de la première année de +ou-10% (non pas de 10 points) alors la valo passe en validation filiere
      if (Math.abs(this.firstYearRate - this.discount[0].discountRateValue) > (this.firstYearRate*10/100)) {

        return true;
      }

    }
    return false;
  }

  ngOnDestroy() {
    this.entrepriseInfoService.entrepriseInfo=null
    this.communicationSubscription.unsubscribe();
    this.popupService.dismissAll();
  }

  loadValuationRequestWorkflow() {
    this.valuationRequestWorkflowService.getValuationRequestHistory(this.valuation.valuationRequest.id).subscribe(data => {
      this.valuationRequestHistory = data;
      this.valuationRequestWorkflow = data[0];

      if (this.valuationRequestHistory.length > 0) {

        let theme = null;
        if (this.isAdmin() && (this.valuationRequestHistory.length > 1 && this.valuationRequestHistory[1].status.value == StatusConstant.RESP_INFO_EXP_FIL)) {
          
        theme = getThemesByRoleAndStatusAndAction(this.authService.currentUserRolesList as Role[], this.valuationRequestHistory[0].status.value, this.valuationRequestHistory[1].status.value, (this.valuation.valuationRequest.isSimulated)?PagesConstants.RESULT_SIMULATION:PagesConstants.RESULT, null)
          this.deployCommunicatioPopup(theme)

        }
        if (this.isExpert() && (this.valuationRequestHistory[0].status.value == StatusConstant.WAIT_INFO_FIL_EXP)) {
           theme = getThemesByRoleAndStatusAndAction(this.authService.currentUserRolesList as Role[], this.valuationRequestHistory[0].status.value, this.valuationRequestHistory[1].status.value, (this.valuation.valuationRequest.isSimulated)?PagesConstants.RESULT_SIMULATION:PagesConstants.RESULT, StatusConstant.RESP_INFO_EXP_FIL)
        }
        if (this.isExpert() && (this.valuationRequestHistory.length > 1 && this.valuationRequestHistory[1].status.value == StatusConstant.VAL_MAN_EXP_QUALI_REJ)) {
         theme = getThemesByRoleAndStatusAndAction(this.authService.currentUserRolesList as Role[], this.valuationRequestHistory[0].status.value, this.valuationRequestHistory[1].status.value, (this.valuation.valuationRequest.isSimulated)?PagesConstants.RESULT_SIMULATION:PagesConstants.RESULT,null)

       }

       if ((this.valuationRequestHistory[0].status.value == StatusConstant.VAL_FIL_REJ) && this.isExpert()) {

        theme = getThemesByRoleAndStatusAndAction(this.authService.currentUserRolesList as Role[], this.valuationRequestHistory[0].status.value, this.valuationRequestHistory[1].status.value, (this.valuation.valuationRequest.isSimulated)?PagesConstants.RESULT_SIMULATION:PagesConstants.RESULT, null)

      }

        if(theme){
          this.deployCommunicatioPopup(theme);

        }



      }
    })


    this.valuationRequestWorkflowService.getValuationRequestStatus(this.valuation.valuationRequest.id).subscribe(data => {
      this.valuationRequestWorkflow = data;
      this.status = (data != null && data.status != null) ? data.status.label : "";
      if (this.valuationRequestWorkflow) {
        this.dataSharing.changeValoStatus(String(data.status.label[0].toLowerCase() + data.status.label.substr(1)));

        if (this.valuationRequestWorkflow.status.value == StatusConstant.REJECTED ||
          (this.valuationRequestWorkflow.status.value == StatusConstant.WAIT_VALID_FIL && !this.isAdmin() && this.isExpert()) ||
          this.valuationRequestWorkflow.status.value == StatusConstant.EXP_VALIDATED ||
          this.valuationRequestWorkflow.status.value == StatusConstant.FIL_VALIDATED || !(this.isAdmin() || this.isExpert())
        ) {
          this.disabled = true
          this.dataSharing.changeStatus("true");
        }
      }
    });
  }

  deployCommunicatioPopup(theme) {
    this.popupService.popupCommunication(this.authService.currentUserRolesList, (this.valuation.valuationRequest.isSimulated)?PagesConstants.RESULT_SIMULATION:PagesConstants.RESULT, this.valuationRequestWorkflow.status.value, this.valuationRequestHistory[1].status.value, this.valuationRequestHistory, theme);
    let subscription = this.popupService.onConfirmCommunication.subscribe(data => {
      if (data) {

        let status = this.statusList.find(expectedStatus => expectedStatus.value == data.theme.Specs[0].action);
        this.saveValuationRequestWorkflow(status, data.comment);
      }
      subscription.unsubscribe();
    })
  }

  saveValuationRequestWorkflow(status: ParamSettingModel, comment?: string, selectedDesc? : MdmInformationsModel) {

    if (status) {
      let valuationRequestWorkflow = {} as ValuationRequestWorkflowModel;
      valuationRequestWorkflow.valuationRequest = this.valuation.valuationRequest;
      valuationRequestWorkflow.status = status;
      valuationRequestWorkflow.comment = comment;
      switch (status.value) {

        case StatusConstant.WAIT_INFO_EXP_CA: {
          valuationRequestWorkflow.variable = {} as WorkflowInstanceVariable;
          valuationRequestWorkflow.variable.expertDesc = this.descInfo;
          valuationRequestWorkflow.variable.companyName = this.entrepriseInfoService.entrepriseInfo.socialReason;
          valuationRequestWorkflow.variable.urlRequest = location.origin + '/#' + this.router.serializeUrl(this.router.createUrlTree(['request', this.valuation.valuationRequest.id]));
          valuationRequestWorkflow.variable.urlResponse = location.href;

          break;
        }
        case StatusConstant.WAIT_INFO_EXP_FIL: {
          valuationRequestWorkflow.variable = {} as WorkflowInstanceVariable;
          valuationRequestWorkflow.variable.expertDesc = this.descInfo;
          valuationRequestWorkflow.variable.urlRequest = location.href;
          valuationRequestWorkflow.variable.urlResponse = location.href;

          break;
        }


        case StatusConstant.WAIT_VALID_FIL: {
          valuationRequestWorkflow.variable = {} as WorkflowInstanceVariable;
          valuationRequestWorkflow.variable.expertDesc = this.descInfo;
          valuationRequestWorkflow.variable.urlRequest = location.href;
          valuationRequestWorkflow.variable.urlResponse = location.href;

          valuationRequestWorkflow.variable.urlRefused = location.href;

          break;
        }
        case StatusConstant.WAIT_INFO_FIL_EXP: {
          valuationRequestWorkflow.variable = {} as WorkflowInstanceVariable;
          valuationRequestWorkflow.variable.expertDesc = this.descInfo;
          valuationRequestWorkflow.variable.administrator = this.adminInfo;

          valuationRequestWorkflow.variable.urlRequest = location.href;
          valuationRequestWorkflow.variable.urlResponse = location.href;


          break;
        }

        case StatusConstant.REJECTED: {
          valuationRequestWorkflow.variable = {} as WorkflowInstanceVariable
          valuationRequestWorkflow.variable.urlRequest = location.origin + '/#' + this.router.serializeUrl(this.router.createUrlTree(['request', this.valuation.valuationRequest.id]));

          valuationRequestWorkflow.variable.urlRefused = location.origin + '/#' + this.router.serializeUrl(this.router.createUrlTree(['request', this.valuation.valuationRequest.id]));
          valuationRequestWorkflow.variable.urlResponse = location.href;


          break;
        }
        case StatusConstant.FIL_VALIDATED:
        case StatusConstant.EXP_VALIDATED: {
          valuationRequestWorkflow.variable = {} as WorkflowInstanceVariable
          valuationRequestWorkflow.variable.urlRequest = location.origin + '/#' + this.router.serializeUrl(this.router.createUrlTree(['request', this.valuation.valuationRequest.id]));

          valuationRequestWorkflow.variable.urlRefused = location.origin + '/#' + this.router.serializeUrl(this.router.createUrlTree(['request', this.valuation.valuationRequest.id]));
          valuationRequestWorkflow.variable.urlResponse = location.href;


          break;
        }

        case StatusConstant.WAIT_VALID_MAN_EXP_QUAL: {
          valuationRequestWorkflow.variable = {} as WorkflowInstanceVariable


          valuationRequestWorkflow.variable.expertDesc = this.descInfo;
          valuationRequestWorkflow.variable.administrator = selectedDesc;

          valuationRequestWorkflow.variable.urlRequest =location.href;
          valuationRequestWorkflow.variable.urlRefused = location.href;
          valuationRequestWorkflow.variable.urlResponse = location.href;


          break;
        }


      }
      this.valuationRequestWorkflowService.saveValuationRequestWorkflow(valuationRequestWorkflow).subscribe(resp => {
        this.valuationRequestWorkflow = resp;
        this.dataSharing.changeValoStatus(status.label[0].toLowerCase() + status.label.substr(1));
      }, err => {
        console.error('error in ', err)
      }, () => {



        let nextStatus = {} as ParamSettingModel;

        switch (this.valuationRequestWorkflow.status.value) {

          case StatusConstant.FIL_VALIDATED:
          case StatusConstant.VAL_SIMULATION:
          case StatusConstant.EXP_VALIDATED: {



            this.router.navigate(['/search'])
            break;
          }

          case StatusConstant.REJECTED: {
            this.popupService.popupInfo("La demande de valorisation n° " + this.valuation.id + " a été refusée.", 'Fermer', null, null, ConfirmationModalComponent);
            const subscription = this.popupService.onConfirm.subscribe((t) => {
              this.router.navigate(['/search'])
              subscription.unsubscribe();

            });
            break;

          }

          case StatusConstant.WAIT_INFO_EXP_FIL: {
            this.popupService.popupInfo("La demande de renseignements  n°" + this.valuation.id + " a été envoyée à la filière.", 'Fermer', null, null, ConfirmationModalComponent);
            break;
          }

          case StatusConstant.WAIT_VALID_FIL: {
            let msg = 'Votre demande n°' + this.valuation.id + ' a été adressée à la filière.';
            this.popupService.popupInfo(msg, 'Fermer', null, null, ConfirmationModalComponent);
            const subscription = this.popupService.onConfirm.subscribe((t) => {
              if (t) {
                this.router.navigate(['/search'])

              }
              subscription.unsubscribe();

            });

            break;
          }

          case StatusConstant.WAIT_INFO_FIL_EXP: {
            let msg = 'La demande de renseignements n°' + this.valuation.id + ' a été envoyée au DESC.';
            this.popupService.popupInfo(msg, 'Fermer', null, null, ConfirmationModalComponent);
            const subscription = this.popupService.onConfirm.subscribe((t) => {
              if (t) {
                this.router.navigate(['/search'])

              }
              subscription.unsubscribe();

            });
            break;
          }
          case StatusConstant.VAL_MAN_EXP_QUALI_REJ:
          case StatusConstant.RESP_INFO_FIL_EXP:
          case StatusConstant.RESP_INFO_CA_EXP: {
            // when it's Information response it should set status to waiting
            nextStatus = this.statusList.find(expectedStatus => expectedStatus.value == StatusConstant.WAIT_EXP_VAL);
            this.saveValuationRequestWorkflow(nextStatus);
            break;
          }
          case StatusConstant.RESP_INFO_EXP_FIL: {
            nextStatus = this.statusList.find(expectedStatus => expectedStatus.value == StatusConstant.WAIT_VALID_FIL);
            this.saveValuationRequestWorkflow(nextStatus);
            break;
          }


          case StatusConstant.WAIT_VALID_MAN_EXP_QUAL: {
            let msg = 'Votre demande n°' + this.valuation.id + " a été adressée à l'expert "+ selectedDesc.fullName+".";
            this.popupService.popupInfo(msg, 'Fermer', null, null, ConfirmationModalComponent);
            const subscription = this.popupService.onConfirm.subscribe((t) => {
              if (t) {
                this.router.navigate(['/search'])

              }
              subscription.unsubscribe();

            });
          }



          default: {

            // this is intentional
          }


        }

      });
    } else {
      console.error('error : cannot find required status');
    }

  }

  isAdmin() {
    if (this.authService.currentUserRolesList.includes(Role.ADMIN_SE)) {
      return true;
    }
    return false;
  }
  isExpert() {
    if (this.authService.currentUserRolesList.includes(Role.SVS_EXPERT_SE)) {
      return true;
    }
    return false;

  }

  loadFinantialPlans() {
    this.processingEquipementValuationService.financialPlans.subscribe(res => {
      this.financialPlans = res;

      if (!this.financialPlans) {
        // should load financial plans to call methode that calculate
        this.financialPlanService.loadFinancialPlanByValuationId(this.idValuation).subscribe(data => {
          this.financialPlans = data;
          let processElement: ValuationProcessingElementsModel = {} as ValuationProcessingElementsModel;
          processElement.plans = this.financialPlans;
          processElement.discounts = this.discount;
          processElement.valuationRequest = this.financialPlans[0].valuation.valuationRequest;
          this.processingEquipementValuationService.getValuationResult(processElement).subscribe(result => {
            this.financialPlans = result;
          })
        }, err => {
          console.error('error in loading finantial plans result page ', err);

        });
      }


    });

  }

  loadMissingData() {
    this.processingEquipementValuationService.dateSubmit.subscribe(dateSubmit => this.dateSubmit = dateSubmit);
    if (!this.dateSubmit) {
      this.dateSubmit = new Date();
    }

    this.loadFinantialPlans();
    this.loadEnterpriseInfo();

  }

  loadEnterpriseInfo() {
    if (!this.entrepriseInfoService.entrepriseInfo) {
      let ret = null;
      let bcp = null;
      if (this.valuation.valuationRequest.isRetUnderwriter) {
        ret = this.valuation.valuationRequest.underwriterId;
      } else {
        bcp = this.valuation.valuationRequest.underwriterId;
      }

      let loadedInfo = {} as EntrepriseInfoModel;
      this.entrepriseInfoService.entrepriseInfo = {} as EntrepriseInfoModel;
      this.entrepriseInfoService.loadEntrepriseInfo(ret, bcp).subscribe(
        (data: EntrepriseInfoModel) => {
          loadedInfo = data;
        },
        err => {
          console.error('error in loading enterpriseInfo result page ', err)
        },
        () => {
          this.entrepriseInfoService.entrepriseInfo.cp = loadedInfo.cp;
          this.entrepriseInfoService.entrepriseInfo.socialReason = loadedInfo.socialReason;
        }
      );
    }
  }

  getFirstYearRateByCatParams() {
    //idValuation is setted to null to get discount from catalog
    this.catDiscountRateService.getFirstYearRateByCatParams(this.valuation.catActivitySector.id, this.valuation.catNature.id, this.valuation.catBrand.id,
      this.valuation.catAssetType.id, this.valuation.valuationRequest.isANewAsset, this.valuation.valuationRequest.paramEnergy.id).subscribe((data) => {
        this.firstYearRate = data;
      }, err => { }, () => {
        this.checkIfAdminValidationNeeded();
      });
  }

  askForValidation() {
    this.popupService.popupAskForValidation(this.descInfo.idReseau, this.valuation,this.descInfo);
    const subscription = this.popupService.onConfirmDescValidator.subscribe((selectedDesc) => {


      if (selectedDesc) {

        this.financialPlanService.saveFinancialPlan(this.financialPlans).subscribe(data => {

          if (this.valuation.valuationRequest.isSimulated) {
            //when validating a simulation
          } else {

            //send mail notif to the chosen expert for validation and change valuation status to WAITING for MAN EXP VALIDATION
            let status = this.statusList.find(expectedStatus => expectedStatus.value == StatusConstant.WAIT_VALID_MAN_EXP_QUAL);
            this.saveValuationRequestWorkflow(status,null,selectedDesc.validatorName.collaboratorInfo);




          }
        }, err => {
          console.error('error saving financial plan ', err);
        });




        // clicked on confirm btn
      }
      subscription.unsubscribe();

    })
  }

  shouldSearhForOtherExpertsForValidation() {
    // if PA<=2 millions and expert amount max< PA => EXPERT
    //if PA>2millions => FILIERE
    if (!this.isFinancingAmountExceptional() && this.valuation.valuationRequest.financingAmount > this.currentUserSVSInfo.maxAmount) {
      return true;
    } else {
      return false;
    }
  }

  getSvsDescCollabInfo(mat: string) {
    this.svsDescCollabService.getSvsDescCollab(mat).subscribe(res => {
      this.currentUserSVSInfo = res;
      this.checkIfNotAllowedExpert();
    });
  }

  showModifButton(){
    if(this.valuationRequestWorkflow){
      if(['EXP_VALIDATED','FIL_VALIDATED','REJECTED'].includes(this.valuationRequestWorkflow.status.value)){
        return false;
      }
      if(this.isExpertNotAllowed  && this.valuationRequestWorkflow.status.value == StatusConstant.WAIT_VALID_MAN_EXP_QUAL){
        return false;
      }
      if(!this.isAdmin() && this.isExpert() && ['WAIT_VALID_FIL','WAIT_INFO_FIL_EXP'].includes(this.valuationRequestWorkflow.status.value)){
        return false;
      }
      return true;
    }
    return true;



  }

}
