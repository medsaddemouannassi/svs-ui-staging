
import { UrlTree } from '@angular/router';
import {of, Subject, throwError} from 'rxjs';
import { SvsDescCollabModel } from 'src/app/shared/models/svs-desc-collab.model';
import { SvsDescModel } from 'src/app/shared/models/svs-desc.model';
import { CatActivitySectorModel } from '../../../shared/models/cat-activity-sector.model';
import { CatAssetTypeModel } from '../../../shared/models/cat-asset-type.model';
import { CatBrandModel } from '../../../shared/models/cat-brand.model';
import { CatNatureModel } from '../../../shared/models/cat-nature.model';
import { EntrepriseInfoModel } from '../../../shared/models/entreprise-info.model';
import { FinancialPlanModel } from '../../../shared/models/financial-plan.model';
import { MdmInformationsModel } from '../../../shared/models/mdm-informations.model';
import { ParamEnergyModel } from '../../../shared/models/param-energy.model';
import { ParamSettingModel } from '../../../shared/models/param-setting.model';
import { Role } from '../../../shared/models/roles.enum';
import { ValuationRequestWorkflowModel } from '../../../shared/models/valuation-request-workflow.model';
import { ValuationRequestModel } from '../../../shared/models/valuation-request.model';
import { ValuationModel } from '../../../shared/models/valuation.model';
import { ValuationDiscountModel } from '../../../shared/models/valuationDiscount.model';
import { StatusConstant } from '../../../shared/status-constant';
import { ResultExpertComponent } from './result-expert.component';
import {error} from 'protractor';


describe('ResultComponent', () => {
  let fixture: ResultExpertComponent;
  let processingEquipementValuationServiceMock: any;
  let popupServiceMock: any;
  let routerMock: any;
  let routeMock: any;
  let valuationServiceMock;
  let financialPlanServiceMock;
  let mdmInformationServiceMock;
  let reportServiceMock;
  let entrepriseInfoMock;
  let decoteServiceMock;
  let valuationRequestDocsServiceMock;
  let dataSharingMock;
  let valuationRequestWorkflowServiceMock;
  let paramSettingServiceMock;
  let authServiceMock;
  let catNatureSerivceMock;
  let catDiscountRateServiceMock;
  let svsDescCollabServiceMock;

  const financialPlans: FinancialPlanModel[] = [];
  const dateSubmit = new Date();
  const valuation: ValuationModel = {
    id: '1',
    discountRateComment: 'comment',
    experId: '3',
    controllerId: '5',
    validatorId: '11',
    descNetwork: null,
    descName: null,
    isSubmitted : null ,
    isDuplicated : null,
    catNature: {
      id: '1',
      label: 'nature test',
      catActivitySector: null,
      comment: 'comment',
      paramNap: null,
      isActive: null

    } as CatNatureModel,
    paramValidationType: {
      id: '1',
      label: 'test'
    },
    catBrand: {
      id: '1',
      label: 'test brand',
      catNature: null,
      comment: '',
      isActive: true,
      catRequest: null

    },
    catAssetType: {
      id: '1',
      label: 'test type',
      catBrand: null,
      comment: 'comment',
      isActive: true,
      catRequest: null


    },
    valuationRequest: {
      id: '1',
      externalSystemRef: 'S',
      underwriterId: '1',
      isRetUnderwriter: true,
      customerPropoId: '1',
      bssResponsible: '1',
      requestComment: 'comment',
      financingAmount: 450,
      assetCreationDate: new Date(),
      assetUsVolume: 50,
      collAssetEstimYear: 2020,
      co2Emission: 15,
      fiscPower: 7,
      isAssetUseAbroad: false,
      isANewAsset: true,
      isLicencePlateAvaileble: true,
      paramUsageUnit: null,
      unitaryQuantity: 2,
      bssResponsibleFullName: 'name',
      paramEnergy: {
        id: '1'
      } as ParamEnergyModel
    } as ValuationRequestModel,
    catActivitySector: {
      id: '1',
      label: 'test activity sector',
      comment: 'test comment',
      isActive: true


    },
    createdDate: new Date()
  };
  beforeEach(() => {

    svsDescCollabServiceMock = {
      getSvsDescCollab: jest.fn().mockReturnValue(of({
        maxAmount: 132_500,
        desc: {
          id: '1'
        } as SvsDescModel
      } as SvsDescCollabModel))
    };

    popupServiceMock = {
      popupInfo: jest.fn().mockReturnValue(of(true)),
      popupPDF: jest.fn().mockReturnValue(of(true)),
      popupCommunication: jest.fn().mockReturnValue(of(true)),
      onConfirmCommunication: new Subject(),
      onConfirmDescValidator: new Subject(),
      onConfirm: of(true),
      popupAskForValidation: jest.fn().mockReturnValue(of(true)),
      dismissAll: jest.fn().mockReturnValue(of(true)),
    };
    processingEquipementValuationServiceMock = {
      financialPlans: new Subject(),
      dateSubmit: new Subject(),
      loadFinancialPlanByValuationId: jest.fn().mockReturnValue(of(true)),
      getValuationResult: jest.fn().mockReturnValue(of(true)),
    };

    processingEquipementValuationServiceMock.financialPlans.next(financialPlans);
    processingEquipementValuationServiceMock.dateSubmit.next(dateSubmit);

    routeMock = {
      snapshot: {
        paramMap: {
          get: jest.fn().mockReturnValue('1')
        }
      }
    };
    routerMock = {
      navigate: jest.fn(),

      serializeUrl: jest.fn().mockReturnValue('url/url'),
      createUrlTree: jest.fn().mockReturnValue(new UrlTree())

    };
    valuationServiceMock = {
      getValuationById: jest.fn().mockReturnValue(of(valuation))
    };

    authServiceMock = {
      currentUserRolesList: [Role.ADMIN_SE, Role.SVS_EXPERT_SE],
        identityClaims : {
            mat: 'M102'
      }
    };
    financialPlans.push({} as FinancialPlanModel);
    financialPlans[0].collateralAssetRating = 5;
    financialPlans[0].valuation = {
      valuationRequest: {}as ValuationRequestModel
    } as ValuationModel;

    financialPlanServiceMock = {
      saveFinancialPlan: jest.fn().mockReturnValue(of(financialPlans)),
      loadFinancialPlanByValuationId: jest.fn().mockReturnValue(of(financialPlans)),
    };
    mdmInformationServiceMock = {
      getMdmCaInformation: jest.fn().mockReturnValue(of(true)),

    };
    decoteServiceMock = {
      getDiscountByValuationId: jest.fn().mockReturnValue(of(null)),
      getDiscountByValuationIdOrFromCat: jest.fn().mockReturnValue(of(true)),
    };

    entrepriseInfoMock = {
      entrepriseInfo: null,
      loadEntrepriseInfo: jest.fn().mockReturnValue(of({} as EntrepriseInfoModel)),


    };

    reportServiceMock = {
      generateReport: jest.fn().mockReturnValue(of(new Blob(['testing'], { type: 'application/pdf' })))
    };

    dataSharingMock = {
      changeStatus: jest.fn().mockReturnValue(of(true)),
      triggerCommunication: new Subject(),
      changeValoStatus: jest.fn().mockReturnValue(of(true)),

    };

    valuationRequestDocsServiceMock = {
      uploadValuationResult: jest.fn().mockReturnValue(of(true)),
      uploadValuationSimulation: jest.fn()
    };

    paramSettingServiceMock = {
      getParamByCode: jest.fn().mockReturnValue(of([{
        id: '11',
        label: 'En attente',
        value: StatusConstant.WAIT_VALID_FIL
      } as ParamSettingModel,
      {
        id: '12',
        label: 'En attente d\'info',
        value: StatusConstant.WAIT_INFO_FIL_EXP
      } as ParamSettingModel]))
    };

    valuationRequestWorkflowServiceMock = {
      saveValuationRequestWorkflow: jest.fn().mockReturnValue(of({} as ValuationRequestWorkflowModel)),
      getValuationRequestStatus: jest.fn().mockReturnValue(of({} as ValuationRequestWorkflowModel)),
      getValuationRequestHistory : jest.fn().mockReturnValue(of([{status : {value : StatusConstant.WAIT_VALID_FIL }} as ValuationRequestWorkflowModel])),

    };

    const catNatureValid = [{
      id: '12849'
    }as CatNatureModel,
    {
      id: '12768'
    }as CatNatureModel,
    {
      id:  '12953'
    }as CatNatureModel] as CatNatureModel[];
    catNatureSerivceMock = {
      getCatNatureValid: jest.fn().mockReturnValue(of(catNatureValid)),
    };

    catDiscountRateServiceMock = {
      getFirstYearRateByCatParams: jest.fn().mockReturnValue(of(80)),
    };

    window.URL.createObjectURL = jest.fn();
    spyOn(window, 'scrollTo').and.returnValue(true);

    fixture = new ResultExpertComponent(
      processingEquipementValuationServiceMock,
      popupServiceMock,
      routerMock,
      routeMock,
      valuationServiceMock,
      financialPlanServiceMock,
      mdmInformationServiceMock,
      reportServiceMock,
      entrepriseInfoMock,
      decoteServiceMock,
      valuationRequestDocsServiceMock,
      dataSharingMock,
      valuationRequestWorkflowServiceMock,
      paramSettingServiceMock,
      authServiceMock,
      catNatureSerivceMock,
      catDiscountRateServiceMock,
      svsDescCollabServiceMock
    );

    fixture.valuation = {
      id: '1',
      valuationRequest: {
        id: '1'
      } as ValuationRequestModel
    } as ValuationModel;

    fixture.ngOnInit();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('Test Component', () => {
    it('should Be Truthy', () => {
      expect(fixture).toBeTruthy();
    });
  });

  describe('Test ngOnInit', () => {
    it('should fill the list', () => {
      dataSharingMock.triggerCommunication.next(true);

      popupServiceMock.onConfirmCommunication.next({
        theme: {
          Specs: [{ action: StatusConstant.STATUS_VAL_WKFL }]
        }
      });
      expect(fixture.idValuation).toEqual('1');
      processingEquipementValuationServiceMock.financialPlans.next(financialPlans);
      processingEquipementValuationServiceMock.dateSubmit.next(dateSubmit);

      expect(fixture.valuation).toEqual(valuation);


    });
  });

  describe('Test navigation', () => {
    it('should navigate to reception', () => {
      fixture.returnToReception();
      expect(routerMock.navigate).toHaveBeenCalled();




    });
  });

  describe('Test saveSimulation', () => {


    it('should call saveFinancingPlan', () => {
      fixture.isAdminValidationNeeded = false;
      fixture.saveSimulation();
      expect(financialPlanServiceMock.saveFinancialPlan).toHaveBeenCalledWith(fixture.financialPlans);
      financialPlanServiceMock.saveFinancialPlan().subscribe(d => {
        expect(popupServiceMock.popupPDF).toHaveBeenCalled();
      });
    });
    it('should call popupInfo and saveFinancialPlan', () => {

      fixture.saveSimulation();
      popupServiceMock.onConfirm = new Subject();
      popupServiceMock.onConfirm.next(true);
      expect(financialPlanServiceMock.saveFinancialPlan).toHaveBeenCalledWith(fixture.financialPlans);
      expect(popupServiceMock.popupInfo).toHaveBeenCalled();
      popupServiceMock.onConfirm.next(true);
    });

    it('should call popupInfo and saveFinancialPlan', () => {
      jest.spyOn(valuationRequestDocsServiceMock, 'uploadValuationSimulation').mockReturnValue(of(error));
      const spyInstance = jest.spyOn(fixture, 'saveValuationRequestWorkflow');
      fixture.saveSimulation();
      popupServiceMock.onConfirm = new Subject();
      popupServiceMock.onConfirm.next(true);
      expect(spyInstance).toHaveBeenCalled();
    });

    it('should call saveFinancingPlan', () => {
      fixture.isAdminValidationNeeded = true;
      fixture.isCurrentUserAdmin = false;
      fixture.financialPlans = [];
      fixture.saveSimulation();
      expect(popupServiceMock.popupInfo).toHaveBeenCalled();
      expect(financialPlanServiceMock.saveFinancialPlan).toHaveBeenCalledWith(fixture.financialPlans);
    });


  });


  describe('Test saveResult', () => {


    it('should call saveFinancingPlan as admin', () => {
      fixture.isAdminValidationNeeded = false;
      fixture.saveResult();
      expect(financialPlanServiceMock.saveFinancialPlan).toHaveBeenCalledWith(fixture.financialPlans);
      financialPlanServiceMock.saveFinancialPlan().subscribe(d => {
        expect(popupServiceMock.popupPDF).toHaveBeenCalled();
      });
    });

    it('should call saveFinancingPlan as expert', () => {
      fixture.isAdminValidationNeeded = false;
      fixture.isCurrentUserAdmin = false;

      fixture.saveResult();
      expect(financialPlanServiceMock.saveFinancialPlan).toHaveBeenCalledWith(fixture.financialPlans);
      financialPlanServiceMock.saveFinancialPlan().subscribe(d => {
        expect(popupServiceMock.popupPDF).toHaveBeenCalled();
      });
    });

    it('should call popupInfo and saveFinancialPlan', () => {
      fixture.isAdminValidationNeeded = true;
      fixture.saveResult();
      popupServiceMock.onConfirm = new Subject();
      popupServiceMock.onConfirm.next(true);
      expect(financialPlanServiceMock.saveFinancialPlan).toHaveBeenCalledWith(fixture.financialPlans);
      expect(popupServiceMock.popupInfo).toHaveBeenCalled();
      popupServiceMock.onConfirm.next(true);
    });


    it('should call saveFinancingPlan', () => {
      fixture.isAdminValidationNeeded = true;
      fixture.isCurrentUserAdmin = false;
      fixture.financialPlans = [];
      fixture.saveResult();
      expect(popupServiceMock.popupInfo).toHaveBeenCalled();
      expect(financialPlanServiceMock.saveFinancialPlan).toHaveBeenCalledWith(fixture.financialPlans);
    });

  });

  describe('Test checkIfMaxAmountIsOverLimit()', () => {
    it('should isOverProvidedPrice be true when financingAmount>2000000', () => {
      fixture.financialPlans = [{
        id: '1',
        planDuration: 180
      } as FinancialPlanModel] as FinancialPlanModel[];
      fixture.valuation = {
        id: '1',
        catActivitySector: {} as CatActivitySectorModel,
        catNature: {
          id: '12768',
          label: 'avion'
        } as CatNatureModel,
        catBrand: {}  as CatBrandModel,
        catAssetType: {} as CatAssetTypeModel,
        valuationRequest: {
          id: '1', financingAmount: 3000000000
        } as ValuationRequestModel
      } as ValuationModel;
      fixture.isCurrentUserAdmin = false;
      fixture.checkIfAdminValidationNeeded();
      expect(fixture.isAdminValidationNeeded).toEqual(true);
    });

    it('should isOverProvidedPrice be true when nature is valid', () => {
      fixture.financialPlans = [];
      fixture.valuation = {
        id: '1',
        catActivitySector: {} as CatActivitySectorModel,
        catNature: {
          id: '12768',
          label: 'avion'
        } as CatNatureModel,
        catBrand: {}  as CatBrandModel,
        catAssetType: {} as CatAssetTypeModel,
        valuationRequest: {
          id: '1', financingAmount: 100000
        } as ValuationRequestModel
      } as ValuationModel;
      fixture.isCurrentUserAdmin = false;
      fixture.checkIfAdminValidationNeeded();
      expect(fixture.isAdminValidationNeeded).toEqual(true);
    });

    it('should isOverProvidedPrice be true when plan duration > 7 years', () => {
      fixture.financialPlans = [{
        id: '1',
        planDuration: 180
      } as FinancialPlanModel] as FinancialPlanModel[];
      fixture.valuation = {
        id: '1',
        catActivitySector: {} as CatActivitySectorModel,
        catNature: {
          id: '1',
          label: 'abateuse'
        } as CatNatureModel,
        catBrand: {}  as CatBrandModel,
        catAssetType: {} as CatAssetTypeModel,
        valuationRequest: {
          id: '1', financingAmount: 100000
        } as ValuationRequestModel
      } as ValuationModel;
      fixture.isCurrentUserAdmin = false;
      fixture.checkIfAdminValidationNeeded();
      expect(fixture.isAdminValidationNeeded).toEqual(true);
    });

    it('should isOverProvidedPrice be true when abs(catdiscount firstYearRate-discount firstYearRate) > 10', () => {
      fixture.firstYearRate = 80;
      fixture.discount = [{
        id: '101',
        discountRateYear: 1,
        discountRateValue: 95
      } as ValuationDiscountModel] as ValuationDiscountModel[];
      fixture.financialPlans = [];
      fixture.valuation = {
        id: '1',
        catActivitySector: {} as CatActivitySectorModel,
        catNature: {
          id: '1',
          label: 'abateuse'
        } as CatNatureModel,
        catBrand: {}  as CatBrandModel,
        catAssetType: {} as CatAssetTypeModel,
        valuationRequest: {
          id: '1', financingAmount: 100000
        } as ValuationRequestModel
      } as ValuationModel;
      fixture.isCurrentUserAdmin = false;
      fixture.checkIfAdminValidationNeeded();
      expect(fixture.isAdminValidationNeeded).toEqual(true);
    });

  });


  describe('Test blobToFile ()', () => {
    it('should return file', () => {
      fixture.valuation = {
        id: '1',
        valuationRequest: {
          id: '1'
        } as ValuationRequestModel
      } as ValuationModel;
      expect(fixture.blobToFile({} as Blob)).toBeDefined();
    });
  });

  describe('Test ngOnDestroy ()', () => {
    it('should unsubscribe', () => {

      fixture.ngOnDestroy();
      expect(dataSharingMock.changeStatus).toHaveBeenCalled();
    });
  });

  describe('Test saveValuationRequestWorkflow()', () => {
    it('should valuationRequestWorkflow status be WAIT_INFO_EXP_CA', () => {
      const resp = {
        valuationRequest: {
          id : '1'
        }as ValuationRequestModel,
        status: {
          value : StatusConstant.WAIT_INFO_EXP_CA,
          label: 'en attente d\'information'
        } as ParamSettingModel
      } as ValuationRequestWorkflowModel;
      spyOn(valuationRequestWorkflowServiceMock, 'saveValuationRequestWorkflow').and.returnValue(of(resp));
      fixture.saveValuationRequestWorkflow({value: StatusConstant.WAIT_INFO_EXP_CA, label : 'en attente d\'information'}as ParamSettingModel);
      expect(valuationRequestWorkflowServiceMock.saveValuationRequestWorkflow).toHaveBeenCalled();
      expect(fixture.valuationRequestWorkflow.status.value).toEqual(StatusConstant.WAIT_INFO_EXP_CA);

    });


    it('should valuationRequestWorkflow status be WAIT_INFO_EXP_FIL', () => {
      const resp = {
        valuationRequest: {
          id : '1'
        }as ValuationRequestModel,
        status: {
          value : StatusConstant.WAIT_INFO_EXP_FIL,
          label: 'en attente d\'information'
        } as ParamSettingModel
      } as ValuationRequestWorkflowModel;
      spyOn(valuationRequestWorkflowServiceMock, 'saveValuationRequestWorkflow').and.returnValue(of(resp));
      fixture.saveValuationRequestWorkflow({value: StatusConstant.WAIT_INFO_EXP_FIL, label : 'en attente d\'information'}as ParamSettingModel);
      expect(valuationRequestWorkflowServiceMock.saveValuationRequestWorkflow).toHaveBeenCalled();
      expect(fixture.valuationRequestWorkflow.status.value).toEqual(StatusConstant.WAIT_INFO_EXP_FIL);

    });

    it('should valuationRequestWorkflow status be WAIT_VALID_FIL', () => {
      const resp = {
        valuationRequest: {
          id : '1'
        }as ValuationRequestModel,
        status: {
          value : StatusConstant.WAIT_VALID_FIL,
          label: 'en attente d\'information'
        } as ParamSettingModel
      } as ValuationRequestWorkflowModel;
      spyOn(valuationRequestWorkflowServiceMock, 'saveValuationRequestWorkflow').and.returnValue(of(resp));
      fixture.saveValuationRequestWorkflow({value: StatusConstant.WAIT_VALID_FIL, label : 'en attente de validation'}as ParamSettingModel);
      expect(valuationRequestWorkflowServiceMock.saveValuationRequestWorkflow).toHaveBeenCalled();
      expect(fixture.valuationRequestWorkflow.status.value).toEqual(StatusConstant.WAIT_VALID_FIL);
    });

    it('should valuationRequestWorkflow status be WAIT_INFO_FIL_EXP', () => {
      const resp = {
        valuationRequest: {
          id : '1'
        }as ValuationRequestModel,
        status: {
          value : StatusConstant.WAIT_INFO_FIL_EXP,
          label: 'en attente d\'information'
        } as ParamSettingModel
      } as ValuationRequestWorkflowModel;
      spyOn(valuationRequestWorkflowServiceMock, 'saveValuationRequestWorkflow').and.returnValue(of(resp));
      fixture.saveValuationRequestWorkflow({value: StatusConstant.WAIT_INFO_FIL_EXP, label : 'en attente de validation'}as ParamSettingModel);
      expect(valuationRequestWorkflowServiceMock.saveValuationRequestWorkflow).toHaveBeenCalled();
      expect(fixture.valuationRequestWorkflow.status.value).toEqual(StatusConstant.WAIT_INFO_FIL_EXP);
    });

    it('should valuationRequestWorkflow status be RESP_INFO_EXP_FIL', () => {
      const resp = {
        valuationRequest: {
          id : '1'
        }as ValuationRequestModel,
        status: {
          value : StatusConstant.RESP_INFO_EXP_FIL,
          label: 'en attente d\'information'
        } as ParamSettingModel
      } as ValuationRequestWorkflowModel;
      spyOn(valuationRequestWorkflowServiceMock, 'saveValuationRequestWorkflow').and.returnValue(of(resp));
      fixture.saveValuationRequestWorkflow({value: StatusConstant.RESP_INFO_EXP_FIL, label : 'en attente de validation'}as ParamSettingModel);
      expect(valuationRequestWorkflowServiceMock.saveValuationRequestWorkflow).toHaveBeenCalled();
      expect(fixture.valuationRequestWorkflow.status.value).toEqual(StatusConstant.RESP_INFO_EXP_FIL);
    });

    it('should valuationRequestWorkflow status be RESP_INFO_EXP_FIL', () => {
      const resp = {
        valuationRequest: {
          id : '1'
        }as ValuationRequestModel,
        status: {
          value : StatusConstant.RESP_INFO_EXP_FIL,
          label: 'en attente d\'information'
        } as ParamSettingModel
      } as ValuationRequestWorkflowModel;
      spyOn(valuationRequestWorkflowServiceMock, 'saveValuationRequestWorkflow').and.returnValue(of(resp));
      fixture.saveValuationRequestWorkflow({value: StatusConstant.RESP_INFO_EXP_FIL, label : 'en attente de validation'}as ParamSettingModel);
      expect(valuationRequestWorkflowServiceMock.saveValuationRequestWorkflow).toHaveBeenCalled();
      expect(fixture.valuationRequestWorkflow.status.value).toEqual(StatusConstant.RESP_INFO_EXP_FIL);
    });

    it('should valuationRequestWorkflow status be RESP_INFO_CA_EXP', () => {
      const resp = {
        valuationRequest: {
          id : '1'
        }as ValuationRequestModel,
        status: {
          value : StatusConstant.RESP_INFO_CA_EXP,
          label: 'en attente d\'information'
        } as ParamSettingModel
      } as ValuationRequestWorkflowModel;
      spyOn(valuationRequestWorkflowServiceMock, 'saveValuationRequestWorkflow').and.returnValue(of(resp));
      fixture.saveValuationRequestWorkflow({value: StatusConstant.RESP_INFO_CA_EXP, label : 'en attente de validation'}as ParamSettingModel);
      expect(valuationRequestWorkflowServiceMock.saveValuationRequestWorkflow).toHaveBeenCalled();
      expect(fixture.valuationRequestWorkflow.status.value).toEqual(StatusConstant.RESP_INFO_CA_EXP);
    });

    it('should valuationRequestWorkflow status be RESP_INFO_FIL_EXP', () => {
      const resp = {
        valuationRequest: {
          id : '1'
        }as ValuationRequestModel,
        status: {
          value : StatusConstant.RESP_INFO_FIL_EXP,
          label: 'en attente d\'information'
        } as ParamSettingModel
      } as ValuationRequestWorkflowModel;
      spyOn(valuationRequestWorkflowServiceMock, 'saveValuationRequestWorkflow').and.returnValue(of(resp));
      fixture.saveValuationRequestWorkflow({value: StatusConstant.RESP_INFO_FIL_EXP, label : 'en attente de validation'}as ParamSettingModel);
      expect(valuationRequestWorkflowServiceMock.saveValuationRequestWorkflow).toHaveBeenCalled();
      expect(fixture.valuationRequestWorkflow.status.value).toEqual(StatusConstant.RESP_INFO_FIL_EXP);
    });

    it('should valuationRequestWorkflow status be FIL_VALIDATED', () => {
      const resp = {
        valuationRequest: {
          id : '1'
        }as ValuationRequestModel,
        status: {
          value : StatusConstant.FIL_VALIDATED,
          label: 'validée par la filière'
        } as ParamSettingModel
      } as ValuationRequestWorkflowModel;
      spyOn(valuationRequestWorkflowServiceMock, 'saveValuationRequestWorkflow').and.returnValue(of(resp));
      fixture.saveValuationRequestWorkflow({value: StatusConstant.FIL_VALIDATED, label : 'validée par la filière'}as ParamSettingModel);
      expect(valuationRequestWorkflowServiceMock.saveValuationRequestWorkflow).toHaveBeenCalled();
      expect(fixture.valuationRequestWorkflow.status.value).toEqual(StatusConstant.FIL_VALIDATED);
    });

    it('should valuationRequestWorkflow status be REJECTED', () => {
      const resp = {
        valuationRequest: {
          id : '1'
        }as ValuationRequestModel,
        status: {
          value : StatusConstant.REJECTED,
          label: 'rejeté'
        } as ParamSettingModel
      } as ValuationRequestWorkflowModel;
      spyOn(valuationRequestWorkflowServiceMock, 'saveValuationRequestWorkflow').and.returnValue(of(resp));
      fixture.saveValuationRequestWorkflow({value: StatusConstant.REJECTED, label : 'rejeté'}as ParamSettingModel);
      expect(valuationRequestWorkflowServiceMock.saveValuationRequestWorkflow).toHaveBeenCalled();
      expect(fixture.valuationRequestWorkflow.status.value).toEqual(StatusConstant.REJECTED);
    });

    it('should valuationRequestWorkflow status be EXP_VALIDATED', () => {
      const resp = {
        valuationRequest: {
          id : '1'
        }as ValuationRequestModel,
        status: {
          value : StatusConstant.EXP_VALIDATED,
          label: 'en attente d\'information'
        } as ParamSettingModel
      } as ValuationRequestWorkflowModel;
      spyOn(valuationRequestWorkflowServiceMock, 'saveValuationRequestWorkflow').and.returnValue(of(resp));
      fixture.saveValuationRequestWorkflow({value: StatusConstant.EXP_VALIDATED, label : 'en attente de validation'}as ParamSettingModel);
      expect(valuationRequestWorkflowServiceMock.saveValuationRequestWorkflow).toHaveBeenCalled();
      expect(fixture.valuationRequestWorkflow.status.value).toEqual(StatusConstant.EXP_VALIDATED);
    });

    it('should valuationRequestWorkflow status be WAIT_VALID_MAN_EXP_QUAL', () => {
      const resp = {
        valuationRequest: {
          id : '1'
        }as ValuationRequestModel,
        status: {
          value : StatusConstant.WAIT_VALID_MAN_EXP_QUAL,
          label: 'en attente d\'information'
        } as ParamSettingModel
      } as ValuationRequestWorkflowModel;
      spyOn(valuationRequestWorkflowServiceMock, 'saveValuationRequestWorkflow').and.returnValue(of(resp));
      fixture.saveValuationRequestWorkflow({value: StatusConstant.WAIT_VALID_MAN_EXP_QUAL, label : 'en attente d\'information'}as ParamSettingModel, '', {fullName: 'xxx'} as MdmInformationsModel);
      expect(valuationRequestWorkflowServiceMock.saveValuationRequestWorkflow).toHaveBeenCalled();
      expect(fixture.valuationRequestWorkflow.status.value).toEqual(StatusConstant.WAIT_VALID_MAN_EXP_QUAL);

    });

  });

  describe('Test loadValuationRequestWorkflow()', () => {
    it('should getValuationRequestStatus ', () => {
      fixture.financialPlans = [];
      fixture.valuationRequestWorkflow = {
        id: '1'
      } as ValuationRequestWorkflowModel;
      fixture.valuation = {
        id: '1',
        catActivitySector: {} as CatActivitySectorModel,
        catNature: {
          label: 'avion'
        } as CatNatureModel,
        catBrand: {}  as CatBrandModel,
        catAssetType: {} as CatAssetTypeModel,
        valuationRequest: {
          id: '1', financingAmount: 3000000000
        } as ValuationRequestModel
      } as ValuationModel;
      fixture.isCurrentUserAdmin = false;
      fixture.checkIfAdminValidationNeeded();
      expect(fixture.isAdminValidationNeeded).toEqual(true);
    });

  });


  describe('Test blobToFile ()', () => {
    it('should return file', () => {
      fixture.valuation = {
        id: '1',
        valuationRequest: {
          id: '1'
        } as ValuationRequestModel
      } as ValuationModel;
      expect(fixture.blobToFile({} as Blob)).toBeDefined();
    });
  });



  describe('Test loadValuationRequestWorkflow()', () => {
    it('should getValuationRequestStatus ', () => {
      fixture.valuationRequestWorkflow = {
        id: '1',

      } as ValuationRequestWorkflowModel;
      fixture.valuation = {
        id: '1',
        valuationRequest: {
          id: '1'
        } as ValuationRequestModel
      } as ValuationModel;

      spyOn(valuationRequestWorkflowServiceMock, 'getValuationRequestHistory').and.returnValue(of([
        {status: {value: StatusConstant.WAIT_INFO_EXP_FIL}},

        {status: {value: StatusConstant.RESP_INFO_EXP_FIL}}
      ]));
      fixture.loadValuationRequestWorkflow();

      expect(valuationRequestWorkflowServiceMock.getValuationRequestStatus).toHaveBeenCalled();
    });

    it('should deploy popup ', () => {

      authServiceMock = {
        currentUserRolesList: [Role.ADMIN_SE]
      };
      valuationRequestWorkflowServiceMock = {
        saveValuationRequestWorkflow: jest.fn().mockReturnValue(of({} as ValuationRequestWorkflowModel)),
        getValuationRequestStatus: jest.fn().mockReturnValue(of({status : {value : StatusConstant.WAIT_VALID_FIL , label: 'E attente de validation'}} as ValuationRequestWorkflowModel)),
        getValuationRequestHistory : jest.fn().mockReturnValue(of([{status : {value : StatusConstant.WAIT_VALID_FIL }} as ValuationRequestWorkflowModel, {status : {value : StatusConstant.RESP_INFO_EXP_FIL }} as ValuationRequestWorkflowModel])),

      };
      fixture = new ResultExpertComponent(
        processingEquipementValuationServiceMock,
        popupServiceMock,
        routerMock,
        routeMock,
        valuationServiceMock,
        financialPlanServiceMock,
        mdmInformationServiceMock,
        reportServiceMock,
        entrepriseInfoMock,
        decoteServiceMock,
        valuationRequestDocsServiceMock,
        dataSharingMock,
        valuationRequestWorkflowServiceMock,
        paramSettingServiceMock,
        authServiceMock,
        catNatureSerivceMock,
        catDiscountRateServiceMock,
        svsDescCollabServiceMock
      );

      fixture.valuationRequestWorkflow = {
        id: '1',

      } as ValuationRequestWorkflowModel;
      fixture.valuation = {
        id: '1',
        valuationRequest: {
          id: '1'
        } as ValuationRequestModel
      } as ValuationModel;
      spyOn(fixture, 'deployCommunicatioPopup');
      fixture.loadValuationRequestWorkflow();
      valuationRequestWorkflowServiceMock.getValuationRequestHistory().subscribe((r) => {
        expect(fixture.deployCommunicatioPopup).toHaveBeenCalled();

      });

    });

    it('should deploy popup expert val mananager rejected', () => {

      authServiceMock = {
        currentUserRolesList: [Role.SVS_EXPERT_SE]
      };
      valuationRequestWorkflowServiceMock = {
        saveValuationRequestWorkflow: jest.fn().mockReturnValue(of({} as ValuationRequestWorkflowModel)),
        getValuationRequestStatus: jest.fn().mockReturnValue(of({status : {value : StatusConstant.WAIT_EXP_VAL , label: 'E attente de validation'}} as ValuationRequestWorkflowModel)),
        getValuationRequestHistory : jest.fn().mockReturnValue(of([{status : {value : StatusConstant.WAIT_EXP_VAL }} as ValuationRequestWorkflowModel, {status : {value : StatusConstant.VAL_MAN_EXP_QUALI_REJ }} as ValuationRequestWorkflowModel])),

      };
      fixture = new ResultExpertComponent(
        processingEquipementValuationServiceMock,
        popupServiceMock,
        routerMock,
        routeMock,
        valuationServiceMock,
        financialPlanServiceMock,
        mdmInformationServiceMock,
        reportServiceMock,
        entrepriseInfoMock,
        decoteServiceMock,
        valuationRequestDocsServiceMock,
        dataSharingMock,
        valuationRequestWorkflowServiceMock,
        paramSettingServiceMock,
        authServiceMock,
        catNatureSerivceMock,
        catDiscountRateServiceMock,
        svsDescCollabServiceMock
      );

      fixture.valuationRequestWorkflow = {
        id: '1',

      } as ValuationRequestWorkflowModel;
      fixture.valuation = {
        id: '1',
        valuationRequest: {
          id: '1'
        } as ValuationRequestModel
      } as ValuationModel;
      spyOn(fixture, 'deployCommunicatioPopup');
      fixture.loadValuationRequestWorkflow();
      valuationRequestWorkflowServiceMock.getValuationRequestHistory().subscribe((r) => {
        expect(fixture.deployCommunicatioPopup).toHaveBeenCalled();

      });

    });


    it('should deploy popup expert val filier rejected', () => {

      authServiceMock = {
        currentUserRolesList: [Role.SVS_EXPERT_SE]
      };
      valuationRequestWorkflowServiceMock = {
        saveValuationRequestWorkflow: jest.fn().mockReturnValue(of({} as ValuationRequestWorkflowModel)),
        getValuationRequestStatus: jest.fn().mockReturnValue(of({status : {value : StatusConstant.VAL_FIL_REJ , label: 'E attente de validation'}} as ValuationRequestWorkflowModel)),
        getValuationRequestHistory : jest.fn().mockReturnValue(of([{status : {value : StatusConstant.VAL_FIL_REJ }} as ValuationRequestWorkflowModel, {status : {value : StatusConstant.WAIT_VALID_FIL }} as ValuationRequestWorkflowModel])),

      };
      fixture = new ResultExpertComponent(
        processingEquipementValuationServiceMock,
        popupServiceMock,
        routerMock,
        routeMock,
        valuationServiceMock,
        financialPlanServiceMock,
        mdmInformationServiceMock,
        reportServiceMock,
        entrepriseInfoMock,
        decoteServiceMock,
        valuationRequestDocsServiceMock,
        dataSharingMock,
        valuationRequestWorkflowServiceMock,
        paramSettingServiceMock,
        authServiceMock,
        catNatureSerivceMock,
        catDiscountRateServiceMock,
        svsDescCollabServiceMock
      );

      fixture.valuationRequestWorkflow = {
        id: '1',

      } as ValuationRequestWorkflowModel;
      fixture.valuation = {
        id: '1',
        valuationRequest: {
          id: '1'
        } as ValuationRequestModel
      } as ValuationModel;
      spyOn(fixture, 'deployCommunicatioPopup');
      fixture.loadValuationRequestWorkflow();
      valuationRequestWorkflowServiceMock.getValuationRequestHistory().subscribe((r) => {
        expect(fixture.deployCommunicatioPopup).toHaveBeenCalled();

      });

    });

    it('should not deploy popup ', () => {
      authServiceMock = {
        currentUserRolesList: []
      };

      valuationRequestWorkflowServiceMock = {
        saveValuationRequestWorkflow: jest.fn().mockReturnValue(of({} as ValuationRequestWorkflowModel)),
        getValuationRequestStatus: jest.fn().mockReturnValue(of({status : {value : StatusConstant.REJECTED , label: 'Rejetée'}} as ValuationRequestWorkflowModel)),
        getValuationRequestHistory : jest.fn().mockReturnValue(of([{status : {value : StatusConstant.WAIT_INFO_FIL_EXP }} as ValuationRequestWorkflowModel, {status : {value : StatusConstant.WAIT_VALID_FIL }} as ValuationRequestWorkflowModel])),

      };
      fixture = new ResultExpertComponent(
        processingEquipementValuationServiceMock,
        popupServiceMock,
        routerMock,
        routeMock,
        valuationServiceMock,
        financialPlanServiceMock,
        mdmInformationServiceMock,
        reportServiceMock,
        entrepriseInfoMock,
        decoteServiceMock,
        valuationRequestDocsServiceMock,
        dataSharingMock,
        valuationRequestWorkflowServiceMock,
        paramSettingServiceMock,
        authServiceMock,
        catNatureSerivceMock,
        catDiscountRateServiceMock,
        svsDescCollabServiceMock
      );

      fixture.valuationRequestWorkflow = {
        id: '1',

      } as ValuationRequestWorkflowModel;
      fixture.valuation = {
        id: '1',
        valuationRequest: {
          id: '1'
        } as ValuationRequestModel
      } as ValuationModel;
      fixture.loadValuationRequestWorkflow();

      expect(valuationRequestWorkflowServiceMock.getValuationRequestStatus).toHaveBeenCalled();
      valuationRequestWorkflowServiceMock.getValuationRequestStatus().subscribe(res => {
        expect(fixture.disabled ).toEqual(true);
      });
    });

    it('should deploy popup as expert', () => {
      authServiceMock = {
        currentUserRolesList: [Role.SVS_EXPERT_SE]
      };

      valuationRequestWorkflowServiceMock = {
        saveValuationRequestWorkflow: jest.fn().mockReturnValue(of({} as ValuationRequestWorkflowModel)),
        getValuationRequestStatus: jest.fn().mockReturnValue(of({status : {value : StatusConstant.REJECTED , label: 'Rejetée'}} as ValuationRequestWorkflowModel)),
        getValuationRequestHistory : jest.fn().mockReturnValue(of([{status : {value : StatusConstant.WAIT_INFO_FIL_EXP }} as ValuationRequestWorkflowModel, {status : {value : StatusConstant.WAIT_VALID_FIL }} as ValuationRequestWorkflowModel])),

      };
      fixture = new ResultExpertComponent(
        processingEquipementValuationServiceMock,
        popupServiceMock,
        routerMock,
        routeMock,
        valuationServiceMock,
        financialPlanServiceMock,
        mdmInformationServiceMock,
        reportServiceMock,
        entrepriseInfoMock,
        decoteServiceMock,
        valuationRequestDocsServiceMock,
        dataSharingMock,
        valuationRequestWorkflowServiceMock,
        paramSettingServiceMock,
        authServiceMock,
        catNatureSerivceMock,
        catDiscountRateServiceMock,
        svsDescCollabServiceMock
      );

      fixture.valuationRequestWorkflow = {
        id: '1',

      } as ValuationRequestWorkflowModel;
      fixture.valuation = {
        id: '1',
        valuationRequest: {
          id: '1'
        } as ValuationRequestModel
      } as ValuationModel;
      fixture.loadValuationRequestWorkflow();

      expect(valuationRequestWorkflowServiceMock.getValuationRequestStatus).toHaveBeenCalled();
      valuationRequestWorkflowServiceMock.getValuationRequestStatus().subscribe(res => {
        expect(fixture.disabled ).toEqual(true);
      });
    });
    it('no workflow ', () => {
      authServiceMock = {
        currentUserRolesList: [Role.SVS_EXPERT_SE]
      };

      valuationRequestWorkflowServiceMock = {
        saveValuationRequestWorkflow: jest.fn().mockReturnValue(of({} as ValuationRequestWorkflowModel)),
        getValuationRequestStatus: jest.fn().mockReturnValue(of(null)),
        getValuationRequestHistory : jest.fn().mockReturnValue(of([ ])),

      };
      fixture = new ResultExpertComponent(
        processingEquipementValuationServiceMock,
        popupServiceMock,
        routerMock,
        routeMock,
        valuationServiceMock,
        financialPlanServiceMock,
        mdmInformationServiceMock,
        reportServiceMock,
        entrepriseInfoMock,
        decoteServiceMock,
        valuationRequestDocsServiceMock,
        dataSharingMock,
        valuationRequestWorkflowServiceMock,
        paramSettingServiceMock,
        authServiceMock,
        catNatureSerivceMock,
        catDiscountRateServiceMock,
        svsDescCollabServiceMock
      );

      fixture.valuationRequestWorkflow = {
        id: '1',

      } as ValuationRequestWorkflowModel;
      fixture.valuation = {
        id: '1',
        valuationRequest: {
          id: '1'
        } as ValuationRequestModel
      } as ValuationModel;
      spyOn(fixture, 'deployCommunicatioPopup');
      fixture.loadValuationRequestWorkflow();
      valuationRequestWorkflowServiceMock.getValuationRequestHistory().subscribe((r) => {
        expect(fixture.deployCommunicatioPopup).toHaveBeenCalledTimes(0);

      });
    });
  });


  describe('Test loadFinantialPlans()', () => {
    it('should load loadFinancialPlanByValuationId when financialPlans is undefined', () => {
      processingEquipementValuationServiceMock.financialPlans.next(null);
      fixture.idValuation = '1';
      fixture.loadFinantialPlans();
      processingEquipementValuationServiceMock.financialPlans.subscribe(res => {


        expect(fixture.financialPlans).toBeNull();
        expect(financialPlanServiceMock.loadFinancialPlanByValuationId).toHaveBeenCalled();
      });
    });
  });

  describe('Test loadEnterpriseInfo()', () => {
    it('should load loadEntrepriseInfo with RET variable when entrepriseInfo is null', () => {
      fixture.valuation = {
        id: '1',
        valuationRequest: {
          id: '1',
          isRetUnderwriter: true,
          underwriterId: '1'
        } as ValuationRequestModel
      } as ValuationModel;

      fixture.loadEnterpriseInfo();
      expect(entrepriseInfoMock.loadEntrepriseInfo).toHaveBeenCalled();
    });

    it('should load loadEntrepriseInfo with BCP variable when entrepriseInfo is null', () => {
      valuation.valuationRequest.isRetUnderwriter = false;
      valuationServiceMock = {
        getValuationById: jest.fn().mockReturnValue(of(valuation))
      };
      fixture = new ResultExpertComponent(
        processingEquipementValuationServiceMock,
        popupServiceMock,
        routerMock,
        routeMock,
        valuationServiceMock,
        financialPlanServiceMock,
        mdmInformationServiceMock,
        reportServiceMock,
        entrepriseInfoMock,
        decoteServiceMock,
        valuationRequestDocsServiceMock,
        dataSharingMock,
        valuationRequestWorkflowServiceMock,
        paramSettingServiceMock,
        authServiceMock,
        catNatureSerivceMock,
        catDiscountRateServiceMock,
        svsDescCollabServiceMock
      );
      fixture.valuation = {
        id: '1',
        valuationRequest: {
          id: '1',
          isRetUnderwriter: false,
          underwriterId: '1'
        } as ValuationRequestModel
      } as ValuationModel;

      fixture.loadEnterpriseInfo();
      expect(entrepriseInfoMock.loadEntrepriseInfo).toHaveBeenCalled();
    });

  });

  describe('Test:  deployCommunicatioPopup', () => {
    it('deployCommunicatioPopup', () => {

      fixture.statusList = [{value : StatusConstant.WAIT_INFO_FIL_EXP} as ParamSettingModel];

      fixture.valuationRequestWorkflow = {status : {value : StatusConstant.WAIT_VALID_FIL}} as ValuationRequestWorkflowModel;
      fixture.valuationRequestHistory = [{status : {value : StatusConstant.WAIT_VALID_FIL}} as ValuationRequestWorkflowModel, {status : {value : StatusConstant.WAIT_EXP_VAL}} as ValuationRequestWorkflowModel];
      fixture.deployCommunicatioPopup(null);
      popupServiceMock.onConfirmCommunication.next({theme: { theme: 'ss', Specs: [{ action: StatusConstant.WAIT_INFO_FIL_EXP }]} });

      expect(popupServiceMock.popupCommunication).toHaveBeenCalled();

    });
    it('deployCommunicatioPopup', () => {

      fixture.statusList = [{value : StatusConstant.WAIT_INFO_FIL_EXP} as ParamSettingModel];

      fixture.valuationRequestWorkflow = {status : {value : StatusConstant.WAIT_VALID_FIL}} as ValuationRequestWorkflowModel;
      fixture.valuationRequestHistory = [{status : {value : StatusConstant.WAIT_VALID_FIL}} as ValuationRequestWorkflowModel, {status : {value : StatusConstant.WAIT_EXP_VAL}} as ValuationRequestWorkflowModel];
      fixture.deployCommunicatioPopup(null);
      popupServiceMock.onConfirmCommunication.next(null);

      expect(popupServiceMock.popupCommunication).toHaveBeenCalled();

    });

  });


  describe('Test isValuationRequestDurationExceptional()', () => {
    it('should return true when a plan duration > 7 years', () => {
      fixture.financialPlans = [{
        id: '1',
        planDuration: 180
      }as FinancialPlanModel,
      {
        id: '2',
        planDuration: 36
      }as FinancialPlanModel];

      expect(fixture.isValuationRequestDurationExceptional()).toEqual(true);
    });

    it('should return true when a plan duration < 2 years', () => {
      fixture.financialPlans = [{
        id: '1',
        planDuration: 2
      }as FinancialPlanModel,
      {
        id: '2',
        planDuration: 180
      }as FinancialPlanModel];

      expect(fixture.isValuationRequestDurationExceptional()).toEqual(true);
    });

    it('should return false when a plan duration > 2 years and < 7 years', () => {
      fixture.financialPlans = [{
        id: '1',
        planDuration: 24
      }as FinancialPlanModel,
      {
        id: '2',
        planDuration: 24
      }as FinancialPlanModel];

      expect(fixture.isValuationRequestDurationExceptional()).toEqual(false);
    });

    it('should return true when a plan duration > 7 years', () => {
      fixture.financialPlans = [];

      expect(fixture.isValuationRequestDurationExceptional()).toEqual(false);
    });

  });

  describe('Test isFinancingAmountExceptional()', () => {
    it('should return true when financingAmount>2_000_000', () => {
      fixture.valuation = {
        id: '1',
        valuationRequest: {
          id: '1',
          financingAmount: 3000000000
        } as ValuationRequestModel
      } as ValuationModel;

      expect(fixture.isFinancingAmountExceptional()).toEqual(true);
    });

    it('should return false when financingAmount<2_000_000', () => {
      fixture.valuation = {
        id: '1',
        valuationRequest: {
          id: '1',
          financingAmount: 30000
        } as ValuationRequestModel
      } as ValuationModel;

      expect(fixture.isFinancingAmountExceptional()).toEqual(false);
    });

  });

  describe('Test isValuationRequestNaturesExceptional()', () => {
    it('should return true when catNature id is included in catNatureService.getExceptionalCatNatures response', () => {
      fixture.valuation = {
        id: '1',
        catActivitySector: {} as CatActivitySectorModel,
        catNature: {
          id: '12768',
          label: 'avion'
        } as CatNatureModel,
        catBrand: {}  as CatBrandModel,
        catAssetType: {} as CatAssetTypeModel,
        valuationRequest: {
          id: '1', financingAmount: 3000000000
        } as ValuationRequestModel
      } as ValuationModel;

      expect(fixture.isValuationRequestNaturesExceptional()).toEqual(true);

    });

    it('should return false when catNature id not is included in catNatureService.getExceptionalCatNatures response', () => {
      fixture.valuation = {
        id: '1',
        catActivitySector: {} as CatActivitySectorModel,
        catNature: {
          id: '1',
          label: 'avion'
        } as CatNatureModel,
        catBrand: {}  as CatBrandModel,
        catAssetType: {} as CatAssetTypeModel,
        valuationRequest: {
          id: '1', financingAmount: 3000000000
        } as ValuationRequestModel
      } as ValuationModel;

      expect(fixture.isValuationRequestNaturesExceptional()).toEqual(false);

    });
  });

  describe('Test isDiscountExceptional()', () => {
    it('should return true when discountRateValue is grater than catalog rate by more than 10%', () => {
      fixture.valuation = {
        id: '1',
        catActivitySector: {
          id: '1',
        } as CatActivitySectorModel,
        catNature: {
          id: '1',
          label: 'avion'
        } as CatNatureModel,
        catBrand: {
          id: '1',
        }  as CatBrandModel,
        catAssetType: {
          id: '1',
        } as CatAssetTypeModel,
        valuationRequest: {
          id: '1', financingAmount: 3000000000,
          paramEnergy: {
            id: '1'
          }
        } as ValuationRequestModel
      } as ValuationModel;



      const resp = [{
        id: '1',
        discountRateYear: 1,
        discountRateValue : 10
      }as ValuationDiscountModel,
      {
        id: '1',
        discountRateYear: 2,
        discountRateValue : 10
      }as ValuationDiscountModel,
      {
        id: '1',
        discountRateYear: 3,
        discountRateValue : 10
      }as ValuationDiscountModel] as ValuationDiscountModel[];
      spyOn(decoteServiceMock, 'getDiscountByValuationIdOrFromCat').and.returnValue(of(resp));

      fixture.discount  = [{
        id: '1',
        discountRateYear: 1,
        discountRateValue : 30
      }as ValuationDiscountModel,
      {
        id: '1',
        discountRateYear: 2,
        discountRateValue : 10
      }as ValuationDiscountModel,
      {
        id: '1',
        discountRateYear: 3,
        discountRateValue : 10
      }as ValuationDiscountModel] as ValuationDiscountModel[];


      expect(fixture.isDiscountExceptional()).toEqual(true);

    });

  });


  describe('Test isDiscountExceptional()', () => {
    it('should return  abs(catdiscount firstYearRate-discount firstYearRate) < 10', () => {
      fixture.firstYearRate = 80;
      fixture.discount = [{
        id: '101',
        discountRateYear: 1,
        discountRateValue: 80
      } as ValuationDiscountModel] as ValuationDiscountModel[];
      fixture.financialPlans = [];
      fixture.valuation = {
        id: '1',
        catActivitySector: {} as CatActivitySectorModel,
        catNature: {
          id: '1',
          label: 'abateuse'
        } as CatNatureModel,
        catBrand: {}  as CatBrandModel,
        catAssetType: {} as CatAssetTypeModel,
        valuationRequest: {
          id: '1', financingAmount: 100000
        } as ValuationRequestModel
      } as ValuationModel;
      fixture.isCurrentUserAdmin = false;
      fixture.checkIfAdminValidationNeeded();
      expect(fixture.isAdminValidationNeeded).toEqual(false);
    });
  });

  describe('Test:  askForValidation', () => {
    it('should call open modal', () => {
      fixture.askForValidation();
      expect(popupServiceMock.popupAskForValidation).toHaveBeenCalled();
      popupServiceMock.onConfirmDescValidator.next({});
    });
  });

  describe('Test:  shouldSearhForOtherExpertsForValidation', () => {
    it('should return true when PA<=2_000_000 and expert amount max< PA', () => {
      fixture.valuation.valuationRequest.financingAmount = 150_000;
      // expert max amount is 132_500
      expect(fixture.shouldSearhForOtherExpertsForValidation()).toEqual(true);
    });

    it('should return false when PA<=2_000_000 and expert amount max< PA', () => {
      fixture.valuation.valuationRequest.financingAmount = 50_000;
      // expert max amount is 132_500
      expect(fixture.shouldSearhForOtherExpertsForValidation()).toEqual(false);
    });
  });


  describe('Test:  showModifButton', () => {
    it('should return false when valuationRequestWorkflow status is REJECTED', () => {
      fixture.valuationRequestWorkflow = {
        id : '1',
        status: {
          value : 'REJECTED'
        } as ParamSettingModel
      } as ValuationRequestWorkflowModel;

      expect(fixture.showModifButton()).toEqual(false);
    });

    it('should return true when valuationRequestWorkflow is null', () => {
      fixture.valuationRequestWorkflow = null;

      expect(fixture.showModifButton()).toEqual(true);
    });

    it('should return false when valuationRequestWorkflow status is WAIT_VALID_MAN_EXP_QUAL and current user is expert', () => {
      fixture.valuationRequestWorkflow = {
        id : '1',
        status: {
          value : 'WAIT_VALID_MAN_EXP_QUAL'
        } as ParamSettingModel
      } as ValuationRequestWorkflowModel;
      spyOn(fixture, 'isExpertNotAllowed').and.returnValue(true);

      expect(fixture.showModifButton()).toEqual(false);
    });

    it('should return false when valuationRequestWorkflow status is WAIT_INFO_FIL_EXP and current user is expert', () => {
      fixture.valuationRequestWorkflow = {
        id : '1',
        status: {
          value : 'WAIT_INFO_FIL_EXP'
        } as ParamSettingModel
      } as ValuationRequestWorkflowModel;
      spyOn(fixture, 'isExpert').and.returnValue(true);
      spyOn(fixture, 'isAdmin').and.returnValue(false);

      expect(fixture.showModifButton()).toEqual(false);
    });

    it('should return true when valuationRequestWorkflow status is WAIT_INFO_FIL_EXP and current user is admin', () => {
      fixture.valuationRequestWorkflow = {
        id : '1',
        status: {
          value : 'WAIT_INFO_FIL_EXP'
        } as ParamSettingModel
      } as ValuationRequestWorkflowModel;
      spyOn(fixture, 'isExpert').and.returnValue(false);
      spyOn(fixture, 'isAdmin').and.returnValue(true);

      expect(fixture.showModifButton()).toEqual(true);
    });

  });


});



