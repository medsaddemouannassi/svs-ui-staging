import { ValuationModel } from '../../../../../shared/models/valuation.model';
import { ResultRequestInformationDescriptiveComponent } from "./result-request-information-descriptive.component";
import { LitigationModel } from '../../../../../shared/models/litigation.model';
import { CatActivitySectorModel } from '../../../../../shared/models/cat-activity-sector.model';
import { CatNatureModel } from '../../../../../shared/models/cat-nature.model';
import { CatBrandModel } from '../../../../../shared/models/cat-brand.model';
import { CatAssetTypeModel } from '../../../../../shared/models/cat-asset-type.model';


describe('ResultRequestInformationDescriptiveComponent', () => {
  let fixture: ResultRequestInformationDescriptiveComponent;


  beforeEach(() => {
    let valuation: ValuationModel = {
      id: '1',
      discountRateComment: 'comment',
      experId: '3',
      controllerId: '5',
      validatorId: '11',
      descNetwork:null,
      descName:null,
      isSubmitted : null,
      isDuplicated : null,
      catNature: {
            id:'1',
            label:'nature test',
            catActivitySector:null,
            comment:'comment',
            paramNap:null,
            isActive: true,
            isValid: false,
            isEnrg: false

      },
      paramValidationType: {
        id:'1',
        label:'test'
      },
      catBrand: {
        id:'1',
        label:'test brand',
        catNature:null,
        comment:'test comment',
        isActive:true,
        catRequest:null



      },
      catAssetType: {
        id:'1',
        label:'test type',
        catBrand:null,
        comment:'test comment',
        isActive:true,
        catRequest:null



      },
      valuationRequest: null,
      catActivitySector: {
        id:'1',
        label:'test activity sector',
        comment:'test comment',
        isActive:true

      },
      createdDate : new Date()
    }

    fixture = new ResultRequestInformationDescriptiveComponent(


    );
    fixture.valuation = valuation;
    fixture.ngOnInit();
  });

  describe('Test Component', () => {
    it('should Be Truthy', () => {
      expect(fixture).toBeTruthy();

    });
  });


  describe('Test setMatDescripValues', () => {
    it('should sel all qualif labels when litigation object is not empty', () => {
      fixture.litigation =  {
        catActivitySector : {
          label : 'BTP LEVAGE'
        } as CatActivitySectorModel,
        catNature : {
          label : 'BANCHES'
        } as CatNatureModel,
        catBrand: {
          label : 'AMADIO EC'
        } as CatBrandModel,
        catAssetType: {
          label : 'ETAIS'
        } as CatAssetTypeModel
      } as LitigationModel;
      fixture.ngOnInit();
      expect(fixture.sectorLabel).toEqual('BTP LEVAGE');
      expect(fixture.natureLabel).toEqual('BANCHES');
      expect(fixture.brandLabel).toEqual('AMADIO EC');
      expect(fixture.assetTypeLabel).toEqual('ETAIS');
    });
  });







});

