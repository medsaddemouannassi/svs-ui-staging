import { Component, OnInit, Input } from '@angular/core';
import { ValuationModel } from '../../../../../shared/models/valuation.model';
import { LitigationModel } from '../../../../../shared/models/litigation.model';

@Component({
  selector: 'app-result-request-information-descriptive',
  templateUrl: './result-request-information-descriptive.component.html',
  styleUrls: ['./result-request-information-descriptive.component.css']
})
export class ResultRequestInformationDescriptiveComponent implements OnInit {
  @Input() valuation:ValuationModel;
  @Input() litigation : LitigationModel; 

  sectorLabel: string;
  natureLabel: string;
  brandLabel: string;
  assetTypeLabel: string;

  constructor() {
     // This is intentional

  }

  ngOnInit(): void {
     // material description section
     this.setMatDescripValues();
  }

  setMatDescripValues(){
    if(this.litigation){
      this.sectorLabel = this.litigation.catActivitySector.label;
      this.natureLabel = this.litigation.catNature.label;
      this.brandLabel = this.litigation.catBrand.label;
      this.assetTypeLabel = this.litigation.catAssetType.label;
    }else if(this.valuation){
      this.sectorLabel = this.valuation.catActivitySector.label;
      this.natureLabel = this.valuation.catNature.label;
      this.brandLabel = this.valuation.catBrand.label;
      this.assetTypeLabel = this.valuation.catAssetType.label;
    }
  }

}
