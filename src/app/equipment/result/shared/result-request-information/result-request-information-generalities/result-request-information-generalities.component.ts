import { Component, OnInit, Input } from '@angular/core';
import { EntrepriseInfoService } from '../../../../../shared/services/entreprise-info.service';
import { ValuationModel } from '../../../../../shared/models/valuation.model';
import { EntrepriseInfoModel } from '../../../../../shared/models/entreprise-info.model';
import { MdmInformationService } from '../../../../../shared/services/mdm-informations.service';

@Component({
  selector: 'app-result-request-information-generalities',
  templateUrl: './result-request-information-generalities.component.html',
  styleUrls: ['./result-request-information-generalities.component.css']
})
export class ResultRequestInformationGeneralitiesComponent implements OnInit {

  clientInfo:EntrepriseInfoModel;
  @Input() dateSubmit:Date;
  @Input() valuation:ValuationModel;
  nom:string;
  idValuation: string;
  networkLabel: string;

  constructor(private entrepriseInfoService : EntrepriseInfoService, private mdmInformationService:MdmInformationService) { }

  ngOnInit(): void {

    this.clientInfo = this.entrepriseInfoService.entrepriseInfo;

    this.mdmInformationService.getMdmCaInformation(this.valuation.experId).subscribe((data)=>{
      this.nom= data.fullName;
      this.networkLabel = data.reseau;
      this.valuation.descNetwork = this.networkLabel;
      this.valuation.descName =this.nom;
    });

   }

}
