import { of } from 'rxjs';
import { ValuationModel } from 'src/app/shared/models/valuation.model';
import { EntrepriseInfoModel } from '../../../../../shared/models/entreprise-info.model';
import { MdmInformationsModel } from '../../../../../shared/models/mdm-informations.model';
import { ResultRequestInformationGeneralitiesComponent } from "./result-request-information-generalities.component";

describe('ResultRequestInformationGeneralitiesComponent', () => {
    let fixture: ResultRequestInformationGeneralitiesComponent;
    let entrepriseServiceMock :any;
    let mdmInformationServiceMock;

    let clientInfo :EntrepriseInfoModel= {
      id: '1',
      subsidiary: false,
      cp: '1',
      description: 'description',
      siren: '555',
      sirenType: 'xx',
      socialReason: 'client name',
      delegation: 'delegation',
      regionalDirection: 'regional direction',
      network: 'TCP'
    } as EntrepriseInfoModel;

    let mdmInformation :MdmInformationsModel ={
      'fullName':'John DOE',
      'reseau':'',
      'delegation':'',
       'mat':'',
       'idReseau':'',
       'idDelegation':'',
       'direction':'',
       'idDirection':'',
       'idFullname': ''

  } as MdmInformationsModel

    beforeEach(() => {

        entrepriseServiceMock = {
          entrepriseInfo : clientInfo
        }
        mdmInformationServiceMock = {
          getMdmCaInformation : jest.fn().mockReturnValue(of(mdmInformation)),
          mdmInformation: mdmInformation

        }

        fixture = new ResultRequestInformationGeneralitiesComponent(entrepriseServiceMock, mdmInformationServiceMock);
        fixture.valuation = {
          experId: 'M000025'
        }as ValuationModel;
        fixture.ngOnInit();

    });

    describe('Test Component', () => {
      it('should Be Truthy', () => {
        expect(fixture).toBeTruthy();
      });


    });

    describe('Test ngOnInit', ()=>{
      it('should initialise clientInfo', ()=>{
        expect(fixture.clientInfo).toEqual(clientInfo);
      });

    })


});

