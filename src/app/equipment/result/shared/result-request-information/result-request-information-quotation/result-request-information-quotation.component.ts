import { Component, Input, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ValuationModel } from '../../../../../shared/models/valuation.model';
import { ValuationDiscountModel } from '../../../../../shared/models/valuationDiscount.model';
import { DecoteService } from '../../../../../shared/services/decote.service';
import { SharedCommunicationDataService } from '../../../../../shared/services/shared-communication-data.service';
import { twoDigitDecimalRound } from '../../../../../shared/ValuationUtils';

@Component({
  selector: 'app-result-request-information-quotation',
  templateUrl: './result-request-information-quotation.component.html',
  styleUrls: ['./result-request-information-quotation.component.css']
})
export class ResultRequestInformationQuotationComponent implements OnInit {


  percentSymbol = "%";
  @Input() valuation:ValuationModel;

   discount:ValuationDiscountModel[];


  equipementState:string;
  sharedCommunicationSubscription: Subscription;
  valoCurrentStatus:string;

  financingAmount : string;


  constructor(private decoteService :DecoteService, private data: SharedCommunicationDataService) { }

  ngOnInit(): void {
    this.equipementState = this.valuation.valuationRequest.isANewAsset ? 'Neuf' : 'Occasion';
    this.decoteService.getDiscountByValuationId(this.valuation.id).subscribe(res=>this.discount = res);
    this.sharedCommunicationSubscription = this.data.currentValoStatus.subscribe(status => this.valoCurrentStatus = status)  
    
    //format financingAmount
    this.financingAmount = twoDigitDecimalRound(this.valuation.valuationRequest.financingAmount.toString());
  }


}
