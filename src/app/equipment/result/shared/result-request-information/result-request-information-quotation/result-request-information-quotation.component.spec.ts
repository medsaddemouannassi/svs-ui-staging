import { of } from 'rxjs';
import { CatNatureModel } from '../../../../../shared/models/cat-nature.model';
import { ValuationModel } from '../../../../../shared/models/valuation.model';
import { ValuationDiscountModel } from '../../../../../shared/models/valuationDiscount.model';
import { ResultRequestInformationQuotationComponent } from "./result-request-information-quotation.component";


describe('ResultRequestInformationQuotationComponent', () => {

    let fixture: ResultRequestInformationQuotationComponent;
    let discounts:ValuationDiscountModel[]= [{id:'',discountRateYear:1,discountRateValue:20,valuation:null,catDiscount:null},{id:'2',discountRateYear:2,discountRateValue:15,valuation:null,catDiscount:null}]
    let valuation: ValuationModel = {
      id: '1',
      isDuplicated : null,
      isSubmitted : null,
      discountRateComment: 'comment',
      experId: '3',
      controllerId: '5',
      validatorId: '11',
      catNature: {
            id:'1',
            label:'nature test',
            catActivitySector:null,
            comment:'comment',
            paramNap:null,
            isActive: true

      }as CatNatureModel,
      paramValidationType: {
        id:'1',
        label:'test'
      },
      catBrand: {
        id:'1',
        label:'test brand',
        catNature:null,
        comment:'comment',
        isActive:true,
        catRequest:null



      },
      catAssetType: {
        id:'1',
        label:'test type',
        catBrand:null,
        comment:'comment',
        isActive:true,
        catRequest:null



      },
      valuationRequest: {
        id:'1',
        externalSystemRef:'S',
        underwriterId:'1',
        isRetUnderwriter:true,
        customerPropoId:'1',
        bssResponsible:'1',
        requestComment:'comment',
        financingAmount:450,
        assetCreationDate: new Date(),
        assetUsVolume:50,
        collAssetEstimYear:2020,
        co2Emission:15,
        fiscPower:7,
        isAssetUseAbroad:false,
        isANewAsset: true,
        isLicencePlateAvaileble:true,
        paramEnergy:null,
        paramUsageUnit:null,
        unitaryQuantity:2,
        bssResponsibleFullName:'name',
        workflowBuisinessKey:'111',
        workflowInstanceId:'111',
        isSimulated: false
      },
      catActivitySector: {
        id:'1',
        label:'test activity sector',
        comment:'comment',
        isActive:true

      },
      createdDate : new Date(),
      descNetwork:null,
      descName:null
    }
    let decoteServiceMock:any;
    let dataMock: any;
    beforeEach(() => {

        decoteServiceMock = {
          getDiscountByValuationId: jest.fn().mockReturnValue(of(discounts))
        }

        dataMock = {
          currentValoStatus :  {
           subscribe : jest.fn().mockReturnValue(of(true))
          }
        }

        fixture = new ResultRequestInformationQuotationComponent(decoteServiceMock,
        dataMock);
        fixture.valuation = valuation;


    });

    describe('Test Component', () => {
      it('should Be Truthy', () => {
        expect(fixture).toBeTruthy();
      });
    });

    describe('Test ngOnInit', () => {
      it('should decote to be full', () => {
        fixture.ngOnInit();
        expect(fixture.discount).toEqual(discounts);
      });

      it('should set equipement state to new ', () => {
        fixture.ngOnInit();
        expect(fixture.equipementState).toEqual('Neuf');



      });
      it('should set equipement state to old ', () => {


        fixture.valuation.valuationRequest.isANewAsset = false;
        fixture.ngOnInit();

        expect(fixture.equipementState).toEqual('Occasion');

      });
    });

});

