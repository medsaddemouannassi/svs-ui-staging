import { Component, OnInit, Input } from '@angular/core';
import { ValuationModel } from '../../../../shared/models/valuation.model';
import { LitigationModel } from '../../../../shared/models/litigation.model';

@Component({
  selector: 'app-result-request-information',
  templateUrl: './result-request-information.component.html',
  styleUrls: ['./result-request-information.component.css']
})
export class ResultRequestInformationComponent implements OnInit {

@Input() valuation: ValuationModel;
@Input() dateSubmit: Date;
@Input() litigation: LitigationModel;

  constructor() {
    //constructor
  }

  ngOnInit(): void {
    //ngOnInit

  }

}
