import { Component, Input, OnInit } from '@angular/core';
import { MdmInformationService } from '../../../../shared/services/mdm-informations.service';
import { ValuationModel } from 'src/app/shared/models/valuation.model';

@Component({
  selector: 'app-result-history',
  templateUrl: './result-history.component.html',
  styleUrls: ['./result-history.component.css']
})
export class ResultHistoryComponent implements OnInit {
  historyList: any[];
  public selectedHistoryItem;
  public nomExpert :string;
  @Input() valuation:ValuationModel;

  constructor(private mdmInformationService:MdmInformationService) {
    //constructor
   }

  ngOnInit(): void {
    // get nom Expert
    this.mdmInformationService.getMdmCaInformation(this.valuation.experId).subscribe((data)=>{
   this.nomExpert= data.fullName;
    },(error)=>{console.error("error loading info from MDM "+ error)},()=>{
      this.historyList = [{id: '1', label: '10/12/2020 - '+ this.nomExpert}];
      this.selectedHistoryItem = this.historyList[0];
    });

  }

}
