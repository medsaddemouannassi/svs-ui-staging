import { of } from 'rxjs';
import { ValuationModel } from 'src/app/shared/models/valuation.model';
import { MdmInformationsModel } from '../../../../shared/models/mdm-informations.model';
import { ResultHistoryComponent } from "./result-history.component";

describe('ResultHistoryComponent', () => {
 let fixture: ResultHistoryComponent;
 let mdmInformationServiceMock:any;
 let mdmInformation :MdmInformationsModel ={
    'fullName':'John DOE',
    'reseau':'',
    'delegation':'',
     'mat':'',
     'idReseau':'',
     'idDelegation':'',
     'idDirection':'',
     'direction':'',
     'idFullname': ''

} as MdmInformationsModel
 beforeEach(() => {

   mdmInformationServiceMock = {
    getMdmCaInformation : jest.fn().mockReturnValue(of(mdmInformation)),

  }

  fixture = new ResultHistoryComponent(mdmInformationServiceMock);
  
  fixture.valuation = {
    experId: 'M000025'
  }as ValuationModel;

  fixture.ngOnInit();
 });

 describe('Test Component', () => {
  it('should Be Truthy', () => {
  expect(fixture).toBeTruthy();
  });
 });


 describe('Test ngOnInit', () => {
  it('should get historyList', () => {

  expect(fixture.historyList).toEqual([{id: '1', label: '10/12/2020 - John DOE'}]);
  });
  it('should get selectedHistoryItem', () => {
  expect(fixture.selectedHistoryItem).toEqual({id: '1', label: '10/12/2020 - John DOE'});
  });
 });







});

