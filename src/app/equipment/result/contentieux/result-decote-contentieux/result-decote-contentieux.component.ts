import { Component, OnInit, Input } from '@angular/core';
import { LitigationModel } from '../../../../shared/models/litigation.model';
import { twoDigitDecimalRound } from '../../../../shared/ValuationUtils';


@Component({
  selector: 'app-result-decote-contentieux',
  templateUrl: './result-decote-contentieux.component.html',
  styleUrls: ['./result-decote-contentieux.component.css']
})
export class ResultDecoteContentieuxComponent implements OnInit {

  @Input() litigation: LitigationModel;
  percentSymbol = "%";
  litigationValueString;
  constructor() {

  }






  ngOnInit(): void {

  }

  convertNumberToTwoDigitDecimalRound(value) {
    return twoDigitDecimalRound(value.toString());

  }
}
