import { Component, OnInit } from '@angular/core';
import { LitigationModel } from '../../../shared/models/litigation.model';
import { ValuationModel } from '../../../shared/models/valuation.model';
import { LitigationService } from '../../../shared/services/litigation-service';
import { ActivatedRoute, Router } from '@angular/router';
import { PopupService } from 'src/app/shared/services/popup.service';
import { ReportServiceService } from '../../../shared/services/report-service.service';
import { ReportElementLitigationModel } from '../../../shared/models/reportElementLitigationModel.model';
import { EntrepriseInfoService } from '../../../shared/services/entreprise-info.service';
import { MdmInformationService } from '../../../shared/services/mdm-informations.service';
import { ConfirmationModalComponent } from '../../../shared/confirmation-modal/confirmation-modal.component';
import { EntrepriseInfoModel } from '../../../shared/models/entreprise-info.model';
import { ValuationRequestDocsService } from '../../../shared/services/valuation-request-docs.service';
import { SharedCommunicationDataService } from '../../../shared/services/shared-communication-data.service';
import { DocumentConstant } from 'src/app/shared/document-constant';

@Component({
  selector: 'app-result-contentieux',
  templateUrl: './result-contentieux.component.html',
  styleUrls: ['./result-contentieux.component.css']
})
export class ResultContentieuxComponent implements OnInit {
  litigation: LitigationModel;
  valuation: ValuationModel;
  descName: string;
  disabled = true;
  idLitigation;
  constructor(private litigationService: LitigationService, private route: ActivatedRoute, private router: Router,
    private popupService: PopupService, private reportService: ReportServiceService,
    private entrepriseInfoService: EntrepriseInfoService, private mdmInformationService: MdmInformationService
    , private valuationRequestDocsService: ValuationRequestDocsService,private dataSharing: SharedCommunicationDataService,
  ) { }
  ngOnInit(): void {
    this.dataSharing.changeStatus("true");
    this.idLitigation = this.route.snapshot.paramMap.get('idLitigation');
    this.litigationService.findLitigationById(this.idLitigation).subscribe(data => {
      this.litigation = data;
      this.valuation = data.valuation
    }, err => { 
      //this is intentional
    }, () => {
      this.loadEnterpriseInfo();
      this.descName= this.valuation.descName;

    }
    )


  }

  returnToReceptionContentieux() {
    this.router.navigate(['contentieux', this.litigation.valuation.id])

  }
  loadEnterpriseInfo() {
    if (!this.entrepriseInfoService.entrepriseInfo) {
      let ret = null;
      let bcp = null;
      if (this.valuation.valuationRequest.isRetUnderwriter) {
        ret = this.valuation.valuationRequest.underwriterId;
      } else {
        bcp = this.valuation.valuationRequest.underwriterId;
      }

      let loadedInfo = {} as EntrepriseInfoModel;
      this.entrepriseInfoService.entrepriseInfo = {} as EntrepriseInfoModel;
      this.entrepriseInfoService.loadEntrepriseInfo(ret, bcp).subscribe(
        (data: EntrepriseInfoModel) => {
          loadedInfo = data;
        },
        err => {
          console.error('error in loading enterpriseInfo result page ', err)
        },
        () => {
          this.entrepriseInfoService.entrepriseInfo = loadedInfo;

        }
      );
    }
  }
  public blobToFile = (theBlob: Blob): File => {
    var b: any = theBlob;

    b.lastModifiedDate = new Date();
    b.name = "Resultat_valorisation_en_contentieux_" + this.valuation.valuationRequest.id + ".pdf";
    //Cast to a File() type
    return new File([<File>theBlob], b.name);
  }
  saveResult() {

    this.popupService.popupInfo('Êtes-vous sûr(e) de confirmer votre cotation en contentieux ?', 'Confirmer', null, 'Annuler', ConfirmationModalComponent);
    let subscription = this.popupService.onConfirm.subscribe((t) => {
      if (t) {
        let reportElementLitigation = {} as ReportElementLitigationModel;
        reportElementLitigation.entrepriseInfo = this.entrepriseInfoService.entrepriseInfo;
        reportElementLitigation.valuation = this.litigation.valuation;
        reportElementLitigation.valuationDate = new Date();
        reportElementLitigation.litigation = this.litigation;
        reportElementLitigation.descName = this.descName;
        this.reportService.generateLitigationReport(reportElementLitigation).subscribe((res) => {

          this.popupService.popupPDF('Confirmer', 'Annuler', null, window.URL.createObjectURL(res), true);

          let subscriptionPDFConfirmation = this.popupService.onConfirm.subscribe((response) => {

            if (response) {
              this.valuationRequestDocsService.uploadValuationResult(this.blobToFile(res), this.valuation.valuationRequest, this.entrepriseInfoService.entrepriseInfo, DocumentConstant.PHASE_LITIGATION).subscribe(() => {
                //this is intentional
              },
                err => {
                  console.error('error at uploadValuationResult', err);
                }, () => {


                  //WF For Litigation


                });
            }
            subscriptionPDFConfirmation.unsubscribe();

          });
        })

      }

      subscription.unsubscribe();




    });

  }


}
