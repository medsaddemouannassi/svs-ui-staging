import { of, Subject } from 'rxjs';
import { ResultContentieuxComponent } from './result-contentieux.component';
import { LitigationModel } from '../../../shared/models/litigation.model';
import { CatAssetTypeModel } from '../../../shared/models/cat-asset-type.model';
import { CatBrandModel } from '../../../shared/models/cat-brand.model';
import { CatNatureModel } from '../../../shared/models/cat-nature.model';
import { CatActivitySectorModel } from '../../../shared/models/cat-activity-sector.model';
import { LitigationDetail } from '../../../shared/models/litigation-detail.model';
import { ValuationModel } from '../../../shared/models/valuation.model';
import { ValuationRequestModel } from '../../../shared/models/valuation-request.model';
import { ReportElementLitigationModel } from '../../../shared/models/reportElementLitigationModel.model';
import { EntrepriseInfoModel } from '../../../shared/models/entreprise-info.model';
import { MdmInformationsModel } from '../../../shared/models/mdm-informations.model';


describe('ResultContentieuxComponent', () => {
  let fixture: ResultContentieuxComponent;
  let litigationServiceMock: any;
  let popupServiceMock: any;
  let reportServiceMock: any;
  let entrepriseInfoServiceMock: any;
  let mdmInformationServiceMock: any;
  let valuationRequestDocsServiceMock: any;
  let dataSharingMock: any;


  let routeMock: any;
  let routerMock: any
  beforeEach(() => {

    let litigation = new Date();

    let dateSubmit = new Date();
    valuationRequestDocsServiceMock = {
      uploadValuationResult: jest.fn().mockReturnValue(of(true)),
    }

    let valuation: ValuationModel = {
      id: '1',
      discountRateComment: 'comment',
      experId: '3',
      controllerId: '5',
      validatorId: '11',
      descNetwork: null,
      descName: null,
      isSubmitted: null,
      isDuplicated: null,
      catNature: {
        id: '1',
        label: 'nature test',
        catActivitySector: null,
        comment: 'comment',
        paramNap: null,
        isActive: null

      } as CatNatureModel,
      paramValidationType: {
        id: '1',
        label: 'test'
      },
      catBrand: {
        id: '1',
        label: 'test brand',
        catNature: null,
        comment: '',
        isActive: true,
        catRequest: null

      },
      catAssetType: {
        id: '1',
        label: 'test type',
        catBrand: null,
        comment: 'comment',
        isActive: true,
        catRequest: null


      },
      valuationRequest: {
        id: '1',
        externalSystemRef: 'S',
        underwriterId: '1',
        isRetUnderwriter: true,
        customerPropoId: '1',
        bssResponsible: '1',
        requestComment: 'comment',
        financingAmount: 450,
        assetCreationDate: new Date(),
        assetUsVolume: 50,
        collAssetEstimYear: 2020,
        co2Emission: 15,
        fiscPower: 7,
        isAssetUseAbroad: false,
        isANewAsset: true,
        isLicencePlateAvaileble: true,
        paramEnergy: null,
        paramUsageUnit: null,
        unitaryQuantity: 2,
        bssResponsibleFullName: 'name'
      } as ValuationRequestModel,
      catActivitySector: {
        id: '1',
        label: 'test activity sector',
        comment: 'test comment',
        isActive: true


      },
      createdDate: new Date()
    }
    dataSharingMock = {
      changeStatus: jest.fn().mockReturnValue(of(true)),
    }

    let litigationMock: LitigationModel = {
      "id": "1",
      "catAssetType": {
        "id": "1"
      } as CatAssetTypeModel,
      "catBrand": {
        "id": "1"
      } as CatBrandModel,
      "catNature": {
        "id": "1"
      } as CatNatureModel,

      "catActivitySector": {
        "id": "1"
      } as CatActivitySectorModel,
      "valuation": valuation,
      "litigationDetails": [
        {
          "id": "1",
          "discountRateYear": 1,
          "discountRateValue": 1,
          "isChecked": true
        } as LitigationDetail,
        {
          "id": "2",
          "discountRateYear": 2,
          "discountRateValue": 2,
          "isChecked": false
        } as LitigationDetail,
        {
          "id": "3",
          "discountRateYear": 3,
          "discountRateValue": 3,
          "isChecked": false
        } as LitigationDetail,
        {
          "id": "4",
          "discountRateYear": 4,
          "discountRateValue": 4,
          "isChecked": false
        } as LitigationDetail,
        {
          "id": "5",
          "discountRateYear": 5,
          "discountRateValue": 5,
          "isChecked": false
        } as LitigationDetail,
        {
          "id": "5",
          "discountRateYear": 5,
          "discountRateValue": 5,
          "isChecked": false
        } as LitigationDetail,
        {
          "id": "6",
          "discountRateYear": 6,
          "discountRateValue": 6,
          "isChecked": false
        } as LitigationDetail,
        {
          "id": "7",
          "discountRateYear": 7,
          "discountRateValue": 7,
          "isChecked": false
        } as LitigationDetail,
        {
          "id": "8",
          "discountRateYear": 8,
          "discountRateValue": 8,
          "isChecked": false
        } as LitigationDetail,
        {
          "id": "9",
          "discountRateYear": 9,
          "discountRateValue": 9,
          "isChecked": false
        } as LitigationDetail,
        {
          "id": "10",
          "discountRateYear": 10,
          "discountRateValue": 10,
          "isChecked": false
        } as LitigationDetail,
        {
          "id": "11",
          "discountRateYear": 11,
          "discountRateValue": 11,
          "isChecked": false
        } as LitigationDetail
      ]

    } as LitigationModel

    let reportElementLitigation = {

      "valuation": valuation,
      "valuationDate": new Date(),
      "descName": "test",
      "litigation": litigationMock,

    } as ReportElementLitigationModel;
    popupServiceMock = {
      popupInfo: jest.fn().mockReturnValue(of(true)),
      popupPDF: jest.fn().mockReturnValue(true),

      onConfirm: of(true),

    };
    routeMock = {
      snapshot: {
        paramMap: {
          get: (key: string) => {
            switch (key) {
              case 'idLitigation': return '1';
            }
          },
        },
      }

    };
    let clientInfo: EntrepriseInfoModel = {
      id: '1',
      subsidiary: false,
      cp: '1',
      description: 'description',
      siren: '555',
      sirenType: 'xx',
      socialReason: 'client name',
      delegation: 'delegation',
      regionalDirection: 'regional direction',
      network: 'TCP'
    } as EntrepriseInfoModel;
    let mdmInformation: MdmInformationsModel = {
      'fullName': 'John DOE',
      'reseau': '',
      'delegation': '',
      'mat': '',
      'idReseau': '',
      'idDelegation': '',
      'direction': '',
      'idDirection': '',
      'idFullname': ''

    } as MdmInformationsModel

    entrepriseInfoServiceMock = {
      entrepriseInfo: null,
      loadEntrepriseInfo: jest.fn().mockReturnValue(of({} as EntrepriseInfoModel)),
    }
    mdmInformationServiceMock = {
      getMdmCaInformation: jest.fn().mockReturnValue(of(mdmInformation)),
      mdmInformation: mdmInformation

    }
    routerMock = {
      navigate: jest.fn().mockReturnValue(new Promise(() => {
        //this is intentional
      })),
    }
    reportServiceMock = {
      generateLitigationReport: jest.fn().mockReturnValue(of(new Blob(["testing"], { type: "application/pdf" })))
    }
    litigationServiceMock = {

      findLitigationById: jest.fn().mockReturnValue(of(
        {
          "id": "1",
          "catAssetType": {
            "id": "1"
          } as CatAssetTypeModel,
          "catBrand": {
            "id": "1"
          } as CatBrandModel,
          "catNature": {
            "id": "1"
          } as CatNatureModel,

          "catActivitySector": {
            "id": "1"
          } as CatActivitySectorModel,
          "valuation": {
            "id": "1"
          } as ValuationModel,
          "litigationDetails": [
            {
              "id": "1",
              "discountRateYear": 1,
              "discountRateValue": 1,
              "isChecked": true
            } as LitigationDetail,
            {
              "id": "2",
              "discountRateYear": 2,
              "discountRateValue": 2,
              "isChecked": false
            } as LitigationDetail,
            {
              "id": "3",
              "discountRateYear": 3,
              "discountRateValue": 3,
              "isChecked": false
            } as LitigationDetail,
            {
              "id": "4",
              "discountRateYear": 4,
              "discountRateValue": 4,
              "isChecked": false
            } as LitigationDetail,
            {
              "id": "5",
              "discountRateYear": 5,
              "discountRateValue": 5,
              "isChecked": false
            } as LitigationDetail,
            {
              "id": "5",
              "discountRateYear": 5,
              "discountRateValue": 5,
              "isChecked": false
            } as LitigationDetail,
            {
              "id": "6",
              "discountRateYear": 6,
              "discountRateValue": 6,
              "isChecked": false
            } as LitigationDetail,
            {
              "id": "7",
              "discountRateYear": 7,
              "discountRateValue": 7,
              "isChecked": false
            } as LitigationDetail,
            {
              "id": "8",
              "discountRateYear": 8,
              "discountRateValue": 8,
              "isChecked": false
            } as LitigationDetail,
            {
              "id": "9",
              "discountRateYear": 9,
              "discountRateValue": 9,
              "isChecked": false
            } as LitigationDetail,
            {
              "id": "10",
              "discountRateYear": 10,
              "discountRateValue": 10,
              "isChecked": false
            } as LitigationDetail,
            {
              "id": "11",
              "discountRateYear": 11,
              "discountRateValue": 11,
              "isChecked": false
            } as LitigationDetail
          ]

        } as LitigationModel
      )),

    };

    fixture = new ResultContentieuxComponent(litigationServiceMock, routeMock, routerMock,
      popupServiceMock, reportServiceMock, entrepriseInfoServiceMock,
      mdmInformationServiceMock, valuationRequestDocsServiceMock,dataSharingMock);

    fixture.ngOnInit();
  });

  describe('Test Component', () => {
    it('should Be Truthy', () => {
      expect(fixture).toBeTruthy();
    });
  });

  describe('Test returnToReceptionContentieux', () => {
    it('should return to reception', () => {

      fixture.returnToReceptionContentieux();
      expect(routerMock.navigate).toHaveBeenCalled();
    });
  });


  describe('saveResult', () => {
    it('should return to reception', () => {
      global.URL.createObjectURL = jest.fn();
      fixture.saveResult();
      popupServiceMock.onConfirm = new Subject();
      popupServiceMock.onConfirm.next(true)

     
    });
  });
  describe('Test blobToFile ()', () => {
    it('should return file', () => {
      fixture.valuation = {
        id: '1',
        valuationRequest: {
          id: '1'
        } as ValuationRequestModel
      } as ValuationModel;
      expect(fixture.blobToFile({} as Blob)).toBeDefined();
    });
  });
  describe('Test loadEnterpriseInfo()', () => {
    it('should load loadEntrepriseInfo with RET variable when entrepriseInfo is null', () => {
      fixture.valuation = {
        id: '1',
        valuationRequest: {
          id: '1',
          isRetUnderwriter: true,
          underwriterId: '1'
        } as ValuationRequestModel
      } as ValuationModel;

      fixture.loadEnterpriseInfo();
      expect(entrepriseInfoServiceMock.loadEntrepriseInfo).toHaveBeenCalled();
    });
    it('should load loadEntrepriseInfo with BCP variable when entrepriseInfo is null', () => {

      fixture.valuation = {
        id: '1',
        valuationRequest: {
          id: '1',
          isRetUnderwriter: false,
          underwriterId: '1'
        } as ValuationRequestModel
      } as ValuationModel;

      fixture.loadEnterpriseInfo();
      expect(entrepriseInfoServiceMock.loadEntrepriseInfo).toHaveBeenCalled();
    });

  });
});

