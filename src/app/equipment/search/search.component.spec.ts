import { TestBed } from '@angular/core/testing';
import { of, throwError } from 'rxjs';
import { PageValuationSearchCatalogue } from '../../shared/models/page-valuation-search-catalogue.model';
import { PageValuationSearch } from '../../shared/models/page-valuation-search.model';
import { ValuationSearchCatalogueModel } from '../../shared/models/valuation-search-catalogue.model';
import { ValuationSearchModel } from '../../shared/models/valuation-search.model';
import { ValuationSharedService } from '../../shared/services/valuation-shared-service';
import { SearchComponent } from './search.component';
import { SearchCatalogueFilterModel } from '../../shared/models/catalogue-search-filter.model';
import { SearchCatalogueResultModel } from '../../shared/models/catalogue-search-data.model';


describe('SearchComponent', () => {

  let fixture: SearchComponent;
  let popupServiceMock: any;
  let valuationSearchServiceMock: any;
  let ChangeDetectorRefMock: any;
  let SearchCatalogueServiceMock: any;
  let dataSharingMock: any;

  let valuationSharedServiceMock: any;

  let resultMaterial: PageValuationSearch = {} as PageValuationSearch;
  let resultCatalogue: PageValuationSearchCatalogue = {} as PageValuationSearchCatalogue;

  resultMaterial.totalElements = 2;
  resultMaterial.content = [
    { valuationRequestId: '15', elementType: 'Demande' } as ValuationSearchModel,
    { valuationRequestId: '15', elementType: 'Valorisation' } as ValuationSearchModel,

  ]

  resultCatalogue.totalElements = 2;
  resultCatalogue.content = [

    { data: { name: 'nat' }, children: [] } as SearchCatalogueResultModel

  ]

  let authServiceMock;



  beforeEach(() => {




    popupServiceMock = {
      popupInfo: jest.fn().mockReturnValue(of(true)),
    };
    valuationSearchServiceMock = {
      findValuationSearch: jest.fn().mockReturnValue(of(resultMaterial))
    }

    ChangeDetectorRefMock = {
      detectChanges: jest.fn().mockReturnValue(true)
    }
    SearchCatalogueServiceMock = {
      findSearchCatalogue: jest.fn().mockReturnValue(of(resultCatalogue))
    }
    dataSharingMock = {
      changeStatus: jest.fn().mockImplementation(() => { }),
    }


    valuationSharedServiceMock = TestBed.inject(ValuationSharedService);



    fixture = new SearchComponent(popupServiceMock, valuationSearchServiceMock, SearchCatalogueServiceMock, ChangeDetectorRefMock, authServiceMock, valuationSharedServiceMock
      , dataSharingMock)



  });

  describe('Test: findValuationMaterialResult', () => {

    it('should get result', () => {

      fixture.findValuationMaterialResult({} as ValuationSearchModel, '', 20, 1);
      expect(valuationSearchServiceMock.findValuationSearch).toHaveBeenCalled();
      valuationSearchServiceMock.findValuationSearch().subscribe(res => {
        expect(fixture.searchResultMaterial).toEqual({ totalRecords: 2, resultList: resultMaterial.content });

      })
    });

    it('should not get result', () => {

      valuationSearchServiceMock = {
        findValuationSearch: jest.fn().mockReturnValue(of(null))
      }
      fixture = new SearchComponent(popupServiceMock, valuationSearchServiceMock, SearchCatalogueServiceMock, ChangeDetectorRefMock, authServiceMock, valuationSharedServiceMock
        , dataSharingMock)


      fixture.findValuationMaterialResult({} as ValuationSearchModel, '', 20, 1);
      expect(valuationSearchServiceMock.findValuationSearch).toHaveBeenCalled();
      valuationSearchServiceMock.findValuationSearch().subscribe(res => {
        expect(popupServiceMock.popupInfo).toHaveBeenCalled();


        expect(fixture.searchResultMaterial).toEqual({ totalRecords: 1, resultList: [{} as ValuationSearchModel] });

      })
    });
    it('should throw error', () => {

      valuationSearchServiceMock = {
        findValuationSearch: jest.fn().mockImplementation(() => {
          return throwError(new Error('my error message'));
        })
      }
      fixture = new SearchComponent(popupServiceMock, valuationSearchServiceMock, SearchCatalogueServiceMock, ChangeDetectorRefMock, authServiceMock, valuationSharedServiceMock
        , dataSharingMock)


      fixture.findValuationMaterialResult({} as ValuationSearchModel, '', 20, 1);
      expect(valuationSearchServiceMock.findValuationSearch).toHaveBeenCalled();
      valuationSearchServiceMock.findValuationSearch().subscribe(res => {


        expect(fixture.searchResultMaterial).toEqual({ totalRecords: 0, resultList: [] });

      })
    });
  });
  describe('Test: findValuationCatalogueResult', () => {

    it('should get result', () => {

      fixture.findValuationCatalogueResult({} as SearchCatalogueFilterModel, '', 20, 1);
      expect(SearchCatalogueServiceMock.findSearchCatalogue).toHaveBeenCalled();
      SearchCatalogueServiceMock.findSearchCatalogue().subscribe(res => {
        expect(fixture.searchResultCatalogue).toEqual({ totalRecords: 1, resultList: resultCatalogue.content });

      })
    });

    it('should not get result', () => {

      SearchCatalogueServiceMock = {
        findSearchCatalogue: jest.fn().mockReturnValue(of(null))
      }
      fixture = new SearchComponent(popupServiceMock, valuationSearchServiceMock, SearchCatalogueServiceMock, ChangeDetectorRefMock, authServiceMock, valuationSharedServiceMock
        , dataSharingMock)

      fixture.findValuationCatalogueResult({} as SearchCatalogueFilterModel, '', 20, 1);
      expect(SearchCatalogueServiceMock.findSearchCatalogue).toHaveBeenCalled();
      SearchCatalogueServiceMock.findSearchCatalogue().subscribe(res => {
        expect(popupServiceMock.popupInfo).toHaveBeenCalled();


        expect(fixture.searchResultCatalogue).toEqual({ totalRecords: 1, resultList: [{} as ValuationSearchCatalogueModel] });

      })
    });

    it('should throw error', () => {

      SearchCatalogueServiceMock = {
        findSearchCatalogue: jest.fn().mockImplementation(() => {
          return throwError(new Error('my error message'));
        })
      }
      fixture = new SearchComponent(popupServiceMock, valuationSearchServiceMock, SearchCatalogueServiceMock, ChangeDetectorRefMock, authServiceMock, valuationSharedServiceMock
        , dataSharingMock)

      fixture.findValuationCatalogueResult({} as SearchCatalogueFilterModel, '', 20, 1);
      expect(SearchCatalogueServiceMock.findSearchCatalogue).toHaveBeenCalled();
      SearchCatalogueServiceMock.findSearchCatalogue().subscribe(res => {


        expect(fixture.searchResultCatalogue).toEqual({ totalRecords: 0, resultList: [] });

      })
    });


  })

  describe('Test: loadResultsByMaterial', () => {
    it('should load result', () => {
      fixture.submited = true;
      fixture.loadResultsByMaterial({ criteria: {} as ValuationSearchModel, sort: '', elementByPage: 20, indexPage: 1 })
      expect(valuationSearchServiceMock.findValuationSearch).toHaveBeenCalled();

    });

    it('should not load result', () => {
      fixture.submited = false;
      fixture.loadResultsByMaterial({ criteria: {} as ValuationSearchModel, sort: '', elementByPage: 20, indexPage: 1 })
      expect(valuationSearchServiceMock.findValuationSearch).toHaveBeenCalledTimes(0);

    });

  })

  describe('Test: loadResultsByCatalogue', () => {
    it('should load result', () => {
      fixture.submited = true;
      fixture.loadResultsByCatalogue({ criteria: {} as ValuationSearchCatalogueModel, sort: '', elementByPage: 20, indexPage: 1 })
      expect(SearchCatalogueServiceMock.findSearchCatalogue).toHaveBeenCalled();

    });

    it('should not load result', () => {
      fixture.submited = false;
      fixture.loadResultsByMaterial({ criteria: {} as ValuationSearchCatalogueModel, sort: '', elementByPage: 20, indexPage: 1 })
      expect(SearchCatalogueServiceMock.findSearchCatalogue).toHaveBeenCalledTimes(0);

    });

  })

  describe('Test: searchValuation', () => {
    it('should set criteria', () => {

      fixture.searchValuation({ criteria: { energyId: '2' } as ValuationSearchModel })
      expect(fixture.criteriaMaterial).toEqual({ energyId: '2' } as ValuationSearchModel)
      expect(fixture.submited).toEqual(true)


    });



  })

  describe('Test: resetValuation', () => {
    it('should remove contain of  list', () => {

      fixture.resetValuation();
      expect(fixture.searchResultMaterial.resultList).toEqual([])
      expect(fixture.searchResultCatalogue.resultList).toEqual([])


    });



  })

})
