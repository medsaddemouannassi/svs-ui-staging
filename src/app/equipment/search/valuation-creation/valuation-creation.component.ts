import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { RefUnderwriterModel } from '../../../shared/models/ref-underwriter.model';
import { RefUnderwriterService } from '../../../shared/services/ref-underwriter.service';
import { EntrepriseInfoModel } from '../../../shared/models/entreprise-info.model';
import { EntrepriseInfoService } from '../../../shared/services/entreprise-info.service';
import { Router } from '@angular/router';
import { PagesPaths } from '../../../shared/pages-constant';
import { SettingConstant } from '../../../shared/setting-constant';


@Component({
  selector: 'app-valuation-creation',
  templateUrl: './valuation-creation.component.html',
  styleUrls: ['./valuation-creation.component.css']
})
export class ValuationCreationComponent implements OnInit {

  @ViewChild('auto') auto;

  focus(e): void {
    e.stopPropagation();
    this.auto.focus();
  }

  legalNamesCustomerOptions: RefUnderwriterModel[] = [];
  sirenOptions: EntrepriseInfoModel[];
  keyword = 'legalName';
  keywordSiren = 'siren';
  isCreateDisable = true;
  valCreationForm;
  isLoading = false;
  isCustomerNamedisabled = false;
  isSirenDisabled = false;
  initialSirenNumber: string;
  selectedSirenObject: any;
  selectedCustomerObject: any;
  isArrowUp = true;
  isSirenResultNotFound = false;
  isCustomerResultNotFound = false;

  customFilter = function (sirenOptions: any[], query: string): any[] {

    return sirenOptions;

  };

  constructor(private fb: FormBuilder, private refUnderwriterService: RefUnderwriterService, private entrepriseInfoService: EntrepriseInfoService,
    private router: Router) { }

  ngOnInit(): void {

    this.valCreationForm = this.fb.group({
      customerId: [null],
      siren: [null]
    })
  }
  
  disableCustomer() {
    this.isCustomerNamedisabled = true;
  }

  disableSiren() {
    this.isSirenDisabled = true;
  }

  enableSiren() {
    this.isSirenDisabled = false;
    this.legalNamesCustomerOptions = [];
    this.isSirenResultNotFound = false;
  }
  selectEventCustomer(event) {
    if (event) {
      this.enableSiren();
    }
  }
  loadCustomer(query) {
    this.disableSiren();
    this.isSirenResultNotFound = false;
    this.legalNamesCustomerOptions = [];
    if (query && query.length > 0) {
      this.refUnderwriterService.loadLegalNamesCustomersByQuery(query).subscribe(
        res => {
          if (res && res.length > 0) {
            if(res){
              res.forEach(element=>{
                if(element.nsiren){
                  element.legalName+=' - '+element.nsiren
                }
              })
            }
            this.legalNamesCustomerOptions=res ;
          } else {
            this.isCustomerResultNotFound = true;
          }

        }
      )
    }
  }

  loadSiren(query) {
    if (query && query.length > 0) {
      this.isLoading = true;
      let list;
      this.valCreationForm.patchValue({
        customerId: null
      })
      this.entrepriseInfoService.loadEntrepriseInfoBySiren(SettingConstant.RET_58, query).subscribe(
        res => {

          list = res;

        }
        , err => {
          //this is intentional
        }, () => {
          this.isLoading = false;
          if (list && list.length > 0) {
            this.sirenOptions = list;
            this.auto.focus();
          } else {
            this.isSirenResultNotFound = true;
          }

        })
    }
  }

  selectCustomer(a) {
    this.isCustomerResultNotFound = false;
    this.valCreationForm.patchValue({
      siren: a.nsiren
    })
    this.selectedCustomerObject = a;
    this.isCreateDisable = false;
    this.selectEventCustomer(a);
    this.isSirenDisabled = true;
  }

  selectSiren(a) {
    this.isSirenResultNotFound = false;
    this.valCreationForm.patchValue({
      customerId: a.socialReason
    })
    this.selectedSirenObject = a;
    this.isCreateDisable = false;
  }

  patchCustomer(event) {
    this.initialSirenNumber = event.value;
  }

  showSection() {
    this.isArrowUp = !this.isArrowUp;
  }

  reset() {
    this.valCreationForm.reset();
    this.isCustomerResultNotFound = false;
    this.isSirenResultNotFound = false;
    this.isLoading = false;
    this.legalNamesCustomerOptions = [];
    this.sirenOptions = [];
    this.isCustomerNamedisabled = false;
    this.isSirenDisabled = false;
    this.isCreateDisable = true;
  }

  createValorisation() {
    if (this.selectedSirenObject && this.selectedSirenObject.idRet) {
      this.router.navigate([PagesPaths.SIMULATION, this.selectedSirenObject.idRet, this.selectedSirenObject.isFromRet])
    } else {
      this.router.navigate([PagesPaths.SIMULATION, this.selectedCustomerObject.id, this.selectedCustomerObject.isRetUnderwriter]);
    }

  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

}
