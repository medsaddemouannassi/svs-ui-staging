import { ValuationCreationComponent } from "./valuation-creation.component";
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';
import { RefUnderwriterModel } from '../../../shared/models/ref-underwriter.model';
import { EntrepriseInfoModel } from '../../../shared/models/entreprise-info.model';

describe('ValuationCreationComponent', () => {

    let fixture: ValuationCreationComponent;
    let formBuilderMock: FormBuilder;
    let refUnderwriterServiceMock;
    let entrepriseInfoServiceMock;
    let routerMock;

    let refUnderwriterList: RefUnderwriterModel[] = [{
        "id": "1255",
        "legalName": "name",
        "nsiren": "nsiren",
        "siret": "siret",
        "isRetUnderwriter": true,
        "mainUnderwId": "2",
        "mainUnderwName": "Renault"
       
      }]
      let refUnderwriter: RefUnderwriterModel = {  "id": "1255",
      "legalName": "name",
      "nsiren": "nsiren",
      "siret": "siret",
      "isRetUnderwriter": true,
      "mainUnderwId": "2",
      "mainUnderwName": "Renault"}

      let sirenList = [
          {socialReason : 'RENAULT SAS'} as EntrepriseInfoModel
      ] as  EntrepriseInfoModel[];

    beforeEach(async () => {

        refUnderwriterServiceMock = {
            loadLegalNamesCustomersByQuery: jest.fn().mockReturnValue(of(refUnderwriterList)),
        }

        entrepriseInfoServiceMock={
            loadEntrepriseInfoBySiren: jest.fn().mockReturnValue(of(sirenList)),
        }

        routerMock = {
            navigate: jest.fn().mockReturnValue(new Promise(() => {
                //this is intentional
            })),
            url: '/catalog'
        }
        
        formBuilderMock = new FormBuilder();
        fixture = new ValuationCreationComponent(formBuilderMock, refUnderwriterServiceMock, entrepriseInfoServiceMock, routerMock);
        fixture.ngOnInit();

    });

    describe('Test Component', () => {
        it('should Be Truthy', () => {
            expect(fixture).toBeTruthy();
        });

    });

    describe('Test loadCustomer', () => {
        it('should load list of customers and isCustomerResultNotFound be false', () => {
            fixture.loadCustomer('mom');
            expect(refUnderwriterServiceMock.loadLegalNamesCustomersByQuery).toHaveBeenCalled();
            expect(fixture.legalNamesCustomerOptions).toEqual(refUnderwriterList);
            expect(fixture.isCustomerResultNotFound).toEqual(false);

        });
        it('should isCustomerResultNotFound be true', () => {
            //service return empty list
            spyOn(refUnderwriterServiceMock, 'loadLegalNamesCustomersByQuery').and.returnValue(of([]));
            fixture.loadCustomer('mom');
            expect(refUnderwriterServiceMock.loadLegalNamesCustomersByQuery).toHaveBeenCalled();
            expect(fixture.legalNamesCustomerOptions).toEqual([]);
            expect(fixture.isCustomerResultNotFound).toEqual(true);

        });
    });
    describe('Test disableCustomer', () => {
        it('should isCustomerNamedisabled to be true', () => {
            fixture.disableCustomer();
          
            expect(fixture.isCustomerNamedisabled).toEqual(true);
        });
    });
    
    describe('Test siren', () => {
        it('should disableSiren', () => {
            fixture.disableSiren();
          
            expect(fixture.isSirenDisabled).toEqual(true);
       
        });
        it('should enableSiren', () => {
            fixture.enableSiren();
          
            expect(fixture.isSirenDisabled).toEqual(false);
            expect(fixture.isSirenResultNotFound).toEqual(false);
        });
    });

    describe('Test selectCustomer', () => {
        it('should set variable to true', () => {
            fixture.selectCustomer(refUnderwriter);
           
            expect(fixture.valCreationForm.controls['siren'].value).toEqual("nsiren");
            expect(fixture.selectedCustomerObject).toEqual(refUnderwriter);
            expect(fixture.isCreateDisable).toEqual(false);

        });
    });

    describe('Test selectSiren', () => {
        it('should customerId be RENAULT SAS', () => {
            let sirenObj = {siren: '78012' ,socialReason : 'RENAULT SAS'} as EntrepriseInfoModel;
            fixture.selectSiren(sirenObj);
           
            expect(fixture.valCreationForm.controls['customerId'].value).toEqual("RENAULT SAS");
            expect(fixture.selectedSirenObject ).toEqual(sirenObj);
            expect(fixture.isCreateDisable).toEqual(false);

        });
    });

    describe('Test patchCustomer', () => {
        it('should patchCustomer', () => {
            fixture.valCreationForm = formBuilderMock.group({
                customerId: ['testCustomerId'],
            });
            fixture.patchCustomer(fixture.valCreationForm.controls['customerId']);
            expect(fixture.initialSirenNumber).toEqual("testCustomerId");   
          
        });
    });
   
    describe('Test showSection', () => {
        it('should isArrowUp be false', () => {
            fixture.isArrowUp = true;
            fixture.showSection();            
            expect(fixture.isArrowUp).toEqual(false);

        });
    });

    describe('Test reset', () => {
        it('should legalNamesCustomerOptions and sirenOptions be empty', () => {
            fixture.reset();
            expect(fixture.isCustomerResultNotFound).toEqual(false);
            expect(fixture.isSirenResultNotFound).toEqual(false);
            expect(fixture.isLoading).toEqual(false);
            expect(fixture.isCustomerNamedisabled).toEqual(false);
            expect(fixture.isSirenDisabled).toEqual(false);
            expect(fixture.isCreateDisable).toEqual(true);
            expect(fixture.legalNamesCustomerOptions).toEqual([]);
            expect(fixture.sirenOptions).toEqual([]);

        });
    });

    describe('Test createValorisation', () => {
        it('should router navigate be called', () => {
            fixture.selectedCustomerObject={id:"2"}

            fixture.createValorisation();
            expect(routerMock.navigate).toHaveBeenCalled();
        });
    });


    describe('Test numberOnly', () => {
        it('should return true when number 5 is typed', () => {
            let event ={
                keyCode : 53,
                key : 5
            }
            expect(fixture.numberOnly(event)).toEqual(true);
        });

        it('should return true when a string is typed', () => {
            let event ={
                keyCode : 100,
                key : 'd',
                which : 100
            }
            expect(fixture.numberOnly(event)).toEqual(false);
        });
    });


    describe('Test loadSiren', () => {
        it('should sirenOptions be equalt to sirenList', () => {
            fixture.loadSiren('78012');
            expect(entrepriseInfoServiceMock.loadEntrepriseInfoBySiren).toHaveBeenCalled();
            expect(fixture.sirenOptions).toEqual(sirenList);
        });

        it('should isSirenResultNotFound be true', () => {
            //service return empty list
            spyOn(entrepriseInfoServiceMock, 'loadEntrepriseInfoBySiren').and.returnValue(of([]));
            fixture.loadSiren('7444');
            expect(entrepriseInfoServiceMock.loadEntrepriseInfoBySiren).toHaveBeenCalled();
            expect(fixture.isSirenResultNotFound).toEqual(true);
        });
    });

});