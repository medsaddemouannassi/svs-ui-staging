import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { ErrorModalComponent } from '../../shared/error-modal/error-modal.component';
import { Role } from '../../shared/models/roles.enum';
import { ValuationSearchCatalogueModel } from '../../shared/models/valuation-search-catalogue.model';
import { ValuationSearchModel } from '../../shared/models/valuation-search.model';
import { AuthService } from '../../shared/services/auth.service';
import { PopupService } from '../../shared/services/popup.service';
import { ValuationSearchService } from '../../shared/services/valuation-search.service';
import { ValuationSharedService } from '../../shared/services/valuation-shared-service';
import { CriteriaComponent } from './criteria/criteria.component';
import { SearchCatalogueFilterModel } from '../../shared/models/catalogue-search-filter.model';
import { SearchCatalogueService } from '../../shared/services/search-catalogue.service';
import { SharedCommunicationDataService } from '../../shared/services/shared-communication-data.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  @ViewChild(CriteriaComponent) criteriaComponent: CriteriaComponent;


  searchResultMaterial = { totalRecords: 0, resultList: [] };
  searchResultCatalogue = { totalRecords: 0, resultList: [] };


  loadingMaterialResult = false;
  loadingCatalogueResult = false;
  numberOfElements = 20;
  submited = false;

  currentPage = 1;
  isCateg: boolean = null;


  criteriaMaterial: ValuationSearchModel = {} as ValuationSearchModel;
  criteriaCatalogue: ValuationSearchCatalogueModel = {} as ValuationSearchCatalogueModel;

  constructor(
    private popupService: PopupService,
    private valuationSearchService: ValuationSearchService,
    private valuationSearchCatalogueService: SearchCatalogueService,
    private cd: ChangeDetectorRef, private authService: AuthService,
    public valuationSharedService: ValuationSharedService,
    private dataSharing: SharedCommunicationDataService,
  ) { }

  ngOnInit(): void {
    window.scrollTo(0, 0);
    this.dataSharing.changeStatus("true");

  }
  findValuationMaterialResult(valuationSearch: ValuationSearchModel, sort: string, elementByPage: number, indexPage: number) {

    if (valuationSearch) {

      if (this.valuationSharedService.getIsFromDuplicatedReception() || this.valuationSharedService.getIsFromRequest()) {
        valuationSearch.isFromDuplicated = true;
      }
      this.loadingMaterialResult = true;

      this.valuationSearchService.findValuationSearch(valuationSearch, sort, elementByPage, indexPage).subscribe(data => {

        if (data != null && data.content.length > 0) {


          this.searchResultMaterial.totalRecords = data.totalElements;
          this.searchResultMaterial.resultList = data.content;
        } else {
          this.searchResultMaterial.totalRecords = 0;
          this.searchResultMaterial.resultList = [];
          this.popupService.popupInfo("Votre recherche n'a donné aucun résultat. Merci de réduire vos critères de recherche pour obtenir des résultats", '', null, '', ErrorModalComponent);
        }
        this.loadingMaterialResult = false;


      }, err => {
        this.searchResultMaterial.totalRecords = 0;
        this.searchResultMaterial.resultList = [];
        this.loadingMaterialResult = false;
        console.error(err);
      }
      )
    }
  }

  findValuationCatalogueResult(searchCatalogue: SearchCatalogueFilterModel,  sort: string,elementByPage: number, indexPage: number) {
    if (searchCatalogue) {


      this.loadingCatalogueResult = true;
      this.valuationSearchCatalogueService.findSearchCatalogue(searchCatalogue,  elementByPage, indexPage).subscribe(data => {
        if (data != null && data.content.length > 0) {
          this.searchResultCatalogue.totalRecords = data.totalElements;
          this.searchResultCatalogue.resultList = data.content;
        } else {
          this.searchResultCatalogue.totalRecords = 0;
          this.searchResultCatalogue.resultList = [];
          this.popupService.popupInfo("Votre recherche n'a donné aucun résultat. Merci de réduire vos critères de recherche pour obtenir des résultats", '', null, '', ErrorModalComponent);
        }
        this.loadingCatalogueResult = false;


      }, err => {
        this.searchResultCatalogue.totalRecords = 0;
        this.searchResultCatalogue.resultList = [];
        this.loadingCatalogueResult = false;
        console.error(err);
      }
      )
    }
  }

  loadResultsByMaterial(event) {

    if (this.submited && event) {
      this.currentPage = event.indexPage;
      this.numberOfElements = event.elementByPage;
      this.findValuationMaterialResult(event.criteria, event.sort, event.elementByPage, event.indexPage)
    }
  }

  loadResultsByCatalogue(event) {

    if (this.submited && event) {
      this.currentPage = event.indexPage;
      this.numberOfElements = event.elementByPage;
      this.findValuationCatalogueResult(event.criteria, event.sort, event.elementByPage, event.indexPage)
    }
  }

  resetValuation() {
    this.searchResultMaterial.resultList = [];
    this.searchResultCatalogue.resultList = [];

  }

  searchValuation(event) {
    this.submited = true;
    this.criteriaMaterial = null;
    this.criteriaCatalogue = null;
    this.cd.detectChanges();
    this.isCateg = event.isCateg;
    if (!event.isCateg) {
      this.criteriaMaterial = event.criteria;

    }
    else {
      this.criteriaCatalogue = event.criteria;

    }

  }


  isUserExpertOrAdmin() {
    if (this.authService.currentUserRolesList.includes(Role.SVS_EXPERT_SE) || this.authService.currentUserRolesList.includes(Role.ADMIN_SE)) {
      return true;
    } else {
      return false;
    }
  }
}
