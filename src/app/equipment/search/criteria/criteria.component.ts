import { DatePipe } from '@angular/common';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { forkJoin, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { CatActivitySectorModel } from '../../../shared/models/cat-activity-sector.model';
import { CatNatureModel } from '../../../shared/models/cat-nature.model';
import { SearchCatalogueFilterModel } from '../../../shared/models/catalogue-search-filter.model';
import { MdmInformationsModel } from '../../../shared/models/mdm-informations.model';
import { ParamEnergyModel } from '../../../shared/models/param-energy.model';
import { RefUnderwriterModel } from '../../../shared/models/ref-underwriter.model';
import { Role } from '../../../shared/models/roles.enum';
import { SvsDescModel } from '../../../shared/models/svs-desc.model';
import { ValuationSearchModel } from '../../../shared/models/valuation-search.model';
import { AuthService } from '../../../shared/services/auth.service';
import { CatActivitySectorService } from '../../../shared/services/cat-activity-sector.service';
import { CatAssetTypeService } from '../../../shared/services/cat-asset-type.service';
import { CatBrandService } from '../../../shared/services/cat-brand.service';
import { CatNatureService } from '../../../shared/services/cat-nature.service';
import { MdmInformationService } from '../../../shared/services/mdm-informations.service';
import { ParamEnergyService } from '../../../shared/services/param-energy.servie';
import { RefUnderwriterService } from '../../../shared/services/ref-underwriter.service';
import { SvsDescCollabService } from '../../../shared/services/svs-desc-collab.service';
import { SvsDescService } from '../../../shared/services/svs-desc.service';
import { ValuationSearchService } from '../../../shared/services/valuation-search.service';
import { ValuationSharedService } from '../../../shared/services/valuation-shared-service';


@Component({
  selector: 'app-criteria',
  templateUrl: './criteria.component.html',
  styleUrls: ['./criteria.component.css']
})
export class CriteriaComponent implements OnInit {
  isSimple = true;
  isCateg = false;
  isAdminOrExpertDesc: boolean = false;
  searchForm: FormGroup;
  searchCategForm: FormGroup;
  legalNamesCustomerOptions: RefUnderwriterModel[] = [];
  catNatureOptions: string[] = [];
  catBrandOptions: string[] = [];
  catEnergyApplicableOptions: ParamEnergyModel [] = [];
  sirenOptions: RefUnderwriterModel[] = [];
  idElementOptions: ValuationSearchModel[] = [];

  isSelectedSector: boolean = false;
  isSelectedNature: boolean = false;
  isSelectedBrand: boolean = false;
  isSelectedCatBrand: boolean = false;
  isSelectedType: boolean = false;
  isSelectedCatType: boolean = false;
  public valuationSearchModel: ValuationSearchModel = {} as ValuationSearchModel;
  agencyOptions: MdmInformationsModel[] = [];
  activitySectorOptions: CatActivitySectorModel[] = [];
  energyOptions: ParamEnergyModel[] = [];
  natureOptions: CatNatureModel[] = [];
  brandOptions: string[] = [];
  assetTypeOptions: string[] = [];
  networkOptions: SvsDescModel[] = [];
  caOptions: MdmInformationsModel[] = [];
  expertOptions: MdmInformationsModel[] = [];
  keyword = 'legalName';
  sirenKeyword = 'nsiren';
  elementKeyword = 'elementId'
  cakeyword = 'fullName';
  expertKeyworf = 'fullName';
  catSectorkeyword = 'label';
  catNaturekeyword = 'label';
  catBrandkeyword = 'label';
  brandkeyword = 'label';
  catTypekeyword = 'label';
  Typekeyword = 'label';

  statusOptions: any[] = [];
  isDate = true;
  selectedElement: string
  @Output() searchValuation = new EventEmitter<Object>();
  @Output() resetValuation = new EventEmitter();

  constructor(private fb: FormBuilder, private refUnderwriterService: RefUnderwriterService, private catActivitySectorService: CatActivitySectorService, private catNatureService: CatNatureService
    , private paramEnergyService: ParamEnergyService, private catBrandService: CatBrandService
    , private catAssetTypeService: CatAssetTypeService
    , private mdmInformationService: MdmInformationService
    , private valuationSearchService: ValuationSearchService
    , private authService: AuthService, public valuationSharedService: ValuationSharedService,
    private svsDescCollabService: SvsDescCollabService,
    private svsDescService: SvsDescService
  ) { }

  ngOnInit(): void {


    this.statusOptions = [
      { label: 'Neuf' },
      { label: 'Occasion' },
    ];
    this.searchCategForm = this.fb.group({

      nature: [null],
      brand: [null],
      energy: [null]

    })
    this.searchForm = this.fb.group({
      lastDiscount: [true],
      date: [null],
      startDate: [{ value: null, disabled: true },
      Validators.pattern(new RegExp("([0-9]{4}[/](0[1-9]|1[0-2])[/]([0-2]{1}[0-9]{1}|3[0-1]{1})|([0-2]{1}[0-9]{1}|3[0-1]{1})[/](0[1-9]|1[0-2])[/][0-9]{4})"))],
      endDate: [{ value: null, disabled: true },
      Validators.pattern(new RegExp("([0-9]{4}[/](0[1-9]|1[0-2])[/]([0-2]{1}[0-9]{1}|3[0-1]{1})|([0-2]{1}[0-9]{1}|3[0-1]{1})[/](0[1-9]|1[0-2])[/][0-9]{4})"))],
      contrKSIOP: null,
      customerId: [null],
      walletId: [null],
      numSiren: [null],
      numDc: [null],
      numSdc: [null],
      sectorId: [null],
      natureId: [null],
      energyId: [null],
      brandId: [null],
      typeId: [null],
      typeLabel: [null],
      typeLabelFiltre:[null],
      brandLabelFiltre:[null],
      networkId: [null],
      agencyId: [null],
      caId: [null],
      descId: [null],
      state: [null],
      elementId: [null],
    });

    this.loadAllRef();
    this.getSvsIsExpertDesc();
  }

  loadAllRef() {
    forkJoin(this.catActivitySectorService.getActivitySectorList().pipe(catchError(error => of([]))), this.paramEnergyService.getAllParamEnergy().pipe(catchError(error => of([]))), this.svsDescService.getSvsDescList().pipe(catchError(error => of([])))

    , this.mdmInformationService.getAllAgencies(), this.paramEnergyService.getApplicableParamEnergyList().pipe(catchError(error => of([])))).subscribe((value: any[]) => {
    
    this.activitySectorOptions = value[0];
    
    this.energyOptions = value[1];
    
    this.networkOptions = value[2];
    
    this.agencyOptions = value[3];
    
    this.catEnergyApplicableOptions = value[4];
    
    
    })

  }


  selectEventCustomer(event) {

    if (event) {
      this.resetCustomerId()
    }
  }
  loadCustomer(query) {

    this.legalNamesCustomerOptions = [];
    if (query && query.length > 0) {
      this.refUnderwriterService.loadLegalNamesCustomersByQuery(query).subscribe(
        res => {

          this.legalNamesCustomerOptions = res;
        }
      )
    }

  }
  selectEventSector(event) {
    if (event) {
      this.isSelectedSector = true;
    }
  }

  selectEventNature(event) {
    if (event) {
      this.isSelectedNature = true;
    }
  }
  loadNature(query) {
    this.catNatureOptions = [];

    this.catNatureService.loadNatureListByQuery(null,
      (query != null && query != undefined && query != '') ? query : null, this.searchCategForm.value.brand,
      this.searchCategForm.value.type,null).subscribe(
        res => {

          this.catNatureOptions = res;
          this.isSelectedNature = false;
        }
      )
  }



  selectEventBrand(event) {
    if (event) {
      this.isSelectedBrand = true;
    }
  }
  selectEventCatBrand(event) {
    if (event) {
      this.isSelectedCatBrand = true;
    }
  }


  loadBrand(query) {
    this.catBrandOptions = [];
    this.catBrandService.loadCatBrandListByQuery(null,
      this.searchCategForm.value.nature, (query != null && query != undefined && query != '') ? query : null,
      this.searchCategForm.value.type).subscribe(
        res => {
          this.catBrandOptions = res;
          this.isSelectedBrand = false;
        });

  }

  loadCatBrand(query) {
    this.brandOptions = [];
    this.catBrandService.loadCatBrandListByLabel((query != null && query != undefined && query != '') ? query : null).subscribe(
      res => {
        this.brandOptions = res;
        this.isSelectedCatBrand = false;
      });

  }

  selectEventType(event) {
    if (event) {
      this.isSelectedType = true;
    }
  }



  selectEventCatType(event) {
    if (event) {
      this.isSelectedCatType = true;
    }
  }

  loadCatType(query) {
    this.assetTypeOptions = [];
    this.catAssetTypeService.loadAssetTypeListByLabel((query != null && query != undefined && query != '') ? query : null).subscribe(
      res => {

        this.assetTypeOptions = res;
        this.isSelectedCatType = false;
      }
    )
  }

  selectEventCa(event) {
    if (event) {
      this.resetCaOptions()
    }
  }
  loadCa(query: string) {
    this.caOptions = [];

    this.mdmInformationService.getAllCollabsByQuery(query).subscribe(data => {
      this.caOptions = data;
    })

  }
  loadExpert(query: string) {
    this.expertOptions = [];
    this.mdmInformationService.getAllCollabsByQuery(query).subscribe(data => {
      this.expertOptions = data;

    })

  }

  selectEventExpert(event) {
    if (event) {
      this.resetExpertOptions()
    }
  }
  selectEventSiren(event) {
    if (event) {
      this.resetNsiren();
    }
  }
  loadSiren(query) {
    this.sirenOptions = [];
    this.refUnderwriterService.loadNsirenByQuery(query).subscribe(
      res => {
        this.sirenOptions = res;
      }
    )
  }
  selectEventIdElements(event) {
    if (event) {
      this.resetIdElementOptions();
    }
  }
  loadIdElements(query) {
    this.idElementOptions = [];
    this.valuationSearchService.findValuations(query).subscribe(
      res => {
        this.idElementOptions = res;
      }
    )
  }



  eventCheck() {
    if (this.searchForm.value.lastDiscount) {
      this.searchForm.patchValue({ date: true });
      this.searchForm.get('startDate').enable();
      this.searchForm.get('endDate').enable();
      this.patchDate();
    } else {
      this.searchForm.patchValue({ date: false });
      this.searchForm.get('startDate').disable();
      this.searchForm.get('endDate').disable();
      this.searchForm.patchValue({ startDate: null });
      this.searchForm.patchValue({ endDate: null });
    }
  }

  eventCheckDate() {

    this.searchForm.patchValue({ lastDiscount: this.searchForm.value.date });
    if (this.searchForm.value.date) {
      this.searchForm.get('startDate').disable();
      this.searchForm.get('endDate').disable();
      this.searchForm.patchValue({ startDate: null });
      this.searchForm.patchValue({ endDate: null });
    } else {
      this.patchLastDate();

      this.searchForm.get('startDate').enable();
      this.searchForm.get('endDate').enable();
    }
  }

  patchDate() {
    var datePipe = new DatePipe("en-US");
    this.searchForm.patchValue({ endDate: datePipe.transform(Date.now(), 'dd/MM/yyyy') });
    var m = new Date(Date.now());
    m.setMonth(m.getMonth() - 6)
    this.searchForm.patchValue({ startDate: datePipe.transform(m, 'dd/MM/yyyy') });
  }
  patchLastDate() {
    var datePipe = new DatePipe("en-US");
    var endDate = new Date(Date.now());
    endDate.setMonth(endDate.getMonth() - 6)
    this.searchForm.patchValue({ endDate: datePipe.transform(endDate, 'dd/MM/yyyy') });
    var startDate = new Date(Date.now());
    startDate.setMonth(startDate.getMonth() - 12)
    this.searchForm.patchValue({ startDate: datePipe.transform(startDate, 'dd/MM/yyyy') });
  }

  lodCatNatureListBySectorID() {


    let id = this.searchForm.value.sectorId;
    this.catNatureService.getNatureListByCatActivitySectorID(id).subscribe(data => {
      this.natureOptions = data;
    });
  }



  more() {

    if (this.isSimple && !this.isCateg)
      this.isSimple = false;
    else if (!this.isSimple && !this.isCateg)
      this.isSimple = true;
    else if (this.isSimple && this.isCateg) {
      this.isCateg = false;
      this.isSimple = true;
    }
    this.searchCategForm.reset();
  }
  moreCateg() {
    if (!this.isCateg)
      this.isCateg = true;
    else
      this.isCateg = false;

    this.isSimple = true;
    this.searchForm.reset();
    this.searchForm.patchValue({ lastDiscount: true })
  }

  reset() {
    this.searchCategForm.reset();
    this.searchForm.reset();
    this.resetIdElementOptions()
    this.resetCustomerId()
    this.resetNsiren()
    this.resetCaOptions()
    this.resetExpertOptions()

    this.resetNature()
    this.resetBrand()
    this.resetCatBrand()
    this.resetCatEnergy()
    this.resetCatType()
    this.searchForm.patchValue({ date: false, lastDiscount: true });
    this.resetValuation.emit();
  }
  search() {
    // Recherche avis matériel
    if (!this.isCateg) {
      this.searchForm.markAllAsTouched();
      if (this.searchForm.valid) {
        this.valuationSearchModel = Object.assign({} as ValuationSearchModel, this.searchForm.value);

        if (this.isUserLitig()) {
          this.valuationSearchModel.isLitigation = true;

          if (this.searchForm.value.contrKSIOP != null && this.searchForm.value.contrKSIOP != '') {
            this.valuationSearchModel.contrKSIOP = this.searchForm.value.contrKSIOP;
          }

        } else {
          this.valuationSearchModel.isLitigation = false;
        }

        if (this.valuationSharedService.getIsFromDuplicatedReception()) {
          this.valuationSearchModel.isFromDuplicated = true;
        }
        else {

          this.valuationSearchModel.isFromDuplicated = false;

        }
        if (this.searchForm.value.caId != null && this.searchForm.value.caId != '') {
          this.valuationSearchModel.caId = this.searchForm.value.caId.mat;
        }
        if (this.searchForm.value.numSiren != null && this.searchForm.value.numSiren != '') {
          this.valuationSearchModel.numSiren = this.searchForm.value.numSiren.nsiren;
        }
        if (this.searchForm.value.descId != null && this.searchForm.value.descId != '') {
          this.valuationSearchModel.descId = this.searchForm.value.descId.mat;
        }
        if (this.searchForm.value.elementId != null && this.searchForm.value.elementId != '') {
          this.valuationSearchModel.elementId = this.searchForm.value.elementId.elementId;
        }
        if (this.searchForm.value.customerId != null && this.searchForm.value.customerId != '') {
          this.valuationSearchModel.customerId = this.searchForm.value.customerId.id;
        }
        if (this.searchForm.value.walletId != null && this.searchForm.value.walletId != '') {
          this.valuationSearchModel.walletId = this.searchForm.value.walletId.id;
        }
        if (this.searchForm.value.numDc != null && this.searchForm.value.numDc != '') {
          this.valuationSearchModel.numDc = this.searchForm.value.numDc.id;
        }
        if (this.searchForm.value.numSdc != null && this.searchForm.value.numSdc != '') {
          this.valuationSearchModel.numSdc = this.searchForm.value.numSdc.id;
        }
        if (this.searchForm.value.typeLabelFiltre != null && this.searchForm.value.typeLabelFiltre != undefined && this.assetTypeOptions.includes(this.searchForm.value.typeLabelFiltre)) {
          this.valuationSearchModel.typeLabelFiltre = this.searchForm.value.typeLabelFiltre
        }else{
          this.valuationSearchModel.typeLabelFiltre = null;
        }
        if (this.searchForm.value.brandLabelFiltre != null && this.searchForm.value.brandLabelFiltre != undefined && this.brandOptions.includes(this.searchForm.value.brandLabelFiltre)) {
          this.valuationSearchModel.brandLabelFiltre = this.searchForm.value.brandLabelFiltre
        }else{
          this.valuationSearchModel.brandLabelFiltre = null;
        }


        if (this.searchForm.value.lastDiscount) {
          var datePipe = new DatePipe("en-US");
          this.valuationSearchModel.endDate = datePipe.transform(Date.now(), 'dd/MM/yyyy');
          var m = new Date(Date.now());
          m.setMonth(m.getMonth() - 6)
          this.valuationSearchModel.startDate = datePipe.transform(m, 'dd/MM/yyyy');
        }


        /***reset if empty or undefined */
        this.resetEmptyMat();

        this.searchValuation.emit({ criteria: this.valuationSearchModel, isCateg: false });
      }
    }
    else {



      // Recherche Catalogue
      let valuationSearchCatalogue = {
        natureLabel: (this.searchCategForm.value.nature != null && this.searchCategForm.value.nature != undefined && this.catNatureOptions.includes(this.searchCategForm.value.nature)) ? this.searchCategForm.value.nature : null,
        brandLabel: (this.searchCategForm.value.brand != null && this.searchCategForm.value.brand != undefined && this.catBrandOptions.includes(this.searchCategForm.value.brand)) ? this.searchCategForm.value.brand : null,
         energyId:(this.searchCategForm.value.energy!=null && this.searchCategForm.value.energy!=undefined ) ? this.searchCategForm.value.energy : null ,
      } as SearchCatalogueFilterModel;


      /***reset if empty or undefined */
      this.resetEmptySearchCat(valuationSearchCatalogue);

      this.searchValuation.emit({ criteria: valuationSearchCatalogue, isCateg: true });

    }

  }
  resetCatSector() {
    this.searchForm.patchValue({ natureId: null })
     this.natureOptions = null

  }
  resetCatNature() {
    this.searchForm.patchValue({ natureId: null })


    this.assetTypeOptions = null
    this.brandOptions = null
  }
  resetCatBrand() {
    this.brandOptions = []

    this.searchForm.get('brandLabelFiltre').reset();
  }
  resetNsiren() {
    this.sirenOptions = [];

  }
  resetCustomerId() {
    this.legalNamesCustomerOptions = [];
  }

  resetNature() {
    this.catNatureOptions = [];
    this.searchCategForm.get('nature').reset();

  }
  resetBrand() {
    this.catBrandOptions = [];
    this.searchCategForm.get('brand').reset();
  }
  resetCatEnergy(){

    this.searchCategForm.get('energy').reset();
  }
  resetCatType() {
    this.assetTypeOptions = [];
    this.searchForm.get('typeLabelFiltre').reset();

  }
  resetIdElementOptions() {
    this.idElementOptions = [];
  }
  resetCaOptions() {
    this.caOptions = [];
  }
  resetExpertOptions() {
    this.expertOptions = [];
  }
  resetEmptyMat() {
    /***reset if empty or undefined search on valo*/
    if (this.valuationSearchModel.customerId == undefined || this.valuationSearchModel.customerId == null) {
      this.searchForm.get('customerId').reset();
      this.resetCustomerId();
    }
    if (this.valuationSearchModel.numSiren == undefined || this.valuationSearchModel.numSiren == null) {
      this.searchForm.get('numSiren').reset();
      this.resetNsiren();
    }
    if (this.valuationSearchModel.descId == undefined || this.valuationSearchModel.descId == null) {
      this.searchForm.get('descId').reset();
      this.resetExpertOptions();
    }
    if (this.valuationSearchModel.elementId == undefined || this.valuationSearchModel.elementId == null) {
      this.searchForm.get('elementId').reset();
      this.resetIdElementOptions()
    }
    if (this.valuationSearchModel.caId == undefined || this.valuationSearchModel.caId == null) {
      this.searchForm.get('caId').reset();
      this.resetCaOptions()
    }
    if (this.valuationSearchModel.walletId == undefined || this.valuationSearchModel.walletId == null) {
      this.searchForm.get('walletId').reset();
    }
    if (this.valuationSearchModel.numDc == undefined || this.valuationSearchModel.numDc == null) {
      this.searchForm.get('numDc').reset();
    }
    if (this.valuationSearchModel.numSdc == undefined || this.valuationSearchModel.numSdc == null) {
      this.searchForm.get('numSdc').reset();
    }
    if (this.valuationSearchModel.typeLabelFiltre == null) {
      this.resetCatType();
    }
    if (this.valuationSearchModel.brandLabelFiltre == null) {
      this.resetCatBrand();
    }
  }

  resetEmptySearchCat(searchVal: SearchCatalogueFilterModel) {
    /***reset if empty or undefined search on catalog */

    if (searchVal.natureLabel == null) {
      this.resetNature();
    }
    if (searchVal.brandLabel == null) {
      this.resetBrand();
    }
    if (searchVal.energyId == null) {
      this.resetCatEnergy();
    }
  }

  isUserLitig() {
    if (this.authService.currentUserRolesList.includes(Role.SVS_CONTENTIEUX)
      && !this.authService.currentUserRolesList.includes(Role.SVS_EXPERT_SE)
      && !this.authService.currentUserRolesList.includes(Role.ADMIN_SE)) {
      return true;
    } else {
      return false;
    }
  }
  getSvsIsExpertDesc() {
    if (this.authService.currentUserRolesList.includes(Role.ADMIN_SE) || this.authService.currentUserRolesList.includes(Role.SVS_EXPERT_SE)){
      this.isAdminOrExpertDesc = true;

    }

  }

}
