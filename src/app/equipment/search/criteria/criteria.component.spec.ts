
import { DatePipe } from '@angular/common';
import { TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';
import { MdmInformationsModel } from '../../../shared/models/mdm-informations.model';
import { ParamEnergyModel } from '../../../shared/models/param-energy.model';
import { RefUnderwriterModel } from '../../../shared/models/ref-underwriter.model';
import { Role } from '../../../shared/models/roles.enum';
import { ValuationSearchModel } from '../../../shared/models/valuation-search.model';
import { ValuationSharedService } from '../../../shared/services/valuation-shared-service';
import { CriteriaComponent } from './criteria.component';
import { SvsDescModel } from '../../../shared/models/svs-desc.model';

describe('CriteriaComponent', () => {
  let fixture: CriteriaComponent;
  let formBuilderMock: FormBuilder;
  let refUnderwriterServiceMock: any;
  let catActivitySectorServiceMock: any;
  let catNatureServiceMock: any;
  let paramEnergyServiceMock: any;
  let catBrandServiceMock: any;
  let catAssetTypeServiceMock: any;
  let mdmInformationServiceMock: any;
  let valuationSearchServiceMock: any;
  let authServiceMock: any;
  let valuationSharedServiceMock:any
  let svsDescCollabService : any;
  let svsDescService: any;

  let refUnderwriterList: RefUnderwriterModel[] = [{
    "id": "1255",
    "legalName": "name",
    "nsiren": "nsiren",
    "siret": "siret",
    "isRetUnderwriter": true,
    "mainUnderwId": "2",
    "mainUnderwName": "Renault"


  }]

  let catNatureModelList: any[] = [{
    "id": "123",
    "label": "label",
    "catActivitySector": null,
    "comment": "string",
    "paramNap": null,
    "isActive": true,

  }]

  let networkList: MdmInformationsModel[] = [{

    "mat": "2",
    "fullName": "test",
    "reseau": "network",
    "delegation": "deleg",
    "idDelegation": "123",
    "idReseau": "456",
    "direction": "direction",
    "idDirection": "789",
    "idFullname": "fullname",
    "mail":"mail"
  }, {
    "mat": "3",
    "fullName": "test1",
    "reseau": "network1",
    "delegation": "deleg1",
    "idDelegation": "124",
    "idReseau": "457",
    "direction": "direction1",
    "idDirection": "788",
    "idFullname": "fullname1",
    "mail":"mail"

  }];

  let paramEnergyList: ParamEnergyModel[] = [{
    "id": "123",
    "label": "label",
  }, {
    "id": "124",
    "label": "label1",
  }]
  let catActivitySectorList: any[] = [{
    "id": "123",
    "label": "label",
    "comment": "comment",
    "isActive": true,
  }]

  let catBrandModelList: any[] = [{
    "id": "123",
    "label": "label",
    "catNature": null,
    "comment": "string",
    "isActive": true,
    catRequest: null
  }]

  let catAssetTypeModelList: any[] = [{
    "id": "123",
    "label": "123",
    "catBrand": null,
    "comment": "comment",
    "isActive": true,
    catRequest: null

  }]

  let catAssetTypeLabelList: any[] = [
    "123","1475"

  ]
  let catBrandLabelList: any[] = [
    "123","1475"

  ]

  let agency: MdmInformationsModel[] = [{

    "mat": "2",
    "fullName": "test",
    "reseau": "network",
    "delegation": "deleg",
    "idDelegation": "123",
    "idReseau": "456",
    "direction": "direction",
    "idDirection": "789",
    "idFullname": "fullname",
    "mail":"mail"

  }, {
    "mat": "3",
    "fullName": "test1",
    "reseau": "network1",
    "delegation": "deleg1",
    "idDelegation": "124",
    "idReseau": "456",
    "direction": "direction1",
    "idDirection": "788",
    "idFullname": "fullname1",
    "mail":"mail"

  }];
  let caList: MdmInformationsModel[] = [{

    "mat": "2",
    "fullName": "test",
    "reseau": "network",
    "delegation": "deleg",
    "idDelegation": "123",
    "idReseau": "456",
    "direction": "direction",
    "idDirection": "789",
    "idFullname": "fullname",
    "mail":"mail"

  }, {
    "mat": "3",
    "fullName": "test1",
    "reseau": "network1",
    "delegation": "deleg1",
    "idDelegation": "124",
    "idReseau": "456",
    "direction": "direction1",
    "idDirection": "788",
    "idFullname": "fullname1",
    "mail":"mail"

  }];
  let valuationSearchList: ValuationSearchModel[] = [{
    "valuationSearchId": "12345",
    "elementId": "12345",
  } as ValuationSearchModel];


  beforeEach(async () => {

    valuationSharedServiceMock = TestBed.inject(ValuationSharedService);

    authServiceMock = {
      currentUserRolesList: ['ROLE_SVS_ADMIN_SE']
    }

    catBrandServiceMock = {
      loadCatBrandListByQuery: jest.fn().mockReturnValue(of(catBrandModelList)),
      getCatBrandListByCatNatureId: jest.fn().mockReturnValue(of(catBrandModelList)),
      loadCatBrandListByLabel: jest.fn().mockReturnValue(of(catBrandModelList))
    }

    valuationSearchServiceMock = {
      findValuations: jest.fn().mockReturnValue(of(valuationSearchList)),

    }

    let descsMock = [
      {
          id: '106',
          descName: 'descName',
          fullNameDescManager: 'fullName',
          matriculeDescManager: 'M0000'
      } as SvsDescModel
      ,
      {
          id: '107',
          descName: 'descName',
          fullNameDescManager: 'fullName',
          matriculeDescManager: 'M0007'
      } as SvsDescModel

  ]
    svsDescService = {

      getSvsDescList: jest.fn().mockReturnValue(of(descsMock)),
       
    }

    refUnderwriterServiceMock = {
      loadLegalNamesCustomersByQuery: jest.fn().mockReturnValue(of(refUnderwriterList)),
      loadNsirenByQuery: jest.fn().mockReturnValue(of(refUnderwriterList))
    }

    mdmInformationServiceMock = {
      getAllAgencies: jest.fn().mockReturnValue(of(agency)),
      getAllNetworks: jest.fn().mockReturnValue(of(networkList)),
      getAllCollabsByQuery: jest.fn().mockReturnValue(of(caList))
    }
    catActivitySectorServiceMock = {
      loadActivitySectorListByQuery: jest.fn().mockReturnValue(of(catActivitySectorList)),
      getActivitySectorList: jest.fn().mockReturnValue(of(catActivitySectorList))
    }

    paramEnergyServiceMock = {
      getAllParamEnergy: jest.fn().mockReturnValue(of(paramEnergyList)),
      getApplicableParamEnergyList: jest.fn().mockReturnValue(of(paramEnergyList)),
    }

    catAssetTypeServiceMock = {
      loadAssetTypeListByQuery: jest.fn().mockReturnValue(of(catAssetTypeModelList)),
      getAssetTypeListByCatBrandId: jest.fn().mockReturnValue(of(catAssetTypeModelList)),
      loadAssetTypeListByLabel: jest.fn().mockReturnValue(of(catAssetTypeModelList))
    }

    catNatureServiceMock = {
      getNatureListByCatActivitySectorID: jest.fn().mockReturnValue(of(catNatureModelList)),
      loadNatureListByQuery: jest.fn().mockReturnValue(of(catNatureModelList))
    }
    formBuilderMock = new FormBuilder();
    fixture = new CriteriaComponent(formBuilderMock, refUnderwriterServiceMock, catActivitySectorServiceMock, catNatureServiceMock
      , paramEnergyServiceMock, catBrandServiceMock, catAssetTypeServiceMock, mdmInformationServiceMock
      , valuationSearchServiceMock, authServiceMock,valuationSharedServiceMock,svsDescCollabService,svsDescService);




    fixture.ngOnInit();
  });


  describe('Test Component', () => {
    it('should Be Truthy', () => {
      expect(fixture).toBeTruthy();
    });

  });

  describe('Test isAdminOrExpert', () => {
    it('should Be Truthy', () => {
      expect(fixture.isAdminOrExpertDesc).toEqual(true);
    });
    it('should Be Falsy', () => {
      authServiceMock = {
        currentUserRolesList: ['']
      }
      fixture = new CriteriaComponent(formBuilderMock, refUnderwriterServiceMock, catActivitySectorServiceMock, catNatureServiceMock
        , paramEnergyServiceMock, catBrandServiceMock, catAssetTypeServiceMock, mdmInformationServiceMock
        , valuationSearchServiceMock, authServiceMock,valuationSharedServiceMock,svsDescCollabService,svsDescService);
      expect(fixture.isAdminOrExpertDesc).toEqual(false);
    })
  });

  

  describe('Test loadNature', () => {
    it('should return result', () => {
      let query: null;
      fixture.loadNature(query);
      expect(fixture.catNatureOptions).toEqual(catNatureModelList);
    });

  });

  describe('Test loadBrand', () => {
    it('should return result', () => {
      let query: null;
      fixture.loadBrand(query);
      expect(fixture.catBrandOptions).toEqual(catBrandModelList);
    });

  });


  describe('Test loadAllRef', () => {
    it('should loadAllRef ', () => {

      fixture.loadAllRef();
      expect(catActivitySectorServiceMock.getActivitySectorList).toHaveBeenCalled();
      expect(paramEnergyServiceMock.getAllParamEnergy).toHaveBeenCalled();
      expect(mdmInformationServiceMock.getAllAgencies).toHaveBeenCalled();
    
    });
  });

  describe('Test loadCustomer', () => {
    it('should loadCustomer ', () => {
      fixture.loadCustomer('mom');
      expect(refUnderwriterServiceMock.loadLegalNamesCustomersByQuery).toHaveBeenCalled();
      expect(fixture.legalNamesCustomerOptions).toEqual(refUnderwriterList);
    });
  });

  describe('Test loadCa', () => {
    it('should loadCa ', () => {
      fixture.loadCa('mom');
      expect(mdmInformationServiceMock.getAllCollabsByQuery).toHaveBeenCalled();
      expect(fixture.caOptions).toEqual(caList);
    });
    it('should loadExpert ', () => {
      fixture.loadExpert('mom');
      expect(mdmInformationServiceMock.getAllCollabsByQuery).toHaveBeenCalled();
      expect(fixture.expertOptions).toEqual(caList);
    });
  });

  describe('Test loadSiren', () => {
    it('should loadSiren ', () => {
      fixture.loadSiren('mom');
      expect(refUnderwriterServiceMock.loadNsirenByQuery).toHaveBeenCalled();
      expect(fixture.sirenOptions).toEqual(refUnderwriterList);
    });
  });

  describe('Test loadIdElements', () => {
    it('should loadIdElements ', () => {
      fixture.loadIdElements('123');
      expect(valuationSearchServiceMock.findValuations).toHaveBeenCalled();
      expect(fixture.idElementOptions[0].elementId).toEqual('12345');
    });
  });

  describe('Test eventCheck', () => {
    it('should initialise date to true  ', () => {

      fixture.searchForm.patchValue({ lastDiscount: true });
      fixture.eventCheck();
      expect(fixture.searchForm.value.date).toEqual(true);
    });
    it('should initialise startdate and enddate  ', () => {

      fixture.searchForm.patchValue({ lastDiscount: false });
      fixture.eventCheck();
      expect(fixture.searchForm.value.endDate).toBeNull;
      expect(fixture.searchForm.value.startDate).toBeNull;
      expect(fixture.searchForm.controls['startDate'].disabled).toEqual(true)
      expect(fixture.searchForm.controls['endDate'].disabled).toEqual(true)
    });
  });

  describe('Test eventCheckDate', () => {
    it('should disabled startDate and endDate  ', () => {

      fixture.searchForm.patchValue({ date: true });
      fixture.eventCheckDate();

      expect(fixture.searchForm.controls['startDate'].disabled).toEqual(true)
      expect(fixture.searchForm.controls['endDate'].disabled).toEqual(true)
    });
    it('should  enabled startDate and endDate   ', () => {
      fixture.searchForm.patchValue({ date: false });
      fixture.eventCheckDate();
      expect(fixture.searchForm.controls['startDate'].enabled).toEqual(true)
      expect(fixture.searchForm.controls['endDate'].enabled).toEqual(true)

    });
  });

  describe('Test patchDate', () => {
    it('should  patch  endDate   ', () => {

      fixture.patchDate();

      var datePipe = new DatePipe("en-US");
      let res = datePipe.transform(Date.now(), 'dd/MM/yyyy')
      expect(fixture.searchForm.getRawValue().endDate).toEqual(res);

    });
    it('should  patch startDate ', () => {

      fixture.patchDate();
      var datePipe = new DatePipe("en-US");

      var m = new Date(Date.now());
      m.setMonth(m.getMonth() - 6)
      let res = datePipe.transform(m, 'dd/MM/yyyy')
      expect(fixture.searchForm.getRawValue().startDate).toEqual(res);


    });
  });

  describe('Test lodCatNatureListBySectorID', () => {
    it('should lodCatNatureListBySectorID ', () => {
      fixture.lodCatNatureListBySectorID();
      expect(catNatureServiceMock.getNatureListByCatActivitySectorID).toHaveBeenCalled();
      expect(fixture.natureOptions).toEqual(catNatureModelList);
    });
  });






  describe('Test more', () => {
    it('should set isSimple  to false', () => {
      fixture.isSimple = true;
      fixture.more();
      expect(fixture.isSimple).toEqual(false);
    });
    it('should set isSimple  to true', () => {
      fixture.isSimple = false;
      fixture.more();
      expect(fixture.isSimple).toEqual(true);
    });
    it('should set isSimple  to true and isCateg false', () => {
      fixture.isSimple = true;
      fixture.isCateg = true;
      fixture.more();
      expect(fixture.isCateg).toEqual(false);
      expect(fixture.isSimple).toEqual(true);
    });
  });


  describe('Test reset', () => {
    it('should reset form', () => {
      fixture.searchForm.patchValue({ brandId: "test" });
      fixture.reset();

      expect(fixture.searchForm.value.brandId).toEqual(null);
      expect(fixture.searchForm.value.date).toEqual(false);
    });
    it('should reset resetCatSector', () => {
      fixture.searchForm.patchValue({ natureId: catNatureModelList[0] });

      fixture.resetCatSector();
      expect(fixture.searchForm.value.natureId).toEqual(null);

    });

    it('should reset resetCatNature', () => {
      fixture.searchForm.patchValue({ natureId: catNatureModelList[0] });

      fixture.resetCatNature();

      expect(fixture.searchForm.value.natureId).toEqual(null);

    });
    it('should reset resetCatBrand', () => {

      fixture.searchForm.patchValue({ typeId: catAssetTypeModelList[0] });
      fixture.resetCatBrand();

      expect(fixture.searchForm.value.brandLabelFiltre).toEqual(null);
    });
    it('should reset reset Nsiren', () => {
      fixture.sirenOptions = refUnderwriterList
      fixture.resetNsiren();
      expect(fixture.sirenOptions.length).toEqual(0);
    });
    it('should reset  CustomerId', () => {
      fixture.legalNamesCustomerOptions = refUnderwriterList
      fixture.resetCustomerId();
      expect(fixture.legalNamesCustomerOptions.length).toEqual(0);
    });
    it('should reset IdElementOptions', () => {
      fixture.loadIdElements('123');
      fixture.resetIdElementOptions();
      expect(fixture.idElementOptions.length).toEqual(0);
    });
    it('should reset CaOptions', () => {
      fixture.caOptions = caList;
      fixture.resetCaOptions();
      expect(fixture.caOptions.length).toEqual(0);
    });
    it('should  reset ExpertOptions', () => {
      fixture.expertOptions = caList;
      fixture.resetExpertOptions();
      expect(fixture.expertOptions.length).toEqual(0);
    });

  });

  describe('Test search', () => {
    it('should initialise  valuationSearchModel', () => {

      fixture.searchForm.patchValue({ caId: caList[0] });
      fixture.searchForm.patchValue({ numSiren: refUnderwriterList[0] });
      fixture.searchForm.patchValue({ descId: caList[0] });
      fixture.searchForm.patchValue({ elementId: valuationSearchList[0] });
      fixture.searchForm.patchValue({ customerId: refUnderwriterList[0] });
      fixture.searchForm.patchValue({ lastDiscount: true });
      fixture.searchForm.patchValue({ walletId: 'test' });
      fixture.searchForm.patchValue({ numSdc: 'test' });
      fixture.searchForm.patchValue({ numDc: 'test' });
      fixture.searchForm.patchValue({ typeLabelFiltre : '123' });
      fixture.searchForm.patchValue({ brandLabelFiltre : '123' });
      fixture.assetTypeOptions = catAssetTypeLabelList;
      fixture.brandOptions = catBrandLabelList;

      fixture.search();
      expect(fixture.assetTypeOptions).toEqual(catAssetTypeLabelList);

      expect(fixture.valuationSearchModel.descId).toEqual("2");
      expect(fixture.valuationSearchModel.customerId).toEqual("1255");
      expect(fixture.valuationSearchModel.elementId).toEqual("12345");
      expect(fixture.valuationSearchModel.numSiren).toEqual("nsiren");
      expect(fixture.valuationSearchModel.typeLabelFiltre).toEqual("123") ;   });

    it('should set contrKSIOP value into valuationSearchModel when isUserLitig returns true', () => {
      spyOn(fixture, 'isUserLitig').and.returnValue(true);

      fixture.searchForm.patchValue({ contrKSIOP: '125454d' });
      fixture.search();

      expect(fixture.valuationSearchModel.contrKSIOP).toEqual("125454d");
    });

  });

  describe('Test moreCateg', () => {
    it('should isCateg be true', () => {
      fixture.isCateg = false;
      fixture.moreCateg();
      expect(fixture.isCateg).toEqual(true);
      expect(fixture.isSimple).toEqual(true);
    });

    it('should isCateg be false', () => {
      fixture.isCateg = true;
      fixture.moreCateg();
      expect(fixture.isCateg).toEqual(false);
    });

  });

 

  describe('Test resetNature', () => {
    it('should catNatureOptions be empty array', () => {
      fixture.resetNature();
      expect(fixture.catNatureOptions).toEqual([]);
      expect(fixture.searchCategForm.get('nature').reset()).toHaveBeenCalled;
    });
  });

  describe('Test resetBrand', () => {
    it('should catBrandOptions be empty array', () => {
      fixture.resetBrand();
      expect(fixture.catBrandOptions).toEqual([]);
      expect(fixture.searchCategForm.get('brand').reset()).toHaveBeenCalled;
    });
  });

 


  describe('Test reset catalog search', () => {
    it('should reset nature if field is empty ', () => {
      fixture.isCateg = true;
      fixture.natureOptions = catNatureModelList;
      fixture.searchCategForm.patchValue({
        nature: 'NONE_EXISTING',
      });
      fixture.search();
      expect(fixture.searchCategForm.get('nature').reset()).toHaveBeenCalled;
    });
    it('should reset brand if field is empty', () => {
      fixture.isCateg = true;
      fixture.brandOptions = catBrandModelList;
      fixture.searchCategForm.patchValue({
        brand: 'NONE_EXISTING',
      });
      fixture.search();
      expect(fixture.searchCategForm.get('brand').reset()).toHaveBeenCalled;
    });
   
  });


  describe('Test reset valorisation (material) search', () => {
    it('should reset sector if field is empty', () => {
      fixture.isCateg = false;
      fixture.searchForm.patchValue({ caId: '' });
      fixture.searchForm.patchValue({ numSiren: '' });
      fixture.searchForm.patchValue({ descId: '' });
      fixture.searchForm.patchValue({ elementId: '' });
      fixture.searchForm.patchValue({ customerId: '' });
      fixture.searchForm.patchValue({ lastDiscount: true });
      fixture.searchForm.patchValue({ walletId: '' });
      fixture.searchForm.patchValue({ numSdc: '' });
      fixture.searchForm.patchValue({ numDc: '' });
      fixture.search();

      fixture.valuationSearchModel.customerId = null
      fixture.valuationSearchModel.numSiren = null
      fixture.valuationSearchModel.descId = null
      fixture.valuationSearchModel.elementId = null
      fixture.valuationSearchModel.caId = null
      fixture.valuationSearchModel.walletId = null
      fixture.valuationSearchModel.numDc = null
      fixture.valuationSearchModel.numSdc = null
      fixture.resetEmptyMat();
      expect(fixture.searchForm.get('caId').reset()).toHaveBeenCalled;
      expect(fixture.searchForm.get('numSiren').reset()).toHaveBeenCalled;
      expect(fixture.searchForm.get('elementId').reset()).toHaveBeenCalled;
      expect(fixture.searchForm.get('descId').reset()).toHaveBeenCalled;
      expect(fixture.searchForm.get('lastDiscount').reset()).toHaveBeenCalled;
      expect(fixture.searchForm.get('customerId').reset()).toHaveBeenCalled;
      expect(fixture.searchForm.get('walletId').reset()).toHaveBeenCalled;
      expect(fixture.searchForm.get('numSdc').reset()).toHaveBeenCalled;
      expect(fixture.searchForm.get('numDc').reset()).toHaveBeenCalled;
    });

    it('should not reset', () => {
      fixture.isCateg = false;
      fixture.searchForm.patchValue({ caId: caList[0] });
      fixture.searchForm.patchValue({ numSiren: refUnderwriterList[0] });
      fixture.searchForm.patchValue({ descId: caList[0] });
      fixture.searchForm.patchValue({ elementId: valuationSearchList[0] });
      fixture.searchForm.patchValue({ customerId: refUnderwriterList[0] });
      fixture.searchForm.patchValue({ lastDiscount: true });
      fixture.searchForm.patchValue({ walletId: 'test' });
      fixture.searchForm.patchValue({ numSdc: 'test' });
      fixture.searchForm.patchValue({ numDc: 'test' });
      fixture.search();
    });

  });

  describe('Test isUserLitig', () => {
    it('should return true when user has SVS_CONTENTIEUX ROLE and has not AMDIN_SE ROLE', () => {
      authServiceMock.currentUserRolesList = [Role.SVS_CONTENTIEUX];
      expect(fixture.isUserLitig()).toEqual(true);
    });
  });


  describe('Test selectEvent', () => {
    it('should reset  CustomerId', () => {
      fixture.legalNamesCustomerOptions = refUnderwriterList
      let event: any = "test"
      fixture.selectEventCustomer(event);
      expect(fixture.legalNamesCustomerOptions.length).toEqual(0);
    });
    it('should reset  selectEventSector', () => {
      let event: any = "test"
      fixture.selectEventSector(event);
      expect(fixture.isSelectedSector).toEqual(true);
    });
    it('should reset  selectEventNature', () => {
      let event: any = "test"
      fixture.selectEventNature(event);
      expect(fixture.isSelectedNature).toEqual(true);
    });
    it('should reset  selectEventBrand', () => {
      let event: any = "test"
      fixture.selectEventBrand(event);
      expect(fixture.isSelectedBrand).toEqual(true);
    });
    it('should reset  selectEventCatBrand', () => {
      let event: any = "test"
      fixture.selectEventCatBrand(event);
      expect(fixture.isSelectedCatBrand).toEqual(true);
    });
    it('should reset  selectEventType', () => {
      let event: any = "test"
      fixture.selectEventType(event);
      expect(fixture.isSelectedType).toEqual(true);
    });
    it('should reset  selectEventCatType', () => {
      let event: any = "test"
      fixture.selectEventCatType(event);
      expect(fixture.isSelectedCatType).toEqual(true);
    });

    it('should reset  selectEventCa', () => {
      fixture.caOptions = caList;
      let event: any = "test"
      fixture.selectEventCa(event);
      expect(fixture.caOptions.length).toEqual(0);
    });

    it('should reset  selectEventExpert', () => {
      fixture.expertOptions = caList;
      let event: any = "test"
      fixture.selectEventExpert(event);
      expect(fixture.expertOptions.length).toEqual(0);
    });

    it('should reset  selectEventSiren', () => {
      fixture.sirenOptions = refUnderwriterList;
      let event: any = "retest"
      fixture.selectEventSiren(event);
      expect(fixture.sirenOptions.length).toEqual(0);
    });

    it('should reset  selectEventIdElements', () => {
      fixture.loadIdElements('123');

      let event: any = "retest"
      fixture.selectEventIdElements(event);

      expect(fixture.idElementOptions.length).toEqual(0);
    });





  });

  describe('Test loadCatBrand', () => {
    it('should brandOptions not empty ', () => {
      let query: null;
      fixture.loadCatBrand(query);
      expect(fixture.brandOptions).toEqual(catBrandModelList);
    });
  });

  describe('Test loadCatType', () => {
    it('should assetTypeOptions not empty ', () => {
      let query: null;
      fixture.loadCatType(query);
      expect(fixture.assetTypeOptions).toEqual(catAssetTypeModelList);
    });
  });


});
