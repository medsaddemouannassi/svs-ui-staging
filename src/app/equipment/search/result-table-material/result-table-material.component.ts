import { Component,SecurityContext, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { LazyLoadEvent } from 'primeng/api/public_api';
import { Table } from 'primeng/table';
import { Role } from '../../../shared/models/roles.enum';
import { ValuationSearchModel } from '../../../shared/models/valuation-search.model';
import { AuthService } from '../../../shared/services/auth.service';
import { PopupService } from '../../../shared/services/popup.service';
import { ValuationRequestDocsService } from '../../../shared/services/valuation-request-docs.service';
import { ValuationSharedService } from '../../../shared/services/valuation-shared-service';
import { StatusLabelConstant } from '../../../shared/status-constant';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-result-table-material',
  templateUrl: './result-table-material.component.html',
  styleUrls: ['./result-table-material.component.css']
})


export class ResultTableMaterialComponent implements OnInit, OnChanges {

  @Input() searchResult: { totalRecords: number, resultList: ValuationSearchModel[] };
  @Input() criteria: ValuationSearchModel
  @Input() loading;
  @Output() onLoad = new EventEmitter<{ criteria: ValuationSearchModel, sort: string, elementByPage: number, indexPage: number }>();
  @ViewChild('dt') dt: Table;

  first = 0
  cols: any[] = [];

  isFromDuplication = false;

  constructor(
    private valuationRequestDocsService: ValuationRequestDocsService, private popupService: PopupService, private authService: AuthService,
    private valautionSharedService: ValuationSharedService, private router: Router,private sanitizer: DomSanitizer

  ) {
  }
  ngOnInit(): void {
    this.initCols();
    this.isFromDuplication = this.valautionSharedService.getIsFromDuplicatedReception() || this.valautionSharedService.getIsFromRequest();


  }
  ngOnChanges(changes: SimpleChanges): void {


    if (changes.criteria && !changes.criteria.firstChange && changes.criteria.currentValue) {
      this.dt.filters = {};

      this.dt.reset();


    }
  }


  loadDataLazy(event: LazyLoadEvent) {

    if (this.criteria) {


      let sort = '';
      if (event.multiSortMeta) {
        event.multiSortMeta.forEach(meta => {
          let field = `${meta.field}:${meta.order == 1 ? 'ASC' : 'DESC'};`;
          sort = sort.concat(field);
        })
      }




      this.cols.forEach(col => {


        this.criteria[col.field] = event.filters[col.field] && event.filters[col.field].value.trim().length > 0 ? event.filters[col.field].value : null;


      })


      this.onLoad.emit({ criteria: this.criteria, sort: sort, elementByPage: event.rows, indexPage: (event.first / event.rows) + 1 });
    }

  }

  displayDocumentValuation(valuationRequestId, hasDocument, valuationId) {
    if (hasDocument) {
      this.valuationRequestDocsService.displayDocumentOfValuationByValuationRequestId(valuationRequestId).subscribe(data => {
        if (data && data.url) {
          if (this.valautionSharedService.getIsFromDuplicatedReception() || this.valautionSharedService.getIsFromRequest()) {
            this.popupService.popupPDF("Dupliquer", "Annuler", null, data.url, false);
            let subscriptionPDFConfirmation = this.popupService.onConfirm.subscribe((response) => {



              if (response) {
                if (this.valautionSharedService.getIsFromDuplicatedReception()) {

                  this.valautionSharedService.setSelectedValuationId(valuationId);
                  let valuationRequestIdToNavigate = this.valautionSharedService.getValuation().valuationRequest.id;
                  this.router.navigate(['/reception/expert/' + valuationRequestIdToNavigate])
                }

                if (this.valautionSharedService.getIsFromRequest()) {

                  this.valautionSharedService.setSelectedValuationId(valuationId);
                  this.router.navigate(['/request/' + this.valautionSharedService.getIdRefTiers() + '/' + this.valautionSharedService.getIdBcp()])
                }
              }
              subscriptionPDFConfirmation.unsubscribe();
            });
          } else {
            let safeUrl = this.sanitizer.sanitize(SecurityContext.RESOURCE_URL, this.sanitizer.bypassSecurityTrustResourceUrl(data.url));

            window.open(safeUrl, "_blank");


          }

        }

      });
    }


  }

  initCols() {
    this.cols = [];
    this.cols.push({ field: '', sort: 'hasDocument', label: "Visualiser le pdf" })
    if (this.isUserLitig()) {
      this.cols.push({ field: 'contrKSIOPLabel', sort: 'contrKSIOP', label: "N°contrat KSIOP" })
    }
    this.cols.push({ field: 'elementLabel', sort: 'valuationLabelSort', label: "ID de l'élément" })
    this.cols.push({ field: 'elementType', sort: 'elementType', label: "Type de l'élément" })
    this.cols.push({ field: 'customerName', sort: 'customerName', label: "Nom du client" })
    this.cols.push({ field: 'updatedDateLabel', sort: 'updatedDate', label: "Date de dernière MAJ" })
    this.cols.push({ field: 'status', sort: 'status', label: "Statut" })
    this.cols.push({ field: 'valuationRequestId', sort: 'valuationRequestLabelSort', label: "ID de la demande" })
    this.cols.push({ field: 'walletLabel', sort: 'walletLabel', label: "Contrepartie" })
    this.cols.push({ field: 'numSirenLabel', sort: 'numSirenLabel', label: "Numero SIREN" })
    this.cols.push({ field: 'numDcLabel', sort: 'numDcLabel', label: "Numéro DC" })
    this.cols.push({ field: 'numSdcLabel', sort: 'numSdcLabel', label: "Numéro SDC" })
    this.cols.push({ field: 'caFullName', sort: 'caFullName', label: "Chargé(e) d'affaires" })
    this.cols.push({ field: 'agencyName', sort: 'agencyName', label: "Agence" })
    this.cols.push({ field: 'descFullName', sort: 'descFullName', label: "DESC" })
    this.cols.push({ field: 'networkName', sort: 'networkName', label: "Réseau" })
    this.cols.push({ field: 'sectorLabel', sort: 'sectorLabel', label: "Secteur" })
    this.cols.push({ field: 'natureLabel', sort: 'natureLabel', label: "Nature" })
    this.cols.push({ field: 'brandLabel', sort: 'brandLabel', label: "Marque" })
    this.cols.push({ field: 'typeLabel', sort: 'typeLabel', label: "Type" })
    this.cols.push({ field: 'energyLabel', sort: 'energyLabel', label: "Energie" })
    this.cols.push({ field: 'stateLabel', sort: 'stateLabel', label: "Etat" })
  }


  navigate(element: ValuationSearchModel, field: string) {

    if (element) {

      if (field == 'valuationRequestId' || (field == 'elementId' && element.elementType.toLowerCase().trim() == 'Demande'.toLowerCase())) {

        window.open(window.location.href.replace('/search', '') + `/request/${element.valuationRequestId}`, '_blank');

      }
      else {
        if (this.authService.currentUserRolesList.find(role => role === Role.SVS_CONTENTIEUX) && (element.status === StatusLabelConstant.validatedParDEsc ||
          element.status === StatusLabelConstant.validatedParLaFilére)) {

          window.open(window.location.href.replace('/search', '') + `/contentieux/${element.valuationId}`, '_blank');
        }

        else {
          if (this.authService.currentUserRolesList.find(role => role === Role.ADMIN_SE || role === Role.SVS_EXPERT_SE)) {
            this.navigateExpertAdmin(element);
          }

        }
      }
    }




  }


  navigateExpertAdmin(element) {

    if (element.status != null &&
      (element.status === StatusLabelConstant.validatedParDEsc ||
        element.status === StatusLabelConstant.validatedParLaFilére || element.status === StatusLabelConstant.simulation)) {

      window.open(window.location.href.replace('/search', '') + `/result/expert/${element.valuationId}`, '_blank');
    }
    else {

      window.open(window.location.href.replace('/search', '') + `/reception/expert/${element.valuationRequestId}`, '_blank');

    }
  }
  showRedirection(element: ValuationSearchModel, field: string) {
    if (element) {

      if (this.authService.currentUserRolesList.find(role => role === Role.ADMIN_SE || role === Role.SVS_EXPERT_SE || role === Role.SVS_CONTENTIEUX)) {
        return true;
      }
      else if (this.authService.currentUserRolesList.find(role => role === Role.CA) && field == 'valuationRequestId' || (field == 'elementId' && element.elementType.toLowerCase().trim() == 'Demande'.toLowerCase())) {
        return true;
      }
      else if (this.authService.currentUserRolesList.find(role => role === Role.CONSULTATION) && field == 'valuationRequestId' || (field == 'elementId' && element.elementType.toLowerCase().trim() == 'Demande'.toLowerCase())) {
        return true;
      } else {
        return false;
      }
    }
  }

  isUserLitig() {
    if (this.authService.currentUserRolesList.includes(Role.SVS_CONTENTIEUX)
      && !this.authService.currentUserRolesList.includes(Role.SVS_EXPERT_SE)
      && !this.authService.currentUserRolesList.includes(Role.ADMIN_SE)) {
      return true;
    } else {
      return false;
    }
  }
}
