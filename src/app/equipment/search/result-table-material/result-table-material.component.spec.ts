import {TestBed} from '@angular/core/testing';
import {LazyLoadEvent} from 'primeng/api/public_api';
import {Table} from 'primeng/table/table';
import {of, Subject} from 'rxjs';
import {Role} from '../../../shared/models/roles.enum';
import {ValuationRequestModel} from '../../../shared/models/valuation-request.model';
import {ValuationSearchModel} from '../../../shared/models/valuation-search.model';
import {ValuationModel} from '../../../shared/models/valuation.model';
import {ValuationSharedService} from '../../../shared/services/valuation-shared-service';
import {StatusLabelConstant} from '../../../shared/status-constant';
import {ResultTableMaterialComponent} from './result-table-material.component';
import {DomSanitizer} from '@angular/platform-browser';


describe('ResultTableComponent', () => {
  let fixture: ResultTableMaterialComponent;
  let valuationRequestDocsServiceMock: any;
  let popupServiceMock: any;
  let authServiceMock: any;
  let routerMock: any;
  let valuationSharedService: any;
  let sanitizer: any;


  beforeEach(() => {
    routerMock = {
      navigate: jest.fn().mockReturnValue(new Promise(() => {
        //this is intentional
      })),
    }
    valuationSharedService = TestBed.inject(ValuationSharedService);

    let valuation = {
      "id": '1',
      "valuationRequest": {
        "id": '1'
      } as ValuationRequestModel
    } as ValuationModel

    popupServiceMock = {
      popupPDF: jest.fn().mockReturnValue(of(true)),
      onConfirm: new Subject<boolean>()

    };


    valuationRequestDocsServiceMock = {
      displayDocumentOfValuationByValuationRequestId: jest.fn().mockReturnValue(of({
        url: 'url'
      })),
      getIsFromDuplicatedReception: jest.fn(),
      getIsFromRequest: jest.fn()
    };

    authServiceMock = {
      'identityClaims': {
        'mat': 'M102'
      },
      'currentUserRolesList': ['ROLE_SVS_ADMIN_SE', 'ROLE_SVS_CONTENTIEUX']
    }
    sanitizer = TestBed.inject(DomSanitizer);

    fixture = new ResultTableMaterialComponent(valuationRequestDocsServiceMock,
      popupServiceMock, authServiceMock, valuationSharedService, routerMock, sanitizer);
    valuationSharedService.setValuation(valuation)

    fixture.criteria = {} as ValuationSearchModel
    fixture.searchResult = {
      totalRecords: 1,
      resultList: [{valuationRequestId: '1', status: StatusLabelConstant.validatedParDEsc} as ValuationSearchModel,
        {valuationRequestId: '1', status: StatusLabelConstant.validatedParDEsc} as ValuationSearchModel]
    }
    fixture.ngOnInit();
  });

  describe('Test: ngOnInit', () => {
    it('should initialize form', () => {

      let cols = []
      cols.push({field: '', sort: 'hasDocument', label: "Visualiser le pdf"})
      cols.push({field: 'elementLabel', sort: 'valuationLabelSort', label: "ID de l'élément"})
      cols.push({field: 'elementType', sort: 'elementType', label: "Type de l'élément"})
      cols.push({field: 'customerName', sort: 'customerName', label: "Nom du client"})
      cols.push({field: 'updatedDateLabel', sort: 'updatedDate', label: "Date de dernière MAJ"})
      cols.push({field: 'status', sort: 'status', label: "Statut"})
      cols.push({field: 'valuationRequestId', sort: 'valuationRequestLabelSort', label: "ID de la demande"})
      cols.push({field: 'walletLabel', sort: 'walletLabel', label: "Contrepartie"})
      cols.push({field: 'numSirenLabel', sort: 'numSirenLabel', label: "Numero SIREN"})
      cols.push({field: 'numDcLabel', sort: 'numDcLabel', label: "Numéro DC"})
      cols.push({field: 'numSdcLabel', sort: 'numSdcLabel', label: "Numéro SDC"})
      cols.push({field: 'caFullName', sort: 'caFullName', label: "Chargé(e) d'affaires"})
      cols.push({field: 'agencyName', sort: 'agencyName', label: "Agence"})
      cols.push({field: 'descFullName', sort: 'descFullName', label: "DESC"})
      cols.push({field: 'networkName', sort: 'networkName', label: "Réseau"})
      cols.push({field: 'sectorLabel', sort: 'sectorLabel', label: "Secteur"})
      cols.push({field: 'natureLabel', sort: 'natureLabel', label: "Nature"})
      cols.push({field: 'brandLabel', sort: 'brandLabel', label: "Marque"})
      cols.push({field: 'typeLabel', sort: 'typeLabel', label: "Type"})
      cols.push({field: 'energyLabel', sort: 'energyLabel', label: "Energie"})
      cols.push({field: 'stateLabel', sort: 'stateLabel', label: "Etat"})
      expect(fixture.cols).toEqual(cols);
    });


    it('should call initCols', () => {
      const spyInstance = jest.spyOn(fixture, 'isUserLitig').mockReturnValue(true);
      fixture.ngOnInit();
      expect(spyInstance).toHaveBeenCalled();
    });

  });

  describe('Test: ngOnChanges', () => {
    it('should call reset', () => {
      let changes: any = {
        criteria: {
          firstChange: false,
          currentValue: true
        }
      }

      fixture.dt = {} as Table
      fixture.dt.filters = {}

      fixture.dt.reset = jest.fn().mockReturnValue(of(true));

      fixture.ngOnChanges(changes);
      expect(fixture.dt.reset).toHaveBeenCalled();
    });

    it('should not call reset', () => {
      let changes: any = {
        criteria: {
          firstChange: true,
          currentValue: true
        }
      }

      fixture.dt = {} as Table
      fixture.dt.filters = {}

      fixture.dt.reset = jest.fn().mockReturnValue(of(true));

      fixture.ngOnChanges(changes);
      expect(fixture.dt.reset).toHaveBeenCalledTimes(0);
    });


  });
  describe('Test: loadDataLazy', () => {
    it('should loadDataLazy ', () => {

      let event: LazyLoadEvent = {} as LazyLoadEvent;
      event.first = 20;
      event.rows = 20;
      event.filters = {
        networkName: {
          value: 'Resau aura'
        }
      }

      const spyOnEmit = jest.spyOn(fixture.onLoad, 'emit');
      fixture.criteria = {networkName: ''} as ValuationSearchModel;
      fixture.loadDataLazy(event);
      expect(fixture.criteria.networkName).toEqual('Resau aura')

      expect(spyOnEmit).toHaveBeenCalledWith({criteria: fixture.criteria, sort: '', elementByPage: 20, indexPage: 2});

    });


    it('should not loadDataLazy ', () => {

      let event: LazyLoadEvent = {} as LazyLoadEvent;
      event.first = 20;
      event.rows = 20;
      event.filters = {
        networkName: {
          value: 'Resau aura'
        }
      }

      const spyOnEmit = jest.spyOn(fixture.onLoad, 'emit');
      fixture.criteria = null;
      fixture.loadDataLazy(event);

      expect(spyOnEmit).toHaveBeenCalledTimes(0);

    });

    it('should loadDataLazy with sort', () => {
      fixture.ngOnInit()
      let event: LazyLoadEvent = {} as LazyLoadEvent;
      event.first = 20;
      event.rows = 20;
      event.filters = {
        networkName: {
          value: 'Resau aura'
        }
      }
      event.multiSortMeta = [
        {
          field: 'elementLabel',
          order: 1,

        }
      ]

      const spyOnEmit = jest.spyOn(fixture.onLoad, 'emit');
      fixture.criteria = {networkName: ''} as ValuationSearchModel;

      fixture.loadDataLazy(event);

      expect(fixture.criteria.networkName).toEqual(event.filters.networkName.value)

      expect(spyOnEmit).toHaveBeenCalledWith({
        criteria: fixture.criteria,
        sort: 'elementLabel:ASC;',
        elementByPage: 20,
        indexPage: 2
      });

    });

    it('should loadDataLazy with sort DESC', () => {
      fixture.ngOnInit()
      let event: LazyLoadEvent = {} as LazyLoadEvent;
      event.first = 20;
      event.rows = 20;
      event.filters = {
        networkName: {
          value: 'Resau aura'
        }
      }
      event.multiSortMeta = [
        {
          field: 'elementLabel',
          order: -1,

        }
      ]

      const spyOnEmit = jest.spyOn(fixture.onLoad, 'emit');
      fixture.criteria = {networkName: ''} as ValuationSearchModel;

      fixture.loadDataLazy(event);

      expect(fixture.criteria.networkName).toEqual(event.filters.networkName.value)

      expect(spyOnEmit).toHaveBeenCalledWith({
        criteria: fixture.criteria,
        sort: 'elementLabel:DESC;',
        elementByPage: 20,
        indexPage: 2
      });

    });


    describe('Test: navigate', () => {
      it('should display request', () => {
        const spyOnWindow = jest.spyOn(window, 'open');
        fixture.navigate({
          valuationRequestId: '2',
          elementType: 'Demande'
        } as ValuationSearchModel, 'valuationRequestId')
        expect(spyOnWindow).toHaveBeenCalled();


      });

      it('should display valo', () => {
        const spyOnWindow = jest.spyOn(window, 'open');
        fixture.navigate({valuationRequestId: '1', elementType: 'Valorisation'} as ValuationSearchModel, 'elementId')
        expect(spyOnWindow).toHaveBeenCalled();


      });
      it('should display contentieux ', () => {
        authServiceMock = {
          currentUserRolesList: [Role.SVS_CONTENTIEUX]
        }
        fixture = new ResultTableMaterialComponent(valuationRequestDocsServiceMock, popupServiceMock, authServiceMock
          , valuationSharedService, routerMock, sanitizer);
        fixture.searchResult = {
          totalRecords: 1,
          resultList: [{valuationRequestId: '1', status: StatusLabelConstant.validatedParDEsc} as ValuationSearchModel,
            {valuationRequestId: '1', status: StatusLabelConstant.validatedParDEsc} as ValuationSearchModel]
        }
        const spyOnWindow = jest.spyOn(window, 'open');
        fixture.navigate({valuationRequestId: '1', elementType: 'Valorisation'} as ValuationSearchModel, 'elementId')
        expect(spyOnWindow).toHaveBeenCalled();

      })
      it('should display contentieux ', () => {
        authServiceMock = {
          currentUserRolesList: [Role.SVS_CONTENTIEUX]
        }
        fixture = new ResultTableMaterialComponent(valuationRequestDocsServiceMock, popupServiceMock, authServiceMock
          , valuationSharedService, routerMock, sanitizer);
        fixture.searchResult = {
          totalRecords: 1,
          resultList: [{valuationRequestId: '1', status: StatusLabelConstant.validatedParDEsc} as ValuationSearchModel,
            {valuationRequestId: '1', status: StatusLabelConstant.validatedParDEsc} as ValuationSearchModel]
        }
        const spyOnWindow = jest.spyOn(window, 'open');
        fixture.navigate({
          valuationRequestId: '1',
          elementType: 'Valorisation',
          status: StatusLabelConstant.validatedParLaFilére
        } as ValuationSearchModel, 'elementId')
        expect(spyOnWindow).toHaveBeenCalled();

      })
      it('should display result ', () => {
        authServiceMock = {
          currentUserRolesList: [Role.ADMIN_SE]
        }
        fixture = new ResultTableMaterialComponent(valuationRequestDocsServiceMock, popupServiceMock, authServiceMock
          , valuationSharedService, routerMock, sanitizer);
        fixture.searchResult = {
          totalRecords: 1,
          resultList: [{valuationRequestId: '1', status: StatusLabelConstant.validatedParDEsc} as ValuationSearchModel,
            {valuationRequestId: '1', status: StatusLabelConstant.validatedParDEsc} as ValuationSearchModel]
        }
        const spyOnWindow = jest.spyOn(window, 'open');
        fixture.navigate({
          valuationRequestId: '1',
          elementType: 'Valorisation',
          status: StatusLabelConstant.validatedParDEsc
        } as ValuationSearchModel, 'elementId')
        expect(spyOnWindow).toHaveBeenCalled();

      })
    });

    describe('Test: displayDocumentValuation', () => {


      it('should popupPDF be called with duplication btns from reqst', () => {
        valuationSharedService.setIsFromRequest(true);
        valuationSharedService.setIsFromDuplicatedReception(false);


        fixture.displayDocumentValuation('55', true, '1');
        valuationRequestDocsServiceMock.displayDocumentOfValuationByValuationRequestId().subscribe((te) => {
          expect(popupServiceMock.popupPDF).toHaveBeenCalled();

          popupServiceMock.onConfirm.next(true);
          popupServiceMock.onConfirm.subscribe((r) => {
            expect(routerMock.navigate).toHaveBeenCalled();

          })


        })

      });
      it('should popupPDF be called with duplication btns from receptiont', () => {
        valuationSharedService.setIsFromRequest(false);
        valuationSharedService.setIsFromDuplicatedReception(true);


        fixture.displayDocumentValuation('55', true, '1');
        valuationRequestDocsServiceMock.displayDocumentOfValuationByValuationRequestId().subscribe((te) => {
          expect(popupServiceMock.popupPDF).toHaveBeenCalled();

          popupServiceMock.onConfirm.next(true);
          popupServiceMock.onConfirm.subscribe((r) => {
            expect(routerMock.navigate).toHaveBeenCalled();

          })
        })

      });

      it('should open blank page', () => {
        jest.spyOn(valuationRequestDocsServiceMock, 'getIsFromDuplicatedReception').mockReturnValue(false);
        jest.spyOn(valuationRequestDocsServiceMock, 'getIsFromRequest').mockReturnValue(false);
        const spyOnWindow = jest.spyOn(window, 'open').mockImplementation();
        fixture.displayDocumentValuation('55', true, '0');
        expect(spyOnWindow).toHaveBeenCalled();
      });

      it('should popupPDF not be called', () => {
        fixture.displayDocumentValuation('55', false, '0');
        expect(popupServiceMock.popupPDF).toHaveBeenCalledTimes(0);

      });


    });


  });

  describe('Test isUserLitig', () => {
    it('should return true when user has SVS_CONTENTIEUX ROLE and has not AMDIN_SE ROLE', () => {
      authServiceMock.currentUserRolesList = [Role.SVS_CONTENTIEUX];
      expect(fixture.isUserLitig()).toEqual(true);
    });
  });

  describe('Test showRedirection', () => {
    it('should return if can be redirected', () => {

      authServiceMock.currentUserRolesList = [];

      expect(fixture.showRedirection({elementType: 'Demande'} as ValuationSearchModel, 'valuationRequestId')).toEqual(false);


      authServiceMock.currentUserRolesList = [Role.CONSULTATION];

      expect(fixture.showRedirection({elementType: 'Demande'} as ValuationSearchModel, 'valuationRequestId')).toEqual(true);

      authServiceMock.currentUserRolesList = [Role.CONSULTATION, Role.CA];

      expect(fixture.showRedirection({elementType: 'Demande'} as ValuationSearchModel, 'valuationRequestId')).toEqual(true);

      authServiceMock.currentUserRolesList = [Role.CONSULTATION, Role.SVS_EXPERT_SE];

      expect(fixture.showRedirection({elementType: 'Demande'} as ValuationSearchModel, 'valuationRequestId')).toEqual(true);


    });
  });

});
