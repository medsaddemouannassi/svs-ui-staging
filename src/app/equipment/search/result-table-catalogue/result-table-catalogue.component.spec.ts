import { LazyLoadEvent } from 'primeng/api/public_api';
import { ResultTableCatalogueComponent } from './result-table-catalogue.component';
import { Table } from 'primeng/table/table';
import { of } from 'rxjs';

import { SearchCatalogueFilterModel } from '../../../shared/models/catalogue-search-filter.model';
import { ParamEnergyModel } from '../../../shared/models/param-energy.model';


describe('ResultTableCatalogueComponent', () => {
  let fixture: ResultTableCatalogueComponent;
  let paramEnergyServiceMock: any;

  let paramEnergyList: ParamEnergyModel[] = [{
    "id": "123",
    "label": "diesel",
  }, {
    "id": "124",
    "label": "biocarburant",
  }]
  beforeEach(() => {
    paramEnergyServiceMock = {
      getApplicableParamEnergyList: jest.fn().mockReturnValue(of(paramEnergyList))
    }

    fixture = new ResultTableCatalogueComponent(paramEnergyServiceMock);
    fixture.criteria = {} as SearchCatalogueFilterModel
    fixture.ngOnInit();
  });

  describe('Test: ngOnInit', () => {
    it('should initialize form', () => {

      let cols = []
      cols.push({ field: 'name', header: "Nom" ,class:"nameLabel"})
      cols.push({ field: 'disocuntRateYearOne', header: "Décote  année 1",class:"inputPercent" })
      cols.push({ field: 'disocuntRateYeartwo', sort: 'décote2', header: "Décote  année 2" ,class:"inputPercent"})
      cols.push({ field: 'disocuntRateYearThree', sort: 'décote3', header: "Décote année 3" ,class:"inputPercent"})
      cols.push({ field: 'disocuntRateYearFour', sort: 'décote4', header: "Décote année 4" ,class:"inputPercent"})
      cols.push({ field: 'disocuntRateYearFive', sort: 'décote5', header: "Décote année 5" ,class:"inputPercent"})
      cols.push({ field: 'commentaire', sort: 'commentaire', header: "Commentaire",class:"inputTextTT" })
      cols.push({ field: 'type', sort: 'type', header: "Type" ,class:"inputTextTT"})
     fixture.energiesList.forEach(element => {
       cols.push({ field: 'energyNature', header: "Pondération "+ element.label ,class:"ponderation"})
      });
          cols.push({ field: 'ponderationRate', header: "Pondération occasion" ,class:"ponderation"})

      expect(fixture.cols).toEqual(cols);

    });



  });


  describe('Test: ngOnChanges', () => {
    it('should call reset', () => {
      let changes: any = {
        criteria: {
          firstChange: false,
          currentValue: true
        }
      }

      fixture.dt = {


      } as Table
      fixture.dt.filters = {
      }

      fixture.dt.reset = jest.fn().mockReturnValue(of(true));

      fixture.ngOnChanges(changes);
      expect(fixture.dt.reset).toHaveBeenCalled();
    });



  });

  describe('Test: loadDataLazy', () => {
    it('should loadDataLazy ', () => {

      let event: LazyLoadEvent = {} as LazyLoadEvent;
      event.first = 20;
      event.rows = 20;
      event.filters = {
        brandLabel: {
          value: 'Resau aura'
        }
      }

      const spyOnEmit = jest.spyOn(fixture.onLoad, 'emit');
      fixture.criteria = { brandLabel: 'brand label' ,natureLabel:null,energyId:null} as SearchCatalogueFilterModel;
      fixture.loadDataLazy(event);
      expect(fixture.criteria.brandLabel).toEqual('brand label')

      expect(spyOnEmit).toHaveBeenCalledWith({ criteria: fixture.criteria, sort: '', elementByPage: 20, indexPage: 2 });

    });

    it('should loadDataLazy with sort', () => {
      fixture.ngOnInit()
      let event: LazyLoadEvent = {} as LazyLoadEvent;
      event.first = 20;
      event.rows = 20;
      event.filters = {
        brandLabelSearch: {
          value: 'brandLabelSearch'
        }
      }
      event.multiSortMeta = [
        {
          field: 'sectorLabel',
          order: 1
        }
      ]

      const spyOnEmit = jest.spyOn(fixture.onLoad, 'emit');



      fixture.loadDataLazy(event);


      expect(spyOnEmit).toHaveBeenCalledWith({ criteria: fixture.criteria, sort: 'sectorLabel:ASC;', elementByPage: 20, indexPage: 2 });

    });






  });
  describe('test fomat percent',()=>
{
  it('should fomat decimal and return value',()=>{
    let n = fixture.formatPercent('20.0');
    expect(n).toEqual(20)
  })
  it('should  not return value',()=>{
    let n = fixture.formatPercent(null);
    expect(n).toEqual('')
  })
}
);


});
