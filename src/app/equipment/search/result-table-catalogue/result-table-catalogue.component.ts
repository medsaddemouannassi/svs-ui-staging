import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { LazyLoadEvent, TreeNode } from 'primeng/api/public_api';
import { Table } from 'primeng/table';

import { ParamEnergyModel } from '../../../shared/models/param-energy.model';
import { ParamEnergyService } from '../../../shared/services/param-energy.servie';
import { SearchCatalogueFilterModel } from '../../../shared/models/catalogue-search-filter.model';
import { SearchCatalogueDataModel } from '../../../shared/models/catalogue-search-data.model';
import { threeDigitDecimalRound } from '../../../shared/ValuationUtils';

@Component({
  selector: 'app-result-table-catalogue',
  templateUrl: './result-table-catalogue.component.html',
  styleUrls: ['./result-table-catalogue.component.css']
})
export class ResultTableCatalogueComponent implements OnInit,OnChanges {

  @Input() searchResult: { totalRecords: number, resultList: SearchCatalogueDataModel[] };
  @Input() criteria: SearchCatalogueFilterModel;
  @Input() loading;
  @Output() onLoad = new EventEmitter<{ criteria: SearchCatalogueFilterModel, sort: string, elementByPage: number, indexPage: number }>();
  @ViewChild('dt') dt: Table;
  first = 0
  cols: any[] = [];

   listData :TreeNode[] = [];
  energiesList : ParamEnergyModel[] = [];
  ngOnInit(): void {
    this.paramEnergyService.getApplicableParamEnergyList().subscribe(data => this.energiesList = data, (err)=>{console.error(err);},()=>{

      this.initCols();
    })


}

formatPercent(val){
  if(val!=null){
    return threeDigitDecimalRound(val);
  }
  else {return ''};
}
  constructor(
   private paramEnergyService : ParamEnergyService
  ) {
  }
 ngOnChanges(changes: SimpleChanges): void {


    if (changes.criteria && !changes.criteria.firstChange && changes.criteria.currentValue) {

      this.dt.reset();


    }

 }


  loadDataLazy(event: LazyLoadEvent) {

    if (this.criteria) {


      let sort = '';
     if (event.multiSortMeta) {
        event.multiSortMeta.forEach(meta => {
          let field = `${meta.field}:${meta.order == 1 ? 'ASC' : 'DESC'};`;
          sort = sort.concat(field);
        })
      }




      this.cols.forEach(col => {


        this.criteria[col.field] = event.filters[col.field] && event.filters[col.field].value.trim().length > 0 ? event.filters[col.field].value : null;


      })


      this.onLoad.emit({ criteria: this.criteria, sort: sort, elementByPage: event.rows, indexPage: (event.first / event.rows) + 1 });
    }

  }




  initCols() {
    this.cols = [];
    this.cols.push({ field: 'name', header: "Nom" ,class:"nameLabel"})
    this.cols.push({ field: 'disocuntRateYearOne', header: "Décote  année 1",class:"inputPercent" })
    this.cols.push({ field: 'disocuntRateYeartwo', sort: 'décote2', header: "Décote  année 2" ,class:"inputPercent"})
    this.cols.push({ field: 'disocuntRateYearThree', sort: 'décote3', header: "Décote année 3" ,class:"inputPercent"})
    this.cols.push({ field: 'disocuntRateYearFour', sort: 'décote4', header: "Décote année 4" ,class:"inputPercent"})
    this.cols.push({ field: 'disocuntRateYearFive', sort: 'décote5', header: "Décote année 5" ,class:"inputPercent"})
    this.cols.push({ field: 'commentaire', sort: 'commentaire', header: "Commentaire",class:"inputTextTT" })
    this.cols.push({ field: 'type', sort: 'type', header: "Type" ,class:"inputTextTT"})
    this.energiesList.forEach(element => {
      this.cols.push({ field: 'energyNature', header: "Pondération "+ element.label ,class:"ponderation"})
    });
        this.cols.push({ field: 'ponderationRate', header: "Pondération occasion" ,class:"ponderation"})

  }


  getEnergyValue(energyList,energyId){
   

    if(energyList && energyList[energyId]){
      return energyList[energyId];
 

    }
    return;
  }



}
