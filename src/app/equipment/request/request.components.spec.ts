import { TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { BehaviorSubject, of, Subject } from 'rxjs';
import { EntrepriseInfoModel } from '../../shared/models/entreprise-info.model';
import { ParamSettingModel } from '../../shared/models/param-setting.model';
import { ValuationRequestWorkflowModel } from '../../shared/models/valuation-request-workflow.model';
import { ValuationSharedService } from '../../shared/services/valuation-shared-service';
import { StatusConstant } from '../../shared/status-constant';
import { RequestComponent } from './request.component';

describe('RequestComponent', () => {
  let fixture: RequestComponent;
  let formBuilderMock: FormBuilder;
  let authServiceMock: any;
  let modalServiceMock: any;
  let entrepriseInfoServiceMock: any;
  let valuationRequestServiceMock: any;
  let routeMock: any;
  let routerMock: any;
  let valuationRequest: any;
  let paramSettingServiceMock: any;
  let popupServiceMock: any;
  let valuationRequestWrkflMock: any;
  let dataSharingMock: any;
  let valuationSharedServiceMock: any;


  beforeEach(() => {
    valuationRequest = {
      id: '1',
      externalSystemRef: 'S',
      underwriterId: '1',
      isRetUnderwriter: true,
      customerPropoId: '1',
      bssResponsible: '1',
      requestComment: 'comment',
      financingAmount: 450,
      assetCreationDate: new Date(),
      assetUsVolume: 50,
      collAssetEstimYear: 2020,
      co2Emission: 15,
      fiscPower: 7,
      isAssetUseAbroad: false,
      isANewAsset: true,
      isLicencePlateAvaileble: true,
      paramEnergy: null,
      paramUsageUnit: null,
      unitaryQuantity: 2,
      bssResponsibleFullName: 'name'
    }

    entrepriseInfoServiceMock = {
      entrepriseInfo: {} as EntrepriseInfoModel
    }

    formBuilderMock = new FormBuilder();
    modalServiceMock = {
      open: jest.fn().mockReturnValue({
        componentInstance: {
          content: '',
          onExit: of(true)
        }
      }),
      dismissAll :jest.fn().mockReturnValue(of(true))
    };
    valuationRequestServiceMock = {
      getValuationRequestById: jest.fn().mockReturnValue(of(valuationRequest))

    }
    authServiceMock = {
      'identityClaims': {
        'mat': 'M102'
      },
      'currentUserRolesList': ['ROLE_SVS_ADMIN_SE','ROLE_SVS_CA']
    }


    routeMock = {
      snapshot: {
        params: {
          idRequest: '1'
        },
        paramMap:{
          get:jest.fn().mockReturnValue('xx')
        }
      }
    }
        routerMock = {
          url: '/search',
          navigate: jest.fn().mockReturnValue(new Promise(() => {
            //this is intentional
        })),
      }
      
    

    paramSettingServiceMock = {
      getParamByCode: jest.fn().mockReturnValue(of([
        { value: StatusConstant.STATUS_CREATED },
        { value: StatusConstant.WAIT_EXP_VAL },
        { value: StatusConstant.WAIT_INFO_EXP_CA },
        { value: StatusConstant.RESP_INFO_CA_EXP },
        { value: StatusConstant.WAIT_INFO_EXP_FIL },
        { value: StatusConstant.RESP_INFO_FIL_EXP },
        { value: StatusConstant.WAIT_VALID_MAN_EXP_QUAL },
        { value: StatusConstant.WAIT_VALID_FIL },
        { value: StatusConstant.WAIT_INFO_FIL_EXP },
        { value: StatusConstant.RESP_INFO_EXP_FIL },
        { value: StatusConstant.VAL_FIL_REJ },
        { value: StatusConstant.FIL_VALIDATED },
        { value: StatusConstant.EXP_VALIDATED },




      ]))
    }

    dataSharingMock = {
      changeStatus: jest.fn().mockImplementation(() => { }),
      lockBtnStatus: new BehaviorSubject(false),
      triggerCommunication: new Subject()

    }
    valuationRequestWrkflMock = {
      getValuationRequestStatus: jest.fn().mockReturnValue(of({ status: { value: StatusConstant.WAIT_INFO_EXP_CA } } as ValuationRequestWorkflowModel)),
      getValuationRequestHistory: jest.fn().mockReturnValue(of([{ status: { value: StatusConstant.WAIT_INFO_EXP_CA } } as ValuationRequestWorkflowModel])),
      saveValuationRequestWorkflow: jest.fn().mockReturnValue(of({ status: { value: StatusConstant.WAIT_INFO_EXP_CA } } as ValuationRequestWorkflowModel)),

    }
    popupServiceMock ={
      popupCommunication: jest.fn().mockReturnValue(of(true)),
      onConfirmCommunication: new Subject(),
      popupInfo: jest.fn().mockReturnValue(of(true))
    }
    valuationSharedServiceMock = TestBed.inject(ValuationSharedService);

    spyOn(window, "scrollTo").and.returnValue(true);

    fixture = new RequestComponent(
      formBuilderMock,
      modalServiceMock,
      entrepriseInfoServiceMock,
      authServiceMock, routeMock, valuationRequestServiceMock,
      valuationRequestWrkflMock,
      paramSettingServiceMock, popupServiceMock, dataSharingMock,
      routerMock,valuationSharedServiceMock
    );
    fixture.ngOnInit();

  });

  describe('Test ngOninit', () => {
    it('should initialize form', () => {

      routeMock = {
        snapshot: {
          params: {
          },
        }
      }
      fixture = new RequestComponent(
        formBuilderMock,
        modalServiceMock,
        entrepriseInfoServiceMock,
        authServiceMock, routeMock, valuationRequestServiceMock,
        valuationRequestWrkflMock,
        paramSettingServiceMock, popupServiceMock, dataSharingMock,
        routerMock, valuationSharedServiceMock
      );
      fixture.ngOnInit();
      const form = {
        clientInfo: null,
        valuationRequest: null
      };

      expect(fixture.requestForm.value).toEqual(form);
    });


    it('should initialize form', () => {
      const form = {
        clientInfo: null,
        valuationRequest: null
      };
      dataSharingMock.triggerCommunication.next(true);

      expect(fixture.requestForm.value).toEqual(form);
    });
    it('should enter else bloc set data sharing status to false ', () => {

      spyOn(valuationRequestWrkflMock, 'getValuationRequestStatus').and.returnValue(of({ status: { value: StatusConstant.WAIT_EXP_VAL } } as ValuationRequestWorkflowModel))
      fixture.ngOnInit();

      expect(fixture.valuationRequestStatus).toEqual(StatusConstant.WAIT_EXP_VAL);

      expect(dataSharingMock.changeStatus).toHaveBeenCalled();
    });
  });

  describe('Test resetForm', () => {
    it('should reset form', () => {
      fixture.resetForm();
      expect(fixture.requestForm.value).toEqual({
        clientInfo: null,
        valuationRequest: null
      });

    });
  });

  describe('Test submit', () => {
    it('should not  send data ', () => {
      fixture.requestForm.controls['clientInfo'].setErrors({ 'incorrect': true });
      expect(fixture.submit()).toEqual(undefined);


    });

    it('should send data ', () => {

      let valuationRequestModel = { files: {} as File ,plans:{plans:[]}};
      fixture.requestForm.controls['valuationRequest'].setValue(valuationRequestModel);
      fixture.requestForm.enable()
      fixture.submit();
      expect(modalServiceMock.open).toHaveBeenCalled();

    });
    it('should quit function  ', () => {
      fixture.requestForm.disable();
      fixture.submit();
    });
  });

  describe('Test Component', () => {
    it('should Be Truthy', () => {
      expect(fixture).toBeTruthy();

    });
  });
  describe('Test:  changeClientInfoLoaded', () => {
    it('should set  clientInfoLoaded true', () => {
      fixture.changeClientInfoLoaded();
      expect(fixture.clientInfoLoaded).toEqual(true);
    });

  });

  describe('Test:  triggerCommunication', () => {
    it('should call saveValuationRequestWorkflow ', () => {
    fixture.statusList = [{ id: '22', label: "repondre à la demande d'information", value: StatusConstant.RESP_INFO_CA_EXP } as ParamSettingModel]

      popupServiceMock.onConfirmCommunication.next({
          theme: {
              Specs: [{ action: StatusConstant.RESP_INFO_CA_EXP }]
          }
      })

      fixture.triggerCommunication(null);

      expect(valuationRequestWrkflMock.saveValuationRequestWorkflow).toHaveBeenCalled;
    });

  });
  describe('Test ngOnDestroy', () => {

    it('should unsubscribe', () => {
      fixture.ngOnDestroy();
      expect(dataSharingMock.changeStatus).toHaveBeenCalled();
    });
  });

  describe('Test duplicate',()=>{
    it('should store valuation', () => {

      let valuationRequestModel = { files: null,plans:null,financingAmount:4500};
      fixture.requestForm.controls['valuationRequest'].setValue(valuationRequestModel);
      fixture.dupliquerRequest();
      expect(valuationSharedServiceMock.getValuationRequest()).toEqual(fixture.getValuationRequestValue());

    });

    it('should store financial plan with product', () => {

      let valuationRequestModel = { files:{files :[ {} as File ]},plans:{plans:[{id:'55',paramProductFamilyId:'4'}]},financingAmount:4500};
      fixture.requestForm.controls['valuationRequest'].setValue(valuationRequestModel);
      fixture.dupliquerRequest();
      expect(valuationSharedServiceMock.getValuationRequest()).toEqual(fixture.getValuationRequestValue());

    });
    it('should store financial plan with profile', () => {

      let valuationRequestModel = { files:{files :[ {} as File ]},plans:{plans:[{id:'55',paramAmortizationProfileId:'4'}]},financingAmount:4500};
      fixture.requestForm.controls['valuationRequest'].setValue(valuationRequestModel);
      fixture.dupliquerRequest();
      expect(valuationSharedServiceMock.getValuationRequest()).toEqual(fixture.getValuationRequestValue());

    });
  })
});

