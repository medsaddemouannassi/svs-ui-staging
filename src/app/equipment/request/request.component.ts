import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { forkJoin, of, Subscription } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ErrorModalComponent } from 'src/app/shared/error-modal/error-modal.component';
import { getThemesByRoleAndStatusAndAction } from '../../shared/communication-themes';
import { ConfirmationModalComponent } from '../../shared/confirmation-modal/confirmation-modal.component';
import { FinancialPlanModel } from '../../shared/models/financial-plan.model';
import { ParamAmortizationProfileModel } from '../../shared/models/param-amortization-profile.model';
import { ParamProductFamilyModel } from '../../shared/models/param-product-family.model';
import { ParamSettingModel } from '../../shared/models/param-setting.model';
import { RefUnderwriterModel } from '../../shared/models/ref-underwriter.model';
import { Role } from '../../shared/models/roles.enum';
import { ValuationRequestWorkflowModel } from '../../shared/models/valuation-request-workflow.model';
import { ValuationRequestModel } from '../../shared/models/valuation-request.model';
import { PagesConstants } from '../../shared/pages-constant';
import { AuthService } from '../../shared/services/auth.service';
import { EntrepriseInfoService } from "../../shared/services/entreprise-info.service";
import { ParamSettingService } from '../../shared/services/param-setting.service';
import { PopupService } from '../../shared/services/popup.service';
import { SharedCommunicationDataService } from '../../shared/services/shared-communication-data.service';
import { ValuationRequestWorkflowService } from '../../shared/services/valuation-request-workflow.service';
import { ValuationSharedService } from '../../shared/services/valuation-shared-service';
import { ValuationRequestService } from '../../shared/services/valuation.request.service';
import { StatusConstant } from '../../shared/status-constant';
import { SummaryModalComponent } from '../summary-modal/summary-modal.component';

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.css']
})
export class RequestComponent implements OnInit, OnDestroy {
  requestForm: FormGroup;
  clientInfoLoaded: boolean = false;
  communicationSubscription: Subscription;

  constructor(
    private fb: FormBuilder,
    private modalService: NgbModal,
    public entrepriseInfoService: EntrepriseInfoService,
    private authService: AuthService,
    private route: ActivatedRoute,
    private valuationRequestService: ValuationRequestService,
    private valuationRequestWrkfl: ValuationRequestWorkflowService,
    private paramSettingService: ParamSettingService,
    private popupService: PopupService,
    private dataSharing: SharedCommunicationDataService,
    private router: Router,
    private valuationSharedService: ValuationSharedService,
  ) {
  }

  idRequest;
  valuationRequest: ValuationRequestModel;
  valuationRequestStatus: string;
  statusList: ParamSettingModel[];
  valuationRequestHistory: ValuationRequestWorkflowModel[]


  ngOnInit(): void {
    window.scrollTo(0, 0);
    this.dataSharing.changeStatus("true");

    this.requestForm = this.fb.group({
      clientInfo: [],
      valuationRequest: []
    });
    this.idRequest = this.route.snapshot.params.idRequest;
    if (this.idRequest) {

      this.requestForm.disable();

      this.dataSharing.triggerCommunication.subscribe(() => {

        this.triggerCommunication(null);

      })

      this.paramSettingService.getParamByCode(StatusConstant.STATUS_VAL_WKFL).subscribe(res => {
        this.statusList = res;
      })
      forkJoin(
        [
          this.valuationRequestWrkfl.getValuationRequestStatus(this.idRequest).pipe(catchError(error => of(null))),
          this.valuationRequestWrkfl.getValuationRequestHistory(this.idRequest).pipe(catchError(error => of(null))),
          this.valuationRequestService.getValuationRequestById(this.idRequest).pipe(catchError(error => of(null)))
        ]).subscribe(res => {
          this.valuationRequestStatus = res[0] ? res[0].status.value : null;
          this.valuationRequestHistory = res[1];
          this.valuationRequest = res[2];
          let theme =null;
          if (this.valuationRequestStatus && this.valuationRequestStatus == StatusConstant.WAIT_INFO_EXP_CA && this.authService.currentUserRolesList.includes(Role.CA)) {
            theme = getThemesByRoleAndStatusAndAction(this.authService.currentUserRolesList as Role[], this.valuationRequestStatus, this.valuationRequestHistory[0].status.value, PagesConstants.REQUEST, StatusConstant.RESP_INFO_CA_EXP)

          } 
          if (this.valuationRequestStatus && this.valuationRequestStatus == StatusConstant.REJECTED && this.authService.currentUserRolesList.includes(Role.CA)) {
            theme = getThemesByRoleAndStatusAndAction(this.authService.currentUserRolesList as Role[], this.valuationRequestStatus, this.valuationRequestHistory[0].status.value, PagesConstants.REQUEST, null)

          } 
          if(theme){
            this.triggerCommunication(theme);

          }else {
            this.dataSharing.changeStatus("true");

          }
        })

    }


  }


  triggerCommunication(initialTheme) {
    this.popupService.popupCommunication(this.authService.currentUserRolesList, PagesConstants.REQUEST, this.valuationRequestStatus, this.valuationRequestHistory[0].status.value, this.valuationRequestHistory, initialTheme);



    let subscription = this.popupService.onConfirmCommunication.subscribe(data => {

      if (data) {

        let task = {} as ValuationRequestWorkflowModel;

        task.status = this.statusList.find(status => status.value == data.theme.Specs[0].action);
        task.valuationRequest = this.valuationRequest;
        task.comment = data.comment;
        this.valuationRequestWrkfl.saveValuationRequestWorkflow(task).subscribe(response => {

          if (task.status.value == StatusConstant.RESP_INFO_CA_EXP) {
            this.popupService.popupInfo("Votre réponse a bien été envoyée.", 'Fermer', null, null, ConfirmationModalComponent);

            let waitTask = {} as ValuationRequestWorkflowModel;

            waitTask.status = this.statusList.find(status => status.value == StatusConstant.WAIT_EXP_VAL);
            waitTask.valuationRequest = this.valuationRequest;

            this.valuationRequestWrkfl.saveValuationRequestWorkflow(waitTask).subscribe(newTask => {

              this.valuationRequestStatus = newTask.status.value;
              this.requestForm.disable();
            })
          }
        })
      }


      subscription.unsubscribe();

    })
  }

  changeClientInfoLoaded() {
    this.clientInfoLoaded = true;
  }


  submit() {
    if (this.requestForm.invalid) {

      const modal = this.modalService.open(ErrorModalComponent,
        {
          scrollable: false,
          windowClass: "errorModalClass",
          centered: true
        }
      );
      modal.componentInstance.content = 'Merci de bien vouloir renseigner l\'ensemble des champs nécessaires';
      return;
    }

    if (this.requestForm.getRawValue().valuationRequest && this.requestForm.getRawValue().valuationRequest.plans.plans.length > 6) {

      const modal = this.modalService.open(ErrorModalComponent,
        {
          scrollable: false,
          windowClass: "errorModalClass",
          centered: true
        }
      );
      modal.componentInstance.content = 'Vous ne pouvez avoir que six plans de financement maximum';
      return;
    }

    if (this.requestForm.disabled) {
      return;
    }
    const modalRef = this.modalService.open(SummaryModalComponent,
      {
        scrollable: true,
        windowClass: "summaryModalClass",
        centered: true,
        backdrop: 'static',
        keyboard: false
      }
    );

    let valuationRequestModel = this.getValuationRequestValue();
    modalRef.componentInstance.valuationRequest = valuationRequestModel;
    modalRef.componentInstance.financialPlanRequest = this.requestForm.getRawValue().valuationRequest.plans;
    modalRef.componentInstance.documentsListToEdit = this.requestForm.value.valuationRequest.documentsListToEdit;
    modalRef.componentInstance.documentsListToDelete = this.requestForm.value.valuationRequest.documentsListToDelete;

    modalRef.componentInstance.isAddMode = (valuationRequestModel.id) ? false : true;
    if (this.requestForm.value.valuationRequest.files != null)
      modalRef.componentInstance.files = this.requestForm.value.valuationRequest.files.files;
    // set refUnderwiterModel

    let refUnderwriteModel: RefUnderwriterModel = {
      id: (this.entrepriseInfoService.entrepriseInfo.isFromRet) ? this.entrepriseInfoService.entrepriseInfo.idRet : this.entrepriseInfoService.entrepriseInfo.cp,
      isRetUnderwriter: this.entrepriseInfoService.entrepriseInfo.isFromRet,
      nsiren: this.entrepriseInfoService.entrepriseInfo.siren,
      siret: '',
      mainUnderwId: this.entrepriseInfoService.entrepriseInfo.mainUnderwId,
      mainUnderwName: this.entrepriseInfoService.entrepriseInfo.mainUnderwName,
      legalName: (this.entrepriseInfoService.entrepriseInfo.socialReason)
    } as RefUnderwriterModel


    modalRef.componentInstance.refUnderwrite = refUnderwriteModel;

    modalRef.componentInstance.onExit.subscribe((t) => {
      if(t){
        this.requestForm.disable();

      }
    });

  }

  resetForm() {
    this.requestForm.reset();
  }

  ngOnDestroy() {
    this.dataSharing.changeStatus("false");
    this.modalService.dismissAll(SummaryModalComponent);
    if (this.communicationSubscription) {
      this.communicationSubscription.unsubscribe();
    }
  }
  dupliquerRequest() {
    // stock information déja saisie dans shared service



    this.valuationSharedService.setIsFromRequest(true);
    this.valuationSharedService.setValuationRequest(this.getValuationRequestValue());
    

    if (this.requestForm.value.valuationRequest.plans && this.requestForm.value.valuationRequest.plans.plans) {
      let plans = this.requestForm.value.valuationRequest.plans.plans

      plans.forEach(plan => {

        if (plan.paramAmortizationProfileId) {
          let paramAmortizationProfile: ParamAmortizationProfileModel = {} as ParamAmortizationProfileModel;
          paramAmortizationProfile.id = plan.paramAmortizationProfileId;

          plan.paramAmortizationProfile = paramAmortizationProfile;
        }


        if (plan.paramProductFamilyId) {
          let paramProductFamily: ParamProductFamilyModel = {} as ParamProductFamilyModel;
          paramProductFamily.id = plan.paramProductFamilyId;

          plan.paramProductFamily = paramProductFamily;
        }


      });
      this.valuationSharedService.setFinancialPlans(plans);
    }else{
      this.valuationSharedService.setFinancialPlans([{} as FinancialPlanModel]);
    }
    if (this.requestForm.value.valuationRequest.files != null) {
      this.valuationSharedService.setDocsToAdd(this.requestForm.value.valuationRequest.files);

    }
    // information client
    this.valuationSharedService.setIdRefTiers(this.route.snapshot.paramMap.get('idRefTiers'));
    this.valuationSharedService.setIdBcp(this.route.snapshot.paramMap.get('idBcp'));



    // redirection to search
    this.router.navigate(['/search'])

  }
  undoDupliquationRequest(){
    this.valuationSharedService.setIsFromRequest(false);

    this.valuationSharedService.undoRequestDuplicationSubject.next(true);
    
  }


  getValuationRequestValue() {
    let valuationRequestModel: any;
    valuationRequestModel = this.requestForm.value.valuationRequest;
    valuationRequestModel.externalSystemRef = 'CRM';
    valuationRequestModel.bssResponsible = this.authService.identityClaims['mat'];
    valuationRequestModel.underwriterId = (this.entrepriseInfoService.entrepriseInfo.isFromRet) ? this.entrepriseInfoService.entrepriseInfo.idRet : this.entrepriseInfoService.entrepriseInfo.cp;
    valuationRequestModel.isRetUnderwriter = this.entrepriseInfoService.entrepriseInfo.isFromRet;
    valuationRequestModel.isRetPmUnderwriter = this.entrepriseInfoService.entrepriseInfo.isPmFromRet;
    valuationRequestModel.isANewAsset = (valuationRequestModel.isAOldAsset == null || !valuationRequestModel.isAOldAsset) ? true : false;
    valuationRequestModel.isAssetUseAbroad = (valuationRequestModel.isAssetUseAbroad == null || !valuationRequestModel.isAssetUseAbroad) ? false : true;
    valuationRequestModel.isLicencePlateAvaileble = (valuationRequestModel.isLicencePlateAvaileble == null || !valuationRequestModel.isLicencePlateAvaileble) ? false : true;

    return valuationRequestModel;
  }

}
