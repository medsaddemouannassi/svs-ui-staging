import { TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { of, Subscription } from 'rxjs';
import { DocumentsComponent } from '../../../shared/documents/documents.component';
import { FinancialPlanModel } from '../../../shared/models/financial-plan.model';
import { ValuationDocumentModel } from '../../../shared/models/valuationDocument.model';
import { ValuationSharedService } from '../../../shared/services/valuation-shared-service';
import { ValuationRequestComponent } from "./valuation-request.component";
import { DomSanitizer } from '@angular/platform-browser';


describe('ValuationRequestComponent', () => {
  let fixture: ValuationRequestComponent;
  let formBuilderMock: FormBuilder;
  let paramEnergyServiceMock: any;
  let paramUsageUnitServiceMock: any;
  let listParamEnergy;
  let listParamUsageUnit;
  let subscriptionMock: Subscription;
  let popupServiceMock;
  let entrepriseInfoServiceMock: any;
  let financialPlanServiceMock: any;
  let valuationRequestDocsServiceMock: any;
  let documentsList: any
  let valuationRequest: any;
  let valuationSharedServiceMock:any;
  let sanitizer :any;

  beforeEach(() => {
    financialPlanServiceMock = {
      loadFinancialPlanByValuationRequestId: jest.fn().mockReturnValue(of([{ valuationRequest: valuationRequest } as FinancialPlanModel])),
      loadFinancialPlanByValuationId: jest.fn().mockReturnValue(of([{ valuationRequest: valuationRequest } as FinancialPlanModel]))
    }
    valuationRequest = {
      id: '5',
      externalSystemRef: 'S',
      underwriterId: '1',
      isRetUnderwriter: true,
      customerPropoId: '1',
      bssResponsible: '1',
      requestComment: 'comment',
      financingAmount: 450,
      assetCreationDate: new Date(),
      assetUsVolume: 50,
      collAssetEstimYear: 2020,
      co2Emission: 15,
      fiscPower: 7,
      isAssetUseAbroad: false,
      isANewAsset: true,
      isLicencePlateAvaileble: true,
      paramEnergy: null,
      paramUsageUnit: null,
      unitaryQuantity: 2,
      bssResponsibleFullName: 'name'
    }

      valuationRequestDocsServiceMock = {
        getBssRespDocsByValReqId: jest.fn().mockReturnValue(of(documentsList)),
        displayDocument: jest.fn().mockReturnValue(of({ url: 'url' }))
      }

    popupServiceMock = {
      popupInfo: jest.fn().mockReturnValue(of(true)),
      onConfirm: of(true)
    };

    documentsList = [{

      "docName": "document1",
    } as ValuationDocumentModel, {

      "docName": "document2"
    } as ValuationDocumentModel]

    subscriptionMock = new Subscription();

    listParamEnergy = [{ id: '1', label: 'Biocarburant' },
    { id: '2', label: 'Gaz' },
    { id: '3', label: 'Diesel' },
    { id: '4', label: 'Essence' },
    { id: '5', label: 'Electrique' },
    { id: '6', label: 'Hydrogène' },
    { id: '7', label: 'Hybride' }];

    listParamUsageUnit = [{ id: '1', unitLabel: 'Kilomètres' },
    { id: '2', unitLabel: 'Heures' },
    { id: '3', unitLabel: 'Nombre de tours' }];

    formBuilderMock = new FormBuilder();

    paramEnergyServiceMock = {
      getAllParamEnergy: jest.fn().mockReturnValue(of(listParamEnergy))

    }
    paramUsageUnitServiceMock = {
      getAllParamUsageUnit: jest.fn().mockReturnValue(of(listParamUsageUnit))
    }
    entrepriseInfoServiceMock = {
      contrepartieChanged: of('contrepartieTest')

    }

    valuationSharedServiceMock = TestBed.inject(ValuationSharedService);

    sanitizer = TestBed.inject(DomSanitizer);


    fixture = new ValuationRequestComponent(
      formBuilderMock, paramEnergyServiceMock, paramUsageUnitServiceMock,
      entrepriseInfoServiceMock, financialPlanServiceMock, valuationRequestDocsServiceMock, popupServiceMock, valuationSharedServiceMock
    ,sanitizer);

    fixture.documentComponent = new DocumentsComponent(formBuilderMock, popupServiceMock);
    fixture.ngOnInit();
  });

  describe('Test: ngOnInit', () => {
    it('should initialize form', () => {

      const form = {
        financingAmount: '',
        mainUnderw: 'contrepartieTest',
        plans: null,
        unitaryQuantity: { value: '' },
        paramEnergy: '',
        isLicencePlateAvaileble: '',
        isAOldAsset: '',
        isAssetUseAbroad: '',
        collAssetEstimYear: '',
        assetUsVolume: '',
        paramUsageUnit: '',
        requestComment: '',
        files: [],
        documentsListToEdit: [],
        documentsListToDelete: [],
        id: ''
      };

      expect(fixture.form.getRawValue()).toEqual(form);

    });

    it('should fill list', () => {

      expect(fixture.energyOptions).toEqual(listParamEnergy);
      expect(fixture.paramUsageUnitOptions).toEqual(listParamUsageUnit);


    });
    it('shoud test duplicate ', ()=>{
      fixture.valuationSharedService.getIsFromRequest= jest.fn().mockReturnValue(true);
      fixture.ngOnInit();
      expect(financialPlanServiceMock.loadFinancialPlanByValuationId.toHaveBeenCalled);
      
    });
  });

  describe('Test ngOnDestroy', () => {
    it('should unsubscribe', () => {
      fixture.ngOnDestroy();
      const spyunsubscribe = jest.spyOn(subscriptionMock, 'unsubscribe');
      expect(spyunsubscribe).toBeDefined();
    });
  });

  describe('Test set valuationRequestModel', () => {
    it('should set form', () => {
      fixture.valuationRequestModel = valuationRequest;
      expect(fixture.form.value.id).toEqual('5');

    });
  });



  describe('Test registerOnChange', () => {
    it('should change value', () => {
      let test;
      fixture.registerOnChange(test);
      expect(fixture.onChange).toEqual(test);
    });
  });

  describe('Test writeValue', () => {
    it('should  write value', () => {
      let value = {
        financingAmount: '',
        mainUnderw: '',
        plans: null,
        unitaryQuantity: { value: '' },
        paramEnergy: '',
        isLicencePlateAvaileble: '',
        isAOldAsset: '',
        isAssetUseAbroad: '',
        collAssetEstimYear: '',
        assetUsVolume: '',
        paramUsageUnit: '',
        requestComment: '',
        files: [],
        documentsListToEdit: [],
        documentsListToDelete: [],
        id: ''
      };

      fixture.writeValue(value);
      expect(fixture.value).toEqual(value);



    });
    it('should not write value', () => {
      let value = ({
        financingAmount: '',
        mainUnderw: { value: '', disabled: true },
        plans: null,
        unitaryQuantity: { value: '' },
        paramEnergy: '',
        isLicencePlateAvaileble: '',
        isAOldAsset: '',
        isAssetUseAbroad: '',
        collAssetEstimYear: '',
        assetUsVolume: '',
        paramUsageUnit: '',
        requestComment: '',
        files: [],
        documentsListToEdit: [],
        documentsListToDelete: [],
        id: ''
      });

      fixture.writeValue(value);
      value = null;
      fixture.writeValue(value);
      expect(fixture.value).not.toEqual(value);


    });
  });


  describe('Test registerOnTouched', () => {
    it('should change onTouched ', () => {
      fixture.registerOnTouched(test);
      expect(fixture.onTouched).not.toEqual(() => {
        // This is intentional
      });
      expect(fixture.onTouched).toEqual(test);

    });
  });

  describe('Test validate', () => {
    it('should retrun null', () => {

      let value = ({
        financingAmount: 55,
        mainUnderw: '',
        plans: null,
        unitaryQuantity: { value: '' },
        paramEnergy: listParamEnergy[0],
        isLicencePlateAvaileble: '',
        isAOldAsset: '',
        isAssetUseAbroad: '',
        collAssetEstimYear: '',
        assetUsVolume: '',
        paramUsageUnit: '',
        requestComment: 'hello',
        files: [{ name: 'file3.png', size: 17567 } as File],
        documentsListToEdit: [],
        documentsListToDelete: [],
        id: ''
      });
      let form;
      fixture.writeValue(value);
      expect(fixture.validate(form)).toEqual(null);

    });

    it('should not retrun null', () => {
      let form: any;
      fixture.form.controls['requestComment'].setErrors({ 'incorrect': true });
      expect(fixture.validate(form)).toEqual({ "valuationRequest": { "valid": false } });
    });
  });


  describe('Test submit', () => {
    it('should emit submitEvent', () => {
      spyOn(fixture.documentComponent, 'setFormTouched');
      fixture.form.controls['requestComment'].setErrors({ 'incorrect': true });
      fixture.submit();
      expect(fixture.submitEvent.emit).toBeDefined();


      let value = ({
        financingAmount: 55,
        mainUnderw: '',
        plans: null,
        unitaryQuantity: { value: '' },
        paramEnergy: listParamEnergy[0],
        isLicencePlateAvaileble: '',
        isAOldAsset: '',
        isAssetUseAbroad: '',
        collAssetEstimYear: '',
        assetUsVolume: '',
        paramUsageUnit: '',
        requestComment: 'hello',
        files: [],
        documentsListToEdit: [],
        documentsListToDelete: [],
        id: ''
      });
      fixture.writeValue(value);
      fixture.submit();
      expect(fixture.submitEvent.emit).toBeDefined();
    });

  });

  describe('Test isPriceInputValid', () => {
    it('should change value', () => {
      let test;
      fixture.isPriceInputValid(test);
      expect(fixture.isPriceIncorrect).toEqual(test);

    });
  });

  describe('Test onBlurPrice', () => {
    it('should isPriceIncorrect be true', () => {
      let value = ({
        financingAmount: null,
        mainUnderw: '',
        plans: null,
        unitaryQuantity: { value: '' },
        paramEnergy: listParamEnergy[0],
        isLicencePlateAvaileble: '',
        isAOldAsset: '',
        isAssetUseAbroad: '',
        collAssetEstimYear: '',
        assetUsVolume: '',
        paramUsageUnit: '',
        requestComment: 'hello',
        files: [],
        documentsListToEdit: [],
        documentsListToDelete: [],
        id: ''
      });
      fixture.writeValue(value);
      fixture.onBlurPrice();
      expect(fixture.isPriceIncorrect).toBeTruthy();

    });

    it('should isPriceIncorrect be true', () => {
      let value = ({

        financingAmount: "55",
        mainUnderw: '',
        plans: null,
        unitaryQuantity: { value: '' },
        paramEnergy: listParamEnergy[0],
        isLicencePlateAvaileble: '',
        isAOldAsset: '',
        isAssetUseAbroad: '',
        collAssetEstimYear: '',
        assetUsVolume: '',
        paramUsageUnit: '',
        requestComment: 'hello',
        files: [],
        documentsListToEdit: [],
        documentsListToDelete: [],
        id: ''
      });
      fixture.writeValue(value);
      fixture.onBlurPrice();
      expect(fixture.isPriceIncorrect).toBeFalsy();

    });
  });


  describe('Test isAOldAsset', () => {
    it('should get value when isAOldAsset is false', () => {


      let value = ({
        financingAmount: 55,
        mainUnderw: '',
        plans: null,
        unitaryQuantity: { value: '' },
        paramEnergy: listParamEnergy[0],
        isLicencePlateAvaileble: '',
        isAOldAsset: false,
        isAssetUseAbroad: '',
        collAssetEstimYear: '',
        assetUsVolume: '',
        paramUsageUnit: '',
        requestComment: 'hello',
        files: [],
        documentsListToEdit: [],
        documentsListToDelete: [],
        id: ''
      });
      fixture.writeValue(value);
      expect(fixture.isAOldAsset).toBeFalsy()
    });

    it('should get value when isAOldAsset is true', () => {


      let value = ({
        financingAmount: 55,
        mainUnderw: '',
        plans: null,
        unitaryQuantity: { value: '' },
        paramEnergy: listParamEnergy[0],
        isLicencePlateAvaileble: '',
        isAOldAsset: true,
        isAssetUseAbroad: '',
        collAssetEstimYear: '',
        assetUsVolume: '',
        paramUsageUnit: '',
        requestComment: 'hello',
        files: [],
        documentsListToEdit: [],
        documentsListToDelete: [],
        id: ''
      });
      fixture.writeValue(value);
      expect(fixture.isAOldAsset).toBeTruthy()
    });
  });

  describe('Test Component', () => {
    it('should Be Truthy', () => {
      expect(fixture).toBeTruthy();

    });
  });

  describe('Test f', () => {
    it('should return controls', () => {
      expect(fixture.f).toEqual(fixture.form.controls);

    });
  });

  describe('Test editDocumentName', () => {
    it('should Be Truthy', () => {
      fixture.documents = documentsList;
      fixture.editedDocumentList = [false, true];
      fixture.editDocumentName(0);
      expect(fixture.editedDocumentList[0]).toBeTruthy();


    });
  });


  describe('Test onBlurDocument', () => {
    it('should Be Falsy', () => {
      fixture.documents = documentsList;
      fixture.editedDocumentList = [true, true];
      fixture.onBlurDocument(0);
      expect(fixture.editedDocumentList[0]).toBeFalsy();


    });
  });

  describe('Test deleteDocument', () => {
    it('should set value into documentsListToDelete', () => {
      fixture.documents = documentsList;
      fixture.editedDocumentList = [true, true];
      fixture.form.patchValue({ documentsListToDelete: null });

      fixture.deleteDocument(0);


      expect(popupServiceMock.popupInfo).toHaveBeenCalled();
      popupServiceMock.onConfirm.subscribe((t) => {
        expect(fixture.form.value.documentsListToDelete.length).toEqual(1);
      })

    });
  });

  describe('Test displayDocument', () => {
    it('should display doc ', () => {
      fixture.documents = documentsList;


      fixture.displayDocument(1)
      expect(valuationRequestDocsServiceMock.displayDocument).toHaveBeenCalled();
      valuationRequestDocsServiceMock.displayDocument(1).subscribe(res => {
        expect(popupServiceMock.popupPDF).toHaveBeenCalled()
      })
    })
  });

  describe('Test setFormValue', () => {
    it('should set value in form ', () => {
      fixture.valuationRequest = valuationRequest

      fixture.setFormValue()

      expect(fixture.form.value.id).toEqual('5');

    })
  });

  describe('Test setDisabledState', () => {
    it('should lock form', () => {
      fixture.setDisabledState(true);
      expect(fixture.form.disable).toHaveBeenCalled
    });

    it('should unlock form', () => {
        fixture.setDisabledState(false);
        expect(fixture.form.enable).toHaveBeenCalled
      });

  });

});

