import { Component, EventEmitter, forwardRef, Input, OnDestroy, OnInit, Output, ViewChild, SecurityContext } from '@angular/core';
import { AbstractControl, ControlValueAccessor, FormBuilder, FormControl, FormGroup, NG_VALIDATORS, NG_VALUE_ACCESSOR, Validators } from '@angular/forms';
import { forkJoin, of, Subscription } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { markFormGroupTouched } from 'src/app/shared/utils';
import { ConfirmationModalComponent } from '../../../shared/confirmation-modal/confirmation-modal.component';
import { documentsValidator } from '../../../shared/documents/document-validator';
import { DocumentsComponent } from '../../../shared/documents/documents.component';
import { ParamEnergyModel } from '../../../shared/models/param-energy.model';
import { ParamUsageUnitModel } from '../../../shared/models/param-usage-unit.model';
import { ValuationRequestModel } from '../../../shared/models/valuation-request.model';
import { ValuationDocumentModel } from '../../../shared/models/valuationDocument.model';
import { EntrepriseInfoService } from '../../../shared/services/entreprise-info.service';
import { FinancialPlanService } from '../../../shared/services/financial-plan.service';
import { ParamEnergyService } from '../../../shared/services/param-energy.servie';
import { ParamUsageUnitService } from '../../../shared/services/param-usage-unit';
import { PopupService } from '../../../shared/services/popup.service';
import { ValuationRequestDocsService } from '../../../shared/services/valuation-request-docs.service';
import { ValuationSharedService } from '../../../shared/services/valuation-shared-service';
import { twoDigitDecimalRound } from '../../../shared/ValuationUtils';
import { PlanListComponent } from '../../plan-list/plan-list.component';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-valuation-request',
  templateUrl: './valuation-request.component.html',
  styleUrls: ['./valuation-request.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ValuationRequestComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => ValuationRequestComponent),
      multi: true
    }
  ]
})
export class ValuationRequestComponent implements OnInit, ControlValueAccessor, OnDestroy {

  @ViewChild(DocumentsComponent) documentComponent: DocumentsComponent;

  get value(): any {
    return this.form.getRawValue();
  }

  set value(value) {   
    this.form.setValue(value);
    this.onChange(value);
    this.onTouched();
  }

  get isAOldAsset(): any {
    if (this.form.get('isAOldAsset').value) {
      this.form.controls['collAssetEstimYear'].setValidators([Validators.required]);
      this.form.controls['assetUsVolume'].setValidators([Validators.required]);
      this.form.controls['paramUsageUnit'].setValidators([Validators.required]);
    }
    else {
      this.form.controls['collAssetEstimYear'].setValidators([]);
      this.form.controls['assetUsVolume'].setValidators([]);
      this.form.controls['paramUsageUnit'].setValidators([]);
    }
    this.form.controls['collAssetEstimYear'].updateValueAndValidity();
    this.form.controls['assetUsVolume'].updateValueAndValidity();
    this.form.controls['paramUsageUnit'].updateValueAndValidity();
    return this.form.get('isAOldAsset').value;
  }

  get f(): {
    [key: string]: AbstractControl;
  } {
    return this.form.controls;
  }


  constructor(private fb: FormBuilder,
    private paramEnergyService: ParamEnergyService,
    private paramUsageUnitService: ParamUsageUnitService,
    private entrepriseInfoService: EntrepriseInfoService,
    private financialPlanService: FinancialPlanService,
    private valuationRequestDocsService: ValuationRequestDocsService,
    private popupService: PopupService,
    public valuationSharedService: ValuationSharedService,
    private sanitizer: DomSanitizer


  ) { }

  form: FormGroup;
  subscriptions: Subscription[] = [];
  subscription: Subscription;
  energyOptions: ParamEnergyModel[] = [];
  paramUsageUnitOptions: ParamUsageUnitModel[] = [];
  affiliateValue: boolean;
  valuationRequest: ValuationRequestModel;
  financialPlan: any;
  displayOnly: boolean = false;
  documents: ValuationDocumentModel[];
  editedDocumentList = [];
  documentName: string;

  @ViewChild(PlanListComponent) plansListComponent: PlanListComponent;
  @Output() submitEvent = new EventEmitter();
  @Input() set valuationRequestModel(valuationRequest: ValuationRequestModel) {
    if (valuationRequest) {

      this.valuationRequest = valuationRequest;

      this.setFormValue();
      this.financialPlanService.loadFinancialPlanByValuationRequestId(valuationRequest.id).subscribe(data => {
        this.initPlansForm(data);
      });


    }
  }
  initPlansForm(data) {
    if (data) {
      let planList = [];

      data.forEach(element => {
        let plan = {
          planDuration: element.planDuration,
          planContrbAmount: element.planContrbAmount ? twoDigitDecimalRound(element.planContrbAmount.toString()) : null,
          remainingAmount: element.remainingAmount ? twoDigitDecimalRound(element.remainingAmount.toString()) : null,
          remainingPercent: element.remainingPercent,
          paramAmortizationProfileId: (element.paramAmortizationProfile) ? element.paramAmortizationProfile.id : null,
          paramProductFamilyId: (element.paramProductFamily) ? element.paramProductFamily.id : null,
          id: (element.id) ? element.id : null,
          isDuplicated: element.isDuplicated

        }

        planList.push(plan);
      })
      if(planList.length > 6){

        this.popupService.popupInfo('Vous avez saisi plus que 6 plans de financement, merci de vérifier', 'Confirmer', null, null, ConfirmationModalComponent);

      }

      this.financialPlan = {
        plans: planList
      }
      if (this.valuationRequest && this.valuationRequest.id != null) {

        this.displayOnly = true;

      }

    }
  }
  isPriceIncorrect: boolean;

  ngOnInit(): void {

    this.form = this.fb.group({
      financingAmount: ['', Validators.required],
      mainUnderw: { value: '', disabled: true },
      plans: [],
      unitaryQuantity: { value: '' },
      paramEnergy: ['', Validators.required],
      isLicencePlateAvaileble: [''],
      isAOldAsset: [''],
      isAssetUseAbroad: [''],
      collAssetEstimYear: [''],
      assetUsVolume: [''],
      paramUsageUnit: [''],
      requestComment: ['', Validators.required],
      files: [[], { validators: [documentsValidator] }],
      documentsListToEdit: [[]],
      documentsListToDelete: [[]],
      id: ['']
    });
    // getContrepartis Value from REt
    // this.form.get('mainUnderw').disable();
    this.subscription = this.entrepriseInfoService.contrepartieChanged
      .subscribe(mainUnderwName => {
        this.form.patchValue({
          mainUnderw: mainUnderwName
        })
      });
    forkJoin(this.paramEnergyService.getAllParamEnergy().pipe(catchError(error => of([]))), this.paramUsageUnitService.getAllParamUsageUnit().pipe(catchError(error => of([])))
    ).subscribe((value: any[]) => {
      this.energyOptions = value[0];
      this.paramUsageUnitOptions = value[1];

    });
    this.subscriptions.push(
      this.form.valueChanges.subscribe(value => {
        this.onChange(value);
        this.onTouched();
      }),

    );

    /**
     * duplication case
     */

    if (this.valuationSharedService.getIsFromRequest()) {


      this.valuationRequest = this.valuationSharedService.getValuationRequest();

      
      this.financialPlanService.loadFinancialPlanByValuationId(this.valuationSharedService.getSelectedValuationId()).subscribe(data => {

        data.forEach(plan => {
          plan.id = null;
          plan.valuation = null;
          plan.valuationRequest = null;
          plan.planEstimatedOut = null;
          plan.isDuplicated = true;
        })



        this.setFormValue();

        if(this.valuationSharedService.getDocsToAdd()){
          this.form.patchValue({ files: this.valuationSharedService.getDocsToAdd() })

        }

        this.initPlansForm(this.valuationSharedService.getFinancialPlans().concat(data));
      });

      this.valuationSharedService.undoRequestDuplicationSubject.subscribe(undo =>{
        if(undo){
          this.valuationRequest = this.valuationSharedService.getValuationRequest();
          this.setFormValue();

          if(this.valuationSharedService.getDocsToAdd()){
            this.form.patchValue({ files: this.valuationSharedService.getDocsToAdd() })
  
          }
        }
      })

    }

  }

  compareFn(item, selected) {
   
    return item.id === selected.id;
  }

  setFormValue() {

    this.form.patchValue({ financingAmount: this.valuationRequest.financingAmount ? twoDigitDecimalRound(this.valuationRequest.financingAmount.toString()) : null })
    this.form.patchValue({ unitaryQuantity: this.valuationRequest.unitaryQuantity })
    this.form.patchValue({ paramEnergy: this.valuationRequest.paramEnergy })
    this.form.patchValue({ isLicencePlateAvaileble: (this.valuationRequest.isLicencePlateAvaileble != null && this.valuationRequest.isLicencePlateAvaileble != undefined) ? this.valuationRequest.isLicencePlateAvaileble : false })
    this.form.patchValue({ isAOldAsset: (this.valuationRequest.isANewAsset != null && this.valuationRequest.isANewAsset != undefined) ? !this.valuationRequest.isANewAsset : false })
    this.form.patchValue({ isAssetUseAbroad: (this.valuationRequest.isAssetUseAbroad != null && this.valuationRequest.isAssetUseAbroad != undefined) ? this.valuationRequest.isAssetUseAbroad : false })
    this.form.patchValue({ collAssetEstimYear: this.valuationRequest.collAssetEstimYear ? this.valuationRequest.collAssetEstimYear : null })
    this.form.patchValue({ assetUsVolume: this.valuationRequest.assetUsVolume ? this.valuationRequest.assetUsVolume : null })
    this.form.patchValue({ paramUsageUnit: this.valuationRequest.paramUsageUnit})
    this.form.patchValue({ requestComment: this.valuationRequest.requestComment ? this.valuationRequest.requestComment : null })
    this.form.patchValue({ id: this.valuationRequest.id ? this.valuationRequest.id : null })



    
    


    if (this.valuationRequest.id) {
      this.valuationRequestDocsService.getBssRespDocsByValReqId(this.valuationRequest.id).
        subscribe((data) => {

          this.documents = data.map(a => { return { ...a } });
          let docs = data.map(a => { return { ...a } });


          this.form.patchValue({ documentsListToEdit: docs });
        }, (err) => { console.error(err) })

    }




  }



  displayDocument(index) {
    this.valuationRequestDocsService.displayDocument(this.documents[index]).subscribe(res => {
      let safeUrl = this.sanitizer.sanitize(SecurityContext.RESOURCE_URL, this.sanitizer.bypassSecurityTrustResourceUrl(res.url));

      window.open(safeUrl, "_blank");    })

  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  onChange: any = () => {
    // This is intentional
  };
  onTouched: any = () => {
    // This is intentional
  };

  registerOnChange(fn): void {
    this.onChange = fn;
  }

  writeValue(value): void {

    if (value) {
      this.value = value;
    }

    if (value === null) {
      this.form.reset();
    }
  }

  registerOnTouched(fn): void {
    this.onTouched = fn;
  }

  validate(_: FormControl): any {
    return this.form.valid ? null : { valuationRequest: { valid: false, }, };
  }



  addNewFundingPlan(): void {
    
    if(this.form.enabled){
      this.plansListComponent.addNewFundingPlan();

    }
  }

  submit(): void {

    this.form.updateValueAndValidity();

    if (this.form.invalid) {

      markFormGroupTouched(this.form);
      this.documentComponent.setFormTouched();

    }
    this.submitEvent.emit();
  }

  isPriceInputValid(response) {
    this.isPriceIncorrect = response;
  }

  onBlurPrice() {

    this.isPriceIncorrect = false;

    if (!this.form.get('financingAmount').value) {
      this.isPriceIncorrect = true;
    } else {
      this.form.get('financingAmount').setValue(twoDigitDecimalRound(this.form.value.financingAmount))

    }

  }
  /*document */
  editDocumentName(index: number) {
    this.editedDocumentList[index] = true;
    this.documentName = this.documents[index].docName;

  }
  onBlurDocument(index: number) {

    this.documents[index].docName = this.documentName;
    this.form.patchValue({ documentsListToEdit: this.documents });

    this.editedDocumentList[index] = false;

  }
  deleteDocument(index: number) {

    this.popupService.popupInfo('Êtes-vous sûr(e) de vouloir supprimer la pièce jointe ?', 'Confirmer', null, 'Annuler', ConfirmationModalComponent);
    const subscription = this.popupService.onConfirm.subscribe((t) => {
      if (t) {
        let deletedDocumentsList = this.form.value.documentsListToDelete;
        if (!deletedDocumentsList) {
          deletedDocumentsList = [];
        }
        deletedDocumentsList.push(this.documents[index])
        this.documents.splice(index, 1)
        this.form.patchValue({ documentsListToEdit: this.documents });
        this.form.patchValue({ documentsListToDelete: deletedDocumentsList });

      }
      subscription.unsubscribe()

    })

  }

  setDisabledState(disabled: boolean) {
    if (disabled) {
      this.form.disable()
    } else {
      this.form.enable()
    }
  }

}
