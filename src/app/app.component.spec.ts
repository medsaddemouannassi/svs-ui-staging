import { of } from 'rxjs';
import { TestBed } from '@angular/core/testing';
import { ValuationSharedService } from './shared/services/valuation-shared-service';
import { AppComponent } from './app.component';
import { LoadingService } from './shared/services/loading-service.service';

describe('app.component.ts', () => {
  let fixture: AppComponent;
  let authServiceMock:any;
  let routerMock: any;
  let svsDescCollabServiceMock: any;
  let dataSharingMock;
  let valuationSharedServiceMock;
  beforeEach(() => {
    valuationSharedServiceMock = TestBed.inject(ValuationSharedService);

    authServiceMock={
      currentUserRolesList:['ROLE_SVS_ADMIN_SE'],
      runInitialLoginSequence:jest.fn().mockReturnValue(Promise.resolve({ data:true }))

    }
    svsDescCollabServiceMock= {
      isDescManager :jest.fn().mockReturnValue(of(true))

    };
    dataSharingMock = {
      currentBtnStatus: {
        subscribe: jest.fn().mockReturnValue(of(true)),
      },
      changeStatus: jest.fn().mockReturnValue(of(true)),
    }

    fixture = new AppComponent(authServiceMock, svsDescCollabServiceMock,valuationSharedServiceMock,routerMock,dataSharingMock,new LoadingService());
  });

  it('should create', () => {
    expect(fixture).toBeTruthy();
  });
  it('should create x', () => {
    authServiceMock={
      currentUserRolesList:['ROLE_SVS_EXPERT_SE'],
      runInitialLoginSequence:jest.fn().mockReturnValue(Promise.resolve({ data:true }))

    }
    svsDescCollabServiceMock= {
      isDescManager :jest.fn().mockReturnValue(of(true))

    };
    fixture = new AppComponent(authServiceMock, svsDescCollabServiceMock,valuationSharedServiceMock,routerMock,dataSharingMock,new LoadingService());
    expect(fixture).toBeTruthy();
  });

  it('root', () => {
    expect(fixture.title).toBe('bpi-ui');
    expect(fixture.items).toEqual([]);

  });
});
