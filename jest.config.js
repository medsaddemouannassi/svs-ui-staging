module.exports = {
	"preset": "jest-preset-angular",
	"setupFilesAfterEnv": [
			"<rootDir>/setupJest.ts"
	],
	"transformIgnorePatterns": [
			"node_modules/(?!@ngrx|ngx-socket-io)" // Last any packages here that error
	],
	"transform": {
			"^.+\\.(ts|js|html)$": "ts-jest"
	},
	collectCoverage: true,
	reporters: ['default',  ['jest-sonar', {
		outputDirectory: 'reports',
		outputName: 'test-report.xml',
		reportedFilePath: 'absolute'
	}]],
	"testPathIgnorePatterns": [
			"<rootDir>/node_modules/",
			"<rootDir>/dist/",
			"<rootDir>/cypress/",
			"<rootDir>/src/test.ts",
	]
};
