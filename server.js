const express = require('express');
const cors = require('cors');
const app = express();
app.use(cors());

require("./server/routes")(app);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

app.set('port', process.env.PORT || 3000);
app.listen(app.get('port'));
module.exports = app;